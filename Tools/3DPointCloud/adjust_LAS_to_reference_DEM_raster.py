
import laspy
import numpy as np
import geopandas as gpd
from shapely.geometry import Point
from pyproj import CRS


from scipy.optimize import curve_fit

try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr



def poly_func(xy, a, b, c, d, e, f):
    x, y = xy
    return a + b*x + c*y + d*x*y + e*x**2 + f*y**2







EPSG = "EPSG:32648"
EPSG = "3857"
LL = "4326"

outRes = 10 # 10mt

Z_MIN = -20
Z_MAX = 400

IN_water = "D:\Mascot\Mascot_water.shp"
INGPS = 'D:\Mascot\GPS.shp'
shapefile = gpd.read_file(IN_water)
shapefile.to_crs(epsg=LL, inplace=True)
print(shapefile)
# Load the LAS file
las = laspy.read("D:\Mascot\points.las")


import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
# Extract the point data
points = np.vstack((las.x,las.y,las.z)).transpose()
print('Points RAW')
print(points)
# pointsDF = pd.DataFrame({
#     'x': points[0],
#     'y': points[1],
#     'z': points[2]
# })
# print(pointsDF)
# Create a GeoDataFrame
gdf = gpd.GeoDataFrame(points, geometry=gpd.points_from_xy(points[:,0], points[:,1]))
# Set the coordinate reference system (CRS)
gdf.set_crs(epsg=EPSG, inplace=True)
print('GDF Mercator')
print(gdf)
gdf.to_crs(epsg=LL, inplace=True)
print('GDF LL')
print(gdf)

xmin = gdf.geometry.x.min()
xmax = gdf.geometry.x.max()
ymin = gdf.geometry.y.min()
ymax = gdf.geometry.y.max()

xminUTM = gdf[0].min()
xmaxUTM = gdf[0].max()
yminUTM = gdf[1].min()
ymaxUTM = gdf[1].max()
widthM = (xmaxUTM - xminUTM) / outRes
heightM = (ymaxUTM - yminUTM) / outRes


# --------------   not used -----------------------
gcps = gpd.read_file(INGPS)
gcps.to_crs(epsg=LL, inplace=True)
print('GCP in LL ')
print(gcps)
gcps['x'] =  gcps.geometry.apply(lambda geom: geom.x) # or siply gcps.geometry.x
gcps['y'] =  gcps.geometry.apply(lambda geom: geom.y)
print(gcps)
# --------------------------------------------------------------------------------------
import fiona
from scipy.interpolate import griddata
gcp_data = fiona.open(INGPS)
print('GCP in FIONA ')
print(gcp_data)

raster_output = 'D:\Mascot\interpolated_elevation_invdist.tif'
print([xmin, ymin, xmax, ymax])
print(widthM, heightM)

options = gdal.GridOptions(format='GTiff', outputType=gdal.GDT_Float32, outputBounds=[xmin, ymin, xmax, ymax],\
                            algorithm='invdist',
                           #algorithm='linear',
                           #algorithm='invdistnn',
                           zfield='z', width=widthM, height=heightM) #txe=[xmin, xmax], tye=[ymin, ymax], tr=[0.0001,0.0001]
# Run gdal.Grid with the options
gdal.Grid(raster_output, INGPS, options=options)

raster_ds = gdal.Open(raster_output)
band = raster_ds.GetRasterBand(1)
cols = raster_ds.RasterXSize
rows = raster_ds.RasterYSize
elevation_array = band.ReadAsArray()
geotransform = raster_ds.GetGeoTransform()

# # Define the grid for interpolation
# print('size')
#
# x = np.linspace(xmin,xmax, cols)
# y = np.linspace(ymin,ymax, rows)
# grid_x, grid_y = np.meshgrid(x, y)
#
# # Interpolate the data
# grid_z = griddata((gcps['x'], gcps['y']), gcps['z'], (grid_x, grid_y), method='cubic')
# #grid_z = griddata((gcps['x'], gcps['y']), gcps['z'], (gdf.geometry.apply(lambda geom: geom.x),gdf.geometry.apply(lambda geom: geom.y)) , method='linear')
#
#
# driver = gdal.GetDriverByName('GTiff')
# out_raster = driver.Create('D:\Mascot\extrapolated_elevation.tif', grid_x.shape[1], grid_y.shape[0], 1, gdal.GDT_Float32)
# out_raster.SetGeoTransform(raster_ds.GetGeoTransform())
# out_band = out_raster.GetRasterBand(1)
# out_band.WriteArray(grid_z)
# out_raster.SetProjection(raster_ds.GetProjection())
# out_band.FlushCache()

raster_ds = None
out_raster = None
out_band = None


# --------------------------------------------------------------------------------------


# Create a grid for interpolation
grid_x, grid_y = np.mgrid[xmin:xmax:cols*1j,
                          ymin:ymax:rows*1j]

# Interpolate reference raster to point cloud locations
pointsLL = np.vstack((gdf.geometry.x,gdf.geometry.y,gdf[2])).transpose()
reference_elevations = griddata((grid_x.flatten(), grid_y.flatten()), elevation_array.flatten(), (pointsLL[:, 0], pointsLL[:, 1]), method='cubic')

las.z = reference_elevations
print('adjusted_points')
print(las)

las.write('D:\\Mascot\\adjusted_point_cloud.las')

print("The point cloud has been adjusted based on the reference raster elevation and saved as 'adjusted_point_cloud.las'.")

sys.exit()






