
import laspy
import numpy as np
import geopandas as gpd
import pandas as pd

from shapely.geometry import Point
from pyproj import CRS

from shapely.geometry import Point
from scipy.optimize import curve_fit
from scipy.spatial import cKDTree


try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr



def poly_func(xy, a, b, c, d, e, f):
    x, y = xy
    return a + b*x + c*y + d*x*y + e*x**2 + f*y**2




EPSG = "EPSG:32648"
EPSG = "3857"
LL = "4326"

outRes = 10 # 10mt

Z_MIN = -10
Z_MAX = 40

IN_water = "D:\Mascot\Mascot_water_areas.shp"
IN_water = "D:\Mascot\Water_dense_aoi.shp"
#INGPS = 'D:\Mascot\GPS.shp'
water_shp = gpd.read_file(IN_water)
water_shp.to_crs(epsg=LL, inplace=True)
print(water_shp)


# Load the LAS file
las = laspy.read("D:\Mascot\points.las")

print(las.header)
header = las.header
for attribute in dir(header):
    if not attribute.startswith('_') and not callable(getattr(header, attribute)):
        print(f"{attribute}: {getattr(header, attribute)}")


# Extract the point data
points = np.vstack((las.x,las.y,las.z,las.red, las.green, las.blue)).transpose()
print('Points RAW')
print(points)

# ---- filtering  points -------


# Build KDTree
tree = cKDTree(points)
# Define the number of neighbors
k = 20
# Query the k nearest neighbors for each point
distances, _ = tree.query(points, k=k)
# Calculate the mean and standard deviation of the distances
mean_distances = np.mean(distances, axis=1)
std_distance = np.std(mean_distances)
# Define a threshold for outliers
threshold = np.mean(mean_distances) + 2 * std_distance
# Filter out the outliers
points = points[mean_distances < threshold]


# -----  good job -----------


gdf = gpd.GeoDataFrame(points, geometry=gpd.points_from_xy(points[:,0], points[:,1]))
# Set the coordinate reference system (CRS)
gdf.set_crs(epsg=EPSG, inplace=True)
print('GDF Mercator')
print(gdf)
gdf.to_crs(epsg=LL, inplace=True)
gdf.rename(columns={0: 'x'}, inplace=True)
gdf.rename(columns={1: 'y'}, inplace=True)
gdf.rename(columns={2: 'z'}, inplace=True)
gdf.rename(columns={3: 'red'}, inplace=True)
gdf.rename(columns={4: 'green'}, inplace=True)
gdf.rename(columns={5: 'blue'}, inplace=True)

print('GDF LL')
print(gdf)

xmin = gdf.geometry.x.min()
xmax = gdf.geometry.x.max()
ymin = gdf.geometry.y.min()
ymax = gdf.geometry.y.max()

xminUTM = gdf['x'].min()
xmaxUTM = gdf['x'].max()
yminUTM = gdf['y'].min()
ymaxUTM = gdf['y'].max()
widthM = (xmaxUTM - xminUTM) / outRes
heightM = (ymaxUTM - yminUTM) / outRes

print(gdf)
#-----------------------------------------------------------------------------------------------

selected_points = gpd.sjoin(gdf, water_shp, op='within')
#selected_points = gpd.overlay(df1=shapefile, df2=gdf, how="intersection", keep_geom_type=True)
deltaZ = selected_points['z'].median()
print(deltaZ)


# adjust by subtracting the median value from the AOIs
# what about creating an interpolated plane to shift the final DEM or DTM ?


gdf['z'] -=deltaZ


filtered_gdf = gdf[(gdf['z'] > Z_MIN) & (gdf['z'] < Z_MAX) ]
xmin_1 = filtered_gdf.x.min()
ymin_1 = filtered_gdf.y.min()
zmin_1 = filtered_gdf.z.min()

# Filter points based on altitude
#mask = (las.z >= Z_MIN) & (las.z <= Z_MAX)
#filtered_points = las.points[mask]


# Create a new LAS file with the filtered points
header = laspy.LasHeader(point_format=2, version="1.2")

header.scales = las.header.scale
header.offsets = np.array([xmin_1,ymin_1,zmin_1])

# header.file_source_id = 1
# header.global_encoding = 0
# header.project_id = "00000000-0000-0000-0000-000000000000"
# header.system_identifier = "Impact"
# header.generating_software = "Impact v5"
# header.file_creation_day_of_year = 0  # Example: 23rd day of the year
# header.file_creation_year = 2025

#header.maxs = np.array([1100.0, 2100.0, 3100.0])
#header.mins = np.array([900.0, 1900.0, 2900.0])
las_out = laspy.LasData(header)

print(header)


las_out.x = filtered_gdf['x']
las_out.y = filtered_gdf['y']
las_out.z = filtered_gdf['z']
las_out.red = filtered_gdf['red']
las_out.green = filtered_gdf['green']
las_out.blue = filtered_gdf['blue']

las_out.write('D:\\Mascot\\filtered_output.las')

sys.exit()












las_out.points = selected_points
#las_out.write("D:\Mascot\points_selected.las")
print('-------------')
print(las.header.offsets)
print(las.header.scale)
print('-------------')
print(las_out.header.offsets)
print(las_out.header.scale)
print('-------------')



# Save the filtered LAS file
las_out.write('D:\\Mascot\\filtered_output.las')























sys.exit()

# --------------   not used -----------------------
gcps = gpd.read_file(INGPS)
gcps.to_crs(epsg=LL, inplace=True)
print('GCP in LL ')
print(gcps)
gcps['x'] =  gcps.geometry.apply(lambda geom: geom.x) # or siply gcps.geometry.x
gcps['y'] =  gcps.geometry.apply(lambda geom: geom.y)
print(gcps)
# --------------------------------------------------------------------------------------
import fiona
from scipy.interpolate import griddata
gcp_data = fiona.open(INGPS)
print('GCP in FIONA ')
print(gcp_data)

raster_output = 'D:\Mascot\interpolated_elevation_invdist.tif'
print([xmin, ymin, xmax, ymax])
print(widthM, heightM)

options = gdal.GridOptions(format='GTiff', outputType=gdal.GDT_Float32, outputBounds=[xmin, ymin, xmax, ymax],\
                            algorithm='invdist',
                           #algorithm='linear',
                           #algorithm='invdistnn',
                           zfield='z', width=widthM, height=heightM) #txe=[xmin, xmax], tye=[ymin, ymax], tr=[0.0001,0.0001]
# Run gdal.Grid with the options
gdal.Grid(raster_output, INGPS, options=options)

raster_ds = gdal.Open(raster_output)
band = raster_ds.GetRasterBand(1)
cols = raster_ds.RasterXSize
rows = raster_ds.RasterYSize
elevation_array = band.ReadAsArray()
geotransform = raster_ds.GetGeoTransform()

# # Define the grid for interpolation
# print('size')
#
# x = np.linspace(xmin,xmax, cols)
# y = np.linspace(ymin,ymax, rows)
# grid_x, grid_y = np.meshgrid(x, y)
#
# # Interpolate the data
# grid_z = griddata((gcps['x'], gcps['y']), gcps['z'], (grid_x, grid_y), method='cubic')
# #grid_z = griddata((gcps['x'], gcps['y']), gcps['z'], (gdf.geometry.apply(lambda geom: geom.x),gdf.geometry.apply(lambda geom: geom.y)) , method='linear')
#
#
# driver = gdal.GetDriverByName('GTiff')
# out_raster = driver.Create('D:\Mascot\extrapolated_elevation.tif', grid_x.shape[1], grid_y.shape[0], 1, gdal.GDT_Float32)
# out_raster.SetGeoTransform(raster_ds.GetGeoTransform())
# out_band = out_raster.GetRasterBand(1)
# out_band.WriteArray(grid_z)
# out_raster.SetProjection(raster_ds.GetProjection())
# out_band.FlushCache()

raster_ds = None
out_raster = None
out_band = None


# --------------------------------------------------------------------------------------


# Create a grid for interpolation
grid_x, grid_y = np.mgrid[xmin:xmax:cols*1j,
                          ymin:ymax:rows*1j]

# Interpolate reference raster to point cloud locations
pointsLL = np.vstack((gdf.geometry.x,gdf.geometry.y,gdf[2])).transpose()
reference_elevations = griddata((grid_x.flatten(), grid_y.flatten()), elevation_array.flatten(), (pointsLL[:, 0], pointsLL[:, 1]), method='cubic')

las.z = reference_elevations
print('adjusted_points')
print(las)

las.write('D:\\Mascot\\adjusted_point_cloud.las')

print("The point cloud has been adjusted based on the reference raster elevation and saved as 'adjusted_point_cloud.las'.")

sys.exit()


# --------------------------------------------------------------------------------------


las_coords = np.vstack((gdf.geometry.x, gdf.geometry.y)).transpose()
#gcp_coords = np.array(list(zip(gcp_data.geometry.x, gcp_data.geometry.y)))
gcp_coords = np.vstack((gcps['x'], gcps['y'])).transpose()
# Interpolate GCP elevations to the extent of the LAS file
interpolated_elevations = griddata(gcp_coords, gcps['z'], las_coords, method='cubic')
print('elevation interp.')
print(interpolated_elevations)
# Calculate the difference between interpolated elevations and LAS elevations
gdf['z_cor'] = interpolated_elevations
#gdf['z_cor'] = gdf[2] - interpolated_elevations
print('elevation diff.')
print(gdf)

# Create a new LAS file with the filtered points
# Create a new LAS file with the filtered points
header = las.header
new_las = laspy.create(point_format=las.point_format, file_version=las.header.version)

new_las.header = las.header
new_las.x = gdf[0]
new_las.y = gdf[1]
new_las.z = gdf['z_cor']


# Filter points based on altitude
mask = (new_las.z > Z_MIN)
mask1 = (new_las.z < Z_MAX)
filtered_points = new_las.points[mask*mask1]

#new_las.points = points
# Save the new LAS file
new_las.write("D:\Mascot\points_flat.las")
sys.exit()


# --------------------------------------------------------------------------------------


# Convert X, Y coordinates to raster grid indices
def world_to_pixel(geo_transform, x, y):
    ul_x = geo_transform[0]
    ul_y = geo_transform[3]
    x_dist = geo_transform[1]
    y_dist = geo_transform[5]
    pixel = int((x - ul_x) / x_dist)
    line = int((ul_y - y) / abs(y_dist))
    return pixel, line

print('Computing delta')
deltas = []
x_coords = gdf.geometry.x
y_coords = gdf.geometry.y
z_coords = gdf[2]
ct = 0
for x, y, z in zip(x_coords, y_coords, z_coords):

    pixel, line = world_to_pixel(geotransform, x, y)
    if 0 <= pixel < elevation_array.shape[1] and 0 <= line < elevation_array.shape[0]:
        gdf['z_cor'] = z - elevation_array[line, pixel]
        ct+=1
    else:
        gdf['z_cor'] = 0

print(gdf)
print(ct)
sys.exit()
# # Convert coordinates to pixel indices
# pixels, lines = world_to_pixel(geotransform, x_coords, y_coords)
#
# # Ensure indices are within bounds
# valid_mask = (pixels >= 0) & (pixels < raster_data.shape[1]) & (lines >= 0) & (lines < raster_data.shape[0])
#
# # Extract valid Z values from raster
# raster_z = np.full(z_coords.shape, np.nan)
# raster_z[valid_mask] = raster_data[lines[valid_mask], pixels[valid_mask]]

# Compute delta and adjust Z values
print('delats')
deltas = z_coords - raster_z
print('adjusting')
adjusted_z = np.where(np.isnan(deltas), z_coords, z_coords - deltas)
gdf['z_cor'] = adjusted_z
print(gdf)

sys.exit()


from scipy.interpolate import griddata

elev_diff = griddata((gcps['x'], gcps['y']), gcps['z'], (gdf.geometry.apply(lambda geom: geom.x),gdf.geometry.apply(lambda geom: geom.y)) , method='linear')

gdf['z_cor']=elev_diff
print('GDF Flat')
print(gdf)
# --------------------------------------------------------------------------------------
sys.exit()

# Fit the polynomial to the GCPs
popt, _ = curve_fit(poly_func, (gcps['x'], gcps['y']), gcps['z'])
print('popt')
print(popt)
print('--------')
print('GDF again')
print(gdf)
fit_elev = poly_func((gdf.geometry.apply(lambda geom: geom.x), gdf.geometry.apply(lambda geom: geom.y)), *popt)
print('fit elev')
print(fit_elev)

gdf['z_cor']=fit_elev
print('GDF Mercator after new z col ')
print(gdf)
header = las.header
new_las = laspy.create(point_format=las.point_format, file_version=las.header.version)

new_las.header = las.header
new_las.x = gdf[0]
new_las.y = gdf[1]
new_las.z = gdf['z_cor']
#new_las.points = points
# Save the new LAS file
new_las.write("D:\Mascot\points_flat.las")
sys.exit()
# --------------------------------------------------------------------------------------


# Create a new LAS file with the filtered points
# Create a new LAS file with the filtered points
header = las.header
new_las = laspy.create(point_format=las.point_format, file_version=las.header.version)

new_las.header = las.header
new_las.x = gdf[0]
new_las.y = gdf[1]
new_las.z = gdf['z_cor']
#new_las.points = points
# Save the new LAS file
new_las.write("D:\Mascot\points_flat.las")
sys.exit()
#las.z = points['z_corrected'].apply(lambda val: val.value)
#las.write('D:\Mascot\corrected_output.las')


sys.exit()




# Create a GeoDataFrame
gdf = gpd.GeoDataFrame(points, geometry=gpd.points_from_xy(points.x, points.y))
# Set the coordinate reference system (CRS)
gdf.set_crs(epsg=EPSG, inplace=True)
gdf.to_crs(epsg=LL, inplace=True)

print(gdf.head())

selected_points = gpd.sjoin(gdf, shapefile, op='within')
#selected_points = gpd.overlay(df1=shapefile, df2=gdf, how="intersection", keep_geom_type=True)
deltaZ = selected_points['z'].mean()
print(deltaZ)


# header = laspy.LasHeader(point_format=3, version="1.2")
# las_out = laspy.LasData(header)
# selected_points = laspy.LasData(header)
# selected_points.points = selected_points
# selected_points.write("D:\Mascot\points_selected.las")

# Assuming your GeoDataFrame has columns 'x', 'y', 'z'
# las_out.x = las.x,
# las_out.y = las.y,
# las_out.z = las.z - deltaZ
las.z -= deltaZ
# Filter points based on altitude
mask = (las.z > Z_MIN)
mask1 = (las.z < Z_MAX)
filtered_points = las.points[mask*mask1]


# Create a new LAS file with the filtered points
header = las.header
new_las = laspy.create(point_format=las.point_format, file_version=las.header.version)
new_las.points = filtered_points
new_las.header = header

# Save the new LAS file
new_las.write("D:\Mascot\points_shifted_filtered.las")

sys.exit()








