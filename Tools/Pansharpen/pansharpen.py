#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import gzip

# import GDAL/OGR modules
try:
    from osgeo import gdal, osr
except ImportError:
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import ImageStore
import ImageProcessing
import json
from Simonets_PROC_libs import *


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Image Pansharpening ')
    print('pansharpen.py pan.tif img1.tif,img2.tif,... [-b] -r -bitdepth overwrite ')
    print(' "" for all or use 5,4,3')
    print('-bitdepth Byte UInt16 Float32')
    print('-r {nearest,bilinear,cubic,cubicspline,lanczos,average}')
    sys.exit(1)

def getArgs(args):
    indir = None
    if (len(sys.argv) < 8):
        usage()
    else:
        try:
            pan_name=json.loads(sys.argv[1])[0]
        except:
            pan_name=sys.argv[1]
        try:
            img_names=json.loads(sys.argv[2])
        except:
            img_names=sys.argv[2].split(',')
        outsuffix=sys.argv[3]
        bands=sys.argv[4]
        resampling=sys.argv[5]
        outDataTypeName=sys.argv[6]
        exponent = sys.argv[7]
        overwrite=sys.argv[8]

        return pan_name,img_names,outsuffix,bands,resampling,outDataTypeName,exponent,overwrite




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_pansharpen(pan_name,img_names,suffix_name,bands,resampling,outDataTypeName,exponent,overwrite):

    try:
        if suffix_name == '':
            suffix_name='_pan'
        else:
            if suffix_name.endswith('.tif'):
                suffix_name=suffix_name.replace('.tif','')
            if suffix_name.startswith('_'):
                pass
            else:
                suffix_name='_'+suffix_name
        maxband=0
        if bands != '':
            bands=bands.split(",")
            maxband=max(bands)
            bands=' -b '.join(bands)
            bands=' -b '+bands
        print(bands)
        exponentVal=''
        if str(exponent) != '':
            exponentVal= '-exponent '+str(exponent)

        # initialize output log
        log = LogStore.LogProcessing('Pansharpen', 'pansharpen')

        log.set_input_file(pan_name)
        log.set_input_file(img_names)
        log.set_parameter('Overwrite output', overwrite)
        log.set_parameter('Out suffix',suffix_name)
        log.set_parameter('Bands',bands)
        log.set_parameter('Resampling',resampling)
        log.set_parameter('Out Data Type',outDataTypeName)

        log.set_parameter('Default gdal -spat_adjust',' intersection')
        log.set_parameter('Default gdal -threads ','ALL_CPUS')
        log.set_parameter('Default gdal -w ','1')
        log.set_parameter('Default gdal -exponent', exponent)
        log.set_parameter('Default gdal -co',' COMPRESS=LZW BIGTIFF=IF_SAFER')

        ImageProcessing.validPathLengths(img_names, None,len(suffix_name)+5, log)


        log.send_message("Running")
        outDataTypeArray={'Byte': 1, 'UInt16': 2,'Float32':6}
        outDataType=outDataTypeArray[outDataTypeName]
        print(outDataType)

        #-------------------------------------------------------------------
        # ------------------------- OPEN PAN     ---------------------------
        #-------------------------------------------------------------------
        pan_nameUnzip=''
        if pan_name.endswith('.gz'):
            log.send_message('Unzipping Pan band ')
            pan_nameUnzip = os.path.join(GLOBALS['data_paths']['tmp_data'],os.path.basename(pan_name).replace('.gz','_tmp.tif'))
            if os.path.exists(pan_nameUnzip):
                os.remove(pan_nameUnzip)


            inF = gzip.open(pan_name, 'rb')
            s = inF.read()
            inF.close()
            open(pan_nameUnzip, 'wb').write(s)
            s=None
            if os.path.exists(pan_nameUnzip):
                pan_name=pan_nameUnzip
            else:
                log.send_error(pan_name+' cannot be unzipped.')
                log.close()
                return

        log.send_message('Opening Pan band ')
        try:
            pan_ds = gdal.Open(pan_name, gdal.GA_ReadOnly)
            pan_bands=pan_ds.RasterCount
        except:
            log.send_error(pan_name+' is not valid')
            log.close()
            return

        print(pan_bands)
        prj=pan_ds.GetProjection()
        srs=osr.SpatialReference(wkt=prj)
        panEPSG=srs.GetAttrValue("AUTHORITY", 1)
        pan_DataType = pan_ds.GetRasterBand(1).DataType
        pan_DataTypeName = gdal.GetDataTypeName(pan_DataType)
        print(pan_DataType)
        pan_ds=None
        log.send_message('Pancromatic band ok ')

        for img_name in img_names:

            outname=img_name.replace(".tif",suffix_name)


            if (overwrite in ["True","true","Yes","Y","y",'1']):
                if os.path.exists(outname+'.tif'):#xx=clean_tmp_file(outdir+'//'+out_name)
                    os.remove(outname+'.tif')
                    log.send_message('Deleting output file')
                if os.path.exists(outname+'.tif.aux.xml'):#xx=clean_tmp_file(outdir+'//'+out_name)
                    os.remove(outname+'.tif.aux.xml')

            elif os.path.exists(outname+'.tif'):
                print("Already processed")
                log.send_message(outname+'.tif  <b>Already Processed </b>')
                continue

            #-------------------------------------------------------------------
            # ------------------------- TEST OVERLAPS  -------------------------
            #-------------------------------------------------------------------
            print("Overlap Test ")
            if not test_overlapping(pan_name,img_name):
                log.send_error("No overlap between: "+pan_name+" "+img_name)
                continue
            log.send_message("Overlap ok")

            #-------------------------------------------------------------------
            # ------------------------- OPEN SLAVE  ---------------------------
            #-------------------------------------------------------------------
            img_ds = gdal.Open(img_name, gdal.GA_ReadOnly)
            img_bands=img_ds.RasterCount
            print(img_bands)
            prj=img_ds.GetProjection()
            srs=osr.SpatialReference(wkt=prj)
            imgEPSG=srs.GetAttrValue("AUTHORITY", 1)
            img_DataType = img_ds.GetRasterBand(1).DataType
            img_DataTypeName = gdal.GetDataTypeName(img_DataType)
            print(img_DataType)
            img_ds=None






            if int(maxband) > int(img_bands):
                log.send_error("Input image does not have selected bands: "+str(maxband)+', found '+str(img_bands))
                continue

            img_name_warp=''
            if pan_DataType > img_DataType or panEPSG != imgEPSG:
                if pan_DataType > img_DataType:
                    log.send_message("Promoting image DataType to "+pan_DataTypeName)
                if panEPSG != imgEPSG:
                    log.send_message("Changing image PROJ to "+str(panEPSG))

                if os.path.exists(img_name+'_tmp'):
                        os.remove(img_name+'_tmp')
                if os.path.exists(img_name+'_tmp.aux.xml'):
                        os.remove(img_name+'_tmp.aux.xml')

                os.system("gdalwarp -of GTiff -multi -ot "+pan_DataTypeName+" -t_srs EPSG:"+str(panEPSG)+" "+img_name+" "+img_name+'_tmp')
                if os.path.exists(img_name+'_tmp'):
                    img_name=img_name+'_tmp'
                    img_name_warp=img_name
                else:
                    log.send_error("Cannot convert image DataType or projection")
                    continue


            log.send_message("Running pansharpening")
            res1 = os.system('python '+GLOBALS['root_path']+'/Libs/win64/GDAL/gdal_pansharpen.py -spat_adjust intersection -threads ALL_CPUS -co COMPRESS=LZW -co BIGTIFF=IF_SAFER "'+pan_name+'" "'+img_name+'" '+bands+' -r '+resampling+' "'+outname+'"')

            if os.path.exists(img_name_warp):
                    os.remove(img_name_warp)
            if os.path.exists(img_name_warp+'.aux.xml'):
                    os.remove(img_name_warp+'.aux.xml')

            if os.path.exists(outname):
                if outDataType < pan_DataType:
                    log.send_message("Rescaling values to match output DataType = "+outDataTypeName)
                    os.system("gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=IF_SAFER -scale " +exponentVal+" -ot "+outDataTypeName+" "+outname+" "+ outname+"_tmp")
                    os.remove(outname)
                    os.rename(outname+"_tmp",outname)
                    if os.path.exists(outname+'.aux.xml'):
                        os.remove(outname+'.aux.xml')
                gdal_edit = "gdal_edit"
                if (GLOBALS['OS'] == "unix"):
                    gdal_edit = "gdal_edit.py"
                res1 = os.system(gdal_edit+' '+outname+' -mo "Impact_product_type=pansharpen" -mo "Impact_bands='+bands+'" -mo "Impact_version='+IMPACT.get_version()+'"')
                os.system("gdaladdo " +outname+' 2 4')
                os.rename(outname,outname+'.tif')
                if os.path.exists(outname+'.aux.xml'):
                    os.remove(outname+'.aux.xml')
                ImageStore.update_file_statistics(outname+'.tif')
            else:
                log.send_error("Pansharpen error")
                continue

        if os.path.exists(pan_nameUnzip):
            os.remove(pan_nameUnzip)


    except Exception as e:
        log.send_error("Generic error: "+str(e))


    log.send_message("Completed.")
    log.close()
    return




if __name__ == "__main__":

    pan_name,img_names,suffix_name,bands,resampling,bitdepth,exponent,overwrite = getArgs(sys.argv)
    res=''
    try:

        res=run_pansharpen(pan_name,img_names,suffix_name,bands,resampling,bitdepth,exponent,overwrite)

    except:
        print("Error " + str(res))
