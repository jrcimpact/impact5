#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json
from scipy.ndimage import filters
import shutil
import numpy
import Colors

from collections import Counter


# import GDAL/OGR modules
try:
    from osgeo import ogr, osr, gdal
except ImportError:
    import ogr, osr, gdal

import gdal_calc

import time


# Import local libraries
#from __config__ import *
import LogStore
import IMPACT
from Simonets_PROC_libs import *
import ImageProcessing

sys.path.append(os.path.join(GLOBALS['root_path'],'Tools/Raster/'))
import Mosaic

gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Water Dynamics: classification of water pixels in stable, positive and negative balance')

    sys.exit(1)

def getArgs(args):

    if (len(sys.argv) < 12):
        print(len(sys.argv))
        usage()
    else:

        overwrite = sys.argv[1]

        try:
            # test if input file with images exists (list too long)
            if os.path.exists(sys.argv[2]):
                file_handler = open(sys.argv[2], 'r')
                img_names = json.loads(file_handler.read())
                file_handler.close()
                os.remove(sys.argv[2])
            else:
                img_names = json.loads(sys.argv[2])
        except:
            img_names = sys.argv[2].split(',')

        start_value = sys.argv[3]
        end_value = sys.argv[4]
        outname = sys.argv[5]
        green = str(sys.argv[6])
        swir = sys.argv[7]
        reproject = sys.argv[8]
        outEPSG = sys.argv[9]
        psizeX = sys.argv[10]
        psizeY = sys.argv[11]

        return overwrite, img_names, start_value, end_value, outname, green, swir, reproject, outEPSG, psizeX, psizeY



def RunWaterBalance(overwrite, img_names, start_value, end_value, OUTname, GREENb, SWIRb, reproject, outEPSG, psizeX, psizeY, tmp_indir):

    try:
        log = LogStore.LogProcessing('Water Balance', 'waterBalance')

        log.set_input_file(img_names)
        log.set_parameter('Output name', OUTname)
        log.set_parameter('Overwrite output', overwrite)
        log.set_parameter('Force reprojection', reproject)
        log.set_parameter('outEPSG', outEPSG)
        log.set_parameter('Pixel size X', psizeX)
        log.set_parameter('Pixel size Y', psizeY)

        kernelsize = 0
        GREEN = int(GREENb)
        SWIR = int(SWIRb)

        # psizeX = float(psizeX)
        # psizeY = float(psizeY)

        outputsize = 1  # enable

        master_mask = ''
        slave_mask = ''


        # initialize output log
        OUTname = os.path.join(GLOBALS['data_paths']['data'],os.path.basename(OUTname).replace('.tif',''))


        log.set_parameter('Out name ',OUTname+'.tif')
        index = "("+str(GREEN)+'-'+str(SWIR)+")/("+str(GREEN)+'+'+str(SWIR)+")"
        log.set_parameter('Index', index)
        log.set_parameter('Start_value', str(start_value))
        log.set_parameter('Snd_value', str(end_value))


        log.set_parameter('Overwrite ', overwrite)
        start_value = float(start_value)
        end_value = float(end_value)

        ImageProcessing.validPathLengths(OUTname, None, 9, log)

        if reproject in ['Yes', 'Y', True, 'True', 1]:
            reproject = True
        else:
            reproject = False

        force_reprojection = False

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------

        outIndex = OUTname + "_Index.tif"
        if os.path.exists(outIndex):
            if overwrite == 'Yes':
                try:
                    os.remove(outIndex)
                    if os.path.exists(outIndex+".aux.xml"):
                        os.remove(outIndex+".aux.xml")


                except Exception as e:
                    print(str(e))
                    log.send_error('Cannot delete output file: ' + str(e))
                    log.close()
                    return False
            else:
                log.send_warning(outIndex+' <b> Already Exists </b>')
                log.close()
                return True

        outPercentage = OUTname + "_Normalized_Percentage.tif"
        if os.path.exists(outPercentage):
            os.remove(outPercentage)
        outCount = OUTname + "_Percentage.tif"
        if os.path.exists(outCount):
            os.remove(outCount)

        # -------------------------------------------------------------------
        # ------------------------- TEST OVERLAPS  -------------------------
        # -------------------------------------------------------------------
        gdal.TermProgress(0.0)
        log.send_message('Overlap Test')
        master_image = img_names[0]
        for img_name in img_names:
            print(img_name)
            if not test_overlapping(master_image,img_name):
                log.send_error("No overlap between: "+master_image+" "+img_name)
                log.close()
                return
        print("Overlap OK")
        # -------------------------------------------------------------------
        # ------------------------- TEST BANDS and PROJ  --------------------
        # -------------------------------------------------------------------
        driver = gdal.GetDriverByName("GTiff")
        projections = []
        dataType = []
        bands = []
        log.send_message('Retrieving image info')
        for img_name in img_names:
            in_ds = gdal.Open(img_name)
            try:
                Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()

            except:
                log.send_error("Projection error in file " + img_name)
                log.close()
                return

            Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()
            projections.append(Proj4)

            dataType.append(in_ds.GetRasterBand(1).DataType)
            bands.append(in_ds.RasterCount)
            in_ds = None

        #Selprojection = Proj4  # take one, if not same, reset to most freq

        Selprojection = "EPSG:"+str(outEPSG)

        print(bands)
        print(max([GREEN,SWIR]))
        print(Selprojection)
        print('---------')

        if not all(x>=max([GREEN,SWIR]) for x in bands):
            log.send_error("Images do not have selected bands: required "+str(max(GREEN,SWIR))+' got '+str(bands))
            log.close()
            return

        if not all(x == bands[0] for x in bands):
            log.send_warning(
                'Input files do not have same number of bands, please ensure selected bands are consistent among input files')

        # if not all(x==dataType[0] for x in dataType):
        #     log.send_warning('Input files do not have the same DataType, promoting output to '+str(gdal.GetDataTypeName(max(dataType))))

        if not all(x==projections[0] for x in projections):
            if reproject not in [True, "True","true","Yes","Y","y",'1']:
                log.send_error('Failure: images have different projections, allow on-the-fly reprojection' )
                log.close()
                return
            #data = Counter(projections)
            #Selprojection = str(data.most_common(1)[0][0])  # Returns the highest occurring item
            log.send_warning('Input files do not have the same projection, promoting output to ' + str(Selprojection))
            force_reprojection=True


        #-------------------------------------------------------------------
        # --------------         BUILD INDEX                       ---------
        #-------------------------------------------------------------------
        gdal.TermProgress(0.1)
        index_list = []
        for img_name in img_names:
            outVRT = os.path.join(tmp_indir,os.path.basename(img_name).replace('.tif','.vrt'))
            out_ds = gdal.Warp(outVRT, img_name, format='VRT', outputType=gdal.GDT_Float32, dstSRS=Selprojection)

            gdal_argv = ['myself']
            gdal_argv.append('--outfile')
            TMPoutIndex = outVRT.replace('.vrt','_Index.tif')
            gdal_argv.append(TMPoutIndex)

            gdal_argv.append('-A')
            gdal_argv.append(outVRT)
            gdal_argv.append('--A_band=' + str(GREEN))

            gdal_argv.append('-B')
            gdal_argv.append(outVRT)
            gdal_argv.append('--B_band=' + str(SWIR))

            gdal_argv.append('--calc=(A-B)/(A+B)')
            gdal_argv.append('--co')
            gdal_argv.append('COMPRESS=LZW')
            gdal_argv.append('--co')
            gdal_argv.append('BIGTIFF=IF_SAFER')
            #
            #gdal_argv.append('--debug')
            #
            sys.argv = gdal_argv
            # log.send_message(gdal_string)
            try:
                gdal_calc.main()
                if os.path.exists(TMPoutIndex):
                    index_list.append(TMPoutIndex)
                else:
                    raise Exception("Error: index calculation failure")
            except Exception as e:
                log.send_error("Expression error: " + str(e).replace("'", ""))
                log.close()
                return

        gdal.TermProgress(0.2)

        input_file_list = os.path.join(tmp_indir, 'input_list.txt')
        f = open(input_file_list, 'w')
        for img in index_list:
            filedata = f.write(img + ' ')
        f.close()

        TMPoutMosaName = os.path.join(tmp_indir,'mosaic.tif')
        gdal_merge = "gdal_merge"
        if (GLOBALS['OS'] == "unix"):
            gdal_merge = "gdal_merge.py"

        res1 = os.system(gdal_merge + ' -tap -separate -co COMPRESS=LZW -co BIGTIFF=IF_SAFER -ps ' +str(psizeX) + ' ' + str(psizeY)+' -o ' + TMPoutMosaName + ' --optfile ' + input_file_list)

        if not os.path.exists(TMPoutMosaName):
            raise Exception("Error: index stack failure")

        # ------------ OUTPUT NBR Classification ---------------

        TMPoutPercentage = TMPoutMosaName.replace(".tif","_Normalized_Percentage.tif")
        if os.path.exists(TMPoutPercentage):
            os.remove(TMPoutPercentage)

        TMPoutCount = TMPoutMosaName.replace(".tif", "_Percentage.tif")
        if os.path.exists(TMPoutCount):
            os.remove(TMPoutCount)

        #-----------------------------------------------------------------------
        #  Open MOSAIC and classify according to user thresholds
        # -----------------------------------------------------------------------

        c = gdal.ColorTable()

        ctable = [

                                            # GAIN  GREEN to dark GREEN
            (Colors.hex2rgb('#bef17c')),
            (Colors.hex2rgb('#baed7a')),
            (Colors.hex2rgb('#b6e978')),
            (Colors.hex2rgb('#b2e576')),
            (Colors.hex2rgb('#aee274')),
            (Colors.hex2rgb('#aade72')),
            (Colors.hex2rgb('#a6da70')),
            (Colors.hex2rgb('#a3d76e')),
            (Colors.hex2rgb('#9fd36c')),
            (Colors.hex2rgb('#9bcf6a')),
            (Colors.hex2rgb('#97cc68')),
            (Colors.hex2rgb('#93c866')),
            (Colors.hex2rgb('#8fc464')),
            (Colors.hex2rgb('#8bc162')),
            (Colors.hex2rgb('#88bd60')),
            (Colors.hex2rgb('#84b95e')),
            (Colors.hex2rgb('#80b65c')),
            (Colors.hex2rgb('#7cb25a')),
            (Colors.hex2rgb('#78ae58')),
            (Colors.hex2rgb('#74ab56')),
            (Colors.hex2rgb('#70a754')),
            (Colors.hex2rgb('#6da352')),
            (Colors.hex2rgb('#69a050')),
            (Colors.hex2rgb('#659c4e')),
            (Colors.hex2rgb('#61984c')),
            (Colors.hex2rgb('#5d954b')),
            (Colors.hex2rgb('#599149')),
            (Colors.hex2rgb('#558d47')),
            (Colors.hex2rgb('#528a45')),
            (Colors.hex2rgb('#4e8643')),
            (Colors.hex2rgb('#4a8241')),
            (Colors.hex2rgb('#467f3f')),
            (Colors.hex2rgb('#427b3d')),
            (Colors.hex2rgb('#3e773b')),
            (Colors.hex2rgb('#3a7439')),
            (Colors.hex2rgb('#377037')),
            (Colors.hex2rgb('#336c35')),
            (Colors.hex2rgb('#2f6933')),
            (Colors.hex2rgb('#2b6531')),
            (Colors.hex2rgb('#27612f')),
            (Colors.hex2rgb('#235e2d')),
            (Colors.hex2rgb('#1f5a2b')),
            (Colors.hex2rgb('#1c5629')),
            (Colors.hex2rgb('#185327')),
            (Colors.hex2rgb('#144f25')),
            (Colors.hex2rgb('#104b23')),
            (Colors.hex2rgb('#0c4821')),
            (Colors.hex2rgb('#08441f')),
            (Colors.hex2rgb('#04401d')),
            (Colors.hex2rgb('#013d1c')),

                                             # W 2 W increase  cyan to blue
            (Colors.hex2rgb('#00f3e2')),
            (Colors.hex2rgb('#00efe1')),
            (Colors.hex2rgb('#00ece0')),
            (Colors.hex2rgb('#00e9e0')),
            (Colors.hex2rgb('#00e6df')),
            (Colors.hex2rgb('#00e3de')),
            (Colors.hex2rgb('#00e0de')),
            (Colors.hex2rgb('#00dcdd')),
            (Colors.hex2rgb('#00d9dc')),
            (Colors.hex2rgb('#00d6dc')),
            (Colors.hex2rgb('#00d3db')),
            (Colors.hex2rgb('#00d0da')),
            (Colors.hex2rgb('#00cdda')),
            (Colors.hex2rgb('#00c9d9')),
            (Colors.hex2rgb('#00c6d8')),
            (Colors.hex2rgb('#00c3d8')),
            (Colors.hex2rgb('#00c0d7')),
            (Colors.hex2rgb('#00bdd6')),
            (Colors.hex2rgb('#00bad6')),
            (Colors.hex2rgb('#00b6d5')),
            (Colors.hex2rgb('#00b3d4')),
            (Colors.hex2rgb('#00b0d4')),
            (Colors.hex2rgb('#00add3')),
            (Colors.hex2rgb('#00aad2')),
            (Colors.hex2rgb('#00a7d2')),
            (Colors.hex2rgb('#00a3d1')),
            (Colors.hex2rgb('#00a0d1')),
            (Colors.hex2rgb('#009dd0')),
            (Colors.hex2rgb('#009acf')),
            (Colors.hex2rgb('#0097cf')),
            (Colors.hex2rgb('#0094ce')),
            (Colors.hex2rgb('#0090cd')),
            (Colors.hex2rgb('#008dcd')),
            (Colors.hex2rgb('#008acc')),
            (Colors.hex2rgb('#0087cb')),
            (Colors.hex2rgb('#0084cb')),
            (Colors.hex2rgb('#0081ca')),
            (Colors.hex2rgb('#007dc9')),
            (Colors.hex2rgb('#007ac9')),
            (Colors.hex2rgb('#0077c8')),
            (Colors.hex2rgb('#0074c7')),
            (Colors.hex2rgb('#0071c7')),
            (Colors.hex2rgb('#006ec6')),
            (Colors.hex2rgb('#006ac5')),
            (Colors.hex2rgb('#0067c5')),
            (Colors.hex2rgb('#0064c4')),
            (Colors.hex2rgb('#0061c3')),
            (Colors.hex2rgb('#005ec3')),
            (Colors.hex2rgb('#005bc2')),
            (Colors.hex2rgb('#0058c2')),



                                                # W to W Decrease BLU YELLOW
            (Colors.hex2rgb('#1755c2')),
            (Colors.hex2rgb('#1b57be')),
            (Colors.hex2rgb('#2059ba')),
            (Colors.hex2rgb('#255bb6')),
            (Colors.hex2rgb('#295eb2')),
            (Colors.hex2rgb('#2e60ae')),
            (Colors.hex2rgb('#3362aa')),
            (Colors.hex2rgb('#3864a6')),
            (Colors.hex2rgb('#3c67a2')),
            (Colors.hex2rgb('#41699e')),
            (Colors.hex2rgb('#466b9a')),
            (Colors.hex2rgb('#4a6d96')),
            (Colors.hex2rgb('#4f7092')),
            (Colors.hex2rgb('#54728e')),
            (Colors.hex2rgb('#59748a')),
            (Colors.hex2rgb('#5d7686')),
            (Colors.hex2rgb('#627982')),
            (Colors.hex2rgb('#677b7e')),
            (Colors.hex2rgb('#6b7d7a')),
            (Colors.hex2rgb('#708076')),
            (Colors.hex2rgb('#758272')),
            (Colors.hex2rgb('#79846e')),
            (Colors.hex2rgb('#7e866a')),
            (Colors.hex2rgb('#838966')),
            (Colors.hex2rgb('#888b62')),
            (Colors.hex2rgb('#8c8d5f')),
            (Colors.hex2rgb('#918f5b')),
            (Colors.hex2rgb('#969257')),
            (Colors.hex2rgb('#9b9453')),
            (Colors.hex2rgb('#9f964f')),
            (Colors.hex2rgb('#a4984b')),
            (Colors.hex2rgb('#a99b47')),
            (Colors.hex2rgb('#ad9d43')),
            (Colors.hex2rgb('#b29f3f')),
            (Colors.hex2rgb('#b7a23b')),
            (Colors.hex2rgb('#bca437')),
            (Colors.hex2rgb('#c0a633')),
            (Colors.hex2rgb('#c5a82f')),
            (Colors.hex2rgb('#caab2b')),
            (Colors.hex2rgb('#cead27')),
            (Colors.hex2rgb('#d3af23')),
            (Colors.hex2rgb('#d8b11f')),
            (Colors.hex2rgb('#dcb41b')),
            (Colors.hex2rgb('#e1b617')),
            (Colors.hex2rgb('#e6b813')),
            (Colors.hex2rgb('#ebba0f')),
            (Colors.hex2rgb('#efbd0b')),
            (Colors.hex2rgb('#f4bf07')),
            (Colors.hex2rgb('#f9c103')),
            (Colors.hex2rgb('#fdc300')),

                                         # LOSS  VIOLET to RED/BLACK
            (Colors.hex2rgb('#a82eb3')),
            (Colors.hex2rgb('#a52daf')),
            (Colors.hex2rgb('#a22cab')),
            (Colors.hex2rgb('#9f2ba8')),
            (Colors.hex2rgb('#9c2aa4')),
            (Colors.hex2rgb('#9929a1')),
            (Colors.hex2rgb('#96289d')),
            (Colors.hex2rgb('#932799')),
            (Colors.hex2rgb('#902696')),
            (Colors.hex2rgb('#8d2692')),
            (Colors.hex2rgb('#8a258f')),
            (Colors.hex2rgb('#87248b')),
            (Colors.hex2rgb('#842387')),
            (Colors.hex2rgb('#822284')),
            (Colors.hex2rgb('#7f2180')),
            (Colors.hex2rgb('#7c207d')),
            (Colors.hex2rgb('#791f79')),
            (Colors.hex2rgb('#761f75')),
            (Colors.hex2rgb('#731e72')),
            (Colors.hex2rgb('#701d6e')),
            (Colors.hex2rgb('#6d1c6b')),
            (Colors.hex2rgb('#6a1b67')),
            (Colors.hex2rgb('#671a63')),
            (Colors.hex2rgb('#641960')),
            (Colors.hex2rgb('#61185c')),
            (Colors.hex2rgb('#5f1859')),
            (Colors.hex2rgb('#5c1755')),
            (Colors.hex2rgb('#591652')),
            (Colors.hex2rgb('#56154e')),
            (Colors.hex2rgb('#53144a')),
            (Colors.hex2rgb('#501347')),
            (Colors.hex2rgb('#4d1243')),
            (Colors.hex2rgb('#4a1140')),
            (Colors.hex2rgb('#47113c')),
            (Colors.hex2rgb('#441038')),
            (Colors.hex2rgb('#410f35')),
            (Colors.hex2rgb('#3e0e31')),
            (Colors.hex2rgb('#3c0d2e')),
            (Colors.hex2rgb('#390c2a')),
            (Colors.hex2rgb('#360b26')),
            (Colors.hex2rgb('#330a23')),
            (Colors.hex2rgb('#300a1f')),
            (Colors.hex2rgb('#2d091c')),
            (Colors.hex2rgb('#2a0818')),
            (Colors.hex2rgb('#270714')),
            (Colors.hex2rgb('#240611')),
            (Colors.hex2rgb('#21050d')),
            (Colors.hex2rgb('#1e040a')),
            (Colors.hex2rgb('#1b0306')),
            (Colors.hex2rgb('#190303'))
        ]

        for cid in range(0, 50):
            c.SetColorEntry(cid, (0,0,0))

        for cid in range(0, len(ctable)):
            c.SetColorEntry(cid+50, ctable[cid])

        c.SetColorEntry(250, ctable[len(ctable) - 1])
        c.SetColorEntry(251, ctable[len(ctable) - 1])

        for cid in range(252,256):
            c.SetColorEntry(cid, (250,250,250))
        # NO WATER on both period


        src_ds = gdal.Open(TMPoutMosaName)
        num_bands = src_ds.RasterCount

        TOTAL = numpy.zeros((src_ds.RasterYSize, src_ds.RasterXSize), dtype=numpy.float32)
        COUNT = numpy.zeros((src_ds.RasterYSize, src_ds.RasterXSize), dtype=numpy.byte)


        progress = 0.7/num_bands
        for b in range(1,num_bands):
            T1 = src_ds.GetRasterBand(b).ReadAsArray().astype(numpy.float32)
            T2 = src_ds.GetRasterBand(b+1).ReadAsArray().astype(numpy.float32)


            T1[T1 < -1] = -1
            T1[T1 > 1] = 1
            T1[T1 == 0] = 0.0000000001

            T2[T2 < -1] = -1
            T2[T2 > 1] = 1
            T2[T2 == 0] = 0.0000000001

            WT1 = (T1 >= start_value) * (T1 <= end_value)
            WT2 = (T2 >= start_value) * (T2 <= end_value)
            WATER = WT1 * WT2

            TOTAL += WT1 * T1
            COUNT += WT1
            if b+1 == num_bands:
                print("LAST band")
                TOTAL += WT2 * T2
                COUNT += WT2

            diff = ((T2 - T1) * 50 + 101) * WATER * (T2 >= T1)         # STABLE but POSITIVE BALANCE
            diff += ((T1 - T2) * 50 + 151) * WATER * (T1 > T2)          # STABLE but NEGATIVE BALANCE

            diff += (T1 * 50 + 201) * ((WT1 == 1) * (WT2 == 0))  # LOSS

            diff += (T2 * 50 + 50) * ((WT1 == 0) * (WT2 == 1))   # GAIN   50-> 100

            diff[diff > 254] = 254
            diff[diff < 0] = 0

            diff[(WT1 == 0) * (WT2 == 0)] = 255

            WT1 = None
            WT2 = None
            T1 = None
            T2 = None

            # ------------ OUTPUT NBR Classification ---------------

            outClass = OUTname + "_T" + str(b) + "-T" + str(b+1) + "_class.tif"
            TMPoutClass = os.path.join(tmp_indir,"_T" + str(b) + "-T" + str(b+1) + "_class.tif")
            dst_ds = driver.Create(TMPoutClass, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Byte,
                                   options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
            dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
            dst_ds.SetProjection(src_ds.GetProjectionRef())

            dst_ds.SetMetadata({'Impact_product_type': 'Water Dynamics', 'Impact_operation': "Water Dynamics",
                                'Impact_version': IMPACT.get_version()})

            outband = dst_ds.GetRasterBand(1)
            outband.WriteArray(diff)
            dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
            dst_ds.GetRasterBand(1).SetColorTable(c)
            dst_ds.FlushCache()
            dst_ds = None
            diff = None

            shutil.move(TMPoutClass,outClass)

            gdal.TermProgress(progress)




        print("Matrix MAX: ")
        maxVal = numpy.nanmax(TOTAL)
        print(maxVal)
        print('After MAx')

        TOTAL *= 100./maxVal
        print('-')
        TOTAL[(TOTAL > 0) * (TOTAL < 1)] = 1
        print('- - ')
        COUNT *= int(100./num_bands)

        print('OK')
        ctable100 = [
            (Colors.hex2rgb('#e44430')),
            (Colors.hex2rgb('#e14331')),
            (Colors.hex2rgb('#df4332')),
            (Colors.hex2rgb('#dd4234')),
            (Colors.hex2rgb('#db4235')),
            (Colors.hex2rgb('#d94237')),
            (Colors.hex2rgb('#d64138')),
            (Colors.hex2rgb('#d44139')),
            (Colors.hex2rgb('#d2403b')),
            (Colors.hex2rgb('#d0403c')),
            (Colors.hex2rgb('#ce403e')),
            (Colors.hex2rgb('#cc3f3f')),
            (Colors.hex2rgb('#c93f41')),
            (Colors.hex2rgb('#c73e42')),
            (Colors.hex2rgb('#c53e43')),
            (Colors.hex2rgb('#c33e45')),
            (Colors.hex2rgb('#c13d46')),
            (Colors.hex2rgb('#be3d48')),
            (Colors.hex2rgb('#bc3c49')),
            (Colors.hex2rgb('#ba3c4b')),
            (Colors.hex2rgb('#b83c4c')),
            (Colors.hex2rgb('#b63b4d')),
            (Colors.hex2rgb('#b43b4f')),
            (Colors.hex2rgb('#b13a50')),
            (Colors.hex2rgb('#af3a52')),
            (Colors.hex2rgb('#ad3a53')),
            (Colors.hex2rgb('#ab3955')),
            (Colors.hex2rgb('#a93956')),
            (Colors.hex2rgb('#a63857')),
            (Colors.hex2rgb('#a43859')),
            (Colors.hex2rgb('#a2385a')),
            (Colors.hex2rgb('#a0375c')),
            (Colors.hex2rgb('#9e375d')),
            (Colors.hex2rgb('#9c375f')),
            (Colors.hex2rgb('#993660')),
            (Colors.hex2rgb('#973661')),
            (Colors.hex2rgb('#953563')),
            (Colors.hex2rgb('#933564')),
            (Colors.hex2rgb('#913566')),
            (Colors.hex2rgb('#8e3467')),
            (Colors.hex2rgb('#8c3468')),
            (Colors.hex2rgb('#8a336a')),
            (Colors.hex2rgb('#88336b')),
            (Colors.hex2rgb('#86336d')),
            (Colors.hex2rgb('#84326e')),
            (Colors.hex2rgb('#813270')),
            (Colors.hex2rgb('#7f3171')),
            (Colors.hex2rgb('#7d3172')),
            (Colors.hex2rgb('#7b3174')),
            (Colors.hex2rgb('#793075')),
            (Colors.hex2rgb('#763077')),
            (Colors.hex2rgb('#742f78')),
            (Colors.hex2rgb('#722f7a')),
            (Colors.hex2rgb('#702f7b')),
            (Colors.hex2rgb('#6e2e7c')),
            (Colors.hex2rgb('#6c2e7e')),
            (Colors.hex2rgb('#692d7f')),
            (Colors.hex2rgb('#672d81')),
            (Colors.hex2rgb('#652d82')),
            (Colors.hex2rgb('#632c84')),
            (Colors.hex2rgb('#612c85')),
            (Colors.hex2rgb('#5e2b86')),
            (Colors.hex2rgb('#5c2b88')),
            (Colors.hex2rgb('#5a2b89')),
            (Colors.hex2rgb('#582a8b')),
            (Colors.hex2rgb('#562a8c')),
            (Colors.hex2rgb('#532a8e')),
            (Colors.hex2rgb('#51298f')),
            (Colors.hex2rgb('#4f2990')),
            (Colors.hex2rgb('#4d2892')),
            (Colors.hex2rgb('#4b2893')),
            (Colors.hex2rgb('#492895')),
            (Colors.hex2rgb('#462796')),
            (Colors.hex2rgb('#442797')),
            (Colors.hex2rgb('#422699')),
            (Colors.hex2rgb('#40269a')),
            (Colors.hex2rgb('#3e269c')),
            (Colors.hex2rgb('#3c259d')),
            (Colors.hex2rgb('#39259f')),
            (Colors.hex2rgb('#3724a0')),
            (Colors.hex2rgb('#3524a1')),
            (Colors.hex2rgb('#3324a3')),
            (Colors.hex2rgb('#3123a4')),
            (Colors.hex2rgb('#2e23a6')),
            (Colors.hex2rgb('#2c22a7')),
            (Colors.hex2rgb('#2a22a9')),
            (Colors.hex2rgb('#2822aa')),
            (Colors.hex2rgb('#2621ab')),
            (Colors.hex2rgb('#2321ad')),
            (Colors.hex2rgb('#2120ae')),
            (Colors.hex2rgb('#1f20b0')),
            (Colors.hex2rgb('#1d20b1')),
            (Colors.hex2rgb('#1b1fb3')),
            (Colors.hex2rgb('#191fb4')),
            (Colors.hex2rgb('#161eb5')),
            (Colors.hex2rgb('#141eb7')),
            (Colors.hex2rgb('#121eb8')),
            (Colors.hex2rgb('#101dba')),
            (Colors.hex2rgb('#0e1dbb')),
            (Colors.hex2rgb('#0c1dbd')),

            (Colors.hex2rgb('#0c1dbd'))  # 101
        ]
        c = gdal.ColorTable()
        c.SetColorEntry(0, (250, 250, 250))
        for cid in range(1, len(ctable100)):
            c.SetColorEntry(cid, ctable100[cid])


        ctableCount = [
            (Colors.hex2rgb('#e25653')),
            (Colors.hex2rgb('#df5652')),
            (Colors.hex2rgb('#dd5751')),
            (Colors.hex2rgb('#db5851')),
            (Colors.hex2rgb('#d85950')),
            (Colors.hex2rgb('#d65a50')),
            (Colors.hex2rgb('#d45b4f')),
            (Colors.hex2rgb('#d25c4f')),
            (Colors.hex2rgb('#cf5d4e')),
            (Colors.hex2rgb('#cd5e4e')),
            (Colors.hex2rgb('#cb5f4d')),
            (Colors.hex2rgb('#c9604d')),
            (Colors.hex2rgb('#c6614c')),
            (Colors.hex2rgb('#c4624b')),
            (Colors.hex2rgb('#c2634b')),
            (Colors.hex2rgb('#c0644a')),
            (Colors.hex2rgb('#bd654a')),
            (Colors.hex2rgb('#bb6649')),
            (Colors.hex2rgb('#b96749')),
            (Colors.hex2rgb('#b76848')),
            (Colors.hex2rgb('#b46948')),
            (Colors.hex2rgb('#b26a47')),
            (Colors.hex2rgb('#b06b47')),
            (Colors.hex2rgb('#ad6c46')),
            (Colors.hex2rgb('#ab6d45')),
            (Colors.hex2rgb('#a96e45')),
            (Colors.hex2rgb('#a76f44')),
            (Colors.hex2rgb('#a47044')),
            (Colors.hex2rgb('#a27143')),
            (Colors.hex2rgb('#a07243')),
            (Colors.hex2rgb('#9e7342')),
            (Colors.hex2rgb('#9b7442')),
            (Colors.hex2rgb('#997541')),
            (Colors.hex2rgb('#977641')),
            (Colors.hex2rgb('#957740')),
            (Colors.hex2rgb('#92783f')),
            (Colors.hex2rgb('#90793f')),
            (Colors.hex2rgb('#8e7a3e')),
            (Colors.hex2rgb('#8c7b3e')),
            (Colors.hex2rgb('#897c3d')),
            (Colors.hex2rgb('#877d3d')),
            (Colors.hex2rgb('#857e3c')),
            (Colors.hex2rgb('#827f3c')),
            (Colors.hex2rgb('#80803b')),
            (Colors.hex2rgb('#7e813b')),
            (Colors.hex2rgb('#7c823a')),
            (Colors.hex2rgb('#798339')),
            (Colors.hex2rgb('#778439')),
            (Colors.hex2rgb('#758538')),
            (Colors.hex2rgb('#738638')),
            (Colors.hex2rgb('#708737')),
            (Colors.hex2rgb('#6e8837')),
            (Colors.hex2rgb('#6c8936')),
            (Colors.hex2rgb('#6a8a36')),
            (Colors.hex2rgb('#678b35')),
            (Colors.hex2rgb('#658c35')),
            (Colors.hex2rgb('#638d34')),
            (Colors.hex2rgb('#618e33')),
            (Colors.hex2rgb('#5e8f33')),
            (Colors.hex2rgb('#5c9032')),
            (Colors.hex2rgb('#5a9132')),
            (Colors.hex2rgb('#579231')),
            (Colors.hex2rgb('#559331')),
            (Colors.hex2rgb('#539430')),
            (Colors.hex2rgb('#519530')),
            (Colors.hex2rgb('#4e962f')),
            (Colors.hex2rgb('#4c972f')),
            (Colors.hex2rgb('#4a982e')),
            (Colors.hex2rgb('#48992d')),
            (Colors.hex2rgb('#459a2d')),
            (Colors.hex2rgb('#439b2c')),
            (Colors.hex2rgb('#419c2c')),
            (Colors.hex2rgb('#3f9d2b')),
            (Colors.hex2rgb('#3c9e2b')),
            (Colors.hex2rgb('#3a9f2a')),
            (Colors.hex2rgb('#38a02a')),
            (Colors.hex2rgb('#36a129')),
            (Colors.hex2rgb('#33a229')),
            (Colors.hex2rgb('#31a328')),
            (Colors.hex2rgb('#2fa427')),
            (Colors.hex2rgb('#2ca527')),
            (Colors.hex2rgb('#2aa626')),
            (Colors.hex2rgb('#28a726')),
            (Colors.hex2rgb('#26a825')),
            (Colors.hex2rgb('#23a925')),
            (Colors.hex2rgb('#21aa24')),
            (Colors.hex2rgb('#1fab24')),
            (Colors.hex2rgb('#1dac23')),
            (Colors.hex2rgb('#1aad23')),
            (Colors.hex2rgb('#18ae22')),
            (Colors.hex2rgb('#16af21')),
            (Colors.hex2rgb('#14b021')),
            (Colors.hex2rgb('#11b120')),
            (Colors.hex2rgb('#0fb220')),
            (Colors.hex2rgb('#0db31f')),
            (Colors.hex2rgb('#0bb41f')),
            (Colors.hex2rgb('#08b51e')),
            (Colors.hex2rgb('#06b61e')),
            (Colors.hex2rgb('#04b71d')),
            (Colors.hex2rgb('#02b81d')),

            (Colors.hex2rgb('#1e9314'))
        ]
        c1 = gdal.ColorTable()
        c1.SetColorEntry(0, (250, 250, 250))
        for cid in range(1, len(ctableCount)):
            c1.SetColorEntry(cid, ctableCount[cid])





        # SAVE Normalized_percentage

        dst_ds = driver.Create(TMPoutPercentage, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Byte,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
        dst_ds.SetProjection(src_ds.GetProjectionRef())

        dst_ds.SetMetadata({'Impact_product_type': 'Water Dynamics', 'Impact_operation': "Normalized_Percentage",
                            'Impact_version': IMPACT.get_version()})
        #dst_ds.GetRasterBand(1).SetNoDataValue(0)
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(TOTAL)
        dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
        dst_ds.GetRasterBand(1).SetColorTable(c)
        dst_ds.FlushCache()
        dst_ds = None
        TOTAL = None


        dst_ds = driver.Create(TMPoutCount, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Byte,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
        dst_ds.SetProjection(src_ds.GetProjectionRef())

        dst_ds.SetMetadata({'Impact_product_type': 'Water Dynamics', 'Impact_operation': "Percentage",
                            'Impact_version': IMPACT.get_version()})
        #dst_ds.GetRasterBand(1).SetNoDataValue(0)
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(COUNT)
        dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
        dst_ds.GetRasterBand(1).SetColorTable(c1)
        dst_ds.FlushCache()
        dst_ds = None
        COUNT = None


        gdal.TermProgress(progress)
        src_ds.FlushCache()
        src_ds = None





        # MOVING DATA to OUTPUT folder
        shutil.move(TMPoutMosaName, outIndex)
        shutil.move(TMPoutPercentage, outPercentage)
        shutil.move(TMPoutCount, outCount)



        gdal.TermProgress(1.0)
        log.send_message("Completed.")
        log.close()


    except Exception as e:
        print(str(e))
        log.send_error("Water Balance  error: "+str(e).replace("'",''))



        if os.path.exists(OUTname+'.tif.aux.xml'):
            os.remove(OUTname+'.tif.aux.xml')
        if os.path.exists(OUTname+".tif"):
            os.remove(OUTname+".tif")


        log.close()
        return False


if __name__ == "__main__":


    # overwrite = 'Yes'
    # master = "E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\Landsat_2015-11-30_utm.tif"
    # slave = "E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\Landsat_2016-02-02_utm.tif"
    #
    # master_mask="E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\AL_result_14_class_26_52_utm.tif"
    # slave_mask="E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\AL_result_14_class_26_52_utm.tif"
    # OUTname = "Degradation_simple_utm.tif"
    #
    # NIR=4
    # SWIR=6
    # kernelsize=7  # 210mt -> 7+7 14 in landsat and 42 in Sentinel
    # save_index = 'Yes'
    # #OUTname='E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\VECTOR_data\\User_Features\\Test_afr.shp'
    # res=RunDegradation(overwrite,master,slave,master_mask,slave_mask,NIR,SWIR,OUTname,kernelsize,save_index)

    overwrite, img_names, start_value, end_value, outname, GREEN, SWIR, reproject, outEPSG, psizeX, psizeY = getArgs(sys.argv)
    tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_water")
    shutil.rmtree(tmp_indir, ignore_errors=True)
    if not os.path.exists(tmp_indir):
        os.makedirs(tmp_indir)

    res = RunWaterBalance(overwrite, img_names, start_value, end_value, outname, GREEN, SWIR, reproject, outEPSG, psizeX, psizeY, tmp_indir)
    shutil.rmtree(tmp_indir, ignore_errors=True)