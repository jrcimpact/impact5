#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import psutil
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import Settings
import IMPACT
import ImageStore
from Simonets_EO_libs import *
from Simonets_PROC_libs import get_image_medians_from_mask
import ImageProcessing


SETTINGS = Settings.load()
numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data unmixing ')
    print('RUN_image_unmixing.py overwrite apply_forest_normalization infiles')
    print()
    sys.exit(0)

def getArgs(args):

    infiles = None
    if (len(sys.argv) < 4):
        usage()
    else:
        overwrite=sys.argv[1]
    apply_fnorm=sys.argv[2]
    infiles=json.loads(sys.argv[3])
    if ( infiles is None ):
        usage()

    else:
        return overwrite,apply_fnorm,infiles


def unmix_tile(src_ds,MinX,MinY,MaxX,MaxY,fnorm_delta_val,endmembers):
    #print MinX,MaxX
    #print MinY,MaxY
    OUTIMG = numpy.zeros((MaxY-MinY,MaxX-MinX,src_ds.RasterCount),numpy.float32)
    for band in range( src_ds.RasterCount ):
        OUTIMG[:,:,band]=src_ds.GetRasterBand(band+1).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY).astype(numpy.float32)
        if len(fnorm_delta_val) > 1:
                    MASK=(OUTIMG[:,:,band]==0).astype(bool)

                    numpy.add(OUTIMG[:,:,band],fnorm_delta_val[band],OUTIMG[:,:,band])
                    OUTIMG[MASK]=0
    OUTIMG[~numpy.isfinite(OUTIMG)] = 0
    pi = numpy.dot(endmembers, numpy.transpose(endmembers))
    pi = numpy.dot(numpy.linalg.inv(pi), endmembers)

    #(M, N, B) = OUTIMG.shape
    M = MaxY-MinY
    N = MaxX-MinX
    unmixed = numpy.zeros((M, N, endmembers.shape[0]), numpy.float32)

    for i in range(M):
        for j in range(N):
            unmixed[i, j] = numpy.dot(pi, OUTIMG[i,j].astype(numpy.float32))


    OUTIMG = None
    return unmixed



def getEndMembers(index):
    params = SETTINGS['processing_classification'][index]
    values = []
    values.append(float(params['b']))
    values.append(float(params['g']))
    values.append(float(params['r']))
    if "rededge" in params:
        values.append(float(params['rededge']))
    if "near" in params:
        values.append(float(params['near']))
    if "swir1" in params:
        values.append(float(params['swir1']))
    if "swir2" in params:
        values.append(float(params['swir2']))
    return values



# ----------------------------------------------------------------------------------------------------------------	
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input list and process file according to flags 
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_unmixing(overwrite,apply_fnorm,infiles):

    # initialize output log
    log = LogStore.LogProcessing('Unmixing', 'unmix')


    REFmask = GLOBALS['root_path']+'/Gui/maps/Global_EVG_map_ll.tif'

    log.set_input_file(infiles)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Apply forest normalization', apply_fnorm)
    log.set_parameter('Forest mask', REFmask)

    ImageProcessing.validPathLengths(infiles, None,5, log)

    for IN in infiles:
        try:

            log.send_message("Opening file ...")
            try:
                src_ds = gdal.Open(IN)
            except:
                log.send_error("Error opening file: " + IN)
                continue

            OUT = ""

            #### LandSat ####
            if IN.endswith('_calrefbyt.tif'):
                OUT = IN.replace('_calrefbyt.tif','_unmix.tif')
                sensor = 'oli'
                fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization')) * 255.
                soilEndmember = getEndMembers('Landsat_soil_endmember')
                vegEndmember = getEndMembers('Landsat_vegetation_endmember')
                waterEndmember = getEndMembers('Landsat_water_endmember')
                endmembers = numpy.array([soilEndmember, vegEndmember, waterEndmember]) * 255.


            #### LandSat float####
            if IN.endswith('_calrefflt.tif'):
                OUT = IN.replace('_calrefflt.tif','_unmix.tif')
                sensor = 'oli'
                fnorm_median_reference = getEndMembers('Landsat_forest_normalization')
                soilEndmember = getEndMembers('Landsat_soil_endmember')
                vegEndmember = getEndMembers('Landsat_vegetation_endmember')
                waterEndmember = getEndMembers('Landsat_water_endmember')
                endmembers = numpy.array([soilEndmember, vegEndmember, waterEndmember])


            #### RapidEye ####
            if IN.endswith('_calrefx10k.tif'):
                OUT = IN.replace('_calrefx10k.tif','_unmix.tif')
                sensor='RE-1'
                fnorm_median_reference = numpy.array(getEndMembers('RapidEye_forest_normalization'))  * 10000 # rededge is in but not used (0)
                soilEndmember = getEndMembers('RapidEye_soil_endmember')
                vegEndmember = getEndMembers('RapidEye_vegetation_endmember')
                waterEndmember = getEndMembers('RapidEye_water_endmember')
                endmembers=numpy.array([soilEndmember,vegEndmember,waterEndmember]) * 10000.



            #### SkyBox ####
            if (('_pansharp_bgrn_' in IN) or ("_ms_"  in IN)) :
                OUT = IN.replace('_calrefx10k.tif','_unmix.tif')
                sensor='SKYBOX'
                fnorm_median_reference=[0.086,0.062,0.043,0.247]
                soilEndmember = [0.14, 0.16, 0.22, 0.39]
                vegEndmember = [0.086,0.062,0.043,0.247]
                waterEndmember = [0.07, 0.039, 0.023, 0.031]
                endmembers=numpy.array([soilEndmember,vegEndmember,waterEndmember]) * 1000.


            #### Sentinel 2A 2B L1C L2A ####
            if ('S2A' in IN or 'S2B' in IN) and ("_MSI" in IN) and ("L1C_" in IN or "L2A_" in IN):
                OUT = IN.replace('.tif','_unmix.tif')
                sensor = 'S2A_L1C'
                soilEndmember = getEndMembers('Landsat_soil_endmember')
                vegEndmember = getEndMembers('Landsat_vegetation_endmember')
                waterEndmember = getEndMembers('Landsat_water_endmember')

                DataType = src_ds.GetRasterBand(1).DataType
                DataType = gdal.GetDataTypeName(DataType)

                print("Type: " + DataType)
                if DataType == "Byte":
                    fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization')) * 255.
                    endmembers = numpy.array([soilEndmember, vegEndmember, waterEndmember]) * 255.
                elif DataType == "UInt16":
                    fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization')) * 10000.
                    endmembers = numpy.array([soilEndmember, vegEndmember, waterEndmember]) * 10000.
                else:
                    fnorm_median_reference = getEndMembers('Landsat_forest_normalization')
                    endmembers=numpy.array([soilEndmember, vegEndmember, waterEndmember])





            if (OUT == '') :
                log.send_error("File not supported. Unable to determine sensor type or not a *_calref*")
                continue
            #OUT = "'" + OUT + "'"

            endmembers = endmembers.astype(float)
            log.send_message(endmembers)

            log.send_message("------")
            log.send_message("Input:" + IN)
            log.send_message("Output:" + OUT)
            log.send_message("Sensor:" + sensor)
            log.send_message("------")


            if (os.path.exists(OUT) and overwrite == 'No'):
                log.send_warning(OUT+' file <b>Already Processed </b>')
                continue


            try:
                fnorm_median_delta=[]
                if (os.path.exists(OUT)):
                    os.remove(OUT)
                if (os.path.exists(OUT+".aux.xml")):
                    os.remove(OUT+".aux.xml")

                if (apply_fnorm == 'Yes'):
                    try:
                        log.send_message("Apply Forest Normalization")
                        # compute bands median value inside a Mask (evergreen forest)
                        fnorm_median=get_image_medians_from_mask(IN,sensor,REFmask)
                        if (len(fnorm_median) > 1):
                            fnorm_median_delta = numpy.subtract(fnorm_median_reference,fnorm_median)
                            #print "DELTA 4 MIXING: --> "
                            log.send_message("Image: -->" + str(fnorm_median))
                            log.send_message("Reference: -->" + str(fnorm_median_reference))
                            log.send_message("Delta: -->" + str(fnorm_median_delta))

                    except:
                        log.send_warning("Forest Normalization Failure: memory error, keep processing ...")

                log.send_message("Processing")

                img_geot=src_ds.GetGeoTransform()
                log.send_message("Estimate memory ...")
                imgSR=osr.SpatialReference()
                imgSR.ImportFromWkt(src_ds.GetProjectionRef())

                driver = gdal.GetDriverByName("GTiff")
                dst_ds = driver.Create( OUT + "_tmp",src_ds.RasterXSize,src_ds.RasterYSize,3,gdal.GDT_Float32,options = [ 'BIGTIFF=IF_SAFER' ]) #GDT_Byte
                dst_ds.SetGeoTransform( src_ds.GetGeoTransform())
                dst_ds.SetProjection( src_ds.GetProjectionRef())
                dst_ds.SetMetadata({'Impact_product_type': 'unmix','Impact_version':IMPACT.get_version()})

                print("File size")
                print(os.stat(IN).st_size)
                TOTmem = ((os.stat(IN).st_size/1024/1000)*8)*9
                Memory_Buffer=1.5

                FREEmem = psutil.virtual_memory().free / 1024 / 1000
                log.send_message("Memory Free:"+str(FREEmem))
                log.send_message("Memory Need:"+str(TOTmem))

                ratio=2
                if (FREEmem < TOTmem + (TOTmem/100*20)):
                    ratio=int(round(TOTmem*Memory_Buffer/FREEmem*1.,))

                log.send_message("Processing using "+str(ratio)+" tiles")

                MaxX = src_ds.RasterXSize
                MaxY = src_ds.RasterYSize

                LenX = int(src_ds.RasterXSize/ratio)
                LenY = int(src_ds.RasterYSize/ratio)

                #print MaxX,MaxY
                #print LenX,LenY

                Xval=[]
                Yval=[]

                for x in range(1,ratio):
                    Xval.append(LenX*x)

                Xval.append(MaxX)

                for x in range(1,ratio):
                    Yval.append(LenY*x)

                Yval.append(MaxY)

                minval=[[],[],[]]
                maxval=[[],[],[]]
                MinX=0
                MinY=0
                for x in range(0,len(Xval)):

                    log.send_message("Tile " + str(x+1)+" of "+str(ratio))
                    for y in range(0,len(Yval)):

                        MaxX=Xval[x]
                        MaxY=Yval[y]
                        #OUTCLASS[MinY:MaxY,MinX:MaxX]=unmix_tile(src_ds,MinX,MinY,MaxX,MaxY,fnorm_median_delta,endmembers)
                        OUTCLASS = unmix_tile(src_ds,MinX,MinY,MaxX,MaxY,fnorm_median_delta,endmembers)

                        for band in range( endmembers.shape[0] ):
                            minval[band].append(numpy.min(OUTCLASS[:,:,band]))
                            maxval[band].append(numpy.max(OUTCLASS[:,:,band]))
                            outband = dst_ds.GetRasterBand(band+1)
                            outband.WriteArray(OUTCLASS[:,:,band],MinX, MinY)
                            dst_ds.FlushCache()

                        OUTCLASS=None

                        MinY=MaxY
                    MinX=MaxX
                    MinY=0

                for band in range(endmembers.shape[0]):
                    minval[band]=numpy.min(minval[band])
                    maxval[band]=numpy.max(maxval[band])



                log.send_message(str(minval))
                log.send_message(str(maxval))

                #----------------------------------
                # --- ---RESCALING function  ------
                #----------------------------------

                # range (-inf,0) -> (0,100)
                # range (0,1) -> (100,200)
                # range (1,+inf) -> (200,255)
                #except:
                try:
                        MinX=0
                        MinY=0
                        for x in range(0,len(Xval)):
                            for y in range(0,len(Yval)):
                                MaxX=Xval[x]
                                MaxY=Yval[y]
                                for band in range( endmembers.shape[0] ):
                                    outband = dst_ds.GetRasterBand(band+1)
                                    OUTCLASS = outband.ReadAsArray(MinX, MinY, (MaxX-MinX), (MaxY-MinY)).astype(numpy.float32)
                                    OUTCLASS = ((OUTCLASS*100+100)*(OUTCLASS>=0)*(OUTCLASS<=1)) + (((OUTCLASS/minval[band]*(-100))+100)*(OUTCLASS<0)) + (((OUTCLASS/maxval[band])*55+200)*(OUTCLASS>1))
                                    outband.WriteArray(OUTCLASS.astype(numpy.byte),MinX, MinY)
                                    dst_ds.FlushCache()
                                    outband = None
                                    OUTCLASS = None
                                MinY = MaxY
                            MinX = MaxX
                            MinY = 0

                        dst_ds=None
                        # --- convert original float unmix into byte with translate. keep tmp1 name to not be recognized by the cron refresh
                        res = os.system("gdal_translate -of GTiff -ot Byte -co COMPRESS=LZW -co TILED=YES -co BIGTIFF=IF_SAFER "+ OUT + "_tmp "+ OUT +"_tmp1" )
                        os.remove(OUT + "_tmp")

                        if os.path.exists(OUT + "_tmp1.aux.xml"):
                            os.rename(OUT + "_tmp1.aux.xml",OUT + ".aux.xml")
                        os.rename(OUT + "_tmp1", OUT)
                        ImageStore.update_file_statistics(OUT)

                        log.send_message("Completed.")
                        log.close()

                except Exception as e:
                    if (os.path.exists(OUT)):
                        dst_ds = None
                        src_ds = None
                        OUTCLASS = None
                        os.remove(OUT)

                    if (os.path.exists(OUT + "_tmp")):
                        os.remove(OUT)
                    log.send_error("Processing Error: "+str(e))

                dst_ds = None
                src_ds = None
                OUTCLASS = None


            # main block error
            except Exception as e:
                if (os.path.exists(OUT)):
                        dst_ds = None
                        src_ds = None
                        OUTCLASS = None
                        os.remove(OUT)
                if (os.path.exists(OUT + "_tmp")):
                    os.remove(OUT)
                log.send_error("Error 0:"+str(e))


        except Exception as e:
            log.send_error("Error on Unmix :"+str(e))
            continue

    log.close()


if __name__ == "__main__":
    overwrite,apply_fnorm,infiles = getArgs(sys.argv)
    run_unmixing(overwrite,apply_fnorm,infiles)
