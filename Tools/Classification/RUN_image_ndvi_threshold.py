#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import time
import numpy
import psutil
import json

# import GDAL/OGR modules
try:
    from osgeo import gdal, osr
except ImportError:
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import ImageStore
from Simonets_EO_libs import *
from Simonets_PROC_libs import get_image_medians_from_mask
import ImageProcessing

numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print()
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data index builder ')
    print('RUN_image_ndvi_threshold.py overwrite,numclass,red,nir,save_index,name_index,infiles')
    print()
    sys.exit(0)

def getArgs(args):

    infiles = None

    if (len(sys.argv) < 8):
        usage()
    else:
        overwrite = sys.argv[1]
        numclass = sys.argv[2]
        red = sys.argv[3]
        nir = sys.argv[4]
        save_index = sys.argv[5]
        name_index = sys.argv[6]
        infiles = json.loads(sys.argv[7])
        if infiles is None :
            usage()
        else:
            return overwrite, numclass, red, nir, save_index, name_index, infiles

# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input list and process file according to flags
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


def run_ndvi_threshold(overwrite, numclass, nir, red, save_index, name_index, infiles):

    # initialize output log
    log = LogStore.LogProcessing('Index Threshold', 'ndvi_threshold')
    log.set_input_file(infiles)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Band 1', nir)
    log.set_parameter('Band 2', red)
    log.set_parameter('Number of cluster', numclass)
    log.set_parameter('Save clustered index', save_index)
    log.set_parameter('Suffix name', name_index)


    if name_index.endswith('.tif'):
        name_index.replace('.tif','')
    if name_index.startswith('_'):
        name_index = name_index[2:]
    ImageProcessing.validPathLengths(infiles, None,len(name_index)+10, log)
    numpy.seterr(divide='ignore', invalid='ignore')
    red = int(red)
    nir = int(nir)
    numclass = int(numclass)-1  # o not used
    name_index = name_index.replace('"','')
    for IN in infiles:
        log.send_message("Processing "+ IN)
        try:

            OUT = IN.replace('.tif','_'+name_index)
            OUT_CLUSTER = IN
            #OUTMASK = IN.replace('.tif','_'+name_index+'_mask')

            if IN.endswith('_calrefbyt.tif'):
                OUT_CLUSTER = IN.replace('_calrefbyt.tif', '_'+name_index+'_cluster')
            if IN.endswith('_calrefflt.tif'):
                OUT_CLUSTER = IN.replace('_calrefflt.tif', '_'+name_index+'_cluster')
            if IN.endswith('_calrefx10k.tif'):
                OUT_CLUSTER = IN.replace('_calrefx10k.tif', '_'+name_index+'_cluster')
            if OUT_CLUSTER == IN:
                OUT_CLUSTER = IN.replace('.tif','_'+name_index+'_cluster')

            if (os.path.exists(OUT+'.tif') and overwrite == 'No'):
                log.send_warning(OUT+'.tif file <b>Already Processed </b>')
                continue
            if (os.path.exists(OUT_CLUSTER+'.tif') and overwrite == 'No'):
                log.send_warning(OUT_CLUSTER+'.tif file <b>Already Processed </b>')
                continue



            if os.path.exists(OUT+'.tif'):
                os.remove(OUT+'.tif')
            if os.path.exists(OUT+'.tif.aux.xml'):
                os.remove(OUT+'.tif.aux.xml')

            if os.path.exists(OUT_CLUSTER+'.tif'):
                os.remove(OUT_CLUSTER+'.tif')
            if os.path.exists(OUT_CLUSTER+'.tif.aux.xml'):
                os.remove(OUT_CLUSTER+'.tif.aux.xml')

            # if os.path.exists(OUTMASK):
            #     os.remove(OUTMASK)

            try:
                src_ds = gdal.Open(IN)
                num_bands = src_ds.RasterCount

                if max(red,nir) > num_bands :
                    log.send_error("Wrong band selection; SET B1="+str(red)+" B2="+str(nir)+" while file has "+str(num_bands)+" bands.")
                    continue

                img_geot = src_ds.GetGeoTransform()
                imgSR = osr.SpatialReference()
                imgSR.ImportFromWkt(src_ds.GetProjectionRef())

                driver = gdal.GetDriverByName("GTiff")
                print("Reading file ")

                # # --------  WRITE NDVI  --------------
                #  SLOOOOOWWWW
                # res1 = os.system('python '+GLOBALS['root_path']+'/OSGeo4W/bin/gdal_calc.py --type=Float32 --co COMPRESS=LZW --co BIGTIFF=IF_SAFER --outfile="'+INDEXTMP
                # +'" -A "'+IN+'" --A_band='+str(nir)+' -B "'+IN+'" --B_band='+str(red)
                # # +' --calc="((A.astype(numpy.float32)-B)/(A.astype(numpy.float32)+B))"'
                # +' --calc="((A.astype(numpy.float32) - B)/(A.astype(numpy.float32)+B))"')
                #


                # TOTmem = ((os.stat(IN).st_size/1024/1000)*8)*5
                TOTmem = ((src_ds.RasterXSize*src_ds.RasterYSize)*32*3)/1024/1000

                Memory_Buffer=2
                FREEmem=psutil.virtual_memory().free / 1024 / 1000
                log.send_message("Memory Free:"+str(FREEmem))
                log.send_message("Memory Need:"+str(TOTmem))

                ratio=1
                if (FREEmem < TOTmem + (TOTmem/100*20)):
                    ratio=int(round(TOTmem*Memory_Buffer/FREEmem*1.,))

                log.send_message("Processing using "+str(ratio)+" tiles")

                MaxX=src_ds.RasterXSize
                MaxY=src_ds.RasterYSize
                LenX=int(src_ds.RasterXSize/ratio)
                LenY=int(src_ds.RasterYSize/ratio)
                print(MaxX, MaxY)
                print(LenX, LenY)

                Xval=[]
                Yval=[]

                for x in range(1,ratio):
                    Xval.append(LenX*x)
                Xval.append(MaxX)

                for x in range(1,ratio):
                    Yval.append(LenY*x)
                Yval.append(MaxY)

                # --------------------------------------------------------------------------------
                # ---------------------      BUILD NDVI      -------------------------------------
                # --------------------------------------------------------------------------------

                ndvi_ds = driver.Create(OUT, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Float32,
                                        options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                ndvi_ds.SetGeoTransform(src_ds.GetGeoTransform())
                ndvi_ds.SetProjection(src_ds.GetProjectionRef())
                ndvi_ds.GetRasterBand(1).SetNoDataValue(255)
                ndvi_ds.SetMetadata({'Impact_product_type': 'Ndvi',
                                    'Impact_operation': "(" + str(nir) + '-' + str(red) + ")/(" + str(nir) + '+' + str(
                                        red) + ")", 'Impact_version': IMPACT.get_version()})

                # mask_ds = driver.Create(OUTMASK, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Byte,
                #                         options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                # mask_ds.SetGeoTransform(src_ds.GetGeoTransform())
                # mask_ds.SetProjection(src_ds.GetProjectionRef())


                MinX=0
                MinY=0
                start = time.time()
                try:
                    for x in range(0,len(Xval)):
                        #print "NDVI Tile " + str(x+1)+" of "+str(ratio)
                        log.send_message("Tile " + str(x+1)+" of "+str(ratio))
                        for y in range(0, len(Yval)):
                            MaxX=Xval[x]
                            MaxY=Yval[y]
                            if red != nir:
                                RED = src_ds.GetRasterBand(red).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY)#.astype(numpy.float32)
                                NIR = src_ds.GetRasterBand(nir).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY)#.astype(numpy.float32)
                                NDVI = (NIR*1.0-RED)/(NIR + RED*1.0)
                                NDVI[RED == 0] = 255
                                NDVI[NIR == 0] = 255
                            else:
                                NDVI = src_ds.GetRasterBand(red).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY)
                                NDVI[NDVI == 0] = 255
                            ndvi_ds.GetRasterBand(1).WriteArray(NDVI,MinX,MinY)
                            #mask_ds.GetRasterBand(1).WriteArray(MASK,MinX,MinY)
                            ndvi_ds.FlushCache()
                            #mask_ds.FlushCache()

                            NDVI = None
                            RED = None
                            NIR = None

                            MinY=MaxY
                        MinX=MaxX
                        MinY=0
                except Exception as e:
                    raise Exception("Error creating " + OUT + '.tif '+str(e))

                print("Index time: " + str(time.time() - start))
                ndvi_ds = None
                if os.path.exists(OUT):
                    print("Renaming out index " + OUT)
                    os.rename(OUT, OUT+'.tif')
                    ImageStore.update_file_statistics(OUT+'.tif')
                else:
                    raise Exception("Error creating "+OUT+'.tif')


                ndvi_ds = gdal.Open(OUT+'.tif')

                # --------------------------------------------------------------------------------
                # ---------------------      BUILD CLUSTER   -------------------------------------
                # --------------------------------------------------------------------------------

                if (save_index == 'Yes'):
                    log.send_message("Saving cluster ....")

                    # --------  WRITE CLASS  --------------
                    dst_ds = driver.Create(OUT_CLUSTER,src_ds.RasterXSize,src_ds.RasterYSize,1,gdal.GDT_Byte,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER'])
                    dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
                    c = gdal.ColorTable()
                    c.CreateColorRamp(0,(0,0,0),0,(0,0,0))
                    c.CreateColorRamp(1,(1,1,255),int(numclass/3),(200,50,1))
                    c.CreateColorRamp(int(numclass/3),(200,50,1),int(numclass/3)*2,(50,100,1))
                    c.CreateColorRamp(int(numclass/3)*2,(50,100,1),numclass+1,(0,255,1))
                    dst_ds.GetRasterBand(1).SetColorTable(c)
                    dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
                    dst_ds.SetProjection(src_ds.GetProjectionRef())
                    dst_ds.SetMetadata({'Impact_product_type': 'Ndvi classes','Impact_operation':"Ndvi classification" ,'Impact_version':IMPACT.get_version()})


                    start = time.time()
                    #stats = ndvi_ds.GetRasterBand(1).GetStatistics(0, 1)
                    imgMin = ndvi_ds.GetRasterBand(1).GetMinimum()
                    imgMax = ndvi_ds.GetRasterBand(1).GetMaximum()

                    print("---------min MAX ----------")
                    print(imgMin, imgMax)
                    # Next dev:  remove empty bin from Histo to maximize the range
                    # HISTO=ndvi_ds.GetRasterBand(1).GetHistogram(min=imgMin, max=imgMax,buckets=numclass)
                    # print type(HISTO)

                    # Numpy solution returns NDVI renge in float : GDAL NOT
                    # classes =numpy.histogram(NDVI,bins=numclass)[1]
                    # print classes
                    # print "----HIST BIN------"
                    # print numpy.histogram(NDVI)
                    # print "###########"
                    classes = numpy.arange(imgMin, imgMax, ((imgMax - imgMin) / (numclass * 1.0)))
                    # print classes

                    MinX=0
                    MinY=0
                    for x in range(0,len(Xval)):
                        #print " CLASS Tile " + str(x+1)+" of "+str(ratio)
                        log.send_message("CLASS Tile " + str(x+1)+" of "+str(ratio))
                        for y in range(0,len(Yval)):
                            startY = time.time()
                            MaxX = Xval[x]
                            MaxY = Yval[y]


                            NDVI = ndvi_ds.GetRasterBand(1).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY)
                            #MASK = mask_ds.GetRasterBand(1).ReadAsArray(MinX,MinY,MaxX-MinX,MaxY-MinY)
                            OUTCLASS = NDVI * 0

                            OUTCLASS[(NDVI < classes[0])] = 1
                            startC = time.time()
                            for i in range(len(classes)-1):
                                # mask = numpy.logical_and( NDVI >= classes[i], NDVI <= classes[i+1])
                                OUTCLASS[(NDVI >= classes[i])*(NDVI <= classes[i+1])]=i+2
                            # print "REC time: " + str(time.time() - startC)
                            OUTCLASS[(NDVI > classes[-1])] = numclass+1
                            OUTCLASS[NDVI == 255] = 0
                            dst_ds.GetRasterBand(1).WriteArray(OUTCLASS, MinX, MinY)
                            ndvi_ds.FlushCache()
                            dst_ds.FlushCache()

                            NDVI = None
                            #MASK = None
                            OUTCLASS = None

                            MinY = MaxY

                            print("Y time: " + str(time.time() - startY))

                        MinX = MaxX
                        MinY = 0
                    #
                    print("CLASS time: " + str(time.time() - start))
                    dst_ds = None
                    OUTCLASS = None

                    if os.path.exists(OUT_CLUSTER):
                        os.rename(OUT_CLUSTER, OUT_CLUSTER + '.tif')
                        ImageStore.update_file_statistics(OUT_CLUSTER + '.tif')

                dst_ds = None
                ndvi_ds = None
                src_ds = None
                #mask_ds = None

                OUTCLASS = None
                NDVI = None



                log.send_message("Completed.")

            except Exception as e:
                dst_ds = None
                src_ds = None
                ndvi_ds = None
                #mask_ds = None
                OUTCLASS = None
                #MASK = None
                NDVI = None

                if os.path.exists(OUT):
                    os.remove(OUT)
                if os.path.exists(OUT_CLUSTER):
                    os.remove(OUT_CLUSTER)
                # if os.path.exists(OUTMASK):
                #     os.remove(OUTMASK)

                log.send_error("Memory Error or Wrong Bands Selection :"+str(e))



        except Exception as e:

            dst_ds = None
            src_ds = None
            ndvi_ds = None
            #mask_ds = None
            OUTCLASS = None
            NDVI = None
            MASK = None

            if os.path.exists(OUT):
                os.remove(OUT)

            if os.path.exists(OUT_CLUSTER):
                os.remove(OUT_CLUSTER)
            # if os.path.exists(OUTMASK):
            #     os.remove(OUTMASK)

            log.send_error("Error :"+str(e))

            continue

    print("Done")
    # time.sleep(1)

    log.close()


if __name__ == "__main__":
    overwrite, numclass, nir, red, save_index, name_index, infiles = getArgs(sys.argv)
    run_ndvi_threshold(overwrite, numclass, nir, red, save_index, name_index, infiles)
