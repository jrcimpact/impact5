#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import multiprocessing
import json
from sklearn.cluster import MiniBatchKMeans
import numpy

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import Settings
import IMPACT


numpy.seterr(divide='ignore', invalid='ignore')
SETTINGS = Settings.load()


def usage():
    print('Author: Ceccherini Guido, Simonetti Dario, Marelli Andrea for European Commission  2017')
    print('Satellite data Kmeans Classification ')
    print()
    sys.exit(0)

def getArgs(args):
    infiles = None
    if (len(sys.argv) < 6):
        usage()
    else:
        infiles = sys.argv[1]
        out_suffix =  sys.argv[2]
        overwrite = sys.argv[3]
        Number0fClusters = sys.argv[4]
        max_iter = sys.argv[5]

        return infiles, out_suffix, overwrite, Number0fClusters, max_iter


def Kmeans_classification(infiles, out_suffix, overwrite, Number0fClusters, max_iter):


#	 Performs iterative clustering using the k-means algorithm.
#    Arguments:
#    `infiles` : The `MxNxB` image on which to perform clustering.
#    `Number0fClusters` (int) [default 10]:
#    Number of clusters to create.  The number produced may be less than
#    `nclusters`.
#    `max_iter` (int) [default 20]:
#    Max number of iterations to perform.

    # initialize output log
    log = LogStore.LogProcessing('Kmeans', 'kmeans')
    log.set_input_file(infiles)
    log.set_parameter('Number 0f Clusters', Number0fClusters)
    log.set_parameter('Max number of iterations to perform', max_iter)
    log.set_parameter('Output suffix', out_suffix)
    log.set_parameter('CPU', multiprocessing.cpu_count())

    for infile in infiles:
        try:

            log.send_message("Processing "+infile)
            outfile = infile.replace('.tif','_'+out_suffix.replace('.tif',''))
            log.set_parameter('Output ', outfile)
            # del tmp file
            try:
                os.remove(outfile)
            except:
                pass


            try:
                ds = gdal.Open(infile)
            except:
                log.send_error("Error opening file: " + infile)
                continue

            if (os.path.exists(outfile+'.tif') and overwrite == False):
                log.send_warning(outfile+' file <b>Already Processed </b>')
                continue
            if (os.path.exists(outfile+'.tif') and overwrite == True):
                try:
                    os.remove(outfile+'.tif')
                except Exception as e:
                    log.send_error("Failure deleting existing output file")
                    continue


            TestData = []

            # Read data from each band
            for band in range(ds.RasterCount):
                    band += 1
                    B = ds.GetRasterBand(band)
                    Array = B.ReadAsArray()
                    # Get shape of array
                    Shape = numpy.ma.shape(Array)
                    # Flatten to 1D array
                    Array = Array.flatten()
                    TestData.append(Array)
                    del Array



            TestData = numpy.array(TestData, dtype=numpy.dtype('float32')) # Convert to float to prevent sklearn error/warning message
            TestData = numpy.transpose(TestData)

            # Define the classifier
            clf = MiniBatchKMeans(n_clusters=Number0fClusters, init='k-means++', max_iter=max_iter, batch_size=10000, verbose=0, compute_labels=True, random_state=None, tol=0.0, max_no_improvement=100, init_size=2000, n_init=10, reassignment_ratio=0.05)

            log.send_message("Performing K-means classification...")
            clf.fit(TestData, y=None)
            labels = clf.predict(TestData)
            del TestData
            labels = labels + 1 #Add 1 to exclude zeros in output raster
            labels = numpy.reshape(labels, Shape)


            ###############################################################################
            #
            # print "Kmeans (info): Finished with %s classes" % Number0fClusters
            # log.send_message("Kmeans (info): Finished with %s classes:"+ Number0fClusters)
            #     #print "Isodata(info): Number of Iterations: %s" % (iter + 1)


            #K = 5

            driver = gdal.GetDriverByName("GTiff")
            dst_ds = driver.Create(outfile, ds.RasterXSize, ds.RasterYSize, 1, gdal.GDT_Byte, options = [ 'BIGTIFF=IF_SAFER','COMPRESS=LZW' ]) #GDT_Byte
            outband = dst_ds.GetRasterBand(1)
            outband.WriteArray(labels)
            dst_ds.SetGeoTransform(ds.GetGeoTransform())
            dst_ds.SetProjection(ds.GetProjectionRef())
            dst_ds.SetMetadata({'Impact_product_type': 'cluster', 'Impact_operation':'kmeans', 'Impact_version': IMPACT.get_version()})
            dst_ds.FlushCache()
            c = gdal.ColorTable()
            c.CreateColorRamp(0,(0,0,0),0,(0,0,0))
            c.CreateColorRamp(1,(1,1,255),int(Number0fClusters/3),(200,50,1))
            c.CreateColorRamp(int(Number0fClusters/3),(200,50,1),int(Number0fClusters/3)*2,(50,100,1))
            c.CreateColorRamp(int(Number0fClusters/3)*2,(50,100,1),Number0fClusters+1,(1,255,1))
            for i in range(0,Number0fClusters-1):
                if c.GetColorEntry(i)[0] == 0:
                    c.SetColorEntry(i,(1,c.GetColorEntry(i)[1],c.GetColorEntry(i)[2]))
                if c.GetColorEntry(i)[1] == 0:
                    c.SetColorEntry(i,(c.GetColorEntry(i)[1],1,c.GetColorEntry(i)[2]))
                if c.GetColorEntry(i)[2] == 0:
                    c.SetColorEntry(i,(c.GetColorEntry(i)[1],c.GetColorEntry(i)[2],1))




            dst_ds.GetRasterBand(1).SetColorTable(c)
            dst_ds = None

            if os.path.exists(outfile):
                os.rename(outfile,outfile+'.tif')
            else:
                log.send_error("Failure in saving out file")

        except Exception as e:
            log.send_error("Error "+str(e))
            # del tmp file
            try:
                os.remove(outfile)
            except:
                pass


    log.send_message("Done")
    log.close()
    return 'ok'

if __name__ == "__main__":
    infiles, out_suffix, overwrite, Number0fClusters, max_iter = getArgs(sys.argv)

    if (overwrite in ["True","true","Yes","Y","y",'1']):
        overwrite = True
    else:
        overwrite = False
    #Kmeans_classification(['E:/IMPACT/IMPACT2/IMPACT/DATA/USER_data/Landsat_test_data/oli_226-068_03072014_calrefbyt.tif'], out_suffix, overwrite, int(Number0fClusters), int(max_iter))
    Kmeans_classification(json.loads(infiles), out_suffix, overwrite, int(Number0fClusters), int(max_iter))



