#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import json

# Import local libraries
from __config__ import *
import LogStore
from Simonets_EO_libs import *
from Simonets_PROC_libs import get_image_medians_from_mask
from PiNo_classifier import *
import ImageProcessing


numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data classifier ---> Pixel Image 2 Natural Objects')
    print('RUN_PiNo_classifier.py overwrite apply_fores_normalization add_palette images')
    print()
    sys.exit(0)


def getArgs(args):
    infiles = None
    if (len(sys.argv) < 6):
        usage()
    else:
        overwrite=sys.argv[1]
        apply_fnorm=sys.argv[2]
        palette=sys.argv[3]
        kernel_size=sys.argv[4]
        infiles=json.loads(sys.argv[5])
        if ( infiles is None ):
            usage()

        else:
            return overwrite, apply_fnorm, palette, kernel_size, infiles

# ----------------------------------------------------------------------------------------------------------------	
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


def run_PiNo_classifier(overwrite, apply_fnorm, palette, kernel_size, infiles):

    # initialize output log
    log = LogStore.LogProcessing('Classification', 'class')

    REFmask=GLOBALS['root_path']+'/Gui/maps/Global_EVG_map_ll.tif'

    log.set_input_file(infiles)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Apply forest normalization', apply_fnorm)
    log.set_parameter('Forest mask', REFmask)
    log.set_parameter('Add default Palette Color Table', palette)
    log.set_parameter('Cloud Kernel Size', kernel_size)

    ImageProcessing.validPathLengths(infiles, None,5, log)

    for IN in infiles: #.split(";"):
        try:
            if (IN  == '' ):
                log.send_error("No images ....")
                continue

            OUT = ""

            if (IN.endswith('_class.tif') or IN.endswith('_unmix.tif') or IN.endswith('_cluster.tif')):
                log.send_warning('Skip classification for file :'+IN)
                continue

            #-----------------------------------------------------------------------------------------
            #------------------------add better test to determine image type -------------------------
            #-----------------------------------------------------------------------------------------
            MTL_met=None
            MET_met=None
            XML_met=None
            MetOBJ=get_Landsat_metadata_info('')  # create empty METOBJ

            if IN.endswith('_calrefbyt.tif') or IN.endswith('_calrefflt.tif') or IN.endswith('_calrefint.tif'):
                basename=os.path.splitext(os.path.basename(IN.replace("_calrefbyt.tif","").replace("_calrefflt.tif","").replace("_calrefint.tif","")))[0]

                try:
                    MTL_met=os.path.dirname(IN)+'/'+'_'.join(basename.split('_')[-3:])+"_MTL.txt"
                    MET_met=os.path.dirname(IN)+'/'+'_'.join(basename.split('_')[-3:])+".met"
                except:
                    pass
                if IN.endswith('_calrefbyt.tif'):
                    fnorm_median_reference=[22,16,11,63,28,10]
                if IN.endswith('_calrefflt.tif'):
                    fnorm_median_reference = [0.086, 0.062, 0.043, 0.247, 0.109, 0.039]  # [22,16,11,63,28,10] byte
                if IN.endswith('_calrefint.tif'):
                    fnorm_median_reference = [0.086*65535, 0.062*65535, 0.043*65535, 0.247*65535, 0.109*65535, 0.039*65535]  # [22,16,11,63,28,10] byte
                    # fnorm_median_reference = [0.086, 0.062, 0.043, 0.247, 0.109, 0.039]
                OUT=IN.replace('_calrefbyt.tif','_class.tif').replace('_calrefflt.tif','_class.tif').replace("_calrefint.tif","_class.tif")
                if (os.path.exists(MTL_met)):
                    MetOBJ=get_Landsat_metadata_info(MTL_met)

                if (os.path.exists(MET_met)):
                    MetOBJ=get_Landsat_metadata_info(MET_met)


            if IN.endswith('_calrefx10k.tif'):
                basename=os.path.splitext(os.path.basename(IN.replace("_calrefx10k.tif","")))[0]
                OUT=IN.replace('_calrefx10k.tif','_class.tif')

                try:
                    XML_met=os.path.dirname(IN)+'/'+'_'.join(basename.split('_')[-5:])+"_metadata.xml"
                except:
                    pass
                fnorm_median_reference=[0.086*10000,0.062*10000,0.043*10000,0,0.247*10000]  # rededge is in but not used (0)

                if (os.path.exists(XML_met)):
                    MetOBJ=get_Rapideye_metadata_info(XML_met)

            if ('_pansharp_bgrn_' in IN) or ("AnalyticMS_SR" in IN):

                OUT = IN.replace('_calrefx10k.tif','_class.tif')
                if not OUT.endswith('_class.tif'):
                    OUT = OUT + "_class.tif"

                MetOBJ.sensor='SKYBOX'
                fnorm_median_reference=[0.086*1000,0.062*1000,0.043*1000,0.247*1000]
                fnorm_median_reference=[0.059371428571429,0.071200000000000,0.065542857142857,0.288600000000000]
                fnorm_median_reference=[593.714285714286000,712.000000000000000,655.428571428571000,2886.000000000000000]

                MetOBJ.clouds=''

            if ('S2A' in IN or 'S2B' in IN) and ("_MSI" in IN) and ("L1C_" in IN or "L2A_" in IN):
                OUT=IN.replace('.tif','_class.tif')
                MetOBJ.sensor='S2A_L1C'
                img_ds = gdal.Open(IN)
                DataType = img_ds.GetRasterBand(1).DataType
                DataType = gdal.GetDataTypeName(DataType)
                img_ds = None
                print("Type: " + DataType)
                if DataType == "Byte":
                    fnorm_median_reference = [22,16,11,63,28,10]
                elif DataType == "UInt16":
                    fnorm_median_reference=[0.086*10000,0.062*10000,0.043*10000,0.247*10000,0.109*10000,0.039*10000] # LandsatLike
                MetOBJ.clouds=''


            # --------------------  NO SAT INFO -------------------

            if ((len(OUT) == 0) or (MetOBJ.sensor == '')):

                #MetOBJ=get_Landsat_metadata_info('')

                if IN.endswith('_calrefbyt.tif'):
                    OUT=IN.replace('_calrefbyt.tif','_class.tif')
                    MetOBJ.sensor='oli'
                    log.send_warning('No metadata .... file processed as LANDSAT\n')
                elif IN.endswith('_calrefflt.tif'):
                    OUT=IN.replace('_calrefflt.tif','_class.tif')
                    MetOBJ.sensor='oli'
                    log.send_warning('No metadata .... file processed as LANDSAT\n')
                elif IN.endswith('_calrefint.tif'):
                    OUT = IN.replace('_calrefint.tif', '_class.tif')
                    MetOBJ.sensor = 'oli'
                    log.send_warning('No metadata .... file processed as LANDSAT\n')
                elif IN.endswith('_calrefx10k.tif'):
                    OUT=OUT.replace('_calrefx10k.tif','_class.tif')
                    MetOBJ.sensor='RE-1'
                    log.send_warning('No metadata .... file processed as RAPIDEYE\n')


            log.send_message(IN)
            log.send_message(OUT)
            if (IN == OUT)or (OUT == ""):
                log.send_error(IN+' file <b>NOT SUPPORTED </b><br>')
                continue

            if (os.path.exists(OUT) and overwrite == 'No'):
                log.send_warning(OUT+' file <b>Already Processed </b>')
                continue

            log.send_message( "-----------")
            log.send_message( "Input:"+IN)
            log.send_message( "Output:"+OUT)
            log.send_message( "Sensor:"+str(MetOBJ.sensor))
            log.send_message( "SunAzi:"+str(MetOBJ.sunazi))
            log.send_message( "Clouds % in metadata:"+str(MetOBJ.clouds))
            log.send_message( "------------")

            detect_clouds=1
            if ( (MetOBJ.clouds == -1) and (MetOBJ.sunazi == 0.) ):
                detect_clouds=0
                log.send_warning("!Cloud detection disabled, no Sun Azimuth in metadata!")

            fnorm_median_delta=[]
            if (os.path.exists(OUT)):
                os.remove(OUT)
            if (os.path.exists(OUT+'.aux.xml')):
                os.remove(OUT+'.aux.xml')

            if apply_fnorm == 'Yes':
                try:
                    log.send_message("Forest Normalization Median Ref vals ")
                    log.send_message(str(fnorm_median_reference))

                    # compute bands median value inside a Mask (evergreen forest)
                    fnorm_median = get_image_medians_from_mask(IN, MetOBJ.sensor, REFmask)#[0.086,0.062,0.043,0.247,0.109,0.039]
                    log.send_message("Forest Normalization Extracted vals ")
                    log.send_message(str(fnorm_median))

                    if (len(fnorm_median) > 1):
                        fnorm_median_delta = numpy.subtract(fnorm_median_reference,fnorm_median)
                        log.send_message(str(fnorm_median))
                        log.send_message(str(fnorm_median_delta))
                    else:
                        log.send_warning("Forest Normalization Failure: no valid pixels,  keep processing ...")


                except Exception as e:
                    log.send_warning("Forest Normalization Failure: memory error, keep processing ...")
                    print(str(e))

            res = PiNo_classifier(IN, OUT, MetOBJ.sensor, detect_clouds, MetOBJ.sunazi, fnorm_median_delta, palette, kernel_size)

            if (res == "1"):
                log.send_message("Completed.")
            elif res in ["Memory Error, file too big. Will be fix soon.", "Landsat: Input image does not have 6 bands", "RapidEye: Input image does not have 5 bands", "Sentinel: Input image does not have 6 bands"]:
                log.send_error(str(res))
            else:
                log.send_warning(str(res))
            res = None
        except Exception as e:
            log.send_error(str(e))
            res = None

    log.close()


if __name__ == "__main__":
    overwrite, apply_fnorm, palette, kernel_size, infiles = getArgs(sys.argv)
    run_PiNo_classifier(overwrite, apply_fnorm, palette, kernel_size, infiles)


