#!/bin python

# Copyright (c) 2017, European Union
# All rights reserved.
# Authors: Ceccherini Guido, Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import time
import numpy
import json

# import GDAL/OGR modules
try:
    from osgeo import gdal, osr
    from osgeo.gdalconst import GA_ReadOnly, GDT_Byte
except ImportError:
    import gdal
    import osr
    from gdalconst import GA_ReadOnly, GDT_Byte

# Import local libraries
from __config__ import *
import LogStore
import Settings
import IMPACT
import ImageProcessing


SETTINGS = Settings.load()
numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print('Author: Ceccherini Guido, Simonetti Dario, Marelli Andrea for European Commission  2017')
    print('Satellite data Principal Component Analysis ')
    print()
    sys.exit(0)


def getArgs(args):
    infiles = None

    if (len(sys.argv) < 4):
        usage()
    else:
        infiles = sys.argv[1]
        out_suffix = sys.argv[2]
        overwrite = sys.argv[3]
        return infiles, out_suffix, overwrite




def PrincCompAnalysis(infiles, out_suffix, overwrite):
    # initialize output log
    log = LogStore.LogProcessing('PCA', 'pca')
    log.set_input_file(infiles)
    log.set_parameter('Output suffix', out_suffix)


    gdal.AllRegister()

    # validate path lenth
    ImageProcessing.validPathLengths(infiles, out_suffix, 5, log)

    for infile in infiles:
        try:
            log.send_message("Processing "+infile)
            outfile = infile.replace('.tif','_'+out_suffix.replace('.tif',''))
            inDataset = gdal.Open(infile,GA_ReadOnly)
            if (os.path.exists(outfile + '.tif') and overwrite == False):
                log.send_warning(outfile + ' file <b>Already Processed </b>')
                continue
            if (os.path.exists(outfile + '.tif') and overwrite == True):
                try:
                    os.remove(outfile + '.tif')
                except Exception as e:
                    log.send_error("Failure deleting existing output file")
                continue

            dims = None
            pos = None
            gdal.AllRegister()
            #OUT = 'C:/Users/cecchgu/AppData/Local/Continuum/Anaconda2/june2001_pca.tif'
            print('------------PCA ---------------')
            #print time.asctime()
            #print 'Input %s'%infiles
            start = time.time()

            try:
                cols = inDataset.RasterXSize
                rows = inDataset.RasterYSize
                bands = inDataset.RasterCount
            except Exception as e:
                print('Error: %s  -- Could not read in file' % e)
                log.send_error("Error, Could not read in file")
                continue
                #sys.exit(1)
            if dims:
                x0,y0,cols,rows = dims
            else:
                x0 = 0
                y0= 0
            if pos is not None:
                bands = len(pos)
            else:
                pos = range(1,bands+1)
          #  data matrix
            #print(x0)
            #print(y0)
            #print(cols)
            #print(rows)
            G = numpy.zeros((rows*cols,bands))
            k = 0

            for b in pos:
                band = inDataset.GetRasterBand(b)
                tmp = band.ReadAsArray(x0,y0,cols,rows).astype(float).ravel()

                #time.sleep(3)
                G[:,k] = tmp - numpy.mean(tmp)
                k += 1
        #  covariance matrix
            C = numpy.mat(G).T*numpy.mat(G)/(cols*rows-1)
        #  diagonalize
            lams,U = numpy.linalg.eigh(C)
        #  sort
            idx = numpy.argsort(lams)[::-1]
            lams = lams[idx]
            U = U[:,idx]

            print('Eigenvalues: %s' % str(lams))
            log.send_message("Eigenvalues " + str(lams))
            #if graphics:
            #    plt.plot(range(1,bands+1),lams)
            #    plt.title(infiles)
            #    plt.ylabel('Eigenvalue')
            #    plt.xlabel('Spectral Band')
            #    plt.show()
            #    plt.close()
        #  project
            PCs = numpy.reshape(numpy.array(G*U),(rows,cols,bands))
            #  write to disk
            driver = inDataset.GetDriver()
            outDataset = driver.Create(outfile,cols,rows,bands,gdal.GDT_Float32,options = [ 'BIGTIFF=IF_SAFER', 'COMPRESS=LZW' ])
            outDataset.SetGeoTransform(inDataset.GetGeoTransform())
            outDataset.SetProjection(inDataset.GetProjectionRef())
            # outDataset.SetMetadata({'Impact_product_type': 'PCA', 'Impact_version': IMPACT.get_version()})

            metadata_default_bands = '1,2,3'
            if bands == 1:
                metadata_default_bands = '1,1,1'
            elif bands == 2:
                metadata_default_bands = '1,2,1'

            outDataset.SetMetadata({
                'Impact_product_type': 'PCA',
                'Impact_default_bands': metadata_default_bands,
                'Impact_version': IMPACT.get_version()
            })

            #projection = inDataset.GetProjection()
            #geotransform = inDataset.GetGeoTransform()

            for k in range(bands):
                outBand = outDataset.GetRasterBand(k+1)
                outBand.WriteArray(PCs[:,:,k],0,0)
                outBand.FlushCache()
            outDataset = None
            inDataset = None
            #print 'result written to: %s'%OUT
            #print 'elapsed time: %s'%str(time.time()-start)
            if os.path.exists(outfile):
                os.rename(outfile, outfile + '.tif')
            else:
                log.send_error("Failure in saving out file")

            print('PCA written to: ' + outfile)
            print('elapsed time: ' + str(time.time() - start))
            print('--done------------------------')

        except Exception as e:
            log.send_error("Error " + str(e))





    log.send_message("Done")
    log.close()
     
if __name__ == '__main__':

    infiles, out_suffix, overwrite = getArgs(sys.argv)

    if (overwrite in ["True","true","Yes","Y","y",'1']):
        overwrite = True
    else:
        overwrite = False
    PrincCompAnalysis(json.loads(infiles), out_suffix, overwrite)

