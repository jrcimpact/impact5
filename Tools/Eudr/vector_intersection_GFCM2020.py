#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json
import time
import shutil
#
import numpy
start = time.time()
# import GDAL/OGR modules
try:
    from osgeo import gdal, osr, ogr
except ImportError:
    import gdal
    import osr
    import ogr

from Simonets_PROC_libs import *

import LogStore
from __config__ import *
import GdalOgr


# -------------------------------------------------------------------
# ------------------------- TMP directory  ---------------------------
# -------------------------------------------------------------------
def clean_tmp_file(tmpindir):
    try:
        for root, dirs, files in os.walk(tmpindir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        return "1"
    except Exception as e:
        print(e)
        return "1"


def initReport(report_file_pt):
    report = '''
                       <html>
                       <head>
                       <title>Intersection report</title>
                       <style>
                           body{font-family: Arial, sans-serif; font-size:14px; width: 660px;}
                           h1{font-family: Arial, sans-serif; font-size:20px; font-weight:bold}
                           h2{font-family: Arial, sans-serif; font-size:18px; font-weight:bold; margin-top:2px; margin-bottom:0px}
                           h3{font-family: Arial, sans-serif; font-size:14px; font-weight:bold; margin-bottom:0; margin-top:2px; padding:0}
                           h4{font-family: Arial, sans-serif; font-size:14px; margin-bottom:0; margin-top:0px; padding:0}
                           table {border-collapse: collapse; width:100%}
                           table, td, th {border: 1px solid black; padding:3}
                       </style>

                       <script src="/libs_javascript/echarts-4.4.0/echarts.min.js"></script>

                       </head>
                       <body>
                       <div id="intro">
                           <h1> Intersection report </h1>
                       </div>
                       '''
    report += '<div><h2> User input parameters:</h2>'
    report_file_pt.write(report)
    return

def populate_report(inData, report_file_pt, warnings):
    # -------------------------------------------------------------------------------------------------
    # ------------------------------------   REPORT   -------------------------------------------------
    # -------------------------------------------------------------------------------------------------
    try:
        # -------------------------------------------------------------------------------------------------
        report = '</br><div><h2> Analysis </h2></br>'
        chart_data = ''
        pos = 0
        for l in inData[0]:

            chart_data += '{ value:'+ str(l) +', itemStyle: {color: "'+str(inData[2][pos])+'"}},'
            pos+=1

        chart_options = ('''
                              {
                                       title: {text: 'Number of polygon VS Forest %', x: 'center'},
                                       tooltip: {
                                            formatter : function (params){
                                                return  params.data.value
                                                }
                                       },
                                       grid: {
                                           left: '5%',
                                           right: '10%',
                                           bottom: '10%',
                                           containLabel: true
                                       },
                                      xAxis: {
                                                 type: 'category',
                                                 axisLabel: {rotate:45, fontSize:10},
                                                 data: '''+ str(inData[1]) +'''
                                      },
                                      yAxis: {
                                                 type: 'value',
                                                 name: 'Count (#)',
                                                 nameLocation:'middle',
                                                 //yAxisIndex: 0
                                             },
                                      series: [{
                                                 type: 'bar',
                                                 
                                                 //name: 'Count',
                                                 data: ['''+ str(chart_data)+''']
                                              }],
                                      dataZoom: [
                                            {
                                                id: 'dataZoomX',
                                                type: 'slider',
                                                xAxisIndex: [0],
                                                filterMode: 'filter',
                                                //start: 1,
                                                //end: 99
                                            },
                                            //{
                                            //    id: 'dataZoomY',
                                            //    type: 'slider',
                                            //    yAxisIndex: [0],
                                            //    filterMode: 'empty',
                                                //start: 0,
                                                //end: 99
                                            //},

                                            {
                                                type: 'inside',
                                                yAxisIndex: 0,
                                                filterMode: 'empty'
                                            }
                    
                    
                                      ],
                                      toolbox: {
                                          show: true,
                                          orient:'vertical',
                                          feature: {
                                              //magicType: {show: true, title: ['bar', 'line'], type: ['bar', 'line']},
                                              restore: {show: true, title: 'Restore'},
                                              saveAsImage: {show: true, title: "Save As Image"}
                                          }
                                      }
                              }''')

        report += '<div id="chartRainDiv" style="width: 600px; height: 500px;"></div>'
        report += "<script> var chartRain = echarts.init(document.getElementById('chartRainDiv')); chartRain.setOption(" + chart_options + ");</script>"
        report += ' </br> </br>'

        # report += '<div><h2> % Distribution </h2></br>'

        # compute %
        tot = sum(inData[0][:])
        inData[0] = [i/tot*100. for i in inData[0][:]]

        chart_data = ''
        pos = 0
        for l in inData[1]:

            chart_data += '{ name:"'+ str(l) +'",value:' +  str(inData[0][pos]) +'},'
            pos+=1


        chart_options = '''
                               {
                                   chart:{
                                       type: 'pie',
                                       },
                                   title: {
                                       text: '% Distribution',
                                       x: 'right'
                                   },
                                   grid: {
                                           top: '15%',
                                   },
                                   legend: {
                                       top: 'bottom',
                                       width:400
                                   },
                                    tooltip: {
                                       formatter : function (params){
                                         return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                       },
                                   },
                                   series: [{
                                       type:'pie',
                                       name: 'Parcel',
                                       colorByPoint: true,
                                       radius: '50%',
                                       itemStyle : {
                                             normal : {
                                                  label : {
                                                     formatter : function (params){
                                                           return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                                     },
                                                     textStyle: {
                                                       color: 'black'
                                                     }
                                                 }
                                             }
                                      },
                                      data: [ '''+chart_data  +''']
                                   }],
                                   color: ''' + str(inData[2]) + ''',
                                  toolbox: {
                                      show: true,
                                      orient:'vertical',
                                      feature: {
                                          saveAsImage: {show: true, title: "Save As Image"}
                                      }
                                  }
                               }'''

        report += '<div id="pieLCDiv" style="width: 600px; height: 500px;"></div>'
        report += "<script> var pieLC = echarts.init(document.getElementById('pieLCDiv')); pieLC.setOption(" + chart_options + ");</script>"
        report += '</br></br>\n'


        report_file_pt.write(report)

        report_file_pt.write(warnings)



        return True

    except Exception as e:
        print(e)
        return False




def getVectorBBOX(aoi):
    # ------------------  preparing BBOX in LL projection if needed  ------------------------
    # does not consider the SetAxisMappingStrategy  since it is a custom function for JRC map download

    shp_info = GdalOgr.get_info_from_shapefile(aoi)
    extent = json.loads(shp_info.get('extent'))
    PROJ4 = shp_info.get('PROJ4')
    bbox = ''
    print("SHP PROJ4")
    print(PROJ4)
    if PROJ4 != '+proj=longlat +datum=WGS84 +no_defs':
        print('reprojecting ...')
        source = osr.SpatialReference()
        source.ImportFromProj4(PROJ4)

        target = osr.SpatialReference()
        target.ImportFromEPSG(4326)

        transform = osr.CoordinateTransformation(source, target)
        shpExtent = ogr.CreateGeometryFromWkt(
            "POLYGON ((" + str.format('{0:.10f}', extent.get('W')) + ' ' + str.format('{0:.10f}',
                                                                                      extent.get('N')) + ',' + \
            str.format('{0:.10f}', extent.get('E')) + ' ' + str.format('{0:.10f}', extent.get('N')) + ',' + \
            str.format('{0:.10f}', extent.get('E')) + ' ' + str.format('{0:.10f}', extent.get('S')) + ',' + \
            str.format('{0:.10f}', extent.get('W')) + ' ' + str.format('{0:.10f}', extent.get('S')) + ',' + \
            str.format('{0:.10f}', extent.get('W')) + ' ' + str.format('{0:.10f}', extent.get('N')) + "))")
        shpExtent.Transform(transform)
        extent = shpExtent.GetEnvelope()
        bbox = str(extent[0] - 0.01) + ',' + str(extent[2] - 0.01) + ',' + str(extent[1] + 0.01) + ',' + str(extent[3] + 0.01)


        print('computing LL bbox')
        print(bbox)

    else:
        bbox = str(extent.get('S') - 0.01) + ',' + str(extent.get('W') - 0.01) + ',' + str(
            extent.get('N') + 0.01) + ',' + str(extent.get('E') + 0.01)

    log.send_message('BBOX: ' + bbox)
    return bbox


def getStatsVertex(inSHP, inMap, forestVals, out_name, report_file_pt ):

    try:

        warnings = ''
        pointsCT = 0

        forestVals = [int(v) for v in forestVals ]

        shp_data_source = ogr.Open(inSHP)


        data_source = gdal.Open(inMap)
        geo_transform = data_source.GetGeoTransform()
        raster_srs = osr.SpatialReference()
        raster_srs.ImportFromWkt(data_source.GetProjectionRef())
        raster_proj4 = raster_srs.ExportToProj4()

        # num_bands = data_source.RasterCount
        num_col = data_source.RasterXSize
        num_line = data_source.RasterYSize
        raster_band = data_source.GetRasterBand(1)
        record = []

        plotData = [[],[],[]]
        plotData[0] = [0,0]
        plotData[1] = ['0','100']
        # plotData[2] = ['#3BFF0F','#C04AB9']
        plotData[2] = ['#0d0887','#f0f921' ]
        # '#f0f921','#fdca26','#fb9f3a','#ed7953','#d8576b','#bd3786','#9c179e','#7201a8','#46039f','#0d0887'


        layer = shp_data_source.GetLayer()
        shp_srs = layer.GetSpatialRef()
        shp_proj4 = shp_srs.ExportToProj4()

        shp_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        raster_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
       # if raster_srs.IsSame(shp_srs) == 0:
        if shp_proj4 != raster_proj4:

            transform = osr.CoordinateTransformation(shp_srs, raster_srs)
            print("-----------------------")
            print('Reprojecting ....')
            print(shp_proj4)
            print(raster_proj4)
            print("-----------------------")


        else:
            transform = None



        ID = 0
        for feature in layer:
            # Get the geometry of the feature
            points = []
            geometry = feature.GetGeometryRef()
            if geometry.GetGeometryType() == ogr.wkbPoint:
                # If the geometry is a point, get the coordinates directly
                x = geometry.GetX()
                y = geometry.GetY()
                points.append([x, y])
            elif geometry.GetGeometryType() == ogr.wkbLineString:
                # If the geometry is a line, get all points
                for i in range(geometry.GetPointCount()):
                    x, y, z = geometry.GetPoint(i)
                    points.append([x, y])
            elif geometry.GetGeometryType() == ogr.wkbPolygon:
                ring = geometry.GetGeometryRef(0)
                for i in range(ring.GetPointCount()):
                    x, y, z = ring.GetPoint(i)
                    points.append([x, y])
            elif geometry.GetGeometryType() == ogr.wkbMultiPolygon:
                for polygon in geometry:
                    ring = polygon.GetGeometryRef(0)  # Get the exterior ring
                    for i in range(ring.GetPointCount()):
                        x, y, z = ring.GetPoint(i)
                        points.append([x, y])
                    # Optionally, handle interior rings (holes) if needed
                    for j in range(1, polygon.GetGeometryCount()):
                        interior_ring = polygon.GetGeometryRef(j)
                        for k in range(interior_ring.GetPointCount()):
                            x, y, z = interior_ring.GetPoint(k)
                            points.append([x, y])


            pointsCT +=1
            for point in points:
                x, y = point[0], point[1]
                if transform:
                    x, y , z= transform.TransformPoint(x, y, 0)

                # Convert raster coordinates to pixel/line
                col = int((x - geo_transform[0]) / geo_transform[1])
                row = int((y - geo_transform[3]) / geo_transform[5])
                # Get raster value
                if 0 <= col < num_col and 0 <= row < num_line:
                    pixel_value = raster_band.ReadAsArray(col, row, 1, 1)[0, 0]
                    if int(pixel_value) in forestVals:
                        res = 100
                        plotData[0][1] += 1
                    else:
                        res = 0
                        plotData[0][0] +=1

                else:
                    warnings += "</br> Vertex with ID=" + str(ID) + " out of map"
                    res = -1


            #-------------   write the json with the original GEOM + attributes -----
                #print('After:', point[0], point[1])
                poly = {
                    "type": "Point",
                    "coordinates": [point[0], point[1]]

                }

                feat = {
                    "type": "Feature", "properties": {str(SETTINGS['vector_editing']['attributes']['ID']): ID, "Value": res, str(SETTINGS['vector_editing']['attributes']['class_t1']):res}, "geometry": poly
                }

                record.append(feat)
                ID+=1


        geometries = {
            "type": "FeatureCollection",
            "features": record
        }

        if inSHP.endswith('json'):
            with open(inSHP, 'r') as f:
                geojson_data = json.load(f)
                try:
                    crs_data = geojson_data["crs"]
                    geometries["crs"]=crs_data
                except Exception as e:
                    pass

        # crs_data = {
        #     "type": "name",
        #     "properties": {
        #         "name": shp_proj4  # Or use WKT, EPSG, etc.
        #     }
        # }

        geo_str = json.dumps(geometries)

        f = open(out_name, "w")
        f.write(geo_str)
        f.close()


        populate_report(plotData, report_file_pt, warnings)

        # print(geo_str)
        print('done')
        print(pointsCT)

        data_source = None
        shp_data_source = None
        end = time.time() - start
        print('Time :', end)


        return True
    except Exception as e:
        log.send_error(str(e))
        return False

def getStatsOGRpoly(inSHP, attribute, inMap, forestVals, statsType, report_file_pt ):

    try:
        warnings = '</br></br><div><h2 style="color:red;">WARNINGS</h2>'
        attribute = '' #  out file will now retain all values
        print(inSHP)
        mem_drv = ogr.GetDriverByName('Memory')
        driver = gdal.GetDriverByName('MEM')

        shp_data_source = ogr.Open(inSHP,1)
        layer = shp_data_source.GetLayer()
        layerDefinition = layer.GetLayerDefn()

        shp_srs = layer.GetSpatialRef()
        shp_proj4 = shp_srs.ExportToProj4()
        print(shp_proj4)




        existing_fields = []
        for i in range(layerDefinition.GetFieldCount()):
            existing_fields.append(layerDefinition.GetFieldDefn(i).GetName())

        MYID = str(SETTINGS['vector_editing']['attributes']['ID'])
        if attribute == '' and MYID not in existing_fields:
                f1 = ogr.FieldDefn(str(MYID), ogr.OFTString)
                layer.CreateField(f1)

        VAL = 'EUFOValue'
        if VAL not in existing_fields:
                f1 = ogr.FieldDefn(str(VAL), ogr.OFTReal)
                layer.CreateField(f1)

        CLASS = str(SETTINGS['vector_editing']['attributes']['class_t1'])
        if CLASS not in existing_fields:
                f1 = ogr.FieldDefn(str(CLASS), ogr.OFTInteger)
                layer.CreateField(f1)



        #
        #
        # basic information

        data_source = gdal.Open(inMap)
        imgGeo = data_source.GetGeoTransform()
        raster_srs = osr.SpatialReference()
        raster_srs.ImportFromWkt(data_source.GetProjectionRef())
        raster_proj4 = raster_srs.ExportToProj4()

        cols = data_source.RasterXSize
        rows = data_source.RasterYSize
        pixelWidth = imgGeo[1]
        pixelHeight = imgGeo[5]

        class_band = data_source.GetRasterBand(1)

        shp_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        raster_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

        transform = None

        if shp_proj4 != raster_proj4:
            transform = osr.CoordinateTransformation(shp_srs, raster_srs)
            print('Reproject geometry to match raster proj')
            print(shp_proj4)
            print(raster_proj4)




        record = []
        plotData = [[],[],[]]
        plotData[0] = [0,0,0,0,0,0,0,0,0,0,0]
        plotData[1] = ['0','1-10','11-20','21-30','31-40','41-50','51-60','61-70','71-80','81-90','91-100']
        # plotData[2] = ['#3BFF0F', '#3BFF0F', '#4DE526', '#60CC3E', '#72B355', '#859A6D', '#938680', '#A27293','#B15EA6', '#C04AB9', '#C04AB9']
        plotData[2] = [ '#0c0240','#0d0887', '#46039f','#7201a8','#9c179e','#bd3786','#d8576b','#ed7953','#fb9f3a','#fdca26','#f0f921']


        # extent = layer.GetExtent()



        ID = 0
        for feature in layer:
            #print(ID)
            # Get the geometry of the feature
            # geometry = feature.GetGeometryRef()
            feat_geom = feature.GetGeometryRef().Clone()
            if transform:
                feat_geom.Transform(transform)
                #y_min, y_max, x_min, x_max = feat_geom.GetEnvelope()

            #else:
                #x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()
            x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()
            # print "Repositioned "
            MinX = int(round((x_min - imgGeo[0]) / pixelWidth))
            MaxX = int(round((x_max - imgGeo[0]) / pixelWidth))

            MinY = int(round((y_max - imgGeo[3]) / pixelHeight))
            MaxY = int(round((y_min - imgGeo[3]) / pixelHeight))

            deltaX = MaxX - MinX
            deltaY = MaxY - MinY
            if deltaX == 0:
                deltaX = 1
            if deltaY == 0:
                deltaY = 1
            src_offset = [MinX, MinY, deltaX, deltaY]

            try:

                if min(src_offset) < 0 or src_offset[0] > cols or src_offset[1] > rows:
                    print('Out')
                    outVal = -1
                    warnings += "</br> feature with ID="+str(ID)+" out of map"
                else:
                    # calculate new geotransform of the feature subset
                    new_gt = (
                        (imgGeo[0] + (src_offset[0] * pixelWidth)),
                        pixelWidth,
                        0.0,
                        (imgGeo[3] + (src_offset[1] * pixelHeight)),
                        0.0,
                        pixelHeight
                    )
                    mem_ds = mem_drv.CreateDataSource('out')
                    mem_layer = mem_ds.CreateLayer('poly', raster_srs, geom_type=ogr.wkbPolygon)
                    newfeature = ogr.Feature(mem_layer.GetLayerDefn())
                    newfeature.SetGeometry(feat_geom)
                    mem_layer.CreateFeature(newfeature)

                    # Rasterize it
                    rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
                    rvds.SetGeoTransform(new_gt)
                    new_gt = None

                    try:
                        gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])
                    except:
                        continue

                    newfeature = None
                    mem_layer = None
                    mem_ds = None
                    rv_array = rvds.ReadAsArray()
                    # filled = False
                    if rv_array.max() == 0:
                        rv_array.fill(1)

                    rvds = None

                    src_array = class_band.ReadAsArray(src_offset[0], src_offset[1], src_offset[2], src_offset[3])

                    totalPixel = numpy.count_nonzero(rv_array)
                    mask = rv_array*(src_array == int(forestVals[0]))

                    if len(forestVals) > 1:
                        for fv in forestVals[1:]:
                            mask += rv_array*(src_array == int(fv))
                    countvalid = numpy.count_nonzero(mask)
                    percent = float(float(countvalid / float(totalPixel)) * 100.)

                    outVal = percent

                    # if ID == 964:
                    #     print(rv_array)
                    #     print(mask)
                    #     print(countvalid)
                    #     print(totalPixel)
                    #     print(percent)

                    # fill the array of category
                    # In case of count it will be 100
                    # split into ranges
                    plotData[0][int(numpy.ceil(outVal / 10.))] += 1

                    if statsType == 'count':
                        outVal = countvalid





                # if attribute == '':
                #     feature.SetField(MYID, ID )  # Set the value for the new attribute
                feature.SetField(MYID, ID)
                feature.SetField(CLASS, round(outVal))  # Set the value for the new attribute
                feature.SetField(VAL, outVal)  # Set the value for the new attribute

                layer.SetFeature(feature)

                feature.Destroy()

                ID += 1



            except Exception as e:
                print(e)
                print('ID:', ID+1)
                #print('Error')
                pass


        populate_report(plotData, report_file_pt, warnings)



        # print(geo_str)
        print('done')


        data_source = None
        class_band = None
        shp_data_source = None
        end = time.time() - start
        print('Time :', end)

        return True
    except Exception as e:
        print(e)
        log.send_error(str(e))
        return False


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2025')
    print()
    print('Vector intersection with GFC 2020 supporting EUDR on deforestation')
    print()
    sys.exit(1)


def getArgs(args):
    indir = None
    if (len(sys.argv) < 10):
        usage()
    else:
        try:
            aoi = json.loads(sys.argv[1])
        except:
            aoi = sys.argv[1]

        try:
            attribute = json.loads(sys.argv[2])
        except:
            attribute = sys.argv[2]

        try:
            fMap = json.loads(sys.argv[3])
        except:
            fMap = sys.argv[3]

        if aoi != '' and type(aoi) == type([]):
            aoi = aoi[0]
        if fMap != '' and type(fMap) == type([]):
            fMap = fMap[0]

        try:
            LC_Ids = json.loads(sys.argv[4])
        except:
            LC_Ids = sys.argv[4]

        proc_mode = sys.argv[5]
        proc_type = sys.argv[6]
        save_tmp = sys.argv[7]

        out_name = sys.argv[8]

        overwrite = sys.argv[9]


        return aoi, attribute, fMap, LC_Ids, proc_mode, proc_type, save_tmp, out_name, overwrite



if __name__ == "__main__":

    aoi, attribute, forestMap, forestVals, proc_mode, proc_type, save_tmp, suffix, overwrite = getArgs(sys.argv)

    log = LogStore.LogProcessing('EUFO Intersection ', 'EUFOintersect')
    log.set_input_file(aoi)
    log.set_input_file(forestMap)


    log.set_parameter('Class IDs', ' '.join(map(str,forestVals)))
    log.set_parameter('Attribute ID', attribute)

    log.set_parameter('Out suffix', suffix)
    log.set_parameter('Overwrite', overwrite)

    log.set_parameter('Mode', proc_mode)
    log.set_parameter('Type', proc_type)
    log.set_parameter('Save GFC map', save_tmp)

    if overwrite in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        overwrite = False
    else:
        overwrite = True

    if save_tmp in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        save_tmp = False
    else:
        save_tmp = True




    # ------------------  preparing tmp folder  ---------------------------------------------
    tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "Eudr")

    if not os.path.exists(tmp_indir):
        os.makedirs(tmp_indir)
    else:
        clean_tmp_file(tmp_indir)

    try:
        download_GFCM = False
        if forestMap == '':
            download_GFCM = True
            if forestVals != ['1']:
                log.send_warning("Provided forest IDs are discarded when using GFC 2020 from JRC")
            forestVals = [1]
        else:
            # do not save user Map again
            save_tmp = False

        format = 'GeoJSON'
        if aoi.endswith('.geojson'):
            out_name = aoi.replace('.geojson', '_' + suffix + '.geojson')
            out_report_name = aoi.replace('.geojson', '_' + suffix + '.html')
            out_name_csv = aoi.replace('.geojson', '_' + suffix + '.csv')
        if aoi.endswith('.json'):
            out_name = aoi.replace('.json', '_' + suffix + '.geojson')
            out_report_name = aoi.replace('.json', '_' + suffix + '.html')
            out_name_csv = aoi.replace('.json', '_' + suffix + '.csv')
        if aoi.endswith('.shp'):
            format = 'ESRI Shapefile'
            out_name = aoi.replace('.shp', '_' + suffix + '.shp')
            out_report_name = aoi.replace('.shp', '_' + suffix + '.html')
            out_name_csv = aoi.replace('.shp', '_' + suffix + '.csv')




        out_tmp_name = os.path.join(tmp_indir, os.path.basename(out_name))



        report_file_pt = open(out_report_name, "w")
        initReport(report_file_pt)
        txt = 'Vector file: ' + str(aoi) + '</br>'
        # if map is taken from tmp folder means it has been downloaded from JRC
        if download_GFCM:
            txt += 'Forest map: GFC2020 map from JRC </br>'
        else:
            txt += 'Forest map: ' + str(forestMap) + '</br>'
        txt += '</div>'
        report_file_pt.write(txt)
        txt=''



        if os.path.exists(out_name):
            if overwrite:
                shutil.rmtree(out_name, ignore_errors=True)
                shutil.rmtree(out_name_csv, ignore_errors=True)
                shutil.rmtree(out_report_name, ignore_errors=True)
                if out_name.endswith('.shp'):
                    shutil.rmtree(out_name.replace('.shp','.dbf'), ignore_errors=True)
                    shutil.rmtree(out_name.replace('.shp','.shx'), ignore_errors=True)
                    shutil.rmtree(out_name.replace('.shp','.shp'), ignore_errors=True)
                    shutil.rmtree(out_name.replace('.shp','.prj'), ignore_errors=True)

            else:
                txt +='</br></br><div><h2 style="color:red;">WARNINGS</h2>'
                txt +="</br>Files already processed, change out name or enable Overwrite option</div> "
                log.send_warning("Files already processed, change out name or enable Overwrite option ")
                sys.exit()

        if download_GFCM:
            bbox = getVectorBBOX(aoi)
            if bbox == '':
                log.send_error("Error computing AOI extent")
                txt += '</br></br><div><h2 style="color:red;">WARNINGS</h2>'
                txt += "</br>Error computing AOI extent.</div> "
                sys.exit()
        else:
            if not test_overlapping(aoi, forestMap):
                txt += '</br></br><div><h2 style="color:red;">WARNINGS</h2>'
                txt += "</br>No overlap between AOI and Forest Map</div> "
                log.send_error("No overlap between AOI and Forest Map")
                sys.exit()

        if download_GFCM:
            import Download_GFCM_from_JRC

            outmap_tmp_name = os.path.join(tmp_indir, 'GFCM_2020_'+suffix+'.tif')
            outmap_name = os.path.join(os.path.dirname(aoi), 'GFCM_2020_'+suffix+'.tif')
            inMap = Download_GFCM_from_JRC.run_downloader(bbox, outmap_tmp_name)
            inMap = inMap[0]
        else:
            inMap = forestMap


        if not os.path.exists(inMap):
            txt += '</br></br><div><h2 style="color:red;">WARNINGS</h2>'
            txt += "</br>Error downloading GFC map or corrupted file(s)."
            log.send_error("Error downloading GFC map or corrupted file</div>")
            sys.exit()






        if proc_mode == 'geometry':
           print('geometry')
           # copy in file into tmp folder and edit the content
           srcDS = gdal.OpenEx(aoi)
           ds = gdal.VectorTranslate(out_tmp_name, srcDS, format=format)
           srcDS = None
           ds = None

           res = getStatsOGRpoly(out_tmp_name, attribute, inMap, forestVals, proc_type, report_file_pt)

        else:
            print('Vertex')
            res = getStatsVertex(aoi, inMap, forestVals, out_tmp_name, report_file_pt)

        if res:
            #shutil.move(out_tmp_name, out_name)

            srcDS = gdal.OpenEx(out_tmp_name)
            # layer = srcDS.GetLayer()
            # shp_srs = layer.GetSpatialRef()
            # shp_proj4 = shp_srs.ExportToProj4()
            # shp_wkt = shp_srs.ExportToWkt()
            # print('PROJ ',shp_wkt)

            # 2do FIX proj assignment - not taken even if the json has a forced crs
            try:
                #VectorOption = gdal.VectorTranslateOptions(srcSRS=shp_wkt, dstSRS=shp_wkt )
                ds = gdal.VectorTranslate(out_name, srcDS, format=format)  #, options=VectorOption
            except Exception as e:
                shutil.move(out_tmp_name, out_name)
                print(e)

            ds = gdal.VectorTranslate(out_name_csv, srcDS, format="CSV")
            srcDS = None
            shp_srs = None
            layer = None
            ds = None

        if save_tmp:
            shutil.move(inMap, outmap_name)

    except Exception as e:
        log.send_error(str(e))


    finally:

        # shutil.rmtree(tmp_indir, ignore_errors=True)
        print('Done')

        log.send_message(
            '<a target="_blank" href=getHtmlFile.py?file=' + out_report_name + '><b>View report result</b></a>')

        log.close()

        print('-----------------------------------------')

        report_file_pt.write(txt)
        report_file_pt.write('</div></body></html>')

        report_file_pt.close()
        if sys.platform.startswith('win'):
            os.system("start %s" %(GLOBALS['url']+'/getHtmlFile.py?file='+out_report_name))

