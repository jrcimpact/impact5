#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json
import time
#


# import GDAL/OGR modules
try:
    from osgeo import gdal, osr, ogr
except ImportError:
    import gdal
    import osr
    import ogr



import multiprocessing


def getStatsPandas(inSHP, inMap):
    print(inSHP)
    start = time.time()
    pointsCT = 0
    VertexPositive = 0

    import geopandas as gpd
    gdf = gpd.read_file(inSHP)
    data_source = gdal.Open(inMap)
    # target_ref = osr.SpatialReference()
    # target_ref.ImportFromWkt(data_source.GetProjectionRef())
    geo_transform = data_source.GetGeoTransform()
    num_bands = data_source.RasterCount
    num_col = data_source.RasterXSize
    num_line = data_source.RasterYSize
    record = []



    for index, row in gdf.iterrows():
        for pt in list(row['geometry'].exterior.coords):
            # point = ogr.CreateGeometryFromWkt("POINT (" + str(pt[0]) + " " + str(pt[1]) + ")")
            # x, y = point.GetX(), point.GetY()
            px = (pt[0] - geo_transform[0]) / geo_transform[1]  # x pixel
            py = (pt[1] - geo_transform[3]) / geo_transform[5]  # y pixel

            if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
                print('OUT')
                continue
            px = int(px)
            py = int(py)


            # retrieve pixel value from raster band
            band_obj = data_source.GetRasterBand(1)
            pixel_value = band_obj.ReadAsArray(px, py, 1, 1)
            if pixel_value == 1:
                VertexPositive +=1

            # print pixel_value[0][0]
            # if pixel_value is None:
            #     # out of image
            #     print('None')
            #     continue
            pointsCT += 1
            #record.append(pixel_value[0][0])
            # print(record)

    # print(record)
    print('done')
    data_source = None
    end = time.time() - start
    print(pointsCT)
    print(VertexPositive)
    print(VertexPositive/pointsCT)
    print('Time :', end)


def getStatsOGR(inSHP, inMap, saveJson):
    print(inSHP)
    pointsCT = 0
    start = time.time()

    shp_data_source = ogr.Open(inSHP)
    data_source = gdal.Open(inMap)
    geo_transform = data_source.GetGeoTransform()
    num_bands = data_source.RasterCount
    num_col = data_source.RasterXSize
    num_line = data_source.RasterYSize
    record = []

    layer = shp_data_source.GetLayer()
    for feature in layer:
        # Get the geometry of the feature
        geometry = feature.GetGeometryRef()
        ring = geometry.GetGeometryRef(0)
        points = ring.GetPointCount()
        for p in range(0, points):
            pt = ring.GetPoint(p)
            pointsCT +=1
            px = (pt[0] - geo_transform[0]) / geo_transform[1]  # x pixel
            py = (pt[1] - geo_transform[3]) / geo_transform[5]  # y pixel

            if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
                print('OUT')
                continue
            px = int(px)
            py = int(py)


            # retrieve pixel value from raster band
            band_obj = data_source.GetRasterBand(1)
            pixel_value = band_obj.ReadAsArray(px, py, 1, 1)

            if saveJson:
                poly = {
                    "type": "Point",
                    "coordinates": [pt[0],pt[1]]
                }
                feat = {
                    "type": "Feature", "properties": {"value": int(pixel_value[0][0])}, "geometry": poly
                }

                record.append(feat)

    if saveJson:
        geometries = {
            "type": "FeatureCollection",
            "features": record,
        }

        geo_str = json.dumps(geometries)

        f = open(inSHP.replace('.geojson','_stats.geojson'), "w")
        f.write(geo_str)
        f.close()

    # print(geo_str)
    print('done')
    print(pointsCT)

    data_source = None
    shp_data_source = None
    end = time.time() - start
    print('Time :', end)


def getVertexIntersection(wkt, inMap, geo_transform):
    # print('IN')
    data_source = gdal.Open(inMap)
    band_obj = data_source.GetRasterBand(1)
    geometry = ogr.CreateGeometryFromWkt(wkt)
    points =[]

    if geometry.GetGeometryType() == ogr.wkbPoint:
        # If the geometry is a point, get the coordinates directly
        x = geometry.GetX()
        y = geometry.GetY()
        points.append([x, y])

    elif geometry.GetGeometryType() == ogr.wkbPolygon:
        ring = geometry.GetGeometryRef(0)
        for i in range(ring.GetPointCount()):
            x, y, z = ring.GetPoint(i)
            points.append([x, y])
    elif geometry.GetGeometryType() == ogr.wkbMultiPolygon:
        for polygon in geometry:
            ring = polygon.GetGeometryRef(0)  # Get the exterior ring
            for i in range(ring.GetPointCount()):
                x, y, z = ring.GetPoint(i)
                points.append([x, y])
            # Optionally, handle interior rings (holes) if needed
            for j in range(1, polygon.GetGeometryCount()):
                interior_ring = polygon.GetGeometryRef(j)
                for k in range(interior_ring.GetPointCount()):
                    x, y, z = interior_ring.GetPoint(k)
                    points.append([x, y])

    VertexPositive = 0
    # print(points)
    for pt in points:
        # pointsCT += 1

        px = (pt[0] - geo_transform[0]) / geo_transform[1]  # x pixel
        py = (pt[1] - geo_transform[3]) / geo_transform[5]  # y pixel

        res = 0
        # if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
        #     warnings += "</br> Vertex with ID=" + str(ID) + " out of map"
        #     res = -1
        #     print("Vertex in feature ID=" + str(ID) + " out of map")

        # else:
        #     px = int(px)
        #     py = int(py)

            # retrieve pixel value from raster band

        pixel_value = band_obj.ReadAsArray(int(px), int(py), 1, 1)
        if int(pixel_value[0][0]) == 1:
            VertexPositive += 1

    feature = None
    geometry = None
    data_source = None
    band_obj = None
    return VertexPositive


def getStatsVertexPP(inSHP, inMap, geo_transform):
    import multiprocessing
    print('VertexPP')
    try:
        start = time.time()
        warnings = ''
        pointsCT = 0
        VertexPositive = 0


        shp_data_source = ogr.Open(inSHP)

        # data_source = gdal.Open(inMap)
        # band_obj = data_source.GetRasterBand(1)
        # geo_transform = data_source.GetGeoTransform()
        # num_bands = data_source.RasterCount
        # num_col = data_source.RasterXSize
        # num_line = data_source.RasterYSize
        record = []


        layer = shp_data_source.GetLayer()
        print('Count: ',layer.GetFeatureCount())

        ID = 0
        result = []
        # [feature], [band_obj], [geo_transform]
        pool = multiprocessing.Pool()

        # geometries = [feature.GetGeometryRef().ExportToWkt() for feature in layer]
        # results = pool.map(getVertexIntersection, geometries)
        # result = [pool.apply_async(func=getVertexIntersection, args=(feature,0,0)) for feature in layer]
        for feature in layer:
            # print(feature.GetGeometryRef())
            # print(band_obj)
            # print(geo_transform)
            # print(feature.GetGeometryRef().ExportToWkt())

            result.append(pool.apply_async(func=getVertexIntersection, args=(feature.GetGeometryRef().ExportToWkt(),inMap,geo_transform)))


        pool.close()
        pool.join()
        # print(result)
        stats = 0
        for res in result:
            stats += res.get()

        print(stats)



        data_source = None
        shp_data_source = None
        end = time.time() - start
        print('Time :', end)


        return True
    except Exception as e:
        print(e)
        return False


def getStatsVertex(inSHP, inMap):

    try:
        start = time.time()
        warnings = ''
        pointsCT = 0
        VertexPositive = 0

        # forestVals = [int(v) for v in forestVals ]

        shp_data_source = ogr.Open(inSHP)
        data_source = gdal.Open(inMap)
        geo_transform = data_source.GetGeoTransform()
        num_bands = data_source.RasterCount
        num_col = data_source.RasterXSize
        num_line = data_source.RasterYSize
        record = []


        layer = shp_data_source.GetLayer()
        ID = 0
        for feature in layer:
            # Get the geometry of the feature
            points = []
            geometry = feature.GetGeometryRef()
            if geometry.GetGeometryType() == ogr.wkbPoint:
                # If the geometry is a point, get the coordinates directly
                x = geometry.GetX()
                y = geometry.GetY()
                points.append([x,y])

            elif geometry.GetGeometryType() == ogr.wkbPolygon:
                ring = geometry.GetGeometryRef(0)
                for i in range(ring.GetPointCount()):
                    x, y, z = ring.GetPoint(i)
                    points.append([x,y])
            elif geometry.GetGeometryType() == ogr.wkbMultiPolygon:
                for polygon in geometry:
                    ring = polygon.GetGeometryRef(0)  # Get the exterior ring
                    for i in range(ring.GetPointCount()):
                        x, y, z = ring.GetPoint(i)
                        points.append([x,y])
                    # Optionally, handle interior rings (holes) if needed
                    for j in range(1, polygon.GetGeometryCount()):
                        interior_ring = polygon.GetGeometryRef(j)
                        for k in range(interior_ring.GetPointCount()):
                            x, y, z = interior_ring.GetPoint(k)
                            points.append([x,y])

            for pt in points:
                pointsCT +=1
                px = (pt[0] - geo_transform[0]) / geo_transform[1]  # x pixel
                py = (pt[1] - geo_transform[3]) / geo_transform[5]  # y pixel


                res = 0
                if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
                    warnings += "</br> Vertex with ID=" + str(ID) + " out of map"
                    res = -1
                    print("Vertex in feature ID=" + str(ID) + " out of map")

                else:
                    px = int(px)
                    py = int(py)


                    # retrieve pixel value from raster band
                    band_obj = data_source.GetRasterBand(1)
                    pixel_value = band_obj.ReadAsArray(px, py, 1, 1)

                    if int(pixel_value[0][0]) == 1:
                        VertexPositive +=1
                    # if int(pixel_value[0][0]) in forestVals:
                    #     res = 100
                    #     plotData[0][1] += 1
                    #
                    # else:
                    #     plotData[0][0] +=1


                    # poly = {
                    #     "type": "Point",
                    #     "coordinates": [pt[0],pt[1]]
                    # }
                    # feat = {
                    #     "type": "Feature", "properties": {str(SETTINGS['vector_editing']['attributes']['ID']): ID, "Value": res, str(SETTINGS['vector_editing']['attributes']['class_t1']):res}, "geometry": poly
                    # }
                    #
                    # record.append(feat)
                    # ID+=1


        # geometries = {
        #     "type": "FeatureCollection",
        #     "features": record,
        # }
        #
        # geo_str = json.dumps(geometries)
        #
        # f = open(out_name, "w")
        # f.write(geo_str)
        # f.close()




        # print(geo_str)
        print('done')
        print(pointsCT)
        print(VertexPositive)
        print(VertexPositive/pointsCT)

        data_source = None
        shp_data_source = None
        end = time.time() - start
        print('Time :', end)


        return True
    except Exception as e:
        print(e)
        return False






def getStatsOGRpoly(inSHP, inMap, saveJson):
    start = time.time()
    import numpy
    print(inSHP)
    pointsCT = 0

    TOTpix = 0
    TOTvalid = 0

    mem_drv = ogr.GetDriverByName('Memory')
    driver = gdal.GetDriverByName('MEM')

    shp_data_source = ogr.Open(inSHP)
    layer = shp_data_source.GetLayer(0)
    # basic information
    extent = layer.GetExtent()
    srs = layer.GetSpatialRef()


    # axis_mapping_strategy = srs.GetAxisMappingStrategy()
    # print("Axis Mapping Strategy:", axis_mapping_strategy)
    #
    # axes_count = srs.GetAxesCount()
    # for i in range(axes_count):
    #     axis_name = srs.GetAxisName('GEOGCS',i)
    #     print(f"Axis {i + 1}: {axis_name}")
    #
    #


    data_source = gdal.Open(inMap)
    imgGeo = data_source.GetGeoTransform()
    sourceSR = osr.SpatialReference()
    sourceSR.ImportFromWkt(data_source.GetProjectionRef())
    cols = data_source.RasterXSize
    rows = data_source.RasterYSize
    pixelWidth = imgGeo[1]
    pixelHeight = imgGeo[5]

    class_band = data_source.GetRasterBand(1)
    record = []
    layer = shp_data_source.GetLayer()


    for feature in layer:

        # Get the geometry of the feature
        # geometry = feature.GetGeometryRef()
        feat_geom = feature.GetGeometryRef().Clone()

        # print(feature.properties)
        geomType = feat_geom.GetGeometryName()


        x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()

        # print "Repositioned "
        MinX = int(round((x_min - imgGeo[0]) / pixelWidth))
        MaxX = int(round((x_max - imgGeo[0]) / pixelWidth))

        MinY = int(round((y_max - imgGeo[3]) / pixelHeight))
        MaxY = int(round((y_min - imgGeo[3]) / pixelHeight))

        deltaX = MaxX - MinX
        deltaY = MaxY - MinY
        if deltaX == 0:
            deltaX = 1
        if deltaY == 0:
            deltaY = 1
        src_offset = [MinX, MinY, deltaX, deltaY]

        try:

            # to be removed on production
            if min(src_offset) < 0 or src_offset[0] > cols or src_offset[1] > rows:
                print('Out')
                continue
            # calculate new geotransform of the feature subset
            new_gt = (
                (imgGeo[0] + (src_offset[0] * pixelWidth)),
                pixelWidth,
                0.0,
                (imgGeo[3] + (src_offset[1] * pixelHeight)),
                0.0,
                pixelHeight
            )
            mem_ds = mem_drv.CreateDataSource('out')
            mem_layer = mem_ds.CreateLayer('poly', sourceSR, geom_type=ogr.wkbPolygon)
            newfeature = ogr.Feature(mem_layer.GetLayerDefn())
            newfeature.SetGeometry(feat_geom)
            mem_layer.CreateFeature(newfeature)

            # Rasterize it
            rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
            rvds.SetGeoTransform(new_gt)
            new_gt = None

            try:
                gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])
            except:
                continue

            newfeature = None
            mem_layer = None
            mem_ds = None
            rv_array = rvds.ReadAsArray()
            # filled = False
            if rv_array.max() == 0:
                # print('Filling : ', rv_array)
                # print('Filling : ', src_offset)
                rv_array.fill(1)
                # filled = True

            rvds = None


            src_array = class_band.ReadAsArray(src_offset[0], src_offset[1], src_offset[2], src_offset[3])

            totalPixel = src_offset[2]*src_offset[3]
            TOTpix += totalPixel
            countvalid = numpy.count_nonzero((src_array == 1)*rv_array)
            TOTvalid += countvalid
            # percent = float(float(countvalid / float(totalPixel)) * 100.)


            feature.Destroy()

            #print('Percent:', percent)

            # if saveJson:
            #     #print('Saving')
            #     if geomType == "POLYGON":
            #         poly = {
            #             "type": "Polygon",
            #             "coordinates": feat_geom.ExportToWkt().replace('POLYGON ((','[[[').replace('))',']]]').replace(',','],[').replace(' ',',')
            #         }
            #     else:
            #         # to be sure POLY and POINT only
            #         # geomType == "POINT":
            #         poly = {
            #             "type": "Point",
            #             "coordinates": feat_geom.ExportToWkt().replace('POINT ((','[[[').replace('))',']]]').replace(',','],[').replace(' ',',')
            #         }
            #
            #     feat = {
            #         "type": "Feature", "properties": {"value": percent}, "geometry": poly
            #     }
            #
            #     record.append(feat)

            pointsCT +=1
        except Exception as e:
            #print(e)
            #print('Error')
            pass
        feat_geom = None

    print("Total % = ", TOTvalid/TOTpix)

    # if saveJson:
    #     geometries = {
    #         "type": "FeatureCollection",
    #         "features": record,
    #     }
    #
    #     geo_str = json.dumps(geometries).replace('"[[','[[').replace(']]"',']]')
    #
    #     f = open(inSHP.replace('.geojson','_stats5.geojson'), "w")
    #     f.write(geo_str)
    #     f.close()

    # print(geo_str)
    print('done')
    print(pointsCT)

    data_source = None
    class_band = None
    shp_data_source = None
    end = time.time() - start
    print('Time :', end)





if __name__ == "__main__":

    me, inSHP, inMap = sys.argv
    inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\parcels_0.2Mega.geojson'
    inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\parcels_30Mega.json'

    # inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\Madagascar.geojson'
    # inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\geojson_size_13Mb.json'
    #inSHP = 'E:\dev\IMPACT5\DATA\parcels_10Mega.geojson'

    #inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\parcels_10Mega.json'
    #inSHP = 'D:\IMPACT\IMPACT5\DATA\EUFO\geojson_size_13Mb.json'
    #inSHP = 'E:\dev\IMPACT5\DATA\parcels_26Mega.geojson'
    # inSHP='E:\\dev\\IMPACT5\\DATA\\uk_la.geojson'
    #inSHP = 'E:\dev\IMPACT5\DATA\Stats_test_fake.shp'
    #inMap='E:\dev\IMPACT5\DATA\LCFM_LCM-10_V005_2020_N39W111_MAP_V12C_C232_Fake.tif'
    inMap='D:\IMPACT\IMPACT5\DATA\EUFO\GFCM_2020_t2.tif'


    import timeit

    # print('PANDAS')
    # getStatsPandas(inSHP, inMap)
    # execution_time = timeit.timeit(getStatsPandas(inSHP, inMap), number=10)
    # print(f"Average execution time: {execution_time / 10} seconds")


    # getStatsPandas(inSHP, inMap)
    data_source = gdal.Open(inMap)

    band_obj = data_source.GetRasterBand(1)
    geo_transform = data_source.GetGeoTransform()


    saveJson = False
    # getStatsOGR(inSHP, inMap, saveJson)
    #getStatsOGRpoly(inSHP, inMap, saveJson)
    print('IMPACT SP')
    getStatsVertex(inSHP, inMap)

    print('IMPACT PP')
    getStatsVertexPP(inSHP, inMap, geo_transform )



