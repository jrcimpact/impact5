#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>


import os

import multiprocessing
from __config__ import *
from osgeo import gdal
# import Settings


def download_product(name, URL):
    import urllib.request as request
    import urllib
    from urllib.error import URLError, HTTPError


    try:

        # data = urllib.request.urlopen(JRC_URL+query_string)
        print(URL)
        res, header = urllib.request.urlretrieve(URL, filename=name) #  data=query_string
    except HTTPError as e:
        print('The server couldn\'t fulfill the request.')
        print('Error code: ', e.code)
        return ''
    except URLError as e:
        print('We failed to reach a server.')
        print('Reason: ', e.reason)
        return ''
    else:
        # everything is fine
        if os.path.exists(name):
            return name
        else:
            return ''
        print('Done')







#              array of products , BBOX in LL, date range for rain, LC yy, outprefix
def run_downloader( bbox, outmap_tmp_name):
    import os
    import urllib



    URLs = []

    master_MAP_outname = outmap_tmp_name

    JRC_URL = 'https://ies-ows.jrc.ec.europa.eu/iforce/eudr/download.py?'

    bboxArray = bbox.split(',')
    print(bboxArray)

    MaxSizeDD = 2

    tiles = []
    # bbox is provided in LL
    if (abs(float(bboxArray[3]) - float(bboxArray[1])) <= MaxSizeDD) and (abs(float(bboxArray[2]) - float(bboxArray[0])) <= MaxSizeDD):
        print('Single Tile')
        MAP_singleTile = True
        params = {
            "BBOX": bbox,
        }

        params = urllib.parse.urlencode(params)
        URL = JRC_URL + params

        #print(URL)
        URLs.append({'URL': URL, 'name': master_MAP_outname })

    # -----------------    split to TILING  request  --------------------------------
    else:

        print('Multi Tile')
        MAP_singleTile = False
        print(bboxArray)

        LLy = float(bboxArray[0])
        #LLx = float(bboxArray[1])
        ct = 0
        while LLy < float(bboxArray[2]):
            LLx = float(bboxArray[1])
            while LLx < float(bboxArray[3]):
                ct += 1

                LLy1 = LLy + MaxSizeDD
                if LLy1 > float(bboxArray[2]):
                    LLy1 = float(bboxArray[2])
                LLx1 = LLx + MaxSizeDD
                if LLx1 > float(bboxArray[3]):
                    LLx1 = float(bboxArray[3])

                params = {
                    "BBOX": str(LLy) + ',' + str(LLx) + ',' + str(LLy1) + ',' + str(LLx1),
                }


                params = urllib.parse.urlencode(params)
                URL = JRC_URL + params
                print(URL)
                URLs.append({'URL': URL, 'name': master_MAP_outname.replace('.tif','_' + str(ct) + '.tif')})
                tiles.append(master_MAP_outname.replace('.tif','_' + str(ct) + '.tif'))
                LLx +=1.9
            LLy += 1.9


    try:
        pool = multiprocessing.Pool(2)
        results = []
        for URL in URLs:
            results.append(pool.apply_async(func=download_product, args=(URL.get('name'), URL.get('URL'))))
        pool.close()
        pool.join()

        jrc_files = []

        if MAP_singleTile == False:
            # merge all tiles and return the single image
            # gdal_merge = "gdal_merge"
            # if (GLOBALS['OS'] == "unix"):
            #     gdal_merge = "gdal_merge.py"
            # tileStr = " ".join(tiles)
            # res1 = os.system(gdal_merge + ' -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES -o ' + master_MAP_outname + '_tmp.tif ' + tileStr)
            # res1 = os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES ' + master_MAP_outname + '_tmp.tif ' + master_MAP_outname)
            # os.remove(master_MAP_outname + '_tmp.tif')

            gdal.BuildVRT(master_MAP_outname+'.vrt',tiles)
            res1 = os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES ' + master_MAP_outname + '.vrt ' + master_MAP_outname)

            for f in tiles:
                os.remove(f)

        if os.path.exists(master_MAP_outname):
            jrc_files.append(master_MAP_outname )


        return jrc_files


    except Exception as e:
        print(str(e))
        return jrc_files

if __name__ == "__main__":
    run_downloader()