#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
from collections import Counter
import json
import time
import copy
import shutil

# import GDAL/OGR modules
try:
    from osgeo import gdal, osr
except ImportError:
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import ImageProcessing
import ImageStore



def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Image Mosaic ')
    print('mosaic.py outName img1.tif,img2.tif,... separate force_reproj nodata overwrite ')
    print('-separate Yes No')
    print('force_reproj Yes No')
    print('nodata 0')
    print('overwrite Yes No')
    sys.exit(1)

def getArgs(args):

    if (len(sys.argv) < 11):
        usage()
    else:

        out_name = sys.argv[1]

        try:
            # test if input file with images exists (list too long)
            if os.path.exists(sys.argv[2]):
                file_handler = open(sys.argv[2], 'r')
                img_names = json.loads(file_handler.read())
                file_handler.close()
                os.remove(sys.argv[2])
            else:
                img_names = json.loads(sys.argv[2])
        except:
            img_names = sys.argv[2].split(',')

        separate = sys.argv[3]
        force_reprojection = sys.argv[4]
        nodata = sys.argv[5]
        psize = str(sys.argv[6])
        resampling = sys.argv[7]
        overwrite = sys.argv[8]
        overviews = sys.argv[9]
        tap = sys.argv[10]

        return out_name, img_names, separate, force_reprojection, nodata, psize, resampling, overwrite, overviews, tap




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_mosaic(outname,img_names,separate,force_reprojection,INnodata,psize,resampling,overwrite,overviews, tap):


    # initialize output log
    log = LogStore.LogProcessing('Mosaic', 'mosaic')


    log.set_input_file(img_names)
    log.set_parameter('Output name',outname)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Separate',separate)
    log.set_parameter('Force reprojection',force_reprojection)
    log.set_parameter('Nodata',INnodata)
    log.set_parameter('Pizel Size',str(psize))
    log.set_parameter('Resampling',resampling)
    log.set_parameter('Align (-tap)', tap)

    log.set_parameter('Default gdal -co',' COMPRESS=LZW BIGTIFF=IF_SAFER')

    ImageProcessing.validPathLengths(img_names, None,len(outname)+5, log)

    log.send_message("Running")

    original_image_list = copy.deepcopy(img_names)
    img2del = []
    try:
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_mosaic")
        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)


        if outname.endswith('.tif'):
            outname = outname.replace('.tif','')

        nodataWarp = ''
        if INnodata != '':
            #nodata = ' -srcnodata '+str(INnodata)+' -vrtnodata '+str(INnodata)
            nodata = ' -n ' + str(INnodata) + ' -a_nodata ' + str(INnodata)
        print('----')
        print(str(psize))

        if str(psize) != '':
            psizeX = abs(float(psize.split(',')[0]))
            psizeY = abs(float(psize.split(',')[1]))

            ps = ' -ps ' + str(psizeX) + ' ' + str(psizeY)
            psize=' -tr ' + str(psizeX) + ' ' + str(psizeY)

        if tap in [True, "True","true","Yes","Y","y",'1']:
            tap = ' -tap '
        else:
            tap = ''

        if separate in [True, "True","true","Yes","Y","y",'1']:
            separate = ' -separate'
            nodata = ' -n '+str(INnodata)+' -a_nodata '+str(INnodata)
        else:
            separate = ''

            # reverse order for gdal in case of overlap and proj selection , top has priorities
            img_names = img_names[::-1]

        if overwrite in ["True","true","Yes","Y","y",'1']:
            if os.path.exists(outname+'.tif'):#xx=clean_tmp_file(outdir+'//'+out_name)
                os.remove(outname+'.tif')
                log.send_message('Deleting output file')
            if os.path.exists(outname+'.tif.aux.xml'):#xx=clean_tmp_file(outdir+'//'+out_name)
                os.remove(outname+'.tif.aux.xml')

        elif os.path.exists(outname+'.tif'):

            log.send_warning(outname+'.tif  <b>Output file exists, change name or set Overwrite. </b>')
            log.close()
            return

        projections = []
        dataType = []
        bands = []
        log.send_message('Retrieving image info')
        for img_name in img_names:
            print(img_name)
            in_ds = gdal.Open(img_name)
            try:
                Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()

            except:
                log.send_error("Projection error in file " + img_name)

            Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()
            projections.append(Proj4)
            dataType.append(in_ds.GetRasterBand(1).DataType)
            bands.append(in_ds.RasterCount)
            in_ds = None

        if not all(x==bands[0] for x in bands) and separate == '':
            log.send_error('Input files do not have the same bands')
            log.close()
            return
        if not all(x==dataType[0] for x in dataType):
            log.send_warning('Input files do not have the same DataType, promoting output to '+str(gdal.GetDataTypeName(max(dataType))))

        if not all(x==projections[0] for x in projections):
            if force_reprojection not in [True, "True","true","Yes","Y","y",'1']:
                log.send_error('Failure: images have different projections, allow on-the-fly reprojection' )
                log.close()
                sys.exit()

            data = Counter(projections)
            Selprojection = str(data.most_common(1)[0][0])  # Returns the highest occurring item
            log.send_warning('Input files do not have the same projection, promoting output to '+str(Selprojection))

            #-------------------------------------------------------------------
            # ------------------------- REPROJ IMG  ----------------------------
            #-------------------------------------------------------------------

            ct=0
            for img_name in img_names:

                in_ds = gdal.Open(img_name)
                Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()

                if Selprojection != Proj4:
                    #print "Reprojecting"
                    out_reproj_name = os.path.join(tmp_indir, os.path.basename(img_name))

                    if os.path.exists(out_reproj_name):
                        os.remove(out_reproj_name)

                    os.system('gdalwarp -of GTiff '+psize+' -r '+resampling+' -t_srs "'+str(Selprojection)+'" -multi -co BIGTIFF=YES '+img_name+' '+out_reproj_name )
                    if os.path.exists(out_reproj_name):
                        img_names[ct] = out_reproj_name
                        img2del.append(out_reproj_name)

                    else:
                        log.send_error('Failure in converting image projection in :'+img_name)
                        log.send_error('Please check if output resolution matches output projection '+str(Selprojection))
                        try:
                            for img in img2del:
                                os.remove(img)
                        except:
                            pass
                        log.close()
                        return
                ct += 1

        if psize == '':
            #-------------------------------------------------------------------
            # ----------------- RECAL RES from WARPED  IMG ---------------------
            #-------------------------------------------------------------------
            resolutionsX = []
            resolutionsY = []
            for img_name in img_names:
                try:
                    in_ds = gdal.Open(img_name)
                    resolutionsX.append(float(in_ds.GetGeoTransform()[1]))
                    resolutionsY.append(abs(float(in_ds.GetGeoTransform()[5])))

                    print(float(in_ds.GetGeoTransform()[1]))
                    print(abs(float(in_ds.GetGeoTransform()[5])))
                    in_ds = None
                except Exception as e:
                    log.send_error('Failure reading file: ' + img_name + ": not used")
                    in_ds = None
                    try:
                        for img in img2del:
                            os.remove(img)
                    except:
                        pass
                    log.close()
                    return

            print("------------------------------------------------------")
            if (not all(x==resolutionsX[0] for x in resolutionsX)):
                log.send_warning('Input files do not have the same resolutions, promoting output to {:.32f}'.format(min(resolutionsX)))


                if len(resolutionsX) < 20:
                    log.send_message('List of resolutuin in X')
                    for res in resolutionsX:
                        log.send_message('{:.32f}'.format(res))
                    log.send_message('List of resolutuin in Y')
                    for res in resolutionsY:
                        log.send_message('{:.32f}'.format(res))


            psize = ' -tr {:.32f} {:.32f}'.format(min(resolutionsX),resolutionsY[resolutionsX.index(min(resolutionsX))])
            ps = ' -ps {:.32f} {:.32f}'.format(min(resolutionsX),resolutionsY[resolutionsX.index(min(resolutionsX))])
            log.send_message('Out pixel sixe: ' +psize)

        log.send_message("Running mosaic")

        img_names_list = ''
        for img in img_names:
            img_names_list += '"'+img+'" '


        if separate != "":
            # gdal_merge handles small difference in pixel size e.g.
            # 0.00026949458523585647197820525101
            # 0.00026949458523585598408722763253
            # this is not the case when building gdalbuildvrt see below

            print("---------------------------")
            print("gdal_merge -separate")
            print(dataType)
            print(str(gdal.GetDataTypeName(max(dataType))))
            print("---------------------------")

            if os.path.exists(outname):
                os.remove(outname)

            input_file_list = os.path.join(tmp_indir, 'input_list.txt')
            f = open(input_file_list, 'w')
            filedata = f.write(img_names_list)
            f.close()
            gdal_merge = "gdal_merge"
            if (GLOBALS['OS'] == "unix"):
                gdal_merge = "gdal_merge.py"
            res1 = os.system(gdal_merge + ' -of GTiff ' + tap + ' -separate -co COMPRESS=LZW -co BIGTIFF=YES -ot '+str(gdal.GetDataTypeName(max(dataType)))+' '+str(ps)+' '+str(nodata)+' -o '+outname+' --optfile '+input_file_list)

        else:
            # gdalbuildvrt -separate takes only the first band of each image (need to create individual vrt x bands)  DS
            # NOTE ON resolutions
            # 0.00026949458523585647197820525101
            # 0.00026949458523585598408722763253
            # this is not the case when building gdalbuildvrt see below
            print("---------------------------")
            print(separate)
            print("gdal_merge")
            print("---------------------------")

            if not all(x == dataType[0] for x in dataType):
                print('Promoting output to ' + str(gdal.GetDataTypeName(max(dataType))) )
            img_names_list = ''
            for img in img_names:
                res1 = os.system('gdal_translate -of VRT -ot '+str(gdal.GetDataTypeName(max(dataType))) + ' "' + img + '" "' + img + '.vrt" ')
                img_names_list += ' ' + img + '.vrt '
                img2del.append(img + '.vrt')
                # Read in the file
                try:
                    f = open(img + '.vrt', 'r')
                    filedata = f.read()
                    f.close()
                    filedata = filedata.replace('<ColorInterp>Green</ColorInterp>', '<ColorInterp>Gray</ColorInterp>')
                    filedata = filedata.replace('<ColorInterp>Red</ColorInterp>', '<ColorInterp>Gray</ColorInterp>')
                    filedata = filedata.replace('<ColorInterp>Blue</ColorInterp>', '<ColorInterp>Gray</ColorInterp>')
                    filedata = filedata.replace('<ColorInterp>Undefined</ColorInterp>', '<ColorInterp>Gray</ColorInterp>')
                    f = open(img + '.vrt', 'w')
                    f.write(filedata)
                    f.close()
                except Exception as e:
                    print("ERROR")
                    print(str(e))
                    time.sleep(5)

            # if os.path.exists(outname+ '.vrt'):
            #     os.remove(outname+ '.vrt')
            # #res1 = os.system('gdalbuildvrt -resolution highest '+str(nodata)+' '+separate+' '+outname+'.vrt '+img_names_list)
            # res1 = os.system('gdalbuildvrt -tap '+psize+' '+str(nodata)+' '+separate+' '+outname+'.vrt '+img_names_list)
            # log.send_message('Virtual file completed, converting ...')
            # img2del.append(outname + '.vrt')

            input_file_list = os.path.join(tmp_indir, 'input_list.txt')
            f = open(input_file_list, 'w')
            filedata = f.write(img_names_list)
            f.close()

            gdal_merge = "gdal_merge"
            if (GLOBALS['OS'] == "unix"):
                gdal_merge = "gdal_merge.py"
            res1 = os.system(gdal_merge + ' -of GTiff ' + tap + ' -co COMPRESS=LZW -co BIGTIFF=YES -ot ' + str(gdal.GetDataTypeName(max(dataType))) + ' ' + str(ps) + ' ' + str(nodata) + ' -o ' + outname + ' --optfile ' + input_file_list)

        res1 = os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES ' + outname + ' ' + outname + '.tmp')
        if os.path.exists(outname + '.tmp'):


            print("Metadata setting ")
            gdal_edit = "gdal_edit"
            if (GLOBALS['OS'] == "unix"):
                gdal_edit = "gdal_edit.py"
            res1 = os.system(gdal_edit +' '+outname + '.tmp -mo "Impact_product_type=mosaic" -mo "Impact_images=long list"  -mo "Impact_version='+IMPACT.get_version()+'"')
            res1 = os.system(gdal_edit +' '+outname + '.tmp -mo "Impact_product_type=mosaic" -mo "Impact_images='+' '.join(original_image_list)+'" -mo "Impact_version='+IMPACT.get_version()+'"')

            if overviews == 'Yes':
                log.send_message('Building pyramids')
                os.system("gdaladdo " +outname+'.tmp 2 4 6 8')
            os.rename(outname+'.tmp',outname+'.tif')
            os.remove(outname)
            if os.path.exists(outname+'.tmp.aux.xml'):
                os.remove(outname+'.tmp.aux.xml')
            log.send_message('Computing statistics')
            ImageStore.update_file_statistics(outname+'.tif')
        else:
            log.send_error("Mosaic error")

        try:
            for img in img2del:
                os.remove(img)
            shutil.rmtree(tmp_indir, ignore_errors=True)

        except Exception:
            pass


    except Exception as e:
        log.send_error("Generic error: "+str(e).replace("'", ''))

    log.send_message("Completed")
    log.close()
    try:
        for img in img2del:
            os.remove(img)
        shutil.rmtree(tmp_indir, ignore_errors=True)

    except Exception:
        pass

    return


if __name__ == "__main__":

    out_name, img_names, separate, force_reproj, nodata, psize, resampling, overwrite, overviews, tap = getArgs(sys.argv)
    res = ''
    try:
        res = run_mosaic(out_name, img_names, separate, force_reproj, nodata, psize, resampling, overwrite, overviews, tap)
    except:
        print("Error "+str(res))
