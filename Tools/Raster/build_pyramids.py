#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import glob
#import cgi

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal

# Import local libraries
import LogStore
import ImageStore
import CSImageStore


def main(infile):

    # retrieve parameters from POST
    #params = cgi.FieldStorage()
    #infile = params['infile'].value
    #infile = params['infile'].value

    # initialize log
    log = LogStore.LogProcessing('Build pyramids', 'buildPyramids')
    if infile.endswith('.tif'):
        INfiles = [infile]
    else:
        INfiles = glob.glob(infile + '/*.tif')

    for INfile in INfiles:
        try:
            log.set_parameter('Input file', INfile)
            ImageStore.set_status(INfile, 'pending')
            if os.path.exists(INfile + '.aux.xml'):
                os.remove(INfile + '.aux.xml')
            log.send_message("Build pyramids ...")
            os.system('gdaladdo ' + INfile + " 2 4 8 16 32 64 128 256 512 1024 2048")
            ImageStore.update_file_statistics(INfile)
        except:
            log.send_error(INfile + '.aux.xml cannot be deleted.')
            log.send_error(INfile + ' error in building index')
            ImageStore.set_status(INfile, 'true')

    log.send_message("Complete")
    log.close()


if __name__ == '__main__':
    infile = sys.argv[1]
    main(infile)
