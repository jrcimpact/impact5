#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json
import numpy
import numpy.ma as ma

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr, gdal
except ImportError:
    import ogr, osr, gdal

# Import local libraries
import LogStore
import IMPACT
from Simonets_PROC_libs import *
import ImageProcessing


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')

def RunRasterMask(infile, inmask, classList):

    try:
        out_name = infile.replace('.tif', '_mask')

        log = LogStore.LogProcessing('Mask', 'Mask')
        log.set_input_file(infile)
        log.set_input_file(inmask)
        log.set_parameter('Classes ', classList)
        ImageProcessing.validPathLengths(out_name, None, 1, log)






        if os.path.exists(out_name + '.tif'):

                try:
                    os.remove(out_name + '.tif')

                except Exception as e:
                    log.send_error('Cannot delete output file '+str(e))
                    log.close()
                    return False



        input_ds = gdal.Open(infile, gdal.GA_ReadOnly)
        masterbands = input_ds.RasterCount
        masterDataType = gdal.GetDataTypeName(input_ds.GetRasterBand(1).DataType)
        print(masterDataType)
        # -------------  EXTRACT alpha band to remove nodata introduced by warp  --------------------------
        mask_ds = gdal.Open(inmask, gdal.GA_ReadOnly)
        maskData = mask_ds.GetRasterBand(1).ReadAsArray().astype(numpy.byte)
        MASK = mask_ds.GetRasterBand(1).ReadAsArray().astype(numpy.byte) * 0 + 1
        for val in classList:

            MASK[maskData == val] = 0
        print("Mask OK ")
        mask_ds = None

        driver = gdal.GetDriverByName("GTiff")
        dst_ds = driver.CreateCopy(out_name, input_ds,1, ['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        dst_ds.SetMetadata({'Impact_product_type': 'Mask', 'Impact_operation': "Mask",
                            'Impact_version': IMPACT.get_version()})

        for x in range(input_ds.RasterCount):
            band_ds = dst_ds.GetRasterBand(x + 1)
            band = band_ds.ReadAsArray()

            # print band.min()
            # print band.max()
            band[MASK == 0] = 0
            band_ds.WriteArray(band)
            band_ds = None

        dst_ds.FlushCache()
        dst_ds = None
        input_ds = None
        os.rename(out_name, out_name+'.tif')

        log.send_message("Completed.")
        log.close()


    except Exception as e:
        log.send_error("Degradation  error: "+str(e))

        input_ds = None
        dst_ds = None


        log.close()
        return False


if __name__ == "__main__":
    infile = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_calrefbyt.tif'
    maskfile = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'
    classList = [1,2]
    res = RunRasterMask(infile, maskfile, classList)


