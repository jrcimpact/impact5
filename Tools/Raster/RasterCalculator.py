#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import ImageProcessing
import IMPACT
import gdal_calc
import urllib
from Simonets_PROC_libs import *

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('RasterCalculator.py [inputs] output_name no_data overwrite')
    sys.exit(1)


def getArgs(args):

    if (len(sys.argv) < 8):
        usage()
    else:

        try:
            img_name_bands=json.loads(sys.argv[1])
        except:
            print("Expecting JSON array, got string")
            img_name_bands=sys.argv[1].split(',')

        out_name = sys.argv[2]
        nodata=sys.argv[3]
        expression = (sys.argv[4])#.decode('utf8')
        type = sys.argv[5]
        overwrite=sys.argv[6]
        reproject=sys.argv[7]
        print(expression)
        return out_name, img_name_bands, nodata, expression, type, overwrite, reproject




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_RasterCalculator(out_name, img_name_bands, nodata, expression, type, overwrite, reproject, ExternalLogFile=False):


    # initialize output log
    if not ExternalLogFile:
        log = LogStore.LogProcessing('RasterCalculator', 'RasterCalculator')
    else:
        log = ExternalLogFile
    infiles=[]
    for v in img_name_bands:
        infiles.append('<b>'+ v['label']+'</b>: '+ v['path']+'  <b>band</b>:'+str(v['band']))

    out_name=os.path.join(GLOBALS['data_paths']['data'], out_name)

    log.set_input_file(infiles)
    log.set_parameter('Output name',out_name+'.tif')
    log.set_parameter('Nodata',nodata)
    log.set_parameter('Expression', expression)
    log.set_parameter('Output data type', type)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Reproject', reproject)

    log.set_parameter('Default gdal parameter for GTiff ',' COMPRESS=LZW BIGTIFF=IF_SAFER')

    ImageProcessing.validPathLengths(out_name, None,len(out_name)+5, log)

    log.send_message("Running")
    try:
        log.send_message('Processing')
        # remove tmp file
        if os.path.exists(out_name):
            os.remove(out_name)

        if (overwrite in ["True", "true", "Yes", "Y", "y", '1']):
            if os.path.exists(out_name+'.tif'):
                os.remove(out_name+'.tif')
            if os.path.exists(out_name + '.tif.aux.xml'):
                os.remove(out_name + '.tif.aux.xml')
        elif os.path.exists(out_name + '.tif'):
            log.send_warning(out_name + '.tif <b> exists, change name or set Overwrite. </b>')
            log.send_message("Completed.")
            if not ExternalLogFile:
                log.close()
            return

        gdal_string = 'gdal_calc --outfile=' + out_name + '.tif'

        gdal_argv = ['myself']
        gdal_argv.append('--outfile')
        gdal_argv.append(out_name)

        gdal_string += " --type="+type
        gdal_argv.append("--type="+type)

        # -------------------------------------------------------------------
        # ------------------------- TEST  OVERLAP ---------------------------
        # -------------------------------------------------------------------
        log.send_message('Overlap Test')
        master_image = img_name_bands[0]['path']
        master_ds = gdal.Open(master_image, gdal.GA_ReadOnly)
        geoTransform = master_ds.GetGeoTransform()
        xSize = master_ds.RasterXSize
        ySize = master_ds.RasterYSize
        xRes = geoTransform[1]
        yRes = geoTransform[5]

        ulx = geoTransform[0]
        uly = geoTransform[3]
        lrx = ulx + geoTransform[1] * xSize
        lry = uly + geoTransform[5] * ySize
        outputBounds1 = (min(ulx, lrx), min(uly, lry), max(ulx, lrx), max(uly, lry))
        outputBounds = (ulx, lry, lrx, uly)
        master_ds=None


        for v in img_name_bands:
            print(v['path'])
            if not test_overlapping(master_image,v['path']):
                log.send_error("No overlap between: "+master_image+" "+v['path'])
                if not ExternalLogFile:
                    log.close()
                return
        print("Overlap OK")

        # -------------------------------------------------------------------
        # ------------------------- TEST  PROJ  -----------------------------
        # -------------------------------------------------------------------
        driver = gdal.GetDriverByName("GTiff")
        Selprojection = None
        projections = []
        dataType = []
        bands = []
        log.send_message('Retrieving image info')
        for v in img_name_bands:
            in_ds = gdal.Open(v['path'])
            try:
                Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()
            except:
                log.send_error("Projection error in file " + v['path'])
                if not ExternalLogFile:
                    log.close()
                return

            Proj4 = osr.SpatialReference(wkt=in_ds.GetProjection()).ExportToProj4()
            projections.append(Proj4)
            if not Selprojection:
                Selprojection = Proj4
            in_ds = None
        # force_reprojection = False
        if not all(x==projections[0] for x in projections):
            if reproject not in [True, "True","true","Yes","Y","y",'1']:
                log.send_error('Failure: images have different projections, allow on-the-fly reprojection' )
                if not ExternalLogFile:
                    log.close()
                return
            log.send_warning('Input files do not have the same projection, promoting output to ' + str(Selprojection))
            # force_reprojection=True

        # print(Selprojection)
        # print(outputBounds)
        # print(outputBounds1)
        # print(xRes,yRes)


        for v in img_name_bands:
            gdal_argv.append('-' + v['label'])
            gdal_argv.append(v['path']+'.vrt1')
            # if force_reprojection:
            out_ds = gdal.Warp(v['path'] + '.vrt1', v['path'], format='VRT', outputType=gdal.GDT_Float32,
                                   dstSRS=Selprojection, outputBounds=outputBounds, xRes=xRes, yRes=yRes,
                                   outputBoundsSRS=Selprojection, targetAlignedPixels=False)
            # else:
            #     out_ds = gdal.Translate(v['path'] + '.vrt', v['path'], format='VRT', outputType=gdal.GDT_Float32)

            out_ds = None

            if v['band'] == 'All':
                gdal_string += ' -' + v['label'] + ' ' + v['path'] + ' --allBands=' + v['label']
                gdal_argv.append('--allBands=' + v['label'] )
            else:
                gdal_string += ' -' + v['label'] + ' ' + v['path']+' --' + v['label'] + '_band=' + str(v['band'])
                gdal_argv.append('--' + v['label'] + '_band=' + str(int(v['band'])))



        if nodata != '':
            gdal_string += " --NoDataValue="+str(nodata)
            gdal_argv.append("--NoDataValue="+str(nodata))

        gdal_string += ' --calc=' + expression
        gdal_argv.append('--calc=' + expression)
        gdal_string += ' --co COMPRESS=LZW --co BIGTIFF=IF_SAFER'

        gdal_argv.append('--co')
        gdal_argv.append('COMPRESS=LZW')
        gdal_argv.append('--co')
        gdal_argv.append('BIGTIFF=IF_SAFER')

        gdal_argv.append('--debug')

        sys.argv = gdal_argv
        log.send_message(gdal_string)
        try:
            gdal_calc.main()
        except Exception as e:
            log.send_error("Expression error: " + str(e).replace("'", ""))


        if os.path.exists(out_name):
            gdal_edit = "gdal_edit"
            if (GLOBALS['OS'] == "unix"):
                gdal_edit = "gdal_edit.py"
            res1 = os.system(gdal_edit +' '+ out_name + ' -mo "Impact_product_type=RasterCalculator" -mo "Impact_operation=' + gdal_string + '" -mo "Impact_version=' + IMPACT.get_version() + '"')
            print(out_name+ '.tif')
            os.rename(out_name, out_name+ '.tif')
        else:
            log.send_error("Creation error: gdal_calc.py error")

    except Exception as e:
        log.send_error("Generic error: "+str(e))

    for v in img_name_bands:
        if os.path.exists(v['path'] + '.vrt1'):
            os.remove(v['path'] + '.vrt1')

    log.send_message("Completed.")
    if not ExternalLogFile:
        log.close()
    return


if __name__ == "__main__":

    out_name, img_name_bands, nodata, expression, type, overwrite, reproject  = getArgs(sys.argv)
    res=''
    try:
        res=run_RasterCalculator(out_name, img_name_bands, nodata, expression, type, overwrite, reproject )
    except:
        print("Error " + str(res))
