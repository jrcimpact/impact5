#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr






if __name__ == "__main__":
    print('Here 0')
    #input = sys.argv[1]
    input="D:\\IMPACT\\DATA\\TH_DTM_Sal.tif"
    try:
        print('Here')
        master_ds = gdal.Open(input, gdal.GA_ReadOnly)
        master_bands = master_ds.RasterCount

        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        master_sourceSR = osr.SpatialReference()
        master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
        m_ulx = master_imgGeo[0]
        m_uly = master_imgGeo[3]
        m_lrx = master_imgGeo[0] + master_cols * master_imgGeo[1]
        m_lry = master_imgGeo[3] + master_rows * master_imgGeo[5]

        TH = master_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
        DTM = master_ds.GetRasterBand(2).ReadAsArray().astype(numpy.float32)
        SAL = master_ds.GetRasterBand(3).ReadAsArray().astype(numpy.float32)

        print(TH)
        print('-----------------------------')
        print(DTM)
        print('-----------------------------')
        print(SAL)


        print([TH,DTM, SAL])



    except Exception as e:
        print('Here2')
        print(e)
