#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import glob
import numpy
import cgi
import cgitb

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal

# Import local libraries
import LogStore
from __config__ import *
import ImageProcessing
import IMPACT
import ImageStore


if __name__ == "__main__":

    cgitb.enable()
    params = cgi.FieldStorage()

    log = LogStore.LogProcessing('Raster Mask ', 'RasterMask')
    #print params
    try:
        infile = params.getvalue('infile')
        minrange = int( params.getvalue('minrange') )
        maxrange = int( params.getvalue('maxrange') )
    except Exception:
        me, infile, minrange, maxrange = sys.argv
        minrange = int(minrange)
        maxrange = int(maxrange)
    #-----------------------------------------------------
    #---------------  INIT SHAPEFILE ---------------------
    #-----------------------------------------------------

    try:
        outfile=infile.replace('.tif','_'+str(minrange)+'_'+str(maxrange)+'_mask.tif_tmp')

        log.set_parameter('Input Raster', infile)
        log.set_parameter('Output',outfile.replace("_tmp",""))
        log.set_parameter('Range Min', minrange)
        log.set_parameter('Range Max: ', maxrange)



        ImageProcessing.validPathLengths(outfile,None, 0, log)

        r = glob.glob(outfile.replace("_tmp","*"))
        for i in r:
            log.send_message("Deleting existing output file.")
            os.remove(i)
        driver = gdal.GetDriverByName("GTiff")
        src_ds = gdal.Open(infile)


        dst_ds = driver.Create( outfile,src_ds.RasterXSize,src_ds.RasterYSize,1,gdal.GDT_Byte,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        dst_ds.SetGeoTransform( src_ds.GetGeoTransform())
        dst_ds.SetProjection( src_ds.GetProjectionRef())
        dst_ds.SetMetadata({'Impact_product_type': 'mask','Impact_operation':"["+str(minrange)+','+str(maxrange)+"]",'Impact_version':IMPACT.get_version()})

        #
        # outData = driver.CreateCopy(outfile, src_ds, 0)
        #outData.GetRasterBand(1).DestroyColorTable()
        #outData.GetRasterBand(1).SetColorTable()
        # c = gdal.ColorTable()
        # ctable=[[0,(0,0,0)],[1,(200,10,50)]]
        # for cid in range(0,2):
        # 	c.SetColorEntry(cid,ctable[cid][1])
        # outData.GetRasterBand(1).SetColorTable(c)

        log.send_message("Reading file in memory....")
        bandval = src_ds.GetRasterBand(1).ReadAsArray()
        #bandval = band.ReadAsArray(0,0,band.XSize,band.YSize)#.astype(numpy.byte)  ATTENTION max is 128

        try:
            nodata=int(src_ds.GetRasterBand(1).GetNoDataValue())
            mask=numpy.logical_or(bandval==nodata,numpy.isnan(bandval))*nodata
            mask[(bandval >= minrange) * (bandval <= maxrange)]=1

            dst_ds.GetRasterBand(1).SetNoDataValue(nodata)
            log.send_message("Setting "+str(nodata)+ " as Nodata")
        except:
            mask=numpy.zeros((src_ds.RasterYSize,src_ds.RasterXSize), dtype=bool)
            mask[(bandval >= minrange) * (bandval <= maxrange)]=1
            log.send_message("Nodata not detected")



        dst_ds.GetRasterBand(1).WriteArray(mask.astype(numpy.byte))
        dst_ds.FlushCache()

        bandval=None
        dst_ds=None
        src_ds=None
        os.rename(outfile,outfile.replace(".tif_tmp",".tif"))
        ImageStore.update_file_statistics(outfile.replace(".tif_tmp",".tif"))

        log.send_message("Completed")

    except Exception as e:
        bandval=None
        dst_ds = None
        src_ds = None
        r = glob.glob(outfile+"*")
        for i in r:
            os.remove(i)

        log.send_error("Processing error: image too big to be loaded in memory or "+str(e))


    log.close()