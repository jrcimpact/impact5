#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import cgi
import cgitb

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal

# Import local libraries
import LogStore
import Settings


cgitb.enable()
params = cgi.FieldStorage()
SETTINGS = Settings.load()


if __name__ == "__main__":

    log = LogStore.LogProcessing('Raster palette color table manager', 'RasterPCT')
    infile = sys.argv[1]
    pct = sys.argv[2]

    #-----------------------------------------------------
    #---------------  INIT SHAPEFILE ---------------------
    #-----------------------------------------------------

    try:
        log.set_parameter('Input Raster', infile)
        if pct != "":
            log.set_parameter('Palette color table', pct)
            log.send_message('Add palette color table to file')

        else:
            log.send_message('Removing palette color table from file')


        driver = gdal.GetDriverByName("GTiff")
        src_ds = gdal.Open(infile, gdal.GA_Update)
        c = gdal.ColorTable()
        if pct == '':
            src_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_GrayIndex)
            ct = src_ds.GetRasterBand(1).GetColorTable()
            ct = None
            src_ds.GetRasterBand(1).SetColorTable(ct)

        else:
            # get pct from settings
            for item in SETTINGS['map_legends']['raster']:
                if item['type'] == pct:
                    for cl in item['classes']:
                        #cl[0] = minval  cl[1] = maxval
                        #cl[3] = minColor  cl[4] = maxColor
                        mincol = cl[3].split(',')
                        maxcol = cl[4].split(',')
                        print(mincol[0])
                        print(mincol[1])
                        print(mincol[2])
                        print(maxcol[0])
                        print(maxcol[1])
                        print(maxcol[2])
                        c.CreateColorRamp(int(cl[0]), (int(mincol[0]), int(mincol[1]), int(mincol[2])), int(cl[1]),(int(maxcol[0]), int(maxcol[1]), int(maxcol[2])))
                    break
            src_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
            src_ds.GetRasterBand(1).SetColorTable(c)

        src_ds = None
        log.send_message("Completed")

    except Exception as e:

        src_ds = None

        print("ERROR")
        log.send_error("Error while saving color table :" + str(e))


    log.close()