#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import time
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

import subprocess

# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import ImageProcessing
import GdalOgr


def usage():
    print()
    print( 'Author: Simonetti Dario for European Commission 2015')
    print()
    print( 'Image Conversion ')
    print( 'RasterConversion.py out_name img_names projection nodata psize overwrite format')
    sys.exit(1)

def getArgs(args):

    if (len(sys.argv) < 11):
        usage()
    else:

        out_name=sys.argv[1]

        try:
            img_names=json.loads(sys.argv[2])
        except:
            img_names=sys.argv[2].split(',')
        projection=sys.argv[3]
        nodata=sys.argv[4]
        psize=sys.argv[5]
        type=sys.argv[6]
        rescale=sys.argv[7]
        bands=sys.argv[8]
        format=sys.argv[9]
        overwrite=sys.argv[10]

        return out_name, img_names, projection, nodata, psize, type, rescale, bands, format, overwrite




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_RasterConversion(out_suffix, img_names, projection, nodata, psize, type, rescale, bands, format, overwrite):


    # initialize output log
    log = LogStore.LogProcessing('RasterConversion', 'RasterConversion')


    log.set_input_file(img_names)
    log.set_parameter('Output suffix',out_suffix)
    log.set_parameter('Projection',projection)
    log.set_parameter('Nodata',nodata)
    log.set_parameter('Pixel Size',psize)
    log.set_parameter('Data Type ',type)
    log.set_parameter('Rescaling',rescale)
    log.set_parameter('Select bands',bands)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Format',format)

    log.set_parameter('Default gdal parameter for GTiff ',' COMPRESS=LZW BIGTIFF=IF_SAFER')
    log.set_parameter('Default gdal parameter for JPEG,PNG,GIF ',' -scale -ot Byte')
    log.set_parameter('Default gdal resampling method ',' nearest')

    ImageProcessing.validPathLengths(img_names, None,len(out_suffix)+5, log)

    log.send_message("Running")
    try:
        driver = gdal.GetDriverByName("GTiff")
        if bands != '':
            if bands.endswith(','):
                bands=bands[:-1]
            if bands.startswith(','):
                bands=bands[1:]
            maxBand = max([int(val) for val in bands.split(',')])
            bands= ' -b '+bands.replace(',',' -b ')

        if out_suffix.startswith('_'):
            out_suffix = out_suffix[1:]

        if rescale != '':
            if rescale == 'auto':
                rescale = "-scale "
            else:
                rescale = "-scale "+rescale.replace(',',' ')
        if psize != '':
            ps=psize.split(",")
            psize = "-tr "+str(ps[0])+" "+str(ps[1])
        if type != '':
            type = " -ot "+type

        if format == "GTiff":
            tmp_format = " -of GTiff "
            suffix = out_suffix
            ext = '.tif'
            co = " -co COMPRESS=LZW -co BIGTIFF=IF_SAFER"

        if format == "JP2OpenJPEG":
            tmp_format=" -of JP2OpenJPEG "
            suffix = out_suffix
            ext = '.jp2'

        if format == "JPEG":
            tmp_format=" -of JPEG "
            suffix = out_suffix
            ext = '.jpeg'
            type= " -ot Byte"
            rescale = "-scale "

        if format == "PNG":
            tmp_format=" -of PNG "
            suffix = out_suffix
            ext = '.png'
            type = " -ot Byte"
            rescale = "-scale "

        if format == "ESRI Shapefile":
            driver = ogr.GetDriverByName("ESRI Shapefile")
            tmp_format = " -of GTiff "  # apply all trnsformation using gdal driver
            suffix = out_suffix
            ext = '.shp'

        if format == "KML":
            driver = ogr.GetDriverByName("KML")
            tmp_format = " -of GTiff "  # apply all trnsformation using gdal driver
            suffix = out_suffix
            ext = '.kml'

        if format == "GML":
            driver = ogr.GetDriverByName("GML")
            tmp_format = " -of GTiff "  # apply all trnsformation using gdal driver
            suffix = out_suffix
            ext = '.gml'

        metadataparams = "Proj:"+projection+' Bands:'+str(bands)+' Nodata:'+str(nodata)+' Res:'+str(psize)+' Scale:'+str(rescale)+' '+str(tmp_format)

        if nodata != '':
            nodata = " -a_nodata " + str(nodata)

        log.send_message('Processing')

        for img_name in img_names:
            try:
                log.send_message('Processing '+img_name)
                filename, file_extension = os.path.splitext(img_name)
                outname = filename + "_" + suffix

                if (overwrite in ["True","true","Yes","Y","y",'1']):
                    if os.path.exists(outname + ext):
                        log.send_message('Deleting output file')
                        if format in ["ESRI Shapefile","KML","GML"]:
                            driver.DeleteDataSource(outname+ext)
                        else:
                            driver.Delete(outname+ext)
                            if os.path.exists(outname+ext+'.aux.xml'):#xx=clean_tmp_file(outdir+'//'+out_name)
                                os.remove(outname+ext+'.aux.xml')
                elif os.path.exists(outname+ext):
                    log.send_warning(outname+ext+' <b> exists, change name or set Overwrite. </b>')
                    continue

                # is possible to get info from Imagestore if geotiff BUT
                # in case of import of jp2 or other format there is no entry onDB
                # go to file query directly  out is
                # info = {
                #     'extent': json.dumps({"E": data_source.RasterXSize, "S": 0, "W": 0, "N": data_source.RasterYSize}),
                #     'PROJ4': '',
                #     'EPSG': '',
                #     'pixel_size': geo_transform[1],
                #     'metadata': json.dumps(metadata),
                #     'num_columns': data_source.RasterXSize,
                #     'num_rows': data_source.RasterYSize,
                #     'no_data': data_source.GetRasterBand(1).GetNoDataValue(),
                #     'simplificationMethod': set_simplification_method(filename)

                # }

                img_info = GdalOgr.get_info_from_tiff(img_name)
                print(img_info)

                imgEPSG = img_info['EPSG']
                imgPROJ4 = img_info['PROJ4']
                innodata = img_info['no_data']
                inbands = img_info['num_bands']
                inDataType = img_info['data_type']

                # in_ds = gdal.Open(img_name)
                # try:
                #     prj = in_ds.GetProjection()
                #     srs = osr.SpatialReference(wkt=prj)
                #     imgEPSG = srs.GetAuthorityCode(None)
                #     inband = info['num_bands']
                #     innodata = (inband.GetNoDataValue())
                #     inDataType = gdal.GetDataTypeName(inband.DataType)
                # except Exception as e:
                #     log.send_error(img_name + ' has wrong projection')
                #     continue


                if "JP2OpenJPEG" in format :
                    if ("Float32" in type or type == '') and inDataType not in ['Byte','Int16','Uint16','Int32','Uint32'] :
                        log.send_error(img_name+' must have Data Type = Byte,Int16,Uint16,Int32,Uint32 <b> Skipping file. </b>')
                        in_ds = None
                        continue
                # ----------  SET BANDS --------------
                if bands != '' and inbands < maxBand:
                    log.send_error(img_name+' <b>does not have selected bands</b>')
                    in_ds=None
                    continue
                # ----------  SET NODATA --------------
                force_clean_nodata = False
                if nodata != '':
                    print('Changing nodata')
                    print(innodata)
                    print(nodata)
                    #nodata = " -a_nodata "+str(nodata)

                    # if nodata is the same gdal is keeping the values
                    if innodata != None:
                        if float(innodata) == nodata:
                            nodata = ''
                        else:
                            # run gdal with nodata = None first to reset existing val
                            force_clean_nodata = True

                projection = projection.split(" ")[0]  # sometimes values from .JS contains names


                print('---------------------------------------------------')
                print(projection)
                print(imgEPSG)
                print(imgPROJ4)

                outPROJ4 = ''
                if projection != '':
                    outPROJ4 = subprocess.getoutput('gdalsrsinfo -o proj4 ' + projection).replace("\n", '')

                    if 'ERROR' in outPROJ4:
                        log.send_error(" Error while converting " + projection + " to Proj4, continuing using provided EPSG code")
                    print("OUT:", outPROJ4)


                if projection != '' and imgPROJ4 is not None and outPROJ4 != imgPROJ4:
                    print("Reproj")
                    # print imgEPSG
                    # if imgEPSG is not None:
                    #     if int(projection) == int(imgEPSG):
                    #         # do not use gdalwarp
                    #         log.send_message("Input image has same projection, skip warping")
                    #         continue



                    print("Reproj .......")
                    nodata_option = ''
                    if os.path.exists(outname + '_tmpRprj'):
                        os.remove(outname + '_tmpRprj')

                    if force_clean_nodata:
                        nodata_option = ' -srcnodata None -dstnodata None'
                        force_clean_nodata=False

                    #print 'gdalwarp -co BIGTIFF=IF_SAFER -t_srs EPSG:' + str(int(projection)) + ' -multi ' + nodata_option + ' ' + psize + ' "' + img_name + '" "' + outname + '_tmpRprj"'




                    os.system('gdalwarp -of GTiff -overwrite -co BIGTIFF=IF_SAFER -t_srs "'+projection+'" -multi '+nodata_option+' '+psize+' "'+img_name+'" "'+outname+'_tmpRprj"')
                    img_name = outname + '_tmpRprj'

                    if not os.path.exists(img_name):
                        log.send_error("Failure in warping image: please check resolution and projection code: "+ projection)
                        continue
                    # else:

                        # # # NO EFFECT
                        # data_source = gdal.Open(img_name)
                        # #srs = osr.SpatialReference(wkt=data_source.GetProjection())
                        # srs = osr.SpatialReference()
                        # srs.ImportFromEPSG(int(projection))
                        # print('-------------------')
                        # srs.SetAuthority('PROJCS', 'EPSG', int(projection))
                        # print(srs.ExportToWkt())
                        # data_source.SetProjection(srs.ExportToWkt())
                        # data_source = None
                        # # data_source.SetProjCS(srs.ExportToWkt())
                        #
                        # print('-------------------')
                        # #
                        # # data_source1 = gdal.Open(img_name)
                        # # srs = osr.SpatialReference(wkt=data_source1.GetProjectionRef())
                        # # print srs.ExportToWkt()
                        # # data_source1 = None

                if force_clean_nodata:
                    log.send_message('Unsetting Nodata')
                    # better use gdal_edit DS
                    print('Unsetting nodata')
                    if os.path.exists('"'+outname+'_tmpnoDta"'):
                        os.remove('"'+outname+'_tmpnoDta"')

                    os.system('gdal_translate -of GTiff -co BIGTIFF=IF_SAFER -a_nodata None "'+img_name + '" ' + psize + ' ' + bands+' '+type+' "'+outname+'_tmpnoDta"')
                    bands = ''
                    psize = ''
                    img_name = outname+'_tmpnoDta'



                # ---- do the final conversion
                # ---- valid also for vector conversion since 1 band is needed + eventually warping operations
                #print 'gdal_translate -co COMPRESS=LZW -co BIGTIFF=IF_SAFER '+img_name +' '+nodata+' '+psize+' '+bands+' '+rescale+' '+type+' '+tmp_format+' '+outname
                os.system('gdal_translate -co COMPRESS=LZW -co BIGTIFF=IF_SAFER "'+img_name +'" '+nodata+' '+psize+' '+bands+' '+rescale+' '+type+' '+tmp_format+' "'+outname+'"')

                if os.path.exists(outname):
                    if format in ["GTiff","JP2OpenJPEG","PNG","JPEG"]:

                        os.rename(outname,outname+ext)
                        if format in ["PNG", "JPEG"]:
                            continue
                        # edit might fail on not GIF or JP2
                        try:
                            gdal_edit = "gdal_edit"
                            if (GLOBALS['OS'] == "unix"):
                                gdal_edit = "gdal_edit.py"
                            res1 = os.system( gdal_edit+' "' + outname + ext + '" -mo "Impact_product_type=Processed Raster" -mo "Impact_operation=' + metadataparams + '" -mo "Impact_version=' + IMPACT.get_version() + '"')
                        except:
                            pass
                        print('Rename xml')
                        if os.path.exists(outname+'.aux.xml'):
                            print('Xml')
                            os.rename('"'+outname+'.aux.xml"', '"'+outname+ext + '.aux.xml"')
                            print('Xml renamed')
                    else:
                        # RASTER 2 VECTOR
                        log.send_message("Converting to " + format)
                        tmp_ds = gdal.Open(outname)
                        inband = tmp_ds.GetRasterBand(1)
                        inDataType = gdal.GetDataTypeName(inband.DataType)
                        tmp_ds = None
                        print(inDataType)
                        if "Float" in inDataType:
                            log.send_warning("Vector conversion from type " + inDataType + " is unsafe due to auto cast to Integer;")
                            log.send_message("If you want to retain some decimal places multiply your raster by 10^N where N is the number of decimal places you want to retain")
                        res1 = os.system('gdal_polygonize -8 -f  "' + format + '" "' + outname + '" "' + outname + ext + '"')
                        os.remove(outname)

                else:
                    log.send_error('Failure in creating image '+outname+ext)
                    continue

                if os.path.exists(outname+'_tmpRprj'):
                    os.remove(outname+'_tmpRprj')
                if os.path.exists(outname+'_tmpRprj.aux.xml'):
                    os.remove(outname+'_tmpRprj.aux.xml')
                if os.path.exists(outname+'_tmpnoDta.aux.xml'):
                    os.remove(outname+'_tmpnoDta.aux.xml')
        # -- for loop
            except Exception as e:
                log.send_error("Unexpected error processing " + outname + ext + ": " + str(e).replace("'", ''))
                continue

    except Exception as e:
        log.send_error("Generic error: "+str(e).replace("'", ''))

    log.send_message("Completed.")
    log.close()
    return


if __name__ == "__main__":

    out_suffix, img_names, projection, nodata, psize, type, rescale, bands, format, overwrite = getArgs(sys.argv)
    res = ''
    try:
        run_RasterConversion(out_suffix, img_names, projection, nodata, psize, type, rescale, bands, format, overwrite)
    except:
        print("Error "+str(res))
