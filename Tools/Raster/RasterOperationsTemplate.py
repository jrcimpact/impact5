#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import numpy


try:
    from osgeo import gdal, osr
except ImportError:
    import gdal, osr



# import GDAL/OGR modules

def usage():
    print
    print 'Author: Simonetti Dario for European Commission 2020'
    print 'Wrapper for Raster operations'
    print
    sys.exit(0)


def getArgs(args):
    infile = None

    if (len(sys.argv) < 2):
        usage()
    else:


        infile = sys.argv[1]


        if (infile is None):
            usage()

        else:
            return infile



# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":

    # Retrieve arguments
    IN = getArgs(sys.argv)
    print IN
    outdir = os.path.join(os.path.dirname(IN),'OUT')
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    OUTname = os.path.join(outdir,os.path.basename(IN))+'f'#.replace('.tif','_proc.tif1')
    print OUTname
    if True:
    #try:

        # -------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        # -------------------------------------------------------------------
        master_ds = gdal.Open(IN, gdal.GA_ReadOnly)
        master_bands = master_ds.RasterCount
        print master_bands
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize


        driver = gdal.GetDriverByName("GTiff")
        OutDataType = gdal.GDT_Byte


        dst_ds = driver.Create(OUTname, master_cols, master_rows, 3, OutDataType,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        dst_ds.SetGeoTransform(master_ds.GetGeoTransform())
        dst_ds.SetProjection(master_ds.GetProjectionRef())

        IN_BAND = (master_ds.GetRasterBand(1).ReadAsArray()) * 0.8
        IN_BAND[IN_BAND<0] = 1
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(IN_BAND)
        dst_ds.FlushCache()
        IN_BAND = None
        outband= None

        print 'B1 done '
        IN_BAND = (master_ds.GetRasterBand(2).ReadAsArray()) * 0.85
        IN_BAND[IN_BAND < 0] = 1
        outband= dst_ds.GetRasterBand(2)
        outband.WriteArray(IN_BAND)
        dst_ds.FlushCache()
        IN_BAND = None
        outband = None
        print 'B2 done '
        IN_BAND = (master_ds.GetRasterBand(3).ReadAsArray()) *1
        outband = dst_ds.GetRasterBand(3)
        outband.WriteArray(IN_BAND)
        print 'B3 done '
        dst_ds.FlushCache()

        dst_ds = None

    #except Exception, e:
        print(str(e))

