#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import shutil
import numpy
# import GDAL/OGR modules
try:
    from osgeo import gdal
except ImportError:
    import gdal


def usage():
    print()
    print( 'Author: Simonetti Dario for European Commission 2022')
    print()
    print( 'Raster Recoder ')
    print( 'RasterRecoderBatch.py infile table outfile')
    sys.exit(1)

def getArgs(args):

    if (len(sys.argv) < 4):
        usage()
    else:

        infile = sys.argv[1]
        table = sys.argv[2]
        outfile = sys.argv[3]

        return infile, table, outfile

def main(infile, table, outfile):

    if not os.path.exists(outfile):
        shutil.copy(infile, outfile)
    else:
        print('Out already exists')
        return

    master_ds = gdal.Open(outfile, gdal.GA_Update)
    raster_band = master_ds.GetRasterBand(1)

    bandval = (raster_band.ReadAsArray().astype(numpy.float))  # set to byte if necessary
    outband = (raster_band.ReadAsArray().astype(numpy.float))

    with open(table) as f:
        contents = f.readlines()
        for line in contents:
            try:
                print(line)
                myrange=line.split(',')
                outband[(bandval == float(myrange[0]))] = float(myrange[1])
            except Exception as e:
                print(str(e))


        raster_band.WriteArray(outband)
        raster_band.FlushCache()

        outband = None
        bandval = None

if __name__ == '__main__':
    infile, table, outfile = getArgs(sys.argv)
    main(infile, table, outfile)
