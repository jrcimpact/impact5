#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import numpy
import multiprocessing

try:
    from osgeo import gdal, osr
except ImportError:
    import gdal, osr

import time



from numba import njit, prange
from numba.types import float32, double


# import GDAL/OGR modules

def usage():
    print
    print('Author: Simonetti Dario for European Commission 2020')
    print('Wrapper for Raster operations')
    print()
    print(' biomass - soil - tmf (a b c )  removals')
    sys.exit(0)


def getArgs(args):

    print
    if (len(sys.argv) < 3):
        usage()
    else:


        IN1 = sys.argv[1]
        IN2 = sys.argv[2]
        # IN3 = sys.argv[3]
        # IN4 = sys.argv[4]


        if (IN1 is None):
            usage()
        if (IN2 is None):
            usage()
        # if (IN3 is None):
        #     usage()
        # if (IN4 is None):
        #     usage()
        else:
            return IN1, IN2

@njit(parallel=True, cache=True)
def compute_avg(data1,data2):
    avg = (data1 + data2) / 2.

    return avg



@njit(double[:, :](double[:, :]))
def average3Line(image):
    C, L = image.shape
    # out = numpy.zeros(C, L)
    out = image*0
    for l in range(1, L - 2):
        out[l:] = numpy.mean(image[l - 1:l + 1, :], axis=0)

    return out



@njit(double[:, :](double[:, :], double[:, :]))
def filter2d(image, filt):
    M, N = image.shape
    Mf, Nf = filt.shape
    Mf2 = Mf // 2
    Nf2 = Nf // 2
    result = numpy.zeros_like(image)
    for i in range(Mf2, M - Mf2):
        for j in range(Nf2, N - Nf2):
            num = 0.0
            for ii in range(Mf):
                for jj in range(Nf):
                    num += (filt[Mf - 1 - ii, Nf - 1 - jj] * image[i - Mf2 + ii, j - Nf2 + jj])
            result[i, j] = num
    return result
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":

    # Retrieve arguments
    IN1, IN2 = getArgs(sys.argv)

    OUTname = os.path.join(os.path.dirname(IN1),'Out_average.tif')#.replace('.tif','_proc.tif1')
    # OUTname_removal = os.path.join(os.path.dirname(IN1),'Out_removal.tif')#.replace('.tif','_proc.tif1')
    print(OUTname)


    # -------------------------------------------------------------------
    # ------------------------- OPEN MASTER  ---------------------------
    # -------------------------------------------------------------------
    master_ds1 = gdal.Open(IN1, gdal.GA_ReadOnly)
    master_cols = master_ds1.RasterXSize
    master_rows = master_ds1.RasterYSize


    master_transform = master_ds1.GetGeoTransform()
    masterXorigin = master_transform[0]
    masterYorigin = master_transform[3]
    masterPixelWidth = master_transform[1]
    masterPixelHeight = master_transform[5]
    MasterXEnd = masterXorigin + masterPixelWidth * master_cols
    MasterYEnd = masterYorigin + masterPixelHeight * master_rows
    EXTENT = (masterXorigin, MasterYEnd, MasterXEnd, masterYorigin)
    print(EXTENT)

    srs = osr.SpatialReference(wkt=master_ds1.GetProjection())
    srs_proj4 = srs.ExportToProj4()
    outEPSG = srs.GetAuthorityCode(None)
    outEPSG = "EPSG:"+str(outEPSG)
    print(outEPSG)

    # -------------------------------------------------------------------------------------
    # reading others
    # -------------------------------------------------------------------------------------
    # print('warping file 2')
    # IN2_tmp = IN2+'_tmp'
    # IN2_ds = gdal.Warp(IN2_tmp, IN2, dstSRS=str(outEPSG), format='Gtiff',
    #                    # xRes=outRes, yRes=outRes,
    #                    outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
    #                    options=['compress=lzw', 'bigtiff=IF_SAFER'])
    IN2_ds = gdal.Open(IN2, gdal.GA_ReadOnly)



    # print('warping file 3')
    # IN134_tmp = os.path.join(os.path.dirname(IN1),'_in134_tmp.vrt')
    # os.system(
    #     'gdalbuildvrt -resolution highest -separate -b 1 '+IN134_tmp+' ' + IN1 + ' ' + IN3 + ' ' + IN4 )
    # # IN3_tmp = IN3 + '_tmp'
    # # IN3_ds = gdal.Warp(IN3_tmp, IN3, dstSRS=str(outEPSG), format='Gtiff',
    # #                    # xRes=outRes, yRes=outRes,
    # #                    outputBounds=EXTENT, outputBoundsSRS=str(outEPSG), multithread=True,
    # #                    options=['compress=lzw', 'bigtiff=IF_SAFER', '-multi'],
    # #                    warpOptions=['multithread=True','multi','-multi']
    # #                    )
    # print('warped')
    # -------------------------------------------------------------------------------------
    # preparing out
    # -------------------------------------------------------------------------------------

    driver = gdal.GetDriverByName("GTiff")
    OutDataType = gdal.GDT_Float32
    dst_ds = driver.Create(OUTname, master_cols, master_rows, 1, OutDataType,
                           options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
    dst_ds.SetGeoTransform(master_ds1.GetGeoTransform())
    dst_ds.SetProjection(master_ds1.GetProjectionRef())
    print('Out init ...')




    Cells = range(master_cols)
    RowRange = range(master_rows)
    band1 = master_ds1.GetRasterBand(1)
    band2 = IN2_ds.GetRasterBand(1)
    outband = dst_ds.GetRasterBand(1)


    step = 1000
    start = time.time()




    # This kind of quadruply-nested for-loop is going to be quite slow.
    # Using Numba we can compile this code to LLVM which then gets
    # compiled to machine code:


    # Now fastfilter_2d runs at speeds as if you had first translated
    # it to C, compiled the code and wrapped it with Python
    image = numpy.random.random((1000, 1000))
    filt = numpy.random.random((10, 10))
    res = filter2d(image, filt)
    print("Normal Proc time: " + str(time.time() - start))

    start = time.time()
    res = average3Line(band1.ReadAsArray().astype(numpy.float32))
    outband.WriteArray(res)
    outband.FlushCache()
    outband = None
    print("AVERAGE  Proc time: " + str(time.time() - start))




    # from numba import double, jit
    # fastfilter_2d = jit(double[:, :](double[:, :], double[:, :]))(filter2d)
    #
    # res = fastfilter_2d(image, filt)
    # print("NUMBA Proc time: " + str(time.time() - start))


    # for ThisRow in range(0, master_rows, step):
    #     if ThisRow + step >= master_rows:
    #         ThisRow = master_rows - step - 1
    #     # xoff = 0, yoff = 0, win_xsize = None, win_ysize = None
    #     line1 = band1.ReadAsArray(0, ThisRow, master_cols, step).astype(numpy.float32)
    #     line2 = band2.ReadAsArray(0, ThisRow, master_cols, step).astype(numpy.float32)
    #
    #
    #     OUT =  (line1 + line2) / 2.
    #     outband.WriteArray(OUT, xoff=0, yoff=ThisRow)
    #     outband.FlushCache()
    #
    #
    #     OUT=None
    #     line1=None
    #     line2=None
    #
    #
    #     if ThisRow % 1000 == 0:
    #         print(ThisRow)
    #
    # outband.FlushCache()
    # band1=None
    # band2=None


