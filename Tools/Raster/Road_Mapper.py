#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import math
import json
import shutil

#import mahotas
#import gdal_sieve
import skimage.morphology

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

from scipy import ndimage

# Import local libraries
import LogStore
from __config__ import *
from Simonets_PROC_libs import *
import ImageProcessing


def RunRoadMapper(img, OUTname, overwrite, roadBand, winsize, sieveSize, nbrThreshold, tmp_indir):

    try:
        log.set_parameter('Master image',img )
        log.set_parameter('Out name ', OUTname)

        ImageProcessing.validPathLengths(OUTname,None, 5, log)

        OUTname = OUTname.replace('.tif', '')
        OUTnameClass = OUTname + '_class'
        OUTopenings = OUTname + '_openings'
        OUTsieve = os.path.join(tmp_indir, os.path.basename(OUTname) + 'sieve')
        OUTopenRaw = os.path.join(tmp_indir, os.path.basename(OUTname) + 'openRAW')

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        if os.path.exists(OUTname+'.tif') :
            if overwrite:
                try:
                    os.remove(OUTname+'.tif')
                    if os.path.exists(OUTname+".tif.oux.xml"):
                        os.remove(OUTname+".tif.out.xml")
                    os.remove(OUTnameClass+'.tif')
                    if os.path.exists(OUTnameClass + ".tif.oux.xml"):
                        os.remove(OUTnameClass + ".tif.oux.xml")
                    os.remove(OUTopenings+'.tif')
                    if os.path.exists(OUTopenings + ".tif.oux.xml"):
                        os.remove(OUTopenings + ".tif.oux.xml")
                except:
                    log.send_error('Cannot delete output file')

                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')

                return True

        #-------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        #-------------------------------------------------------------------
        master_ds = gdal.Open(img, gdal.GA_ReadOnly)
        master_bands=master_ds.RasterCount

        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        master_sourceSR = osr.SpatialReference()
        master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
        m_ulx=master_imgGeo[0]
        m_uly=master_imgGeo[3]
        m_lrx=master_imgGeo[0]+master_cols*master_imgGeo[1]
        m_lry=master_imgGeo[3]+master_rows*master_imgGeo[5]




        driver = gdal.GetDriverByName("GTiff")
        dst_ds = driver.Create(OUTname,master_cols,master_rows,1,gdal.GDT_Float32,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        dst_ds.SetGeoTransform( master_ds.GetGeoTransform())
        dst_ds.SetProjection( master_ds.GetProjectionRef())

        sieve_ds = driver.Create(OUTsieve,master_cols,master_rows,1,gdal.GDT_Byte,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        sieve_ds.SetGeoTransform( master_ds.GetGeoTransform())
        sieve_ds.SetProjection( master_ds.GetProjectionRef())

        # ----------------------------------------------
        # ---  READING IMG and apply moving window -----
        # ----------------------------------------------
        master_band = master_ds.GetRasterBand(roadBand).ReadAsArray().astype(numpy.float32)

        print(master_band.shape)
        out_band=master_band*0
        stepx=int(master_cols/winsize)
        stepy=int(master_rows/winsize)

        print(master_cols)
        print(master_rows)
        print(numpy.min(master_band))
        print(numpy.max(master_band))

        #result = gdal.SieveFilter(master_ds.GetRasterBand(1), None, sieve_ds.GetRasterBand(1), sieveSize, 8, callback=gdal.TermProgress)
        #print "SIEVE"

        #sieved_band = sieve_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
        #sieve_ds = None
        #print "OK"

        # im = ndimage.gaussian_filter(master_band, 1)
        #
        # sx = ndimage.sobel(im, axis=0, mode='constant')
        # sy = ndimage.sobel(im, axis=1, mode='constant')
        # sob = numpy.hypot(sx, sy)

        # plt.figure(figsize=(16, 5))
        # plt.subplot(141)
        # plt.imshow(im, cmap=plt.cm.gray)
        # plt.axis('off')
        # plt.title('square', fontsize=20)
        # plt.subplot(142)
        # plt.imshow(sx)
        # plt.axis('off')
        # plt.title('Sobel (x direction)', fontsize=20)
        # plt.subplot(143)
        # plt.imshow(sob)
        # plt.axis('off')
        # plt.title('Sobel filter', fontsize=20)

        #im += 0.07 * numpy.random.random(im.shape)

        # sx = ndimage.sobel(im, axis=0, mode='constant')
        # sy = ndimage.sobel(im, axis=1, mode='constant')
        # sob = numpy.hypot(sx, sy)

        print('Trasf OK ')
        #sobOUT= sob

        x = winsize//2
        y = winsize//2
        print(x)
        print(y)

        # while (x+winsize < master_rows):
        #     while (y+winsize < master_cols):
        #
        #         #out_band[x+winsize/2,y+winsize/2]=numpy.sum(master_band[x:x+winsize,y:y+winsize])
        #         out_band[x,y]=math.ceil(numpy.sum(denoisedIMG[x-winsize/2:x-winsize/2+winsize,y-winsize/2:y-winsize/2+winsize])/(winsize*winsize*1.)*100)
        #         #sobOUT[x,y]=math.ceil(numpy.sum(sob[x-winsize/2:x-winsize/2+winsize,y-winsize/2:y-winsize/2+winsize])/(winsize*winsize*1.)*100)
        #         y+=1
        #     x+=1
        #     y=0

        while (x + winsize < master_rows):
            while (y+winsize < master_cols):

                #out_band[x+winsize/2,y+winsize/2]=numpy.sum(master_band[x:x+winsize,y:y+winsize])
                out_band[x,y]=(numpy.sum(master_band[x-winsize//2:x-winsize//2+winsize,y-winsize//2:y-winsize//2+winsize]))
                #sobOUT[x,y]=math.ceil(numpy.sum(sob[x-winsize/2:x-winsize/2+winsize,y-winsize/2:y-winsize/2+winsize])/(winsize*winsize*1.)*100)
                y+=1
            x+=1
            y=0

        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(out_band)
        dst_ds.FlushCache()
        dst_ds = None
        print('Sum ok')


        kernel8 = numpy.ones((3, 3))
        kernel7 = numpy.ones((7, 7))
        kernel4 = numpy.ones((3, 3))
        kernel4[0, 0] = 0
        kernel4[0, 2] = 0
        kernel4[2, 0] = 0
        kernel4[2, 2] = 0

        MASK = out_band >= nbrThreshold
        # else:
        #     MASK = out_band > 0.2*winsize*1.2*denoiseFactor/2
        #CORE = (mmorph.dilate(MASK, kernel8))
        #CORE = mahotas.morph.erode(MASK, kernel4)
        CORE = skimage.morphology.binary_erosion(MASK, kernel4)
        print('Core ok')
        # if nbrThreshold:
        #     out_band[out_band < 0.1*(winsize/2)*(winsize/2) ] = 0





        core_ds = driver.Create(OUTopenRaw, master_cols, master_rows, 1, gdal.GDT_Byte,options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        core_ds.SetGeoTransform(master_ds.GetGeoTransform())
        core_ds.SetProjection(master_ds.GetProjectionRef())
        # sx = ndimage.sobel(out_band, axis=0, mode='constant')
        # sy = ndimage.sobel(out_band, axis=1, mode='constant')
        # sob = numpy.hypot(sx, sy)
        outband = core_ds.GetRasterBand(1)
        outband.WriteArray(CORE)
        core_ds.FlushCache()


        result = gdal.SieveFilter(core_ds.GetRasterBand(1), None, sieve_ds.GetRasterBand(1), sieveSize, 8, callback=gdal.TermProgress)



        sieved_band = sieve_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
        sieve_ds = None
        dst_ds = None
        core_ds = None

        print("SIEVE OK ")

        #sieve_ds = None
        MASK = CORE > 0
        CORE = (CORE - sieved_band) * MASK
        dst_ds = driver.Create(OUTopenings, master_cols, master_rows, 1, gdal.GDT_Byte,options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        dst_ds.SetGeoTransform(master_ds.GetGeoTransform())
        dst_ds.SetProjection(master_ds.GetProjectionRef())
        # sx = ndimage.sobel(out_band, axis=0, mode='constant')
        # sy = ndimage.sobel(out_band, axis=1, mode='constant')
        # sob = numpy.hypot(sx, sy)
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(CORE)
        dst_ds.FlushCache()
        dst_ds = None

        #master_ds = None


        out_band *= 0
        winsize = 3
        x = winsize // 2
        y = winsize // 2
        while (x + winsize < master_rows):
            while (y + winsize < master_cols):
                out_band[x,y]=math.ceil(numpy.sum(sieved_band[x-winsize//2:x-winsize//2+winsize,y-winsize//2:y-winsize//2+winsize])/(winsize*winsize*1.)*100)
                y += 1
            x += 1
            y = 0
        final_ds = driver.Create(OUTnameClass, master_cols, master_rows, 1, gdal.GDT_Byte, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        final_ds.SetGeoTransform(master_ds.GetGeoTransform())
        final_ds.SetProjection(master_ds.GetProjectionRef())
        outband = final_ds.GetRasterBand(1)
        outband.WriteArray(out_band)
        final_ds.FlushCache()
        ctable = [
            [0, 0, (0, 0, 0)],
            [1, 10, (200, 50, 50)],
            [10, 20, (235, 160, 5)],
            [20, 30, (220, 255, 80)],
            [30, 40, (170, 255, 1)],
            [40, 50, (180, 235, 110)],
            [50, 60, (85, 255, 1)],
            [60, 70, (50, 200, 1)],
            [70, 80, (50, 160, 1)],
            [80, 90, (40, 110, 1)],
            [90, 101, (40, 80, 1)],

        ]
        c = gdal.ColorTable()
        for cid in range(0, len(ctable)):
            c.CreateColorRamp(ctable[cid][0], ctable[cid][2], ctable[cid][1], ctable[cid][2])

        final_ds.GetRasterBand(1).SetColorTable(c)
        final_ds = None

        os.rename(OUTname,OUTname+'.tif')
        os.rename(OUTnameClass,OUTnameClass + '.tif')
        os.rename(OUTopenings, OUTopenings + '.tif')

        log.send_message("File processed.")



    except Exception as e:
        log.send_error("Moving Window  error: "+str(e))
        return False


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Road Mapper ')
    print('Road_Mapper.py inputs out_suffix overwrite roadBand roadKernel roadMMU roadThreshold')
    sys.exit(1)


def getArgs(args):

    if (len(sys.argv) < 8):
        usage()
    else:
        try:
            inputs=json.loads(sys.argv[1])
        except:
            inputs=sys.argv[1].split(',')

        out_suffix = sys.argv[2]
        overwrite = sys.argv[3]
        roadBand = int(sys.argv[4])
        roadKernel = int(sys.argv[5])
        roadMMU = int(sys.argv[6])
        roadThreshold = float(sys.argv[7])

        return inputs, out_suffix, overwrite, roadBand, roadKernel, roadMMU, roadThreshold





if __name__ == "__main__":


    inputs, out_suffix, overwrite, roadBand, roadKernel, roadMMU, roadThreshold = getArgs(sys.argv)

    if overwrite in ["True", "true", "Yes", "Y", "y", '1']:
        overwrite = True
    else:
        overwrite = False

    try:
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "RoadMapper")
        try:
            shutil.rmtree(tmp_indir, ignore_errors=True)
        except Exception:
            pass

        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)
        print(tmp_indir)

        log = LogStore.LogProcessing('Road Mapper', 'Road Mapper')
        log.set_parameter('Overwrite ',overwrite)
        log.set_parameter('Band ',roadBand)
        log.set_parameter('Kernel size ',roadKernel)
        log.set_parameter('Sieve MMU ',roadMMU)
        log.set_parameter('Kernel sum Thresghold ',roadThreshold)

        for img in inputs:
            outname = img.replace('.tif','_'+out_suffix+'.tif')
            res = RunRoadMapper(img, outname, overwrite, roadBand, roadKernel, roadMMU, roadThreshold, tmp_indir)

        log.send_message("Completed.")
        log.close()
        try:
            shutil.rmtree(tmp_indir, ignore_errors=True)
        except Exception:
            pass


    except Exception as e:
        log.send_error("Road Mapper error: " + str(e).replace("'", ''))

    log.close()
    try:
        shutil.rmtree(tmp_indir, ignore_errors=True)
    except Exception:
        pass

    # winsize = 7
    # sieveSize = 300
    # denoiseFactor = 0
    # nbrThreshold = 3.5



    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo_17-18_max.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo_17-18_max_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)


    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo_NBR_base_S2_Kabo_CIB_WB_2017-01-01--2017-12-31__0-0.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo2017_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)
    #
    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo_NBR_second_S2_Kabo_CIB_WB_2018-01-01--2018-12-31__0-0.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\kabo2018_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)

    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\IFO_2018.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\IFO_2018_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)
    #
    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\IFO_2017.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\NEW\IFO_2017_" + str(winsize) + "x" + str(winsize) + '_' + str(sieveSize) + "_" + str(denoiseFactor) + "_" + str(nbrThreshold) + ".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)

    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\SEFYD_DeltaNBR_S2_bin.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\SEFYD_DeltaNBR_S2_bin_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res = RunMovingWindow(overwrite, master, winsize, sieveSize, denoiseFactor, nbrThreshold, OUTname)
    #
    # master = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\c_1_CIB_DeltaNBR_S2_bin.tif"
    # OUTname = "E:\dev\IMPACT3\IMPACT\DATA\ROAD\c_1_CIB_DeltaNBR_S2_bin_"+str(winsize)+"x"+str(winsize)+'_'+str(sieveSize)+"_"+str(denoiseFactor)+"_"+str(nbrThreshold)+".tif"
    # res=RunMovingWindow(overwrite,master,winsize,sieveSize,denoiseFactor,nbrThreshold,OUTname)
    #

