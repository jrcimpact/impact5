#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from Simonets_PROC_libs import *
import ImageProcessing


if __name__ == "__main__":


    overwrite = True
    Input = sys.argv[1]
    try:
        option = sys.argv[2]
    except:
        option = '-add'

    try:
        #NIR=int(NIRb)
        #SWIR=int(SWIRb)
        # initialize output log

        log = LogStore.LogProcessing('Add / Remove Palette', 'Add / Remove Palette')
        log.set_parameter('Input image',Input )
        log.set_parameter('Option',option)



        # # ----------------------------------------------------
        # # ------ DEL out file if necessary  ------------------
        # # ----------------------------------------------------
        # if os.path.exists(OUTname) :
        #     if overwrite:
        #         try:
        #             os.remove(OUTname)
        #             if os.path.exists(OUTname+".out.xml"):
        #                 os.remove(OUTname+".out.xml")
        #
        #         except:
        #             log.send_error('Cannot delete output file')
        #             log.close()
        #             sys.exit()
        #     else:
        #         log.send_warning(OUTname+' <b> Already Exists </b>')
        #         log.close()
        #         sys.exit()



        #-------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        #-------------------------------------------------------------------
        master_ds = gdal.Open(Input, gdal.GA_Update)
        master_bands = master_ds.RasterCount
        print(master_bands)
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        master_sourceSR = osr.SpatialReference()
        master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
        m_ulx=master_imgGeo[0]
        m_uly=master_imgGeo[3]
        m_lrx=master_imgGeo[0]+master_cols*master_imgGeo[1]
        m_lry=master_imgGeo[3]+master_rows*master_imgGeo[5]
        print(master_sourceSR.ExportToWkt())

        # ----------------------------------------------------
        # -------------        INFO   ------------------------
        # ----------------------------------------------------
        log.send_message("Input Parameters ")
        log.send_message("----------------------------------------------------------")
        log.send_message("Rows = "+str(master_rows))
        log.send_message("Cols = "+str(master_cols))
        log.send_message("Proj = "+str(master_sourceSR))
        log.send_message("ULX = "+str(m_ulx))
        log.send_message("ULY = "+str(m_uly))
        log.send_message("LRX = "+str(m_lrx))
        log.send_message("LRY = "+str(m_lry))
        log.send_message("----------------------------------------------------------")


        if option == '-clean':
            master_ds.GetRasterBand(1).SetColorTable(None)
            log.send_message("Completed.")
            log.close()
            sys.exit()

        # driver = gdal.GetDriverByName("GTiff")
        # dst_ds = driver.Create( OUTname,master_cols,master_rows,1,gdal.GDT_Byte,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        # dst_ds.SetGeoTransform( master_ds.GetGeoTransform())
        # dst_ds.SetProjection( master_ds.GetProjectionRef())
        #
        # # ----------------------------------------------
        # # ---  READING IMG and apply moving window -----
        # # ----------------------------------------------
        #
        # master_band=master_ds.GetRasterBand(1).ReadAsArray().astype(numpy.byte)
        #
        # outband = dst_ds.GetRasterBand(1)
        # outband.WriteArray(master_band)
        # dst_ds.FlushCache()

        ROADLESS = [
            [0, (0, 0, 0)],
            [1, (0, 0, 0)],
            [2, (0, 0, 0)],
            [3, (0, 0, 0)],
            [4, (0, 0, 0)],
            [5, (0, 0, 0)],
            [6, (0, 0, 0)],
            [7, (0, 0, 0)],
            [8, (0, 0, 0)],
            [9, (0, 0, 0)],
            [10,(0, 80, 0)],
            [11, (0, 100, 0)],
            [12, (0, 0, 0)],
            [13, (0, 0, 0)],
            [14, (0, 0, 0)],
            [15, (0, 0, 0)],
            [16, (0, 0, 0)],
            [17, (0, 0, 0)],
            [18, (0, 0, 0)],
            [19, (0, 0, 0)],
            [20, (0, 0, 0)],
            [21, (30, 130, 0)],
            [22, (30, 130, 0)],
            [23, (100, 160, 0)],
            [24, (100, 160, 0)],
            [25, (0, 0, 0)],
            [26, (0, 0, 0)],
            [27, (0, 0, 0)],
            [28, (0, 0, 0)],
            [29, (0, 0, 0)],
            [30, (0, 0, 0)],
            [31, (180, 210, 60)],
            [32, (180, 210, 60)],
            [33, (180, 210, 60)],
            [34, (180, 210, 60)],
            [35, (0, 0, 0)],
            [36, (0, 0, 0)],
            [37, (0, 0, 0)],
            [38, (0, 0, 0)],
            [39, (0, 0, 0)],
            [40, (255, 255, 155)],
            [41, (255, 235, 30)],
            [42, (255, 215, 30)],
            [43, (255, 200, 20)],
            [44, (255, 180, 15)],
            [45, (255, 160, 10)],
            [46, (255, 140, 10)],
            [47, (255, 120, 10)],
            [48, (255, 100, 10)],
            [49, (255, 80, 10)],
            [50, (0, 0, 0)],
            [51, (255, 60, 0)],
            [52, (255, 20, 0)],
            [53, (215, 0, 0)],
            [54, (0, 0, 0)],
            [55, (0, 0, 0)],
            [56, (0, 0, 0)],
            [57, (0, 0, 0)],
            [58, (0, 0, 0)],
            [59, (0, 0, 0)],
            [60, (0, 0, 0)],
            [61, (170, 80, 80)],
            [62, (145, 90, 65)],
            [63, (120, 95, 50)],
            [64, (0, 0, 0)],
            [65, (0, 0, 0)],
            [66, (0, 0, 0)],
            [67, (0, 0, 0)],
            [68, (0, 0, 0)],
            [69, (0, 0, 0)],
            [70, (0, 0, 0)],
            [71, (0, 77, 168)],
            [72, (0, 157, 200)],
            [73, (32, 178, 170)],
            [74, (102, 205, 170)],
            [75, (0, 0, 0)],
            [76, (0, 0, 0)],
            [77, (0, 0, 0)],
            [78, (0, 0, 0)],
            [79, (0, 0, 0)],
            [80, (0, 0, 0)],
            [81, (51, 99, 51)],
            [82, (98, 161, 80)],
            [83, (188, 209, 105)],
            [84, (255, 228, 158)],
            [85, (250, 208, 128)],
            [86, (250, 180, 150)],
            [87, (204, 163, 163)],
            [88, (0, 0, 0)],
            [89, (0, 0, 0)],
            [90, (0, 0, 0)],
            [91, (255, 255, 255)],
            [92, (209, 255, 115)],
            [93, (240, 255, 210)],
            [94, (240, 255, 255)],
            [95, (0, 0, 0)],
            [96, (0, 0, 0)],
            [97, (0, 0, 0)],
            [98, (0, 0, 0)],
            [99, (0, 0, 0)],
            [100, (0, 0, 0)]
        ]

        PBS = [

            [0, (1, 1, 1)],
            [1, (255, 255, 255)],
            [2, (175, 238, 238)],
            [3, (1, 255, 255)],
            [4, (255, 1, 1)],
            [5, (255, 1, 1)],
            [6, (200, 190, 220)],
            [7, (100, 149, 237)],
            [8, (1, 1, 205)],
            [9, (150, 250, 200)],
            [10, (10, 108, 1)],
            [11, (1, 128, 1)],
            [12, (34, 139, 34)],
            [13, (50, 205, 50)],
            [14, (190, 255, 90)],
            [15, (30, 250, 30)],
            [16, (120, 160, 50)],
            [17, (255, 1, 1)],
            [18, (255, 1, 1)],
            [19, (255, 1, 1)],
            [20, (160, 225, 150)],
            [21, (210, 250, 180)],
            [22, (215, 238, 158)],
            [23, (255, 1, 1)],
            [24, (255, 1, 1)],
            [25, (128, 118, 26)],
            [26, (140, 150, 30)],
            [27, (153, 193, 193)],
            [28, (216, 238, 160)],
            [29, (237, 255, 193)],
            [30, (240, 250, 220)],
            [31, (227, 225, 170)],
            [32, (212, 189, 184)],
            [33, (255, 255, 1)],
            [34, (255, 225, 255)],
            [35, (140, 5, 198)],
            [36, (158, 132, 123)],
            [37, (255, 1, 1)],
            [38, (255, 1, 1)],
            [39, (255, 1, 1)],
            [40, (40, 70, 20)],
            [41, (145, 1, 10)],
            [42, (100, 100, 100)],
            [43, (255, 1, 1)]
        ]

        PINO = [
            [0, (0, 0, 0)],
            [1, (255, 255, 255)],
            [2, (192, 242, 255)],
            [3, (1, 255, 255)],
            [4, (0, 0, 0)],
            [5, (1, 1, 255)],
            [6, (1, 123, 255)],
            [7, (110, 150, 255)],
            [8, (168, 180, 255)],
            [9, (160, 255, 90)],
            [10, (1, 80, 1)],
            [11, (12, 113, 1)],
            [12, (1, 155, 1)],
            [13, (100, 190, 90)],
            [14, (146, 255, 165)],
            [15, (0, 0, 0)],
            [16, (210, 255, 153)],
            [17, (0, 0, 0)],
            [18, (0, 0, 0)],
            [19, (0, 0, 0)],
            [20, (0, 0, 0)],
            [21, (237, 255, 193)],
            [22, (200, 230, 200)],
            [23, (0, 0, 0)],
            [24, (0, 0, 0)],
            [25, (0, 0, 0)],
            [26, (0, 0, 0)],
            [27, (0, 0, 0)],
            [28, (0, 0, 0)],
            [29, (0, 0, 0)],
            [30, (200, 200, 150)],
            [31, (227, 225, 170)],
            [32, (0, 0, 0)], [33, (0, 0, 0)],
            [34, (255, 225, 255)],
            [35, (140, 5, 190)],
            [36, (255, 1, 1)],
            [37, (0, 0, 0)],
            [38, (0, 0, 0)],
            [39, (0, 0, 0)],
            [40, (20, 40, 10)],
            [41, (170 ,150,150)],
            [42, (100, 100, 100)]
        ]

        MANGROVES = [
            [0, (0, 0, 0)],
            [1, (0, 0, 0)],
            [2, (0, 0, 0)],
            [3, (0, 0, 0)],
            [4, (0, 0, 0)],
            [5, (0, 0, 0)],
            [6, (0, 0, 0)],
            [7, (0, 0, 0)],
            [8, (0, 0, 0)],
            [9, (0, 0, 0)],
            [10, (1, 150, 1)],    # '1.0* stable with flat trend'       Green
            [11, (34, 255, 34)],  # '1.0* stable with negative trend'
            [12, (1, 80, 1)],     # '1.0* stable with positive trend'
            [13, (220, 255, 220)], # 1.0* light disturbed but stable
            [14, (0, 0, 0)],
            [15, (0, 0, 0)],
            [16, (0, 0, 0)],
            [17, (0, 0, 0)],
            [18, (0, 0, 0)],
            [19, (0, 0, 0)],
            [20, (100, 100, 255)],   #'2.0* never stable flat trend       Blue
            [21, (153, 153, 255)],   #'2.0* never stable negative trend
            [22, (1, 1, 200)],       #'2.0* never stable positive trend
            [23, (0, 0, 0)],
            [24, (0, 0, 0)],
            [25, (0, 0, 0)],
            [26, (0, 0, 0)],
            [27, (0, 0, 0)],
            [28, (0, 0, 0)],
            [29, (0, 0, 0)],
            [30, (255, 130, 1)],   #'3.1* Disturbed with flat trend'    Orange
            [31, (255, 170, 85)],  #'3.1* Disturbed with negative trend'
            [32, (190, 100, 1)],   #'3.1* Disturbed with positive trend'
            [33, (0, 0, 0)],
            [34, (0, 0, 0)],
            [35, (0, 0, 0)],
            [36, (0, 0, 0)],
            [37, (0, 0, 0)],
            [38, (0, 0, 0)],
            [39, (0, 0, 0)],
            [40, (255, 1, 1)],    #'4.0* degradation trend     Red
            [41, (0 ,0,0)],
            [42, (0, 0, 0)],
            [43, (0, 0, 0)],
            [44, (0, 0, 0)],
            [45, (0, 0, 0)],
            [46, (0, 0, 0)],
            [47, (0, 0, 0)],
            [48, (0, 0, 0)],
            [49, (0, 0, 0)],
            [50, (120, 1, 60)],    #'5.0* Regenerated'        Pink
            [51, (255, 90, 170)],      # '5.1* Regenerated'
            [52, (255, 180, 210)],   #'5.2* regeneration trend but not completed'
            [53, (0, 0, 0)],
            [54, (0, 0, 0)],
            [55, (0, 0, 0)],
            [56, (0, 0, 0)],
            [57, (0, 0, 0)],
            [58, (0, 0, 0)],
            [59, (0, 0, 0)],
            [60, (205,205, 1)]      # '6.0* Rehabilitated'
        ]

        ESACCI = [
            [  0,(  0,  0,  0)],
            [ 10,(255,255,100)],
            [ 11,(255,255,100)],
            [ 12,(255,255,  0)],
            [ 20,(170,240,240)],
            [ 30,(220,240,100)],
            [ 40,(200,200,100)],
            [ 50,(  0,100,  0)],
            [ 60,(  0,160,  0)],
            [ 61,(  0,160,  0)],
            [ 62,(170,200,  0)],
            [ 70,(  0, 60,  0)],
            [ 71,(  0, 60,  0)],
            [ 72,(  0, 80,  0)],
            [ 80,( 40, 80,  0)],
            [ 81,( 40, 80,  0)],
            [ 82,( 40,100,  0)],
            [ 90,(120,130,  0)],
            [100,(140,160,  0)],
            [110,(190,150,  0)],
            [120,(150,100,  0)],
            [121,(120, 75,  0)],
            [122,(150,100,  0)],
            [130,(255,180, 50)],
            [140,(255,204,204)],
            [150,(255,235,175)],
            [151,(255,200,100)],
            [152,(255,210,120)],
            [153,(255,235,175)],
            [160,(  0,120, 90)],
            [170,(  0,150,120)],
            [180,(  0,200,130)],
            [190,(195, 20,  0)],
            [200,(255,245,215)],
            [201,(220,220,220)],
            [202,(255,245,215)],
            [210,(  0, 70,200)],
            [220,(255,255,255)],
            ]




        if option == 'DEM':
            colors = gdal.ColorTable()
            colors.CreateColorRamp(0, (46, 154, 88), 1800, (251, 255, 128))
            colors.CreateColorRamp(1800, (251, 255, 128), 2800, (224, 108, 31))
            colors.CreateColorRamp(2800, (224, 108, 31), 3800, (200, 55, 55))
            master_ds.GetRasterBand(1).SetRasterColorTable(colors)
            master_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)

        if option == 'RUSLE':
            colors = gdal.ColorTable()
            colors.CreateColorRamp(0, (0, 0, 0, 255), 0, (0, 0, 0, 255))
            colors.CreateColorRamp(1, (1, 128, 1, 255), 50, (206, 206, 1, 255))
            colors.CreateColorRamp(50, (210, 210, 1, 255), 100, (255, 128, 1, 255))
            colors.CreateColorRamp(100, (255, 128, 1, 255), 150, (255, 1, 1, 255))
            colors.CreateColorRamp(150, (200, 30, 30, 255), 200, (255, 1, 255, 255))
            colors.CreateColorRamp(200, (200, 200, 30, 255), 600, (150, 50, 255, 255))
            colors.CreateColorRamp(600, (150, 50, 200, 255), 65000, (200, 200, 200, 255))
            # colors.CreateColorRamp(1, (10, 250, 10), 150, (242, 10, 200))
            # colors.CreateColorRamp(150, (242, 238, 162), 65000, (242, 206, 133))
            master_ds.GetRasterBand(1).SetRasterColorTable(colors)
            master_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)

        if option in ['PBS','ROADLESS','PINO','MANGROVES','ESACCI']:
            if option == 'PBS':
                ctable = PBS
            if option == 'ROADLESS':
                ctable = ROADLESS
            if option == 'PINO':
                ctable = PINO
            if option == 'MANGROVES':
                ctable = MANGROVES
            if option == 'MANGROVES':
                ctable = ESACCI

            c = gdal.ColorTable()
            for cid in range(0,len(ctable)):
                c.SetColorEntry(cid, ctable[cid][1])

            master_ds.GetRasterBand(1).SetColorTable(c)


        # dst_ds=None
        master_ds = None


        log.send_message("Completed.")
        log.close()



    except Exception as e:
        log.send_error("Palette error: "+str(e))
        log.close()
