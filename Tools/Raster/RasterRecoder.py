#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import random
import numpy
import time

# import GDAL/OGR modules
from Simonets_PROC_libs import bbox_to_pixel_offsets

try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import ImageStore
import LogStore
from __config__ import *
#from Simonets_PROC_libs import *

if __name__ == '__main__':

    #print "Content-type: text/html;\n"
    log = LogStore.LogProcessing('Raster recode ', 'EditingRaster')
    infile = sys.argv[1]
    band = int(sys.argv[2])
    ranges = json.loads(sys.argv[3])
    save_to_new = sys.argv[4]
    out_suffix = sys.argv[5]
    recode_extent = sys.argv[6]
    AOIwkt = sys.argv[7]



    if save_to_new in ["yes", "Yes", "true", "True", "Y", "y"]:
        save_to_new = True
    else:
        save_to_new = False

    #-----------------------------------------------------
    #---------------  INIT SHAPEFILE ---------------------
    #-----------------------------------------------------

    try:

        log.set_parameter('Input Raster', infile)
        log.set_parameter('Band',band)
        log.set_parameter('Save to new file', save_to_new)
        log.set_parameter('Suffix', out_suffix)
        log.set_parameter('Extent', recode_extent)

        if AOIwkt != 'No':
            log.set_parameter('Use AOI', 'Yes')
        else:
            log.set_parameter('Use AOI', 'No')

        newfile = ''
        infile_orig = infile # use in case of failure to reset the status and resfresh the page
        if save_to_new:
            newfile = infile.replace('.tif', "_" + out_suffix.replace('.tif', '') + '.tif')
            try:
                if os.path.exists(newfile):
                    os.remove(newfile)
                out_ds = gdal.Translate(newfile + '.tmp', infile, bandList = [band], format = 'GTiff', creationOptions = ['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                out_ds = None
                band = 1
                infile = newfile + '.tmp'
            except:
                raise Exception("Error deleting " + newfile)
                # OLD raise Exception, "Error deleting " + newfile
                #log.send_error("Error deleting " + newfile)


        else:
            # set "pending on DB" to prevent propagation on JS
            ImageStore.set_status(infile, 'pending')

        driver = gdal.GetDriverByName("GTiff")
        src_ds = gdal.Open(infile, 1)


        log.send_message("Reading file in memory....")
        src_band = src_ds.GetRasterBand(band)
        MinX = 0
        MinY = 0
        deltaX = src_ds.RasterXSize
        deltaY = src_ds.RasterYSize

        transform = src_ds.GetGeoTransform()
        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = transform[5]
        xEnd = xOrigin + pixelWidth * src_ds.RasterXSize
        yEnd = yOrigin + pixelHeight * src_ds.RasterYSize

        wkt = "POLYGON ((" + str(xOrigin) + " " + str(yOrigin) + "," + str(xEnd) + " " + str(yOrigin) + "," + str(
            xEnd) + " " + str(yEnd) + "," + str(xOrigin) + " " + str(yEnd) + "," + str(xOrigin) + " " + str(
            yOrigin) + "))"
        rasterExtentGeom = ogr.CreateGeometryFromWkt(wkt)

        # --------------------------------------------------------------------------------------------------------------
        # -----------------------  READ MAP EXTENT ONLY ----------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        if recode_extent != '':

            recode_extent = recode_extent.split(",")

            minXmap = float(recode_extent[0])
            minYmap = float(recode_extent[1])
            maxXmap = float(recode_extent[2])
            maxYmap = float(recode_extent[3])

            minXmap = max([minXmap, xOrigin])
            minYmap = max([minYmap, yEnd])

            maxXmap = min([maxXmap, xEnd])
            maxYmap = min([maxYmap, yOrigin])

            try:
                MinX, MinY, deltaX, deltaY = bbox_to_pixel_offsets(transform, [minXmap, maxXmap, minYmap, maxYmap])
                if min([MinX, MinY, deltaX, deltaY]) < 0:
                    raise Exception("AOI is negative")
            except Exception as e:
                raise Exception("Error determining subset location: " + str(e))


            print(MinX, MinY, deltaX, deltaY)
            log.send_message(str(MinX)+' '+str(MinY)+' '+str(deltaX)+' '+str(deltaY))

            wkt = "POLYGON ((" + str(minXmap) + " " + str(minYmap) + "," + str(maxXmap) + " " + str(minYmap) + "," + str(
                maxXmap) + " " + str(maxYmap) + "," + str(minXmap) + " " + str(maxYmap) + "," + str(minXmap) + " " + str(
                minYmap) + "))"
            rasterExtentGeom = ogr.CreateGeometryFromWkt(wkt)

        mask_array = numpy.ones((deltaY, deltaX), dtype=bool)
        print('--------------')
        print(deltaX,deltaY)

        # --------------------------------------------------------------------------------------------------------------
        # -----------------------  BURN AOI ----------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        if AOIwkt != 'No':
            print('Importing AOI')
            my_coordinates = []
            try:
                for item in json.loads(AOIwkt)['features']:
                    my_coordinates.append(item['geometry']['coordinates'])
                myGeoJson = json.dumps({"type": "MultiPolygon", "coordinates": my_coordinates})

                AOIGeom = ogr.CreateGeometryFromJson(myGeoJson)
                targetSR = osr.SpatialReference()
                sourceSR = osr.SpatialReference()
                targetSR.ImportFromWkt(src_ds.GetProjectionRef())
                sourceSR.ImportFromEPSG(3857)
                if gdal.VersionInfo()[0] == '3':
                    sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                    targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

                Geo_transform = osr.CoordinateTransformation(sourceSR, targetSR)
                AOIGeom.Transform(Geo_transform)
                #print AOIGeom.IsValid()
                AOIGeom = AOIGeom.Buffer(0.0)
                print(AOIGeom.IsValid())


            except Exception as e:
                print(str(e))
                raise Exception(str(e))


            intersection = rasterExtentGeom.Intersection(AOIGeom)
            if intersection.ExportToWkt() == "GEOMETRYCOLLECTION EMPTY":
                raise Exception('No intersection between raster and AOI')


            #print "Geom are OK "
            minXmap, maxXmap, minYmap, maxYmap = intersection.GetEnvelope()

            #print "Repositioned "
            MinX = int((minXmap - xOrigin) / pixelWidth)
            MaxX = numpy.ceil((maxXmap - xOrigin) / pixelWidth) # + 1

            MinY = int((maxYmap - yOrigin) / pixelHeight)
            MaxY = numpy.ceil((minYmap - yOrigin) / pixelHeight) # + 1

            MaxX = min(MaxX,src_ds.RasterXSize)
            MaxY = min(MaxY,src_ds.RasterYSize)

            deltaX = MaxX - MinX
            deltaY = MaxY - MinY

            # --- test MIN MAX within image size
            deltaX = int(min(deltaX,src_ds.RasterXSize))
            deltaY = int(min(deltaY,src_ds.RasterYSize))
            deltaX = int(max(1,deltaX))
            deltaY = int(max(1,deltaY))

            log.send_message(str(MinX) + ' ' + str(MinY) + ' ' + str(deltaX) + ' ' + str(deltaY))

            new_gt = (
                minXmap,
                pixelWidth,
                0.0,
                maxYmap,
                0.0,
                pixelHeight
            )
            ogr_ds = ogr.GetDriverByName('Memory').CreateDataSource('wrk')
            mem_poly_lyr = ogr_ds.CreateLayer('poly', srs=targetSR, geom_type=ogr.wkbPolygon)
            feat = ogr.Feature(mem_poly_lyr.GetLayerDefn())
            feat.SetGeometryDirectly(intersection)
            mem_poly_lyr.CreateFeature(feat)

            driver = gdal.GetDriverByName('MEM')
            rvds = driver.Create('', deltaX, deltaY, 1, gdal.GDT_Byte)
            rvds.SetGeoTransform(new_gt)
            new_gt = None
            gdal.RasterizeLayer(rvds, [1], mem_poly_lyr, burn_values=[1], options=['ALL_TOUCHED=TRUE'])
            mask_array = rvds.ReadAsArray().astype(numpy.bool)


        bandval = src_band.ReadAsArray(MinX, MinY, deltaX, deltaY)
        outband = src_band.ReadAsArray(MinX, MinY, deltaX, deltaY)

        for myrange in ranges:
            try:
                if myrange[0] == myrange[1]:
                    outband[(bandval == float(myrange[0]))*mask_array] = float(myrange[2])
                    print(myrange[0])
                else:
                    outband[((bandval >= float(myrange[0])) * (bandval <= float(myrange[1]))) * mask_array] = float(myrange[2])
                    print(myrange[0])
                    print(myrange[1])
            except Exception as e:
                print(str(e))
                log.send_error("Memory error" + str(e))





        src_band.WriteArray(outband,MinX, MinY)
        src_ds.FlushCache()

        outband = None
        bandval = None

        # ADD LOOKUP TABLE ID NEW CODE DOES NOT EXISTS
        lookup_table = src_band.GetColorTable()

        if lookup_table is not None:
            log.send_message("Lookup table found")
            num_colors = lookup_table.GetCount()
            log.send_message("Num colors:" + str(num_colors))
            try:
                for myrange in ranges:
                    # add missing entry
                    randR = random.randint(1,255)
                    randG = random.randint(1,255)
                    randB = random.randint(1,255)
                    if int(myrange[2]) > num_colors:
                        log.send_message("Set new val " + str(myrange[2]))
                        lookup_table.SetColorEntry(int(myrange[2]), (randR, randB, randG))
                    # ensure is not zero
                    else:
                        log.send_message("val exists " + str(myrange[2]))
                        color_entry = lookup_table.GetColorEntry(int(myrange[2]))
                        if color_entry[0] == 0 and color_entry[1] == 0 and color_entry[2] == 0:
                            log.send_message("Update val " + str(myrange[2]))
                            lookup_table.SetColorEntry(int(myrange[2]), ((randR, randB, randG)))
            except Exception as e:
                src_ds = None
                src_band = None
                raise Exception('Error while saving color table')

            # error if writing here , strange
            src_ds.GetRasterBand(1).SetColorTable(lookup_table)

        src_ds.FlushCache()
        src_ds = None
        src_band = None


        if save_to_new:
            os.rename(infile, infile.replace('.tmp', ''))
        print("Cleaning overviews")
        res1 = os.system('gdaladdo -clean ' + infile.replace('.tmp', ''))
        ImageStore.update_file_statistics(infile.replace('.tmp', ''))
        log.send_message("Completed")
        log.close()

    except Exception as e:
        bandval = None
        src_ds = None
        src_band = None

        # -- force layer update to propagate event to map and restore editing win
        # pending = does not rebuild stats
        ImageStore.set_status(infile_orig, 'pending')
        os.utime(infile_orig, None)
        ImageStore.update_file_info(infile_orig)
        ImageStore.set_status(infile_orig, 'true')


        log.send_error("Processing error "+str(e).replace("'",''))
        if os.path.exists(newfile + '.tmp'):
            os.remove(newfile + '.tmp')

        log.close()

