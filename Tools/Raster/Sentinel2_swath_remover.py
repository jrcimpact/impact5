#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import shutil
import numpy
import copy
from datetime import datetime
from scipy import ndimage

try:
    from osgeo import gdal, osr
except ImportError:
    import gdal, osr



# import GDAL/OGR modules

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2020')
    print('Params:')
    print('Infile: input file')
    print('Band: band to be corrected')
    print('Correction: % -+[0-100] of each pixel to be added to the original value following a W-E gradient. Negative % will lower values in the W while keeping the same values on the E ')
    print('Start: row to start with (usually = 0)')
    print('Start transition in %: apply a smooth correction of % of the image height starting from Start value. (set to o to start from first row)')
    print('Stop: row to stop at (usually = MaxRow)')
    print('Stop transition in %: apply a smooth transition from a % of the image height to Stop value (set to 0 to stop at last row)')
    print()
    print('Swath is rotated by 12deg, correction goes from W to E adding the Correction value using a gradient')
    print('Since the BRDF effect is more evident over the evergreen forest, the Start-Stop and % options can be')
    print('used to limit the scope of the correction ')

    sys.exit(0)


def getArgs(args):
    infile = None

    if (len(sys.argv) < 7):
        usage()
    else:

        infile = sys.argv[1]
        band = sys.argv[2]
        factor = sys.argv[3]

        start = sys.argv[4]
        start_percent = sys.argv[5]

        stop = sys.argv[6]
        stop_percent = sys.argv[7]

        invert_direction = False
        rorate = True

        try:
            invert_direction = sys.argv[8]
            if (invert_direction in ["True", "true", "Yes", "Y", "y", '1']):
                invert_direction = True
            else:
                invert_direction = False
        except Exception as e:
            invert_direction = False


        try:
            rotate = sys.argv[9]
            if (rotate in ["False", "false", "No", "N", "n", '0']):
                rotate = False
            else:
                rotate = True
        except Exception as e:
            rotate = True

        if (infile is None):
            usage()

        else:
            return infile, int(band), float(factor), int(start), float(start_percent)/100., int(stop), float(stop_percent)/100., invert_direction, rotate


def get_gradient_2d(start, stop, width, height, is_horizontal):
    if is_horizontal:
        return numpy.tile(np.linspace(start, stop, width), (height, 1))
    else:
        return numpy.tile(np.linspace(start, stop, height), (width, 1)).T


def get_gradient_3d(width, height, start_list, stop_list, is_horizontal_list):
    result = numpy.zeros((height, width, len(start_list)), dtype=np.float)

    for i, (start, stop, is_horizontal) in enumerate(zip(start_list, stop_list, is_horizontal_list)):
        result[:, :, i] = get_gradient_2d(start, stop, width, height, is_horizontal)

    return result


# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":

    # Retrieve arguments
    IN, band , factor, start, start_percent, stop, stop_percent, invert_direction, rotate = getArgs(sys.argv)
    print(IN)
    print(band)
    print(factor)
    print(start)
    print(start_percent)
    print(stop)
    print(stop_percent)
    print(invert_direction)
    print(rotate)

    
    startTime = datetime.now()

    factor = factor*1.
    #factor = factor/100.
    print(factor)

    outdir = os.path.join(os.path.dirname(IN),'OUT')
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    OUTname = os.path.join(outdir,os.path.basename(IN))#.replace('.tif','')
    print(OUTname)
    if not os.path.exists(OUTname):
        shutil.copy(IN,OUTname)

    master_ds = gdal.Open(OUTname, gdal.GA_Update)
    master_cols = master_ds.RasterXSize
    master_rows = master_ds.RasterYSize
    raster_band = master_ds.GetRasterBand(band)

    orig_ds = gdal.Open(IN, gdal.GA_ReadOnly)
    orig_band = orig_ds.GetRasterBand(band)
    IN_BAND = orig_band.ReadAsArray()#.astype(numpy.byte)
    orig_band = None
    orig_ds = None
    # OUT_BAND = (master_ds.GetRasterBand(b).ReadAsArray().astype(numpy.byte))
    #MASK = IN_BAND[(IN_BAND < abs(factor))]#.astype(bool)

    #NEG = (IN_BAND < abs(factor)).astype(bool)
    #NEG_VAL = IN_BAND[NEG]
    print('master_rows', master_rows)
    print('master_cols', master_cols)
    # centerR = int(master_rows/2.)
    # centerC = int(master_cols/2.)

    dataStart = 0
    dataStop = master_cols

    if int(rotate) :
        print('rotating ........')
        GRADIENT = ndimage.rotate(IN_BAND,12, reshape=True)

        print('master_rows mod', GRADIENT.shape[0])
        print('master_cols mod', GRADIENT.shape[1])
        deltaR = GRADIENT.shape[0] - master_rows
        deltaC = GRADIENT.shape[1] - master_cols
        centerR1 = int(GRADIENT.shape[0] / 2.)
        centerC1 = int(GRADIENT.shape[1] / 2.)
        # print(deltaR, deltaC)
        # print(centerR1, centerC1)
        #GRADIENT = numpy.ones((TMP.shape[0], TMP.shape[1])) * 100.
        master_rows = GRADIENT.shape[0]
        master_cols = GRADIENT.shape[1]
        centerR = int(master_rows / 2.)
        centerC = int(master_cols / 2.)

        #centerR= 3000
        for c in range(master_cols):
            if GRADIENT[centerR,c] > 5:
                dataStart=c-300
                if dataStart < 0:
                    dataStart = 0
                break
        for c in range(master_cols-1,0,-1):

            if GRADIENT[centerR,c] > 5:
                dataStop=c
                break
        #dataStart = 4000
        print('Starting Data Val',dataStart)
        print('Stop Data Val',dataStop)

  
        GRADIENT *=0
        GRADIENT +=100


    else:
        GRADIENT = numpy.ones((master_rows, master_cols)) * 100






    print('rotate', rotate)

    #MASK = (IN_BAND == 0)
    print('processing')
    start_row = 0
    start_buff = start_row
    if start > 0:
        start_row = start
        start_buff = start + int(master_rows*start_percent)
    delta_r_start = start_buff - start_row
    
    end_row = master_rows
    end_buff = master_rows
    if (stop > 0):
        end_row = stop
        if (end_row > master_rows ):
            end_row = master_rows-1
        end_buff = end_row - int(master_rows*stop_percent)
    delta_r_end = end_row-end_buff

    print('Start', start_row)
    print('Start b ', start_buff)
    print('End ', end_row)
    print('End b ', end_buff)

 
    print('------------------------')
    print(stop)
    print('------------------------')

    validDataRange = dataStop-dataStart
    col_val = factor/validDataRange

    for i in range(validDataRange):
        #val =((master_cols - i) * (factor / master_cols))
        val = 100 + (validDataRange - i) * col_val
        GRADIENT[start_buff:end_buff, dataStart+i] = val

        # start top smooth  
        if (start > 0):
            for r in range(start,start_buff,1):
                GRADIENT[r,dataStart+i] = 100+((val-100)/delta_r_start)*(r-start)

                # start top smooth  
        if (stop > 0):
            for r in range(end_buff,end_row,1):
                GRADIENT[r,dataStart+i] = val-((val-100)/delta_r_end)*(r-end_buff)
        
        
        

    print('GRADIENT done')
    
    if int(rotate) == 1:
        print('rotating back ........')
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        GRADIENT = ndimage.rotate(GRADIENT,-12,reshape=False)
        # TMP = ndimage.rotate(TMP,-12,reshape=False)
        centerR = int(GRADIENT.shape[0] / 2.)
        centerC = int(GRADIENT.shape[1] / 2.)
        GRADIENT = GRADIENT[int(centerR-master_rows/2.):int(centerR+master_rows/2.), int(centerC-master_cols/2.):int(centerC+master_cols/2.)]
        # TMP = TMP[int(centerR-master_rows/2.):int(centerR+master_rows/2.), int(centerC-master_cols/2.):int(centerC+master_cols/2.)]
        print('gradient_rows', GRADIENT.shape[0])
        print('gradient_cols', GRADIENT.shape[1])


    # OUT = GRADIENT[int(centerR - master_rows/2.):master_rows, int(centerC - master_cols/2.):master_cols]
    print('GRADIENT Applyed now saving')

    for c in range(master_rows):

        IN_BAND[c,:] = IN_BAND[c,:] * (GRADIENT[c, :]/100.)
    master_ds.GetRasterBand(band).WriteArray(IN_BAND)# * (GRADIENT/100.))
    # master_ds.GetRasterBand(band).WriteArray(GRADIENT)
    master_ds = None
    del master_ds

    NEG = 0
    NEG_VAL = 0
    IN_BAND = 0
    OUT_BAND = 0

    print('Band ' + str(band) + ' with factor ' + str(factor))
    print(datetime.now() - startTime )



