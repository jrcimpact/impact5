#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
# import GDAL/OGR modules
try:
    from osgeo import ogr, osr, gdal
except ImportError:
    import ogr, osr, gdal

import os
# Import local libraries


gdal.TermProgress = gdal.TermProgress_nocb


def RunRasterClass(infile1, infile2, infile3, infile4, infile5):

    try:
        out_name = infile1.replace('.tif', '_class')

        if os.path.exists(out_name + '.tif'):
                try:
                    os.remove(out_name + '.tif')
                except Exception, e:
                    return False



        input_ds1 = gdal.Open(infile1, gdal.GA_ReadOnly)
        input_ds2 = gdal.Open(infile2, gdal.GA_ReadOnly)
        input_ds3 = gdal.Open(infile3, gdal.GA_ReadOnly)
        input_ds4 = gdal.Open(infile4, gdal.GA_ReadOnly)
        input_ds5 = gdal.Open(infile5, gdal.GA_ReadOnly)




        driver = gdal.GetDriverByName("GTiff")
        dst_ds = driver.CreateCopy(out_name, input_ds1,1, ['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        OUT = dst_ds.GetRasterBand(1).ReadAsArray()

        band1 = input_ds1.GetRasterBand(1).ReadAsArray()
        band2 = input_ds2.GetRasterBand(1).ReadAsArray()
        band3 = input_ds3.GetRasterBand(1).ReadAsArray()
        band4 = input_ds4.GetRasterBand(1).ReadAsArray()
        band5 = input_ds5.GetRasterBand(1).ReadAsArray()

        TMP = band1 > band2
        TMP = TMP > band3
        TMP = TMP > band4
        TMP = TMP > band5
        OUT[TMP] = 1

        TMP = band1 > band2
        TMP = TMP > band3
        TMP = TMP > band4
        TMP = TMP > band5
        OUT[TMP] = 2

        dst_ds.WriteArray(OUT)
        dst_ds.FlushCache()
        dst_ds = None
        input_ds = None
        os.rename(out_name, out_name+'.tif')


    except Exception,e:


        input_ds = None
        dst_ds = None
        return False


if __name__ == "__main__":
    infile1 = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'
    infile2 = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'
    infile3 = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'
    infile4 = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'
    infile5 = 'E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/tm_226-068_28072000_class.tif'

    res = RunRasterClass(infile1, infile2, infile3, infile4, infile5)


