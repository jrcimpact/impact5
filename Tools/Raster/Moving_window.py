#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import math

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
from Simonets_PROC_libs import *
import ImageProcessing


def RunMovingWindow(overwrite,master,winsize,OUTname,Myfunction,OutDataType):

    try:
        #NIR=int(NIRb)
        #SWIR=int(SWIRb)
        # initialize output log

        log = LogStore.LogProcessing('Moving Window', 'Embrapa test')
        log.set_parameter('Master image',master )
        log.set_parameter('Out name ',OUTname)
        log.set_parameter('Overwrite ',overwrite)
        log.set_parameter('Window size ',winsize)
        log.set_parameter('Window function ', Myfunction)
        log.set_parameter('Out DataType', OutDataType)

        ImageProcessing.validPathLengths(OUTname,None, 5, log)



        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        if os.path.exists(OUTname) :
            if overwrite:
                try:
                    os.remove(OUTname)
                    if os.path.exists(OUTname+".out.xml"):
                        os.remove(OUTname+".out.xml")

                except:
                    log.send_error('Cannot delete output file')
                    log.close()
                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')
                log.close()
                return True



        #-------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        #-------------------------------------------------------------------
        master_ds = gdal.Open(master, gdal.GA_ReadOnly)
        master_bands=master_ds.RasterCount
        print(master_bands)
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        master_sourceSR = osr.SpatialReference()
        master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
        m_ulx=master_imgGeo[0]
        m_uly=master_imgGeo[3]
        m_lrx=master_imgGeo[0]+master_cols*master_imgGeo[1]
        m_lry=master_imgGeo[3]+master_rows*master_imgGeo[5]
        print(master_sourceSR.ExportToWkt())



        # ----------------------------------------------------
        # -------------        INFO   ------------------------
        # ----------------------------------------------------
        log.send_message("Master Parameters ")
        log.send_message("----------------------------------------------------------")
        log.send_message("Rows = "+str(master_rows))
        log.send_message("Cols = "+str(master_cols))
        log.send_message("Proj = "+str(master_sourceSR))
        log.send_message("ULX = "+str(m_ulx))
        log.send_message("ULY = "+str(m_uly))
        log.send_message("LRX = "+str(m_lrx))
        log.send_message("LRY = "+str(m_lry))
        log.send_message("----------------------------------------------------------")




        driver = gdal.GetDriverByName("GTiff")
        OutDataType = gdal.GDT_Float32
        if OutDataType == "Byte":
            OutDataType = gdal.GDT_Byte

        dst_ds = driver.Create( OUTname,master_cols,master_rows,1,OutDataType,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        dst_ds.SetGeoTransform( master_ds.GetGeoTransform())
        dst_ds.SetProjection( master_ds.GetProjectionRef())

        # ----------------------------------------------
        # ---  READING IMG and apply moving window -----
        # ----------------------------------------------

        master_band=master_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
        print(master_band.shape)
        out_band=master_band*0
        stepx=int(master_cols/winsize)
        stepy=int(master_rows/winsize)

        print(master_cols)
        print(master_rows)
        print(numpy.min(master_band))
        print(numpy.max(master_band))

        x = winsize / 2
        y = winsize / 2

        if Myfunction == "PERCENTAGE":
            while (x+winsize < master_rows):
                while (y+winsize < master_cols):
                    out_band[x,y]=math.ceil(numpy.sum(master_band[x-winsize/2:x-winsize/2+winsize,y-winsize/2:y-winsize/2+winsize])/(winsize*winsize*1.)*100)
                    y+=1
                x+=1
                y=0
        if Myfunction == "STD":
            while (x + winsize < master_rows):
                while (y + winsize < master_cols):
                    out_band[x, y] = numpy.std(master_band[x - winsize / 2:x - winsize / 2 + winsize, y - winsize / 2:y - winsize / 2 + winsize])
                    y += 1
                x += 1
                y = 0

        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(out_band)
        dst_ds.FlushCache()

        if Myfunction == "PERCENTAGE":
            ctable=[
                    [0, 0, (0, 0, 0)],
                    [1, 10, (200, 50, 50)],
                    [10, 20, (235 ,160 ,5)],
                    [20, 30, (220,255,80)],
                    [30, 40, (170, 255, 1)],
                    [40, 50, (180, 235, 110)],
                    [50, 60, (85,255,1)],
                    [60, 70, (50, 200, 1)],
                    [70, 80, (50 ,160 ,1)],
                    [80, 90, (40, 110 ,1)],
                    [90, 100,(40, 80 ,1)],

                   ]
            c = gdal.ColorTable()
            for cid in range(0,len(ctable)):
                c.CreateColorRamp(ctable[cid][0],ctable[cid][2],ctable[cid][1],ctable[cid][2])

            dst_ds.GetRasterBand(1).SetColorTable(c)


        dst_ds=None
        master_ds = None


        log.send_message("Completed.")
        log.close()

    except Exception as e:
        log.send_error("Moving Window  error: "+str(e))
        log.close()
        return False


if __name__ == "__main__":


    overwrite = True
    winsize=5
    master = "E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/Remove_tm_226-068_02081990_calrefbyt.tif"
    OUTname = "E:/dev/IMPACT3/IMPACT/DATA/Landsat_test_data/STD_"+str(winsize)+"x"+str(winsize)+".tif"

    res=RunMovingWindow(overwrite,master,winsize,OUTname,"STD","FLOAT")


