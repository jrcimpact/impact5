#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import subprocess
import time
import math
import json

# import GDAL/OGR modules
try:
    from Simonets_PROC_libs import *
except Exception:
    # if docker and libs is not in path
    from JRC_SD_libs.Simonets_PROC_libs import *

try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import ImageStore

import ImageProcessing


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Scan input directory and clip tif file using existing SHP file')
    print()
    sys.exit(1)


def getArgs(args):
    indir = None
    if (len(sys.argv) < 6):
        usage()
    else:

        shp_names = json.loads(sys.argv[1])
        img_names = json.loads(sys.argv[2])
        overwrite_clip = sys.argv[3]
        use_features = sys.argv[4]
        touching = sys.argv[5]

        return shp_names, img_names, overwrite_clip, use_features, touching


def clip_input_file(shp_names, img_names, overwrite_clip, use_features, touching):
    # initialize output log
    log = LogStore.LogProcessing('Clip', 'clip')
    log.set_input_file(shp_names)
    log.set_input_file(img_names)
    log.set_parameter('Overwrite output', overwrite_clip)
    log.set_parameter('Use single features', use_features)
    log.set_parameter('All touching ', touching)

    os.environ['SHAPE_ENCODING'] = "utf-8"
    maxlen = 0
    print(shp_names)
    for i in shp_names:
        maxlen = max(maxlen, len(os.path.basename(i)))

    ImageProcessing.validPathLengths(img_names, None, maxlen + 10, log)

    if touching.lower() in [True, "true", 'yes', 'yes']:
        touching = "YES"
    else:
        touching = 'NO'

    try:
        IMGs = img_names
        if (len(IMGs) == 0):
            log.send_message('No images')
            log.close()
            return

        for shp in shp_names:
            shpdir = os.path.dirname(shp)
            shpname = os.path.basename(shp)
            shpbasename = shpname.replace(".shp", "")
            log.send_message("Using SHP:" + shpbasename)

            # ---------- OPEN SHP -----------
            shp_driver = ogr.GetDriverByName("ESRI Shapefile")
            shpNew = None
            if use_features == 'No':
                # some shp have bad geometry cousing gdal 2.1 failing in -cutline (self-intersection etc etc)
                # apply a disolve before
                # e.g country borders, provinces etc etc 
                shpNew = os.path.join(GLOBALS['data_paths']['tmp_data'], os.path.basename(shp))
                os.system(
                    'ogr2ogr ' + shpNew + ' ' + shp + ' -dialect sqlite -sql "SELECT ST_Union(geometry) AS geometry FROM \'' + os.path.basename(
                        shp).replace('.shp', '') + '\'"  -explodecollections ')
                shp = shpNew

            shp_ds = shp_driver.Open(shp, update=0)
            layer = shp_ds.GetLayer(0)

            # ----- OPEN IMG -------------
            for img in IMGs:

                outdir = os.path.dirname(img)
                dirname = outdir
                # dirname=os.path.basename(dirname)   # no subwolder -> 2b improved
                inraster = os.path.basename(img)
                # log.send_message("Dirname:"+outdir)
                # if dirname == os.path.basename(os.path.dirname(GLOBALS['data_paths']["data"])):
                #     dirname=''

                log.send_message("Processing image:" + dirname + '/' + inraster)

                if test_overlapping(shp, img):
                    log.send_message("Intersects")
                    raster_ds = gdal.Open(img)
                    transform = raster_ds.GetGeoTransform()
                    xOrigin = float(transform[0])
                    yOrigin = float(transform[3])
                    pixelWidth = float(transform[1])
                    pixelHeight = float(transform[5])
                    xEnd = float(xOrigin + pixelWidth * float(raster_ds.RasterXSize))
                    yEnd = float(yOrigin + pixelHeight * float(raster_ds.RasterYSize))
                    print("Xorigin = " + str.format('{0:.20f}', xOrigin))
                    print("Yorigin = " + str.format('{0:.20f}', yOrigin))
                    print("Xend = " + str.format('{0:.20f}', xEnd))
                    print("Yend = " + str(yEnd))
                    print("Col = " + str(raster_ds.RasterXSize))
                    print("Row = " + str(raster_ds.RasterYSize))

                    sourceSR = layer.GetSpatialRef()
                    targetSR = osr.SpatialReference()
                    targetSR.ImportFromWkt(raster_ds.GetProjectionRef())
                    sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                    targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                    coordTrans = osr.CoordinateTransformation(sourceSR, targetSR)

                    if use_features == 'Yes':
                        layer.ResetReading()
                        feature = layer.GetNextFeature()
                        while feature:

                            feat_geom = feature.GetGeometryRef()  # .Clone()
                            if feat_geom.GetArea() == 0.0:
                                log.send_warning("Geometry " + str(feature.GetFID()) + ' is invalid')
                                feature = layer.GetNextFeature()
                                continue

                            if sourceSR.ExportToProj4() != targetSR.ExportToProj4():
                                feat_geom.Transform(coordTrans)

                            ULX, LRX, LRY, ULY = feat_geom.GetEnvelope()

                            print('FEATURE =' + str(feature.GetFID()))

                            if ULY < yEnd or ULX > xEnd or LRX < xOrigin or LRY > yOrigin:
                                # log.send_message('None intersection found. ')
                                feature = layer.GetNextFeature()
                                continue

                            if (ULX < xOrigin):
                                ULX = xOrigin
                                # print "X out on W"
                            if (ULY > yOrigin):
                                ULY = yOrigin
                                # print "Y out"
                            if (LRX > xEnd):
                                LRX = xEnd  # bit of overflow in case of rounding effect: ignore by gdal if bigger
                                # print "X out on E"
                            if (LRY < yEnd):
                                LRY = yEnd  # bit of overflow in case of rounding effect: ignore by gdal if bigger
                            #  Recompute BBOX coordinate of IMG using shp extent
                            #  in gdal2.1 -projwin shift img
                            # print 'ULX ULY point'
                            px = math.floor((float(ULX) - float(xOrigin)) / float(pixelWidth))
                            py = math.floor((float(ULY) - float(yOrigin)) / float(pixelHeight))
                            px1 = math.ceil((float(LRX) - float(xOrigin)) / float(pixelWidth))
                            py1 = math.ceil((float(LRY) - float(yOrigin)) / float(pixelHeight))
                            print('Pixels')
                            print(px, py, px1, py1)

                            ULX = xOrigin + px * pixelWidth
                            ULY = yOrigin + py * (pixelHeight)
                            LRX = xOrigin + px1 * pixelWidth
                            LRY = yOrigin + py1 * (pixelHeight)

                            print(ULX, ULY, LRX, LRY)
                            # avoid creating double CALIBRATED_data in case file has no direcory
                            # if (not os.path.exists(os.path.join(outdir,dirname))):
                            #     log.send_message('Creating out folder in : '+outdir+dirname)
                            #     os.mkdir(outdir+dirname)

                            # outraster = outdir+dirname+"/"+shpbasename+"_"+str(tmpname)+'_'+inraster
                            outraster = os.path.join(outdir, shpbasename + "_" + str(
                                feature.GetFID()) + '_' + inraster).replace('\\', '/')

                            if ((overwrite_clip == 'Yes') and (os.path.exists(outraster))):
                                os.remove(outraster)
                                if (os.path.exists(outraster + '.aux.xml')):
                                    os.remove(outraster + '.aux.xml')

                            if (os.path.exists(outraster)):
                                log.send_message("Output exists, skip " + outraster)
                            else:
                                # subprocess.call(['gdalwarp', img, outraster+'_tmp','-overwrite','-multi','-co', 'TILED=YES', '-co', 'COMPRESS=LZW', '-co', 'BIGTIFF=IF_SAFER','-wo','CUTLINE_ALL_TOUCHED=YES','-q','-cutline', shp,'-cwhere', "'ClipID'= %s" % tmpname,'-te',str.format('{0:.25f}',ULX),str.format('{0:.25f}',LRY),str.format('{0:.25f}',LRX),str.format('{0:.25f}',ULY)])
                                subprocess.call(
                                    ['gdalwarp', '-of', 'GTiff', img, outraster + '_tmp', '-overwrite', '-multi',
                                     '-co', 'TILED=YES', '-co', 'COMPRESS=LZW', '-co', 'BIGTIFF=IF_SAFER', '-wo',
                                     'CUTLINE_ALL_TOUCHED=' + touching, '-q', '-cutline', shp, '-cwhere',
                                     "fid=%d" % feature.GetFID(), '-te', str.format('{0:.25f}', ULX),
                                     str.format('{0:.25f}', LRY), str.format('{0:.25f}', LRX),
                                     str.format('{0:.25f}', ULY)])
                                if os.path.exists(outraster + "_tmp"):
                                    res1 = os.system('gdaladdo "' + outraster + '_tmp" 2 4 ')
                                    os.rename(outraster + "_tmp", outraster)
                                    if os.path.exists(outraster + "_tmp.aux.xml"):
                                        os.remove(outraster + "_tmp.aux.xml")
                                    ImageStore.update_file_statistics(outraster)
                                else:
                                    log.send_error("Error clipping files:" + str(outraster))

                            feature = layer.GetNextFeature()

                    if use_features == 'No':

                        ULX, LRX, LRY, ULY = layer.GetExtent()
                        if sourceSR.ExportToProj4() != targetSR.ExportToProj4():
                            print("Reproj")
                            shpExtent = ogr.CreateGeometryFromWkt(
                                "POLYGON ((" + str.format('{0:.25f}', ULX) + ' ' + str.format('{0:.25f}', ULY) + ',' + \
                                str.format('{0:.25f}', LRX) + ' ' + str.format('{0:.25f}', ULY) + ',' + \
                                str.format('{0:.25f}', LRX) + ' ' + str.format('{0:.25f}', LRY) + ',' + \
                                str.format('{0:.25f}', ULX) + ' ' + str.format('{0:.25f}', LRY) + ',' + \
                                str.format('{0:.25f}', ULX) + ' ' + str.format('{0:.25f}', ULY) + "))")
                            shpExtent.Transform(coordTrans)
                            ULX, LRX, LRY, ULY = shpExtent.GetEnvelope()

                        if (ULX < xOrigin):
                            ULX = xOrigin
                            # print "X min out "
                        if (ULY > yOrigin):
                            ULY = yOrigin
                            # print "Y min out"
                        if (LRX > xEnd):
                            LRX = xEnd  # bit of overflow in case of rounding effect: ignore by gdal if bigger
                            # print "X max out"
                        if (LRY < yEnd):
                            LRY = yEnd  # bit of overflow in case of rounding effect: ignore by gdal if bigger
                            # print "Y max  out"

                        #  Recompute BBOX coordinate of IMG using shp extent
                        #  in gdal2.1 -projwin shift img
                        px = math.floor((float(ULX) - float(xOrigin)) / float(pixelWidth))
                        py = math.floor((float(ULY) - float(yOrigin)) / float(pixelHeight))
                        px1 = math.ceil((float(LRX) - float(xOrigin)) / float(pixelWidth))
                        py1 = math.ceil((float(LRY) - float(yOrigin)) / float(pixelHeight))
                        print('Pixels')
                        print(px, py, px1, py1)

                        ULX = xOrigin + px * pixelWidth
                        ULY = yOrigin + py * (pixelHeight)
                        LRX = xOrigin + px1 * pixelWidth
                        LRY = yOrigin + py1 * (pixelHeight)

                        tmpname = shpbasename

                        # if (not os.path.exists(os.path.join(outdir,dirname))):
                        #     log.send_message('Creating out folder in : '+outdir+dirname)
                        #     os.mkdir(outdir+dirname)

                        outraster = os.path.join(outdir, tmpname + '_' + inraster).replace('\\', '/')

                        if ((overwrite_clip == 'Yes') and (os.path.exists(outraster))):
                            os.remove(outraster)
                            if (os.path.exists(outraster + '.aux.xml')):
                                os.remove(outraster + '.aux.xml')

                        if (os.path.exists(outraster)):
                            log.send_warning(outraster + "  <b>Already Processed </b>")
                        else:
                            if os.path.exists(outraster + "_tmp"):
                                os.remove(outraster + "_tmp")
                            subprocess.call(
                                ['gdalwarp', '-of', 'GTiff', img, outraster + '_tmp', '-multi', '-co', 'TILED=YES',
                                 '-co', 'COMPRESS=LZW', '-co', 'BIGTIFF=IF_SAFER', '-wo',
                                 'CUTLINE_ALL_TOUCHED=' + touching, '-cutline', shp, '-te', str.format('{0:.25f}', ULX),
                                 str.format('{0:.25f}', LRY), str.format('{0:.25f}', LRX), str.format('{0:.25f}', ULY)])
                            if os.path.exists(outraster + "_tmp"):
                                print('warp done')
                                res1 = os.system('gdaladdo "' + outraster + '_tmp" 2 4 ')
                                os.rename(outraster + "_tmp", outraster)
                                if os.path.exists(outraster + "_tmp.aux.xml"):
                                    os.remove(outraster + "_tmp.aux.xml")
                                ImageStore.update_file_statistics(outraster)
                            else:
                                log.send_error("Error clipping files:" + str(outraster))
                else:
                    print('skip')

            shp_ds = None

            try:
                if os.path.exists(shpNew):
                    shp_driver.DeleteDataSource(shpNew)
            except:
                pass

        log.send_message("Completed.")
    except Exception as e:
        log.send_error("Error clipping files:" + str(e))
        try:
            if os.path.exists(shpNew):
                shp_driver.DeleteDataSource(shpNew)
        except Exception as e:
            pass

    # time.sleep(1)
    log.close()


if __name__ == "__main__":
    shp_names, img_names, overwrite_clip, use_features, touching = getArgs(sys.argv)
    clip_input_file(shp_names, img_names, overwrite_clip, use_features, touching)
