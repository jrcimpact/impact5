#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import shutil
import gzip
import math
import time
import numpy

# Import local libraries
from __config__ import *
import IMPACT
from Simonets_EO_libs import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal


numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print()
    print('Author: Simonetti Dario for European Commission (GEM unit) 2014')
    print()
    print('Scan input directory for Landsat data to be calibrated')
    print()
    sys.exit(1)

def error(message):
    print()
    print(message)
    print()
    usage()
    sys.exit(1)

def getArgs(args):

    IN = None

    if (len(sys.argv) < 4):
        usage()
    else:
        IN = sys.argv[1]
        outdir = sys.argv[2]
        overwrite = sys.argv[3]


    if (IN is not None and outdir is not None ):
        if (not os.path.exists(IN) or not os.path.exists(outdir)):
            error('The input file "' + IN +'" or "'+ outdir + '" does not exist')
        else:
            return IN, outdir, overwrite
    else:
        usage()

# ----------------------------------------------------------------------------------------------------------------	
# ----------------------------------------------------------------------------------------------------------------
#
#            MAIN CODE : DN to TOA Reflectance  
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------	

def process_rapideye_file(infile, outfile):

    try:
        print("Processing "+infile+"<br>")

        XML_met = infile.replace(".tif","_metadata.xml")

        if (os.path.exists(XML_met)):
            MYMET = XML_met
        else:
                print("Metadata not valid <br>")
                #sys.exit(0)
                return "Metadata not valid"


        MetOBJ = get_Rapideye_metadata_info(MYMET)
        print("sensor:"+str(MetOBJ.sensor)+"<br>")
        print("sunel:"+str(MetOBJ.sunel)+"<br>")
        print("sunazi:"+str(MetOBJ.sunazi)+"<br>")
        print("ESdist:"+str(MetOBJ.ESdistance)+"<br>")

        if (MetOBJ.sensor == '' or MetOBJ.sunel == 0 ):
            return "Metadata not complete <br>"

        #http://www.blackbridge.com/rapideye/upload/RE_Product_Specifications_ENG.pdf

        bESUN = [1997.8,1863.5,1560.4,1395.0,1124.4]

        src_ds = gdal.Open(infile)

        num_bands=src_ds.RasterCount
        if (not num_bands == 5 ) :
            src_ds = None
            return "Input image does not have 6 bands <br>"

        print("--------- Processing -----------------")
        driver = gdal.GetDriverByName("GTiff")

        dst_ds = driver.Create( outfile,src_ds.RasterXSize,src_ds.RasterYSize,num_bands,gdal.GDT_UInt16)  # , options = [ 'COMPRESS=LZW' ] creates bigger images
        dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
        dst_ds.SetProjection(src_ds.GetProjectionRef())
        dst_ds.SetMetadata({'Impact_product_type': 'calibrated','Impact_operation':"DN to TOA Reflectance" ,'Impact_version':IMPACT.get_version()})

        start = time.time()

        for b in range(1,(num_bands+1)):

            band = src_ds.GetRasterBand(b)
            bandval = band.ReadAsArray(0,0,band.XSize,band.YSize).astype(numpy.uint16)
            brefl=(bandval/100.) * math.pi * MetOBJ.ESdistance

            Besun_cosun = bESUN[b-1] * math.cos((math.pi/180.0)*(90.0 - MetOBJ.sunel))
            numpy.divide(brefl,Besun_cosun,brefl)

            brefl[brefl < 0.]=0.
            brefl[brefl > 1.]=1.
            brefl[numpy.equal(bandval,0.)]=0.
            bandval=None
            del bandval

            dst_ds.GetRasterBand(b).WriteArray(brefl*10000)

            brefl=None
            del brefl

        #close roperly the dataset
        dst_ds = None
        src_ds = None
        res = os.system('gdaladdo "'+outfile+'" 2 4 8 ') #-createonly -init 0

        # In case processing in DATA folder
        try:
            shutil.copy(XML_met, os.path.dirname(outfile))
        except:
            pass

        print("Processing time = "+str(time.time() - start))
        return "1"
    except Exception as e:
        print(str(e))
        time.sleep(3)
        return "Generic Error in Calibration"




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------	


# -----------   CLEAN TM FILE -------------- 	
def is_directory_processed_correctly(outdir, list):
    res=True
    for file in list:
        if (not os.path.exists(outdir+file)):
            return False
    return res

def clean_out_folder(outdir):	

    for name in os.listdir(outdir):
        os.remove(outdir+name)

    return "1"

# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


def run_rapideye_processing(IN, outdir, overwrite):
    try:
        if not os.path.exists(IN):
            return "File not available, close processing windows to reload datasets."

        dirname = os.path.dirname(IN).split('/')[-1]
        out_message = 'No RapidEye images to process <br>'
        outName = os.path.join(outdir, dirname, os.path.basename(IN).replace('.tif', '_calrefx10k.tif'))


        if not os.path.exists(outdir+dirname):
            os.mkdir(outdir+dirname)

        if overwrite == 'Yes':
            if os.path.exists(outName):
                os.remove(outName)

        if not os.path.exists(IN):
            return "Input file not valid <br>"
        else:
            # ----- test if output directory contains processed data ------

            if os.path.exists(outName):
                return "<b>Already Processed</b>"

            else:
                out_message=''
                res = process_rapideye_file(IN,outName)
                if res == "1":
                    return "1"
                else:
                    print(str(res))
                    #res = clean_out_folder(outdir+dirname)
                    #os.rmdir(outdir+dirname)
                    return "Error processing file: "+ str(res)

    except Exception as e:
        return str(e)


if __name__ == "__main__":
    IN, outdir, overwrite = getArgs(sys.argv)
    run_rapideye_processing(IN, outdir, overwrite)
