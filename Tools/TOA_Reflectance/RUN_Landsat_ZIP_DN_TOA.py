#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json

# Import local libraries
from __config__ import *
import LogStore
import ImageProcessing
from Process_Landsat_ZIP_DN_TOA import *


def usage():
    print()
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data Pre processing ---> RAW ZIP Landsat data')
    print('RUN_Landsat_ZIP_DN_TOA.py overwrite outdir indir img_names')
    print()
    sys.exit(0)


def getArgs(args):
    infiles = None

    if (len(sys.argv) < 7):
        usage()
    else:
        overwrite = sys.argv[1]
        outdir = sys.argv[2]
        infiles = json.loads(sys.argv[3])
        proc_mode = sys.argv[4]
        date_mode = sys.argv[5]
        proc_opt = sys.argv[6]
        if (infiles is None):
            usage()

        else:
            return overwrite, outdir, infiles, proc_mode, date_mode, proc_opt



# ----------------------------------------------------------------------------------------------------------------	
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":

    # Retrieve arguments
    overwrite, outdir, img_names, proc_mode, date_mode, proc_opt = getArgs(sys.argv)

    # Initialize output log
    log = LogStore.LogProcessing('Landsat GZ to TOA Reflectance', 'landsat_to_toa')
    log.set_input_file(img_names)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Output directory', outdir)
    log.set_parameter('Date format', date_mode)

    if proc_mode == "DN":
        processing_mode = "DN"
    else:
        processing_mode = "DN2TOA"

    if proc_opt == "TOAb":
        processing_opt = "Reflectance in Byte [0-255]"
    else:
        processing_opt = "Reflectance in Float [0-1]"


    log.set_parameter('Processing Mode', processing_mode)
    log.set_parameter('Processing Option', processing_opt)

    tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_landsat")


    ImageProcessing.validPathLengths(img_names, outdir, 10, log)

    try:
        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)


        for IN in img_names:
            try:
                clean_tmp_file(tmp_indir)
                if not os.path.exists(IN):
                    log.send_error('File not available, close processing windows to reload datasets.')
                    continue

                log.send_message("RUN Processing " + str(os.path.basename(IN))+" ...")
                res = process_landsat_file(IN, outdir, overwrite, tmp_indir, log, proc_mode, date_mode, proc_opt)
                clean_tmp_file(tmp_indir)

            except Exception as e:
                log.send_error(str(e))

        try:
            clean_tmp_file(tmp_indir)
            #os.rmdir(tmp_indir)
        except Exception as e:
            log.send_error("Error cleaning tmp folder: "+str(e))

    except Exception as e:
        log.send_error(e)

    log.close()
