#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import shutil
import tarfile
import gzip
import glob
import numpy
import math
import multiprocessing

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal

# Import local libraries
from __config__ import *
import IMPACT
from Simonets_EO_libs import *


def rename_and_zip_file(INfile, out_name):
    try:
        shutil.move(INfile, out_name)
        with open(out_name, 'rb') as f_in, gzip.open(out_name+'.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        f_out.close()
        f_in.close()
        os.remove(out_name)
    except:
        print('File already compressed')




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            MAIN CODE : from zip file to layerstaked
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def process_landsat_file(file, outdir, overwrite, tmp_indir, log, proc_mode, date_mode, proc_opt):

    try:
        print("Processing raw file: " + file)

        if file.endswith('.tar.gz') or file.endswith('.tgz'):
            tar=tarfile.open(file,'r:gz')
        if file.endswith('.tar.bz') or file.endswith('.tbz'):
            tar=tarfile.open(file,'r:bz2')
        if file.endswith('.tar'):
            tar=tarfile.open(file)

        if ('L2SP' in file or file.endswith('SR.tar') or file.endswith('TA.tar') or file.endswith('SR.tar.gz') or file.endswith('TA.tar.gz')) and proc_mode != "DN":
            proc_mode = "DN"
            date_mode = "ORIGINAL"
            log.send_warning("TOA conversion not possible. Image is already in TA or SR. Changing to DN (Layer-stack).")

        tar.extractall(tmp_indir)

        os.chdir(tmp_indir)
        MTL_met=['']
        MET_met=['']
        if proc_mode != "DN":
            MTL_met=glob.glob('*_MTL.txt')
            MET_met=glob.glob('*.met')
            if (len(MTL_met) == 0 and len(MET_met) == 0 ):
                log.send_error("Metadata not valid; TOA conversion not possible. Select DN (Layer-stack).")
                return "Metadata not valid"

            if (os.path.exists(MTL_met[0])):
                MYMET = MTL_met[0]
            else:
                if (os.path.exists(MET_met[0])):
                    MYMET = MET_met[0]
                else:
                    log.send_error("Metadata not valid; TOA conversion not possible. Select DN (Layer-stack).")
                    return "Metadata not valid"

        else:
            MYMET = glob.glob('*.xml')
            print(MYMET)
            print(len(MYMET))
            if len(MYMET) > 0:
                MYMET = MYMET[0]
                if not os.path.exists(MYMET):
                    log.send_error("Product not valid; Not a Reflectance product. Select DN to TOA conversion.")
                    return "Product not valid"
            else:
                log.send_error("Product not valid; Not a Reflectance product. Select DN to TOA conversion.")
                return "Product not valid"

        #---------------------------------------
        # ------  TOA Conversion options -------
        # --------------------------------------

        MetOBJ = get_Landsat_metadata_info(MYMET)

        log.send_message("sensor " + str(MetOBJ.sensor))
        log.send_message("gain " + str(MetOBJ.gain))
        log.send_message("bias " + str(MetOBJ.bias))
        log.send_message("sunel " + str(MetOBJ.sunel))
        log.send_message("sunazi " + str(MetOBJ.sunazi))
        log.send_message("ESdist " + str(MetOBJ.ESdistance))
        log.send_message("YYYY " + str(MetOBJ.yyyy))
        log.send_message("MM " + str(MetOBJ.mm))
        log.send_message("DD " + str(MetOBJ.dd))
        log.send_message("PATH " + str(MetOBJ.path))
        log.send_message("ROW " + str(MetOBJ.row))

        if MetOBJ.sensor == '' and proc_mode == "DN" :
            log.send_error("Metadata not complete, SENSOR not detected. Is  --Keep DN values-- the correct option for this dataset? ")
            return "Metadata not complete "

        if (len(MetOBJ.gain) == 0 or MetOBJ.sunel == 0 or MetOBJ.sunazi == 0) and  proc_mode != "DN" :
            log.send_error("Metadata not complete. Use Mosaic Tool to layer-stack desired bands." )
            return "Metadata not complete "

        if MetOBJ.sensor == 'oli':
            bESUN = [2067.000,1893.000,1603.000,972.6,245.0,79.72]
        if MetOBJ.sensor == 'etm':
            bESUN = [1969.000,1840.000,1551.000,1044.000,225.700,82.070]
        if MetOBJ.sensor == 'tm':
            bESUN = [1957.000,1826.000,1554.000,1036.000,215.000,80.670]    # similar

        YEAR = MetOBJ.yyyy
        MONTH = MetOBJ.mm
        DAY = MetOBJ.dd

        PATH = MetOBJ.path
        ROW = MetOBJ.row

        if date_mode != "ORIGINAL":
            if len(MetOBJ.sensor) == 0 or len(PATH) == 0 or len(ROW) == 0  or len(YEAR) == 0:
                log.send_error("Metadata File not supported; TOA conversion not possible. Select DN to layer-stack bands." )
                return "Metadata File not supported"

        if date_mode == "DDMMYYYY":
            out_basename = MetOBJ.sensor + '_' + PATH + '-' + ROW + '_' + DAY + MONTH + YEAR
        if date_mode == "YYYYMMDD":
                out_basename = MetOBJ.sensor + '_' + PATH + '-' + ROW + '_' + YEAR + MONTH + DAY
        if date_mode == "ORIGINAL":
            out_basename = os.path.basename(file).replace('.tar.gz','').replace('.tar.bz','').replace('.tar','')

        raw_basename = os.path.basename(file).replace('.tar.gz', '').replace('.tar.bz', '').replace('.tar', '')
        out_name_fullPath = os.path.join(outdir, out_basename, out_basename + '.tif')
        out_name = out_basename + '.tif'
        print(out_name)
        print('-------------------------')
        if proc_opt == "TOAb":
            out_name_fullPath = out_name_fullPath.replace('.tif','_calrefbyt.tif')
            out_name = out_name.replace('.tif','_calrefbyt.tif')
        if proc_opt == "TOAf":
            out_name_fullPath = out_name_fullPath.replace('.tif','_calrefflt.tif')
            out_name = out_name.replace('.tif', '_calrefflt.tif')
        if proc_opt == "TOAi":
            out_name_fullPath = out_name_fullPath.replace('.tif','_calrefint.tif')
            out_name = out_name.replace('.tif', '_calrefint.tif')
        print(proc_opt)
        print("Processing: " + out_name)

        if (overwrite == 'Yes'):
            if os.path.exists( out_name_fullPath):
                os.remove(out_name_fullPath)
            if os.path.exists(out_name_fullPath + '.aux.xml'):
                os.remove(out_name_fullPath + '.aux.xml')

        if overwrite == 'No' and os.path.exists(out_name_fullPath):
            log.send_warning("File Already processed")
            os.chdir('..')
            return 'Already Processed'


        # ---------------------------------------------------------------
        #                   Start processing
        # ---------------------------------------------------------------

        # ETM gap mask are not processed, add rules
        try:
            #os.chdir('.//tmp//')
            # set all lowercase
            for files in glob.glob('*.TIF'):
                shutil.move(files,files.lower())
                #print(files.lower())
            for files in glob.glob('*.tif'):
                shutil.move(files,files.lower())
                #print(files.lower())
            # execute multiprocessing
            jobs = []
            # identify collection in order to apply the rescaling factor
            collection = file.split('_')
            if len(collection) > 2:
                collection = collection[-2]

            if MetOBJ.sensor == "oli":
                Impact_bands = 'B2,B3,B4,B5,B6,B7'
                if ('_sr_band2' in files or 'srb2' in files or 'sr_b2' in files or 'tab2' in files) and proc_mode != "DN":
                    log.send_warning("Detected OLI/TIRS product Surface Reflectance or TOA Reflectance, changing to DN acquisition")
                    proc_mode = "DN"

                    # out_basename = os.path.basename(file).replace('.tar.gz', '').replace('.tar.bz', '').replace('.tar','')
                    # out_name_fullPath = os.path.join(outdir, out_basename, out_basename + '.tif')
                    # out_name = out_basename + '.tif'

                for ID in ['_qa', '_bqa', '_cfmask', '_cfmask_conf', '_ipflag', '_cloud','_lineageqa','_pixelqa','_radsatqa','_sraerosolqa', '_sea4', '_sez4','_soa4','_soz4']:
                    for INfile in glob.glob('*'+ID+'.tif'):
                        out = out_basename+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()

                for ID in ['_qa_pixel','qa_aerosol', '_sr_cloud_qa','_cloud_qa', '_sr_atmos_opacity', '_qa_radsat', '_saa', '_sza', '_vaa', '_vza', '_atran', '_cdist', '_drad', '_emis', '_emsd', '_trad', '_urad']:
                    for INfile in glob.glob('*'+ID+'.tif'):
                        out = out_basename+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()

                for ID in ['1','8','9','10','11']:
                    for INfile in glob.glob('*_b'+ID+'.tif'):
                        out = out_basename+'_band'+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()
                    for INfile in glob.glob('*sr_band'+ID+'.tif'):
                        out = out_basename+'_sr_band'+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()
                    for INfile in glob.glob('*tab'+ID+'.tif'):
                        out = out_basename+'_tab'+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()
                    for INfile in glob.glob('*srb' + ID + '.tif'):
                        out = out_basename + '_srb' + ID + '.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()

            # find TH band
            if MetOBJ.sensor == "tm" or MetOBJ.sensor == "etm" :
                Impact_bands = 'B1,B2,B3,B4,B5,B7'
                if ('_sr_band2' in files or 'srb2' in files or 'sr_b2' in files or 'tab2' in files) and proc_mode != "DN":
                    log.send_warning("Detected TM/ETM product Surface Reflectance, changing from TOA conversion to DN")
                    proc_mode = "DN"
                    # out_basename = os.path.basename(file).replace('.tar.gz', '').replace('.tar.bz', '').replace('.tar','')
                    # out_name_fullPath = os.path.join(outdir, out_basename, out_basename + '.tif')
                    # out_name = out_basename + '.tif'


                for ID in ['_qa', '_qa_pixel', '_bqa', '_cfmask', '_cfmask_conf', '_ipflag', '_sr_cloud_qa', '_cloud','_cloud_qa','_lineageqa','_pixelqa','_radsatqa', '_sr_atmos_opacity', '_qa_radsat','_sraerosolqa', '_sea4', '_sez4','_soa4','_soz4','_saa', '_sza', '_vaa', '_vza', '_atran','_cdist','_drad','_emis','_emsd','_trad','_urad']:
                    for INfile in glob.glob('*'+ID+'.tif'):
                        out = out_basename+ID+'.tif'
                        j = multiprocessing.Process(target=rename_and_zip_file, args=(INfile, out,))
                        jobs.append(j)
                        j.start()


                for THfiles in glob.glob('*_b6*1.tif'):
                    out = out_basename+'_band61.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(THfiles, out,))
                    jobs.append(j)
                    j.start()

                for THfiles in glob.glob('*_b6*2.tif'):
                    out = out_basename+'_band62.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(THfiles, out,))
                    jobs.append(j)
                    j.start()

                for THfiles in glob.glob('*_??6.tif'):
                    out = out_basename+'_band60.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(THfiles, out,))
                    jobs.append(j)
                    j.start()

                for THfiles in glob.glob('*_b60.tif'):
                    out = out_basename+'_band60.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(THfiles, out,))
                    jobs.append(j)
                    j.start()

                for THfiles in glob.glob('*_b6.tif'):
                    out = out_basename+'_band60.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(THfiles, out,))
                    jobs.append(j)
                    j.start()

                # find PAN band
                for PANfiles in glob.glob('*_b8*.tif'):
                    out = out_basename+'_band80.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(PANfiles, out,))
                    jobs.append(j)
                    j.start()

                # find new 2017 QA band for tme and etm  band
                for BQA in glob.glob('*_bqa.tif'):
                    out = out_basename+'_bqa.tif'
                    j = multiprocessing.Process(target=rename_and_zip_file, args=(BQA, out,))
                    jobs.append(j)
                    j.start()

            #------ rename metadata and extra files -------
            for ID in ['_GCP.txt', '_MTL.txt', '.met', '_VER.txt', '_VER.jpg', '_RT_ANG.txt','_ANG.txt','.xml','MD5.txt']:
                for INfile in glob.glob('*'+ID):
                    shutil.move(INfile,out_basename+ID)


            # Wait for all process to finish (ZIP)
            for j in jobs:
                j.join()


            # ----------  LAYERSTACK remaining 6 TIF bands  ------------
            print('searching for tif bands 2 stack ')
            tif2del=glob.glob('*.tif')   # scan fro TIF to delete before creating OUT tif
            print(tif2del)
            src_ds = gdal.Open(tif2del[0])
            driver = gdal.GetDriverByName("GTiff")


            if proc_opt == "TOAb":
                outDataType = gdal.GDT_Byte
            if proc_opt == "TOAf":
                outDataType = gdal.GDT_Float32
            if proc_opt == "TOAi":
                outDataType = gdal.GDT_UInt16

            if proc_mode == "DN" and proc_opt != "TOAi" and collection not in ['01', '02']:
                outDataType = gdal.GDT_UInt16
                out_name = out_name.replace('_calrefbyt.tif','_calrefint.tif').replace('_calrefflt.tif','_calrefint.tif')
                log.send_warning(
                    "Detected RAW Uint16 input changing to UInt16 type")
                proc_mode = "DN"


            if proc_mode == "DN" and proc_opt != "TOAf" and collection in ['01', '02']:
                log.send_warning(
                    "Detected Surface Reflectance or TOA Reflectance, changing to Float type")
                scale_factor = '0.0001'
                if collection == '02':
                    scale_factor = '0.0000275 + -0.2'
                log.send_warning(
                    "Rescaling factor used to convert raw values:"+scale_factor)

                proc_mode = "DN"
                outDataType = gdal.GDT_Float32
                out_name = out_name.replace('_calrefbyt.tif', '_calrefflt.tif').replace('_calrefint.tif', '_calrefflt.tif')

            print('Creating file: ', out_name)
            dst_ds = driver.Create(out_name, src_ds.RasterXSize, src_ds.RasterYSize, len(tif2del), outDataType, options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER','TILED=YES' ] )  #GEE
            dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
            dst_ds.SetProjection(src_ds.GetProjectionRef())
            if proc_mode == "DN":
                dst_ds.SetMetadata({'Impact_product_type': 'layerstack', 'Impact_operation': "Input values","Impact_bands": Impact_bands,
                                    'Impact_version': IMPACT.get_version()})
            else:
                dst_ds.SetMetadata({'Impact_product_type': 'calibrated', 'Impact_operation': "DN to TOA Reflectance","Impact_bands": Impact_bands,
                                    'Impact_version': IMPACT.get_version()})

            src_ds = None
            del src_ds
            print("Format= " + gdal.GetDataTypeName(outDataType) + "<br>")
            b=1
            for infile in tif2del:
                log.send_message("Processing band: "+str(b))
                src_ds = gdal.Open(infile)
                band = src_ds.GetRasterBand(1)
                if proc_mode == "DN":
                    bandval = band.ReadAsArray(0, 0, band.XSize, band.YSize).astype(numpy.float32)
                    mask = bandval == 0
                    if collection == '01':
                        bandval *= 0.0001
                        bandval[bandval < 0.] = 0.
                        bandval[bandval > 1.] = 1.
                        bandval[mask] = 0.
                    if collection == '02':
                        bandval *= 0.0000275
                        bandval -= 0.2
                        bandval[bandval < 0.] = 0.
                        bandval[bandval > 1.] = 1.
                        bandval[mask] = 0.
                    dst_ds.GetRasterBand(b).WriteArray(bandval)
                else:

                    bandval = band.ReadAsArray(0,0,band.XSize,band.YSize).astype(numpy.float32)
                    ESdist = (MetOBJ.ESdistance* MetOBJ.ESdistance)
                    Besun_cosun = bESUN[b-1] * math.cos((math.pi/180.0)*(90.0 - MetOBJ.sunel))    #GEE
                    mask = bandval == 0

                    bandval *= MetOBJ.gain[b-1]
                    bandval += MetOBJ.bias[b-1]
                    bandval *= (math.pi * ESdist)/Besun_cosun

                    bandval[bandval < 0.]=0.
                    bandval[bandval > 1.]=1.
                    bandval[mask]= 0.
                    if proc_opt == "TOAb":
                        bandval *= 255.
                    dst_ds.GetRasterBand(b).WriteArray(bandval)

                brefl=None
                del brefl
                src_ds = None
                del src_ds
                band=None
                del band
                bandval=None
                del bandval
                b=b+1


            dst_ds = None
            # ---- rename and add compression since create LZW option does not work properly
            shutil.move(out_name, 'tmp_'+out_name)
            res1 = os.system('gdal_translate tmp_'+ out_name +' ' + out_name + ' -co COMPRESS=LZW -co BIGTIFF=IF_SAFER -co TILED=YES')

            res1 = os.system('gdal_translate -scale '+out_name+' '+out_basename+'.jpg -outsize 30% 30% -b 5 -b 4 -b 2 -ot Byte -of JPEG -co WORLDFILE=ON  ')
            shutil.move(out_basename+'.wld',out_basename+'.jpw')

            for tmpfile in glob.glob("tmp*.tif"):
                #print "TMP_FILE "+ tmpfile
                os.unlink(tmpfile)

            for tmpfile in tif2del :
                os.remove(tmpfile)

            outdir = os.path.dirname(out_name_fullPath)
            if not os.path.exists(outdir):
                os.mkdir(outdir)

        # ------  MOVE FILE TO DATA DIR  ----------
            for INfile in glob.glob('*.*'):
                    shutil.move(INfile,outdir+'//'+INfile)
            os.chdir('..//')       # get out ftom TMP directory
        except Exception as e :
            log.send_error("Error processing :"+str(e))
            os.chdir('..//')       # get out ftom TMP directory in case of fail
            return "Error processing "+str(e)

        log.send_message("Completed.")
        return True

    except Exception as e :
        log.send_error("Error processing "+str(e))
        return False




# -----------   CLEAN TM FILE --------------
def create_tmp_file(tmpindir):
    if (not os.path.exists(tmpindir)):
        os.mkdir(tmpindir)
    return "1"

def clean_tmp_file(tmpindir):
    for root, dirs, files in os.walk(tmpindir, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    return "1"

