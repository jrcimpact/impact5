#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import re
import json

# Import local libraries
from __config__ import *
import LogStore
import Rapideye_DN_to_TOA_Reflectance
from Simonets_EO_libs import *
from Simonets_PROC_libs import get_image_medians_from_mask
import ImageProcessing


def usage():
    print
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data Calibration ---> DN to TOA Reflectance')
    print('RUN_Rapideye_DN_to_TOA_Reflectance.py overwrite output_directory images')
    print()
    sys.exit(0)


def getArgs(args):
    infiles = None
    if len(sys.argv) < 4:
        usage()
    else:
        overwrite = sys.argv[1]
        outdir = sys.argv[2]
        infiles = json.loads(sys.argv[3])
        if infiles is None:
            usage()
        else:
            return overwrite, outdir, infiles


# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    overwrite, outdir, img_names = getArgs(sys.argv)

    # initialize output log
    log = LogStore.LogProcessing('RapidEye Dn 2 TOA Reflectance', 'rapideye_to_toa')
    log.set_input_file(img_names)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Output directory', outdir)

    ImageProcessing.validPathLengths(img_names, outdir,15, log)

    try:
        for IN in img_names:
            fname = os.path.basename(IN)

            if bool(re.search('RE[1-9]', fname)):
                log.send_message('RAPIDEYE: processing '+IN)
                try:
                    res = Rapideye_DN_to_TOA_Reflectance.run_rapideye_processing(IN, outdir, overwrite)
                    if res == '1':
                        log.send_message("Completed")
                    else:
                        if (res == '<b>Already Processed</b>'):
                            log.send_warning(res)
                        else:
                            log.send_error(res)
                except:
                    log.send_error('File not available, close processing windows to reload datasets.')
            else:
                log.send_error(str(fname)+" not a valid RapidEye (do not contain RE[1-9])")

    except Exception as e:
        log.send_error(e)

    log.close()

