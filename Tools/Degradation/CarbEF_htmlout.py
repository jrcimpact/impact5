# -*- coding: utf-8 -*-
# this module creates an xlsx output

import os.path
import os
import time

class carbefHTML():

    #filename=None
    #log=None
    #lang='EN'
    styleCode="""
        <style>
        @page {size: A4; margin: 2.5cm; padding:0}
        html, body {width: 210mm;height: 297mm;}
  
        body{font-family: Arial, sans-serif; font-size:14px; text-align: justify; text-justify: inter-word; width: 17cm;margin-top:0px;counter-reset:section}
        body:before{counter-reset: section}
        h1{font-family: Arial, sans-serif; font-size:22px; font-weight:bold; margin-top:22px}
        h1.mainTitle{font-size:28px;}
        h1:before{
            counter-increment: section;
            content: counter(section) " - ";
        }
        h1.mainTitle:before{
            counter-reset:section;
            content:"";
        }
        h1.results:before{
            counter-reset:section;
            content:"";
        }
        h2{font-family: Arial, sans-serif; font-size:18px; font-weight:bold; margin-top:6px; margin-bottom:0}
        h3{font-family: Arial, sans-serif; font-size:14px; font-weight:bold; margin-bottom:0; margin-top:6px; padding:0}
        p{font-family: Arial, sans-serif; font-size:14px; margin-left:40px; margin-top:0; padding:0}
        ul {list-style-type: none;}
        ul li:before {
            content: '\\2014';
            position: absolute;
            margin-left: -20px;
            }
        table, th, tr, td {
            border: 0px solid black;
            border-collapse: collapse;
            text-align: left;
            height:1.1em;
            }
        td{text-align:right; max-width:25%;padding-left:10px}
        th {border-bottom:1px solid black; text-align:center; max-width:30%;padding-left:10px}
        tr.line {border-bottom:1px solid black;}
        table.center{margin-left:auto; margin-right:auto}
        span.toFill{color:red; font-weight:bold}
        div.result{border: 1px solid black; }
    </style>
    """
    trStrings={'Title':{'EN':'<h1 class="mainTitle">Synthesis report: deforested and degraded areas and associated CO<sub>2</sub> emissions</h1>',
                        'FR':u"<h1 class='mainTitle'>Rapport de synthèse&nbsp;: surfaces déboisées et dégradées et émissions de CO<sub>2</sub> associées</h1>"},
               'ProcessingDoneOn':{'EN':'Processing done on {}','FR':u"Traitements effectués le {}"},
               'Title1_GeographicLocation':{'EN':'<h2>Geographic Location</h2>',
                                            'FR':u"<h2>Localisation géographique</h2>"},
               'Country':{'EN':"Country: <span class='toFill'>[Country name]</span>",
                          'FR':u"Pays: <span class='toFill'>[Nom du pays]</span>"},
               'GeographicWindow':{'EN':'Geographic Window:',
                                   'FR':u"Fenêtre géographique&nbsp:"},
               'from':{'EN':'from','FR':u"de"},
               'to':{'EN':'to', 'FR':u"à"},
               'years':{'EN':'years','FR':u"années"},
               'ZoneOfInterest':{'EN':'Zone of interest: <span class="toFill">[Zone of interest]</span>',
                                 'FR':u"Zone d'intérêt&nbsp;: <span class='toFill'>[Zone d'intérêt]</span>"},
               'PeriodOfAnalysis':{'EN':"<h2>Periods of analysis</h2>",'FR':u"<h2>Périodes de l'analyse</h2>"},
               'Period':{"EN":"Period","FR":u"Période"},
               'forestDefinition':{"EN":"<h2>Forest definition</h2>",
                                   "FR":u"<h2>Définition de la forêt</h2>"},
               'MinimumForestUnit':{"EN":"Minimum Forest Unit (MFU):","FR":u"Unité Forestière Minimale (UFM) :"},
               'TreeCoverThreshold':{"EN":"Tree cover threshold:","FR":u"Couverture arborée minimale&nbsp;:"},
               'TreeCoverLossMap':{'EN':'<h2>Tree cover loss map</h2>', 'FR':u'<h2>Carte des pertes du couvert arboré</h2>'},
               'TreeCoverLossMapText':{'EN':'The MFU grid of forest changes is derived from the following map of tree cover loss:',
                                       'FR':u"La grille des UFM représentant les changements du couvert forestier est dérivée de la carte des pertes de la couverture arborée suivante&nbsp;:"},
               'DisagShapefile':{'EN':'<h2>Landcover</h2>','FR':u"<h2>Couverture du sol</h2>"},
               'DisagShapefileText':{'EN':'Data are desaggregated using a lancover file (shapefile).','FR':u"Les données sont désagrégées au moyen d'un fichier de couverture du sol (shapefile)."},
               'ExceptionfileText':{'EN':u"Exception are defined with a raster file.", 'FR':u"Les exceptions sont définies au moyen d'un fichier raster."},
               'File':{'EN':'File:','FR':u"Fichier :"},
               'Source':{'EN':'Source: <span class="toFill">[Please describe source]</span>','FR':u'Source : <span class="toFill">[Veuillez décrire la source]</span>'},
               'EmissionFactors':{'EN':'<h2>Emissions factors</h2>','FR':u"<h2>Facteur d'emission</h2>"},
               'EmissionFactorsMapText': {'EN': 'The emission factors are calculated from the carbon stock indicated in the following map:',
                                       'FR': u"Les facteurs d'émission sont calculés à partir des valeurs de stock de carbone de la carte suivante&nbsp;:"},
               'EmissionFactorsCteText':{'EN':'The emission factor is a constant equal to {:.2f} tC/ha.',
                                         'FR':u"Le facteur d'emission est une constante égale à {:.2f} tC/ha."},
               'EmissionFactorsOptionA':{'EN':u'For calculating the degradation emissions in option A, a proportion of {:.1f}% of the carbon stock is used. Option B sums the carbon loss for each pixel having lost its tree cover, emissions for degraded and deforested MFU are computed the same way.',
                                         'FR':u"Pour le calcul des émissions provenant de la dégradation dans l'option A, une proportion de {:.1f}% du stock de carbone est utilisée. L'option B additionne les pertes de carbone de chaque pixel ayant perdu son couvert arboré, les emissions pour les UFM degradées et deforestées sont calculées de la même manière."},
               'Expert':{'EN':'<h2>Expert</h2><span class="toFill">Please insert expert name</span>',
                         'FR':u"<h2>Expert</h2><span class='toFill'>Veuillez insérer le nom de l'expert</span>"},
               'ResultsTitle':{'EN':'<h1 class="results">Results</h1>','FR':u"<h1 class='results'>Résultats</h1>"},
               'ResultsText': {'EN': 'Reported values are rounded up to integer. Results are also available in an Excel file.',
                               'FR': u"Les valeurs rapportées sont arrondies à l'entier supérieur. Les résultats sont également disponibles dans un fichier Excel."},
               'ResultsTitle1':{"EN":"<h1>Reporting areas and emissions of deforestation and forest degradation</h1>",
                                "FR":u"<h1>Déforestation et dégradation forestière&nbsp;: aires et émissions</h1>"},
               'Results1subText1':{'EN':'The reporting of areas affected by deforestation and forest degradation and their associated CO<sub>2</sub> emissions is done based on the classification of the grid of minimum forest units (MFU). Only the MFUs classified as deforested or degraded are considered for the reporting. The area of deforested or degraded MFUs is available in the Excel version of the report.',
                                   'FR':u"Le calcul des surfaces touchées par la déforestation et la dégradation des forêts et de leurs émissions de CO<sub>2</sub> associées est basée sur la classification de la grille des unités forestières minimales (UFM). Seules les UFM classées comme déboisées ou dégradées sont prises en compte dans le calcul. Les surfaces des UFM déboisées ou dégradées sont disponibles dans la version Excel du rapport."},
               'Results1subText2':{'EN':'Two options are available to the users. Option A considers the total forest cover of the MFUs while option B uses only the pixels that are detected as tree cover loss in deforested or degraded MFUs. The choice between option A and B is left to the user, according the requirements of the study and of the quality of the tree cover loss data at his/her disposal. In case of tree cover loss maps that are prone to miss small scale disturbances of tree cover, the user is advised to choose option A.',
                                   'FR':u"Deux options sont données aux utilisateurs. L'option A considère la couverture forestière totale des UFM tandis que l'option B n'utilise que les pixels détectés comme ayant subi une perte de leur couvert arboré. Le choix entre les options A et B est laissé à l'utilisateur, en fonction des exigences de l'étude et de la qualité des données de pertes de la couverture arborée mises à sa disposition. Dans le cas de cartes de pertes de couvert forestier qui sont susceptibles de passer de rater systématiquement des perturbations à petite échelle de la couverture arborée, il est conseillé à l'utilisateur de choisir l'option A."},
               'AreaOfForestTitle':{'EN':'<h2 class="results">Area of forest at the start of the analysis</h2>',
                                    'FR':u"<h2 class='results'>Superficie de la forêt au début de l'analyse</h2>"},
               'AreaOfForestText':{'EN':'Total area of forest (MFUs area are adjusted for non-forest presence) at the start of the period 1 ({}): {} ha',
                                   'FR':u"Superficie totale de la forêt (la superficie des UFM est ajustée pour la présence de couvert non-arboré) au début de la période 1 ({}): {} ha"},
               'OptionATitle':{'EN':'<h2 class="results">OPTION A &ndash; Minimum Forest Unit</h2>',
                               'FR':u"<h2 class='results'>OPTION A &ndash; Unité Forestière Minimale </h2>"},
               'OptionAText':{'EN':'In option A, the areas and emissions are calculated according to the entire tree cover content of the MFU (the MFU area is adjusted for non-tree cover presence). For deforestation, the entire carbon content of the MFUs is converted in CO<sub>2</sub> emissions. For forest degradation, a proportion of the carbon content of the MFUs is converted in CO2 emissions. The proportion is indicated by the user as the "Proportion (%) of carbon stock emitted by degradation". For option A, a proportion of {:.1f}% of the MFU carbon stock is used for calculating the forest degradation emission factors.',
                              'FR':u"Dans l'option A, les superficies et les émissions sont calculées en fonction de la totalité de la couverture arborée de l'UFM (la zone de l'UFM est ajustée pour la présence de non-couvert arboré). Pour la déforestation, toute la teneur en carbone des UFM est convertie en émissions de CO2. Pour la dégradation des forêts, une partie de la teneur en carbone des UFM est convertie en émissions de CO2. La proportion est indiquée par l'utilisateur comme la \"Proportion (%) de stock de carbone émis par la dégradation\". Pour l'option A, une proportion de {:.1f}% du stock de carbone de l'UFM est utilisée pour calculer les facteurs d'émission de la dégradation forestière."},
               'OptionBTitle':{'EN':'<h2 class="results">Option B &ndash; Pixels of tree cover loss</h2>',
                               'FR':u"<h2 class='results'>Option B &ndash; Pixels de pertes du couvert arboré</h2>"},
               'OptionBText':{'EN':'In option B, the areas and the emissions are calculated according to count of tree cover pixels loss in deforested or degraded MFUs. The emissions are calculated in the same way for deforestation and degradation. In both case, only the carbon content present in the pixels of tree cover loss is converted in CO<sub>2</sub> emissions.',
                              'FR':u"Dans l'option B, les superficies et les émissions sont calculées en fonction du nombre de pixels identifiés comme pertes du couvert arboré dans les UFM déboisées ou dégradées. Les émissions sont calculées de la même manière pour la déforestation et la dégradation. Dans les deux cas, seule la teneur en carbone présente dans les pixels de perte est convertie en émissions de CO<sub>2</sub>."},
               'ResultTitle2':{'EN':'<h1>Exceptions: area excluded from the analysis</h1>',
                               'FR':u"<h1>Exceptions&nbsp;: zones exclues de l'analyse</h1>"},
               'Result2Text':{'EN':'The following table indicates the area chosen by the user to be excluded from the analysis above. The user has to indicate the period (1 or 2) to which each exception refers to. The total biomass in tC of the area is reported, not its value in equivalent CO<sub>2</sub>.',
                              'FR':u"Le tableau suivant indique la superficie des zones choisies par l'utilisateur pour être exclue de l'analyse ci-dessus. L'utilisateur doit indiquer la période (1 ou 2) à laquelle chaque exception se réfère. La biomasse totale en tC de la zone est rapportée, pas sa valeur en équivalent CO<sub>2</sub>."},
               'ResultTitle3':{'EN':'<h1>Forest changes per landuse strata</h1>',
                               'FR':u"<h1>Changements du couvert forestier par classes d'utilisation des terres</h1>"},
               'Result3Text':{'EN':'The area and emissions of deforestation and forest degradation disaggregated by land use classes can be found in the Excel file.',
                              'FR':u"Les superficies déboisées et dégradées avec leurs émissions ventilées par classes d'utilisation des terres peuvent être trouvées dans le fichier Excel."},
               'NoException':{'EN':'No exception map was processed', 'FR':u"Aucune carte d'exception n'a été traitée."},
               'NoLanduse':{'EN':'No shapefile defined to disaggregate the areas and emissions by land use classes.',
                            'FR':u"Pas de shapefile défini pour ventiler les aires et les émissions par classes d'occupation du sol."},
               'noIntersectionExceptionLossMap':{'EN':'The exception map does not cover the Tree cover loss map, no exeption case found.',
                                                 'FR':u"La carte d'exception ne recouvre pas la carte de pertes de couvert arboré, aucun cas d'exception rencontré."}
    }
    #dataStore = []

    def __init__(self, fname, logObj=None, lang='EN'):
        self.filename=fname
        self.log=logObj
        self.lang=lang
        self.dataStore = []
        # check and open file
        if self.checkFile() == False:
            self.__printErr__('IO error.')
            return None

    def __printErr__(self, msg):
        if self.log is not None:
            self.log.send_error(str(msg))
        else:
            print(msg)

    def __printWarn__(self, msg):
        if self.log is not None:
            self.log.send_warning(str(msg))
        else:
            print(msg)

    def checkFile(self):
        fileExists = None
        if os.path.isfile(self.filename):
            fileExists = True
        if fileExists:
            try:
                self.deleteFile()
            except (OSError, IOError) as  e:
                ipos=0
                while os.path.exists('{}_tmp_{}.html'.format( os.path.splitext(self.filename)[0], ipos )):
                    ipos += 1
                self.filename = '{}_tmp_{}.html'.format( os.path.splitext(self.filename)[0], ipos-1 )
                self.__printWarn__('IOerror while trying to create HTML output file (could not delete existing file, please revise); creating a temporary file {}.'.format(self.filename))
                return True

    def deleteFile(self):
        try:
            os.remove(self.filename)
        except (OSError, IOError) as e:
            self.__printWarn__('Could not delete file {}'.format(self.filename))
            raise

    def addText(self, txt):
        self.dataStore.append(txt)

    def skipLine(self):
        self.dataStore.append('<br/>')

    def toFile(self):
        with open(self.filename, 'w',encoding='utf-8') as thisFile:
            thisFile.write('<!DOCTYPE html>')
            thisFile.write('<html>')
            thisFile.write('<head>')
            thisFile.write('<meta charset="utf-8"/>')
            thisFile.write('<title>CarbEF report</title>')
            thisFile.write(str(self.styleCode))
            thisFile.write('</head>')
            thisFile.write('<body>')
            for ii in self.dataStore:
                thisFile.write(ii)
            thisFile.write('</body>')
            thisFile.write('</html>')

def doHTML(log, outHTMLFile,
           useConversionMapBool, useDisagShpBool, disagShp, inputFile, conversionMapFile,
           LANG, PXPTOT,
           useExceptMapBool, exceptMap, uniqExceptCount, uniqExceptBiom,
           EMDefP1TOT, EMDegP1TOT, EMDefP2TOT, EMDegP2TOT,
           MFUTreeDefP1AreaTOT, MFUTreeDefP2AreaTOT, MFUTreeDegP1AreaTOT, MFUTreeDegP2AreaTOT,
           MFUDegradationEmission1, MFUDegradationEmission2, MFUDeforestationEmission1, MFUDeforestationEmission2,
           psize, startYY1, endYY1, startYY2, endYY2, useTwoPeriodsBool,
           kernel_size, biomass_value, degradationPercent, forestThreshold,
           ULX, ULY, LRX, LRY, toEqCO2):

    deltaYY1 = endYY1-startYY1 + 1
    deltaYY2 = endYY2-startYY2 + 1

    if useTwoPeriodsBool:
        deltaYY2 = endYY2-startYY2 + 1
        deltaYYTotal = endYY2 - startYY1 + 1
    else:
        deltaYY2 = None
        deltaYYTotal = deltaYY1

    sqrps_ha = psize * psize * 1. / 10000.0  # gives the size of a pixel in ha
    bxsize_ha = kernel_size * kernel_size * psize * psize * 1.0 / 10000.0  # box size in ha
    biomass_valuePx = biomass_value * sqrps_ha # biomass_value is read from the interface: value per ha, biomass_valuePx is the biomass factor per pixel
    if useConversionMapBool:
        OptionBEMDEfP1 = round(EMDefP1TOT * toEqCO2)
        OptionBEMDEgP1 = round(EMDegP1TOT * toEqCO2)
        OptionBEMDEfP2 = round(EMDefP2TOT * toEqCO2)
        OptionBEMDEgP2 = round(EMDegP2TOT * toEqCO2)
    else:
        OptionBEMDEfP1 = round(PXPTOT['Deforest_p1'] * toEqCO2 * biomass_valuePx)
        #OptionBEMDEgP1 = round(PXPTOT['Degrad_p1'] * toEqCO2 * degradationPercent * biomass_valuePx / 100.0)
        OptionBEMDEgP1 = round(PXPTOT['Degrad_p1'] * toEqCO2 *  biomass_valuePx )
        OptionBEMDEfP2 = round(PXPTOT['Deforest_p2'] * toEqCO2 * biomass_valuePx)
        #OptionBEMDEgP2 = round(PXPTOT['Degrad_p2'] * toEqCO2 * degradationPercent * biomass_valuePx / 100.0)
        OptionBEMDEgP2 = round(PXPTOT['Degrad_p2'] * toEqCO2 * biomass_valuePx )

    myHtml = carbefHTML(outHTMLFile, logObj=log, lang=LANG)

    myHtml.addText(myHtml.trStrings['Title'][myHtml.lang])
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings['ProcessingDoneOn'][myHtml.lang].format(time.strftime("%d/%m/%Y")))
    myHtml.skipLine()
    myHtml.addText( myHtml.trStrings["Title1_GeographicLocation"][myHtml.lang] )
    myHtml.addText( myHtml.trStrings["Country"][myHtml.lang] )
    myHtml.skipLine()

    myHtml.addText( u"{} longitude: {:.2f}&deg; {} {:.2f}&deg;, latitude: {:.2f}&deg; {} {:.2f}&deg;".format(myHtml.trStrings["GeographicWindow"][myHtml.lang],
                                                                                               ULX,
                                                                                               myHtml.trStrings["to"][myHtml.lang],
                                                                                               LRX,
                                                                                               LRY,
                                                                                               myHtml.trStrings["to"][myHtml.lang],
                                                                                               ULY
                                                                                               ))


    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["ZoneOfInterest"][myHtml.lang])
    myHtml.skipLine()

    myHtml.addText( myHtml.trStrings["PeriodOfAnalysis"][myHtml.lang] )
    myHtml.addText( u'{} 1: {} {} {} {} ({} {})'.format(myHtml.trStrings["Period"][myHtml.lang],
                                                       myHtml.trStrings["from"][myHtml.lang],
                                                       startYY1,
                                                       myHtml.trStrings["to"][myHtml.lang],
                                                       endYY1,
                                                       deltaYY1,
                                                       myHtml.trStrings["years"][myHtml.lang]
                                                       ))
    myHtml.skipLine()
    if useTwoPeriodsBool:
        myHtml.addText( u'{} 2: {} {} {} {} ({} {})'.format(myHtml.trStrings["Period"][myHtml.lang],
                                                      myHtml.trStrings["from"][myHtml.lang],
                                                      startYY2,
                                                      myHtml.trStrings["to"][myHtml.lang],
                                                      endYY2,
                                                      deltaYY2,
                                                      myHtml.trStrings["years"][myHtml.lang]
                                                      ))

    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["forestDefinition"][myHtml.lang])
    myHtml.addText(u"{} {:.2f} ha".format(myHtml.trStrings["MinimumForestUnit"][myHtml.lang], bxsize_ha))
    myHtml.skipLine()
    myHtml.addText(u"{} {}%".format(myHtml.trStrings["TreeCoverThreshold"][myHtml.lang],  forestThreshold*100))
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["TreeCoverLossMap"][myHtml.lang])
    myHtml.addText(myHtml.trStrings["TreeCoverLossMapText"][myHtml.lang])
    myHtml.skipLine()
    myHtml.addText(u"{}<br/>{}".format(myHtml.trStrings["File"][myHtml.lang], inputFile))
    myHtml.skipLine()
    myHtml.addText( myHtml.trStrings["Source"][myHtml.lang])
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["EmissionFactors"][myHtml.lang])
    if useConversionMapBool:
        myHtml.addText(myHtml.trStrings["EmissionFactorsMapText"][myHtml.lang])
        myHtml.skipLine()
        myHtml.addText(u"{}<br/>{}".format(myHtml.trStrings["File"][myHtml.lang], conversionMapFile))
        myHtml.skipLine()
    else:
        myHtml.addText(myHtml.trStrings["EmissionFactorsCteText"][myHtml.lang].format(biomass_value))
        myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["EmissionFactorsOptionA"][myHtml.lang].format(degradationPercent))
    myHtml.skipLine()

    if useDisagShpBool:
        myHtml.addText(myHtml.trStrings['DisagShapefile'][myHtml.lang])
        myHtml.addText(myHtml.trStrings['DisagShapefileText'][myHtml.lang])
        myHtml.addText(u"<br />")
        myHtml.addText(u"{}<br/>{}".format(myHtml.trStrings['File'][myHtml.lang], disagShp[0]))
        myHtml.addText(u"<br/>")
        myHtml.addText(myHtml.trStrings["Source"][myHtml.lang])

    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings["Expert"][myHtml.lang])

    myHtml.addText(myHtml.trStrings["ResultsTitle"][myHtml.lang])
    myHtml.addText(myHtml.trStrings["ResultsText"][myHtml.lang])

    myHtml.addText( myHtml.trStrings['ResultsTitle1'][myHtml.lang])
    myHtml.addText( myHtml.trStrings['Results1subText1'][myHtml.lang])
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings['Results1subText2'][myHtml.lang])

    myHtml.addText(myHtml.trStrings['AreaOfForestTitle'][myHtml.lang])
    myHtml.addText(myHtml.trStrings['AreaOfForestText'][myHtml.lang]
                   .format(startYY1,
                           round( (PXPTOT['Deforest_p1']+PXPTOT['Deforest_p2']+PXPTOT['Degrad_p1']+PXPTOT['Degrad_p2']+PXPTOT['FF_10']+PXPTOT['FF_2x']+PXPTOT['FF_3x']) * sqrps_ha)))

    # Option A
    myHtml.addText(myHtml.trStrings['OptionATitle'][myHtml.lang])
    myHtml.addText(myHtml.trStrings['OptionAText'][myHtml.lang].format(degradationPercent))
    myHtml.skipLine()
    myHtml.skipLine()
    if myHtml.lang=='EN':
        myHtml.addText('<table>')
        myHtml.addText('<tr><th></th><th>Total<br/>(ha)</th><th>Total<br/>(tEqCO<sub>2</sub>)</th><th>Annual rate<br/>(ha/year)</th><th>Annual rate<br/>(tEqCO<sub>2</sub>/year)</th></tr>')
        #Period 1
        myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY1, endYY1))
        myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(MFUTreeDefP1AreaTOT,
                               toEqCO2 * MFUDeforestationEmission1,
                               MFUTreeDefP1AreaTOT/deltaYY1,
                               toEqCO2 * MFUDeforestationEmission1/deltaYY1))
        myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(MFUTreeDegP1AreaTOT,
                               toEqCO2 * MFUDegradationEmission1,
                               MFUTreeDegP1AreaTOT/deltaYY1,
                               toEqCO2 * MFUDegradationEmission1/deltaYY1))
        #Period 2
        if useTwoPeriodsBool:
            myHtml.addText('<tr><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY2, endYY2))
            myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDefP2AreaTOT,
                                   toEqCO2 * MFUDeforestationEmission2,
                                   MFUTreeDefP2AreaTOT/deltaYY2,
                                   toEqCO2 * MFUDeforestationEmission2/deltaYY2))
            myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDegP2AreaTOT,
                                   toEqCO2 * MFUDegradationEmission2,
                                   MFUTreeDegP2AreaTOT/deltaYY2,
                                   toEqCO2 * MFUDegradationEmission2/deltaYY2))
            #Total period
            myHtml.addText('<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr class="line"><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY1, endYY2))
            myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDefP1AreaTOT+MFUTreeDefP2AreaTOT,
                                   toEqCO2 * (MFUDeforestationEmission1+MFUDeforestationEmission2),
                                   (MFUTreeDefP1AreaTOT+MFUTreeDefP2AreaTOT) / deltaYYTotal,
                                   toEqCO2 * (MFUDeforestationEmission1+MFUDeforestationEmission2) / deltaYYTotal))
            myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format( (MFUTreeDegP1AreaTOT + MFUTreeDegP2AreaTOT),
                                    toEqCO2 * (MFUDegradationEmission1 + MFUDegradationEmission2),
                                    (MFUTreeDegP1AreaTOT + MFUTreeDegP2AreaTOT)/ deltaYYTotal,
                                    toEqCO2 * (MFUDegradationEmission1 + MFUDegradationEmission2) / deltaYYTotal))
        myHtml.addText('</table>')
    elif myHtml.lang=='FR':
        myHtml.addText('<table>')
        myHtml.addText('<tr><th></th><th>Total<br/>(ha)</th><th>Total<br/>(tEqCO<sub>2</sub>)</th><th>Annual rate<br/>(ha/year)</th><th>Annual rate<br/>(tEqCO<sub>2</sub>/year)</th></tr>')
        #Period 1
        myHtml.addText(u'<tr><td>Période {}-{}</td></tr>'.format(startYY1, endYY1))
        myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(MFUTreeDefP1AreaTOT,
                               toEqCO2 * MFUDeforestationEmission1,
                               MFUTreeDefP1AreaTOT/deltaYY1,
                               toEqCO2 * MFUDeforestationEmission1/deltaYY1))
        myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(MFUTreeDegP1AreaTOT,
                               toEqCO2 * MFUDegradationEmission1,
                               MFUTreeDegP1AreaTOT/deltaYY1,
                               toEqCO2 * MFUDegradationEmission1/deltaYY1))
        if useTwoPeriodsBool:
        #Period 2
            myHtml.addText('<tr><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText(u'<tr><td>Période {}-{}</td></tr>'.format(startYY2, endYY2))
            myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDefP2AreaTOT,
                                   toEqCO2 * MFUDeforestationEmission2,
                                   MFUTreeDefP2AreaTOT/deltaYY2,
                                   toEqCO2 * MFUDeforestationEmission2/deltaYY2))
            myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDegP2AreaTOT,
                                   toEqCO2 * MFUDegradationEmission2,
                                   MFUTreeDegP2AreaTOT/deltaYY2,
                                   toEqCO2 * MFUDegradationEmission2/deltaYY2))
            #Total period
            myHtml.addText('<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr class="line"><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText(u'<tr><td>Période {}-{}</td></tr>'.format(startYY1, endYY2))
            myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(MFUTreeDefP1AreaTOT+MFUTreeDefP2AreaTOT,
                                   toEqCO2 * (MFUDeforestationEmission1+MFUDeforestationEmission2),
                                   (MFUTreeDefP1AreaTOT+MFUTreeDefP2AreaTOT) / deltaYYTotal,
                                   toEqCO2 * (MFUDeforestationEmission1+MFUDeforestationEmission2) / deltaYYTotal))
            myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format( (MFUTreeDegP1AreaTOT + MFUTreeDegP2AreaTOT),
                                    toEqCO2 * (MFUDegradationEmission1 + MFUDegradationEmission2),
                                    (MFUTreeDegP1AreaTOT + MFUTreeDegP2AreaTOT)/ deltaYYTotal,
                                    toEqCO2 * (MFUDegradationEmission1 + MFUDegradationEmission2) / deltaYYTotal))
        myHtml.addText('</table>')

    #Option B
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings['OptionBTitle'][myHtml.lang])
    myHtml.addText(myHtml.trStrings['OptionBText'][myHtml.lang])

    if myHtml.lang=='EN':
        myHtml.addText('<table>')
        myHtml.addText('<tr><th></th><th>Total<br/>(ha)</th><th>Total<br/>(tEqCO<sub>2</sub>)</th><th>Annual rate<br/>(ha/year)</th><th>Annual rate<br/>(tEqCO<sub>2</sub>/year)</th></tr>')
        #Period 1
        myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY1, endYY1))
        myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(round(PXPTOT['Deforest_p1'] * sqrps_ha),
                               OptionBEMDEfP1,
                               round(PXPTOT['Deforest_p1'] * sqrps_ha/ deltaYY1) ,
                               OptionBEMDEfP1 / deltaYY1))
        myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(round(PXPTOT['Degrad_p1'] * sqrps_ha),
                               OptionBEMDEgP1,
                               round(PXPTOT['Degrad_p1'] * sqrps_ha/deltaYY1),
                               OptionBEMDEgP1 / deltaYY1))

        if useTwoPeriodsBool:
        #Period 2
            myHtml.addText('<tr><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY2, endYY2))
            myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round(PXPTOT['Deforest_p2'] * sqrps_ha),
                                   OptionBEMDEfP2,
                                   round(PXPTOT['Deforest_p2'] * sqrps_ha/ deltaYY2) ,
                                   OptionBEMDEfP2 / deltaYY2))
            myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round(PXPTOT['Degrad_p2'] * sqrps_ha),
                                   OptionBEMDEgP2,
                                   round(PXPTOT['Degrad_p2'] * sqrps_ha/deltaYY2),
                                   OptionBEMDEgP2 / deltaYY2))
            # Total period
            myHtml.addText('<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr class="line"><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY1, endYY2))
            myHtml.addText('<tr><td>Deforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2']) * sqrps_ha),
                                   OptionBEMDEfP1 + OptionBEMDEfP2,
                                   round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2']) * sqrps_ha / deltaYYTotal),
                                   (OptionBEMDEfP1 + OptionBEMDEfP2) / deltaYYTotal))
            myHtml.addText('<tr><td>Degradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round( (PXPTOT['Degrad_p1']+PXPTOT['Degrad_p2']) * sqrps_ha),
                                   OptionBEMDEgP1 + OptionBEMDEgP2,
                                   round((PXPTOT['Degrad_p1']+PXPTOT['Degrad_p2']) * sqrps_ha / deltaYYTotal),
                                   ( OptionBEMDEgP1 + OptionBEMDEgP2) / deltaYYTotal))
        myHtml.addText('</table>')

    elif myHtml.lang=='FR':
        myHtml.addText('<table>')
        myHtml.addText(u'<tr><th></th><th>Total<br/>(ha)</th><th>Total<br/>(tEqCO<sub>2</sub>)</th><th>Taux annuel<br/>(ha/year)</th><th>Taux annuel<br/>(tEqCO<sub>2</sub>/year)</th></tr>')
        #Period 1
        myHtml.addText(u'<tr><td>Période {}-{}</td></tr>'.format(startYY1, endYY1))
        myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(round(PXPTOT['Deforest_p1'] * sqrps_ha),
                               OptionBEMDEfP1,
                               round(PXPTOT['Deforest_p1'] * sqrps_ha/ deltaYY1) ,
                               OptionBEMDEfP1 / deltaYY1))
        myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                       .format(round(PXPTOT['Degrad_p1'] * sqrps_ha),
                               OptionBEMDEgP1,
                               round(PXPTOT['Degrad_p1'] * sqrps_ha/deltaYY1),
                               OptionBEMDEgP1 / deltaYY1))
        if useTwoPeriodsBool:
        #Period 2
            myHtml.addText('<tr><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr><td>Period {}-{}</td></tr>'.format(startYY2, endYY2))
            myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round(PXPTOT['Deforest_p2'] * sqrps_ha),
                                   OptionBEMDEfP2,
                                   round(PXPTOT['Deforest_p2'] * sqrps_ha/ deltaYY2) ,
                                   OptionBEMDEfP2 / deltaYY2))
            myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round(PXPTOT['Degrad_p2'] * sqrps_ha),
                                   OptionBEMDEgP2,
                                   round(PXPTOT['Degrad_p2'] * sqrps_ha/deltaYY2),
                                   OptionBEMDEgP2 / deltaYY2))
            # Total period
            myHtml.addText('<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText('<tr class="line"><td></td><td></td><td></td><td></td><td></td></tr>')
            myHtml.addText(u'<tr><td>Période {}-{}</td></tr>'.format(startYY1, endYY2))
            myHtml.addText(u'<tr><td>Déforestation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2']) * sqrps_ha),
                                   OptionBEMDEfP1 + OptionBEMDEfP2,
                                   round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2']) * sqrps_ha / deltaYYTotal),
                                   (OptionBEMDEfP1 + OptionBEMDEfP2) / deltaYYTotal))
            myHtml.addText(u'<tr><td>Dégradation</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'
                           .format(round( (PXPTOT['Degrad_p1']+PXPTOT['Degrad_p2']) * sqrps_ha),
                                   OptionBEMDEgP1 + OptionBEMDEgP2,
                                   round((PXPTOT['Degrad_p1']+PXPTOT['Degrad_p2']) * sqrps_ha / deltaYYTotal),
                                   ( OptionBEMDEgP1 + OptionBEMDEgP2) / deltaYYTotal))
        myHtml.addText('</table>')

    # exceptions
    myHtml.skipLine()
    myHtml.skipLine()
    myHtml.addText(myHtml.trStrings['ResultTitle2'][myHtml.lang])
    if useExceptMapBool:
        myHtml.addText(myHtml.trStrings['ExceptionfileText'][myHtml.lang])
        myHtml.addText('<br/>')
        myHtml.addText(u"{}<br/>{}".format(myHtml.trStrings['File'][myHtml.lang], exceptMap[0]))
        myHtml.addText('<br/>')
        myHtml.skipLine()
        myHtml.addText(myHtml.trStrings['Result2Text'][myHtml.lang])
        if myHtml.lang=='EN':
            myHtml.addText('<table>')
            myHtml.addText('<tr><th>Exception code</th><th>Area (ha)</th><th>Carbon stock (tC/ha)</th></tr>')
            anycase = False
            for ii in uniqExceptCount:
                if ii !=0:
                    myHtml.addText('<tr><td>{}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'.format(ii, uniqExceptCount[ii], uniqExceptBiom[ii]))
                    anycase = True
            myHtml.addText('</table>')
            if not anycase:
                myHtml.addText( myHtml.trStrings['noIntersectionExceptionLossMap'][myHtml.lang] )
        elif myHtml.lang=='FR':
            myHtml.addText('<table>')
            myHtml.addText(u"<tr><th>Code d'exception</th><th>Aire (ha)</th><th>Stock de carbone (tC/ha)</th></tr>")
            for ii in uniqExceptCount:
                if ii !=0:
                    myHtml.addText('<tr><td>{}</td><td>{:.0f}</td><td>{:.0f}</td></tr>'.format(ii, uniqExceptCount[ii], uniqExceptBiom[ii]))
            myHtml.addText('</table>')
    else:
        myHtml.addText(myHtml.trStrings['NoException'][myHtml.lang])

    myHtml.skipLine()
    myHtml.skipLine()

    if useDisagShpBool:
        myHtml.addText(myHtml.trStrings['ResultTitle3'][myHtml.lang])
        myHtml.addText(myHtml.trStrings['Result3Text'][myHtml.lang])
    else:
        myHtml.addText(myHtml.trStrings['ResultTitle3'][myHtml.lang])
        myHtml.addText(myHtml.trStrings['NoLanduse'][myHtml.lang])

    myHtml.toFile()
