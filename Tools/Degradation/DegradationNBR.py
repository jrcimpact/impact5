#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import json
from scipy.ndimage import filters
import shutil
import numpy


# import GDAL/OGR modules
try:
    from osgeo import ogr, osr, gdal
except ImportError:
    import ogr, osr, gdal

# Import local libraries
import LogStore
import IMPACT
from Simonets_PROC_libs import *
import ImageProcessing


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')

def RunDegradation(overwrite, master, slave, master_mask, slave_mask, OUTname, NIRb, SWIRb, kernelsize, reproject):

    try:
        log = LogStore.LogProcessing('Degradation', 'degradationNBR')

        NIR = int(NIRb)
        SWIR = int(SWIRb)
        kernelsize = int(kernelsize)
        outputsize = 1  # enable

        if len(master) != 1 :
            log.send_error('Wrong Master input ')
            log.close()
            return False
        if len(slave) != 1 :
            log.send_error('Wrong Slave input ')
            log.close()
            return False

        master = master[0]
        slave = slave[0]

        if len(master_mask) == 0:
            master_mask = ''
        else:
            master_mask=master_mask[0]
        if len(slave_mask) == 0:
            slave_mask = ''
        else:
            slave_mask = slave_mask[0]

        # initialize output log
        OUTname = os.path.join(GLOBALS['data_paths']['data'],os.path.basename(OUTname).replace('.tif',''))

        log.set_parameter('Master image',master )
        log.set_parameter('Slave image',slave )
        log.set_parameter('Out name ',OUTname+'.tif')
        log.set_parameter('Kernel radius ', str(kernelsize))
        log.set_parameter('Force reprojection ', str(reproject))

        log.set_parameter('Overwrite ', overwrite)

        ImageProcessing.validPathLengths(OUTname, None, 9, log)

        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_nbr")

        # del temporary file
        if os.path.exists(tmp_indir):
            os.remove(tmp_indir)

        if reproject in ['Yes', 'Y', True, 'True', 1]:
            reproject = True
        else:
            reproject = False

        kernel = numpy.zeros((2*kernelsize, 2*kernelsize))
        y, x = numpy.ogrid[-kernelsize:kernelsize, -kernelsize:kernelsize]
        mask = x**2 + y**2 <= kernelsize**2
        kernel[mask] = 1
        kernel = kernel.astype(bool)

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        if os.path.exists(OUTname+".tif"):
            if overwrite == 'Yes':
                try:
                    os.remove(OUTname+".tif")
                    if os.path.exists(OUTname+".tif.aux.xml"):
                        os.remove(OUTname+".tif.aux.xml")
                    if os.path.exists(OUTname+"_class.tif"):
                        os.remove(OUTname+"_class.tif")
                    if os.path.exists(OUTname+"_class.tif.aux.xml"):
                        os.remove(OUTname+"_class.tif.aux.xml")

                except Exception as e:
                    print(str(e))
                    log.send_error('Cannot delete output file: ' + str(e))
                    log.close()
                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')
                log.close()
                return True

        # -------------------------------------------------------------------
        # ------------------------- TEST OVERLAPS  -------------------------
        # -------------------------------------------------------------------
        print("Overlap Test ")
        print(master)
        print(slave)

        if not test_overlapping(master,slave):
            log.send_error("No overlap between: "+master+" "+slave)
            log.close()
            return
        print("Overlap OK")

        # -------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        # -------------------------------------------------------------------
        master_ds = gdal.Open(master, gdal.GA_ReadOnly)
        master_bands = master_ds.RasterCount
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        master_sourceSR = osr.SpatialReference()
        master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
        m_ulx=master_imgGeo[0]
        m_uly=master_imgGeo[3]
        m_lrx=master_imgGeo[0]+master_cols*master_imgGeo[1]
        m_lry=master_imgGeo[3]+master_rows*master_imgGeo[5]

        master_resolutionsX = float(master_ds.GetGeoTransform()[1])
        master_resolutionsY = abs(float(master_ds.GetGeoTransform()[5]))
        master_dataType = master_ds.GetRasterBand(1).DataType

        # -------------------------------------------------------------------
        # ------------------------- OPEN SLAVE  ---------------------------
        # -------------------------------------------------------------------
        slave_ds = gdal.Open(slave, gdal.GA_ReadOnly)
        slave_bands = slave_ds.RasterCount
        slave_cols = slave_ds.RasterXSize
        slave_rows = slave_ds.RasterYSize
        slave_imgGeo = slave_ds.GetGeoTransform()
        slave_sourceSR = osr.SpatialReference()
        slave_sourceSR.ImportFromWkt(slave_ds.GetProjectionRef())
        s_ulx=slave_imgGeo[0]
        s_uly=slave_imgGeo[3]
        s_lrx=slave_imgGeo[0]+slave_cols*slave_imgGeo[1]
        s_lry=slave_imgGeo[3]+slave_rows*slave_imgGeo[5]


        #-------------------------------------------------------------------
        # -------------- TEST ON SIZE  -> INTERSECTION EXTRACTION  ---------
        #-------------------------------------------------------------------

        # ADD test on intersection / projection etc
        print(max(NIR, SWIR))
        if (slave_bands < max(NIR,SWIR)) or (master_bands <  max(NIR,SWIR)):
            log.send_error("Images do not have selected bands: required "+str(max(NIR,SWIR))+' got '+str(master_bands)+','+str(slave_bands))
            log.close()
            return False

        # ADD test on intersection / projection etc

        reproj_needed = False

        if (slave_rows != master_rows) or (slave_cols != master_cols):
            if reproject == False:
                log.send_error("Degradation error: images do not have same dimension")
                log.send_error("Subset your input images or allow on-the-fly reprojection and image fitting")
                log.close()
                return False
            else:
                reproj_needed = True

        if str(m_ulx) != str(s_ulx) or str(m_uly) != str(s_uly) or str(m_lrx) != str(s_lrx) or str(m_lry) != str(s_lry):
            if reproject == False:
                log.send_error("Degradation error: images do not have same ULxy, LRxy")
                log.send_error("Reproject or subset your input images or allow on-the-fly reprojection and image fitting")
                log.close()
                return False
            else:
                reproj_needed = True

        # -----------------------  LOADING MASK -------------------------------



        #-------------------------------------------------------------------
        # ------------------------- OPEN MASTER MASK ---------------------------
        #-------------------------------------------------------------------
        try:
            master_mask_bands = 0
            if os.path.exists(master_mask):
                master_mask_ds = gdal.Open(master_mask, gdal.GA_ReadOnly)
                master_mask_bands = master_mask_ds.RasterCount
                print(master_mask_bands)
                master_mask_cols = master_mask_ds.RasterXSize
                master_mask_rows = master_mask_ds.RasterYSize
                master_mask_imgGeo = master_mask_ds.GetGeoTransform()
                master_mask_sourceSR = osr.SpatialReference()
                master_mask_sourceSR.ImportFromWkt(master_mask_ds.GetProjectionRef())
                mm_ulx=master_mask_imgGeo[0]
                mm_uly=master_mask_imgGeo[3]
                mm_lrx=master_mask_imgGeo[0]+master_mask_cols*master_mask_imgGeo[1]
                mm_lry=master_mask_imgGeo[3]+master_mask_rows*master_mask_imgGeo[5]
                print(master_mask_sourceSR.ExportToWkt())

                # ADD
                # -------------- TEST ON SIZE  -> INTERSECTION EXTRACTION  ---------

                if master_mask_rows != master_rows or master_mask_cols != master_cols:
                    if reproject == False:
                        log.send_error("Degradation error: Master masks do not have same dimension and projection")
                        log.close()
                        return False
                    else:
                        reproj_needed = True
                # else:
                #     MASK *= (master_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)
                                        
        except Exception as e:
            log.send_error("Error opening Master Mask "+master_mask+": "+str(e))
            log.close()
            return False

        #-------------------------------------------------------------------
        # ------------------------- OPEN SLAVE MASK ---------------------------
        #-------------------------------------------------------------------
        try:
            slave_mask_bands = 0
            if os.path.exists(slave_mask):
                slave_mask_ds = gdal.Open(slave_mask, gdal.GA_ReadOnly)
                slave_mask_bands = slave_mask_ds.RasterCount
                print(slave_mask_bands)
                slave_mask_cols = slave_mask_ds.RasterXSize
                slave_mask_rows = slave_mask_ds.RasterYSize
                slave_mask_imgGeo = slave_mask_ds.GetGeoTransform()
                slave_mask_sourceSR = osr.SpatialReference()
                slave_mask_sourceSR.ImportFromWkt(slave_mask_ds.GetProjectionRef())
                sm_ulx = slave_mask_imgGeo[0]
                sm_uly = slave_mask_imgGeo[3]
                sm_lrx = slave_mask_imgGeo[0]+slave_mask_cols*slave_mask_imgGeo[1]
                sm_lry = slave_mask_imgGeo[3]+slave_mask_rows*slave_mask_imgGeo[5]
                print(slave_mask_sourceSR.ExportToWkt())

                # ADD
                # -------------- TEST ON SIZE  -> INTERSECTION EXTRACTION  ---------
                if slave_mask_rows != slave_rows or slave_mask_cols != slave_cols:
                    if reproject == False:
                        log.send_error("Degradation error: Slave masks do not have same dimension and projection")
                        log.close()
                        return False
                    else:
                        reproj_needed = True
                # else:
                #     MASK *= (slave_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)
                                        
        except Exception as e:
            log.send_error("Error opening Slave Mask "+slave_mask+": "+str(e))
            log.close()
            return False

        #reproj_needed = True
        if reproj_needed == False:

            MASK = numpy.ones((master_rows, master_cols), dtype=numpy.bool)

            if os.path.exists(master_mask):
                MASK *= (master_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)
            if os.path.exists(slave_mask):
                MASK *= (slave_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)
        else:
            print("-------------------------------------------------------")
            print("------ FORCING IMAGE REPROJECTION and FITTING ---------")
            print("-------------------------------------------------------")

            # Extent and projection from Time1 image
            ULRR = str(m_ulx) + ' ' + str(m_uly) + ' ' + str(m_lrx) + ' ' + str(m_lry)
#            minXYmaxXY = str(m_ulx) + ' ' + str(m_lry) + ' ' + str(m_lrx) + ' ' + str(m_uly)
            outputBounds = (min(m_ulx, m_lrx), min(m_uly, m_lry), max(m_ulx, m_lrx), max(m_uly, m_lry))
            # preparing temp directory

            if not os.path.exists(tmp_indir):
                os.makedirs(tmp_indir)

            #   create a layerstak of all necessary bands and masks
            master_ds = None
            slave_ds = None
            master_mask_ds = None
            slave_mask_ds = None

            if slave_sourceSR.ExportToWkt() != master_sourceSR.ExportToWkt():
                print("slave need reproj")
                print(slave_sourceSR)
                print('----')
                print(master_sourceSR)
                print('----')
                slaveWrp = os.path.join(tmp_indir,'slave.tit')
                gdal.Warp(slaveWrp, slave, dstSRS=master_sourceSR, format='Gtiff', outputBounds=outputBounds,
                          options=['compress=lzw', 'bigtiff=IF_SAFER'])
                if os.path.exists(slaveWrp):
                    slave = slaveWrp
                else:
                    log.send_error("Error warping Time2 image")
                    log.close()
                    shutil.rmtree(tmp_indir, ignore_errors=True)
                    return False


            if os.path.exists(master_mask):
                if master_mask_sourceSR.ExportToWkt() != master_sourceSR.ExportToWkt():
                    print("master mask need reproj")
                    master_maskWrp = os.path.join(tmp_indir, 'master_mask.tit')
                    gdal.Warp(master_maskWrp, master_mask, dstSRS=master_sourceSR, format='Gtiff', outputBounds=outputBounds,
                              options=['compress=lzw', 'bigtiff=IF_SAFER'])
                    if os.path.exists(master_maskWrp):
                        master_mask = master_maskWrp
                    else:
                        log.send_error("Error warping Time1 mask")
                        log.close()
                        shutil.rmtree(tmp_indir, ignore_errors=True)
                        return False
            if os.path.exists(slave_mask):
                if slave_mask_sourceSR.ExportToWkt() != master_sourceSR.ExportToWkt():
                    print("slave mask need reproj")
                    slave_maskWrp = os.path.join(tmp_indir, 'slave_mask.tit')
                    gdal.Warp(slave_maskWrp, slave_mask, dstSRS=master_sourceSR, format='Gtiff',outputBounds=outputBounds,
                              options=['compress=lzw', 'bigtiff=IF_SAFER'])
                    if os.path.exists(slave_maskWrp):
                        slave_mask = slave_maskWrp
                    else:
                        log.send_error("Error warping Time2 mask")
                        log.close()
                        shutil.rmtree(tmp_indir, ignore_errors=True)
                        return False

            ps = ' -ps {:.32f} {:.32f}'.format(master_resolutionsX, master_resolutionsY)

            input_file_list = master + " " + slave + ' ' + master_mask + ' ' + slave_mask


            outnameStack = os.path.join(tmp_indir,'stack.tif')
            if os.path.exists(outnameStack):
                os.remove(outnameStack)

            gdal_merge = "gdal_merge"
            if (GLOBALS['OS'] == "unix"):
                gdal_merge = "gdal_merge.py"
            res1 = os.system(gdal_merge + ' -separate -co COMPRESS=LZW -co BIGTIFF=IF_SAFER -ot ' + str(gdal.GetDataTypeName(master_dataType)) + ' -ul_lr ' + ULRR + ' ' + str(ps) + ' -o ' + outnameStack + ' ' + input_file_list)
            if not os.path.exists(outnameStack):
                log.send_error("Error reprojecting images: " + str(res1))
                log.close()
                shutil.rmtree(tmp_indir, ignore_errors=True)
                return False

            master_ds = gdal.Translate(outnameStack + '_master', outnameStack, bandList=range(1,master_bands + 1), format='GTiff', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            slave_ds = gdal.Translate(outnameStack + '_slave', outnameStack, bandList=range(master_bands+1,master_bands + slave_bands + 1 ), format='GTiff', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])

            if os.path.exists(master_mask):
                master_mask_ds = gdal.Translate(outnameStack + '_masterMask', outnameStack, bandList=range(master_bands + slave_bands + 1 ,master_bands + slave_bands + master_mask_bands + 1), format='GTiff', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            if os.path.exists(slave_mask):
                slave_mask_ds = gdal.Translate(outnameStack + '_slaveMask', outnameStack, bandList=range(master_bands + slave_bands + master_mask_bands + 1, master_bands + slave_bands + master_mask_bands + slave_mask_bands + 1), format='GTiff', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])

            # -------------------------------------------------------------------
            # ------------------------- OPEN MASTER  ---------------------------
            # -------------------------------------------------------------------
            master_bands = master_ds.RasterCount
            master_cols = master_ds.RasterXSize
            master_rows = master_ds.RasterYSize
            master_imgGeo = master_ds.GetGeoTransform()
            master_sourceSR = osr.SpatialReference()
            master_sourceSR.ImportFromWkt(master_ds.GetProjectionRef())
            m_ulx = master_imgGeo[0]
            m_uly = master_imgGeo[3]
            m_lrx = master_imgGeo[0] + master_cols * master_imgGeo[1]
            m_lry = master_imgGeo[3] + master_rows * master_imgGeo[5]

            master_resolutionsX = float(master_ds.GetGeoTransform()[1])
            master_resolutionsY = abs(float(master_ds.GetGeoTransform()[5]))
            master_dataType = master_ds.GetRasterBand(1).DataType

            # -------------------------------------------------------------------
            # ------------------------- OPEN SLAVE  ---------------------------
            # -------------------------------------------------------------------
            slave_bands = slave_ds.RasterCount
            slave_cols = slave_ds.RasterXSize
            slave_rows = slave_ds.RasterYSize
            slave_imgGeo = slave_ds.GetGeoTransform()
            slave_sourceSR = osr.SpatialReference()
            slave_sourceSR.ImportFromWkt(slave_ds.GetProjectionRef())
            s_ulx = slave_imgGeo[0]
            s_uly = slave_imgGeo[3]
            s_lrx = slave_imgGeo[0] + slave_cols * slave_imgGeo[1]
            s_lry = slave_imgGeo[3] + slave_rows * slave_imgGeo[5]

            MASK = numpy.ones((master_rows, master_cols), dtype=numpy.bool)

            if os.path.exists(master_mask):
                MASK *= (master_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)
            if os.path.exists(slave_mask):
                MASK *= (slave_mask_ds.GetRasterBand(1).ReadAsArray() == 1).astype(numpy.bool)



        # ----------------------------------------------------
        # -------------        INFO   ------------------------
        # ----------------------------------------------------
        log.send_message("Master Parameters ")
        log.send_message("----------------------------------------------------------")
        log.send_message("Rows = "+str(master_rows))
        log.send_message("Cols = "+str(master_cols))
        #log.send_message("Proj = "+str(master_sourceSR))
        log.send_message("ULX = "+str(m_ulx))
        log.send_message("ULY = "+str(m_uly))
        log.send_message("LRX = "+str(m_lrx))
        log.send_message("LRY = "+str(m_lry))
        log.send_message("MASK = "+str(master_mask))
        log.send_message("----------------------------------------------------------")

        log.send_message("Slave Parameters ")
        log.send_message("----------------------------------------------------------")
        log.send_message("Rows = "+str(slave_rows))
        log.send_message("Cols = "+str(slave_cols))
        #log.send_message("Proj = "+str(slave_sourceSR))
        log.send_message("ULX = "+str(s_ulx))
        log.send_message("ULY = "+str(s_uly))
        log.send_message("LRX = "+str(s_lrx))
        log.send_message("LRY = "+str(s_lry))
        log.send_message("MASK = "+str(slave_mask))
        log.send_message("----------------------------------------------------------")

        # ------------------------------------
        # ---  READING IMG and apply MEDIAN   ---------
        # ----------------------------------------------

        gdal.TermProgress(0.0)
        import time

        mNBR=((master_ds.GetRasterBand(NIR).ReadAsArray().astype(numpy.float32)) - (master_ds.GetRasterBand(SWIR).ReadAsArray().astype(numpy.float32)))
        gdal.TermProgress(0.1)
        mNBR/=((master_ds.GetRasterBand(NIR).ReadAsArray().astype(numpy.float32)) + (master_ds.GetRasterBand(SWIR).ReadAsArray().astype(numpy.float32)))
        gdal.TermProgress(0.2)
        if kernelsize > 0:
            mNBR *= MASK
            mNBR -= filters.median_filter(mNBR,footprint=kernel) #, mode='constant', cval=0.1
            mNBR[mNBR < -1] = -1
            mNBR[mNBR > 0] = 0
        gdal.TermProgress(0.3)
        sNBR = ((slave_ds.GetRasterBand(NIR).ReadAsArray().astype(numpy.float32)) - (slave_ds.GetRasterBand(SWIR).ReadAsArray().astype(numpy.float32)))
        gdal.TermProgress(0.4)
        sNBR /= ((slave_ds.GetRasterBand(NIR).ReadAsArray().astype(numpy.float32)) + (slave_ds.GetRasterBand(SWIR).ReadAsArray().astype(numpy.float32)))
        gdal.TermProgress(0.5)
        # filtered sNBR
        if kernelsize > 0:
            sNBR *= MASK
            sNBR -= filters.median_filter(sNBR,footprint=kernel) #, mode='nearest', cval=0.1
            sNBR[sNBR < -1] = -1
            sNBR[sNBR > 0] = 0

        gdal.TermProgress(0.60)
        diff = (mNBR-sNBR)
        diff[diff < 0] = 0
        diff[diff > 1] = 1
        # diff *= -1?
        diff[MASK == 0] = 255
        gdal.TermProgress(0.70)

        # remove 0 NBR as nan
        diff[~numpy.isfinite(diff)] = 0


        # ------------ OUTPUT NBR -------------------------
        driver = gdal.GetDriverByName("GTiff")
        dst_ds = driver.Create( OUTname+".tif",master_cols,master_rows,1,gdal.GDT_Float32,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        dst_ds.SetGeoTransform( master_ds.GetGeoTransform())
        dst_ds.SetProjection( master_ds.GetProjectionRef())
        dst_ds.SetMetadata({'Impact_product_type': 'NBR','Impact_operation':"("+str(NIR)+'-'+str(SWIR)+")/("+str(NIR)+'+'+str(SWIR)+")",'Impact_version':IMPACT.get_version()})
        dst_ds.GetRasterBand(1).SetNoDataValue(255)
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(diff)
        dst_ds.FlushCache()
        dst_ds = None

        mNBR = 0
        sNBR = 0

        # ----------- NBR CLASSIFICATION ---------------------
        inGT = master_ds.GetGeoTransform()
        outGT = (inGT[0], inGT[1] * outputsize, inGT[2], inGT[3], inGT[4], inGT[5]*outputsize)
        out_cols = int(master_cols/outputsize)
        out_rows = int(master_rows/outputsize)
        diff = numpy.ma.masked_values(diff, 255)
        OUT = numpy.zeros((out_rows, out_cols))

        try:
            if outputsize == 1:
                    diff = abs(diff) * 100
                    OUT[(diff < 4.0)] = 10
                    OUT[(diff >= 4.0)] = 30
                    OUT[MASK == 0] = 40

                    diff = None
                    gdal.TermProgress(0.8)

            # else:
            #     c, r = 0, 0
            #     thisC, thisR = 0,0
            #
            #     step = 100./out_cols*1.
            #     step /=100.
            #     step = 0.8 + step
            #     while c+outputsize <= master_cols:
            #         gdal.TermProgress(step)
            #         step+=0.001
            #         while r+outputsize <= master_rows:
            #             cellMean = abs((diff[r:r+outputsize, c:c+outputsize]).mean()) * 100
            #             if cellMean >= 0 and cellMean < 0.513021:
            #                 CLASS = 10
            #             elif cellMean > 0.513021 and cellMean < 1.0:
            #                 CLASS = 30
            #             elif cellMean >= 1.0 and cellMean < 1.532308:
            #                 CLASS = 31
            #             elif cellMean >= 1.532308 and cellMean < 3.003053:
            #                 CLASS = 32
            #             elif cellMean >= 3.003053:
            #                 CLASS = 33
            #             else:
            #                 CLASS = 40
            #
            #             OUT[thisR, thisC] = CLASS
            #             r+= outputsize
            #             thisR += 1
            #         r = 0
            #         c+= outputsize
            #         thisR = 0
            #         thisC += 1

                # diff = None

            gdal.TermProgress(1.0)
            # ------------ OUTPUT NBR Classification ---------------
            driver = gdal.GetDriverByName("GTiff")
            dst_ds = driver.Create( OUTname+"_class.tif", out_cols, out_rows, 1, gdal.GDT_Byte, options = ['COMPRESS=LZW', 'BIGTIFF=IF_SAFER' ]) #GDT_Byte
            dst_ds.SetGeoTransform( outGT )
            dst_ds.SetProjection( master_ds.GetProjectionRef())
            dst_ds.SetMetadata({'Impact_product_type': 'Nbr class', 'Impact_operation': "Nbr classification", 'Impact_version':IMPACT.get_version()})
            dst_ds.GetRasterBand(1).SetNoDataValue(0)
            outband = dst_ds.GetRasterBand(1)
            outband.WriteArray(OUT)
            dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
            c = gdal.ColorTable()
            ctable = [
                [0, (0, 0, 0)],
                [10, (1, 128, 1)],
                [30, (255, 204, 1)],
                [40, (192, 192, 192)]
            ]
            for cid in range(0, len(ctable)):
                c.SetColorEntry(ctable[cid][0], ctable[cid][1])
            dst_ds.GetRasterBand(1).SetColorTable(c)
            dst_ds.FlushCache()
            gdal.TermProgress(1.0)
            dst_ds = None
            OUT = None

        except Exception as e:
            log.send_error("Degradation  error: " + str(e).replace("'",''))
            raise

        master_ds = None
        slave_ds = None
        master_mask_ds = None
        slave_mask_ds = None

        shutil.rmtree(tmp_indir, ignore_errors=True)

        log.send_message("Completed.")
        log.close()


    except Exception as e:
        print(str(e))
        log.send_error("Degradation  error: "+str(e).replace("'",''))



        if os.path.exists(OUTname+'.tif.aux.xml'):
            os.remove(OUTname+'.tif.aux.xml')
        if os.path.exists(OUTname+".tif"):
            os.remove(OUTname+".tif")
        if os.path.exists(OUTname+'_class.tif'):
            os.remove(OUTname+'_class.tif')
        if os.path.exists(OUTname+'_class.tif.aux.xml'):
            os.remove(OUTname+'_class.tif.aux.xml')

        master_ds = None
        slave_ds = None
        master_mask_ds = None
        slave_mask_ds = None
        
        shutil.rmtree(tmp_indir, ignore_errors=True)

        log.close()
        return False


if __name__ == "__main__":


    # overwrite = 'Yes'
    # master = "E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\Landsat_2015-11-30_utm.tif"
    # slave = "E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\Landsat_2016-02-02_utm.tif"
    #
    # master_mask="E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\AL_result_14_class_26_52_utm.tif"
    # slave_mask="E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\CALIBRATED_data\\Landsat_test_data\\AL_result_14_class_26_52_utm.tif"
    # OUTname = "Degradation_simple_utm.tif"
    #
    # NIR=4
    # SWIR=6
    # kernelsize=7  # 210mt -> 7+7 14 in landsat and 42 in Sentinel
    # save_index = 'Yes'
    # #OUTname='E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\VECTOR_data\\User_Features\\Test_afr.shp'
    # res=RunDegradation(overwrite,master,slave,master_mask,slave_mask,NIR,SWIR,OUTname,kernelsize,save_index)

    me, overwrite, master_img, slave_img, master_mask, slave_mask, outname, nir, swir, kernel_size, reproject = sys.argv

    res = RunDegradation(overwrite, json.loads(master_img), json.loads(slave_img), json.loads(master_mask), json.loads(slave_mask), outname, nir, swir, kernel_size, reproject)
