#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Bruno Combal, Simonetti Dario, Marelli Andrea, Verhegghen Astrid
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import shutil
import sys
import time
import json
import numpy
import math
import numbers

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import CarbEF_statout
import CarbEF_htmlout
from Simonets_PROC_libs import *
import ImageProcessing

DEBUG=False # if true, creates outputs for validation
gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
timeSleep = 4
toEqCO2 = 1.0 / 0.2727  # factor to convert tC into tEqCO2

def wst(num, separator=' '):
    num = '{}'.format(int(num))
    newString = ''

    for ii, ipos in zip(num[::-1], range(1, len(num)+1)):
        if ipos % 3 == 0:
            newString = separator+ ii + newString
        else:
            newString = ii + newString

    return newString

# ensure that the MFU cells always overimpose, whatever is the image ULX/ULY.
# Depends on the MFU width MFUW, expressed in original image pixel size
# assumes images do not have any angle, coordinates in meters
# origin is at (0, 0)
def getAbsoluteXYOffset(GT, MFUX, MFUY):
    psx = math.fabs(GT[1])
    psy = math.fabs(GT[5])
    ULX = GT[0]
    ULY = GT[3]

    if ULX >= 0:
        nx = math.floor(ULX / psx) # number of pixels, round to the next integer (as 0 may correspond to a fraction of pixel)
        xOffset = MFUX - math.fmod(nx,MFUX) # number of pixels nx, divided in steps of MFUX, rest gives the shift
    else:
        nx = math.ceil( math.fabs(ULX) / psx) #coordinate 0 can fall in a fraction of pixel
        xOffset = math.fmod(nx,MFUX)
    if ULY >=0:
        ny = math.floor(ULY/psy)
        yOffset = MFUY - math.fmod(ny,MFUY)
    else:
        ny = math.floor( math.fabs(ULY)/psy)
        yOffset = math.fmod(ny,MFUY)

    return xOffset, yOffset

def toLonLat(pointX, pointY, inSpatialRefWkt, outputEPSG=4326):
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(pointX, pointY)
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromWkt( inSpatialRefWkt )
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(outputEPSG)
    inSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    outSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
    point.Transform(coordTransform)
    print('-----------------------------------------------')
    print(point.GetX(), point.GetY())
    print(outputEPSG)
    return point.GetX(), point.GetY()
    # # ------  GDAL 3 changes X Y default return val --------
    # # https://github.com/OSGeo/gdal/issues/1546
    # if gdal.VersionInfo()[0] == '3':
    #     if 4326 == outputEPSG:
    #         return point.GetY(), point.GetX()
    #     else:
    #         return point.GetX(), point.GetY()


def mapToPixel(mx, my, gt):
    if gt[2] + gt[4] == 0:  # Simple calc, no inversion required
        px = (mx - gt[0]) / gt[1]
        py = (my - gt[3]) / gt[5]
    else:
        px, py = ApplyGeoTransform(mx, my, InvGeoTransform(gt))
    return int(px + 0.5), int(py + 0.5)

def pixelToMap(px, py, gt):
    mx, my = ApplyGeoTransform(px, py, gt)
    return mx, my

def ApplyGeoTransform(inx, iny, gt):
    ''' Apply a geotransform
        @param  inx       Input x coordinate (double)
        @param  iny       Input y coordinate (double)
        @param  gt        Input geotransform (six doubles)
        @return outx,outy Output coordinates (two doubles)
    '''
    outx = gt[0] + inx * gt[1] + iny * gt[2]
    outy = gt[3] + inx * gt[4] + iny * gt[5]
    return (outx, outy)

def InvGeoTransform(gt_in):
    # we assume a 3rd row that is [1 0 0]
    # Compute determinate
    det = gt_in[1] * gt_in[5] - gt_in[2] * gt_in[4]

    if (abs(det) < 0.000000000000001):
        return

    inv_det = 1.0 / det

    # compute adjoint, and divide by determinate
    gt_out = [0, 0, 0, 0, 0, 0]
    gt_out[1] = gt_in[5] * inv_det
    gt_out[4] = -gt_in[4] * inv_det

    gt_out[2] = -gt_in[2] * inv_det
    gt_out[5] = gt_in[1] * inv_det

    gt_out[0] = (gt_in[2] * gt_in[3] - gt_in[0] * gt_in[5]) * inv_det
    gt_out[3] = (-gt_in[1] * gt_in[3] + gt_in[0] * gt_in[4]) * inv_det

    return gt_out

# alignToImg: warp image imgToAlign onto imgRef, to get a superimposable grid.
# imgRef: filename, reference file
# imgToAlign: filename, image to warp onto imgRef
# aligned: filename, warp result
# resampleType: warp algorithm, see gdal.Warp documentation
def alignToImg(imgRef, imgToAlign, aligned,resampleType, thisLog, outType=None):
    refFid = gdal.Open(imgRef, gdal.GA_ReadOnly)
    ns = refFid.RasterXSize
    nl = refFid.RasterYSize
    gt = refFid.GetGeoTransform()
    proj = refFid.GetProjection()
    ul = pixelToMap(0,0,gt)
    lr = pixelToMap(ns, nl, gt)
    outputBounds = ( min(ul[0], lr[0]), min(ul[1],lr[1]), max(ul[0],lr[0]), max(ul[1],lr[1]) )
    inDatatype = refFid.GetRasterBand(1).DataType

    try:
        if outType is not None:
            print('Warp casting {} to data type {}'.format(imgToAlign, outType))
            gdal.Warp(aligned, imgToAlign, dstSRS=proj, dstNodata=0,\
                      outputBounds=outputBounds, outputBoundsSRS=proj,\
                      xRes=abs(gt[1]), yRes=abs(gt[5]), \
                      outputType=outType,\
                      resampleAlg=resampleType, format='Gtiff', options=['compress=lzw','bigtiff=IF_SAFER'])

        else:
            print('Warp keeps {} original data type'.format(imgToAlign))
            gdal.Warp(aligned, imgToAlign, dstSRS=proj, dstNodata=0,\
                      outputBounds=outputBounds, outputBoundsSRS=proj,\
                      xRes=abs(gt[1]), yRes=abs(gt[5]),\
                      resampleAlg=resampleType, format='Gtiff', options=['compress=lzw','bigtiff=IF_SAFER'])
    except:
        thisLog.send_error('Error while warping image {}'.format(imgToAlign))
        time.sleep(timeSleep)
        return -1
    return 0

# reproject a shapefile to targetSRS. If the shp SRS is unknown, assume EPSG:4326
# save result to "fileOut"
# return True for success, False otherwise
# note: discard data that are not from colum 'field' (it was found that some shp can have ill encoded column,
# resulting in warning when reprojecting
def reprojShp(shp, targetSRS, fileOut, log, field=None):
    driver = ogr.GetDriverByName('ESRI Shapefile')
    # input SpatialReference
    dataSource = driver.Open(shp, 0)
    layer = dataSource.GetLayer()
    spatialRef = layer.GetSpatialRef()
    if spatialRef is None or spatialRef=='':
        spatialRef = osr.SpatialReference()
        spatialRef.ImportFromEPSG(4326)
        mmuOutputLog(log, {'Reprojecting shapefile: source SRS not found':'assuming EPSG:4326'})

    spatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    targetSRS.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    coordTrans = osr.CoordinateTransformation(spatialRef, targetSRS)
    deleteFile(fileOut)
    try:
        if os.path.exists(fileOut):
            driver.DeleteDataSource(fileOut)
    except Exception as e:
        mmuOutputLog(log, {"In reprojSHP: could not delete existing file {}".format(fileOut):str(e)}, 'send_error')
        return False
    outDS = driver.CreateDataSource(fileOut)
    outLayer = outDS.CreateLayer("disagTMP", targetSRS, geom_type=ogr.wkbMultiPolygon)
    # add fields
    inLayerDefn = layer.GetLayerDefn()
    if field is None:
        for ii in range(0, inLayerDefn.GetFieldCount()):
            fieldDefn = inLayerDefn.GetFieldDefn(ii)
            outLayer.CreateField(fieldDefn)
    else:
        for ii in range(0, inLayerDefn.GetFieldCount()):
            fieldDefn = inLayerDefn.GetFieldDefn(ii)
            if fieldDefn.GetName()==field:
                outLayer.CreateField(fieldDefn)

    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()
    # loop through the input features
    inFeature = layer.GetNextFeature()
    while inFeature:
        geom = inFeature.GetGeometryRef() # get the input geometry
        geom.Transform(coordTrans) # reproject the geometry
        outFeature = ogr.Feature(outLayerDefn) # create a new feature
        outFeature.SetGeometry(geom) # set the geometry and attribute
        if field is None:
            for ii in range(0, outLayerDefn.GetFieldCount()):
                outFeature.SetField(outLayerDefn.GetFieldDefn(ii).GetNameRef(), inFeature.GetField(ii))
        else:
            for ii in range(0, inLayerDefn.GetFieldCount()):
                if inLayerDefn.GetFieldDefn(ii).GetName() == field:
                    outFeature.SetField(0, inFeature.GetField(ii))
        outLayer.CreateFeature(outFeature) # add the feature to the shapefile
        outFeature = None # dereference the features and get the next input feature
        inFeature = layer.GetNextFeature()

    # Save and close the shapefiles
    dataSource = None
    outDS = None

    return True

#
# Count uniq values in an image, returns area for each uniq value
# Report sum of biomass factor (in number of pixels) if provided in input
# assumes biomass file grid matches img grid
def uniqValCountRaster(img, sqrps_ha, biomassVal = None):
    fid = gdal.Open(img, gdal.GA_ReadOnly)
    ns = fid.RasterXSize
    nl = fid.RasterYSize

    uniq=numpy.array([])
    for il in range(nl):
        uniq = numpy.append(uniq, numpy.unique(numpy.ravel(fid.GetRasterBand(1).ReadAsArray(0, il, ns, 1))))
    uniq = numpy.unique(uniq) # for the last line read

    # count uniq occurences
    countUniq = {}
    biomassUniq = {}
    for ii in uniq:
        countUniq[ii] = 0
        biomassUniq[ii] = 0

    # compute number of pixels for each uniq
    fidBIOM = None
    if biomassVal is not None:
        if isinstance(biomassVal, numbers.Number): # single value
            for ii in countUniq:
                for il in range(nl):
                    thisData = numpy.ravel(fid.GetRasterBand(1).ReadAsArray(0, il, ns, 1))
                    countUniq[ii] += numpy.array(thisData == ii).astype(int).sum()
                    biomassUniq[ii] += countUniq[ii] * biomassVal
                countUniq[ii] *= sqrps_ha
        elif isinstance(biomassVal, str) or isinstance(biomassVal, unicode): # map of values
            fidBIOM = gdal.Open(biomassVal, gdal.GA_ReadOnly)
            for ii in countUniq:
                for il in range(nl):
                    thisData = numpy.ravel(fid.GetRasterBand(1).ReadAsArray(0, il, ns, 1))
                    countUniq[ii] += numpy.array(thisData == ii).astype(int).sum()
                    if fidBIOM is not None:
                        thisBiom = numpy.ravel(fidBIOM.GetRasterBand(1).ReadAsArray(0, il, ns, 1))
                        biomassUniq[ii] += numpy.array((thisData == ii) * thisBiom).sum()
                countUniq[ii] *= sqrps_ha
        else:
            raise ValueError('Unknown type for biomassVal passed to function uniqValCount.')

    return countUniq, biomassUniq

# returns list of unique values for field, and associated integer
def rasterizeDisag(shp, field, refImage, cellSize, disagMap, log):
    # create an empty image, identical to refImage
    refFid = gdal.Open(refImage, gdal.GA_ReadOnly)
    ns = int(refFid.RasterXSize / cellSize)
    nl = int(refFid.RasterYSize / cellSize)

    gt = refFid.GetGeoTransform()
    newGt = (gt[0], cellSize * gt[1], gt[2], gt[3], gt[4], cellSize*gt[5])
    proj = refFid.GetProjection()
    projRef = refFid.GetProjectionRef()
    targetSRS = osr.SpatialReference()
    #targetSRS.CloneGeogCS(projRef)
    targetSRS.ImportFromWkt(projRef)
    refFid = None

    if not(test_overlapping(shp, refImage)):
        log.send_error('Disagregation shapefile and input raster are not overlapping.')
        return False

    # resolve overlapping issues
    shpClean = shp

    # reproject shp into target raster SRS
    tmpDirShp = os.path.join(GLOBALS['data_paths']['tmp_data'], "degradationReportModuleSHPTMP")
    if os.path.exists(tmpDirShp):
        shutil.rmtree(tmpDirShp, ignore_errors=True)
    try:
        os.makedirs(tmpDirShp)
    except Exception as e:
        log.send_message('Error message {} when try to create temporary directory for shp operations'.format(str(e)))
        if not os.path.exists(tmpDirShp):
            log.send_error('Could not create temporary directory {}. Please correct.'.format(tmpDirShp))
            log.close()
            return False
    tmpShp = os.path.join( tmpDirShp, '{}_disag.shp'.format( os.path.basename(shp) ))
    reprojShp(shpClean, targetSRS, tmpShp, log, field)
    outDs = gdal.GetDriverByName('GTiff').Create(disagMap, round(ns), round(nl), 1, gdal.GDT_Int16, options=['compress=lzw','bigtiff=IF_SAFER'])
    outDs.SetGeoTransform(newGt)
    outDs.SetProjection( proj )

    # set default to -1
    lineDefault = numpy.zeros((1,ns)) - 1
    for il in range(nl):
        outDs.GetRasterBand(1).WriteArray(lineDefault, 0, il)

    # get shapefile information
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataSource = driver.Open(tmpShp, 0)
    # assume n layers
    layer = dataSource.GetLayer()
    try:
        uniqValues = list(set([feature.GetFieldAsString(field) for feature in layer]))
    except Exception as e:
        mmuOutputLog(log, {'Error when accessing field {} from file : '.format(field): shp}, 'send_error')
        mmuOutputLog(log, {'Error message': str(e)}, 'send_error')
        time.sleep(timeSleep)
        log.close()
        return False
    if uniqValues == []:
        mmuOutputLog(log, {'Error in input disagregation shapefile {}: '.format(shp):'field {} has no value'.format(field)}, 'send_error')
        time.sleep(timeSleep)
        return False
    #dataSource = None  # must be closed

    classes = {-1:"Other"}
    try:
        for ii in range(len(uniqValues)):
            classes[ii]=uniqValues[ii]
            mmuOutputLog(log, {uniqValues[ii]:ii}, 'all_no_error')
            # select features
            layer.SetAttributeFilter( "{} = '{}'".format(field, uniqValues[ii]) )
            gdal.RasterizeLayer(outDs, [1], layer, burn_values = [ii] )

            # options = gdal.RasterizeOptions(format='gtiff', where='"{}"="{}"'.format(field, uniqValues[ii]),
            #                                 xRes=newGt[1], yRes=newGt[5], outputSRS=projRef,
            #                                 width=ns, height=nl,
            #                                 layers=["disagTMP"],
            #                                 burnValues=[ii],
            #                                 #bands=[1]
            #                                 )
            # gdal.RasterizeLayer(outDs, [1], tmpShp, ["disagTMP"], options=options)

        log.set_parameter('Disaggregation layers codes','')
        for ii in classes: log.set_parameter(ii, 'Code: {}'.format(classes[ii] ))
    except Exception as e:
        mmuOutputLog(log, {'Error calling gdal.RasterizeLayer': str(e)}, 'send_error')
        time.sleep(timeSleep)
        log.close()
        return False

    dataSource = None
    return classes
# ___________________
# try deleting a file
# ___________________
def deleteFile(thisFile):
    try:
        os.remove(thisFile)
    except:
        return False
    return True

# ___________________
# Check if file exists and overwrite is false
# ___________________
def canOverwrite(thisFile, overwrite):
    if os.path.isfile(thisFile):
        if overwrite:
            return True # file exists overwrite is true
        else:
            return False # file exists overwrite is false
    else: # file does not exists yet, can overwrite
        return True
# ___________________
# messages in logs
# ___________________
def mmuOutputLog(thisLog, msg, channel='all_no_error'):
    for ii in msg.keys():
        if channel.lower() in ['set_parameter','parameter','all', 'all_noerror','all_no_error','noerror','no_error']:
            thisLog.set_parameter(ii, msg[ii])
        if channel.lower() in ['send_message','message','all', 'all_noerror','all_no_error','noerror','no_error']:
            thisLog.send_message('{}:\t{}'.format(ii,msg[ii]))
        if channel.lower() in ['send_error','error','all']:
            thisLog.send_error('{}:\t{}'.format(ii,msg[ii]))

# __________
# Check there is no data for period 2 (code=4)
# errorCode: 0 operation where performed, 1: could not open file, 2: error when processing
# message: None: testCode not found, notNone: testCode was found
# __________
def checkOnly1stPeriod(img, testCode):
    message = None
    errorCode = 0
    try:
        fid = gdal.Open(img, gdal.GA_ReadOnly)
        if fid is None:
            message='Could not open file {} when trying to check non-presence of data for period 2.'
            errorCode = 1
            return message, errorCode
        ns = fid.RasterXSize
        nl = fid.RasterYSize

        for il in range(nl):
            thisData = fid.GetRasterBand(1).ReadAsArray(0, il, ns, 1)
            testCodeFound = thisData == testCode
            if testCodeFound.any():
                print('testCodeFound')
                message='Value {} found in the image. Consider recoding this image.'.format(testCode)
                break

    except Exception as e:
        message = 'Error in function checkOnly1stPeriod: {}'.str(e)
        errorCode = 2
        return message, errorCode

    return message, errorCode

# ----------
# main business function
# ----------
def Run_Carbef(log, overwrite, master_img, outname, kernel_size, biomass_value,
               biomassDegradPercent, startYY1, endYY1, startYY2, endYY2, useTwoPeriodsBool,
               forest_mmu_fraction, useConversionMapBool, conversionMapFile,
               useDisagShpBool, disagShp, disagField, useExceptMapBool, exceptMap):

    try:

        if len(master_img) != 1 :
            log.send_error('Wrong Master input ')
            log.close()
            time.sleep(timeSleep)
            return False

        master_img=master_img[0]
        if not os.path.exists(master_img):
            log.send_error('Wrong master_img input ')
            log.close()
            time.sleep(timeSleep)
            return False

        if not useTwoPeriodsBool:
            testCode=4
            thisTest, thisCode = checkOnly1stPeriod(master_img, testCode)
            if thisTest is not None :
                if thisCode==0:
                    log.send_error('You wanted to analyse only 1 period, but data (value = {}) for the second period are indicated in the input file.'.format(testCode))
                    log.send_error('Please correct the input file to have only data refering to the first period')
                log.send_error(thisTest)
                log.close()
                return False

        # create temporary data
        tmpDir = os.path.join(GLOBALS['data_paths']['tmp_data'], "degradationReportModuleTMP")
        if os.path.exists(tmpDir):
            shutil.rmtree(tmpDir, ignore_errors=True)

        try:
            os.makedirs(tmpDir)
        except Exception as e:
            log.send_message('Error message {} when trying to create general temporary directory'.format(str(e)))
            if not os.path.exists(tmpDir):
                log.send_error('Could not create temporary directory {}. Please correct.'.format(tmpDir))
                log.close()
                time.sleep(timeSleep)
                return False

        # create a temporary warped filename
        warpedEFMap =os.path.join( tmpDir, '{}_warped.gtif'.format(outname))

        # create a temporary rasterized disagregation layer
        disagMap = os.path.join( tmpDir, '{}_disag.gtif'.format(outname))

        # create a temporary warped exception filename
        warpedExceptMap = os.path.join( tmpDir, '{}_except.gtif'.format(outname))

        if (useConversionMapBool):
            if len(conversionMapFile)==1:
                conversionMapFile = conversionMapFile[0]
            else:
                log.send_error('Please select 1 file only for the biomass conversion map.')
                log.close()
                time.sleep(timeSleep)
                return False
            if not os.path.exists(conversionMapFile):
                log.send_error('Wrong Conversion Map File ')
                log.close()
                time.sleep(timeSleep)
                return False

        # initialize output  file name
        # caution, outname used before
        outname=os.path.join(os.path.dirname(master_img), os.path.basename(outname).replace('.tif',''))
        if not canOverwrite(outname+ '_class.tif', overwrite.lower() in ["true", "yes", "y", '1']):
            log.send_error('Overwite not allowed and output file already exists: please delete or rename output file or allow ovewriting')
            log.send_error('File {} exists, overwrite not allowed. Please correct'.format(outname+ '_class.tif'))
            log.close()
            return False

        if (useConversionMapBool):
            msg = {'Activity map': master_img, 'MFU size (px)': kernel_size, 'Output name': outname,
                   'Overwrite': overwrite, 'Use conversion map':useConversionMapBool, 'Conversion map file':conversionMapFile}
        else:
            msg = {'Activity map': master_img, 'MFU size (px)': kernel_size, 'Output name': outname,
                   'Overwrite': overwrite, 'Use conversion map': useConversionMapBool, 'Biomass value ':biomass_value,
                   'Degradation emission as percentage of deforestation emission':biomassDegradPercent}
        mmuOutputLog(log, msg)

        ImageProcessing.validPathLengths(outname,None, 5, log)
        kernel_size = int(kernel_size)
        biomass_value = float(biomass_value)
        # -------------------------------------------------------------------
        # ------ DEL out files and tmp files if necessary  ------------------
        # -------------------------------------------------------------------
        # always delete warped file (in case it was not deleted at the end of the process).
        if os.path.exists(warpedEFMap):
            deleteFile(warpedEFMap+'.aux.xml') # try silently
            if (not deleteFile(warpedEFMap)):
                log.send_error('Could not delete existing temporary file {}. Please delete it manually.'.format(warpedEFMap))
                log.close()
                time.sleep(timeSleep)
                return False
        if os.path.exists(warpedExceptMap):
            deleteFile(warpedExceptMap + '.aux.xml')  # try silently
            if (not deleteFile(warpedExceptMap)):
                log.send_error(
                    'Could not delete existing temporary file {}. Please delete it manually.'.format(warpedEFMap))
                log.close()
                time.sleep(timeSleep)
                return False

        # ------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        # ------------------------------------------------------------------
        master_ds = gdal.Open(master_img, gdal.GA_ReadOnly)
        if master_ds is None:
            print("Could not open file {}. Check it exists and is not use by another software".format(master_img))
            log.send_error('Could not open file {}, (file does not exist or is used by another software.)'.format(master_img))
            log.close()
            time.sleep(timeSleep)
            return False
        #master_bands=master_ds.RasterCount
        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize
        master_imgGeo = master_ds.GetGeoTransform()
        m_ulx = master_imgGeo[0]
        m_uly = master_imgGeo[3]
        m_lrx = master_imgGeo[0] + master_cols * master_imgGeo[1]
        m_lry = master_imgGeo[3] + master_rows * master_imgGeo[5]
        prj = master_ds.GetProjection()
        srs = osr.SpatialReference(wkt=prj)
        projRef = master_ds.GetProjectionRef()
        sqrps_ha = abs(master_imgGeo[1] * master_imgGeo[5]) * 1. / 10000.0  # gives the size of a pixel in ha, pixels could be rectangles...

        #if srs.IsProjected() and srs.GetAttrValue('unit') in ['metre', 'meter', 'metres', 'meters', 'm']: #may not work with some continental projections
        if not srs.GetAttrValue('unit') in ['metre', 'meter', 'metres', 'meters', 'm']:
              log.send_error("Input image must be projected and have pixel size in metre. Unit found is {}".format(srs.GetAttrValue('unit')) )
              master_ds = None
              log.close()
              time.sleep(timeSleep)
              return False

        # ----------------------------------------------------
        # -------------        INFO   ------------------------
        # ----------------------------------------------------
        msg={"Activity map Rows":master_rows, "Activity map Columns":master_cols,"Activity map Pixel resolution":master_imgGeo[1],"Activity map unit":srs.GetAttrValue('unit'),"Activity map ULX":m_ulx, "Activity map ULY":m_uly,"Activity map LRX":m_lrx,"Activity map LRY":m_lry,"Activity map no data value":master_ds.GetRasterBand(1).GetNoDataValue()}
        mmuOutputLog(log, msg)
        out_cols=int(master_cols/kernel_size)
        out_rows=int(master_rows/kernel_size)

        # ----------------------------------------
        # ---- Warp emission map, if required ----
        # ----------------------------------------
        if useConversionMapBool:
            mmuOutputLog(log, {"Warping Emission image": conversionMapFile}, 'send_message')
            # note: the input biomass can be either on a float or an int or byte. Internal computation (multiplication) are cast to float
            warpError=alignToImg(master_img, conversionMapFile, warpedEFMap, 'near', log)
            if warpError!= 0:
                log.send_error('Error in pre-processing emission map {}, with code {}'.format(conversionMapFile, warpError))
                time.sleep(timeSleep)
                log.close()
                return False
            wefmFID = gdal.Open(warpedEFMap, gdal.GA_ReadOnly)
            inBFM = wefmFID.GetRasterBand(1).ReadAsArray().astype(numpy.float)
        # ----------------------------------------
        # ---- Compute exception areas and corresponding biomass
        # ----------------------------------------
        uniqExceptCount = ''
        uniqExceptBiom = ''
        if useExceptMapBool:
            mmuOutputLog(log, {"Warping Exception image": exceptMap[0]})
            warpError = alignToImg(master_img, exceptMap[0], warpedExceptMap, 'near', log)
            if warpError != 0:
                log.send_error('Error in pre-processing exception map {}, with code {}'.format(exceptMap[0], warpError))
                time.sleep(timeSleep)
                log.close()
                return False
            exceptFID = gdal.Open(warpedExceptMap, gdal.GA_ReadOnly)
            inExcept = exceptFID.GetRasterBand(1).ReadAsArray().astype(numpy.int)
            # get unique values
            mmuOutputLog(log, {"Detecting unique exception values": ''})
            if useConversionMapBool:
                uniqExceptCount, uniqExceptBiom = uniqValCountRaster(warpedExceptMap, sqrps_ha, warpedEFMap)
            else:
                uniqExceptCount, uniqExceptBiom = uniqValCountRaster(warpedExceptMap, sqrps_ha, biomass_value)
            mmuOutputLog(log, {"Exception values":uniqExceptCount})
            mmuOutputLog(log, {"Exception biomass":uniqExceptBiom})

        # ------------------------------------
        # rasterize disaggregation layer if required
        # ------------------------------------
        # Disaggregation arrays must be exist in any case
        EMDegP1Disag = {}
        EMDegP2Disag = {}
        EMDefP1Disag = {}
        EMDefP2Disag = {}
        ARDegP1Disag = {}
        ARDegP2Disag = {}
        ARDefP1Disag = {}
        ARDefP2Disag = {}
        EMMFUDegP1Disag = {}
        EMMFUDegP2Disag = {}
        EMMFUDefP1Disag = {}
        EMMFUDefP2Disag = {}
        ARMFUDegP1Disag = {}
        ARMFUDegP2Disag = {}
        ARMFUDefP1Disag = {}
        ARMFUDefP2Disag = {}
        if useDisagShpBool:
            mmuOutputLog(log, {"Rasterizing disaggregation layer": disagShp[0]}, 'send_message')

            if not os.path.exists(disagShp[0]):
                log.send_error('Wrong disaggregation File ')
                log.close()
                time.sleep(timeSleep)
                return False

            # reproject shp if necessary
            try:
                disagElements = rasterizeDisag(disagShp[0], disagField, master_img, kernel_size, disagMap, log)
                if disagElements == False:
                    log.send_error('Error in rasterizeDisag function ')
                    log.close()
                    return False
                disagMapFID = gdal.Open(disagMap, gdal.GA_ReadOnly)
                disagArray = disagMapFID.GetRasterBand(1).ReadAsArray(0, 0, disagMapFID.RasterXSize, disagMapFID.RasterYSize)
                disaMapFid = None

                for ii in disagElements:
                    # for pixel based count
                    EMDegP1Disag[disagElements[ii]] = 0 # Emission, Degradation, Period 1, disaggregated
                    EMDegP2Disag[disagElements[ii]] = 0
                    EMDefP1Disag[disagElements[ii]] = 0
                    EMDefP2Disag[disagElements[ii]] = 0
                    ARDegP1Disag[disagElements[ii]] = 0 # Areas, Degradation, Period 1, disaggregated
                    ARDegP2Disag[disagElements[ii]] = 0
                    ARDefP1Disag[disagElements[ii]] = 0
                    ARDefP2Disag[disagElements[ii]] = 0
                    # same for MFU counts
                    EMMFUDegP1Disag[disagElements[ii]] = 0 # Emission, Degradation, Period 1, disaggregated
                    EMMFUDegP2Disag[disagElements[ii]] = 0
                    EMMFUDefP1Disag[disagElements[ii]] = 0
                    EMMFUDefP2Disag[disagElements[ii]] = 0
                    ARMFUDegP1Disag[disagElements[ii]] = 0 # Areas, Degradation, Period 1, disaggregated
                    ARMFUDegP2Disag[disagElements[ii]] = 0
                    ARMFUDefP1Disag[disagElements[ii]] = 0
                    ARMFUDefP2Disag[disagElements[ii]] = 0
            except Exception as e:
                log.send_error('Error while processing the disagregation file: {}'.format(str(e)))
                time.sleep(timeSleep)
                log.close()
                return False
        # ------------------------------------
        # Processing: loop reading master_img [and warpedEFMap if needed] by kernels size
        # computation done for each chunk of data
        # ------------------------------------
        mmuOutputLog(log, {'MFU statistics':'initialised'},'send_message')
        progress=0.0
        progressProp = {'read':0.1, 'proc':0.7, 'save1':0.1, 'save2':0.1}
        gdal.TermProgress(progress)
        #nodata = (master_ds.GetRasterBand(1).GetNoDataValue())

        IN = master_ds.GetRasterBand(1).ReadAsArray().astype(numpy.byte)
        OUT_CLASS = numpy.zeros( (out_rows, out_cols))
        OUT_ND = numpy.zeros( (out_rows, out_cols) ) #IN*0
        OUT_FF = numpy.zeros( (out_rows, out_cols) ) #IN*0
        OUT_NFNF = numpy.zeros( (out_rows, out_cols) ) #IN*0
        OUT_FNF1 = numpy.zeros( (out_rows, out_cols) ) #IN*0
        OUT_FNF2 = numpy.zeros( (out_rows, out_cols) ) #IN*0
        forestThreshold = forest_mmu_fraction / 100.0
        inGT = master_ds.GetGeoTransform()
        xOffset=0
        yOffset=0

        # for this version, do not offset. The reason is that many place in the code must be updates,
        # such as the computation of the emission (c/kernel_size, r/kernel_size). To be introduced when code is rewritten
        #xOffset, yOffset = getAbsoluteXYOffset(inGT, kernel_size, kernel_size)
        #outGT = (inGT[0] + xOffset * inGT[1], inGT[1] * kernel_size, inGT[2], inGT[3] - math.fabs(yOffset*inGT[5]), inGT[4], inGT[5] * kernel_size)
        outGT = (inGT[0], inGT[1] * kernel_size, inGT[2], inGT[3], inGT[4], inGT[5] * kernel_size)
        log.send_message('MFU starting point offsets in pixels {} {}'.format(xOffset, yOffset))

        OUT_EMP1 = numpy.zeros((out_rows, out_cols))
        OUT_EMP2 = numpy.zeros((out_rows, out_cols))
        EMDefP1TOT = 0
        EMDefP2TOT = 0
        EMDegP1TOT = 0
        EMDegP2TOT = 0
        #exceptTOT  = 0
        MFUTreeDefP1AreaTOT = 0 # total area of MFU, count of pixel for TT, TNT1, TNT2, for degradation, period 1
        MFUTreeDefP2AreaTOT = 0
        MFUTreeDegP1AreaTOT = 0
        MFUTreeDegP2AreaTOT = 0
        MFUDegradationEmission1 = 0  # emission, all MFU, for class 31, class 33, class 23
        MFUDegradationEmission2 = 0  # emission, all MFU, for class 32, class 33
        MFUDeforestationEmission1 = 0  # emission, all MFU, for class 21 and 24
        MFUDeforestationEmission2 = 0  # emission, all MFU, for class 22 and class 23

        CLASSTOT = {'0':0, '99':0, '10':0, '21':0, '22':0,'23':0, '24':0, '31':0, '32':0, '33':0, '41':0, '42':0, '43':0, '44':0}
        PXPTOT = {'Degrad_p1':0, 'Degrad_p2':0, 'Deforest_p1':0, 'Deforest_p2':0, 'FF_10':0, 'FF_4x':0, 'FF_2x':0 , 'FF_3x':0, 'NF_10':0, 'NF_2x':0, 'NF_3x':0, 'NF_4x':0, 'FNF1_4x':0, 'FNF2_4x':0, 'ND_1x':0, 'ND_2x':0, 'ND_3x':0, 'ND_4x':0, 'ND_0x':0}
        FTOT, NFTOT, FNF1TOT, FNF2TOT, NDTOT = [0,0,0,0,0]

        progress += progressProp['read']
        gdal.TermProgress(progress)

        try :

            c, r = 0,0
            thisC, thisR = 0,0
            while c+kernel_size <= master_cols:
                while r+kernel_size <= master_rows:

                    masked = IN[r:r+kernel_size,c:c+kernel_size]
                    if useExceptMapBool:
                        # set to 0 any data for which an exception code was found
                        masked *= ( inExcept[r:r+kernel_size,c:c+kernel_size] == 0).astype(numpy.int)

                    FF = numpy.count_nonzero(masked == 1)
                    NF = numpy.count_nonzero(masked == 2)
                    FNF1 = numpy.count_nonzero(masked == 3)
                    FNF2 = numpy.count_nonzero(masked == 4)
                    ND = numpy.count_nonzero(masked <1) + numpy.count_nonzero(masked > 4) #set to no data anything that is not 1,2,3,4. For example, gdalwarp set default to 128
                    TOT = FF + NF + FNF1 + FNF2   # count of pixels different from ND
                    F0 = FF + FNF1 + FNF2
                    third = int(round(TOT * forestThreshold))
                    FTOT += FF
                    NFTOT += NF
                    FNF1TOT += FNF1
                    FNF2TOT += FNF2
                    NDTOT += ND
                    EMDefP1 = 0 # Emission Deforestation Period 1 (historical)
                    EMDefP2 = 0 # Emission Deforestation Period 2 (recent)
                    EMDegP1 = 0 # Emission Degradation Period 1 (historical)
                    EMDegP2 = 0 # Emission Degradation Period 2 (recent)
                    # MFUEmissionFactor: sum of emission factor for TT, TNT1 and TN2 in a MFU

                    if useConversionMapBool:
                        MFUEmissionFactorMap = numpy.sum(
                            ((masked == 1).astype(int) + (masked == 3).astype(int) + (masked == 4).astype(int)) * inBFM[r:r + kernel_size,c:c + kernel_size]) * sqrps_ha
                    else:
                        MFUEmissionFactorConst = numpy.sum(
                            ((masked == 1).astype(int) + (masked == 3).astype(int) + (masked == 4).astype(int)) * biomass_value) * sqrps_ha
                    # MFUTreeArea is the surface covered by trees in any period.

                    MFUTreeArea = numpy.sum(
                        ((masked == 1).astype(int) + (masked == 3).astype(int) + (masked == 4).astype(int))) * sqrps_ha

                    #print MFUTreeArea, MFUTreeDefP1AreaTOT, MFUTreeDefP2AreaTOT, MFUTreeDegP1AreaTOT, MFUTreeDegP2AreaTOT

                    # counter for emissions from the MFU itself:
                    # emissionMFU = average(biomass factor) * Npixel * proportion_factor
                    # average(biomass_factor): average emission factor for the MFU
                    # npixel: number of no-ND pixels in the MFU
                    # proportion_factor: 100%=deforestation, 50% degradation.

                    # ---- Decision tree ---
                    UNIT_CLASS = 99 # default
                    if ND > TOT / 3.0:
                        UNIT_CLASS = 0
                        PXPTOT['ND_0x'] += FF + NF + FNF1 + FNF2 + ND
                    else: # ND < NPIXEL / 3.0
                        if F0 < third:
                            if FNF1 == 0 and FNF2 == 0:
                                UNIT_CLASS = 41
                                PXPTOT['FF_4x'] += FF
                                PXPTOT['NF_4x'] += NF
                                PXPTOT['ND_4x'] += ND
                            elif FNF1 != 0 and FNF2 == 0:
                                UNIT_CLASS = 42
                                PXPTOT['FF_4x'] += FF
                                PXPTOT['NF_4x'] += NF
                                PXPTOT['FNF1_4x'] += FNF1
                                PXPTOT['ND_4x'] += ND
                            elif FNF1 == 0 and FNF2 != 0:
                                UNIT_CLASS = 43
                                PXPTOT['FF_4x'] += FF
                                PXPTOT['NF_4x'] += NF
                                PXPTOT['FNF2_4x'] += FNF2
                                PXPTOT['ND_4x'] += ND
                            elif FNF1 != 0 and FNF2 != 0:
                                UNIT_CLASS = 44
                                PXPTOT['FF_4x'] += FF
                                PXPTOT['NF_4x'] += NF
                                PXPTOT['FNF1_4x'] += FNF1
                                PXPTOT['FNF2_4x'] += FNF2
                                PXPTOT['ND_4x'] += ND
                            else: UNIT_CLASS = 99 # to debug the logic, closes the cases
                        else: # F0>= 30%
                            if FNF1 == 0 and FNF2 == 0:
                                UNIT_CLASS = 10
                                PXPTOT['FF_10'] += FF
                                PXPTOT['NF_10'] += NF
                                PXPTOT['ND_1x'] += ND
                            elif FNF1 !=0 and FNF2 == 0:

                                if FF >= third:
                                    # degradation, period 1 (historical)
                                    UNIT_CLASS = 31
                                    PXPTOT['Degrad_p1'] += FNF1
                                    PXPTOT['NF_3x'] += NF
                                    PXPTOT['FF_3x'] += FF
                                    PXPTOT['ND_3x'] += ND

                                    MFUTreeDegP1AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDegP1 = numpy.sum( (masked == 3).astype(int) * inBFM[r:r + kernel_size, c:c + kernel_size]) * sqrps_ha
                                        # per MFU
                                        MFUDegradationEmission1 += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                        if useDisagShpBool:
                                            EMDegP1Disag[ disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]] ] += EMDegP1
                                            ARDegP1Disag[ disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]] ] += numpy.sum( (masked == 3) ).astype(int) * sqrps_ha
                                            EMMFUDegP1Disag[ disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]] ] +=  MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                            ARMFUDegP1Disag[ disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]] ] += MFUTreeArea
                                    else:
                                        MFUDegradationEmission1 += MFUEmissionFactorConst * biomassDegradPercent / 100.0

                                        if useDisagShpBool:
                                            EMDegP1Disag[ disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha * biomass_value
                                            ARDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha
                                            EMMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                            ARMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                else:
                                    # Deforestation, period 1 (historical)

                                    UNIT_CLASS = 21
                                    PXPTOT['Deforest_p1'] += FNF1
                                    PXPTOT['NF_2x'] += NF
                                    PXPTOT['FF_2x'] += FF
                                    PXPTOT['ND_2x'] += ND

                                    MFUTreeDefP1AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDefP1 = numpy.sum( (masked == 3).astype(int) * inBFM[r:r + kernel_size, c:c + kernel_size]) * sqrps_ha
                                        # Emission per MFU
                                        MFUDeforestationEmission1 += MFUEmissionFactorMap
                                        if useDisagShpBool:
                                            EMDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDefP1
                                            ARDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum( (masked == 3)).astype(int) * sqrps_ha
                                            EMMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap
                                            ARMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDeforestationEmission1 += MFUEmissionFactorConst
                                        if useDisagShpBool:
                                            EMDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1  * sqrps_ha * biomass_value
                                            ARDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1  * sqrps_ha
                                            EMMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst
                                            ARMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                            elif FNF1 == 0 and FNF2 != 0:
                                if FF >= third:
                                    # degradation, period 2 (recent)
                                    UNIT_CLASS = 32
                                    PXPTOT['Degrad_p2'] += FNF2
                                    PXPTOT['NF_3x'] += NF
                                    PXPTOT['FF_3x'] += FF
                                    PXPTOT['ND_3x'] += ND

                                    MFUTreeDegP2AreaTOT += MFUTreeArea

                                    if useConversionMapBool:
                                        EMDegP2 = numpy.sum( (masked == 4).astype(int) * inBFM[r:r + kernel_size, c:c + kernel_size]) * sqrps_ha
                                        # per MFU
                                        MFUDegradationEmission2 += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                        if useDisagShpBool:
                                            EMDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDegP2
                                            ARDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum( (masked == 4)).astype(int) * sqrps_ha
                                            EMMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                            ARMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDegradationEmission2 += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                        if useDisagShpBool:
                                            EMDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha * biomass_value
                                            ARDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha
                                            EMMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                            ARMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                else:
                                    # deforestation, period 2 (recent)
                                    UNIT_CLASS = 22
                                    PXPTOT['Deforest_p2'] += FNF2
                                    PXPTOT['NF_2x'] += NF
                                    PXPTOT['FF_2x'] += FF
                                    PXPTOT['ND_2x'] += ND

                                    MFUTreeDefP2AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDefP2 = numpy.sum((masked == 4).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        # Emission per MFU
                                        MFUDeforestationEmission2 += MFUEmissionFactorMap
                                        if useDisagShpBool:
                                            EMDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDefP2
                                            ARDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum((masked == 4)).astype(int) * sqrps_ha
                                            EMMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap
                                            ARMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDeforestationEmission2 += MFUEmissionFactorConst
                                        if useDisagShpBool:
                                            EMDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha * biomass_value
                                            ARDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha
                                            EMMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst
                                            ARMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                            elif FNF1 !=0 and FNF2 != 0:
                                if FF >= third:
                                    # degradation, period 1 and 2
                                    UNIT_CLASS = 33
                                    PXPTOT['Degrad_p1'] += FNF1
                                    PXPTOT['Degrad_p2'] += FNF2
                                    PXPTOT['NF_3x'] += NF
                                    PXPTOT['FF_3x'] += FF
                                    PXPTOT['ND_3x'] += ND

                                    MFUTreeDegP2AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDegP1 = numpy.sum( (masked == 3).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        EMDegP2 = numpy.sum((masked == 4).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        # Emission per MFU
                                        MFUDegradationEmission1 += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                        MFUDegradationEmission2 += MFUEmissionFactorMap * (1 - biomassDegradPercent / 100.0) * biomassDegradPercent / 100.0
                                        if useDisagShpBool:
                                            EMDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDegP1
                                            EMDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDegP2
                                            ARDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum( (masked == 3)).astype(int) * sqrps_ha
                                            ARDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum((masked == 4)).astype(int) * sqrps_ha
                                            EMMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                            EMMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap * (1 - biomassDegradPercent / 100.0) * biomassDegradPercent / 100.0
                                            ARMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDegradationEmission1 += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                        MFUDegradationEmission2 += MFUEmissionFactorConst * (1 - biomassDegradPercent / 100.0) * biomassDegradPercent / 100.0
                                        if useDisagShpBool:
                                            EMDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha * biomass_value
                                            EMDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha * biomass_value
                                            ARDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha
                                            ARDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha
                                            EMMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                            EMMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * (1 - biomassDegradPercent / 100.0) * biomassDegradPercent / 100.0
                                            ARMFUDegP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea

                                elif (FF+FNF2) >= third:
                                    # degradation in period 1 followed by deforestation in period 2
                                    UNIT_CLASS = 23
                                    PXPTOT['Deforest_p2'] += FNF2
                                    PXPTOT['Degrad_p1'] += FNF1
                                    PXPTOT['NF_2x'] += NF
                                    PXPTOT['FF_2x'] += FF
                                    PXPTOT['ND_2x'] += ND

                                    MFUTreeDefP2AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDegP1 = numpy.sum((masked == 3).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        EMDefP2 = numpy.sum((masked == 4).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        # Emission per MFU
                                        MFUDegradationEmission1 += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                        MFUDeforestationEmission2 += MFUEmissionFactorMap * (1 - biomassDegradPercent / 100.0)
                                        if useDisagShpBool:
                                            EMDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDegP1
                                            EMDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDefP2
                                            ARDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] +=  numpy.sum((masked == 3)).astype(int) * sqrps_ha
                                            ARDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum((masked == 4)).astype(int) * sqrps_ha
                                            EMMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap * biomassDegradPercent / 100.0
                                            EMMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap * (1 - biomassDegradPercent / 100.0)
                                            ARMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDegradationEmission1 +=  MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                        MFUDeforestationEmission2 += MFUEmissionFactorConst * (1 - biomassDegradPercent / 100.0)
                                        if useDisagShpBool:
                                            EMDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha * biomass_value
                                            EMDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha * biomass_value
                                            ARDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF2 * sqrps_ha
                                            ARDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha
                                            EMMFUDegP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * biomassDegradPercent / 100.0
                                            EMMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst * (1 - biomassDegradPercent / 100.0)
                                            ARMFUDefP2Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                else:
                                    # deforestation period 1 and change in period 2 (period 2 is ignored)
                                    UNIT_CLASS = 24
                                    PXPTOT['Deforest_p1'] += FNF1
                                    PXPTOT['NF_2x'] += FNF2
                                    PXPTOT['NF_2x'] += NF
                                    PXPTOT['FF_2x'] += FF
                                    PXPTOT['ND_2x'] += ND

                                    MFUTreeDefP1AreaTOT += MFUTreeArea
                                    if useConversionMapBool:
                                        EMDefP1 = numpy.sum((masked == 3).astype(int) * (inBFM[r:r + kernel_size, c:c + kernel_size])) * sqrps_ha
                                        # Emission per MFU
                                        MFUDeforestationEmission1 += MFUEmissionFactorMap
                                        if useDisagShpBool:
                                            EMDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += EMDefP1
                                            ARDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += numpy.sum((masked == 3)).astype(int) * sqrps_ha
                                            EMMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorMap
                                            ARMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                                    else:
                                        MFUDeforestationEmission1 += MFUEmissionFactorConst
                                        if useDisagShpBool:
                                            EMDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha * biomass_value
                                            ARDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += FNF1 * sqrps_ha
                                            EMMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUEmissionFactorConst
                                            ARMFUDefP1Disag[disagElements[disagArray[int(r / kernel_size), int(c / kernel_size)]]] += MFUTreeArea
                            else: UNIT_CLASS = 99

                    CLASSTOT[str(UNIT_CLASS)] += 1

                    if useConversionMapBool:
                        EMDefP1TOT += EMDefP1
                        EMDefP2TOT += EMDefP2
                        EMDegP1TOT += EMDegP1
                        EMDegP2TOT += EMDegP2

                    #OUT_ND[r:r+kernel_size,c:c+kernel_size] = ND
                    OUT_ND[thisR, thisC] = ND
                    #OUT_FF[r:r+kernel_size,c:c+kernel_size] = FF
                    OUT_FF[thisR, thisC] = FF
                    #OUT_NFNF[r:r+kernel_size,c:c+kernel_size] = NF
                    OUT_NFNF[thisR, thisC] = NF
                    #OUT_FNF1[r:r+kernel_size,c:c+kernel_size] = FNF1
                    OUT_FNF1[thisR, thisC] = FNF1
                    #OUT_FNF2[r:r+kernel_size,c:c+kernel_size] = FNF2
                    OUT_FNF2[thisR, thisC] = FNF2
                    #IN[r:r+kernel_size,c:c+kernel_size]=UNIT_CLASS
                    OUT_CLASS[thisR, thisC] = UNIT_CLASS
                    if useConversionMapBool:
                        OUT_EMP1[thisR, thisC] = EMDefP1 + EMDegP1
                        OUT_EMP2[thisR, thisC] = EMDefP2 + EMDegP2

                    r += kernel_size
                    thisR +=1
                r = 0
                thisR = 0
                c += kernel_size
                thisC += 1
                progress = progressProp['read'] + (c/(1.0*master_cols)) * progressProp['proc']
                gdal.TermProgress(progress)

        except Exception as e:
            log.send_error(str(e))
            log.send_error('Stopped at c {}, r {}'.format(c,r))
            log.send_error('UNIT_CLASS {}'.format(UNIT_CLASS))
            log.close()
            master_ds = None
            if useConversionMapBool:
                wefmFID = None
            time.sleep(timeSleep)
            return False

        # clean up temporary files
        if useConversionMapBool:
            wefmFID = None  # free FID before deleting
        if os.path.exists(warpedEFMap):
            deleteFile(warpedEFMap+'aux.xml') # try silently
            if (not deleteFile(warpedEFMap)):
                log.send_warning('Could not delete temporary file {}. Please delete it manually.'.format(warpedEFMap))
                log.close()
                time.sleep(timeSleep)
                return False

        # --------------------------------------------------------------------------
        # save  UNIT_CLASS
        # --------------------------------------------------------------------------
        try:
            driver = gdal.GetDriverByName("GTiff")
            dst_ds = driver.Create( outname+'_class.tif', out_cols, out_rows,1, gdal.GDT_Byte, options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
            dst_ds.SetGeoTransform( outGT )
            dst_ds.SetProjection( master_ds.GetProjectionRef() )
            dst_ds.SetMetadata({'Impact_product_type': 'MFU class', 'Impact_operation':"MFU degradation", 'Impact_version':IMPACT.get_version(), 'Impact_band_interpretation':'1:Classification'})
            dst_ds.GetRasterBand(1).SetNoDataValue(0)
            outband = dst_ds.GetRasterBand(1)
            outband.WriteArray(OUT_CLASS)
            # ADD PALETTE
            dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
            c = gdal.ColorTable()
            ctable = [[0,  (192, 192, 192)],
                      [99, (0, 0, 0)],
                      [10, (121,163,121)],
                      [21, (105,30,248)],
                      [22, (255,69,0)],
                      [23, (163,67,32)],
                      [24, (105,30,248)],
                      [31, (115,255,0)],
                      [32, (248,185,24)],
                      [33, (63,221,224)],
                      [41, (228,230,194)],
                      [42, (228,230,194)],
                      [43, (228,230,194)],
                      [44, (228, 230, 194)]]
            for cid in range(0, len(ctable)):
                c.SetColorEntry(ctable[cid][0], ctable[cid][1])
            dst_ds.GetRasterBand(1).SetColorTable(c)
            dst_ds.FlushCache()
            dst_ds = None
            OUT_CLASS = None
        except Exception as e:
            log.send_error('Error writting {}_class.tif'.format(outname))
            log.send_error(str(e))
            log.close()
            time.sleep(timeSleep)
            return False

        progress = progressProp['save1']
        gdal.TermProgress(progress)

        # --------------------------------------------------------------------------
        # save  CHANGE
        # --------------------------------------------------------------------------
        try:
            if DEBUG:
                driver = gdal.GetDriverByName("GTiff")
                dst_ds = driver.Create( outname+'_forest_change.tif', out_cols, out_rows, 5, gdal.GDT_UInt16, options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
                dst_ds.SetGeoTransform( outGT )
                dst_ds.SetProjection( master_ds.GetProjectionRef())
                dst_ds.SetMetadata({'Impact_product_type': 'MFU change','Impact_operation':"degradation",'Impact_band_interpretation':'1:ND, 2:FF, 3:NFNF, 4:FNF1, 5:FNF2', 'Impact_default_bands':'4,1,5','Impact_version':IMPACT.get_version()})
                dst_ds.GetRasterBand(1).WriteArray(OUT_ND)
                progress = progressProp['save1'] + (progressProp['save2']-progressProp['save1']) / 5.0
                gdal.TermProgress(progress)
                dst_ds.GetRasterBand(2).WriteArray(OUT_FF)
                progress = progressProp['save1'] + 2*(progressProp['save2'] - progressProp['save1']) / 5.0
                gdal.TermProgress(progress)
                dst_ds.GetRasterBand(3).WriteArray(OUT_NFNF)
                progress = progressProp['save1'] + 3*(progressProp['save2'] - progressProp['save1']) / 5.0
                gdal.TermProgress(progress)
                dst_ds.GetRasterBand(4).WriteArray(OUT_FNF1)
                progress = progressProp['save1'] + 4*(progressProp['save2'] - progressProp['save1']) / 5.0
                gdal.TermProgress(progress)
                dst_ds.GetRasterBand(5).WriteArray(OUT_FNF2)
                dst_ds.FlushCache()
        except Exception as e:
            log.send_error('Error writing {}_change.tif'.format(outname))
            log.send_error(str(e))
            log.close()
            time.sleep(timeSleep)
            return False

        progress = progressProp['save2']
        gdal.TermProgress(progress)

        dst_ds = None
        OUT_ND = None
        OUT_FF = None
        OUT_NFNF = None

        # --------------------------------------------------------------------------
        # save  BIOMASS
        # --------------------------------------------------------------------------
        try:
            if DEBUG:
                driver = gdal.GetDriverByName("GTiff")
                dst_ds = driver.Create( outname+'_emissions.tif', out_cols, out_rows,3, gdal.GDT_Float32, options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ])
                dst_ds.SetGeoTransform( outGT )
                dst_ds.SetProjection( master_ds.GetProjectionRef())
                if useConversionMapBool:
                    dst_ds.SetMetadata({'Impact_product_type': 'MFU biomass', 'Impact_operation': "degradation",
                                        'Impact_band_interpretation': '1:FNF1*biomass_factor_map (tEqCO2), 2:FNF2*biomass_factor_map (tEqCO2), 3:(FNF1+FNF2)*biomass_factor_map (tEqCO2)',
                                        'Impact_default_bands': '2,2,2', 'Impact_version': IMPACT.get_version()})
                    dst_ds.GetRasterBand(1).WriteArray(OUT_EMP1 * toEqCO2)
                    progress = progressProp['save2'] + (1.0 - progressProp['save2']) / 3.0
                    gdal.TermProgress(progress)
                    dst_ds.GetRasterBand(2).WriteArray(OUT_EMP2 * toEqCO2)
                    progress = progressProp['save2'] + 2 * (1.0 - progressProp['save2']) / 3.0
                    gdal.TermProgress(progress)
                    dst_ds.GetRasterBand(3).WriteArray( (OUT_EMP1 + OUT_EMP2) * toEqCO2 )
                else:
                    dst_ds.SetMetadata({'Impact_product_type': 'MFU biomass','Impact_operation':"degradation", 'Impact_band_interpretation':'1:FNF1*biomass_factor, 2:FNF2*biomass_factor, 3:(FNF1+FNF2)*biomass_factor', 'Impact_default_bands': '2,2,2', 'Impact_version':IMPACT.get_version()})
                    dst_ds.GetRasterBand(1).WriteArray(OUT_FNF1 * biomass_value * sqrps_ha * toEqCO2)
                    progress = progressProp['save2'] + (1.0-progressProp['save2']) / 3.0
                    gdal.TermProgress(progress)
                    dst_ds.GetRasterBand(2).WriteArray(OUT_FNF2 * biomass_value * sqrps_ha * toEqCO2)
                    progress = progressProp['save2'] + 2*(1.0 - progressProp['save2']) / 3.0
                    gdal.TermProgress(progress)
                    dst_ds.GetRasterBand(3).WriteArray((OUT_FNF1 + OUT_FNF2)*biomass_value * sqrps_ha * toEqCO2)
                dst_ds.FlushCache()
        except Exception as e:
            log.send_error('Error writting {}_biomass.tif'.format(outname))
            log.send_error(str(e))
            log.close()
            time.sleep(timeSleep)
            return False

        dst_ds = None
        OUT_FNF1 = None
        OUT_FNF2 = None
        master_ds = None

        try:
            UL_LL = toLonLat(m_ulx, m_uly, projRef )
            LR_LL = toLonLat(m_lrx, m_lry, projRef )
            if useDisagShpBool:
                EMDisag = {'useDisagShpBool':useDisagShpBool,'disagElements':disagElements,
                       'EmDegP1':EMDegP1Disag,'EmDegP2':EMDegP2Disag,
                       'EmDefP1':EMDefP1Disag, 'EmDefP2':EMDefP2Disag,
                       'ArDegP1':ARDegP1Disag,'ArDegP2':ARDegP2Disag,
                       'ArDefP1':ARDefP1Disag,'ArDefP2':ARDefP2Disag
                       }
            else: EMDisag={'useDisagShpBool':False}


            for thisLang in ['EN', 'FR']:
                outHTMLFile = outname + "_report_" + thisLang + '.html'
                outXLSFile = '{}_report_{}.xlsx'.format(outname, thisLang)
                log.send_message('Report file ({}): '.format(thisLang) + outname + '_report_' + thisLang + '.html')
                log.send_message(r'<a target="_blank" href=getHtmlFile.py?file={}><b>View report result ("{}")</b></a>'.format(outHTMLFile.replace(' ','%20'), outHTMLFile))
                log.send_message('See corresponding Excel file {}'.format(outXLSFile))
                print(master_imgGeo[1])
                print(UL_LL[0])
                print(UL_LL[1])
                print(LR_LL[0])
                print(LR_LL[1])

                CarbEF_htmlout.doHTML(log, outHTMLFile,
                                      useConversionMapBool, useDisagShpBool, disagShp, master_img, conversionMapFile,
                                      thisLang, PXPTOT,
                                      useExceptMapBool, exceptMap, uniqExceptCount, uniqExceptBiom,
                                      EMDefP1TOT, EMDegP1TOT, EMDefP2TOT, EMDegP2TOT,
                                      MFUTreeDefP1AreaTOT, MFUTreeDefP2AreaTOT, MFUTreeDegP1AreaTOT,MFUTreeDegP2AreaTOT,
                                      MFUDegradationEmission1, MFUDegradationEmission2,
                                      MFUDeforestationEmission1, MFUDeforestationEmission2,
                                      master_imgGeo[1], startYY1, endYY1, startYY2, endYY2,useTwoPeriodsBool,
                                      kernel_size,
                                      biomass_value, biomassDegradPercent, forestThreshold,
                                      UL_LL[0], UL_LL[1], LR_LL[0], LR_LL[1], toEqCO2)

                CarbEF_statout.makeXlsOut(outXLSFile, log,
                                          master_img, master_imgGeo[1],
                                          UL_LL[0], UL_LL[1], LR_LL[0], LR_LL[1],
                                          startYY1, endYY1, startYY2, endYY2,
                                          kernel_size, forestThreshold,
                                          biomass_value, biomassDegradPercent,
                                          useConversionMapBool, conversionMapFile,
                                          useExceptMapBool, exceptMap, uniqExceptCount, uniqExceptBiom,
                                          useTwoPeriodsBool, useDisagShpBool, disagShp,
                                          thisLang, toEqCO2,
                                          FTOT, NFTOT, FNF1TOT, FNF2TOT, NDTOT, PXPTOT, CLASSTOT,
                                          EMDefP1TOT, EMDefP2TOT, EMDegP1TOT, EMDegP2TOT, EMDisag,
                                          EMMFUDegP1Disag, EMMFUDegP2Disag, EMMFUDefP1Disag, EMMFUDefP2Disag,
                                          ARMFUDegP1Disag, ARMFUDegP2Disag, ARMFUDefP1Disag, ARMFUDefP2Disag,
                                          MFUTreeDefP1AreaTOT, MFUTreeDefP2AreaTOT, MFUTreeDegP1AreaTOT, MFUTreeDegP2AreaTOT,
                                          MFUDegradationEmission1, MFUDegradationEmission2, MFUDeforestationEmission1, MFUDeforestationEmission2)
        except Exception as e:
            log.send_error('Error while creating report')
            log.send_error(str(e))
            time.sleep(timeSleep)
            log.close()
        gdal.TermProgress(1.0)

        log.send_message("Completed.")
        log.close()

    except Exception as e:
        log.send_error("Error in the degradation module: "+str(e))
        time.sleep(timeSleep)
        dst_ds = None
        if os.path.exists(outname):
            os.remove(outname)
        if os.path.exists(outname+'.aux.xml'):
            os.remove(outname+'.aux.xml')
        if os.path.exists(outname+"_tmp"):
            os.remove(outname+"_tmp")
        if os.path.exists(outname+'_tmp.aux.xml'):
            os.remove(outname+'_tmp.aux.xml')

        log.close()
        time.sleep(timeSleep)
        return False

# ________________________________
# main code
# parse parameters sent by DegradationCARBEF.js
# check values before sending parameters to RunDegradation_CARBEF
# ________________________________
if __name__ == "__main__":
    me, overwrite, master_img, outname, kernel_size, biomass_value, biomassDegradPercent, \
    startYY1, endYY1, startYY2, endYY2, useTwoPeriods, forest_mmu_definition, \
    useConversionMap, conversionMap, useDisagShp, disagShp, disagField, useExceptMap, exceptMap = sys.argv

    log = LogStore.LogProcessing('Degradation', 'CarbEF')

    useTwoPeriodsBool = False
    if useTwoPeriods.lower() in ['y','yes','t','true',1]:
        useTwoPeriodsBool = True

    useConversionMapBool=False
    if useConversionMap.lower() in ['y','yes','t','true','1']:
        useConversionMapBool=True
        biomass_value=-1 # must be set to allow float(biomass_value) to work in the next function call
    useDisagShpBool=False
    if useDisagShp.lower() in ['y','yes','t','true','1']: useDisagShpBool=True
    useExceptMapBool = False
    if useExceptMap.lower() in ['y','yes','t','true','1']: useExceptMapBool=True

    try:
        Run_Carbef(log, overwrite, json.loads(master_img), outname, int(kernel_size),
                            float(biomass_value), float(biomassDegradPercent),
                            int(startYY1), int(endYY1), int(startYY2), int(endYY2), useTwoPeriodsBool,
                            float(forest_mmu_definition),
                            useConversionMapBool, json.loads(conversionMap),
                            useDisagShpBool, json.loads(disagShp), disagField, useExceptMapBool, json.loads(exceptMap))
    except Exception as e:
        print('Could not call CarbEF')
        print(str(e))
        log.send_error('Could not call CarbEF. ')
        time.sleep(timeSleep)


