# -*- coding: utf-8 -*-
# this module creates an xlsx output

import xlsxwriter
import os.path
import os
import time

class carbefXLSX():

    #filename=None # fully qualified path
    #log = None
    #workbook = None
    #worksheetList = {} # dictionary: worksheet name and its pointer
    #orderedWorksheet = [] # because dictionary keys are not ordered
    #styles = {}
    #lang='EN'
    trStrings={'Main':{'EN':u'Parameters','FR':u'Paramètres '},
               'MFU':{'EN':u'Option A (MFU)','FR':u'Option A (UFM)'},
               'MFUTitle':{'EN':'Option A - Minimum Forest Unit','FR':u"Option A – Unités minimales de Forêt"},
               'Concepts':{'EN':'Concepts','FR':u'Concepts'},
               'ConceptsTitle':{'EN':'Concepts','FR':u'Concepts'},
               'please describe':{'EN':'Please describe','FR':u'Veuillez décrire'},
               'ha':{'EN':'ha','FR':u'ha'},
               'in':{'EN':'in','FR':'en'},
               'Pixels':{'EN':u'Option B (Pixels)','FR':u'Option B (Pixels)'},
               'Pixels descr 1':{'EN':'In this section, the areas and the emissions are calculated according to count of tree cover pixels loss in deforested or degraded MFUs',
                                 'FR':u"Dans cette section, les superficies et les émissions sont calculées en fonction du nombre de pixels identifiés comme pertes du couvert arboré dans les UFM déboisées ou dégradées"},
               'Pixels descr 2':{'EN':'The emissions are calculated in the same way for deforestation and forest degradation.',
                                 'FR':u"Les émissions sont calculées de la même manière pour la déforestation et la dégradation."},
               'Pixels descr 3':{'EN':'In both case, only the carbon stock that was present in the pixels that lost their tree cover is converted in CO2 emissions.',
                                 'FR':u"Dans les deux cas, seule la teneur en carbone qui était présente dans les pixels ayant perdu leur couvert arboré est convertie en émissions de CO2."},
               'Pixels descr 4':{'EN':'Area of forest at the start of the analysis (MFU are adjusted for non-tree cover presence)',
                                'FR':u"Superficie de forêt au début de l'analyse (la superficie des UFMs est ajustée pour la présence de couvert non-arboré)"},
               'Exceptions':{'EN':u'Exceptions','FR':u'Exceptions'},
               'TitleMain':{'EN':u'Synthesis report: deforested and degraded areas and associated CO2 emissions',
                                 'FR':u'Rapport de synthèse : surfaces déboisées et dégradées et émissions de CO2 associées'},
               'TitleMFU':{'EN':u'MFU','FR':u'UFM'},
               'PixelsTitle':{'EN':'Option B - Pixels of tree cover loss',
                              'FR':u"Option B – Pixels de pertes du couvert arboré"},
               'ExceptionsTitle':{'EN':u'Exceptions','FR':u'Exceptions'},
               'Geographic location':{'EN':'Geographic location','FR':'Localisation'},
               'Geographic window':{'EN':'Geographic window','FR':u'Fenêtre géographique'},
               'Country':{'EN':'Country','FR':'Pays'},
               'Zone of interest':{'EN':'Area of interest','FR':u"Zone d'intérêt"},
               'Period of analysis':{'EN':'Period of analysis and year span','FR':u"Période d'analyse et durée"},
               'Period 1':{'EN':'Period 1','FR':u'Période 1'},
               'Period 2': {'EN': 'Period 2', 'FR': u'Période 2'},
               'years':{'EN':'years','FR':u'années'},
               'Forest definition':{'EN':'Forest definition','FR':u'Définition de la forêt'},
               'Minimum Forest Unit (MFU)':{'EN':'Minimum Forest Unit (MFU)','FR':u'Unité Minimale de Forêt'},
               'Tree cover threshold':{'EN':'Tree cover Threshold','FR':u'Limite de couvert arborée'},
               'Tree cover loss map':{'EN':'Tree cover loss map','FR':u'Carte de perte de couvert arboré'},
               'Lancover shapefile':{'EN':'Lancover shapefile', 'FR':u"Shapefile d'occupation des sols"},
               'Exception map':{'EN':'Exception map', 'FR':u"Carte d'exceptions"},
               'Source':{'EN':'Source','FR':u'Source'},
               'File':{'EN':'File','FR':u'Fichier'},
               'Processing done on':{'EN':'Processing done on','FR':u'Traitement effectué le'},
               'Emission factor':{'EN':'Emission factor','FR':u"Facteur d'émission"},
               'Emission factor for deforestation':{'EN':'Emission factor for deforestation','FR':u"Facteur d'émission pour la déforestation"},
               'which is':{'EN':'which is','FR':u'qui correspond à'},
               'Emission factor for degradation':{'EN':'Option A - for forest degradation emissions, proportion of','FR':u"Option A - pour les emissions dues à la dégradation de la forêt, proportion de"},
               'of the deforestation emission factor':{'EN':'of the deforestation emission factor','FR':u"du facteur d'émission de la déforestation"},
               'Deforested and degraded areas':{'EN':'Deforested and degraded areas (proportion of tree cover in the MFU)','FR':u"Aires deboisées et degradées (en proportion de la couverture arborée dans l'UFM)"},
               'Total (ha)':{'EN':'Total (ha)','FR':'Total (ha)'},
               'Total': {'EN': 'Total', 'FR': 'Total'},
               'Total (tEqCO2)':{'EN':'Total (tEqCO2)', 'FR':u"Total (tEqCO2)"},
               'Annual rate (ha/year)':{'EN':'Annual rate (ha/year)','FR':'Taux annuel (ha/an)'},
               'Annual rate (tEQCO2/year)':{'EN': 'Annual rate (tEqCO2/year)', 'FR': u"Taux annuel (tEqCO2/an)"},
               'Deforestation':{'EN':'Deforestation','FR':u'Déforestation'},
               'Degradation':{'EN':'Degradation','FR':u'Dégradation'},
               'Degradation over the 2 periods':{'EN':'Degradation over the two periods',
                                                 'FR':u'Dégradation sur les deux périodes'},
               'MFU_describe':{'EN':'In this section, the areas and emissions are calculated according to the entire tree cover content of the MFU (the MFU area is adjusted for non-tree cover presence).',
                               'FR':u"Dans cette section, les superficies et les émissions sont calculées en fonction de la totalité de la couverture arborée de l'UFM (la zone de l'UFM est ajustée pour la présence de non-couvert arboré)"},
               'MFU_describeBis':{'EN':'For deforestation, the entire carbon content of the MFUs is converted in CO2 emissions. For forest degradation, a proportion (decided by the user) of the carbon content of the MFUs is converted in CO2 emissions.',
                                  'FR':u"Pour la déforestation, toute la teneur en carbone des UFM est convertie en émissions de CO2. Pour la dégradation des forêts, une partie (decidée par l'utilisateur) de la teneur en carbone des UFM est convertie en émissions de CO2."},
               'MFU_describeTer':{'EN':'A proportion of {}% of the MFU carbon stock is used for calculating the forest degradation emission factors',
                                  'FR':u"Une proportion de {}% du stock de carbone de l'UFM est utilisée pour calculer les facteurs d'émissions de la dégradation"},
               'MFU_describeQuattro':{'EN':'Area of forest at the start of the analysis (MFU are adjusted for non-tree cover presence)',
                                     'FR':u"Superficie de forêt au début de l'analyse (la superficie des UFM est ajustée pour la présence de couvert non-arboré)"},
               'MFU_describeCinque':{'EN':'{} ha in {}',
                                     'FR':u"{} ha en {}"},
               'C02 emissions due to deforestation and degradation':{'EN':'C02 emissions due to deforestation and degradation (proportion of tree cover in the MFU)',
                                                                     'FR':u"Emissions de CO2 dues à la déforestation et à la dégradation (en proportion de la couverture arborée de l'UFM)"},
               'Landuse breakdown':{'EN':'Landuse breakdown (proportion of tree cover in the MFU)','FR':u'Détails par occupation des sols (en proportion de la couverture arborée dans les UFM)'},
               'Period 1, deforestation':{'EN':'Period 1, deforestation', 'FR':u'Période 1, déforestation'},
               'Period 2, deforestation': {'EN': 'Period 2, deforestation', 'FR': u'Période 2, déforestation'},
               'Period 1, degradation': {'EN': 'Period 1, degradation', 'FR': u'Période 1, dégradation'},
               'Period 2, degradation': {'EN': 'Period 2, degradation', 'FR': u'Période 2, dégradation'},
               'Area (ha)':{'EN':'Area (ha)', 'FR':u'Aire (ha)'},
               'Emissions (tEqCO2)':{'EN':'Emissions (tEqCO2)','FR':u'Emissions (tEqCO2)'},
               'comment_longperiod':{'EN':'Rate is calculated with respect to the longest possible duration, from beginning of first period to the end of the last period',
                                     'FR':u'Le taux est calculé par rapport à la période la plus longue possible, du début de la première période à la fin de la seconde.'},
               'MFU Note A':{'EN':'NOTE: The total areas of MFU, irrespective of the actual tree cover are:',
                             'FR':u"Note: l'aire totale des UMF, indépendemment de leur couvert arboré est:"},
               'MFU Note B':{'EN':'Total MFU area counted as forest in 2013 before degradation and deforestation process: ',
                             'FR':u'Aire totale des UMF comptées en forêt en 2013 avant les processus de déforestation et de dégradation'},
               'Pixels deforested and degraded areas':{'EN':'Deforested and degraded areas (tree cover loss pixels)',
                                                       'FR':u"Aires déforestées et dégradées (pixels de couverture arborée perdus)"},
               'Pixels CO2 emissions due to deforestation and degradation':{'EN':'C02 emissions due to deforestation and degradation  (tree cover loss pixels)',
                                                                            'FR':u"Emissions de CO2 dues à la déforestation et à la dégradation (Pixels de couverture arborée perdus)"},
               'Pixel Landuse breakdown':{'EN':'Landuse breakdown (tree cover loss pixels)',
                                          'FR':u'Détails par occupation des sols (Pixels de couverture arborée perdus)'},
               'ExceptionsDescr':{'EN':'The user has to indicate the period to which each exception refers to. The total biomass of the area is reported, not its value equivalent CO2',
                                  'FR':u"L'utilisateur doit indiquer la période à laquelle il se réfère. La biomasse totale de l'aire est indiquée, pas sa valeur en équivalent CO2"},
               'Exception code':{'EN':'Exception code','FR':u"Code d'exception"},
               'biomass (tC/ha)':{'EN':'biomass (tC/ha)', 'FR':u"biomasse (tC/ha)"},
               'biomass (tC)':{'EN':'biomass (tC)', 'FR':u"biomasse (tC)"},
               'no exception':{'EN':'No exception map was used','FR':u"Aucune carte d'exception n'a été utilisé "},
               'fig4xlsreport_A.png':{'EN':'fig4xlsreport_A_EN.png','FR':'fig4xlsreport_A_FR.png'},
               'fig4xlsreport_B.png': {'EN': 'fig4xlsreport_B_EN.png', 'FR': 'fig4xlsreport_B_FR.png'},
               'not an exception':{'EN':'Areas out of exception codes (0)', 'FR':"Cas hors codes d'exception (0)"},
               'noIntersectionExceptionLossMap': {
                   'EN': 'The exception map does not cover the Tree cover loss map, no exeption case found.',
                   'FR': u"La carte d'exception ne recouvre pas la carte de pertes de couvert arboré, aucun cas d'exception rencontré."}

               }

    def __init__(self, fname, wrkshtLst=[], logObj=None, lang='EN'):
        self.filename = fname
        self.log = logObj
        self.lang=lang
        self.workbook = None
        self.worksheetList = {}
        self.styles={}
        self.orderedWorksheet = []
        for ii in wrkshtLst:
            trWRKSHT = self.trStrings[ii][self.lang]
            self.orderedWorksheet.append(trWRKSHT)
            self.worksheetList[trWRKSHT] = None # actual pointer will be assigned in self.create

        # check and open file
        if self.checkFile():
            self.create()
        else:
            self.__printErr__('IO error. Exit.')
            return None

    def close(self):
        if self.workbook is not None:
            self.workbook.close()

    def __printErr__(self, msg):
        if self.log is not None:
            self.log.send_error(msg)
        else:
            print(msg)

    def __printWarn__(self, msg):
        if self.log is not None:
            self.log.send_warning(msg)
        else:
            print(msg)

    def checkFile(self):
        # rule: if file exists, try to delete, else create with a different name.
        # the idea is not to fail/crash the code in case a file is already open.
        fileExists = None
        if os.path.isfile(self.filename):
            fileExists = True
        if fileExists:
            try:
                self.deleteFile()
            except (OSError, IOError) as e:
                ipos = 0
                while os.path.exists( '{}_tmp_{}.xlsx'.format( os.path.splitext(self.filename)[0], ipos ) ):
                    ipos += 1
                self.filename= '{}_tmp_{}.xlsx'.format( os.path.splitext(self.filename)[0], ipos-1 )
                self.__printWarn__('IOerror while trying to create XLSX output file (could not delete existing file, please revise); creating a temporary file {}.'.format(self.filename))
            return True
        return True

    def deleteFile(self):
        try:
            os.remove(self.filename)
        except (OSError, IOError) as e:
            self.__printWarn__('Could not delete file {}'.format(self.filename))
            raise

    def create(self):
        self.workbook = xlsxwriter.Workbook(self.filename)
        if not(self.worksheetList): #no worksheet definition
            self.worksheetList[''] = self.workbook.add_worksheet()
        else:
            for ii in self.orderedWorksheet:
                self.worksheetList[ii] = self.workbook.add_worksheet(ii)

        # add formats
        self.styles['Title'] = self.workbook.add_format({'bold':True, 'font_size':20,'font_name':'Verdana'})
        self.styles['Title1'] = self.workbook.add_format({'bold': True, 'font_size': 11})
        self.styles['Normal'] = self.workbook.add_format({'font_size':12, })
        self.styles['toFill'] = self.workbook.add_format({'font_size':12, 'color':'red','bold':True})
        self.styles['Italic'] = self.workbook.add_format({'italic':True})
        self.styles['TabHeader'] = self.workbook.add_format({'bold':True, 'align':'center'})
        self.styles['CellBorderSouth1'] = self.workbook.add_format()
        self.styles['CellBorderSouth1'].set_bottom()
        self.styles['Float2'] =  self.workbook.add_format()
        self.styles['Float2'].set_num_format('0.00')
        self.styles['Float0'] =  self.workbook.add_format()
        self.styles['Float0'].set_num_format('0')
        self.styles['RoundPercent'] = self.workbook.add_format()
        self.styles['RoundPercent'].set_num_format('0%')
        self.styles['date'] = self.workbook.add_format()
        self.styles['date'].set_num_format('d mmmm yyyy')
        self.styles['BorderNorth1'] = self.workbook.add_format()
        self.styles['BorderNorth1'].set_top(1)

    def setCol(self, wrksht, first_col, last_col, width, thisFormat):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[ self.trStrings[wrksht][self.lang] ].set_column(first_col, last_col, width, self.styles[thisFormat])

    def tabColor(self, wrksht, color):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[self.trStrings[wrksht][self.lang]].set_tab_color(color)

    def set_borderNorth(self, wrksht, row, col):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[ self.trStrings[wrksht][self.lang] ].format(row,col,self.styles['BorderNorth1'])

    def merge_range(self, wrksht, rowS, colS, rowE, colE, data, thisFormat):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[ self.trStrings[wrksht][self.lang] ].merge_range(rowS, colS, rowE, colE, data, self.styles[thisFormat])

    def write_comment(self, wrksht, row, col, text, visible=False):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[ self.trStrings[wrksht][self.lang] ].write_comment(row, col, text, {'visible':visible})

    def insert_image(self, wrksht, row, col, filename):
        # wrksht is given as in init(..., wrkshtLst, ...), i.e. not translated
        self.worksheetList[self.trStrings[wrksht][self.lang]].insert_image(row,col,filename)

    def write(self, data=[]):
        #data is an array of arrays
        for thisData in data: # data=[wrksheet, dataToWrite]
            # dataToWrite = row, col, item
            thisWrksheet = self.trStrings[thisData[0]][self.lang]
            if len(thisData[1])==3:
                self.worksheetList[thisWrksheet].write(thisData[1][0], thisData[1][1], thisData[1][2])
            else:
                self.worksheetList[thisWrksheet].write(thisData[1][0], thisData[1][1], thisData[1][2], self.styles[thisData[1][3]])

# the main hereafter is for test purpose
def makeXlsOut(outfile, log, \
               inputFile, psize, ULX, ULY, LRX, LRY, \
               startYY1, endYY1, startYY2, endYY2, \
               kernel_size, forestThreshold,\
               biomass_value, degradationPercent, \
               useConversionMapBool, conversionMapFile, \
               useExceptMapBool, exceptMap, uniqExceptCount, uniqExceptBiom,\
               useTwoPeriodsBool, useDisagShpBool, disagShp, \
               LANG, toEqCO2,\
               FTOT, NFTOT, FNF1TOT, FNF2TOT, NDTOT, PXPTOT, CLASSTOT, \
               EMDefP1TOT, EMDefP2TOT, EMDegP1TOT, EMDegP2TOT, EMDisag, \
               EMMFUDegP1Disag, EMMFUDegP2Disag, EMMFUDefP1Disag, EMMFUDefP2Disag, \
               ARMFUDegP1Disag, ARMFUDegP2Disag, ARMFUDefP1Disag, ARMFUDefP2Disag, \
               MFUTreeDefP1AreaTOT, MFUTreeDefP2AreaTOT, MFUTreeDegP1AreaTOT, MFUTreeDegP2AreaTOT,\
               MFUDegradationEmission1, MFUDegradationEmission2, MFUDeforestationEmission1, MFUDeforestationEmission2\
               ):

    myExcel = carbefXLSX(outfile, wrkshtLst=['Main','Concepts','MFU','Pixels','Exceptions'], logObj=log, lang=LANG)
    if myExcel is None:
        log.send_error('Error in makeXlsOut')
        return False

    myExcel.tabColor('Main', 'red')
    myExcel.tabColor('MFU', 'green')
    myExcel.tabColor('Pixels', 'blue')
    myExcel.tabColor('Exceptions', '#C0C0C0')

    sqrps_ha = psize * psize * 1. / 10000.0
    bxsize_ha = kernel_size * kernel_size * psize * psize * 1.0 / 10000.0
    deltaYY1 = endYY1-startYY1 + 1
    biomass_valuePx = biomass_value * sqrps_ha
    if useTwoPeriodsBool:
        deltaYY2 = endYY2-startYY2 + 1
        deltaYYTotal = endYY2 - startYY1 + 1
    else:
        deltaYY2 = None
        deltaYYTotal = deltaYY1

    dataMain1=[
        ['Main', [0,0, myExcel.trStrings['TitleMain'][myExcel.lang],'Title']],
        ['Main', [2,0, myExcel.trStrings['Geographic location'][myExcel.lang],'Title1']],
        ['Main', [3,1, myExcel.trStrings['Country'][myExcel.lang]]], ['Main', [3, 2, myExcel.trStrings['please describe'][myExcel.lang],'toFill']],
        ['Main', [5,1, myExcel.trStrings['Geographic window'][myExcel.lang]]],
        ['Main', [5, 3, 'ULX']], ['Main', [5, 4, ULX, 'Float2']], ['Main', [5, 5, 'ULY']], ['Main', [5, 6, ULY, 'Float2']],
        ['Main', [6, 3, 'LRX']], ['Main', [6, 4, LRX, 'Float2']], ['Main', [6, 5, 'LRY']], ['Main', [6, 6, LRY, 'Float2']],
        ['Main', [7,1, myExcel.trStrings['Zone of interest'][myExcel.lang]]], ['Main', [7, 2, myExcel.trStrings['please describe'][myExcel.lang],'toFill']],
        ['Main', [9,0, myExcel.trStrings['Period of analysis'][myExcel.lang],'Title1']],
        ['Main', [10,1, myExcel.trStrings['Period 1'][myExcel.lang]]], ['Main', [10, 2, startYY1]], ['Main', [10, 3, endYY1]], ['Main', [10, 4, deltaYY1]], ['Main', [10,5, myExcel.trStrings['years'][myExcel.lang]]],
        ['Main', [11,1, myExcel.trStrings['Period 2'][myExcel.lang]]], ['Main', [11, 2, startYY2]], ['Main', [11, 3, endYY2]], ['Main', [11, 4, deltaYY2]], ['Main', [11, 5, myExcel.trStrings['years'][myExcel.lang]]],
        ['Main', [13,0, myExcel.trStrings['Forest definition'][myExcel.lang],'Title1']],
        ['Main', [14,1, myExcel.trStrings['Minimum Forest Unit (MFU)'][myExcel.lang]]], ['Main',[14,2, kernel_size*kernel_size*sqrps_ha,'Float2']],['Main',[14,3,'ha']],
        ['Main', [15,1, myExcel.trStrings['Tree cover threshold'][myExcel.lang]]], ['Main', [15,2,forestThreshold*100,'Float2']],['Main', [15,3,u'%']],
        ['Main', [17,0, myExcel.trStrings['Tree cover loss map'][myExcel.lang],'Title1']],
        ['Main', [18,1,myExcel.trStrings['Source'][myExcel.lang]]], ['Main', [18, 2, myExcel.trStrings['please describe'][myExcel.lang], 'toFill']],
        ['Main', [19, 1, myExcel.trStrings['File'][myExcel.lang]]], ['Main', [19,2,inputFile]],
        ['Main', [20,1, myExcel.trStrings['Processing done on'][myExcel.lang]]], ['Main', [20,2,time.strftime("%d/%m/%Y"),'date']],
        ['Main', [21,0, myExcel.trStrings['Emission factor'][myExcel.lang], 'Title1']], ['Main', [22,1,myExcel.trStrings['Emission factor for deforestation'][myExcel.lang]]],
        ['Main', [24,1, myExcel.trStrings['Emission factor for degradation'][myExcel.lang]]], ['Main', [25,2,degradationPercent/100.0, 'RoundPercent']],['Main',[25,3,myExcel.trStrings['of the deforestation emission factor'][myExcel.lang]]]
        ]
    if useConversionMapBool: # using a map for conversion factor
        dataMain2=[
            ['Main', [23, 2, myExcel.trStrings['File'][myExcel.lang]]], ['Main', [23, 3, conversionMapFile]]
        ]
    else:
        dataMain2=[
            ['Main',[23, 2, biomass_value]],['Main',[23, 3, 'tC/ha']],['Main', [23, 4, myExcel.trStrings['which is'][myExcel.lang]]], ['Main', [23 ,5, biomass_value*toEqCO2 ]], ['Main',[23,6,'tEqCO2']]
        ]
    if useDisagShpBool:
        dataMain3=[
            ['Main', [28, 0, myExcel.trStrings['Lancover shapefile'][myExcel.lang], 'Title1']],
            ['Main', [29, 1, myExcel.trStrings['Source'][myExcel.lang]]], ['Main', [29, 2, myExcel.trStrings['please describe'][myExcel.lang], 'toFill']],
            ['Main', [30, 1, myExcel.trStrings['File'][myExcel.lang]]],
            ['Main', [30, 2, disagShp[0]]]
        ]

    myExcel.write(dataMain1)
    myExcel.write(dataMain2)
    if useDisagShpBool:
        myExcel.write(dataMain3)
    myExcel.setCol('Main', 1, 1, 28, 'Normal')

    # insert image, do translate
    try:
        log.send_message(u'{}/fig4xlsreport.png'.format(os.path.dirname(os.path.realpath(__file__))))
        myExcel.write([['Concepts', [0, 0, myExcel.trStrings[ 'ConceptsTitle' ][myExcel.lang],'Title']]])
        myExcel.insert_image('Concepts', 2,0, u'{}/{}'.format( os.path.dirname(os.path.realpath(__file__)), myExcel.trStrings['fig4xlsreport_A.png'][myExcel.lang]))
        myExcel.insert_image('Concepts', 2, 10, u'{}/{}'.format(os.path.dirname(os.path.realpath(__file__)),
                                                            myExcel.trStrings['fig4xlsreport_B.png'][myExcel.lang]))
    except IOError as e:
        myExcel.__printErr__('Could not find image fig4xlsreport.png')

    dataMFU=[
        ['MFU', [0, 0, myExcel.trStrings['MFUTitle'][myExcel.lang],'Title']],
        ['MFU', [1, 0, myExcel.trStrings['MFU_describe'][myExcel.lang]]],
        ['MFU', [2, 0, myExcel.trStrings['MFU_describeBis'][myExcel.lang]]],
        ['MFU', [3, 0, myExcel.trStrings['MFU_describeTer'][myExcel.lang].format(degradationPercent)]],
        ['MFU', [5, 0, myExcel.trStrings['MFU_describeQuattro'][myExcel.lang]]],
        ['MFU', [6, 0, round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2'] + PXPTOT['Degrad_p1'] + PXPTOT['Degrad_p2'] + PXPTOT['FF_10'] +PXPTOT['FF_2x'] + PXPTOT['FF_3x']) * sqrps_ha)]],
        ['MFU', [6, 1, 'ha {} {}'.format(myExcel.trStrings['in'][myExcel.lang], startYY1)]],
        ['MFU', [7, 0, myExcel.trStrings['Deforested and degraded areas'][myExcel.lang], 'Title1']],
        ['MFU', [9, 2, myExcel.trStrings['Total (ha)'][myExcel.lang], 'TabHeader']], ['MFU', [9, 3, myExcel.trStrings['Annual rate (ha/year)'][myExcel.lang], 'TabHeader']],
        ['MFU', [10, 1, '{}-{}'.format(startYY1,endYY1),'Italic']],
        ['MFU', [11, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]], ['MFU', [11,2,MFUTreeDefP1AreaTOT,'Float0']], ['MFU', [11,3,MFUTreeDefP1AreaTOT/deltaYY1,'Float0']],
        ['MFU', [12, 1, myExcel.trStrings['Degradation'][myExcel.lang]]], ['MFU', [12,2,MFUTreeDegP1AreaTOT,'Float0']], ['MFU', [12,3,MFUTreeDegP1AreaTOT/deltaYY1,'Float0']]
        ]
    if useTwoPeriodsBool:
        dataMFU.append(['MFU', [14, 1, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataMFU.append(['MFU', [15, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataMFU.append(['MFU', [15, 2, MFUTreeDefP2AreaTOT,'Float0']])
        dataMFU.append(['MFU', [15, 3, MFUTreeDefP2AreaTOT/deltaYY2,'Float0']])
        dataMFU.append(['MFU', [16, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataMFU.append(['MFU', [16, 2, MFUTreeDegP2AreaTOT,'Float0']])
        dataMFU.append(['MFU', [16, 3, MFUTreeDegP2AreaTOT/deltaYY2,'Float0']])
        dataMFU.append(['MFU', [18, 1, '{} {}-{}'.format(myExcel.trStrings['Total'][myExcel.lang], startYY1, endYY2), 'Italic']])
        dataMFU.append(['MFU', [19, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataMFU.append(['MFU', [19, 2, '=C12+C16','Float0']])
        dataMFU.append(['MFU', [19, 3, '=C20/{}'.format(deltaYYTotal),'Float0']])
        dataMFU.append(['MFU', [20, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataMFU.append(['MFU', [20, 2, '=C13+C17','Float0']])
        dataMFU.append(['MFU', [20, 3, '=C21/{}'.format(deltaYYTotal),'Float0']])

    dataMFUbis = [
        ['MFU', [24, 0, myExcel.trStrings['C02 emissions due to deforestation and degradation'][myExcel.lang], 'Title1']],
        ['MFU', [26, 2, myExcel.trStrings['Total (tEqCO2)'][myExcel.lang], 'TabHeader']],['MFU', [26, 3, myExcel.trStrings['Annual rate (tEQCO2/year)'][myExcel.lang], 'TabHeader']],
        ['MFU', [27, 1, '{}-{}'.format(startYY1, endYY1), 'Italic']],
        ['MFU', [28, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]], ['MFU',[28,2, toEqCO2 * MFUDeforestationEmission1,'Float0']], ['MFU',[28,3, toEqCO2*MFUDeforestationEmission1/deltaYY1,'Float0']],
        ['MFU', [29, 1, myExcel.trStrings['Degradation'][myExcel.lang]]], ['MFU',[29,2, toEqCO2*MFUDegradationEmission1,'Float0']], ['MFU',[29,3, toEqCO2*MFUDegradationEmission1/deltaYY1,'Float0']]
            ]
    if useTwoPeriodsBool:
        dataMFUbis.append(['MFU', [31, 1, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataMFUbis.append(['MFU', [32, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataMFUbis.append(['MFU', [32,2, toEqCO2*MFUDeforestationEmission2,'Float0']])
        dataMFUbis.append(['MFU', [32,3, toEqCO2*MFUDeforestationEmission2/deltaYY2,'Float0']])
        dataMFUbis.append(['MFU', [33, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataMFUbis.append(['MFU', [33,2, toEqCO2*MFUDegradationEmission2,'Float0']])
        dataMFUbis.append(['MFU', [33,3, toEqCO2*MFUDegradationEmission2/deltaYY2,'Float0']])
        dataMFUbis.append(['MFU', [35, 1, '{} {}-{}'.format(myExcel.trStrings['Total'][myExcel.lang], startYY1, endYY2), 'Italic']])
        dataMFUbis.append(['MFU', [36, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataMFUbis.append(['MFU', [36,2,'=C29+C33','Float0']])
        dataMFUbis.append(['MFU', [36,3,'=C37/{}'.format(deltaYYTotal),'Float0']])
        dataMFUbis.append(['MFU', [37, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataMFUbis.append(['MFU', [37,2,'=(C30+C34)','Float0']])
        dataMFUbis.append(['MFU', [37,3,'=C38/{}'.format(deltaYYTotal),'Float0']])

    dataMFUNote=[
        ['MFU', [8, 5, myExcel.trStrings['MFU Note A'][myExcel.lang]]],
        ['MFU', [9, 5, myExcel.trStrings['MFU Note B'][myExcel.lang]]],
        ['MFU', [10, 5, round((CLASSTOT['21'] + CLASSTOT['24'] + CLASSTOT['22'] + CLASSTOT['23'] + CLASSTOT['31'] +CLASSTOT['32'] + CLASSTOT['33'] + CLASSTOT['10']) * bxsize_ha),'Float0']],
        ['MFU', [10, 6, 'ha']],
        ['MFU', [12, 6, myExcel.trStrings['Total (ha)'][myExcel.lang],'TabHeader']],
        ['MFU', [13, 5, '{}-{}'.format(startYY1, endYY1), 'Italic']],
        ['MFU', [14, 5, myExcel.trStrings['Deforestation'][myExcel.lang]]], ['MFU', [14, 6, round((CLASSTOT['21'] + CLASSTOT['24']) * bxsize_ha), 'Float0']],
        ['MFU', [15, 5, myExcel.trStrings['Degradation'][myExcel.lang]]], ['MFU', [15, 6, round(CLASSTOT['31'] * bxsize_ha), 'Float0']],
        ]
    if useTwoPeriodsBool:
        dataMFUNote.append(['MFU', [17, 5, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataMFUNote.append(['MFU', [18, 5, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataMFUNote.append(['MFU', [18, 6, round((CLASSTOT['22'] + CLASSTOT['23']) * bxsize_ha),'Float0']])
        dataMFUNote.append(['MFU', [19, 5, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataMFUNote.append(['MFU', [19, 6, round(CLASSTOT['32'] * bxsize_ha), 'Float0']])
        dataMFUNote.append(['MFU', [21, 5, '{}-{}'.format(startYY1, endYY2), 'Italic']])
        dataMFUNote.append(['MFU', [22, 5, myExcel.trStrings['Degradation over the 2 periods'][myExcel.lang]]])
        dataMFUNote.append(['MFU', [22, 6, round(CLASSTOT['33'] * bxsize_ha),'Float0']])

    myExcel.write(dataMFU)
    myExcel.write(dataMFUbis)
    myExcel.write(dataMFUNote)

    myExcel.setCol('MFU', 1, 2, 28, 'Normal')
    myExcel.setCol('MFU',2, 9,28,'Normal')
    myExcel.write_comment('MFU', 18,1, myExcel.trStrings['comment_longperiod'][myExcel.lang])
    myExcel.write_comment('MFU', 35, 1, myExcel.trStrings['comment_longperiod'][myExcel.lang])

    dataPixelNote = [
        ['Pixels', [3+5, 5, myExcel.trStrings['MFU Note A'][myExcel.lang]]],
        ['Pixels', [4+5, 5, myExcel.trStrings['MFU Note B'][myExcel.lang]]],
        ['Pixels', [5+5, 5, round((CLASSTOT['21'] + CLASSTOT['24'] + CLASSTOT['22'] + CLASSTOT['23'] + CLASSTOT['31'] +CLASSTOT['32'] + CLASSTOT['33'] + CLASSTOT['10']) * bxsize_ha),'Float0']],
        ['Pixels', [5+5,6,'ha']],
        ['Pixels', [7+5, 6, myExcel.trStrings['Total (ha)'][myExcel.lang],'TabHeader']],
        ['Pixels', [8+5, 5, '{}-{}'.format(startYY1, endYY1), 'Italic']],
        ['Pixels', [9+5, 5, myExcel.trStrings['Deforestation'][myExcel.lang]]], ['Pixels', [9+5, 6, round((CLASSTOT['21'] + CLASSTOT['24']) * bxsize_ha), 'Float0']],
        ['Pixels', [10+5, 5, myExcel.trStrings['Degradation'][myExcel.lang]]], ['Pixels', [10+5, 6, round(CLASSTOT['31'] * bxsize_ha), 'Float0']],
        ]
    if useTwoPeriodsBool:
        dataPixelNote.append(['Pixels', [12+5, 5, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataPixelNote.append(['Pixels', [13+5, 5, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataPixelNote.append(['Pixels', [13+5, 6, round((CLASSTOT['22'] + CLASSTOT['23']) * bxsize_ha),'Float0']])
        dataPixelNote.append(['Pixels', [14+5, 5, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataPixelNote.append(['Pixels', [14+5, 6, round(CLASSTOT['32'] * bxsize_ha), 'Float0']])
        dataPixelNote.append(['Pixels', [16+5, 5, '{}-{}'.format(startYY1, endYY2), 'Italic']])
        dataPixelNote.append(['Pixels', [17+5, 5, myExcel.trStrings['Degradation over the 2 periods'][myExcel.lang]]])
        dataPixelNote.append(['Pixels', [17+5, 6, round(CLASSTOT['33'] * bxsize_ha),'Float0']])

    dataPixels=[
        ['Pixels',[0,0, myExcel.trStrings['PixelsTitle'][myExcel.lang],'Title']],
        ["Pixels",[1,0,myExcel.trStrings['Pixels descr 1'][myExcel.lang]]],
        ["Pixels",[2,0,myExcel.trStrings['Pixels descr 2'][myExcel.lang]]],
        ["Pixels", [3, 0, myExcel.trStrings['Pixels descr 3'][myExcel.lang]]],
        ['Pixels', [5, 0, myExcel.trStrings['Pixels descr 4'][myExcel.lang]]],
        ['Pixels',[6,0, round((PXPTOT['Deforest_p1'] + PXPTOT['Deforest_p2'] + PXPTOT['Degrad_p1'] + PXPTOT['Degrad_p2'] + PXPTOT['FF_10'] +PXPTOT['FF_2x'] + PXPTOT['FF_3x']) * sqrps_ha),'Float0']],
        ['Pixels',[6,1,'ha {} {}'.format(myExcel.trStrings['in'][myExcel.lang], startYY1)]],
        ['Pixels',[7,0,myExcel.trStrings['Pixels deforested and degraded areas'][myExcel.lang], 'Title1']],
        ['Pixels',[9,2, myExcel.trStrings['Total (ha)'][myExcel.lang], 'TabHeader']], ['Pixels', [9,3, myExcel.trStrings['Annual rate (ha/year)'][myExcel.lang],'TabHeader']],
        ['Pixels', [10, 1, '{}-{}'.format(startYY1, endYY1), 'Italic']],
        ['Pixels', [11, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]], ['Pixels',[11,2, round(PXPTOT['Deforest_p1'] * sqrps_ha),'Float0']], ['Pixels',[11,3,'=C12/{}'.format(deltaYY1),'Float0']],
        ['Pixels', [12, 1, myExcel.trStrings['Degradation'][myExcel.lang]]], ['Pixels',[12,2, round(PXPTOT['Degrad_p1'] * sqrps_ha),'Float0']],
        ['Pixels', [12, 3, '=C13/{}'.format(deltaYY1), 'Float0']],
        ['Pixels', [24, 0, myExcel.trStrings['Pixels CO2 emissions due to deforestation and degradation'][ myExcel.lang], 'Title1']],
        ['Pixels', [26, 2, myExcel.trStrings['Total (tEqCO2)'][myExcel.lang], 'TabHeader']],
        ['Pixels', [26, 3, myExcel.trStrings['Annual rate (tEQCO2/year)'][myExcel.lang], 'TabHeader']],
        ['Pixels', [27, 1, '{}-{}'.format(startYY1, endYY1), 'Italic']]
        ]

    if useTwoPeriodsBool:
        dataPixels.append(['Pixels', [14, 1, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataPixels.append(['Pixels', [15, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataPixels.append(['Pixels',[15,2, round(PXPTOT['Deforest_p2'] * sqrps_ha),'Float0']])
        dataPixels.append(['Pixels',[15,3,'=C16/{}'.format(deltaYY2),'Float0']])
        dataPixels.append(['Pixels', [16, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataPixels.append(['Pixels',[16,2,round(PXPTOT['Degrad_p2'] * sqrps_ha),'Float0']])
        dataPixels.append(['Pixels',[16,3,'=C17/{}'.format(deltaYY2),'Float0']])
        dataPixels.append(['Pixels', [18, 1, '{}-{}'.format(startYY1, endYY2), 'Italic']])
        dataPixels.append(['Pixels', [19, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
        dataPixels.append(['Pixels',[19,2,'=C12+C16','Float0']])
        dataPixels.append(['Pixels',[19,3,'=C20/{}'.format(deltaYYTotal),'Float0']])
        dataPixels.append(['Pixels', [20, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
        dataPixels.append(['Pixels',[20,2,'=C13+C17','Float0']])
        dataPixels.append(['Pixels',[20,3,'=C21/{}'.format(deltaYYTotal),'Float0']])
        dataPixels.append(['Pixels', [31, 1, '{}-{}'.format(startYY2, endYY2), 'Italic']])
        dataPixels.append(['Pixels', [35, 1, '{}-{}'.format(startYY1, endYY2), 'Italic']])

    myExcel.write(dataPixels)
    myExcel.write(dataPixelNote)
    myExcel.setCol('Pixels',1,6,28,'Normal')

    if useConversionMapBool:
        dataPixels2= [
            ['Pixels', [28, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]],
            ['Pixels', [28, 2, round(EMDefP1TOT * toEqCO2),'Float0']], ['Pixels', [28, 3, '=C29/{}'.format(deltaYY1),'Float0']],
            ['Pixels', [29, 1, myExcel.trStrings['Degradation'][myExcel.lang]]],
            ['Pixels', [29, 2, round(EMDegP1TOT * toEqCO2),'Float0']], ['Pixels', [29, 3, '=C30/{}'.format(deltaYY1),'Float0']],
            ['Pixels', [32, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]],
            ]
        if useTwoPeriodsBool:
            dataPixels2.append(['Pixels', [32, 2, round(EMDefP2TOT * toEqCO2),'Float0']])
            dataPixels2.append(['Pixels', [32, 3, '=C33/{}'.format(deltaYY2),'Float0']])
            dataPixels2.append(['Pixels', [33, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [33, 2, round(EMDegP2TOT * toEqCO2),'Float0']])
            dataPixels2.append(['Pixels', [33, 3, '=C34/{}'.format(deltaYY2),'Float0']])
            dataPixels2.append(['Pixels', [36, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [36, 2, '=C29+C33','Float0']])
            dataPixels2.append(['Pixels', [36, 3, '=C37/{}'.format(deltaYYTotal),'Float0']])
            dataPixels2.append(['Pixels', [37, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [37, 2, '=C30+C34','Float0']])
            dataPixels2.append(['Pixels', [37, 3, '=C38/{}'.format(deltaYYTotal),'Float0']])
    else: # case where biomass is a constant
        dataPixels2 =[
            ['Pixels', [28, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]],
            ['Pixels', [28, 2, round(PXPTOT['Deforest_p1'] * toEqCO2 * biomass_valuePx),'Float0']], ['Pixels', [28, 3, '=C29/{}'.format(deltaYY1),'Float0']],
            ['Pixels', [29, 1, myExcel.trStrings['Degradation'][myExcel.lang]]],
            #['Pixels', [29, 2, round(PXPTOT['Degrad_p1'] * toEqCO2 * degradationPercent * biomass_valuePx / 100.0),'Float0']],
            ['Pixels', [29, 2, round(PXPTOT['Degrad_p1'] * toEqCO2 * biomass_valuePx ), 'Float0']],
            ['Pixels', [29, 3, '=C30/{}'.format(deltaYY1),'Float0']],
            ]
        if useTwoPeriodsBool:
            dataPixels2.append(['Pixels', [32, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [32, 2, round(PXPTOT['Deforest_p2'] * toEqCO2 * biomass_valuePx),'Float0']])
            dataPixels2.append(['Pixels', [32, 3, '=C33/{}'.format(deltaYY2),'Float0']])
            dataPixels2.append(['Pixels', [33, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
            #dataPixels2.append(['Pixels', [33, 2, round(PXPTOT['Degrad_p2'] * toEqCO2 * degradationPercent * biomass_valuePx / 100.0),'Float0']])
            dataPixels2.append(['Pixels', [33, 2, round(PXPTOT['Degrad_p2'] * toEqCO2 * biomass_valuePx), 'Float0']])
            dataPixels2.append(['Pixels', [33, 3, '=C34/{}'.format(deltaYY2),'Float0']])
            dataPixels2.append(['Pixels', [36, 1, myExcel.trStrings['Deforestation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [36, 2, '=C29+C33','Float0']])
            dataPixels2.append(['Pixels', [36, 3, '=C37/{}'.format(deltaYYTotal),'Float0']])
            dataPixels2.append(['Pixels', [37, 1, myExcel.trStrings['Degradation'][myExcel.lang]]])
            dataPixels2.append(['Pixels', [37, 2, '=C30+C34','Float0']])
            dataPixels2.append(['Pixels', [37, 3, '=C38/{}'.format(deltaYYTotal),'Float0']])

    myExcel.write(dataPixels2)
    myExcel.write_comment('Pixels', 18,1, myExcel.trStrings['comment_longperiod'][myExcel.lang])
    myExcel.write_comment('Pixels', 35, 1, myExcel.trStrings['comment_longperiod'][myExcel.lang])

    if useDisagShpBool:
        dataMFU2=[
            ['MFU', [39, 0, myExcel.trStrings['Landuse breakdown'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 2, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 3, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 4, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 5, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 6, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 7, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 8, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['MFU', [41, 9, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
        ]
        myExcel.merge_range('MFU', 40, 2, 40, 3, myExcel.trStrings['Period 1, deforestation'][myExcel.lang], 'TabHeader')
        myExcel.merge_range('MFU', 40, 4, 40, 5, myExcel.trStrings['Period 1, degradation'][myExcel.lang], 'TabHeader')
        myExcel.merge_range('MFU', 40, 6, 40, 7, myExcel.trStrings['Period 2, deforestation'][myExcel.lang],'TabHeader')
        myExcel.merge_range('MFU', 40, 8, 40, 9, myExcel.trStrings['Period 2, degradation'][myExcel.lang], 'TabHeader')
        ipos=42
        for ii in EMMFUDefP1Disag:
            dataMFU2.append(['MFU', [ipos, 1, ii]])
            dataMFU2.append(['MFU', [ipos, 2, ARMFUDefP1Disag[ii], 'Float0']])
            dataMFU2.append(['MFU', [ipos, 3, toEqCO2 * EMMFUDefP1Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 4, ARMFUDegP1Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 5, toEqCO2 * EMMFUDegP1Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 6, ARMFUDefP2Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 7, toEqCO2 * EMMFUDefP2Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 8, ARMFUDegP2Disag[ii],'Float0']])
            dataMFU2.append(['MFU', [ipos, 9, toEqCO2 * EMMFUDegP2Disag[ii],'Float0']])
            ipos+=1
        myExcel.write(dataMFU2)

        dataPixels3=[
            ['Pixels', [38+1, 0, myExcel.trStrings['Pixel Landuse breakdown'][myExcel.lang],'Title1']],
            ['Pixels', [40+1, 2, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 3, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 4, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 5, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 6, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 7, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 8, myExcel.trStrings['Area (ha)'][myExcel.lang], 'TabHeader']],
            ['Pixels', [40+1, 9, myExcel.trStrings['Emissions (tEqCO2)'][myExcel.lang], 'TabHeader']],
        ]
        ipos=41+1
        keyList = sorted(EMDisag['ArDefP1'])
        for ii in keyList:
            dataPixels3.append(['Pixels', [ipos, 1, ii]])
            dataPixels3.append(['Pixels', [ipos, 2, EMDisag['ArDefP1'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 3, toEqCO2 * EMDisag['EmDefP1'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 4, EMDisag['ArDegP1'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 5, toEqCO2 * EMDisag['EmDegP1'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 6, EMDisag['ArDefP2'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 7, toEqCO2 * EMDisag['EmDefP2'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 8, EMDisag['ArDegP2'][ii], 'Float0']])
            dataPixels3.append(['Pixels', [ipos, 9, toEqCO2 * EMDisag['EmDegP2'][ii], 'Float0']])
            ipos+=1
        myExcel.write(dataPixels3)
        myExcel.merge_range('Pixels', 39+1, 2, 39+1, 3, myExcel.trStrings['Period 1, deforestation'][myExcel.lang],'TabHeader')
        myExcel.merge_range('Pixels', 39+1, 4, 39+1, 5, myExcel.trStrings['Period 1, degradation'][myExcel.lang], 'TabHeader')
        myExcel.merge_range('Pixels', 39+1, 6, 39+1, 7, myExcel.trStrings['Period 2, deforestation'][myExcel.lang], 'TabHeader')
        myExcel.merge_range('Pixels', 39+1, 8, 39+1, 9, myExcel.trStrings['Period 2, degradation'][myExcel.lang], 'TabHeader')
        myExcel.setCol('Pixels', 2, 9, 28, 'Normal')

    if not useExceptMapBool:
        dataExceptions = [
            ['Exceptions', [0, 0, myExcel.trStrings['Exceptions'][myExcel.lang], 'Title']],
            ['Exceptions', [2, 0, myExcel.trStrings['no exception'][myExcel.lang]]]
        ]
        myExcel.write(dataExceptions)
    else:
        dataExceptions=[['Exceptions', [0, 0, myExcel.trStrings['ExceptionsTitle'][myExcel.lang],'Title']],
                        ['Exceptions', [1, 0, myExcel.trStrings['Exception map'][myExcel.lang],'Title1']], ['Exceptions', [1, 1, exceptMap[0]]],
                        ['Exceptions', [2, 1, myExcel.trStrings['Source'][myExcel.lang]]],
                        ['Exceptions', [2, 2, myExcel.trStrings['please describe'][myExcel.lang], 'toFill']],
                        ['Exceptions', [3, 0, myExcel.trStrings['Exception code'][myExcel.lang],'TabHeader']],
                        ['Exceptions', [3, 1, myExcel.trStrings['Area (ha)'][myExcel.lang],'TabHeader']],
                        ['Exceptions', [3, 2, myExcel.trStrings['biomass (tC)'][myExcel.lang],'TabHeader']]
                        ]
        ipos = 4
        anycase=False
        for ii in uniqExceptCount:
            if ii !=0:
                dataExceptions.append(['Exceptions', [ipos, 0, ii]])
                dataExceptions.append(['Exceptions', [ipos, 1, uniqExceptCount[ii], 'Float0']])
                dataExceptions.append(['Exceptions', [ipos, 2, uniqExceptBiom[ii]*psize*psize/10000.0, 'Float0']])
                ipos += 1
                anycase=True
        if not anycase:
            dataExceptions.append(['Exceptions', [ipos, 0, myExcel.trStrings['noIntersectionExceptionLossMap'][myExcel.lang]]])
        myExcel.write(dataExceptions)
        myExcel.setCol('Exceptions', 0, 2, 28, 'Normal')

    myExcel.close()
    myExcel = None
    del myExcel


