#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.





# *************************   IMPORTANT  *************************
    #  WRONG and DIFFERENT RES by USING  the below lines
    #         numpy.nansum(BIOMASS_DIFF[DEFO], dtype=numpy.float32)
    #         numpy.nansum(BIOMASS_DIFF*DEFO , dtype=numpy.float32)

     # correct stats for each class are computed by using unique and count
     #        val, ct = numpy.unique(BIOMASS_DIFF[DEFO * map1mask],return_counts=True)
     #        multArray = val*ct
     #        bioSum = numpy.nansum(multArray)

      # BIOMASS_DIFF.sum()  gives wrong number , sum is computet per class


# Import global libraries
import os
import sys
import json
import numpy
import numpy.ma as ma

import subprocess

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr, gdal
except ImportError:
    import ogr, osr, gdal

# Import local libraries
import LogStore
import IMPACT
from Simonets_PROC_libs import *
import ImageProcessing


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')


def clean_tmp_file(tmpindir):
    try:
        for root, dirs, files in os.walk(tmpindir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
    except Exception as e:
        pass
    return "1"


def rgb2hex(r, g, b):
    return "'#{:02x}{:02x}{:02x}'".format(r, g, b)


def RunForesER(mapT1, mapT2, disturbMap, user_biomass_map, biomass_value, biomassDegradPercent, useConversionMap, out_name, overwrite, outEPSG, user_nodata):

    try:
        out_name = out_name.replace('.tif', '')

        log = LogStore.LogProcessing('ForesER', 'ForestER')
        log.set_input_file(mapT1)
        log.set_input_file(mapT2)
        log.set_parameter('Disturbance Map ', disturbMap)
        log.set_parameter('Biomass Map ', user_biomass_map)
        log.set_parameter('Biomass value ', biomass_value)
        log.set_parameter('Nodata value ', user_nodata)
        log.set_parameter('Use Biomass Map', useConversionMap)
        log.set_parameter('Emission by Degradation', biomassDegradPercent)
        log.set_parameter('Processing EPSG code ', outEPSG)
        log.set_parameter('Output report name', out_name + '.html')
        log.set_parameter('Overwrite ', overwrite)
        ImageProcessing.validPathLengths(out_name, None, 1, log)
        if user_nodata != '':
            user_nodata = float(user_nodata)

        DEBUG = False


        mapT1 = json.loads(mapT1)[0]
        mapT2 = json.loads(mapT2)[0]
        if useConversionMap.lower() in ['y', 'yes', 't', 'true', '1']:
            useConversionMap = True
            user_biomass_map = json.loads(user_biomass_map)[0]

            user_biomass_map_user = user_biomass_map
        else:
            useConversionMap = False
            user_biomass_map = ''
            user_biomass_map_user = ''

        print('Use Bio Map:',useConversionMap)

        if disturbMap == '[]':
            disturbMap = ''
        else:
            disturbMap = json.loads(disturbMap)[0]


        try:
            # Extract forest type and related biomass
            biomass_value_perClass = biomass_value.split(',')
            forestTypes_num = len(biomass_value_perClass)
            print('forestTypes_num')
            print(forestTypes_num)
            ClassMinMaxBiom = []
            for x in range(0,forestTypes_num,3):
                if biomass_value_perClass[x] != '' and biomass_value_perClass[x+1] != '':
                    if biomass_value_perClass[x+2] == '':
                        biomass_value_perClass[x + 2] = 0.0
                    ClassMinMaxBiom.append([int(biomass_value_perClass[x]),int(biomass_value_perClass[x+1]),float(biomass_value_perClass[x+2])])

            print('ClassMinMaxBiom')
            print(ClassMinMaxBiom)

        except Exception as e:
            print("Error: " + str(e))
            pass
        if int(biomassDegradPercent) > 0:
            biomassDegradPercent = float(biomassDegradPercent)/100.
        else :
            biomassDegradPercent = 0

        if not os.path.exists(mapT1) :
            log.send_error('Wrong Map 1 ')
            log.close()
            return False
        if not os.path.exists(mapT2):
            log.send_error('Wrong Map 1')
            log.close()
            return False

        if useConversionMap and not os.path.exists(user_biomass_map):
            log.send_error('Wrong Biomass map1')
            log.close()
            return False

        if disturbMap != '' and not os.path.exists(disturbMap):
            log.send_error('Wrong disturbance map')
            log.close()
            return False

        # -------------------------------------------------------------------
        # ------------------------- TMP directory  ---------------------------
        # -------------------------------------------------------------------
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "ForestER")
        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)
        else:
            clean_tmp_file(tmp_indir)


        # ----------------------------------------------------
        # ------          NAME out file     ------------------
        # ----------------------------------------------------
        ChangeMap = out_name + '_biomass_change'
        ChangeMapClassPNG = out_name + '_biomass_change.png'
        ChangeMapClass = out_name + '_biomass_change_class'
        EmissionMap = out_name + '_emission'


        if os.path.exists(out_name + '.html'):
            if overwrite == 'Yes':
                try:
                    os.remove(out_name + '.html')

                except Exception as e:
                    log.send_error('Cannot delete output file '+str(e))
                    log.close()
                    return False
            else:
                log.send_warning(out_name + ' <b> Already Exists </b>')
                log.close()
                return True

        if os.path.exists(ChangeMap+'.tif'):
            os.remove(ChangeMap+'.tif')
        if os.path.exists(EmissionMap+'.tif'):
            os.remove(EmissionMap+'.tif')
        if os.path.exists(ChangeMapClass+'.tif'):
            os.remove(ChangeMapClass+'.tif')


        log.send_message("Overlap Test ")
        # -------------------------------------------------------------------
        # ------------------------- TEST OVERLAPS  -------------------------
        # -------------------------------------------------------------------

        if not test_overlapping(mapT1,mapT2):
            log.send_error("No overlap between: "+mapT1+" "+mapT2)
            log.close()
            return False

        log.send_message("Overlap Map1 & Map2: OK")

        if disturbMap != '' and not(test_overlapping(disturbMap,mapT1) and test_overlapping(disturbMap,mapT2)):
            log.send_error("No overlap with 2 input maps and disturbance map: "+disturbMap)
            log.close()
            return False
        log.send_message("Overlap Disturbance & Maps: OK")

        if user_biomass_map != '' and not (test_overlapping(user_biomass_map, mapT1) and test_overlapping(user_biomass_map, mapT2)):
            log.send_error("No overlap with imput maps T1,T2 and Biomass ")
            log.close()
            return False
        log.send_message("Overlap Biomass & Maps: OK")


        if user_biomass_map != '' and disturbMap != '' and not test_overlapping(user_biomass_map, disturbMap):
            log.send_error("No overlap between biomass and disturbance map")
            log.close()
            return False
        log.send_message("Overlap Biomass & Disturbance: OK ")

        mapT1_tmp = os.path.join(tmp_indir, os.path.basename(mapT1).replace('.tif','_1.tif'))
        mapT2_tmp = os.path.join(tmp_indir, os.path.basename(mapT2).replace('.tif','_2.tif'))


        outProj4 = subprocess.getoutput('gdalsrsinfo -o proj4 '+ outEPSG).replace("\n",'')
        if 'ERROR' in outProj4:
            log.send_error(" Error while converting "+outEPSG+" to Proj4")
            print(outProj4)
        else:
            outEPSG = outProj4
        # if outEPSG == "ESRI:102022":
        #     outEPSG = "+proj=aea +lat_0=0 +lon_0=25 +lat_1=20 +lat_2=-23 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs +type=crs"
        #
        # if outEPSG == "ESRI:102022":
        #     outEPSG = "+proj=aea +lat_0=0 +lon_0=25 +lat_1=20 +lat_2=-23 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs +type=crs"
        #


        print("OUT:",outEPSG)
        print("----")


        T1_ds = gdal.Warp(mapT1_tmp, mapT1, dstSRS=str(outEPSG), format='Gtiff', options=['compress=lzw', 'bigtiff=IF_SAFER','-dstalpha'])
        T2_ds = gdal.Warp(mapT2_tmp, mapT2, dstSRS=str(outEPSG), format='Gtiff', options=['compress=lzw', 'bigtiff=IF_SAFER','-dstalpha'])

        # datatype1 = gdal.GetDataTypeName(T1_ds.GetRasterBand(1).DataType)
        # datatype2 = gdal.GetDataTypeName(T1_ds.GetRasterBand(1).DataType)



        T1_alpha_ds = gdal.Translate(mapT1_tmp+'alpha.vrt', T1_ds, format='VRT', bandList=[2])
        T2_alpha_ds = gdal.Translate(mapT2_tmp+'alpha.vrt', T2_ds, format='VRT', bandList=[2])

        geoTransform = T1_ds.GetGeoTransform()
        ulx = geoTransform[0]
        uly = geoTransform[3]
        lrx = ulx + geoTransform[1] * T1_ds.RasterXSize
        lry = uly + geoTransform[5] * T1_ds.RasterYSize

        geoTransform = T2_ds.GetGeoTransform()
        ulx1 = geoTransform[0]
        uly1 = geoTransform[3]
        lrx1 = ulx1 + geoTransform[1] * T2_ds.RasterXSize
        lry1 = uly1 + geoTransform[5] * T2_ds.RasterYSize

        wkt1 = "POLYGON ((" + str(ulx) + " " + str(uly) + "," + str(lrx) + " " + str(uly) + "," + str(lrx) + " " + str(lry) + "," + str(ulx) + " " + str(lry) + "," + str(ulx) + " " + str(uly) + "))"
        wkt2 = "POLYGON ((" + str(ulx1) + " " + str(uly1) + "," + str(lrx1) + " " + str(uly1) + "," + str(lrx1) + " " + str(lry1) + "," + str(ulx1) + " " + str(lry1) + "," + str(ulx1) + " " + str(uly1) + "))"

        poly1 = ogr.CreateGeometryFromWkt(wkt1)
        poly2 = ogr.CreateGeometryFromWkt(wkt2)

        intersection = poly1.Intersection(poly2)
        VALIDEXTENT = intersection.GetEnvelope()

        if disturbMap != '':
            disturbMap_tmp = os.path.join(tmp_indir, os.path.basename(disturbMap))
            gdal.Warp(disturbMap_tmp, disturbMap, dstSRS=str(outEPSG), format='Gtiff',options=['compress=lzw', 'bigtiff=IF_SAFER'])
            disturbMap = disturbMap_tmp

        if user_biomass_map != '':
            user_biomass_map_tmp = os.path.join(tmp_indir, os.path.basename(user_biomass_map))

            #user_biomass_map = user_biomass_map_tmp
            Biom_ds = gdal.Warp(user_biomass_map_tmp, user_biomass_map, dstSRS=str(outEPSG), format='Gtiff',options=['compress=lzw', 'bigtiff=IF_SAFER'])



            geoTransform = Biom_ds.GetGeoTransform()
            ulx3 = geoTransform[0]
            uly3 = geoTransform[3]
            lrx3 = ulx3 + geoTransform[1] * Biom_ds.RasterXSize
            lry3 = uly3 + geoTransform[5] * Biom_ds.RasterYSize
            wkt3 = "POLYGON ((" + str(ulx3) + " " + str(uly3) + "," + str(lrx3) + " " + str(uly3) + "," + str(lrx3) + " " + str(lry3) + "," + str(ulx3) + " " + str(lry3) + "," + str(ulx3) + " " + str(uly3) + "))"
            poly3 = ogr.CreateGeometryFromWkt(wkt3)




            intersection = intersection.Intersection(poly3)

            VALIDEXTENT = intersection.GetEnvelope()
            user_biomass_map = user_biomass_map_tmp

            Biom_ds = None





        EXTENT = str(VALIDEXTENT[0]) + ' ' + str(VALIDEXTENT[2]) + ' ' + str(VALIDEXTENT[1]) + ' ' + str(VALIDEXTENT[3])
        TMP_VRT_MAPS = os.path.join(tmp_indir, 'mapT12_biom_disturb.vrt')
        TMP_VRT_ALPHA_MAPS = os.path.join(tmp_indir, 'mapT12_biom_disturb_alpha.vrt')
        os.system('gdalbuildvrt -resolution highest -separate -te ' + EXTENT + ' -b 1 ' + TMP_VRT_MAPS + ' ' + mapT1_tmp + ' ' + mapT2_tmp + ' ' + user_biomass_map + ' ' + disturbMap)
        os.system('gdalbuildvrt -resolution highest -separate -te ' + EXTENT + ' ' + TMP_VRT_ALPHA_MAPS + ' ' + mapT1_tmp + 'alpha.vrt ' + mapT2_tmp + 'alpha.vrt ' + user_biomass_map + ' ' + disturbMap)
        # get geo info from warped and staked files
        mapT12_ds = gdal.Open(TMP_VRT_MAPS, gdal.GA_ReadOnly)
        master_imgGeo = mapT12_ds.GetGeoTransform()
        master_projRef = mapT12_ds.GetProjectionRef()
        master_cols = mapT12_ds.RasterXSize
        master_rows = mapT12_ds.RasterYSize
        masterXorigin = master_imgGeo[0]
        masterYorigin = master_imgGeo[3]
        masterPixelWidth = master_imgGeo[1]
        masterPixelHeight = master_imgGeo[5]
        MasterXEnd = masterXorigin + masterPixelWidth * master_cols
        MasterYEnd = masterYorigin + masterPixelHeight * master_rows


        # -------------  EXTRACT alpha band to remove nodata introduced by warp  --------------------------
        mapT12_alpha_ds = gdal.Open(TMP_VRT_ALPHA_MAPS, gdal.GA_ReadOnly)
        ALPHA_T1 = mapT12_alpha_ds.GetRasterBand(1).ReadAsArray().astype(numpy.byte)
        ALPHA_T2 = mapT12_alpha_ds.GetRasterBand(2).ReadAsArray().astype(numpy.byte)
        mapT12_alpha_ds = None
        T1_alpha_ds = None
        T2_alpha_ds = None


        # reset Extent in case of miss allignement (1 pixel )
        print("New Extent after stack")
        EXTENT = str(masterXorigin) + ' ' + str(MasterYEnd) + ' ' + str(MasterXEnd) + ' ' + str(masterYorigin)
        log.send_message("EXTENT:" + EXTENT)


        # conversion between pixel and hectars
        conversionFactor = ((master_imgGeo[1] * abs(master_imgGeo[5]))) / 10000.
        # conversionFactor = 1.0

        print(conversionFactor)
        log.set_parameter('Conversion factor ha/pixel: ', conversionFactor)

        print('Reading Map1')
        MAP1 = mapT12_ds.GetRasterBand(1).ReadAsArray()#.astype(numpy.uint16)
        print('Reading Map2')
        MAP2 = mapT12_ds.GetRasterBand(2).ReadAsArray()#.astype(numpy.uint16)

        if user_nodata != '':
            NODATA = ((ALPHA_T1 == 0) + (ALPHA_T2 == 0) + (MAP1 == user_nodata) + (MAP2 == user_nodata)) > 0
        else:
            NODATA = ((ALPHA_T1 == 0) + (ALPHA_T2 == 0)) > 0
        ALPHA_T1 = None
        ALPHA_T2 = None


        print('Preparing Forest Map1')
        MAP1Forest = numpy.zeros((master_rows, master_cols)) #, numpy.float32
        print('Preparing Forest Map2')
        MAP2Forest = numpy.zeros((master_rows, master_cols)) #, numpy.float32
        #
        # # ---- Reload BIOMASS map
        #
        MAPBiomassT1 = numpy.zeros((master_rows, master_cols), numpy.float32)
        MAPBiomassT2 = numpy.zeros((master_rows, master_cols), numpy.float32)

        # SET FAKE VALUE TO NODATA
        print('Setting Nodata Map1')
        MAP1Forest[NODATA] = numpy.nan
        print('Setting Nodata Map2')
        MAP2Forest[NODATA] = numpy.nan
        #
        Biom_T1_T2_warning=False

        if not useConversionMap:
            print(" Init Biomass map from user values ")
            ForestClassCounter = 1
            for item in ClassMinMaxBiom:
                print('Class: ', item[0], ' ',  item[2])
                mask = ((MAP1 >= item[0]) * (MAP1 <= item[1])) > 0
                MAP1Forest += mask * ForestClassCounter
                MAPBiomassT1 += (mask * item[2]) * conversionFactor

                mask = ((MAP2 >= item[0]) * (MAP2 <= item[1])) > 0
                MAP2Forest += mask * ForestClassCounter
                MAPBiomassT2 += (mask * item[2]) * conversionFactor
                ForestClassCounter += 1

        else:
            print(" Init Biomass map from file")
            MAPBiomass = (mapT12_ds.GetRasterBand(3).ReadAsArray()).astype(numpy.float32) *1.0

            ForestClassCounter = 1
            averageBiomPerClassT1 = []
            averageBiomPerClassT2 = []



            for item in ClassMinMaxBiom:
                mask = ((MAP1 >= item[0]) * (MAP1 <= item[1])) > 0
                MAP1Forest += mask * ForestClassCounter
                avgBiom1 = numpy.nanmean(MAPBiomass[mask])
                averageBiomPerClassT1.append(avgBiom1)
                MAPBiomassT1[mask] = avgBiom1 * conversionFactor

                # print('Class',ForestClassCounter)
                # print('AVG biom t1 REF', numpy.nanmean(biom) / conversionFactor)
                # print('AVG biom t1', numpy.nanmean(biom / conversionFactor))
                # print(numpy.nanmean(MAPBiomassT1[mask] / conversionFactor))
                # print(numpy.nanmean(MAPBiomassT1[mask]) / conversionFactor)
                # print(numpy.nanmean(MAPBiomass[mask]))
                # driver = gdal.GetDriverByName("GTiff")
                # tmp1b_ds = driver.Create(
                #     'D:\\IMPACT5\\IMPACT\\DATA\\ForesER\\Biom_T1_C' + str(ForestClassCounter) + '.tif', master_cols,
                #     master_rows, 1,
                #     gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
                # tmp1b_ds.SetGeoTransform(master_imgGeo)
                # tmp1b_ds.SetProjection(master_projRef)
                # outband = tmp1b_ds.GetRasterBand(1)
                # outband.WriteArray(biom / conversionFactor)
                # tmp1b_ds = None



                mask = ((MAP2 >= item[0]) * (MAP2 <= item[1])) > 0
                MAP2Forest += mask * ForestClassCounter
                # MAPBiomassT2 += (MAPBiomass2 * mask) * conversionFactor
                # averageBiomPerClassT2.append(numpy.nanmean(MAPBiomass2[mask]))

                avgBiom2 = numpy.nanmean(MAPBiomass[mask])
                averageBiomPerClassT2.append(avgBiom2)
                MAPBiomassT2[mask] = avgBiom2 * conversionFactor

                mask = None
                # print('AVG biom t2 REF', numpy.nanmean(biom) / conversionFactor)
                # print('AVG biom t2', numpy.nanmean(biom / conversionFactor))
                # print(numpy.nanmean(MAPBiomassT2[mask] / conversionFactor))
                # print(numpy.nanmean(MAPBiomassT2[mask]) / conversionFactor)
                # print(numpy.nanmean(MAPBiomass[mask]))
                # driver = gdal.GetDriverByName("GTiff")
                # tmp1b_ds = driver.Create('D:\\IMPACT5\\IMPACT\\DATA\\ForesER\\Biom_T2_C'+str(ForestClassCounter)+'.tif', master_cols, master_rows, 1,
                #                          gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
                # tmp1b_ds.SetGeoTransform(master_imgGeo)
                # tmp1b_ds.SetProjection(master_projRef)
                # outband = tmp1b_ds.GetRasterBand(1)
                # outband.WriteArray(biom/ conversionFactor)
                # tmp1b_ds = None

                if abs(((avgBiom2-avgBiom1)/avgBiom1)*100) > 10:
                    Biom_T1_T2_warning = True

                ForestClassCounter +=1

            # ----------------------------    Version 2024 --- stop the execution here and report the avg Biom values   ----------------------
            report = '''
                                            <html>
                                            <head>
                                            <title>Report on deforestation and forest "degradation" and resulting biomass and emissions</title>
                                            <style>
                                                body{font-family: Arial, sans-serif; font-size:14px; width: 660px;}
                                                h1{font-family: Arial, sans-serif; font-size:20px; font-weight:bold}
                                                h2{font-family: Arial, sans-serif; font-size:18px; font-weight:bold; margin-top:2px; margin-bottom:0px}
                                                h3{font-family: Arial, sans-serif; font-size:14px; font-weight:bold; margin-bottom:0; margin-top:2px; padding:0}
                                                h4{font-family: Arial, sans-serif; font-size:14px; margin-bottom:0; margin-top:0px; padding:0}
                                                table {border-collapse: collapse; width:100%}
                                                table, td, th {border: 1px solid black; padding:3}
                                            </style>
                                            </head>
                                            <body>
                                            <div id="intro">
                                                <h1> Extraction of average Biomass values per class </h1>
                                            </div>

                                            '''
            report += '<div><h2> User input parameters:</h2>'
            report += 'Biomass map: ' + str(user_biomass_map_user) + ' </br>'
            pos = 0
            for item in ClassMinMaxBiom:
                report += ' --> [{:,.0f},{:,.0f}] = Time1: {:,.1f} (t/ha)    Time2: {:,.1f} (t/ha)   </br> '.format(
                    item[0], item[1], averageBiomPerClassT1[pos], averageBiomPerClassT2[pos])
                pos += 1

            if Biom_T1_T2_warning:
                report += '<p style="color:red;">WARNING: biomass average values per forest class in Time 1 and Time 2 differ more than 10%.</br> Result might not be correct. </br>' + \
                          ' Please verify the extracted biomass value for each forest type before re-running ForestER.</p></br> '

            report += '</div>'
            report += "</body></html>"
            text_file = open(out_name + '.html', "w")
            text_file.write(report)
            text_file.close()
            log.send_message(
                '<a target="_blank" href=getHtmlFile.py?file=' + out_name + '.html><b>View report result</b></a>')
            if sys.platform.startswith('win'):
                os.system("start %s" % (out_name+'.html'))

            clean_tmp_file(tmp_indir)
            log.close()
            return False


        MAPBiomass = None
        MAPBiomassT1 *= 1.0
        MAPBiomassT2 *= 1.0




        # ---- Reload DISTURBANCE MAP  map


        if disturbMap == '':
            MAPdist = numpy.zeros((master_rows,master_cols), numpy.byte)
        else:
            print(" reading disturbance map ")
            # last band in stack 3 or 4 position
            MAPdist = (mapT12_ds.GetRasterBand(mapT12_ds.RasterCount).ReadAsArray()).astype(numpy.byte)


        driver = gdal.GetDriverByName("GTiff")
        #
        # tmp1_ds = driver.Create('D:\\IMPACT5\\IMPACT\\DATA\\ForesER\\Biom_T1.tif', master_cols, master_rows, 1,
        #                         gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        # tmp1_ds.SetGeoTransform(master_imgGeo)
        # tmp1_ds.SetProjection(master_projRef)
        # outband = tmp1_ds.GetRasterBand(1)
        # outband.WriteArray(MAPBiomassT1)
        # tmp1_ds = None
        #
        # tmp1b_ds = driver.Create('D:\\IMPACT5\\IMPACT\\DATA\\ForesER\\Biom_T2.tif', master_cols, master_rows, 1,
        #                          gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        # tmp1b_ds.SetGeoTransform(master_imgGeo)
        # tmp1b_ds.SetProjection(master_projRef)
        # outband = tmp1b_ds.GetRasterBand(1)
        # outband.WriteArray(MAPBiomassT2)
        # tmp1b_ds = None


        # ----------------------------------------------------------------------------------------------
        # --- Calculate CHANGE based on biomass classes considering FOREST TYPES
        # ----------------------------------------------------------------------------------------------
        # MAPBiomassT1[MAPBiomassT1 == NODATA] = numpy.nan
        # MAPBiomassT2[MAPBiomassT2 == NODATA] = numpy.nan

        # Change in Biomass masked by forest
        print('Biomass difference')
        MAPBiomassT2 -= MAPBiomassT2 * biomassDegradPercent * (MAPdist == 1)
        BIOMASS_DIFF = (MAPBiomassT2 - MAPBiomassT1)
        # BIOMASS_DIFF = round(BIOMASS_DIFF,1)


        # # when using biomass map small differences might affect the stable forest computation, round val
        # if useConversionMap:
        #     numpy.around(BIOMASS_DIFF, decimals=0, out=BIOMASS_DIFF)



        # MAPBiomassT1 = None
        # MAPBiomassT2 = None
        tmp_ds = driver.Create(ChangeMap, master_cols, master_rows, 1, gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(master_imgGeo)
        tmp_ds.SetProjection(master_projRef)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(BIOMASS_DIFF)
        tmp_ds.SetMetadata({'Impact_product_type': 'Biomass t/pixel difference', 'Impact_operation': "ForestER",
                            'Impact_version': IMPACT.get_version()})
        tmp_ds = None

        print('Computing forest masks')
        FOREST_TO_FOREST = ((MAP1Forest > 0) * (MAP2Forest > 0)).astype(numpy.byte)

        DEFO = ((MAP1Forest > 0) * (MAP2Forest == 0)).astype(numpy.byte)
        DECR = (FOREST_TO_FOREST * (BIOMASS_DIFF < 0)).astype(numpy.byte)

        AFO = ((MAP1Forest == 0) * (MAP2Forest > 0))
        INCR = (FOREST_TO_FOREST * (BIOMASS_DIFF > 0))

        STABLEFOREST = (FOREST_TO_FOREST * (BIOMASS_DIFF == 0))
        STABLE_NF = ((MAP1Forest == 0) * (MAP2Forest == 0))

        OUT = (DEFO*1 + DECR*2 + STABLEFOREST*3 + INCR*4 + AFO*5).astype(numpy.byte)

        OUT[NODATA] = 6

        FFpixel = numpy.nansum(STABLEFOREST)
        NFNFpixel = numpy.nansum(STABLE_NF)
        FNFpixel = numpy.nansum(DEFO)
        NFFpixel = numpy.nansum(AFO)
        INCRpixel = numpy.nansum(INCR)
        DECRpixel = numpy.nansum(DECR)



        FFarea = FFpixel * conversionFactor
        NFNFarea = NFNFpixel * conversionFactor
        FNFarea = FNFpixel * conversionFactor
        NFFarea = NFFpixel * conversionFactor
        INCRarea = INCRpixel * conversionFactor
        DECRarea = DECRpixel * conversionFactor



        if DEBUG:
            print('-------------------------------------------')
            print('DEFO',numpy.nansum(DEFO))
            print('DECR',numpy.nansum(DECR))
            print('STAB',numpy.nansum(STABLEFOREST))
            print('AFO',numpy.nansum(AFO))
            print('INCR',numpy.nansum(INCR))
            print('NF',numpy.nansum(STABLE_NF))

            print('-------------------------------------------')
            print('MAP1 F1:', numpy.nansum(MAP1Forest==1))
            print('Ha1 F1:', numpy.nansum(MAP1Forest==1)*conversionFactor)
            print('BIO1 F1:', numpy.nansum(MAPBiomassT1[MAP1Forest==1]))

            print('MAP1 F2:', numpy.nansum(MAP1Forest==2))
            print('Ha1 F2:', numpy.nansum(MAP1Forest==2)*conversionFactor)
            print('BIO1 F2:', numpy.nansum(MAPBiomassT1[MAP1Forest==2]))

            print('MAP1 F4:', numpy.nansum(MAP1Forest==3))
            print('Ha1 F4:', numpy.nansum(MAP1Forest==3)*conversionFactor)
            print('BIO1 F4:', numpy.nansum(MAPBiomassT1[MAP1Forest==3]))
            print('-------------------------------------------')

        STABLE_NF = None
        STABLEFOREST = None
        # ---------------------  STATS --------------------------------------

        pixelTot = numpy.nansum(OUT < 6) * 1.0
        TotAreaHectars = pixelTot * conversionFactor



        # --------  WRITE BIOMASS CHANGE  --------------

        dst_ds = driver.Create(ChangeMapClass, master_cols, master_rows, 1, gdal.GDT_Byte,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
        c = gdal.ColorTable()
        ctable = [
            [0, (80, 80, 80)],
            [1, (250, 10, 10)],
            [2, (250, 80, 10)],
            [3, (1, 128, 1)],
            [4, (120, 250, 120)],
            [5, (10, 180, 10)],
            [6, (250, 250, 250)]
        ]

        for cid in range(0, len(ctable)):
            c.SetColorEntry(ctable[cid][0], ctable[cid][1])
        dst_ds.GetRasterBand(1).SetColorTable(c)

        dst_ds.GetRasterBand(1).SetColorTable(c)
        dst_ds.SetGeoTransform(master_imgGeo)
        dst_ds.SetProjection(master_projRef)
        dst_ds.SetMetadata({
                               'Impact_product_type': 'Biomass classes -> 0 Non Forest, 1 Defo, 2 Biom Decr, 3 Stable Forest, 4 Biom Incr, 5 Afo, 6 Nodata',
                               'Impact_operation': "ForestER",
                               'Impact_version': IMPACT.get_version()})
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(OUT)
        OUT = None

        png_ds = gdal.Translate(ChangeMapClassPNG, dst_ds, format='PNG', width=500, height=0)
        png_ds = None
        dst_ds = None





        # ----------------------------------
        # REMOVE TO CALC BIOM
        # BIOMASS_DIFF = 1
        # ----------------------------------
        DEFO_Class = []
        AFO_Class = []
        FOR_INC_Class = []
        FOR_DEC_Class = []
        FOR_STABLE_Class = []

        areaForestT1xClass = []
        areaForestT2xClass = []
        bioForestT1xClass = []
        bioForestT2xClass = []


        bioDEFOsum = 0.0
        bioAFOsum = 0.0
        bioINCRsum = 0.0
        bioDECRsum = 0.0
        bioBalance = 0.0


        print('Computing biomass')
        ForestClassCounter = 1.
        for item in ClassMinMaxBiom:
            map1mask = (MAP1Forest == ForestClassCounter).astype(numpy.byte)
            map2mask = (MAP2Forest == ForestClassCounter).astype(numpy.byte)

            # biomass per each class
            area1 = numpy.nansum(map1mask * conversionFactor)
            area2 = numpy.nansum(map2mask * conversionFactor)
            areaForestT1xClass.append(area1)
            areaForestT2xClass.append(area2)
            # biomass per each class
            if not useConversionMap:
                bioForestT1xClass.append(area1 * item[2])        # control conversion DADA
                bioForestT2xClass.append(area2 * item[2])        # control conversion DADA

            # ----------------  new version does not accept Biom Map  --------------
            # else:
            #     # bioForestT1xClass.append(numpy.nansum(MAPBiomassT1[(MAP1Forest == pos)>0] ))  # control conversion DADA
            #     # bioForestT2xClass.append(numpy.nansum(MAPBiomassT2[(MAP2Forest == pos)>0] )) # control conversion DADA
            #     # bioForestT1xClass.append(numpy.nansum(MAPBiomassT1 * (MAP1Forest == pos)))  # control conversion DADA  closer  values
            #     # bioForestT2xClass.append(numpy.nansum(MAPBiomassT2 * (MAP2Forest == pos))) # control conversion DADA    closer  values
            #     # val, ct = numpy.unique(MAPBiomassT1[map1mask], return_counts=True)
            #     val, ct = numpy.unique(MAPBiomassT1 * map1mask, return_counts=True)
            #     multArray = val * ct
            #     bioForestT1xClass.append(numpy.nansum(multArray))
            #     print('T1 Class ', ForestClassCounter, ' CT=', ct, 'Bio=', numpy.nansum(multArray))
            #     # val, ct = numpy.unique(MAPBiomassT2[map2mask], return_counts=True)
            #     val, ct = numpy.unique(MAPBiomassT2 * map2mask, return_counts=True)
            #     multArray = val * ct
            #     bioForestT2xClass.append(numpy.nansum(multArray))
            #     print('T2 Class ', ForestClassCounter, ' CT=', ct, 'Bio=', numpy.nansum(multArray))

            # biomass per each direction
            # WRONG
            # tmp = numpy.nansum(BIOMASS_DIFF[DEFO * map1mask], dtype=numpy.float32)
            # print(tmp)

            val, ct = numpy.unique(BIOMASS_DIFF[(DEFO * map1mask)>0],return_counts=True)
            multArray = val*ct
            bioSum = numpy.nansum(multArray)
            bioDEFOsum += bioSum
            bioBalance += bioSum
            DEFO_Class.append(bioSum) # * conversionFactor   DADA
            print('Class DEFO', ForestClassCounter, ' CT=',ct, 'Bio=', bioSum)

            # WRONG
            # tmp = numpy.nansum(BIOMASS_DIFF[AFO * map2mask], dtype=numpy.float32)
            # print(tmp)
            val, ct = numpy.unique(BIOMASS_DIFF[(AFO * map2mask)>0],return_counts=True)
            multArray = val*ct
            bioSum = numpy.nansum(multArray)
            AFO_Class.append(bioSum)  # * conversionFactor       DADA    change map2mask to map1mask
            bioAFOsum += bioSum
            bioBalance += bioSum
            print('Class AFO', ForestClassCounter, ' CT=', ct, 'Bio=', bioSum)

            # WRONG
            # tmp = numpy.nansum(BIOMASS_DIFF[INCR * map2mask], dtype=numpy.float32)
            # print(tmp)
            val, ct = numpy.unique(BIOMASS_DIFF[(INCR * map2mask)>0], return_counts=True)
            multArray = val * ct
            bioSum = numpy.nansum(multArray)
            FOR_INC_Class.append(bioSum)  # * conversionFactor
            bioINCRsum += bioSum
            bioBalance += bioSum
            print('Class INCR', ForestClassCounter, ' CT=', ct, 'Bio=', bioSum)


            # # WRONG
            # tmp = numpy.nansum(BIOMASS_DIFF[DECR * map1mask], dtype=numpy.float32)
            # print(tmp)
            val, ct = numpy.unique(BIOMASS_DIFF[(DECR * map1mask)>0], return_counts=True)
            multArray = val * ct
            bioSum = numpy.nansum(multArray)
            FOR_DEC_Class.append(bioSum)  # * conversionFactor
            bioDECRsum += bioSum
            bioBalance += bioSum
            print('Class DECR', ForestClassCounter, ' CT=', ct, 'Bio=', bioSum)



            # val, ct = numpy.unique(MAPBiomassT1 * STABLEFOREST * map1mask, return_counts=True)
            # multArray = val * ct
            # bioSum = numpy.nansum(multArray)
            # FOR_STABLE_Class.append(bioSum)  # * conversionFactor
            # print('Class STABLE F', ForestClassCounter, ' CT=', ct, 'Bio=', bioSum)


            ForestClassCounter += 1.



        #
        # tmp1_ds = driver.Create('E:\\dev\\IMPACT5\\DATA\\RDC\\ForestER\\DEFO_Biom_tmp.tif', master_cols, master_rows, 1,
        #                         gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        # tmp1_ds.SetGeoTransform(master_imgGeo)
        # tmp1_ds.SetProjection(master_projRef)
        # outband = tmp1_ds.GetRasterBand(1)
        # outband.WriteArray(BIOMASS_DIFF * DEFO)
        # tmp1_ds = None
        #
        # tmp1b_ds = driver.Create('E:\\dev\\IMPACT5\\DATA\\RDC\\ForestER\\AFO_tmp.tif', master_cols, master_rows, 1,
        #                          gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])  # GDT_Byte
        # tmp1b_ds.SetGeoTransform(master_imgGeo)
        # tmp1b_ds.SetProjection(master_projRef)
        # outband = tmp1b_ds.GetRasterBand(1)
        # outband.WriteArray(BIOMASS_DIFF * AFO)
        # tmp1b_ds = None
        #
        # tmp2_ds = driver.Create('E:\\dev\\IMPACT5\\DATA\\RDC\\ForestER\\INC_tmp.tif', master_cols, master_rows, 1,
        #                         gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        # tmp2_ds.SetGeoTransform(master_imgGeo)
        # tmp2_ds.SetProjection(master_projRef)
        # outband = tmp2_ds.GetRasterBand(1)
        # outband.WriteArray(INCR)
        # tmp2_ds = None
        # #
        # tmp3_ds = driver.Create('E:\\dev\\IMPACT5\\DATA\\RDC\\ForestER\\DEC_tmp.tif', master_cols, master_rows, 1,
        #                         gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        # tmp3_ds.SetGeoTransform(master_imgGeo)
        # tmp3_ds.SetProjection(master_projRef)
        # outband = tmp3_ds.GetRasterBand(1)
        # outband.WriteArray(BIOMASS_DIFF * DECR)
        # tmp3_ds = None
        #
        # tmp3_ds = driver.Create('E:\\dev\\IMPACT5\\DATA\\RDC\\ForestER\\STABLE_tmp.tif', master_cols, master_rows, 1,
        #                         gdal.GDT_Float32, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        # tmp3_ds.SetGeoTransform(master_imgGeo)
        # tmp3_ds.SetProjection(master_projRef)
        # outband = tmp3_ds.GetRasterBand(1)
        # outband.WriteArray(BIOMASS_DIFF * STABLEFOREST )
        # tmp3_ds = None


        # print()
        # bioBalance_wrong =numpy.nansum(BIOMASS_DIFF, dtype=numpy.float32)
        # print(bioBalance_wrong)
        log.send_message('BIOMASS balance: {:,.1f} T'.format(bioBalance))






        # Create color ramp for each forest class, ordered by biomass value Darker = Higher Biomass
        ClassMinMaxBiomSort = sorted(ClassMinMaxBiom, key=lambda x: -x[2])
        tot_color = len(ClassMinMaxBiom) * 1.0
        rDelta = (254-100) / tot_color
        gDelta = 100 / tot_color
        bDelta = 50 / tot_color
        plotClassColor = []
        plotClassColorSort = []

        pos = 0
        for item in ClassMinMaxBiomSort:
            plotClassColorSort.append([item,rgb2hex(int(20 + rDelta * pos), int(100 + gDelta * pos), int(20 + bDelta * pos))])
            pos += 1

        for item in ClassMinMaxBiom:
            for item1 in plotClassColorSort:
                if item == item1[0]:
                    plotClassColor.append(item1[1])

        # set of default class color: static but can be changed
        nonForestColor = rgb2hex(80, 80, 80)
        defoColor = rgb2hex(250, 10, 10)
        afoColor = rgb2hex(10, 180, 10)
        decColor = rgb2hex(250, 80, 10)
        incColor = rgb2hex(120, 250, 120)
        stablelForestColor = rgb2hex(1, 128, 1)




        pos = 1
        for item in ClassMinMaxBiom:
            tmp1 = numpy.nansum(MAP1Forest == pos) * conversionFactor # conversionFactor * PIXEL = Ha
            tmp2 = numpy.nansum(MAP2Forest == pos) * conversionFactor # conversionFactor * PIXEL = Ha

            areaForestT1xClass.append( tmp1 )
            areaForestT2xClass.append( tmp2 )

            # if biomass per class is safer to compute from the value instead of the biomass map (sum is not always correct)
            if not useConversionMap:
                bioForestT1xClass.append(tmp1 * item[2])        # control conversion DADA
                bioForestT2xClass.append(tmp2 * item[2])        # control conversion DADA
            else:
                # bioForestT1xClass.append(numpy.nansum(MAPBiomassT1[(MAP1Forest == pos)>0] ))  # control conversion DADA
                # bioForestT2xClass.append(numpy.nansum(MAPBiomassT2[(MAP2Forest == pos)>0] )) # control conversion DADA
                # bioForestT1xClass.append(numpy.nansum(MAPBiomassT1 * (MAP1Forest == pos)))  # control conversion DADA  closer  values
                # bioForestT2xClass.append(numpy.nansum(MAPBiomassT2 * (MAP2Forest == pos))) # control conversion DADA    closer  values
                val, ct = numpy.unique(MAPBiomassT1[(MAP1Forest == pos) > 0], return_counts=True)
                multArray = val * ct

                bioForestT1xClass.append(numpy.nansum(multArray))
                val, ct = numpy.unique(MAPBiomassT2[(MAP2Forest == pos) > 0], return_counts=True)
                multArray = val * ct
                bioForestT2xClass.append(numpy.nansum(multArray))

            pos += 1
        tmp1 = 0
        tmp2 = 0
        chartArea_options = '''
                           {
                                title: {text: 'Area (ha) per forest types', x: 'center'} ,
                                tooltip: {},
                                grid: {
                                    left: '5%',
                                    right: '10%',
                                    bottom: '5%',
                                    containLabel: true
                                },

                                xAxis: {
                                       type: 'category',
                                       axisLabel: {rotate:0, fontSize:10},
                                       data: ['Map 1', 'Map 2']

                                },
                                yAxis: {
                                       type: 'value',
                                       name: 'Area (ha)',
                                       nameLocation:'middle',
                                       yAxisIndex: 0
                                   },
                                toolbox: {
                                    show: true,
                                    orient:'vertical',
                                    feature: {
                                        magicType: {show: true, title: ['bar', 'line', 'pie'], type: ['bar', 'line', 'pie']},
                                        restore: {show: true, title: 'Restore'},
                                        saveAsImage: {show: true, title: "Save As Image"}
                                    }
                                },
                                series: [
                                '''
        pos = 0
        for item in ClassMinMaxBiom:
            chartArea_options += '{'+(''' type:'bar', name: '[{},{}]', data: [{:.1f},{:.1f}], color:{}''').format(item[0],item[1],areaForestT1xClass[pos],areaForestT2xClass[pos],plotClassColor[pos])
            chartArea_options += ",label:{normal:{show: true,formatter: '{c} \\n {a}'}}"
            chartArea_options += "},"
            pos += 1
        chartArea_options = chartArea_options[:-1]
        chartArea_options += ''']

                        }'''


        #  ----------------------------------  chart BIOMASS -------------------------------------------

        chartBiom_options = '''
                                   {
                                        title: {text: 'Biomass (t) per forest type', x: 'center', subtext:'No change detection involved'} ,
                                        tooltip: {},
                                        grid: {
                                            left: '10%',
                                            right: '10%',
                                            bottom: '5%',
                                            containLabel: true
                                        },

                                        xAxis: {
                                               type: 'category',
                                               axisLabel: {rotate:0, fontSize:10},
                                               data: ['Map 1', 'Map 2 \\n (with disturbance)']

                                        },
                                        yAxis: {
                                               type: 'value',
                                               name: 'Biomass (t)',
                                               nameLocation:'middle',
                                               yAxisIndex: 0
                                           },
                                        toolbox: {
                                            show: true,
                                            orient:'vertical',
                                            feature: {
                                                magicType: {show: true, title: ['bar', 'line', 'pie'], type: ['bar', 'line', 'pie']},
                                                restore: {show: true, title: 'Restore'},
                                                saveAsImage: {show: true, title: "Save As Image"}
                                            }
                                        },
                                        series: [
                                        '''
        pos = 0
        for item in ClassMinMaxBiom:
            chartBiom_options += '{' + (''' type:'bar', name: '[{},{}]', data: [{:.0f},{:.0f}], color:{}''').format(item[0],item[1],bioForestT1xClass[pos],bioForestT2xClass[pos],plotClassColor[pos])
            chartBiom_options += ",label:{normal:{show: true,formatter: '{c} \\n {a}'}} "
            chartBiom_options += "},"
            pos += 1
        chartBiom_options = chartBiom_options[:-1]
        chartBiom_options += ''']

                                }'''











        mapT12_ds = None
        T1_ds = None
        T2_ds = None
        try:
            report = '''
                <html>
                <head>
                <title>Report on deforestation and forest "degradation" and resulting biomass and emissions</title>
                <style>
                    body{font-family: Arial, sans-serif; font-size:14px; width: 660px;}
                    h1{font-family: Arial, sans-serif; font-size:20px; font-weight:bold}
                    h2{font-family: Arial, sans-serif; font-size:18px; font-weight:bold; margin-top:2px; margin-bottom:0px}
                    h3{font-family: Arial, sans-serif; font-size:14px; font-weight:bold; margin-bottom:0; margin-top:2px; padding:0}
                    h4{font-family: Arial, sans-serif; font-size:14px; margin-bottom:0; margin-top:0px; padding:0}
                    table {border-collapse: collapse; width:100%}
                    table, td, th {border: 1px solid black; padding:3}
                </style>

                <script src="/libs_javascript/echarts-4.4.0/echarts.min.js"></script>

                </head>
                <body>
                <div id="intro">
                    <h1> Report on deforestation and forest "degradation" and resulting emissions </h1>
                </div>
                <div id="GeoInfo">
                    <h2>Geographic location</h2>
                    Country: [Country name] </br>
                    Geographic window: longitude: [--] to [--], latitude: [--] to [--] </br>
                    Zone of interest: [Zone of interest] </br></br>
                </div>
                '''
            report += '<div><h2> User input parameters:</h2>'
            if disturbMap != '':
                report += '- weight for "degradation": ' + str(biomassDegradPercent * 100) + '% of biomass value at Time 2 or input map</br>'


            if not useConversionMap:
                report += ' Provided biomass for class: </br>'
                for item in ClassMinMaxBiom:
                    report += ' --> [{:,.0f},{:,.0f}] = {:,.1f} (t/ha) --> {:,.1f} (t/pixel) </br> '.format(item[0],item[1],item[2],item[2] * conversionFactor)

            # ----------------  new version does not accept Biom Map  --------------
            # else:
            #     report += 'Average biomass values derived from biomass maps: ' + str(user_biomass_map_user) + ' </br>'
            #     pos = 0
            #     for item in ClassMinMaxBiom:
            #         report += ' --> [{:,.0f},{:,.0f}] = Time1: {:,.1f} (t/ha) : {:,.1f} (t/pixel)  Time2: {:,.1f} (t/ha) : {:,.1f} (t/pixel)  </br> '.format(item[0], item[1], averageBiomPerClassT1[pos], averageBiomPerClassT1[pos]*conversionFactor, averageBiomPerClassT2[pos], averageBiomPerClassT2[pos] *conversionFactor)
            #         pos +=1
            #
            #     report += '<p>NOTE: for the computation of the biomass difference (T2-T1), average values are rounded to the closer integer '+\
            #               ' to tolerate small differences and avoid introducing false changes between forest types </p>'
            #     if Biom_T1_T2_warning:
            #         report += '<p style="color:red;">WARNING: biomass average values per forest class in Time 1 and Time 2 differ more than 10%.</br> Result might not be correct. </br>'+\
            #                   ' Please provide biomass value for each forest type.</p></br> '


            report += '</div>'



            # -------------------------    AREA CALC      ---------------------------------------------------------------
            report += '<div><h1>Area Count</h1>'
            report += '<h4>Based on class and biomass difference </h4></br>'

            areaCountReport = '''<table id="areaTable" style='font-size:90%' >
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Pixels</th>
                                            <th>(ha)</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody align="right">
                                        <tr>
                                            <th>Forest to Forest (same biomass) </th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>Non Forest to Non Forest </th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>Deforestation (Forest to Non Forest) </th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>Aforestation (Non Forest to Forest) </th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>Increase (Forest to Forest with higher Biomass) </th>
                                            <td>{:,.0f}</tdv>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>Decrease (Forest to Forest with lower Biomass) </th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                        <tr>
                                            <th>TOT</th>
                                            <td>{:,.0f}</td>
                                            <td>{:,.1f}</td>
                                            <td>{:,.2f}</td>
                                        </tr>
                                    </tbody>
                                </table></br>'''.format(FFpixel, FFarea, FFarea * 100 / TotAreaHectars,
                                                        NFNFpixel, NFNFarea, NFNFarea * 100 / TotAreaHectars,
                                                        FNFpixel, FNFarea, FNFarea * 100 / TotAreaHectars,
                                                        NFFpixel, NFFarea, NFFarea * 100 / TotAreaHectars,
                                                        INCRpixel, INCRarea, INCRarea * 100 / TotAreaHectars,
                                                        DECRpixel, DECRarea, DECRarea * 100 / TotAreaHectars,
                                                        pixelTot, TotAreaHectars,(FFarea+NFNFarea+FNFarea+NFFarea+INCRarea+DECRarea) / TotAreaHectars *100
                                                        )

            report += areaCountReport

            # areaCountReport += 'Net deforestation (disturbance not counted): {} pixel -> {:,.2f} ha -> {:,.2f} % </br>'.format(FNFpixel-NFFpixel, FNFarea-NFFarea, (FNFarea-NFFarea) / TotAreaHectars * 100)
            # areaCountReport += 'Net disturbance : {} pixel -> {:,.2f} ha -> {:,.2f} %'.format((INCRpixel-DECRpixel),INCRarea-DECRarea, (INCRarea-DECRarea) / TotAreaHectars * 100)

            chart_options = '''
                {
                         title: {text:'Area (ha)', x: 'center'},
                         tooltip: {},
                         grid: {
                             left: '15%',
                             right: '10%',
                             bottom: '5%',
                             containLabel: true
                         },

                        xAxis: {
                                   type: 'category',
                                   axisLabel: {rotate:45, fontSize:10},
                                   data: ['Forest to Forest (same biomass)', 'Non Forest to Non Forest','Deforestation', 'Aforestation', 'Decrease', 'Increase']

                        },
                        yAxis: {
                                   type: 'value',
                                   name: 'Area (ha)',
                                   nameLocation:'middle',
                                   yAxisIndex: 0
                               },

                        series: [{
                                   type: 'bar',
                                   data: [''' + \
                                            '{value:'+'{:.1f}'.format(FFarea)+',itemStyle:{color:'+stablelForestColor+'}},'+\
                                            '{value:'+'{:.1f}'.format(NFNFarea)+',itemStyle:{color:'+nonForestColor+'}},'+\
                                            '{value:'+'{:.1f}'.format(FNFarea)+',itemStyle:{color:'+defoColor+'}},'+\
                                            '{value:'+'{:.1f}'.format(NFFarea)+',itemStyle:{color:'+afoColor+'}},'+\
                                            '{value:'+'{:.1f}'.format(DECRarea)+',itemStyle:{color:'+decColor+'}},'+\
                                            '{value:'+'{:.1f}'.format(INCRarea)+',itemStyle:{color:'+incColor+'}}]' + '''

                                 }],
                        toolbox: {
                            show: true,
                            orient:'vertical',
                            feature: {
                                magicType: {show: true, title: ['bar', 'line', 'pie'], type: ['bar', 'line', 'pie']},
                                restore: {show: true, title: 'Restore'},
                                saveAsImage: {show: true, title: "Save As Image"}
                            }
                        }
                }'''



            report += '<div id="chartTotArea" style="width: 600px; height: 500px;"> </div>'

            report += "<script> var myAreaChart = echarts.init(document.getElementById('chartTotArea')); myAreaChart.setOption(" + chart_options + ");</script>"

            report += "</br>"
            report += "</div>"


            # ------------------------- END OF AREA CALC ---------------------------------------------------------------

            # ------------------------- BIOMASS BALANCE ----------------------------------------------------------------
            biomassReport = '<div>'
            biomassReport += '<h1>Biomass report: </h1> '
            biomassReport += '<h4>Changes are computed per class and biomass difference including disturbance layer </h4>'

            biomassReport += '<div></br><img style="border:1px solid black; margin-left:150px" src="getImageFile.py?filename=' + ChangeMapClassPNG + '&format=png" alt="Biomass change map" width=300></br></br>'
            biomassReport +=' </br> Interactive map visible in Main Panel; name = '+ChangeMapClass.replace(GLOBALS['root_path'],'/')+'</div></br>'

            classHeader = ''
            defoCells = ''
            afoCells = ''
            decCells = ''
            incCells = ''
            balanceCells = ''
            pos = 0

            for item in ClassMinMaxBiom:
                classHeader += "<th> Forest Class [{:,.0f},{:,.0f}]</th>".format(item[0],item[1])
                defoCells += "<td>{:,.0f}</td>".format(DEFO_Class[pos])
                afoCells += "<td>{:,.0f}</td>".format(AFO_Class[pos])
                decCells += "<td>{:,.0f}</td>".format(FOR_DEC_Class[pos])
                incCells += "<td>{:,.0f}</td>".format(FOR_INC_Class[pos])
                balanceCells += "<td>{:,.0f}</td>".format(DEFO_Class[pos]+AFO_Class[pos]+FOR_DEC_Class[pos]+FOR_INC_Class[pos])
                pos += 1

            defoCells += "<td>{:,.0f}</td>".format(bioDEFOsum)
            afoCells += "<td>{:,.0f}</td>".format(bioAFOsum)
            decCells += "<td>{:,.0f}</td>".format(bioDECRsum)
            incCells += "<td>{:,.0f}</td>".format(bioINCRsum)
            balanceCells += "<td><b>{:,.0f}</b></td>".format(bioBalance)

            biomassReport += '''<table id="biomassTable" style='font-size:90%' >
                                                <thead>
                                                    <tr>
                                                        <th></th>'''
            biomassReport += classHeader
            biomassReport +='''                         <th>Sum (t)</th>
                                                    </tr>
                                                </thead>
                                                <tbody align="right">
                                                    <tr>
                                                        <th>Deforestation </th>''' + defoCells + '''

                                                    </tr>
                                                    <tr>
                                                        <th>Aforestation</th>''' + afoCells + '''

                                                    </tr>
                                                    <tr>
                                                        <th>Decrease </th>''' + decCells + '''

                                                    </tr>

                                                    <tr>
                                                        <th>Increase </th>''' + incCells + '''

                                                    </tr>
                                                    <tr>
                                                        <th>Biomass balance </th>''' + balanceCells + '''

                                                    </tr>
                                                </tbody>
                                            </table></br>'''

            biomassReport += '</div>'

            report += biomassReport

            # SET TO GREEN and RED
            if bioBalance < 0:
                balance ='{:,.0f} (t)'.format(bioBalance)
                color = "'#ff0000'"
            else:
                balance = '{:,.0f} (t)'.format(bioBalance)
                color = "'#000000'"


            chart_options = '''
                            {
                                     title: {text: 'Biomass balance = ''' + str(balance) + ''' ', x: 'center', textStyle:{color: '''+ color + ''' } },
                                     tooltip: {},
                                     grid: {
                                         left: '15%',
                                         right: '10%',
                                         bottom: '5%',
                                         containLabel: true
                                     },

                                    xAxis: {
                                               type: 'category',
                                               axisLabel: {rotate:10, fontSize:10},
                                               data: ['Deforestation', 'Aforestation', 'Decrease', 'Increase']

                                    },
                                    yAxis: {
                                               type: 'value',
                                               name: 'Biomass (t)',
                                               nameLocation:'middle',
                                               yAxisIndex: 0
                                           },
                                    toolbox: {
                                        show: true,
                                        orient:'vertical',
                                        feature: {
                                            magicType: {show: true, title: ['bar', 'line', 'pie'], type: ['bar', 'line', 'pie']},
                                            restore: {show: true, title: 'Restore'},
                                            saveAsImage: {show: true, title: "Save As Image"}
                                        }
                                    },
                                    series: [
                                    '''
            pos = 0
            for item in ClassMinMaxBiom:
                chart_options += '{' + ('''type:'bar', name: '[{},{}]', data: [{:.0f},{:.0f},{:.0f},{:.0f}], color:{}''').format(item[0], item[1],DEFO_Class[pos],AFO_Class[pos],FOR_DEC_Class[pos],FOR_INC_Class[pos],plotClassColor[pos])
                chart_options += ",label:{normal:{show: true,formatter: '{c} \\n {a}'}}},"
                pos += 1
            chart_options = chart_options[:-1]
            chart_options += ''']

                }'''


            report += '<div id="chartBalance" style="width: 600px; height: 500px;"></div>'
            report += "<script> var chartBalance = echarts.init(document.getElementById('chartBalance')); chartBalance.setOption(" + chart_options + ");</script>"

            #------------------------- END OF BIOMASS BALANCE ----------------------------------------------------------



            # ------------------------- AREA and BIOMASS IN FOREST------------------------------------------------------

            report += '''
                        <div>
                            <h2>Forest area and biomass count as derived from input maps and user parameters </h2>
                            </br>
                        '''
            forestText = ''
            pos = 0
            for item in ClassMinMaxBiom:
                forestText += ('<tr><td>[{},{}]</td> <td>{:,.1f}</td> <td>{:,.1f}</td><td>[{},{}]</td> <td>{:,.0f}</td> <td>{:,.0f}</td></tr>').format(item[0], item[1], areaForestT1xClass[pos], areaForestT2xClass[pos],item[0], item[1], bioForestT1xClass[pos], bioForestT2xClass[pos])
                pos += 1

            report += '''
                            <table>
                                <tbody align="right">
                                <tr>
                                    <td colspan=3 id="chartArea" style="width: 330px; height: 400px;"</td> <td colspan=3 id="chartBiom"  style="width: 330px; height: 400px;" ></td>
                                </tr>
                                <tr><td>Class</td><td>Map 1</td><td>Map 2</td><td>Class</td><td>Map 1</td><td>Map 2</td></tr> '''\
                               + forestText + '''
                                </tbody>
                            </table>

                        </div>
                    '''

            # ----------------------------------    EMISSION REPORT ---------------------------------------------------

            emissionReport = '</br><h1>Emission report</h1>'
            emissionReport += ' Biomass (dry weight) to carbon equivalents (C)(ton) = factor 0.5 </br>'#.format(bioBalance * -0.5)
            if bioBalance <= 0:
                emissionReport += ' - Total Emission (max): {:,.1f} tC </br>'.format(bioBalance*-0.5)
            else:
                emissionReport += ' - Total Emission (max): 0 tC ( emission balance is <=0)</br>'

            emissionReport += ' - Emission Deforestation (max): {:,.1f} tC </br>'.format(bioDEFOsum*-0.5)
            emissionReport += ' - Emission "Degradation": {:,.1f} tC </br>'.format(bioDECRsum*-0.5,)

            report += "<script> var chartArea = echarts.init(document.getElementById('chartArea')); chartArea.setOption(" + chartArea_options + ");</script>"
            report += "<script> var chartBiom = echarts.init(document.getElementById('chartBiom')); chartBiom.setOption(" + chartBiom_options + ");</script>"

            #report += biomassReport
            report += emissionReport
            report += "</body></html>"
            text_file = open(out_name + '.html', "w")
            text_file.write(report)
            text_file.close()
            log.send_message(
                '<a target="_blank" href=getHtmlFile.py?file=' + out_name + '.html><b>View report result</b></a>')

            # os.system("start %s" % (out_name+'.html'))
            # if sys.platform.startswith('win'):
            #     os.system("start %s" % (out_name+'.html'))

        except Exception as e:
            #import time
            print(str(e))

            mapT12_ds = None
            T1_ds = None
            T2_ds = None
            png_ds = None
            dst_ds = None

            clean_tmp_file(tmp_indir)
            #time.sleep(10)

        mapT12_ds = None
        T1_ds = None
        T2_ds = None
        png_ds = None
        dst_ds = None

        log.send_message("Completed.")
        log.close()

        clean_tmp_file(tmp_indir)
        if os.path.exists(ChangeMap):
            os.rename(ChangeMap,ChangeMap+'.tif')
        if os.path.exists(ChangeMapClass):
            os.rename(ChangeMapClass,ChangeMapClass+'.tif')




    except Exception as e:
        log.send_error("ForestER  error: "+str(e))

        mapT12_ds = None
        T1_ds = None
        T2_ds = None
        png_ds = None
        dst_ds = None

        log.close()
        clean_tmp_file(tmp_indir)

        return False


if __name__ == "__main__":

    me, mapT1, mapT2, disturbMap, user_biomass_map, biomass_value, biomassDegradPercent, useConversionMap, out_name, overwrite, outEPSG, nodata = sys.argv
    res = RunForesER(mapT1, mapT2, disturbMap, user_biomass_map, biomass_value, biomassDegradPercent, useConversionMap, out_name, overwrite, outEPSG, nodata)


