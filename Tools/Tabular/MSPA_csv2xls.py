#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import glob
#import cgi
import numpy



def main():

    # ALGO DESCRIPTION
    # Reformat stats file generated with MSPA tool from GUIDOS Toolbox (https://ec.europa.eu/jrc/en/scientific-tool/guidos-toolbox)
    # for every txt file in input dir:
        # 	1-> extracts start from multiple lines
        # 	2-> concatenates file name and stats in 1 line
        # 	3-> appends current stats into the out file

    # initialize log
    INDIR = ('F:/IMPACT5/DATA/CSV')
    txt_files = glob.glob(os.path.join(INDIR, "mUAP*.txt"))
    OUTname = os.path.join(INDIR, 'Output_Stats.csv')

    try:
        print('Processing ...')
        # print(txt_files)

        #-------------------------------------------------------------------
        # ------------------------- OPEN FILES   ---------------------------
        #-------------------------------------------------------------------

        if os.path.exists(OUTname+'.tif') :
            try:
                os.remove(OUTname+'.tif')
            except:
                print('error deleting out file ')
                sys.exit()
        file_handler = open(OUTname, 'w')

        header_0 = "--,FG,FG,FG,FG,FG,FG,FG,FG,FG,FG,FG,FG,FG,FG,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,TOT,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPOL,NPIXELS,NPIXELS,NPIXELS,NPIXELS,NPIXELS\n"
        header_1 = "UAP,CORE(s),CORE(m),CORE(l),ISLET,PERFORATION,EDGE,LOOP,BRIDGE,BRANCH,Background,Missing,Opening,Core-Opening,Border-Opening,CORE(s),CORE(m),CORE(l),ISLET,PERFORATION,EDGE,LOOP,BRIDGE,BRANCH,Background,Missing,Opening,Core-Opening,Border-Opening,CORE(s),CORE(m),CORE(l),ISLET,PERFORATION,EDGE,LOOP,BRIDGE,BRANCH,Background,Missing,Opening,Core-Opening,Border-Opening,Background,Missing,Opening,Core-Opening,Border-Opening\n"
        file_handler.write(header_0)
        file_handler.write(header_1)

        for file in txt_files:
            basename = os.path.basename(file)
            print(file)
            with open(file) as f:
                contents = f.readlines()
                name = "PR_"+contents[1].split("_")[2]
                print(name)
                cores = contents[6].split(":")[1].replace("/"," ").strip().split()
                corem = contents[7].split(":")[1].replace("/"," ").strip().split()
                corel = contents[8].split(":")[1].replace("/"," ").strip().split()
                islet = contents[9].split(":")[1].replace("/"," ").strip().split()
                perforation = contents[10].split(":")[1].replace("/"," ").strip().split()
                edge = contents[11].split(":")[1].replace("/"," ").strip().split()
                loop = contents[12].split(":")[1].replace("/"," ").strip().split()
                bridge = contents[13].split(":")[1].replace("/"," ").strip().split()
                branch = contents[14].split(":")[1].replace("/"," ").strip().split()
                bkgd = contents[15].split(":")[1].replace("/"," ").strip().split()        # from background replace the / with space to keep the last one
                missing = contents[16].split(":")[1].replace("/"," ").strip().split()
                opening = contents[17].split(":")[1].replace("/"," ").strip().split()
                coreopen = contents[18].split(":")[1].replace("/"," ").strip().split()
                borderopen = contents[19].split(":")[1].replace("/"," ").strip().split()

                if len(missing) == 3:
                    missing.insert(0,"0")    #  missing does not have first value

                # print("cores",cores)
                # print("corem",corem)
                # print("corel",corel)
                # print("borderopen",borderopen)
                # print("bkgd",bkgd)
                # print("missing",missing)
                # print("opening",opening)
                # print("coreopen",coreopen)
                # print("borderopen",borderopen)
                
                # print("bkgd[3]",bkgd[3])
                # print("missing[3]",missing[3])
                # print("opening[3]",opening[3])
                # print("coreopen[3]",coreopen[3])
                # print("borderopen[3]",borderopen[3])

                line = ",".join([name,cores[0],corem[0],corel[0],islet[0],perforation[0],edge[0],loop[0],bridge[0],branch[0],bkgd[0],missing[0],opening[0],coreopen[0],borderopen[0],\
                                      cores[1],corem[1],corel[1],islet[1],perforation[1],edge[1],loop[1],bridge[1],branch[1],bkgd[1],missing[1],opening[1],coreopen[1],borderopen[1],\
                                      cores[2],corem[2],corel[2],islet[2],perforation[2],edge[2],loop[2],bridge[2],branch[2],bkgd[2],missing[2],opening[2],coreopen[2],borderopen[2],\
                                      bkgd[3],missing[3],opening[3],coreopen[3],borderopen[3]])+"\n"
                                      

                # ------   section to nername particular values in the out line -----------
                line = line.replace("--","0")
                line = line.replace("Integrity","100")

                file_handler.write(line)
                

    except Exception as e:
        print(str(e))

    #
    file_handler.close()
    print("Done")


if __name__ == '__main__':
    main()

