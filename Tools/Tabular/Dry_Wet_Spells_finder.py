#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np
data = np.loadtxt('CHIRPS-daily-1980_2021_fix.csv', usecols=(0,1,2,3), delimiter="/", skiprows=1, dtype='i4')


def findMaxConsecutiveOnes(nums):
    n = len(nums)
    if n == 1 and nums[0] == 1:
        return 1

    res = 0
    c = 0

    for i in range(n):
        if nums[i] == 1:
            c += 1
            res = max(c, res)
        else:
            c = 0
    return res

print(data.shape)
rows,cols = data.shape #reading number of rows and columns into variables
#   mm/dd/yyyy/value
years = set(data[:,2])
years = [int(x) for x in years]
print("YY,MM,Dry,Wet")

# for i in range(years[0],years[-1]):
for i in range(years[0], years[-1]+1):
    for m in range(1, 13):
        dry = np.zeros(32)
        wet = np.zeros(32)
        dd = 0
        for x in range(0, rows):
            if int(data[x,2]) == i and int(data[x,0]) == m:
                if data[x,3]/662 < 1:
                    dry[dd] = 1
                if data[x,3]/662 > 10:
                    wet[dd] = 1
                dd += 1
        dry = [int(v) for v in dry]
        max1 = findMaxConsecutiveOnes(dry)
        wet = [int(v) for v in wet]
        max2 = findMaxConsecutiveOnes(wet)
        print(i,",", m,",", max1,",", max2)

# for i in range(years[0],years[-1]):
for i in range(years[0], years[-1]+1):
        dry = np.zeros(366)
        wet = np.zeros(366)
        dd = 0
        for x in range(0, rows):
            if int(data[x,2]) == i:
                if data[x,3]/662 < 1:
                    dry[dd] = 1
                if data[x,3]/662 > 10:
                    wet[dd] = 1
                dd += 1
        dry = [int(v) for v in dry]
        max1 = findMaxConsecutiveOnes(dry)
        wet = [int(v) for v in wet]
        max2 = findMaxConsecutiveOnes(wet)
        print(i,",", max1,",", max2)