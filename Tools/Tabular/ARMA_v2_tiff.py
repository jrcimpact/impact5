#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
#import cgi

from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal

import numpy
from sklearn.linear_model import LinearRegression

# Import local libraries

def estimate_coef(x, y):
    # number of observations/points
    n = numpy.size(x)

    # mean of x and y vector
    m_x, m_y = numpy.mean(x), numpy.mean(y)

    # calculating cross-deviation and deviation about x
    SS_xy = numpy.sum(y * x) - n * m_y * m_x
    SS_xx = numpy.sum(x * x) - n * m_x * m_x

    # calculating regression coefficients
    b_1 = SS_xy / SS_xx
    b_0 = m_y - b_1 * m_x

    return (b_0, b_1)


def main():
    # ALGO DESCRIPTION
    # for every plot:
        # 	1-> compute the STABLE NDII as median-1std of the last 10years
        # 						if STABLE NDII < 0.45 -> not stable or too low -> jump to next plot
        # 	2-> from last observation (recent year) procede BACKWARD until NDII <= STABLE NDII
        # 						if no values are detected -> no drop -> stable forest -> jump to next plot
        # 	3-> compute the MIN of NDII from 87 to YYYY in witch the STABLE NDII has been observed (Y_stbl_NDII)
        # 						if MIN NDII is > 0.35 -> minimum is too high -> flag plot as 'NEED FURTHER INSPECTION'
        # 	4-> identification of the start of regrow after MIN
        # 						if MIN NDII >= 0.15 -> get the YYYY_min (candidate to be a good starting point)
        # 						else SEARCH a suitable value > 0.15 within the range of observation [YYYY_min, Y_stbl_NDII]
        # Consideration:
        # The BACKWARD analysys is safer since we assume that in the last 10Y we do not observe degradation/deforestation. Hence going back is possible to identify drops in the signal down to the MIN NDII and subsequent detection of photosintetic activity (often with a delta/shift of 2/3 years)
        # The SEARCH function in step 4 guarantees the selection of suitable MIN above a threshold of 0.15 (to be decided) and not only the absolute minimum


    # initialize log

    img = "E:\\dev\\IMPACT5\\DATA\\Trat_data_Impact\\NDII_1987_2022.tif"
    img = "E:\\dev\\IMPACT5\\DATA\\PhangNah\\SARMA\\NDII_1988_2024.tif"
    OUTname = img.replace('.tif','_out_Dec24.tif')


    try:
        print('VA')

        #-------------------------------------------------------------------
        # ------------------------- OPEN MASTER  ---------------------------
        #-------------------------------------------------------------------
        master_ds = gdal.Open(img, gdal.GA_ReadOnly)
        master_bands = master_ds.RasterCount

        master_cols = master_ds.RasterXSize
        master_rows = master_ds.RasterYSize

        bands = master_ds.ReadAsArray().astype(numpy.float32)
        print(bands)
        print(bands.shape)
        out_band = bands[0,:,:] * 0


        if os.path.exists(OUTname+'.tif') :
            try:
                os.remove(OUTname+'.tif')
                if os.path.exists(OUTname+".tif.oux.xml"):
                    os.remove(OUTname+".tif.out.xml")
            except:
                #log.send_error('Cannot delete output file')
                print('Cannot delete output file')
        driver = gdal.GetDriverByName("GTiff")
        dst_ds = driver.Create(OUTname,master_cols,master_rows,1,gdal.GDT_Byte,options = [ 'COMPRESS=LZW','BIGTIFF=IF_SAFER' ]) #GDT_Byte
        dst_ds.SetGeoTransform(master_ds.GetGeoTransform())
        dst_ds.SetProjection(master_ds.GetProjectionRef())






        pos = 0
        plotCt = 0  # increment is done on start to bypass the continue statment , so set to -1
        startYY = 1987

        ABSOLUTE_STABLE_VAL = 0.51

        Xaxe = numpy.arange(bands.shape[0]).reshape(-1, 1)
        print('Axe')
        print(Xaxe)
        for c in range(master_cols-1):
            for r in range(master_rows-1):
                line = bands[:,r,c]
                line = numpy.nan_to_num(line)

                plotCt += 1
                # all year stable median
                all_yy_stable_median = numpy.median(line)
                all_yy_stable_mean = numpy.mean(line)

                all_yy_min = numpy.min(line)
                all_yy_max = numpy.max(line)
                all_yy_stable_std = numpy.std(all_yy_stable_median)
                all_yy_stable_minTh = all_yy_stable_median - all_yy_stable_std
                model = LinearRegression().fit(Xaxe, line)

                slope = model.coef_

                # -16 -> last 16 years considered as stable
                last_years_val = line[-10:]
                last_yy_stable_median = numpy.median(last_years_val)
                last_yy_stable_std = numpy.std(last_years_val)
                last_yy_stable_minTh = last_yy_stable_median - last_yy_stable_std
                # print('Median: ', all_yy_stable_median)
                # print('Mean: ', all_yy_stable_mean)

                #  ---------------------------------------------------------------------------------------------------------
                # this section tests cases where all NDII vals are above the STABLE values (can be also empirically defined)
                # e.g. 0.58  and identifies flat, increasing or decreasing trends
                # isolated disturbance events with NDII < 0.48 are handled by old ARMA section
                #  ---------------------------------------------------------------------------------------------------------
                if all_yy_stable_median >= ABSOLUTE_STABLE_VAL and all_yy_stable_mean >= ABSOLUTE_STABLE_VAL and all_yy_min > 0.48:
                    # print('1.* stable', plotCt, 'intercept:', model.intercept_, 'slope:', model.coef_)
                    # define classes according to slope and intercept
                    # 1.0  --------
                    # 1.1  ---_ _ _  >
                    # 1.2  _ _ _ --- <

                    if slope < -0.001:
                        out_band[r, c] = 11
                        # print(plotCt, '1.1 stable with negative trend', 'intercept:', model.intercept_, 'slope:',
                        #       slope)
                    elif slope > 0.001:
                        out_band[r, c] = 12
                        # print(plotCt, '1.2* stable with positive trend', 'intercept:', model.intercept_, 'slope:',
                        #       slope)
                    else:
                        out_band[r, c] = 10
                        # print(plotCt, '1.0* stable with flat trend', 'intercept:', model.intercept_, 'slope:',
                        #       slope)
                    continue

                #  ---------------------------------------------------------------------------------------------------------
                # this section tests cases where all NDII vals and median NDII are below the STABLE value
                # never recovered
                #  ---------------------------------------------------------------------------------------------------------
                if all_yy_stable_median <= ABSOLUTE_STABLE_VAL and all_yy_max < ABSOLUTE_STABLE_VAL:
                    # print('3.* not reach stable', plotCt, 'intercept:', model.intercept_, 'slope:', model.coef_)
                    # define classes according to slope and intercept and split soil from vegetation
                    # 2.0  --------
                    # 2.1  ---_ _ _  >
                    # 2.2  _ _ _ --- <
                    if slope < -0.001:
                        out_band[r, c] = 21
                        # print(plotCt, '2.1 never stable with negative trend', 'intercept:', model.intercept_,
                        #       'slope:', slope)
                    elif slope > 0.001:
                        out_band[r, c] = 22
                        # print(plotCt, '2.2* never stable with positive trend', 'intercept:', model.intercept_,
                        #       'slope:', slope)
                    else:
                        out_band[r, c] = 20
                        # print(plotCt, '2.0* never flat trend', 'intercept:', model.intercept_, 'slope:', slope)
                    continue

                #  ---------------------------------------------------------------------------------------------------------
                # this section tests if forest is NOT recovered in last YY
                #  ---------------------------------------------------------------------------------------------------------
                if last_yy_stable_minTh < 0.48:
                    #  ------------------------------------------------------------------
                    #  Ongoing disturbance: from STABLE to DROP and NOT FULLY RECOVERED
                    #  ------------------------------------------------------------------
                    if model.intercept_ >= all_yy_stable_minTh:
                        out_band[r, c] = 31
                        # print(plotCt, '3.1 Disturbed with negative trend', 'intercept:', model.intercept_, 'slope:',
                        #       slope)
                        continue
                    else:
                        # ------------------------------------------------------------------
                        #  Ongoing disturbance/regeneration: from NON STABLE to DROP and NOT FULLY RECOVERED
                        #  ------------------------------------------------------------------
                        if slope < -0.001:
                            out_band[r, c] = 40
                            # print(plotCt, '4.0 degradation trend', 'intercept:', model.intercept_, 'slope:', slope)
                        elif slope > 0.001:
                            out_band[r, c] = 52
                            # print(plotCt, '5.2* regeneration trend but not completed', 'intercept:',
                            #       model.intercept_, 'slope:', slope)
                        else:
                            out_band[r, c] = 30
                            # print(plotCt, '3.0 disturbed flat trend', 'intercept:', model.intercept_, 'slope:',
                            #       slope)

                        continue

                # print stable_minTh
                # add condition if too low

                #  ---------------------------------------------------------------------------------------------------------
                # this section tests if forest is RECOVERED in last YY
                #  ---------------------------------------------------------------------------------------------------------

                index_top = 0
                data_min = 0
                delta_index = 0
                index_min = 0
                valid = 0
                growing_index = 0

                if last_yy_stable_minTh >= 0.48:
                    # going backwards to identify drops
                    for index_top in range(len(line) - 1, 0, -1):
                        if line[index_top] <= last_yy_stable_minTh:
                            # if index_top > 5 and line[index_top - 3] < stable_minTh and \
                            #                    (line[index_top-1] <= stable_minTh or line[index_top-2] <= stable_minTh or line[index_top-3] <= stable_minTh):
                            # print 'Drop detected, going back ...'
                            # print "IndexTop = "+ str(index_top)
                            # print startYY+index_top
                            # for y in range(x, 1, -1):
                            #    if line[y] <= line[x] and line[y]

                            # get minimum
                            data_min = numpy.min(line[:index_top])
                            index_min, = numpy.where(line[:index_top] == data_min)
                            index_min = index_min[0]

                            if data_min < 0.35:
                                # print "Absolute MIN:" + str(data_min)
                                # print "Absolute MIN YY :" + str(index_min)
                                # print "Absolute GAP YY:" + str(index_top - index_min)
                                valid = 1

                                # now identify the start of regrow after min
                                if data_min >= 0.15:
                                    # value is our candidate
                                    growing_index = index_min
                                else:
                                    for pos in range(index_min, index_top):
                                        if line[pos] >= 0.15:
                                            growing_index = pos
                                            break
                                # TEST if there are no values bigger then median -1std (stable_minTh)  between min and first drop
                                # in case shift back the top do better define the forest maturity

                                for pos1 in range(growing_index, index_top):
                                    # print "Range" + str(startYY+pos1)
                                    # print line[pos1]
                                    if line[pos1] >= last_yy_stable_minTh:
                                        # print "Resetting index_Top from "+str(index_top)+" to "+str(pos1)
                                        index_top = pos1
                                        break

                                delta_index = index_top - growing_index
                                break
                            else:
                                # if minimum value appears in the first years makes the difference between regeneration or occasional drop (disturbance)
                                if index_min < 5:
                                    out_band[r, c] = 50
                                    # print(plotCt, '5.0* Regenerated', 'intercept:', model.intercept_, 'slope:',
                                    #       slope, 'index_min:', index_min)
                                else:
                                    out_band[r, c] = 13
                                    # print(plotCt, '1.3* light disturbed but MIN too high', 'intercept:',
                                    #       model.intercept_, 'slope:', slope, 'index_min:', index_min)

                                valid = 0
                                break
                        else:
                            continue

                    # not needed on production, good for testing
                    if valid != 0:
                        # -----------------------------------------------------------------------------------------------
                        # if minimum value appears in the first years makes the difference between Regeneration or Rehab
                        # -----------------------------------------------------------------------------------------------
                        if index_min < 5:
                            out_band[r, c] = 53
                            # print(plotCt, '5.3* Regenerated', 'intercept:', model.intercept_, 'slope:', slope,
                            #       'index_min:', index_min)
                        else:
                            out_band[r, c] = 60
                            # print(plotCt, '6.0* Rehabilitated', 'intercept:', model.intercept_, 'slope:', slope,
                            #       'index_min:', index_min)

                    # end for each plot
                    # add values/codes from print function to save them into the out file
                    # currently only info from rehabilitated plot are printer out
                    # row = numpy.append(line, [last_yy_stable_median, last_yy_stable_std, last_yy_stable_minTh,
                    #                           startYY + index_min, data_min, startYY + growing_index,
                    #                           startYY + index_top,
                    #                           delta_index, valid])
                    # outTable.append(row)
                else:
                    out_band[r, c] = 0
                    print(plotCt, "CONDITION 2 IMPLEMENT")
                    # stable_median, stable_std, stable_minTh, index_min, data_min, growing_index, index_top, delta_index, valid_trend

                # # end for each plot
                # row = numpy.append(line, [last_yy_stable_median, last_yy_stable_std, last_yy_stable_minTh, startYY + index_min, data_min, startYY+growing_index, startYY+index_top, delta_index, valid])
                # outTable.append(row)

        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(out_band)

        MANGROVES = [
            [0, (0, 0, 0)],
            [1, (0, 0, 0)],
            [2, (0, 0, 0)],
            [3, (0, 0, 0)],
            [4, (0, 0, 0)],
            [5, (0, 0, 0)],
            [6, (0, 0, 0)],
            [7, (0, 0, 0)],
            [8, (0, 0, 0)],
            [9, (0, 0, 0)],
            [10, (1, 150, 1)],  # '1.0* stable with flat trend'       Green
            [11, (34, 255, 34)],  # '1.0* stable with negative trend'
            [12, (1, 80, 1)],  # '1.0* stable with positive trend'
            [13, (220, 255, 220)],  # 1.0* light disturbed but stable
            [14, (0, 0, 0)],
            [15, (0, 0, 0)],
            [16, (0, 0, 0)],
            [17, (0, 0, 0)],
            [18, (0, 0, 0)],
            [19, (0, 0, 0)],
            [20, (100, 100, 255)],  # '2.0* never stable flat trend       Blue
            [21, (153, 153, 255)],  # '2.0* never stable negative trend
            [22, (1, 1, 200)],  # '2.0* never stable positive trend
            [23, (0, 0, 0)],
            [24, (0, 0, 0)],
            [25, (0, 0, 0)],
            [26, (0, 0, 0)],
            [27, (0, 0, 0)],
            [28, (0, 0, 0)],
            [29, (0, 0, 0)],
            [30, (255, 130, 1)],  # '3.1* Disturbed with flat trend'    Orange
            [31, (255, 170, 85)],  # '3.1* Disturbed with negative trend'
            [32, (190, 100, 1)],  # '3.1* Disturbed with positive trend'
            [33, (0, 0, 0)],
            [34, (0, 0, 0)],
            [35, (0, 0, 0)],
            [36, (0, 0, 0)],
            [37, (0, 0, 0)],
            [38, (0, 0, 0)],
            [39, (0, 0, 0)],
            [40, (255, 1, 1)],  # '4.0* degradation trend     Red
            [41, (0, 0, 0)],
            [42, (0, 0, 0)],
            [43, (0, 0, 0)],
            [44, (0, 0, 0)],
            [45, (0, 0, 0)],
            [46, (0, 0, 0)],
            [47, (0, 0, 0)],
            [48, (0, 0, 0)],
            [49, (0, 0, 0)],
            [50, (120, 1, 60)],  # '5.0* Regenerated'        Pink
            [51, (255, 90, 170)],  # '5.1* Regenerated'
            [52, (255, 180, 210)],  # '5.2* regeneration trend but not completed'
            [53, (200, 160, 180)],  # '5.3* regeneration trend '
            [54, (0, 0, 0)],
            [55, (0, 0, 0)],
            [56, (0, 0, 0)],
            [57, (0, 0, 0)],
            [58, (0, 0, 0)],
            [59, (0, 0, 0)],
            [60, (205, 205, 1)]  # '6.0* Rehabilitated'    Yellow Acid
        ]

        ctable = MANGROVES

        c = gdal.ColorTable()
        for cid in range(0, len(ctable)):
            c.SetColorEntry(cid, ctable[cid][1])

        dst_ds.GetRasterBand(1).SetColorTable(c)


        dst_ds.FlushCache()
        dst_ds = None

    # [10, (1, 150, 1)],  # '1.0* stable with flat trend'       Green
    # [11, (34, 255, 34)],  # '1.0* stable with negative trend'
    # [12, (1, 80, 1)],  # '1.0* stable with positive trend'
    # [13, (220, 255, 220)],  # 1.0* light disturbed but stable
    #
    # [20, (100, 100, 255)],  # '2.0* never stable flat trend       Blue
    # [21, (153, 153, 255)],  # '2.0* never stable negative trend
    # [22, (1, 1, 200)],  # '2.0* never stable positive trend
    #
    # [30, (255, 130, 1)],  # '3.1* Disturbed with flat trend'    Orange
    # [31, (255, 170, 85)],  # '3.1* Disturbed with negative trend'
    # [32, (190, 100, 1)],  # '3.1* Disturbed with positive trend'
    #
    # [40, (255, 1, 1)],  # '4.0* degradation trend     Red
    #
    # [50, (120, 1, 60)],  # '5.0* Regenerated'        Pink
    # [51, (255, 90, 170)],  # '5.1* Regenerated'
    # [52, (255, 180, 210)],  # '5.2* regeneration trend but not completed'
    #
    # [60, (205, 205, 1)]  # '6.0* Rehabilitated'    Yellow Acid









    except Exception as e:
        print(str(e))
        # log.send_error('')
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(out_band)
        dst_ds.FlushCache()
        dst_ds = None

    # log.close()
    #file_handler.write(file_content)
    # numpy.savetxt(file_handler, outTable, fmt='%10.3f', delimiter=',')
    # file_handler.close()



if __name__ == '__main__':
    #infile = sys.argv[1]
    main()

