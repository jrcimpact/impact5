#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
#import cgi

from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal
except ImportError:
    import ogr
    import gdal

import numpy


# Import local libraries
import LogStore
import ImageStore


def estimate_coef(x, y):
    # number of observations/points
    n = numpy.size(x)

    # mean of x and y vector
    m_x, m_y = numpy.mean(x), numpy.mean(y)

    # calculating cross-deviation and deviation about x
    SS_xy = numpy.sum(y * x) - n * m_y * m_x
    SS_xx = numpy.sum(x * x) - n * m_x * m_x

    # calculating regression coefficients
    b_1 = SS_xy / SS_xx
    b_0 = m_y - b_1 * m_x

    return (b_0, b_1)


def main():

    # ALGO DESCRIPTION
    # for every plot:
        # 	1-> compute the STABLE NDII as median-1std of the last 10years
        # 						if STABLE NDII < 0.45 -> not stable or too low -> jump to next plot
        # 	2-> from last observation (recent year) procede BACKWARD until NDII <= STABLE NDII
        # 						if no values are detected -> no drop -> stable forest -> jump to next plot
        # 	3-> compute the MIN of NDII from 87 to YYYY in witch the STABLE NDII has been observed (Y_stbl_NDII)
        # 						if MIN NDII is > 0.35 -> minimum is too high -> flag plot as 'NEED FURTHER INSPECTION'
        # 	4-> identification of the start of regrow after MIN
        # 						if MIN NDII >= 0.15 -> get the YYYY_min (candidate to be a good starting point)
        # 						else SEARCH a suitable value > 0.15 within the range of observation [YYYY_min, Y_stbl_NDII]
        # Consideration:
        # The BACKWARD analysys is safer since we assume that in the last 10Y we do not observe degradation/deforestation. Hence going back is possible to identify drops in the signal down to the MIN NDII and subsequent detection of photosintetic activity (often with a delta/shift of 2/3 years)
        # The SEARCH function in step 4 guarantees the selection of suitable MIN above a threshold of 0.15 (to be decided) and not only the absolute minimum


    # initialize log
    log = LogStore.LogProcessing('Read Tabular Data', 'tabularData')
    log.set_parameter('Input file', '')

    try:
        print('VA')
        out_name = os.path.join(GLOBALS['data_paths']['data'], "test.csv")
        file_handler = open(out_name, 'w')

        header = "1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,stable_median, stable_std, stable_minTh, index_min, data_min, growing_index, index_top, delta_index, valid_trend\n"
        file_handler.write(header)

        #Time, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
        myData_orig = [
            [1987, 0.765, 0.671, 0.672, 0.666, 0.638, 0.68, 0.674, 0.665, 0.678, 0.69, 0.655, 0.616, 0.575, 0.583, 0.604, 0.585, 0.577, 0.545, 0.366, 0.21, 0.273, 0.421, 0.27, 0.232, 0.288],
            [1988, 0.811, 0.671, 0.661, 0.657, 0.651, 0.676, 0.666, 0.659, 0.678, 0.68, 0.647, 0.629, 0.586, 0.585, 0.612, 0.6, 0.635, 0.602, 0.471, 0.285, 0.363, 0.632, 0.379, 0.408, 0.317],
            [1989, 0.797, 0.667, 0.668, 0.651, 0.658, 0.681, 0.669, 0.668, 0.687, 0.693, 0.671, 0.59, 0.571, 0.567, 0.588, 0.574, 0.515, 0.435, 0.28, 0.132, 0.167, 0.258, 0.16, 0.266, 0.276],
            [1990, 0.709, 0.668, 0.651, 0.617, 0.664, 0.685, 0.678, 0.679, 0.689, 0.689, 0.647, 0.601, 0.577, 0.563, 0.59, 0.563, 0.567, 0.46, 0.253, 0.191, 0.22, 0.327, 0.273, 0.29, 0.271],
            [1991, 0.682, 0.654, 0.639, 0.621, 0.636, 0.656, 0.659, 0.648, 0.659, 0.672, 0.645, 0.591, 0.545, 0.547, 0.579, 0.547, 0.536, 0.397, 0.257, 0.127, 0.17, 0.275, 0.325, 0.229, 0.323],
            [1992, 0.841, 0.682, 0.649, 0.631, 0.647, 0.66, 0.661, 0.654, 0.644, 0.68, 0.65, 0.623, 0.562, 0.558, 0.587, 0.46, 0.234, 0.157, 0.048, 0.01, 0.017, 0.09, 0.037, 0.144, 0.296],
            [1993, 0.86, 0.676, 0.633, 0.63, 0.631, 0.656, 0.653, 0.642, 0.641, 0.658, 0.627, 0.632, 0.584, 0.578, 0.611, 0.54, 0.253, 0.126, 0.067, 0.032, 0.012, 0.22, 0.128, 0.212, 0.353],
            [1994, 0.915, 0.689, 0.669, 0.651, 0.663, 0.683, 0.676, 0.662, 0.654, 0.66, 0.628, 0.616, 0.563, 0.545, 0.579, 0.499, 0.031, 0.038, 0.003, -0.022, -0.012, 0.091, 0.013, 0.128, 0.318],
            [1995, 0.878, 0.697, 0.663, 0.645, 0.657, 0.674, 0.674, 0.653, 0.654, 0.658, 0.615, 0.626, 0.576, 0.56, 0.594, 0.541, 0.187, 0.193, 0.171, 0.091, 0.104, 0.201, 0.126, 0.281, 0.376],
            [1996, 0.769, 0.686, 0.654, 0.652, 0.655, 0.668, 0.636, 0.657, 0.653, 0.654, 0.599, 0.6, 0.548, 0.54, 0.575, 0.535, 0.324, 0.352, 0.348, 0.143, 0.163, 0.282, 0.286, 0.436, 0.425],
            [1997, 0.846, 0.69, 0.665, 0.657, 0.66, 0.682, 0.674, 0.662, 0.663, 0.635, 0.58, 0.587, 0.551, 0.541, 0.579, 0.543, 0.114, 0.391, 0.375, 0.057, 0.082, 0.16, 0.333, 0.473, 0.421],
            [1998, 0.767, 0.696, 0.67, 0.664, 0.671, 0.691, 0.677, 0.668, 0.67, 0.635, 0.615, 0.612, 0.56, 0.517, 0.562, 0.502, 0.288, 0.474, 0.498, 0.302, 0.363, 0.417, 0.474, 0.572, 0.412],
            [1999, 0.828, 0.682, 0.658, 0.653, 0.651, 0.671, 0.674, 0.663, 0.658, 0.641, 0.596, 0.595, 0.568, 0.545, 0.582, 0.532, 0.307, 0.588, 0.565, 0.343, 0.39, 0.495, 0.552, 0.591, 0.481],
            [1900, 0.646, 0.663, 0.64, 0.64, 0.641, 0.668, 0.663, 0.651, 0.641, 0.623, 0.575, 0.572, 0.552, 0.552, 0.59, 0.592, 0.611, 0.714, 0.699, 0.504, 0.541, 0.592, 0.667, 0.666, 0.53],
            [1901, 0.665, 0.645, 0.627, 0.625, 0.63, 0.648, 0.645, 0.661, 0.646, 0.615, 0.588, 0.581, 0.558, 0.569, 0.602, 0.595, 0.652, 0.688, 0.68, 0.622, 0.639, 0.631, 0.677, 0.668, 0.57],
            [1902, 0.664, 0.643, 0.629, 0.626, 0.638, 0.661, 0.657, 0.649, 0.644, 0.628, 0.59, 0.564, 0.546, 0.544, 0.586, 0.573, 0.756, 0.718, 0.719, 0.714, 0.719, 0.735, 0.706, 0.663, 0.511],
            [1903, 0.726, 0.665, 0.632, 0.64, 0.651, 0.67, 0.661, 0.656, 0.665, 0.637, 0.602, 0.563, 0.543, 0.525, 0.579, 0.56, 0.73, 0.726, 0.734, 0.735, 0.736, 0.75, 0.736, 0.687, 0.529],
            [1904, 0.697, 0.655, 0.625, 0.633, 0.632, 0.655, 0.648, 0.644, 0.649, 0.615, 0.579, 0.549, 0.537, 0.515, 0.567, 0.547, 0.691, 0.681, 0.695, 0.689, 0.693, 0.716, 0.712, 0.673, 0.527],
            [1905, 0.755, 0.669, 0.638, 0.647, 0.648, 0.671, 0.658, 0.669, 0.671, 0.629, 0.596, 0.574, 0.545, 0.528, 0.57, 0.54, 0.729, 0.69, 0.702, 0.707, 0.728, 0.76, 0.726, 0.709, 0.522],
            [1906, 0.673, 0.653, 0.635, 0.642, 0.638, 0.662, 0.647, 0.647, 0.652, 0.615, 0.564, 0.563, 0.54, 0.52, 0.56, 0.546, 0.716, 0.699, 0.704, 0.725, 0.727, 0.737, 0.714, 0.684, 0.555],
            [1907, 0.586, 0.617, 0.624, 0.624, 0.626, 0.65, 0.64, 0.634, 0.638, 0.596, 0.58, 0.566, 0.542, 0.521, 0.567, 0.549, 0.723, 0.681, 0.688, 0.723, 0.726, 0.731, 0.711, 0.699, 0.524],
            [1908, 0.616, 0.637, 0.627, 0.636, 0.634, 0.653, 0.646, 0.642, 0.647, 0.607, 0.567, 0.556, 0.529, 0.498, 0.548, 0.515, 0.711, 0.672, 0.692, 0.722, 0.724, 0.73, 0.709, 0.681, 0.549],
            [1909, 0.665, 0.647, 0.631, 0.636, 0.638, 0.654, 0.647, 0.645, 0.654, 0.612, 0.574, 0.556, 0.535, 0.509, 0.556, 0.521, 0.697, 0.669, 0.696, 0.733, 0.73, 0.739, 0.704, 0.681, 0.557],
            [1910, 0.612, 0.645, 0.622, 0.625, 0.639, 0.656, 0.648, 0.642, 0.652, 0.585, 0.543, 0.54, 0.513, 0.478, 0.547, 0.497, 0.695, 0.657, 0.68, 0.718, 0.731, 0.728, 0.687, 0.656, 0.522],
            [1911, 0.634, 0.633, 0.616, 0.625, 0.627, 0.641, 0.631, 0.632, 0.641, 0.603, 0.566, 0.556, 0.532, 0.49, 0.538, 0.492, 0.676, 0.648, 0.666, 0.703, 0.704, 0.699, 0.668, 0.573, 0.483],
            [1912, 0.636, 0.639, 0.61, 0.614, 0.623, 0.638, 0.636, 0.617, 0.637, 0.577, 0.549, 0.553, 0.515, 0.483, 0.534, 0.49, 0.673, 0.651, 0.665, 0.704, 0.706, 0.713, 0.673, 0.63, 0.462],
            [1913, 0.52, 0.657, 0.618, 0.638, 0.652, 0.655, 0.642, 0.632, 0.66, 0.522, 0.479, 0.575, 0.526, 0.496, 0.56, 0.521, 0.655, 0.624, 0.647, 0.706, 0.712, 0.697, 0.657, 0.635, 0.459],
            [1914, 0.562, 0.653, 0.626, 0.637, 0.646, 0.65, 0.64, 0.637, 0.656, 0.553, 0.478, 0.569, 0.522, 0.506, 0.554, 0.506, 0.653, 0.62, 0.646, 0.7, 0.706, 0.696, 0.654, 0.53, 0.481],
            [1915, 0.572, 0.662, 0.626, 0.629, 0.647, 0.656, 0.645, 0.639, 0.659, 0.534, 0.496, 0.577, 0.521, 0.494, 0.556, 0.497, 0.631, 0.586, 0.619, 0.683, 0.7, 0.709, 0.632, 0.545, 0.491],
            [1916, 0.6, 0.661, 0.627, 0.626, 0.643, 0.655, 0.642, 0.64, 0.659, 0.569, 0.528, 0.562, 0.514, 0.49, 0.552, 0.5, 0.631, 0.583, 0.61, 0.651, 0.677, 0.686, 0.639, 0.579, 0.493],
            [1917, 0.503, 0.655, 0.622, 0.633, 0.645, 0.653, 0.632, 0.632, 0.659, 0.557, 0.508, 0.58, 0.515, 0.496, 0.556, 0.509, 0.619, 0.589, 0.612, 0.652, 0.683, 0.689, 0.654, 0.589, 0.498],
            [1918, 0.562, 0.647, 0.618, 0.622, 0.645, 0.649, 0.641, 0.633, 0.651, 0.592, 0.554, 0.574, 0.51, 0.497, 0.546, 0.497, 0.631, 0.602, 0.621, 0.644, 0.66, 0.677, 0.645, 0.615, 0.5],
            [1919, 0.613, 0.666, 0.621, 0.627, 0.651, 0.661, 0.652, 0.646, 0.666, 0.577, 0.529, 0.58, 0.507, 0.481, 0.544, 0.494, 0.641, 0.608, 0.629, 0.649, 0.671, 0.696, 0.63, 0.572, 0.479]
        ]

        myData = numpy.transpose(myData_orig)[1:]
        outTable = []

        pos = 0

        startYY = 1987


        for line in myData:
            #print '-------------------'
            #print line
            #outMatrix[pos] = numpy.append(line,[0])
            #numpy.savetxt(file_handler, myData, fmt='%10.3f', delimiter=',')
            # -16 -> last 16 years considered as stable
            stable_val = line[-10:]
            #print stable_val
            stable_median = numpy.median(stable_val)
            stable_std = numpy.std(stable_val)
            stable_minTh =  stable_median - stable_std
            #print stable_minTh
            # add condition if too low

            index_top = 0
            data_min = 0
            delta_index = 0
            index_min = 0
            valid = 0
            growing_index = 0

            if stable_minTh >= 0.48:

                # going backwards to identify drops
                for index_top in range(len(line)-1, 0, -1):
                    if line[index_top] <= stable_minTh:
                        #if index_top > 5 and line[index_top - 3] < stable_minTh and \
                        #                    (line[index_top-1] <= stable_minTh or line[index_top-2] <= stable_minTh or line[index_top-3] <= stable_minTh):
                        #print 'Drop detected, going back ...'
                        #print "IndexTop = "+ str(index_top)
                        #print startYY+index_top
                        #for y in range(x, 1, -1):
                        #    if line[y] <= line[x] and line[y]




                        # get minimum
                        data_min = numpy.min(line[:index_top])
                        index_min, = numpy.where(line[:index_top] == data_min)
                        index_min = index_min[0]





                        if data_min < 0.35 :
                            #print "Absolute MIN:" + str(data_min)
                            #print "Absolute MIN YY :" + str(index_min)
                            #print "Absolute GAP YY:" + str(index_top - index_min)
                            valid = 1

                            # now identify the start of regrow after min
                            if data_min >= 0.15:
                                # value is our candidate
                                growing_index = index_min
                            else:
                                for pos in range(index_min,index_top):
                                    if line[pos] >= 0.15:
                                        growing_index = pos
                                        break
                            #print "Growing val:" + str(growing_index)
                            #print "Growing YY :" + str(startYY+growing_index)
                            # TEST if there are no values bigger then median -1std (stable_minTh)  between min and first drop
                            # in case shift back the top do better define the forest maturity

                            for pos1 in range(growing_index, index_top):
                                #print "Range" + str(startYY+pos1)
                                #print line[pos1]
                                if line[pos1] >= stable_minTh:

                                    #print "Resetting index_Top from "+str(index_top)+" to "+str(pos1)
                                    index_top = pos1
                                    break




                            delta_index = index_top - growing_index
                            break

                        else:
                            print("MIN is too high:" + str(data_min))
                            valid = 0
                            break

                    else:
                        continue
                                      #stable_median, stable_std, stable_minTh, index_min, data_min, growing_index, index_top, delta_index, valid_trend
            row = numpy.append(line, [stable_median, stable_std, stable_minTh, startYY + index_min, data_min, startYY+growing_index, startYY+index_top, delta_index, valid])

            outTable.append(row)












    except Exception as e:
        print(str(e))
        log.send_error('')

    log.close()
    #file_handler.write(file_content)
    numpy.savetxt(file_handler, outTable, fmt='%10.3f', delimiter=',')
    file_handler.close()


if __name__ == '__main__':
    #infile = sys.argv[1]
    main()

