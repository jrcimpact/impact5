#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
from __config__ import *
import Settings
import os
import sys

print('Authenticate')
SETTINGS = Settings.load()
if SETTINGS['internet_options']['username'] != "":
    proxySTR = "http://" + SETTINGS['internet_options']['username'] + ':' + SETTINGS['internet_options']['password'] + '@' + \
               SETTINGS['internet_options']['proxy']
else:
    proxySTR = "http://" + SETTINGS['internet_options']['proxy']


if sys.platform != "linux":
    print('Setting HTTP_PROXY')
    if os.path.exists(GLOBALS['proxy']):
        os.system("set HTTP_PROXY=" + proxySTR)
        os.system("set HTTPS_PROXY=" + proxySTR)
        os.environ["HTTPS_PROXY"] = proxySTR
        os.environ["HTTP_PROXY"] = proxySTR
        print('Setting proxy auth')
    else:
        proxySTR = ''
    os.system('start ' + os.path.join(GLOBALS['root_path'], 'Tools', 'Gee', 'eeAuthenticate.bat ') + proxySTR)
else:
    #os.system('earthengine authenticate')
    print('Authenticate using the /Tools/GEE/eeAuthenticate_docker.sh script')
# import ee
# ee.Authenticate()

