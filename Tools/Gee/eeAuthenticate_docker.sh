#!/usr/bin/env bash
#
docker exec -it $(docker ps| grep 'impact:'|cut -d " " -f 1) bash -c "runuser -u www-data -- earthengine authenticate --auth_mode=notebook"

#for windows users using bash console different from Powershell
#winpty docker exec -it $(docker ps| grep 'impact:'|cut -d " " -f 1) bash -c "runuser -u www-data -- earthengine authenticate --auth_mode=notebook"
