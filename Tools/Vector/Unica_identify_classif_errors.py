#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries

# preliminary work 
#  ogr2ogr Artificial_N.shp Merge_North_v2.shp -of 'ESRI Shapefile' -dialect sqlite -sql "select T1_class from Merge_North_v2 where T1_class in ('1','11','111','112','12','121','122','123','124','125','126','127','128','129','13','131','132','133','14','141','142')"

#  ogr2ogr Artificial_N_buffer500.shp Artificial_N.shp -dialect sqlite -sql "select ST_Buffer(ST_Union(geometry),500) as geometry from Artificial_N"

#
# gdal_proximity.py Artificial_S_2m.tif Artificial_S_2m_distance100.tif -maxdist 100 -ot Byte
# Zonal stats with min of distance band
# call this script
# ogr2ogr -sql "select Error from Merge_South_v2 where Error != '0'" Error_S.shp Merge_South_v2.shp
# ogr2ogr -sql "select Changed from Merge_South_v2 where Changed != '0'" Changed_S.shp Merge_South_v2.shp
#  ....

import os
import sys
import numpy
import json
import time

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from Simonets_PROC_libs import *
import LogStore


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
#
# def getStats_from_image(image, band, fields, position):






def identify_errors(inSHP):
    # initialize output log
    log = LogStore.LogProcessing('Identify_errors', 'Identify_errors')
    log.set_input_file(inSHP)


    T1_class = 'T1_class'
    T2_class = 'T2_class'
    T1_cluster = "T1_cluster"
    PBS_field = "F1_1_mode"
    G_field = "F2_1_mode"
    N_field = "F2_2_mode"
    VV_field = "F2_3_mode"
    Distance_field = "F1_1_min"

    error_field = "Error"
    changed_field = "Changed"


    ESRIDriver = ogr.GetDriverByName('ESRI Shapefile')

    try:
        #-------------------------------------------------------------------
        # ------------------------- OPEN VECTOR  ---------------------------
        #-------------------------------------------------------------------
        try:
            shp_ds = ogr.Open(inSHP,gdal.GA_Update)
            layer = shp_ds.GetLayer(0)
            #layerTmp = shp_ds.GetLayer(0)
            layerDefinition = layer.GetLayerDefn()
            SHPfield=[]
            update_fields=0

            TotalFeatures = layer.GetFeatureCount()

            targetSR = layer.GetSpatialRef()

            layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom
            print("Total feature count: " + str(TotalFeatures))
            i = 0.0

        except Exception as e:
            log.send_error("Error opening file, ensure is not in use by other applications")
            log.close()
            return


        for i in range(layerDefinition.GetFieldCount()):
            SHPfield.append(layerDefinition.GetFieldDefn(i).GetName())

        # general fields

        if changed_field not in SHPfield:
            print("ADD Changed")
            f1 = ogr.FieldDefn(changed_field, ogr.OFTString)
            layer.CreateField(f1)
            f1 = None
        if error_field not in SHPfield:
            print("ADD Error")
            f1 = ogr.FieldDefn(error_field, ogr.OFTString)
            layer.CreateField(f1)
            f1 = None
        if T2_class not in SHPfield:
            print("ADD T2_class")
            f1 = ogr.FieldDefn(T2_class, ogr.OFTString)
            layer.CreateField(f1)
            f1 = None
        print('OK')


        image_pos = 1
        mem_drv = ogr.GetDriverByName('Memory')
        driver = gdal.GetDriverByName('MEM')

        try:

            #-------------------------------------------------------------------
            # ---------------- LOOP FEATURES RASTER  ---------------------------
            #-------------------------------------------------------------------


            i = 0
            feat = layer.GetNextFeature()
            while feat is not None:
                try:
                    gdal.TermProgress(i/TotalFeatures)
                    i += 1
                    
                    
                    ERROR=0
                    CHANGED=0

                    T1Class = feat.GetField(T1_class)
                    if T1Class is not None:
                        T1Class = int(T1Class)
                    outClass = T1Class
                    
                    T1Cluster = feat.GetField(T1_cluster)
                    if T1Cluster is not None:
                        T1Cluster= int(T1Cluster)
                    else:
                        T1Cluster=0
                    

                    PBS = feat.GetField(PBS_field)
                    G = feat.GetField(G_field)
                    N = feat.GetField(N_field)
                    VV = feat.GetField(VV_field)
                    Distance = feat.GetField(Distance_field)

                    if PBS is not None:
                        PBS= int(PBS)
                    else:
                        PBS=0
                    
                    if G is not None:
                        G= int(G)
                    else:
                        G=0
                    
                    if N is not None:
                        N= int(N)
                    else:
                        N=0
                    
                    if VV is not None:
                        VV= int(VV)
                    else:
                        VV=0                        
                    
                    if Distance is not None:
                        Distance= int(Distance)
                    else:
                        Distance=255  


                    # ------      CLASS 6       -----------
                    # Tree cover to  Shrub
                    if T1Class in [6] and PBS in [10,11,12,13] and N < 120 and VV > 125:
                        outClass = 311
                    if T1Class in [6] and PBS in [10,11,12,13] and N >= 120 and VV > 125:
                        outClass = 324

                    # Tree cover to Grass / Shrub
                    if T1Class in [6] and PBS in [14,15,16,25,26,27,28,29,30,31] and VV < 125 :
                        if N < 115:
                            outClass = 321 # Grass
                        else:
                            outClass = 324  # Transition
                    
                    # Tree cover to Grass / Agri
                    if T1Class in [6] and PBS in [28,29,30,31,33,34,41]:
                        if G > 60:
                            outClass = 211 # Grass
                        else:
                            outClass = 321 # Grass



                    # ------      CLASS 3*      -----------
                    if T1Class in [3,31,311] and PBS in [30,31] :
                        if VV < 125:
                            outClass = 321  # Grass
                        else:
                            outClass = 311  # Broadl.

                    if T1Class in [3, 31] and PBS in [25,26] and VV > 125:
                        outClass = 311  #  Broadl.

                    if T1Class in [3, 31] and PBS in [25,26] and VV < 115:
                        outClass = 321  # Grass

                    if T1Class in [31] and PBS in [25,26] and VV < 125:
                        outClass = 321  # Grass

                    # ------      CLASS 333      -----------
                    if T1Class in [333] and PBS in [10,11,12] and VV > 125 and VV < 145:
                        outClass = 311  #  Broadl.
                    if T1Class in [333] and PBS in [26, 13,14,15,16] and VV > 125 and VV < 145:
                        outClass = 323  # Sclerophillus
                    if T1Class in [333] and PBS in [26, 13, 14, 15, 16] and VV > 100 and VV < 145:
                        outClass = 324  # Transition
                    if T1Class in [333] and PBS in [26,28,29, 13, 14, 15, 16] and VV > 100 and VV < 145:
                        outClass = 321  # Grass
                    if T1Class in [333] and PBS in [10,11,12,13,14,15,16,26,28,29] and VV > 145:
                        outClass = 323  # Sclerophillus

                    if T1Class in [6,3,31,311,333]: 
                        if T1Class != T1Cluster and T1Class != outClass :  
                            CHANGED=outClass
                            #ERROR=2
                            outClass = T1Class
                            
                        if T1Class != outClass and int(feat.GetGeometryRef().GetArea()) < 200:
                            CHANGED=outClass
                            outClass = T1Class
                            
                           # ERROR=2
                    
                    if T1Class != outClass:
                        # if distance from artificial is < 50mt do nt change the poly 
                        if Distance < 5:
                            CHANGED=outClass
                            outClass = T1Class
                            #CHANGED=2
                            ERROR=0
                        else:
                            ERROR=1
                        
                        # test if too close to artificial 
                        #layerTmp.SetSpatialFilter(feat.GetGeometryRef().Buffer(100))
                        #featTmp = layerTmp.GetNextFeature()
                        #artificial_found = False
                        #while featTmp is not None:
                        #    if str(featTmp.GetField(T1_class)) in ['1','11','111','112','12','121','122','123','124','125','126','127','128','129','13','131','132','133','14','141','142']: 
                        #        artificial_found = True
                                
                        #        featTmp = None
                                
                        #    else:
                        #        featTmp = layerTmp.GetNextFeature()
                        
                        #layerTmp.ResetReading()
                        #ERROR=1
                        #if artificial_found:
                        #    outClass = T1Class
                        #    ERROR=2
                        
                        #print('Out on loop '+str(i))
                        
                        
                    feat.SetField(T2_class, outClass)
                    feat.SetField(error_field, ERROR)
                    feat.SetField(changed_field, CHANGED)
                    layer.SetFeature(feat)
                    feat.Destroy()
                    feat = None

                    feat = layer.GetNextFeature()
                    
                except Exception as e:
                    print('ERROR')
                    print(str(e))

                    feat.SetField(T2_class, outClass)
                    feat.SetField(error_field, ERROR)
                    feat.SetField(changed_field, CHANGED)
                    layer.SetFeature(feat)
                    feat.Destroy()
                    feat = None
                    feat = layer.GetNextFeature()




        except Exception as e:
            print(str(e))
        # Close DataSources
        shp_ds.Destroy()
        log.send_message("Statistics complete: "+inSHP)



    except Exception as e:
        print(str(e))
        log.send_error("Statistics error: "+str(e).replace("'",''))

    log.close()


if __name__ == "__main__":

    me, inSHP = sys.argv
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    identify_errors(inSHP)