#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.



# Import global libraries
import os
import sys
import glob

import arcpy
from arcpy import env
arcpy.env.parallelProcessingFactor = 100



def RunRawUnion(files, OUT_path_name,  clusterTol, skipDeleteField ):

    try:
        layername = os.path.basename(OUT_path_name)
        if arcpy.Exists(layername):
            arcpy.Delete_management(layername)

        if skipDeleteField == False:
            print('Delete fielsd')
            for tmpfile in files:
                yy = os.path.basename(tmpfile).split('_')[1][2:4]
                print(yy)
                fields = arcpy.ListFields(tmpfile)
                # fields2del = [fields.name for field in fields if (field.name != "Afgkode" and field.name != "Journalnr") ]
                fields2del = []
                for field in fields:

                    if field.name not in ["Afgkode", "Journalnr", "FID", "Shape"]:
                        # fields2del.append(field)
                        arcpy.management.DeleteField(tmpfile, field.name)



                # Aggiungi il nuovo campo con il nuovo nome
                arcpy.management.AddField(tmpfile, "k" + str(yy), "TEXT")
                arcpy.management.AddField(tmpfile, "j" + str(yy), "TEXT")

                # Copia i dati dal vecchio campo al nuovo campo
                with arcpy.da.UpdateCursor(tmpfile, ['Afgkode', 'Journalnr', "k" + str(yy), "j" + str(yy)]) as cursor:
                    for row in cursor:
                        row[2] = row[0]
                        row[3] = row[1]
                        cursor.updateRow(row)

                # Elimina il vecchio campo
                arcpy.management.DeleteField(tmpfile, 'Afgkode')
                arcpy.management.DeleteField(tmpfile, 'Journalnr')

        print('Union')
        arcpy.Union_analysis(files, layername, "NO_FID", clusterTol)
        arcpy.FeatureClassToShapefile_conversion(layername, os.path.dirname(OUT_path_name))
        if arcpy.Exists(layername):
            arcpy.Delete_management(layername)



    except Exception as e:
        print(e)

def Runparcel(files, OUTname, clusterTol, fixTopo ):

    try:

        # ####################################################
        #      selelct original poly from input shapefile
        # ####################################################
        tmpfiles=[]
        for file in files:
            yy = file.split('_')[1][2:4]
            print(yy)
            name = os.path.basename(file).replace('.shp', '')
            # SQL = "select geometry, Afgkode as k" + str(yy) + ", Journalnr as j" + str(yy) +" from "+str(name) +"  where GeometryType(geometry) in ('POLYGON','MULTIPOLIGON')"
            SQL = "select geometry, Afgkode as k" + str(yy) + ", Journalnr as j" + str(yy) +" from "+str(name)

            tmpfile = os.path.join(os.path.dirname(OUTname),os.path.basename(file).replace('.shp',"_simple.shp"))
            if not os.path.exists(tmpfile):
                print(os.path.dirname(OUTname))
                print(tmpfile)
                # arcpy.management.CopyFeatures(file, tmpfile)
                # outFeatureClass = tmpfile.replece('.shp', '_single.shp')
                print('MULTI 2 SINGLE')
                arcpy.MultipartToSinglepart_management(file, tmpfile.replace('.shp','_SP.shp'))
                print('RepairGeometry_management')
                arcpy.RepairGeometry_management(tmpfile.replace('.shp','_SP.shp'))
                arcpy.Update_analysis(tmpfile.replace('.shp','_SP.shp'), tmpfile.replace('.shp','_SP.shp'), tmpfile, "BORDERS", clusterTol)



                # arcpy.Generalize_edit(tmpfile, "2 Meters")

                if fixTopo:
                # Define the path to your geodatabase
                    gdb_path = tmpfile.replace('.shp','.mdb')

                    out_folder_path = os.path.dirname(tmpfile)
                    out_name = os.path.basename(tmpfile).replace('.shp', '.gdb')
                    arcpy.CreatePersonalGDB_management(out_folder_path, out_name)
                    # arcpy.FeatureClassToGeodatabase_conversion(tmpfile, tmpfile.replace('.shp', '.gdb'))
                    print('GDB done ')

                    # Create a feature dataset
                    arcpy.management.CreateFeatureDataset(gdb_path, r'FeatureDataset', arcpy.Describe(tmpfile).spatialReference)
                    print('DB done')

                    # Add the feature class to the feature dataset
                    arcpy.management.CopyFeatures(tmpfile, gdb_path + r'/FeatureDataset/feature_class')


                           # Create a topology
                    arcpy.management.CreateTopology(gdb_path + r'/FeatureDataset', 'Topology')

                    arcpy.management.AddFeatureClassToTopology(gdb_path + r'/FeatureDataset/Topology',
                                                               gdb_path + r'/FeatureDataset/feature_class')

                    # Add topology rules (example: Must Not Have Dangles)
                    arcpy.management.AddRuleToTopology(gdb_path + r'/FeatureDataset/Topology',
                                                       'Must Not Overlap (Area)',
                                                       gdb_path + r'/FeatureDataset/feature_class')

                    print('Rules')
                    # Validate the topology
                    error_table = arcpy.management.ValidateTopology(gdb_path + r'/FeatureDataset/Topology', 'Full_Extent')
                    print('Validated')
                    error_fc = gdb_path + r'/FeatureDataset'


                    # Export topology errors
                    topology_path = gdb_path + r'/FeatureDataset/Topology'
                    arcpy.management.ExportTopologyErrors(topology_path, error_fc,'Err')
                    print('Error saved internally')
                    arcpy.conversion.FeatureClassToShapefile([gdb_path + r'/FeatureDataset/Err_poly'], os.path.dirname(tmpfile))
                    print('Validated end')



                fields = arcpy.ListFields(tmpfile)
                # fields2del = [fields.name for field in fields if (field.name != "Afgkode" and field.name != "Journalnr") ]
                fields2del=[]
                for field in fields:
                    print(field.name)
                    if field.name not in ["Afgkode","Journalnr","FID","Shape"]:
                        # fields2del.append(field)
                        arcpy.management.DeleteField(tmpfile, field.name)

                print('Deleted')

                # Aggiungi il nuovo campo con il nuovo nome
                arcpy.management.AddField(tmpfile, "k"+str(yy), "TEXT")
                arcpy.management.AddField(tmpfile, "j"+str(yy), "TEXT")

                # Copia i dati dal vecchio campo al nuovo campo
                with arcpy.da.UpdateCursor(tmpfile, ['Afgkode', 'Journalnr',"k"+str(yy),"j"+str(yy)]) as cursor:
                    for row in cursor:
                        row[2] = row[0]
                        row[3] = row[1]
                        cursor.updateRow(row)
                print('Copy field')
                # Elimina il vecchio campo
                arcpy.management.DeleteField(tmpfile, 'Afgkode')
                arcpy.management.DeleteField(tmpfile, 'Journalnr')




            tmpfiles.append(tmpfile)

        print('Running Union')

        print(tmpfiles)
        print(OUTname)
        ct = 0
        tmp1 = tmpfiles.pop(0)
        tmp2 = tmpfiles.pop(0)
        print('Processing ')
        print(tmp1)
        print(tmp2)

        arcpy.Union_analysis([tmp1,tmp2], OUTname.replace('.shp','')+'_ct'+str(ct)+'MP.shp', "ALL", clusterTol)
        arcpy.MultipartToSinglepart_management(OUTname.replace('.shp', '') + '_ct' + str(ct) + 'MP.shp',
                                               OUTname.replace('.shp', '') + '_ct' + str(ct) + 'SP.shp')
        print('RepairGeometry_management Union')
        arcpy.RepairGeometry_management(OUTname.replace('.shp', '') + '_ct' + str(ct) + 'SP.shp')
        print('Update Analysis Union')
        arcpy.Update_analysis(OUTname.replace('.shp', '') + '_ct' + str(ct) + 'SP.shp',
                              OUTname.replace('.shp', '') + '_ct' + str(ct) + 'SP.shp',
                              OUTname.replace('.shp', '') + '_ct' + str(ct) + '.shp', "BORDERS", 1)
        print('Cleaning Fields')


        print('TMP : '+ OUTname.replace('.shp','')+'_ct'+str(ct)+'.shp' )


        while len(tmpfiles) > 0:

            tmp1 = tmpfiles.pop(0)
            print('Processing ' + tmp1)
            ct+=1
            arcpy.Union_analysis([OUTname.replace('.shp','')+'_ct'+str(ct-1)+'.shp', tmp1], OUTname.replace('.shp','')+'_ct'+str(ct)+'MP.shp', "ALL", clusterTol)
            arcpy.MultipartToSinglepart_management(OUTname.replace('.shp','')+'_ct'+str(ct)+'MP.shp', OUTname.replace('.shp','')+'_ct'+str(ct)+'SP.shp')
            print('RepairGeometry_management Union')
            arcpy.RepairGeometry_management(OUTname.replace('.shp','')+'_ct'+str(ct)+'SP.shp')
            print('Update Analysis Union')
            arcpy.Update_analysis(OUTname.replace('.shp','')+'_ct'+str(ct)+'SP.shp', OUTname.replace('.shp','')+'_ct'+str(ct)+'SP.shp', OUTname.replace('.shp','')+'_ct'+str(ct)+'.shp', "BORDERS", 1)
            print('Cleaning Fields')
            fields = arcpy.ListFields(OUTname.replace('.shp','')+'_ct'+str(ct)+'.shp')
            # fields2del = [fields.name for field in fields if (field.name != "Afgkode" and field.name != "Journalnr") ]
            fields2del = []
            for field in fields:
                print(field)
                print(field.name)
                if field.name.startswith('FID_'):
                    # fields2del.append(field)
                    arcpy.management.DeleteField(OUTname.replace('.shp','')+'_ct'+str(ct)+'.shp', field.name)




        arcpy.Rename_management(OUTname.replace('.shp','')+'_ct'+str(ct)+'.shp', OUTname)

        print('Union done')
        fields = arcpy.ListFields(OUTname)
        # fields2del = [fields.name for field in fields if (field.name != "Afgkode" and field.name != "Journalnr") ]
        fields2del = []
        for field in fields:
            print(field)
            print(field.name)
            if field.name.startswith('FID_'):
                # fields2del.append(field)
                arcpy.management.DeleteField(OUTname, field.name)

        print('Adding Area field')
        arcpy.management.AddField(OUTname, "Area", "DOUBLE")
        exp = "!SHAPE.AREA@SQUAREMETERS!"
        arcpy.CalculateField_management(OUTname, "Area", exp, "PYTHON_9.3")


        print('OUT file: ', OUTname)



    except Exception as e:
        print(e)

    return
        

if __name__ == "__main__":

    try:
        # ----------------------------------------------------------------------------
        # --------------------------   CHANGE your VAR HERE  -------------------------
        # ----------------------------------------------------------------------------
        overwrite = False  # or False
        IN_directory = 'E:/dev/IMPACT5/DATA/CROP/SHP3'
        IN_filter = 'Marker*.shp'
        OUT_name = 'Parcel_Merge'  # prefix with GridID  is taken from the loop
        geodatabase_name = "GeoDB.gdb"
        gdb_path = os.path.join(IN_directory,geodatabase_name)
        # Create a file geodatabase
        if arcpy.Exists(gdb_path):
            print("Geodatabase exists.")
        else:
            print("Geodatabase does not exist.")
            arcpy.CreateFileGDB_management(IN_directory, geodatabase_name)

        print('DBcreated successfully')
        env.workspace = gdb_path

        tolerance = 2

        fixTopo = False   # validate Topology - converts to GDB but takes 10h or more per shp
        # ----------------------------------------------------------------------------
        # ----------------------------------------------------------------------------
        grid_tile_list2mosaic = []

        splitFiles = ['E:/dev/IMPACT5/DATA/CROP/SHP3/Grid0.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid1.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid2.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid3.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid4.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid5.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid6.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid7.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid8.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid9.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid10.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid11.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid12.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid13.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid14.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid15.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid16.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid17.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid18.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid19.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid20.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid21.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid22.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid23.shp',
                      'E:/dev/IMPACT5/DATA/CROP/SHP3/Grid24.shp'

                      ]

        for grid in splitFiles:
            gridName = os.path.basename(grid).replace('.shp','')
            print(gridName)
            OUT_layer_name = gridName + '_' + OUT_name
            OUT_directory = os.path.join(IN_directory,'OUT',gridName)
            IN = os.path.join(IN_directory,IN_filter)
            OUT_path_name = os.path.join(OUT_directory,OUT_layer_name)


            print(OUT_directory)
            print(OUT_path_name)
            grid_tile_list2mosaic.append(OUT_path_name+'.shp')
            if not os.path.exists(OUT_directory):
                os.makedirs(OUT_directory)



            if os.path.exists(OUT_path_name+'.shp') and overwrite == False:
                print('Output already there. Exit or use overwrite = True')
                fields = arcpy.ListFields(OUT_path_name+'.shp')
                # fields2del = [fields.name for field in fields if (field.name != "Afgkode" and field.name != "Journalnr") ]
                fields_to_delete = [field.name for field in fields if field.name.startswith("FID_")]
                if fields_to_delete:
                    arcpy.DeleteField_management(OUT_path_name+'.shp', fields_to_delete)
                    print("Deleted fields")
                else:
                    print("No fields found starting with the specified string.")

                continue

            if overwrite:
                files2del = glob.glob(os.path.join(OUT_directory,'*.shp'))
                for file in files2del:
                    try:
                        print(file)
                        arcpy.Delete_management(file)
                    except Exception as e:
                        print(e)


            files = glob.glob(IN)

            if len(files)== 0:
                print('No files')
                sys.exit()

            tmpINdir = os.path.join(OUT_directory, 'IN')
            if not os.path.exists(tmpINdir):
                os.makedirs(tmpINdir)

            skipDeleteField = True
            for file in files:
                # Create feature layers

                tmpOut = os.path.join(tmpINdir, os.path.basename(file))
                print('tmpOut')
                print(tmpOut)
                if not os.path.exists(tmpOut):
                    skipDeleteField = False
                    arcpy.MakeFeatureLayer_management(file, "input_layer")

                    # Select features in the input shapefile based on the extent of the selecting shapefile
                    arcpy.SelectLayerByLocation_management("input_layer", "INTERSECT", grid)

                    # Optionally, you can save the selected features to a new shapefile


                    arcpy.CopyFeatures_management("input_layer", tmpOut)
                    arcpy.Delete_management("input_layer")


            files = glob.glob( os.path.join(tmpINdir,IN_filter))
            if len(files)== 0:
                print('No files')
                sys.exit()
            print('Start_processing')
            print(files)
            #Runparcel(files, OUT_path_name, tolerance, fixTopo)

            RunRawUnion(files, OUT_path_name, tolerance, skipDeleteField)   # replace OUT_name with OUT_path_name
            # Runparcel(files, OUT_path_name, tolerance, fixTopo)

        finalMerge = os.path.join(IN_directory, OUT_name )
        print('Preparing final union ')
        if not os.path.exists(finalMerge+'.shp'):

            if arcpy.Exists(OUT_name):
                arcpy.Delete_management(OUT_name)
            print(grid_tile_list2mosaic)
            print('Grid Union')
            arcpy.Union_analysis(grid_tile_list2mosaic, OUT_name, "NO_FID", tolerance)
            arcpy.FeatureClassToShapefile_conversion(OUT_name, finalMerge+'.shp')
            if arcpy.Exists(OUT_name):
                arcpy.Delete_management(OUT_name)


    except Exception as e:
        print('Parcel  Error')
        print(str(e))

    finally:
        # log.close()
        print('Done')

    sys.exit()






