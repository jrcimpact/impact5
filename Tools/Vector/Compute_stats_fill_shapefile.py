#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import json
import time

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from Simonets_PROC_libs import *
import LogStore


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
#
# def getStats_from_image(image, band, fields, position):






def Compute_stats_fill_shapefile(inSHP, inIMGS, nodata_list, statsList, statsBands, noLog = False):
    # initialize output log
    if not noLog:
        log = LogStore.LogProcessing('Statistics', 'statistics')
        log.set_input_file(inSHP)
        log.set_input_file(inIMGS)


        if statsBands != 'All':
            log.set_parameter("Bands ", str(statsBands))
        else:
            log.set_parameter("Bands ", "All")


        log.send_message('Parameters: '+ statsList)


    #['i_area', 'i_para', 'i_count', 'i_percent', 'i_mean', 'i_median', 'i_mode', 'i_sd', 'i_min', 'i_max']

    if statsList == '':
        statsType = []
    else:
        statsType = statsList.split(',')

    #statsType = ['i_area', 'i_count', 'i_percent' ]
    # statsType = ['i_count', 'i_percent', 'i_mean']
    # statsType.append(fieldID)

    outDriver = ogr.GetDriverByName('ESRI Shapefile')

    #inIMG = inIMGS[0]
    goodIMG = []
    used_fieldName = []
    try:

        # -------------------------------------------------------------------
        # ------------------------- STATS LIST    ---------------------------
        # -------------------------------------------------------------------

        if len(statsType) == 0:
            if not noLog:
                log.send_error("No option selected")
                log.close()
            return

        #-------------------------------------------------------------------
        # ------------------------- TEST OVERLAPS  -------------------------
        #-------------------------------------------------------------------
        for inIMG in inIMGS:
            if not test_overlapping(inSHP,inIMG):
                if not noLog:
                    log.send_error("No overlap between: "+inSHP+" "+inIMG)
            else:
                raster_ds = gdal.Open(inIMG, gdal.GA_ReadOnly)
                num_bands = raster_ds.RasterCount
                raster_ds = None

                if statsBands == 'All':
                    bands2use = range(1, num_bands + 1)
                    goodIMG.append([inIMG, bands2use])
                else:

                    if int(statsBands) <= num_bands:
                        bands2use = [int(statsBands)]
                        goodIMG.append([inIMG, bands2use])
                    else:
                        if not noLog:
                            log.send_error("Wrong band for image: " + inIMG+'. Skipping file')

        if len(goodIMG) == 0:
            if not noLog:
                log.send_error("No intersecting images or invalid band selection")
                log.close()
            return

        # -------------------------------------------------------------------
        # -----------------------  SAVE FIELD INFO on file ------------------
        # -------------------------------------------------------------------

        ct = 1
        fieldName_2use = []
        text_file = open(inSHP.replace('.shp','.txt'), "w")
        for item in goodIMG:
            for b in item[1]:
                for stat in statsType:
                    if stat in ['i_area', 'i_para']:
                        if stat not in fieldName_2use:
                            fieldName_2use.append(stat)
                    else:
                        fieldName_2use.append('F'+str(ct)+'_'+str(b)+'_'+stat)
            text_file.write('F'+str(ct) + ' -> ' + item[0] + '\n')
            ct +=1
        text_file.close()


        #-------------------------------------------------------------------
        # ------------------------- OPEN VECTOR  ---------------------------
        #-------------------------------------------------------------------
        try:
            shp_ds = ogr.Open(inSHP, gdal.GA_Update)
            layer = shp_ds.GetLayer(0)
            layerDefinition = layer.GetLayerDefn()
            SHPfield=[]
            update_fields = 0
            missingFeatures = 0

            TotalFeatures = layer.GetFeatureCount()

            targetSR = layer.GetSpatialRef()

            layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom
            print("Total feature count: " + str(TotalFeatures))
            i = 0.0

        except Exception as e:
            if not noLog:
                log.send_error("Error opening file, ensure is not in use by other applications")
                log.close()
            return


        for i in range(layerDefinition.GetFieldCount()):
            #print "------------------------------------"
            SHPfield.append(layerDefinition.GetFieldDefn(i).GetName())
            #print layerDefinition.GetFieldDefn(i).GetName()

        # general fields
        for field in fieldName_2use:
            if field not in SHPfield:
                print("ADD " + str(field))
                f1 = ogr.FieldDefn(field, ogr.OFTReal)
                layer.CreateField(f1)
                f1 = None

        # flag to not recompute area and para
        area_already_calc = False
        para_already_calc = False
        feat_area_already_calc = False
        feat_para_already_calc = False

        image_pos = 1
        mem_drv = ogr.GetDriverByName('Memory')
        driver = gdal.GetDriverByName('MEM')

        for item in goodIMG:

            fullPath = item[0]
            bands = item[1]
            print('--------------------------')
            print("Processing " + fullPath)
            print('--------------------------')
            # getStats_from_image(fullPath, bands, fieldName_2use, image_pos)

            #-------------------------------------------------------------------
            # ------------------------- OPEN RASTER  ---------------------------
            #-------------------------------------------------------------------
            class_ds = gdal.Open(fullPath, gdal.GA_ReadOnly)
            bandDs = class_ds.GetRasterBand(1)
            nodata = bandDs.GetNoDataValue()
            if not noLog:
                log.send_message('Input : ' + fullPath)
                log.send_message("Nodata value: " + str(nodata))
                log.send_message("Additional Nodata value: " + str(nodata_list))

            DataType = bandDs.DataType
            DataType = gdal.GetDataTypeName(DataType)
            bandDs = None
            if nodata_list != "":
                if DataType == 'Byte' and (float(nodata_list) < 0 or float(nodata_list) > 256 or "." in nodata_list):
                    nodata_list = ''
                    if not noLog:
                        log.send_warning("Additional Nodata is not used: outside image Byte range [0-255]")

            cols = class_ds.RasterXSize
            rows = class_ds.RasterYSize
            imgGeo = class_ds.GetGeoTransform()
            sourceSR = osr.SpatialReference()
            sourceSR.ImportFromWkt(class_ds.GetProjectionRef())

            pixelWidth = imgGeo[1]
            pixelHeight = imgGeo[5]

            ulx = imgGeo[0]
            uly = imgGeo[3]
            lrx = imgGeo[0] + cols * pixelWidth
            lry = imgGeo[3] + rows * pixelHeight

            print(ulx, lrx, uly, lry)

            wkt = "POLYGON ((" + str(ulx) + " " + str(uly) + "," + str(lrx) + " " + str(uly) + "," + str(
                lrx) + " " + str(lry) + "," + str(ulx) + " " + str(lry) + "," + str(ulx) + " " + str(uly) + "))"

            rasterExtentGeom = ogr.CreateGeometryFromWkt(wkt)
            if gdal.VersionInfo()[0] == '3':
                sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

            transformToshp = osr.CoordinateTransformation(sourceSR, targetSR)
            transformToimg = osr.CoordinateTransformation(targetSR, sourceSR)
            reproject = False
            print(sourceSR.ExportToProj4())
            print(targetSR.ExportToProj4())
            if sourceSR.ExportToProj4() != targetSR.ExportToProj4():
                reproject = True
                rasterExtentGeom.Transform(transformToshp)
                bbox = rasterExtentGeom.GetEnvelope()
                ulx = bbox[0]
                lrx = bbox[1]
                uly = bbox[3]
                lry = bbox[2]
            else:
                print("Same projection")

            # print "RASTER geom"
            # print rasterExtentGeom
            #
            # print "Bands:"
            # print num_bands
            # print "No data :"
            # print nodata
            # print "USER No data :"
            # print nodata_list
            # print '-------------------'

            # remove features outside image extent
            layer.ResetReading()
            layer.SetSpatialFilterRect(ulx, lry, lrx, uly)
            countTotal = layer.GetFeatureCount()
            missingFeatures = TotalFeatures - countTotal
            feat = layer.GetNextFeature()
            emptyFeatures = 0

            #-------------------------------------------------------------------
            # ---------------- LOOP FEATURES RASTER  ---------------------------
            #-------------------------------------------------------------------


            i = 0

            while feat is not None:
                gdal.TermProgress(i/countTotal)
                i += 1
                feat_geom = feat.GetGeometryRef().Clone()
                if reproject:
                    # Reprojecting
                    feat_geom.Transform(transformToimg)

                x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()

                # print "Repositioned "
                MinX = int(round((x_min - imgGeo[0]) / pixelWidth))
                MaxX = int(round((x_max - imgGeo[0]) / pixelWidth))

                MinY = int(round((y_max - imgGeo[3]) / pixelHeight))
                MaxY = int(round((y_min - imgGeo[3]) / pixelHeight))

                deltaX = MaxX - MinX
                deltaY = MaxY - MinY
                if deltaX == 0:
                    deltaX = 1
                if deltaY == 0:
                    deltaY = 1
                src_offset = [MinX, MinY, deltaX, deltaY]

                try:

                    if min(src_offset) < 0 or src_offset[0] > cols or src_offset[1] > rows:
                        raise Exception('Box is out')
                        continue
                    # calculate new geotransform of the feature subset
                    new_gt = (
                        (imgGeo[0] + (src_offset[0] * pixelWidth)),
                        pixelWidth,
                        0.0,
                        (imgGeo[3] + (src_offset[1] * pixelHeight)),
                        0.0,
                        pixelHeight
                    )
                    mem_ds = mem_drv.CreateDataSource('out')
                    mem_layer = mem_ds.CreateLayer('poly', sourceSR, geom_type=ogr.wkbPolygon)
                    newfeature = ogr.Feature(mem_layer.GetLayerDefn())
                    newfeature.SetGeometry(feat_geom)
                    mem_layer.CreateFeature(newfeature)

                    # Rasterize it
                    rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
                    rvds.SetGeoTransform(new_gt)
                    new_gt = None

                    try:
                        gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])
                    except:
                        continue

                    newfeature = None
                    mem_layer = None
                    mem_ds = None
                    rv_array = rvds.ReadAsArray()
                    #  ------  if polygon covers a tiny portion of a pixel, keep the pixel
                    if rv_array.max() == 0:
                        rv_array.fill(1)
                    rvds = None

                    for band in bands:
                        class_band = class_ds.GetRasterBand(band)

                        statsFieldName = "F"+str(image_pos)+'_'+str(band)+'_'

                        try:
                            src_array = class_band.ReadAsArray(src_offset[0], src_offset[1],src_offset[2], src_offset[3])
                        except:
                            continue

                        class_band = None
                        masked = None
                        masked = numpy.ma.masked_invalid(src_array)
                        if nodata is not None:
                            masked = numpy.ma.masked_values(masked, nodata)
                        if nodata_list != '':
                            masked = numpy.ma.masked_values(masked, float(nodata_list))
                        # mask value in virtual raster outside shape boundaries (in case shp is not a box)
                        masked = numpy.ma.array(masked, mask= rv_array == 0)
                        totalPixel = numpy.count_nonzero(rv_array)
                        # # GET number of non 0 (includes NaN)
                        # countnonzeroWithNaN = numpy.count_nonzero(masked)

                        # GET numbero of NaN including 0
                        countvalid = masked.count()
                        #percent = float(float(totalPixel/float(countvalid))*100.)
                        percent = float(float(countvalid / float(totalPixel)) * 100.)
                        # if feat.GetField("ID") == 1834:
                        #     cellMean = numpy.nanmean(masked)
                        #     cellMedian = numpy.ma.median(masked)
                        #     print("\n\n\n")
                        #     print(feat.GetField("ID"))
                        #     print('Offset: ' + str(src_offset))
                        #     print('Feature Shape \n' + str(rv_array))
                        #
                        #     print('min: ' + str(src_array.min()))
                        #     print('max: ' + str(src_array.max()))
                        #     print('NaN: ' + str(nodata))
                        #
                        #     print('rv_array')
                        #     print(rv_array)
                        #     print('tatal pixel: ' + str(totalPixel))
                        #
                        #     print("Number of masked : " + str(totalPixel - countvalid))
                        #
                        #     print('Valid removing Nodata: ' + str(countvalid))
                        #     #print 'Non 0 removing Nodata: '+str(countnonzero)
                        #     print('SUM of valid: ' + str(masked.sum()))
                        #
                        #     print("% of data (incl. 0) without NaN: " + str(percent))
                        #     print("Cell Mean: " + str(cellMean))
                        #     print("Cell cellMedian: " + str(cellMedian))
                        #
                        #     print('Original \n' + str(src_array))
                        #     print('Masked \n' + str(masked))
                        #
                        #     print(str(float(feat.GetGeometryRef().GetArea())))
                        #
                        #     time.sleep(10)


                        for stat in statsType:

                            if stat == 'i_area' and not area_already_calc:
                                feat.SetField(stat, str(float(feat.GetGeometryRef().GetArea())))
                                feat_area_already_calc = True
                                continue
                            # http://www.umass.edu/landeco/research/fragstats/documents/Metrics/Shape%20Metrics/Metrics/P7%20-%20PARA.htm
                            if stat == 'i_para' and not para_already_calc:
                                feat.SetField(stat, float(feat.GetGeometryRef().Boundary().Length()/feat.GetGeometryRef().GetArea()))
                                feat_para_already_calc = True
                                continue

                            if stat == 'ct':
                                feat.SetField(statsFieldName + stat, int(countvalid))

                            if stat == 'pct':
                                feat.SetField(statsFieldName + stat, float(percent))

                            if stat == 'mean':
                                feat.SetField(statsFieldName + stat, float(numpy.ma.mean(masked)))

                            elif stat == 'med':
                                feat.SetField(statsFieldName + stat, float(numpy.ma.median(masked)))

                            elif stat == 'mode':
                                 feat.SetField(statsFieldName + stat, float(MYmode2D(masked)))

                            elif stat == 'sd':
                                feat.SetField(statsFieldName + stat, float(numpy.std(masked)))

                            elif stat == 'min':
                                feat.SetField(statsFieldName + stat, float(numpy.min(masked)))

                            elif stat == 'max':
                                feat.SetField(statsFieldName + stat, float(numpy.max(masked)))

                    layer.SetFeature(feat)
                    feat.Destroy()


                    feat = None
                    feat_geom = None
                    src_array = None
                    rv_array = None
                    masked = None
                    percent = None
                    cellMean = None
                    countvalid = None
                    countnonzeroWithNaN = None
                    countnonzero = None
                    totalPixel = None
                    feat = layer.GetNextFeature()

                except Exception as e:
                    # print('ERROR')
                    # print(str(e))

                    emptyFeatures += 1
                    for val in statsType:
                        if str(val) in ['i_area', 'i_para']:
                            continue
                        else:
                            for band in bands:
                                statsFieldName = "F" + str(image_pos) + '_' + str(band) + '_'
                                feat.SetField(statsFieldName + val, '')
                    layer.SetFeature(feat)
                    feat.Destroy()
                    feat = None
                    feat = layer.GetNextFeature()

                    feat_geom = None
                    src_array = None
                    rv_array = None
                    masked = None
                    percent = None
                    cellMean = None
                    countvalid = None
                    countnonzeroWithNaN = None
                    countnonzero = None
                    totalPixel = None




            if feat_area_already_calc :
                area_already_calc = True
            if feat_para_already_calc:
                para_already_calc = True


            class_ds = None
            image_pos += 1
        # Close DataSources
        shp_ds.Destroy()
        if not noLog:
            log.send_message("Statistics complete: "+inSHP)



    except Exception as e:
        print(str(e))
        if not noLog:
            log.send_error("Statistics error: "+str(e).replace("'",''))

    if not noLog:
        if missingFeatures > 0:
            log.send_warning(str(missingFeatures) + " have no intersection: untouched")
        if emptyFeatures > 0 :
            log.send_warning(str(emptyFeatures)+ " have partial intersection: set to 0")

        log.close()


if __name__ == "__main__":

    me, inSHP, inIMG, nodata_list, statsList, statsBands = sys.argv
    Compute_stats_fill_shapefile(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
