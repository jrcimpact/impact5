#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import numpy

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import ImageProcessing


if __name__ == "__main__":

    me, overwrite, full_path, layername, outname, resolution, ids = sys.argv

    log = LogStore.LogProcessing('Vector 2 Raster ', 'vector2raster')
    try:
        ids = json.loads(ids)
    except:
        log.send_error('Class Id : Empty! ')
        log.close()
        sys.exit()

    if not outname.endswith('.tif'):
        outname += ".tif"
    outname = os.path.join(os.path.dirname(full_path.replace(".shp", "")), outname)
    overwrite = True if overwrite == "Yes" else False

    log.set_parameter('Input Vector', full_path)
    log.set_parameter('Input Layer', layername)
    log.set_parameter('Output', outname)
    log.set_parameter('Output resolution', resolution)
    log.set_parameter('Overwrite', overwrite)
    log.set_parameter('Class Ids: ', ', '.join(ids))


    ImageProcessing.validPathLengths(outname, None, 8, log)
    # Define pixel_size and NoData value of new raster
    if not overwrite and os.path.exists(outname):
        log.send_warning(outname+' <b> Already Exists </b>')
        log.close()
        sys.exit()
    try:
        if os.path.exists(outname):
            os.remove(outname)
        if os.path.exists(outname+'.aux.xml'):
            os.remove(outname+'.aux.xml')
        log.send_message("Deleting existing output file.")

        if os.path.exists(outname+"_tmp"):
            os.remove(outname+"_tmp")
        if os.path.exists(outname+'_tmp.aux.xml'):
            os.remove(outname+'_tmp.aux.xml')

    except:
        log.send_error(outname+' cannot be deleted.')
        log.close()
        sys.exit()

    NoData_value = 0
    resolution = float(resolution)
    # Filename of input OGR file
    vector_fn = 'test.shp'

    # Open the data source and read in the extent
    source_ds = ogr.Open(full_path, 0)
    source_layer = source_ds.GetLayer()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()

    # Create the destination data source
    x_res = int((x_max - x_min) / resolution)
    y_res = int((y_max - y_min) / resolution)
    target_ds = gdal.GetDriverByName('GTiff').Create(outname+'_tmp', x_res, y_res, 1, gdal.GDT_Byte,
                                                     options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
    raster = numpy.zeros((y_res, x_res), dtype=numpy.byte)
    #raster += 1
    target_ds.GetRasterBand(1).WriteArray(raster)
    target_ds.SetGeoTransform((x_min, resolution, 0, y_max, 0, -resolution))
    target_ds.SetProjection(source_layer.GetSpatialRef().ExportToWkt())

    filter = ''
    # nodatafilter = False
    for id in ids:
        # if str(id) == '0':
        #     nodatafilter = True
        # else:
            filter += layername + " = '"+str(id)+"' or "
    filter = filter[:-4]
    source_layer.SetAttributeFilter(filter)
    gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])

    # if nodatafilter:
    #     # source_ds.ReleaseResultSet(source_layer)
    #     source_layer.SetAttributeFilter(layername + " = '0'")
    #     gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[0])
    #     target_ds.GetRasterBand(1).SetNoDataValue(0)

    target_ds = None
    source_ds = None
    source_layer = None
    if os.path.exists(outname+"_tmp"):
        os.rename(outname+"_tmp", outname)
    if os.path.exists(outname+'_tmp.aux.xml'):
        os.rename(outname+'_tmp.aux.xml', outname+'.aux.xml')

    log.send_message("Raster 2 Vector complete: "+outname)

    log.close()
    sys.exit()
