#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import json
import time

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from Simonets_PROC_libs import *
import LogStore


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
#
# def getStats_from_image(image, band, fields, position):






def Compute_mode_shapefile(inSHP, noLog = True):
    # initialize output log
    if not noLog:
        log = LogStore.LogProcessing('Statistics', 'statistics')
        log.set_input_file(inSHP)

    try:

        # open SHP file
        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataSource = driver.Open(inSHP, 0)

        sql = "select T1_class, count(T1_class), PBS, AVG(F1_1_mode),AVG(F1_2_mode),AVG(F1_3_mode) from '" + str(os.path.basename(inSHP)).replace('.shp', '') + "' group by T1_class, PBS"
        layer = dataSource.ExecuteSQL(sql, dialect='SQLITE')
        if layer != None:
            for i, feature in enumerate(layer):
                print(float(feature.GetField(0)), float(feature.GetField(1)), float(feature.GetField(2)), float(feature.GetField(3)),float(feature.GetField(4)),float(feature.GetField(5)))

        else:
            pass

        feature = None

    except Exception as e:
        print(str(e))
        if not noLog:
            log.send_error("Statistics error: "+str(e).replace("'",''))




if __name__ == "__main__":

    # me, inSHP = sys.argv

    shpList = ["Classification_Merge_N_PBS_3.shp", "Classification_Merge_N_PBS_6.shp","Classification_Merge_N_PBS_31.shp","Classification_Merge_N_PBS_32.shp","Classification_Merge_N_PBS_33.shp",
              "Classification_Merge_N_PBS_311.shp","Classification_Merge_N_PBS_312.shp","Classification_Merge_N_PBS_313.shp",
              "Classification_Merge_N_PBS_321.shp","Classification_Merge_N_PBS_322.shp","Classification_Merge_N_PBS_323.shp","Classification_Merge_N_PBS_324.shp",
              "Classification_Merge_N_PBS_331.shp","Classification_Merge_N_PBS_332.shp","Classification_Merge_N_PBS_333.shp","Classification_Merge_N_PBS_334.shp"

              ]

    for x in shpList:
        fullpath="E:\\simondr\\Documents\\Downloads\\Stats_by_class_v2\\"
        Compute_mode_shapefile(fullpath+x)
    for x in shpList:
        fullpath="E:\\simondr\\Documents\\Downloads\\Stats_by_class_v2\\"
        Compute_mode_shapefile(fullpath+x.replace("N_PBS","S_PBS"))