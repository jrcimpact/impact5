#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import random

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *

import ImageProcessing


gdal.TermProgress = gdal.TermProgress_nocb


def RunSampling(INraster, OUTname, num_points):

    try:
    # initialize output log
        log = LogStore.LogProcessing('Sampling', 'sampling')
        
        log.set_parameter('INraster',INraster )
        #log.set_parameter('Grid Height,Width', str(gridHeight)+","+str(gridWidth))
        log.set_parameter('% of random sample', str(num_points))

        ImageProcessing.validPathLengths(OUTname,None, 5, log)

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        outDriver = ogr.GetDriverByName('ESRI Shapefile')
        overwrite = True
        if os.path.exists(OUTname):
            if overwrite:
                try:
                    outDriver.DeleteDataSource(OUTname)
                except:
                    log.send_error('Cannot delete output file')
                    log.close()
                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')
                log.close()
                return True



        # ----------------------------------------------------
        # ------   GET EXTENT FROM RASTER   ------------------
        # ----------------------------------------------------
        raster_ds = gdal.Open(INraster)
        srcband = raster_ds.GetRasterBand(1)
        geo_transform = raster_ds.GetGeoTransform()
        prj = raster_ds.GetProjection()
        srs = osr.SpatialReference(wkt=prj)
        cols = raster_ds.RasterXSize
        rows = raster_ds.RasterYSize
        transform = raster_ds.GetGeoTransform()
        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = -transform[5]

        print(xOrigin)
        print(yOrigin)
        print(pixelWidth)
        print(pixelHeight)

        #drv = ogr.GetDriverByName("ESRI Shapefile")
        drv = ogr.GetDriverByName('Memory')
        tmp_layername = 'Dissolve'
        vector_ds = drv.CreateDataSource(tmp_layername)
        dst_layer = vector_ds.CreateLayer(tmp_layername, srs=srs)
        dst_layer.CreateField(ogr.FieldDefn('Class',ogr.OFTInteger))  # 2 FIX using ID from settings
        print('running_gdal')
        gdal.Polygonize(srcband, None, dst_layer, 0, [], callback=None)
        print('done')

        layer_diss = vector_ds.ExecuteSQL(
            "SELECT ST_Buffer(ST_Union(geometry),-0.001) from " + tmp_layername + " WHERE Class != 0", dialect="SQLITE")

        layer_class2 = vector_ds.ExecuteSQL("SELECT ST_Union(geometry) from " + tmp_layername + " WHERE Class = 11",
                                         dialect="SQLITE")
        layer2 = layer_class2.GetNextFeature()
        class2_geom = layer2.GetGeometryRef()

        # # get maximum poly
        # layer_class2 = vector_ds.ExecuteSQL("SELECT (geometry) from " + tmp_layername + " WHERE Class = 2",
        #                                     dialect="SQLITE")
        # maxClass2_items = layer_class2.GetFeatureCount()
        # print "Cl2 Feature count"
        # print maxClass2_items


        layer_class3 = vector_ds.ExecuteSQL("SELECT ST_Union(geometry) from " + tmp_layername + " WHERE Class = 10",
                                            dialect="SQLITE")
        # print "Cl3 Feature count"
        print(layer_class3.GetFeatureCount())
        layer3 = layer_class3.GetNextFeature()
        class3_geom = layer3.GetGeometryRef()
        # # get maximum poly
        # layer_class3 = vector_ds.ExecuteSQL("SELECT (geometry) from " + tmp_layername + " WHERE Class = 3",
        #                                     dialect="SQLITE")
        # maxClass3_items = layer_class3.GetFeatureCount()
        # print "Cl3 Feature count"
        # print maxClass3_items

        # ----------------------------------------------------
        # ------       Creating TMP shp     ------------------
        # ----------------------------------------------------

        #outDriver = ogr.GetDriverByName('ESRI Shapefile')
        outDriver = ogr.GetDriverByName('Memory')
        outDataSource = outDriver.CreateDataSource("TMP")
        outLayer = outDataSource.CreateLayer("TMP", geom_type=ogr.wkbPolygon,srs=srs)
        featureDefn = outLayer.GetLayerDefn()
        f1 = ogr.FieldDefn("PlotID", ogr.OFTInteger)
        outLayer.CreateField(f1)
        f2 = ogr.FieldDefn("SubID", ogr.OFTInteger)
        outLayer.CreateField(f2)
        f3 = ogr.FieldDefn("Random", ogr.OFTInteger)
        outLayer.CreateField(f3)
        f1 = None
        f2 = None
        f3 = None


        # ------------------------------------  DOMAIN BBBOX -----------------------------------------
        feature = layer_diss.GetNextFeature()
        feat_geom_diss = feature.GetGeometryRef()
        env = feat_geom_diss.GetEnvelope()
        xmin, ymin, xmax, ymax = env[0], env[2], env[1], env[3]
        print(xmin, ymin, xmax, ymax)

        # ------------------------------------   RUN class 2 -----------------------------------------
        counter = 0
        retry = 0
        list_raster_XY=[]

        # in teo only 1 feature
        if layer_diss.GetFeatureCount() == 1:
            # feature = layer_diss.GetNextFeature()
            # feat_geom_diss = feature.GetGeometryRef()
            # env = feat_geom_diss.GetEnvelope()
            # xmin, ymin, xmax, ymax = env[0], env[2], env[1], env[3]

            for i in range(num_points):
                print(i)
                while counter < num_points and retry < 1000:

                    point = ogr.Geometry(ogr.wkbPoint)
                    print(point)
                    print(random.uniform(xmin, xmax), random.uniform(ymin, ymax))
                    point.AddPoint(random.uniform(xmin, xmax),
                                   random.uniform(ymin, ymax))
                    print(point)
                    print('Ready')
                    # add geom from attribute or class X instead
                    if point.Within(class2_geom):
                        print('within')
                        refCol = int(((point.GetX() - xOrigin) / pixelWidth))
                        refRow = int(((yOrigin - point.GetY()) / pixelHeight))

                        if str(refCol)+"_"+str(refRow) in list_raster_XY:
                            retry +=1
                            print(retry)
                        else:

                            list_raster_XY.append(str(refCol)+"_"+str(refRow))
                            sub_counter = 0
                            poly = ogr.Geometry(ogr.wkbPolygon)
                            for cellX in range(- 1, 2):
                                for cellY in range(- 1, 2):
                                    ring = ogr.Geometry(ogr.wkbLinearRing)
                                    col = refCol + cellX
                                    row = refRow + cellY
                                    ring.AddPoint(xOrigin + col * pixelWidth,
                                                  yOrigin + row * pixelHeight * -1)
                                    ring.AddPoint(xOrigin + pixelWidth + col * pixelWidth,
                                                  yOrigin + row * pixelHeight * -1)
                                    ring.AddPoint(xOrigin + pixelWidth + col * pixelWidth,
                                                  yOrigin - pixelHeight + row * pixelHeight * -1)
                                    ring.AddPoint(xOrigin + col * pixelWidth,
                                                  yOrigin - pixelHeight + row * pixelHeight * -1)
                                    ring.AddPoint(xOrigin + col * pixelWidth,
                                                  yOrigin + row * pixelHeight * -1)

                                    poly.AddGeometry(ring)
                            outFeature = ogr.Feature(featureDefn)
                            outFeature.SetGeometry(poly)
                            outFeature.SetField("PlotID",counter+1)
                            outFeature.SetField("SubID", "2")
                            outFeature.SetField("Random",random.uniform(0, 10000))
                            outLayer.CreateFeature(outFeature)
                            outFeature.Destroy()
                            outFeature = None
                            #sub_counter += 1
                            counter += 1
                            poly.Destroy()
                            poly = None
                            print(counter)
            # outDataSource2.ExecuteSQL("REPACK " + outLayer2.GetName())
            # outDataSource2.Destroy()
            print('OUT')
            print("number of Retry Cl2",str(retry))
            print("number of selected point for Class 2", str(counter))


            # ------------------------------------   RUN class 3 -----------------------------------------

            # num_points = 10
            counter = 0
            retry = 0
            list_raster_XY = []

            # in teo only 1 feature
            if layer_diss.GetFeatureCount() == 1:
                # feature = layer_diss.GetNextFeature()
                # feat_geom_diss = feature.GetGeometryRef()
                # env = feat_geom_diss.GetEnvelope()
                # xmin, ymin, xmax, ymax = env[0], env[2], env[1], env[3]

                for i in range(num_points):
                    while counter < num_points and retry < 100:

                        point = ogr.Geometry(ogr.wkbPoint)
                        point.AddPoint(random.uniform(xmin, xmax),
                                       random.uniform(ymin, ymax))

                        # add geom from attribute or class X instead

                        if point.Within(class3_geom):

                            refCol = int(((point.GetX() - xOrigin) / pixelWidth))
                            refRow = int(((yOrigin - point.GetY()) / pixelHeight))
                            print(str(refCol) + "_" + str(refRow))
                            if str(refCol) + "_" + str(refRow) in list_raster_XY:
                                retry += 1
                            else:

                                list_raster_XY.append(str(refCol) + "_" + str(refRow))
                                sub_counter = 0
                                poly = ogr.Geometry(ogr.wkbPolygon)
                                for cellX in range(- 1, 2):
                                    for cellY in range(- 1, 2):
                                        ring = ogr.Geometry(ogr.wkbLinearRing)

                                        col = refCol + cellX
                                        row = refRow + cellY
                                        ring.AddPoint(xOrigin + col * pixelWidth,
                                                      yOrigin + row * pixelHeight * -1)
                                        ring.AddPoint(xOrigin + pixelWidth + col * pixelWidth,
                                                      yOrigin + row * pixelHeight * -1)
                                        ring.AddPoint(xOrigin + pixelWidth + col * pixelWidth,
                                                      yOrigin - pixelHeight + row * pixelHeight * -1)
                                        ring.AddPoint(xOrigin + col * pixelWidth,
                                                      yOrigin - pixelHeight + row * pixelHeight * -1)
                                        ring.AddPoint(xOrigin + col * pixelWidth,
                                                      yOrigin + row * pixelHeight * -1)

                                        poly.AddGeometry(ring)
                                outFeature = ogr.Feature(featureDefn)
                                outFeature.SetGeometry(poly)
                                outFeature.SetField("PlotID", num_points+counter + 1)
                                outFeature.SetField("SubID", "3")
                                outFeature.SetField("Random", random.uniform(0, 10000))
                                outLayer.CreateFeature(outFeature)
                                outFeature.Destroy()
                                outFeature = None
                                #sub_counter += 1
                                counter += 1
                                poly.Destroy()
                                poly = None
                                print(counter)

                print("number of Retry Cl2", str(retry))
                print("number of selected point for Class 2", str(counter))


            # ----------------------------------------------------
            # ------       Creating TMP shp     ------------------
            # ----------------------------------------------------

            outDriver = ogr.GetDriverByName('ESRI Shapefile')
            #outDriver = ogr.GetDriverByName('Memory')
            exportDataSource = outDriver.CreateDataSource(OUTname)
            exportLayer = exportDataSource.CreateLayer(os.path.basename(OUTname).replace('.shp',''), geom_type=ogr.wkbPolygon, srs=srs)
            featureDefn = exportLayer.GetLayerDefn()
            f1 = ogr.FieldDefn("PlotID", ogr.OFTInteger)
            exportLayer.CreateField(f1)
            f2 = ogr.FieldDefn("SubID", ogr.OFTInteger)
            exportLayer.CreateField(f2)


            # ------------------   shuffling -------------------------
            random_layer = outDataSource.ExecuteSQL("SELECT PlotID,SubID FROM " + outLayer.GetName() + " ORDER BY Random")


            feat = random_layer.GetNextFeature()
            while feat is not None:
                exportLayer.CreateFeature(feat)
                feat = random_layer.GetNextFeature()
            exportDataSource = None

        else:
            print("Error after dissolving classes")

    except Exception as e:
        print(str(e))

    log.close()

        

if __name__ == "__main__":

    overwrite=False
    OUTname=''
    stepX=''
    stepY=''
    rows=''
    columns=''
    EPSG=''

    randomNumber = 2

    # randomNumber = 10
    # INraster = 'E:/dev/IMPACT3/IMPACT/DATA/NBR/test.tif'
    # OUTname = INraster.replace('.tif','.shp')
    # RunSampling(INraster, OUTname, randomNumber)
    #
    INraster = r'D:\IMPACT4_Apache\DATA\Landsat_test_data\AOI_tm_226-068_28072000_class.tif'
    OUTname = INraster.replace('.tif','.shp')
    RunSampling(INraster, OUTname, randomNumber)


    #
