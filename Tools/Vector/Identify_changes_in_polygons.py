#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import numpy
import json
import time

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from Simonets_PROC_libs import *
import LogStore


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
#
# def getStats_from_image(image, band, fields, position):






def identify_changes(inSHP, imageT1, imageT2, imageT3):
    # initialize output log
    log = LogStore.LogProcessing('Identify_changes', 'Identify_changes')
    log.set_input_file(inSHP)
    log.set_input_file(imageT1)
    log.set_input_file(imageT2)
    log.set_input_file(imageT3)

    bands = [1]
    classChangeField = 'T1_class'
    changed_field = "Change"
    cluster_field = "T1_cluster"
    nbr_field = 'NBR_90'

    ESRIDriver = ogr.GetDriverByName('ESRI Shapefile')

    try:

        #-------------------------------------------------------------------
        # ------------------------- TEST OVERLAPS  -------------------------
        #-------------------------------------------------------------------
        for inIMG in [imageT1, imageT2, imageT3]:
            if not test_overlapping(inSHP,inIMG):
                log.send_error("No overlap between: "+inSHP+" "+inIMG)
            else:
                raster_ds = gdal.Open(inIMG, gdal.GA_ReadOnly)
                num_bands = raster_ds.RasterCount
                raster_ds = None

        #         test bands

        #-------------------------------------------------------------------
        # ------------------------- OPEN VECTOR  ---------------------------
        #-------------------------------------------------------------------
        try:
            shp_ds = ogr.Open(inSHP,gdal.GA_Update)
            layer = shp_ds.GetLayer(0)
            layerDefinition = layer.GetLayerDefn()
            SHPfield=[]
            update_fields=0

            TotalFeatures = layer.GetFeatureCount()

            targetSR = layer.GetSpatialRef()

            layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom
            print("Total feature count: " + str(TotalFeatures))
            i = 0.0

        except Exception as e:
            log.send_error("Error opening file, ensure is not in use by other applications")
            log.close()
            return


        for i in range(layerDefinition.GetFieldCount()):
            #print "------------------------------------"
            SHPfield.append(layerDefinition.GetFieldDefn(i).GetName())
            #print layerDefinition.GetFieldDefn(i).GetName()

        # general fields

        if changed_field not in SHPfield:
            print("ADD Change")
            # f1 = ogr.FieldDefn(changed, ogr.OFTReal)
            f1 = ogr.FieldDefn(changed_field, ogr.OFTInteger)
            layer.CreateField(f1)
            f1 = None
        print('OK')

        if classChangeField not in SHPfield:
            print("ADD " + str(classChangeField))
            # f1 = ogr.FieldDefn(classChangeField, ogr.OFTReal)
            f1 = ogr.FieldDefn(classChangeField, ogr.OFTInteger)
            layer.CreateField(f1)
            f1 = None
        print('OK')

        if cluster_field not in SHPfield:
            print("ADD Change")
            # f1 = ogr.FieldDefn(changed, ogr.OFTReal)
            f1 = ogr.FieldDefn(cluster_field, ogr.OFTInteger)
            layer.CreateField(f1)
            f1 = None
        print('OK')

        image_pos = 1
        mem_drv = ogr.GetDriverByName('Memory')
        driver = gdal.GetDriverByName('MEM')

        try:
            #-------------------------------------------------------------------
            # ------------------------- OPEN RASTER  ---------------------------
            #-------------------------------------------------------------------
            imageT1_ds = gdal.Open(imageT1, gdal.GA_ReadOnly)
            imageT2_ds = gdal.Open(imageT2, gdal.GA_ReadOnly)
            imageT3_ds = gdal.Open(imageT3, gdal.GA_ReadOnly)
            #
            # imageT1_b1 = imageT1_ds.GetRasterBand(1)
            # imageT1_b2 = imageT1_ds.GetRasterBand(2)
            # imageT1_b3 = imageT1_ds.GetRasterBand(3)
            #
            # imageT2_b1 = imageT2_ds.GetRasterBand(1)
            # imageT2_b2 = imageT2_ds.GetRasterBand(2)
            # imageT2_b3 = imageT2_ds.GetRasterBand(3)

            cols = imageT1_ds.RasterXSize
            rows = imageT1_ds.RasterYSize
            imgGeo = imageT1_ds.GetGeoTransform()
            sourceSR = osr.SpatialReference()
            sourceSR.ImportFromWkt(imageT1_ds.GetProjectionRef())

            pixelWidth = imgGeo[1]
            pixelHeight = imgGeo[5]

            ulx = imgGeo[0]
            uly = imgGeo[3]
            lrx = imgGeo[0] + cols * pixelWidth
            lry = imgGeo[3] + rows * pixelHeight

            print(ulx, lrx, uly, lry)

            wkt = "POLYGON ((" + str(ulx) + " " + str(uly) + "," + str(lrx) + " " + str(uly) + "," + str(
                lrx) + " " + str(lry) + "," + str(ulx) + " " + str(lry) + "," + str(ulx) + " " + str(uly) + "))"

            rasterExtentGeom = ogr.CreateGeometryFromWkt(wkt)
            if gdal.VersionInfo()[0] == '3':
                sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

            transformToshp = osr.CoordinateTransformation(sourceSR, targetSR)
            transformToimg = osr.CoordinateTransformation(targetSR, sourceSR)
            reproject = False
            print(sourceSR.ExportToProj4())
            print(targetSR.ExportToProj4())
            if sourceSR.ExportToProj4() != targetSR.ExportToProj4():
                reproject = True
                rasterExtentGeom.Transform(transformToshp)
                bbox = rasterExtentGeom.GetEnvelope()
                ulx = bbox[0]
                lrx = bbox[1]
                uly = bbox[3]
                lry = bbox[2]
            else:
                print("Same projection")


            # remove features outside image extent
            layer.ResetReading()
            layer.SetSpatialFilterRect(ulx, lry, lrx, uly)
            countTotal = layer.GetFeatureCount()
            missingFeatures = TotalFeatures - countTotal
            feat = layer.GetNextFeature()
            emptyFeatures = 0

            #-------------------------------------------------------------------
            # ---------------- LOOP FEATURES RASTER  ---------------------------
            #-------------------------------------------------------------------


            i = 0

            while feat is not None:
                gdal.TermProgress(i/countTotal)
                i += 1
                feat_geom = feat.GetGeometryRef().Clone()
                if reproject:
                    # Reprojecting
                    feat_geom.Transform(transformToimg)

                x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()

                # print "Repositioned "
                MinX = int(round((x_min - imgGeo[0]) / pixelWidth))
                MaxX = int(round((x_max - imgGeo[0]) / pixelWidth))

                MinY = int(round((y_max - imgGeo[3]) / pixelHeight))
                MaxY = int(round((y_min - imgGeo[3]) / pixelHeight))

                deltaX = MaxX - MinX
                deltaY = MaxY - MinY

                src_offset = [MinX, MinY, deltaX, deltaY]

                try:

                    if min(src_offset) < 0 or src_offset[0] > cols or src_offset[1] > rows:
                        #print 'Box is out'
                        raise Exception('Box is out')
                        continue
                    # calculate new geotransform of the feature subset
                    new_gt = (
                        (imgGeo[0] + (src_offset[0] * pixelWidth)),
                        pixelWidth,
                        0.0,
                        (imgGeo[3] + (src_offset[1] * pixelHeight)),
                        0.0,
                        pixelHeight
                    )
                    mem_ds = mem_drv.CreateDataSource('out')
                    mem_layer = mem_ds.CreateLayer('poly', sourceSR, geom_type=ogr.wkbPolygon)
                    newfeature = ogr.Feature(mem_layer.GetLayerDefn())
                    newfeature.SetGeometry(feat_geom)
                    mem_layer.CreateFeature(newfeature)

                    # Rasterize it
                    rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
                    rvds.SetGeoTransform(new_gt)
                    new_gt = None

                    gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])

                    newfeature = None
                    mem_layer = None
                    mem_ds = None
                    rv_array = rvds.ReadAsArray()

                    rvds = None

                    bands_stats = []

                    for band in bands:
                        band_T1 = imageT1_ds.GetRasterBand(band)
                        band_T2 = imageT2_ds.GetRasterBand(band)
                        band_T3 = imageT3_ds.GetRasterBand(band)

                        src_array_T1 = band_T1.ReadAsArray(src_offset[0], src_offset[1],src_offset[2], src_offset[3])
                        src_array_T2 = band_T2.ReadAsArray(src_offset[0], src_offset[1],src_offset[2], src_offset[3])
                        src_array_T3 = band_T3.ReadAsArray(src_offset[0], src_offset[1],src_offset[2], src_offset[3])

                        band_T1 = None
                        masked = None
                        masked_T1 = numpy.ma.masked_invalid(src_array_T1)
                        masked_T2 = numpy.ma.masked_invalid(src_array_T2)
                        masked_T3 = numpy.ma.masked_invalid(src_array_T3)

                        # if nodata is not None:
                        #     masked = numpy.ma.masked_values(masked, nodata)
                        #
                        # if nodata_list != '':
                        #     masked = numpy.ma.masked_values(masked, float(nodata_list))

                        # mask value in virtual raster outside shape boundaries (in case shp is not a box)
                        masked_T1 = numpy.ma.array(masked_T1, mask= rv_array == 0)
                        masked_T2 = numpy.ma.array(masked_T2, mask= rv_array == 0)
                        masked_T3 = numpy.ma.array(masked_T3, mask= rv_array == 0)

                        bands_stats.append([float(MYmode2D(masked_T1)),float(MYmode2D(masked_T2)),float(MYmode2D(masked_T3))])

                    # print(feat.GetField("ID"))
                    # print(bands_stats)

                    origClass = int(feat.GetField("T2_class"))
                    outClass = 0
                    changed = 0
                    # if abs(bands_stats[0] - bands_stats[1]) <



                    NBR = bands_stats[0][0]
                    PINO = bands_stats[0][1]
                    CLUSTER = bands_stats[0][2]

                    # if int(feat.GetField("ID")) == 85929:
                    #     print(PINO)
                    #     print(NBR)
                    #     print(PINO)
                    #     # print(int(feat.GetField("T3_class")))
                    #     # print(outClass == 0 and origClass == 10 and bands_stats[0][0] > -0.8 and bands_stats[0][0] < -0.58)

                    # Class Water
                    if PINO in [5,6,7,8]:
                        outClass = 80

                    if outClass == 0:
                        if PINO in [9,10,11,12,13,40,42] and (origClass == 10 or origClass == 95) and NBR > -0.8 and NBR <= -0.63:
                            outClass = 10

                    if outClass == 0:
                        if PINO in [9,10,11,12,13,40,42] and (origClass == 10 or origClass == 95) and NBR < -0.54:
                            outClass = 95

                    if outClass == 0:
                        if origClass == 80 and NBR < -0.8:
                            outClass = 80
                        if origClass == 90 and NBR < -0.54  and PINO > 13 and PINO < 21:
                            outClass = 90

                    #  Grassland
                    if outClass == 0:
                        if PINO in [30,31] and origClass == 30:
                            outClass = 30
                        if PINO >= 16 and PINO < 30 and origClass == 20:
                            outClass = 20



                    if outClass == 0:
                        if PINO in [30,31,35,41] and NBR > -0.1 and origClass == 60:
                            outClass = 60

                    if outClass == 0:
                        if PINO in [10,11,40,42]:
                            outClass = 10
                        if PINO in [9,12,13,14] and NBR < -0.54 and origClass == 95:
                            outClass = 95

                    if outClass == 95 and CLUSTER in [3,6,11,14,19,21]:
                            outClass = 10
                    if outClass == 10 and CLUSTER == 30:
                            outClass = 95
                    if outClass == 10 and CLUSTER == 8:
                        outClass = 95

                    if outClass == 0 and CLUSTER in [19,21]:
                        outClass = 10

                    if outClass != origClass and outClass != 0:
                        changed = 1  # class Built-up RED

                    if outClass == 0:
                        outClass = 100

                    # if outClass == 0 and origClass == 10 and NBR > -0.8 and NBR < -0.54:
                    #     # if int(feat.GetField("ID")) == 273259:
                    #     #     print('INSIDE')
                    #     outClass = 10
                    #
                    #
                    # if outClass == 0 and origClass == 95 and NBR > -0.65 and NBR < -0.54:
                    #     outClass = 95

                    # #     Water
                    # if outClass == 0 and NBR < -0.8:
                    #     outClass = 90
                    #
                    #
                    # if outClass == 0 and NBR > -0.8 and NBR < -0.65:
                    #     outClass = 50
                    # if outClass == 0 and NBR >= -0.65 and NBR < -0.54:
                    #     outClass = 20
                    #
                    #
                    #
                    #
                    # if (origClass != 10 or origClass != 95 ) and bands_stats[0][0] > -0.65 and bands_stats[0][0] < -0.6:
                    #     outClass = 95





                    # if outClass == 0 and origClass == 50 and bands_stats[0][0] < -0.8:
                    #     outClass = 80
                    # feat.SetField(classChangeField, int(feat.GetField("T1_class")))
                    feat.SetField(classChangeField, outClass)
                    feat.SetField(changed_field, changed)
                    feat.SetField(cluster_field, CLUSTER)
                    # feat.SetField(nbr_field, NBR)

                    # feat.SetField(classChangeField, bands_stats[0][0])



                    layer.SetFeature(feat)
                    feat.Destroy()
                    # if int(feat.GetField("ID")) == 273259:
                    #     return

                    feat = None
                    feat_geom = None
                    src_array_T1 = None
                    src_array_T2 = None
                    rv_array = None
                    masked_T1 = None
                    masked_T2 = None

                    feat = layer.GetNextFeature()

                except Exception as e:
                    print('ERROR')
                    print(str(e))

                    feat.SetField(classChangeField, 0)
                    feat.SetField(changed_field, 0)
                    layer.SetFeature(feat)
                    feat.Destroy()
                    feat = None
                    feat = layer.GetNextFeature()

                    feat_geom = None
                    rv_array = None
                    src_array_T1 = None
                    rv_array_T2 = None
                    masked_T1 = None
                    masked_T2 = None


        except Exception as e:
            print(str(e))
        # Close DataSources
        shp_ds.Destroy()
        log.send_message("Statistics complete: "+inSHP)



    except Exception as e:
        print(str(e))
        log.send_error("Statistics error: "+str(e).replace("'",''))

    if missingFeatures > 0:
        log.send_warning(str(missingFeatures) + " have no intersection: untouched")
    if emptyFeatures > 0 :
        log.send_warning(str(emptyFeatures)+ " have partial intersection: set to 0")

    log.close()


if __name__ == "__main__":

    me, inSHP, NBR, PINO, cluster = sys.argv
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    identify_changes(inSHP, NBR, PINO, cluster)