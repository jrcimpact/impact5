#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.



# interesting reading: https://www.geeksforgeeks.org/stratified-random-sampling-an-overview/?ref=ml_lbp

# todo: use fast lib --- https://gis.stackexchange.com/questions/469503/speed-up-reading-gpkg-as-geopandas-dataframe

# Import global libraries
import os
import sys
import random
import numpy
import shutil
import itertools
import json

import geopandas as gpd
import pandas as pd

import shapely.geometry
import pyproj

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import Settings
SETTINGS = Settings.load()

import ImageProcessing

gdal.TermProgress = gdal.TermProgress_nocb



tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "Dissolve")
shutil.rmtree(tmp_indir, ignore_errors=True)
if not os.path.exists(tmp_indir):
    os.makedirs(tmp_indir)



def apply_dissolve(INfile, classField):
    gdf = gpd.read_file(INfile)
    # total_area= gdf.to_crs(3857).area.sum()
    # print('Raw Area = '+str(total_area))

    gdf = gdf.dissolve(by=classField)


    shpBasename = os.path.basename(INfile)
    TMP_file = os.path.join(tmp_indir, shpBasename)
    print(TMP_file)
    # shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file.replace('.shp', '.prj'))
    gdf.to_file(TMP_file)

    return TMP_file










# def RunSampling(INfile, OUTname, overwrite, stratified, segmentation, TotSamples, minCoveragePercent, classField, exclusionList, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance):
def RunDissolve(INfile, OUTname, overwrite, classField, log):

    try:

        
        ImageProcessing.validPathLengths(OUTname,None, 5, log)

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        outDriver = ogr.GetDriverByName('ESRI Shapefile')

        if not os.path.exists(INfile):
            log.send_error('Cannot read input file --> '+ INfile)

            return False

        if os.path.exists(OUTname):
            if overwrite:
                try:
                    outDriver.DeleteDataSource(OUTname)
                except:
                    log.send_error('Cannot delete output file')

                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')

                return True



        # ####################################################
        #      selelct original poly from input shapefile
        # ####################################################

        if INfile.endswith('.shp'):

            out_shp_path = apply_dissolve(INfile, classField)

            shutil.move(out_shp_path, OUTname)
            shutil.move(out_shp_path.replace('.shp', '.dbf'), OUTname.replace('.shp', '.dbf'))
            shutil.move(out_shp_path.replace('.shp', '.prj'), OUTname.replace('.shp', '.prj'))
            shutil.move(out_shp_path.replace('.shp', '.shx'), OUTname.replace('.shp', '.shx'))
            shutil.move(out_shp_path.replace('.shp', '.cpg'), OUTname.replace('.shp', '.cpg'))
            log.send_message(' Dissolve completed')
            return True

        else:
            log.send_message(' Dissolve failed: input format not supported')
            log.close()
            return False





    except Exception as e:

        log.send_error("Error :")
        log.send_error(str(e))
        log.close()
    return
        

if __name__ == "__main__":

    me, INfile, suffix, overwrite, classField = sys.argv
    log = LogStore.LogProcessing('Dissolve', 'Dissolve')
    try:
        #INfile = json.loads(INfile)[0]

        if overwrite in ['No', False, 0, 'NO', 'N', 'False', 'false']:
            overwrite = False
        else:
            overwrite = True





        OUTname = INfile.replace('.shp', suffix +'.shp')



        log.set_parameter('IN File ', INfile)
        log.set_parameter('OUT File ', OUTname)
        log.set_parameter('overwrite ', overwrite)

        log.set_parameter('DBF Class Attribute ', classField)

        RunDissolve(INfile, OUTname, overwrite, classField, log)
        # print(getClassArea(INfile,'T1_class'))


    except Exception as e:
        print('Dissolve Error')
        log.send_error(str(e))

    finally:
        log.close()
    import sys
    sys.exit()
