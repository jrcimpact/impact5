#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries

# preliminary work 
#  ogr2ogr Artificial_N.shp Merge_North_v2.shp -of 'ESRI Shapefile' -dialect sqlite -sql "select T1_class from Merge_North_v2 where T1_class in ('1','11','111','112','12','121','122','123','124','125','126','127','128','129','13','131','132','133','14','141','142')"

#  ogr2ogr Artificial_N_buffer500.shp Artificial_N.shp -dialect sqlite -sql "select ST_Buffer(ST_Union(geometry),500) as geometry from Artificial_N"

#
# gdal_proximity.py Artificial_S_2m.tif Artificial_S_2m_distance100.tif -maxdist 100 -ot Byte
# Zonal stats with min of distance band
# call this script
# ogr2ogr -sql "select Error from Merge_South_v2 where Error != '0'" Error_S.shp Merge_South_v2.shp
# ogr2ogr -sql "select Changed from Merge_South_v2 where Changed != '0'" Changed_S.shp Merge_South_v2.shp
#  ....

import os
import sys
import numpy
import json
import time

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from Simonets_PROC_libs import *
import LogStore


gdal.TermProgress = gdal.TermProgress_nocb
numpy.seterr(divide='ignore', invalid='ignore')
#
# def getStats_from_image(image, band, fields, position):






def identify_errors(inSHP):
    # initialize output log
    log = LogStore.LogProcessing('Identify_errors', 'Identify_errors')
    log.set_input_file(inSHP)


    T1_class = 'T1_class'
    T2_class = 'T2_class'
    T1_cluster = "T1_cluster"
    


    ESRIDriver = ogr.GetDriverByName('ESRI Shapefile')

    try:
        #-------------------------------------------------------------------
        # ------------------------- OPEN VECTOR  ---------------------------
        #-------------------------------------------------------------------
        try:
            shp_ds = ogr.Open(inSHP,gdal.GA_Update)
            layer = shp_ds.GetLayer(0)
            #layerTmp = shp_ds.GetLayer(0)
            layerDefinition = layer.GetLayerDefn()
            SHPfield=[]
            update_fields=0

            TotalFeatures = layer.GetFeatureCount()

        except Exception as e:
            log.send_error("Error opening file, ensure is not in use by other applications")
            log.close()
            return


        

        try:

            #-------------------------------------------------------------------
            # ---------------- LOOP FEATURES RASTER  ---------------------------
            #-------------------------------------------------------------------


            i = 0
            feat = layer.GetNextFeature()
            while feat is not None:
                try:
                    gdal.TermProgress(i/TotalFeatures)
                    i += 1
                    

                    T1Class = feat.GetField(T1_class)
                    if T1Class is not None:
                        T1Class = int(T1Class)
                    outClass = T1Class
                    
                    T2Class = feat.GetField(T2_class)
                    if T2Class is not None:
                        T2Class= int(T2Class)
                    else:
                        T2Class=0
                    
                  


                    # ------      CLASS    -----------
                    # Tree cover to  Shrub
                    if T1Class == 0 and T2Class != 0:
                        feat.SetField(T1_class, T2Class)
                        layer.SetFeature(feat)
                        
                    

                    feat.Destroy()
                    feat = None

                    feat = layer.GetNextFeature()
                    
                except Exception as e:
                    print('ERROR')
                    print(str(e))

                    
                    feat.Destroy()
                    feat = None
                    feat = layer.GetNextFeature()




        except Exception as e:
            print(str(e))
        # Close DataSources
        shp_ds.Destroy()
        log.send_message("Statistics complete: "+inSHP)



    except Exception as e:
        print(str(e))
        log.send_error("Statistics error: "+str(e).replace("'",''))

    log.close()


if __name__ == "__main__":

    me, inSHP = sys.argv
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    identify_errors(inSHP)