#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import json, sys

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import ImageStore



#if __name__ == '__main__':
def run(shp_name):
    #print "Content-type: text/html;\n"
    weigths = { 0:0,
                111:1.45948941129823,
                112:0.839442038329478,
                121:0.735342047230148,
                122:0.668402277367533,
                123:0.120508700778715,
                124:0.616327168509091,
                131:3.27967033532724,
                132:0.525398189839287,
                133:7.6060573488347,
                141:0.898878263385266,
                142:1.62496625830491,
                211:0.904308807478751,
                221:1.0836796673214,
                222:1.17421313648498,
                223:1.43932292901504,
                242:0.965785723162024,
                311:1.68079589143607,
                312:0.739013269139438,
                321:0.57127378095823,
                322:0.145146867687557,
                323:1.30434540755851,
                331:0.0364714704143341,
                332:0.0893666457627519,
                333:0.471645256097598,
                411:0.0335480676348509,
                421:0.0591063107629274,
                422:0.137908660923132,
                511:0.0253096789594357,
                512:0.17863123598167,
                521:0.199118007405092,
                523:5.79102694220761,
                2121:0.159507761428605,
                2122:2.61466402185255,
                2124:0.413490607666393}

    try:

        shp_ds = ogr.Open(shp_name, 1)
        layer = shp_ds.GetLayer(0)
        layerName = layer.GetName()
        layerDefinition = layer.GetLayerDefn()
        feat = layer.GetNextFeature()

        while feat is not None:
            val = int(feat.GetField("T1_class"))
            print(val)
            print( weigths[val])
            feat.SetField("Weight", weigths[val])
            layer.SetFeature(feat)
            feat = layer.GetNextFeature()
        shp_ds.Destroy()
        shp_ds = None

        return 'OK'

    except Exception as e:
        print('Error: '+str(e))


if __name__ == "__main__":

    me, inSHP = sys.argv
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    run(inSHP)