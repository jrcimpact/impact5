#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries

import sys
import os
import time
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr


# Import local libraries
from __config__ import *
import LogStore
import IMPACT
import Settings
import ImageProcessing
import numpy
from Simonets_PROC_libs import test_overlapping


def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Vactor 2 Raster Conversion ')
    print('vectorConversion.py out_name, img_names, psize, attribute, attributeType, pct, overwrite')
    sys.exit(1)


def getArgs(args):
    if (len(sys.argv) < 8):
        print(len(sys.argv))
        time.sleep(5)
        usage()
    else:

        out_name = sys.argv[1]

        try:
            shp_names = json.loads(sys.argv[2])
        except:
            shp_names = sys.argv[2].split(',')

        try:
            psize = json.loads(sys.argv[3])[0]
        except:
            psize = sys.argv[3]

        attribute = sys.argv[4]
        attributeType = sys.argv[5]
        pct = sys.argv[6]
        overwrite = sys.argv[7]

        return out_name, shp_names, psize, attribute, attributeType, pct, overwrite


# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_VectorConversion(out_name, shp_names, psize, attribute, attributeType, pct, overwrite, nodata=0, ExternalLogFile=False):
    rasterTemplate = ''
    # if param contains a valid path then use this file as template
    if os.path.exists(psize):
        out_resolution = 'from input file ' + psize
        rasterTemplate = psize
    else:
        out_resolution = psize

    # initialize output log
    if not ExternalLogFile:
        log = LogStore.LogProcessing('Vector Conversion', 'VectorConversion')
    else:
        log = ExternalLogFile
    log.set_input_file(shp_names)
    log.set_parameter('Output suffix', out_name)
    log.set_parameter('Pixel Size', out_resolution)
    log.set_parameter('Attribute', attribute)
    log.set_parameter('Attribute Type', attributeType)
    log.set_parameter('PCT', pct)
    log.set_parameter('Overwripctte output', overwrite)
    log.set_parameter('Nodata', nodata)

    ImageProcessing.validPathLengths(shp_names, None, len(out_name) + 5, log)
    log.send_message("Running")

    SETTINGS = Settings.load()

    try:

        log.send_message('Processing')

        for shp_name in shp_names:
            try:
                log.send_message('Processing ' + shp_name)
                filename, file_extension = os.path.splitext(shp_name)
                outname = filename + "_" + out_name
                ext = '.tif'
                if outname.endswith(ext):
                    outname = outname.replace(ext, '')

                if (overwrite in ["True", "true", "Yes", "Y", "y", '1']):
                    if os.path.exists(outname + ext):
                        log.send_message('Deleting output file')
                        driver = gdal.GetDriverByName("GTiff")
                        driver.Delete(outname + ext)
                        if os.path.exists(outname + ext + '.aux.xml'):
                            os.remove(outname + ext + '.aux.xml')
                elif os.path.exists(outname + ext):
                    log.send_warning(outname + ext + ' <b> exists, change name or set Overwrite. </b>')
                    continue

                # Open the data source and read in the extent
                source_ds = ogr.Open(shp_name, 0)
                source_layer = source_ds.GetLayer()
                x_min, x_max, y_min, y_max = source_layer.GetExtent()
                # print(source_layer.GetExtent())

                # SHP specific attributes
                layer_definition = source_layer.GetLayerDefn()

                # print(attribute)
                attribute_typeName = 'null'
                # ---------------------------------------------
                # IMPORTANT : by setting utf-8 is not necessary to use decode()
                #             layer_definition.GetFieldDefn(i).GetName().decode('utf-8', 'ignore')
                # reload(sys)
                # sys.setdefaultencoding("utf-8")
                # ---------------------------------------------
                for i in range(layer_definition.GetFieldCount()):
                    if attribute == layer_definition.GetFieldDefn(
                            i).GetName():  # .decode('utf-8').encode('ascii', 'replace'):
                        attribute_typeID = layer_definition.GetFieldDefn(i).GetType()
                        attribute_typeName = layer_definition.GetFieldDefn(i).GetFieldTypeName(attribute_typeID)
                        # print('Attributes type ',attribute_typeID)
                        # print('Attributes name ',attribute_typeName)
                        if attributeType == "Auto":
                            if attribute_typeID == 0:
                                attributeType = gdal.GDT_Int32
                            elif attribute_typeID == 2:
                                attributeType = gdal.GDT_Float64
                            elif attribute_typeID == 4:
                                # string
                                attributeType = gdal.GDT_Float32
                                log.send_warning('Attribute ' + attribute + ' of type ' + str(
                                    attribute_typeName) + ' treated as Float32 bit')
                            # ---- attribute 6 is Float32

                            elif attribute_typeID == 12:
                                attributeType = gdal.GDT_Float64
                            else:
                                log.send_error('Attribute ' + attribute + ' of type ' + str(
                                    attribute_typeName) + ' is not supported.')
                                if not ExternalLogFile:
                                    log.close()
                                return

                        else:
                            attributeType = eval('gdal.' + str(attributeType))
                        print("Match @ pos = " + str(i))
                        break
                    else:
                        log.send_message(layer_definition.GetFieldDefn(i).GetName().replace("'", '') + ' ignored')

                log.send_message("Preparing raster file: " + outname)
                log.send_message("Input Field Type: " + str(attribute_typeName))
                log.send_message("Output Field Type: " + str(attributeType))

                if rasterTemplate == '':
                    # Create the destination data source
                    x_res = round(float((x_max - x_min) / float(out_resolution)))
                    y_res = round(float((y_max - y_min) / float(out_resolution)))
                    out_projection = source_layer.GetSpatialRef().ExportToWkt()
                    out_geotransform = (x_min, float(out_resolution), 0, y_max, 0, - float(out_resolution))

                else:
                    if not test_overlapping(shp_name, rasterTemplate):
                        log.send_error("No overlap between raster template and vector file")
                        continue

                    template_ds = gdal.Open(rasterTemplate)
                    x_res = template_ds.RasterXSize
                    y_res = template_ds.RasterYSize
                    out_projection = template_ds.GetProjectionRef()
                    out_geotransform = template_ds.GetGeoTransform()

                target_ds = gdal.GetDriverByName('GTiff').Create(outname, x_res, y_res, 1, attributeType,
                                                                 options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                # print('Output init')
                raster = numpy.zeros((y_res, x_res), dtype=numpy.uint)
                if nodata != 0:
                    raster += nodata
                target_ds.GetRasterBand(1).WriteArray(raster)
                target_ds.SetGeoTransform(out_geotransform)
                target_ds.SetProjection(out_projection)

                if pct != 'None':
                    # search PCT in VECTOR legend
                    c = gdal.ColorTable()
                    for item in SETTINGS['map_legends']['vector']:
                        if item['name'] == pct:
                            # unique val PCT works on Byte or Int32; otherwise use ranges
                            if attributeType == gdal.GDT_Byte or attributeType == gdal.GDT_UInt16:
                                for cid in item['classes']:
                                    id = int(cid[0])
                                    r = int(cid[2].split(',')[0])
                                    g = int(cid[2].split(',')[1])
                                    b = int(cid[2].split(',')[2])
                                    c.SetColorEntry(id, (r, g, b))
                                target_ds.GetRasterBand(1).SetColorTable(c)
                            else:
                                log.send_warning(
                                    "This Palette Color Table cannot be applied, cast data to Byte or UInt16")
                    # search PCT in RASTER legend
                    for item in SETTINGS['map_legends']['raster']:
                        if item['name'] == pct:
                            try:
                                for cid in item['classes']:
                                    minV = int(cid[0])
                                    maxV = int(cid[1])
                                    rMin = int(cid[3].split(',')[0])
                                    gMin = int(cid[3].split(',')[1])
                                    bMin = int(cid[3].split(',')[2])
                                    rMax = int(cid[4].split(',')[0])
                                    gMax = int(cid[4].split(',')[1])
                                    bMax = int(cid[4].split(',')[2])

                                    c.CreateColorRamp(minV, (rMin, gMin, bMin), maxV, (rMax, gMax, bMax))
                                target_ds.GetRasterBand(1).SetColorTable(c)
                            except Exception as e:
                                print(e)
                                log.send_warning(
                                    "This Palette Color Table cannot be applied, negative or decimal range values")

                # print('Rasterizing ....')
                # print(attribute)
                # options = gdal.RasterizeOptions(format='gtiff', where='"{}"="{}"'.format(field, uniqValues[ii]),
                #                                 xRes=newGt[1], yRes=newGt[5], outputSRS=projRef,
                #                                 width=ns, height=nl,
                #                                 layers=["disagTMP"],
                #                                 burnValues=[ii],
                #                                 outputType=gdal.GDT_UInt16
                #                                 )
                #
                # options = gdal.RasterizeOptions(format='gtiff', where='"{}"="{}"'.format(field, uniqValues[ii]),
                #                                 xRes=newGt[1], yRes=newGt[5], outputSRS=projRef,
                #                                 width=ns, height=nl,
                #                                 layers=["disagTMP"],
                #                                 burnValues=[ii],
                #                                 outputType=gdal.GDT_UInt16
                #                                 )
                # options = gdal.RasterizeOptions(
                #                                 "ATTRIBUTE=%s" % attribute,
                #                                 outputType=gdal.GDT_UInt16,
                #                                 width=x_res, height=y_res
                #
                #                                 )


                gdal.RasterizeLayer(target_ds, [1], source_layer, options=["outputType=%s" % attributeType,"ATTRIBUTE=%s" % attribute])
                # gdal.RasterizeLayer(target_ds, [1], source_layer, options=options)
                target_ds = None
                source_ds.Destroy()
                source_ds = None
                source_layer = None
                # print('Attribute before conversion')
                # print(attribute)
                #metadataparams = ' Rasterize Attribute: ' + str(attribute).decode(encoding = 'UTF-8', errors = 'ignore').encode(encoding = 'ascii', errors = 'replace')
                metadataparams = ' Rasterize Attribute: ' + (attribute.encode('ascii','replace')).decode()
                # print('after:')
                # print(metadataparams)
                gdal_edit = "gdal_edit"
                if (GLOBALS['OS'] == "unix"):
                    gdal_edit = "gdal_edit.py"
                res1 = os.system(
                    gdal_edit +' '+ outname + ' -mo "Impact_product_type=vector2raster" -mo "Impact_operation=' + metadataparams + '" -mo "Impact_version=' + IMPACT.get_version() + '"')
                os.rename(outname, outname + ext)
            # -- for loop
            except Exception as e:
                log.send_error("Unexpected error processing " + outname + ext + ": " + str(e).replace("'", ''))
                continue

    except Exception as e:
        log.send_error("Generic error: " + str(e).replace("'", ''))

    log.send_message("Completed.")
    if not ExternalLogFile:
        log.close()
    return outname + ext


if __name__ == "__main__":

    out_name, shp_names, psize, attribute, attributeType, pct, overwrite = getArgs(sys.argv)
    try:
        res = run_VectorConversion(out_name, shp_names, psize, attribute, attributeType, pct, overwrite, nodata=0, ExternalLogFile=False)
    except:
        print("Error " + str(res))
    time.sleep(1)
