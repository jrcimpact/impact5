#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import json, sys
import geopandas as gpd
# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries


#if __name__ == '__main__':
def run(shp_name, field, exclusionList):
    #print "Content-type: text/html;\n"



    try:

        print('Reading in file ....')
        gdf = gpd.read_file(shp_name, include_fields=[field])

        if len(exclusionList) > 0:
            print('Selection ....')
            if gdf[field].dtypes == 'float64':
                exclusionList = ([float(val) for val in exclusionList])
            if gdf[field].dtypes == 'int64':
                exclusionList = ([int(float(val)) for val in exclusionList])
            # #  ---  object type = String = default val in Impact

            gdf = gdf[~gdf[field].isin(set(exclusionList))]

        # Get unique values in the stratification column
        unique_strata = gdf[field].unique()

        # Calculate the total area of all strata
        # total_area = gdf.area.sum()
        print(unique_strata)
        # print(total_area)

        AreaTot = 0
        strataArray = {}
        for strata in unique_strata:
            gdf1 = gdf[gdf[field] == strata]
            print('Strata:',strata)
            area = gdf1.area.sum()
            AreaTot += area
            strataArray[strata] = area

        print('RESULT ------------- ')

        for strata in unique_strata:
            print('{},{},{}'.format(strata, strataArray[strata], strataArray[strata]/AreaTot*100. ))




        return 'OK'

    except Exception as e:
        print('Error: '+str(e))


if __name__ == "__main__":

    me, inSHP, Field , inPoints, pointField = sys.argv
    exclusionList = [0]
    
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    run(inSHP, Field, exclusionList)