#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.



# interesting reading: https://www.geeksforgeeks.org/stratified-random-sampling-an-overview/?ref=ml_lbp

# todo: use fast lib --- https://gis.stackexchange.com/questions/469503/speed-up-reading-gpkg-as-geopandas-dataframe

# Import global libraries
import os
import sys
import glob
import random
import numpy
import shutil
import itertools
import json

import geopandas as gpd
import pandas as pd

import shapely.geometry
import shapely
import pyproj

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import Settings
SETTINGS = Settings.load()

import ImageProcessing

gdal.TermProgress = gdal.TermProgress_nocb


tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "Parcel_Join")
shutil.rmtree(tmp_indir, ignore_errors=True)
if not os.path.exists(tmp_indir):
    os.makedirs(tmp_indir)


def round_coordinates(geom, ndigits=2):
    def _round_coords(x, y, z=None):
        x = round(x, ndigits)
        y = round(y, ndigits)

        if z is not None:
            z = round(x, ndigits)
            return (x, y, z)
        else:
            return (x, y)

    return shapely.ops.transform(_round_coords, geom)



# def RunSampling(INfile, OUTname, overwrite, stratified, segmentation, TotSamples, minCoveragePercent, classField, exclusionList, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance):
def Runparcel(files, OUTname, overwrite, tmp_indir, log):

    try:

        
        ImageProcessing.validPathLengths(OUTname,None, 5, log)

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        outDriver = ogr.GetDriverByName('ESRI Shapefile')


        if os.path.exists(OUTname):
            if overwrite:
                try:
                    outDriver.DeleteDataSource(OUTname)
                except:
                    log.send_error('Cannot delete output file')

                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')

                return True



        # ####################################################
        #      selelct original poly from input shapefile
        # ####################################################
        tmfiles=[]
        for file in files:
            yy = file.split('_')[1][2:4]
            print(yy)
            name = os.path.basename(file).replace('.shp', '')
            # SQL = "select geometry, Afgkode as k" + str(yy) + ", Journalnr as j" + str(yy) +" from "+str(name) +"  where GeometryType(geometry) in ('POLYGON','MULTIPOLIGON')"
            SQL = "select geometry, Afgkode as k" + str(yy) + ", Journalnr as j" + str(yy) +" from "+str(name)

            tmpfile = os.path.join(tmp_indir,name+"_sel.shp")
            tmfiles.append(tmpfile)
            if overwrite and os.path.exists(tmpfile):
                outDriver.DeleteDataSource(tmpfile)
            if not os.path.exists(tmpfile):
                os.system('ogr2ogr -f "ESRI Shapefile"  ' + tmpfile + ' ' + file + ' -dialect sqlite -sql "' + SQL +'"') #-simplify 1


        mergetmp = os.path.join(tmp_indir, "MergeOGR.shp")
        if overwrite and os.path.exists(mergetmp):
            outDriver.DeleteDataSource(mergetmp)
        if not os.path.exists(mergetmp):
            os.system('ogrmerge -f "ESRI Shapefile" -overwrite_ds -single -field_strategy Union -o '+mergetmp+' '+tmp_indir+'/*.shp')


        # ------------------------------------------------------------------------------------------------------------------------------------

        # TOLERANCE = 1
        # MinSQmt = 30
        #
        #
        # file1 = tmfiles[0]
        # print(file1)
        # res_join = gpd.read_file(file1)
        # res_join = res_join.explode()
        # print(res_join.type.unique())
        # res_join.geometry = res_join.geometry.buffer(-TOLERANCE)
        # res_join.geometry = res_join.geometry.apply(round_coordinates, ndigits=0)
        # # res_union.geometry = res_union.geometry.simplify(tolerance=TOLERANCE, preserve_topology=True)
        # res_join.geometry = res_join.geometry.buffer(TOLERANCE)
        #
        # res_join = res_join.loc[res_join['geometry'].is_valid]
        # res_join = res_join[res_join.geometry.type != 'Point']
        # res_join = res_join[res_join.geometry.type != 'MultiPoint']
        # res_join = res_join[res_join.geometry.type != 'LineString']
        # res_join = res_join[res_join.geometry.type != 'MultiLineString']
        # # res_union = res_union[res_union.geometry.type != 'GeometryCollection' ]
        # print(res_join.type.unique())
        #
        #
        #
        # for file in tmfiles[1:]:
        #     print(file)
        #     df = gpd.read_file(file)
        #     print(df.type.unique())
        #     # df.geometry = shapely.set_precision(df.geometry.array, grid_size=0.1)
        #     # df = df.explode()
        #     df.geometry = df.geometry.buffer(-TOLERANCE)
        #     df.geometry = df.geometry.apply(round_coordinates, ndigits=0)
        #     # res_union.geometry = res_union.geometry.simplify(tolerance=TOLERANCE, preserve_topology=True)
        #     df.geometry = df.geometry.buffer(TOLERANCE)
        #
        #     df = df.loc[df['geometry'].is_valid]
        #     df = df[df.geometry.type != 'Point']
        #     df = df[df.geometry.type != 'MultiPoint']
        #     df = df[df.geometry.type != 'LineString']
        #     df = df[df.geometry.type != 'MultiLineString']
        #     # res_union = res_union[res_union.geometry.type != 'GeometryCollection' ]
        #     print(df.type.unique())
        #
        #     res_join = gpd.sjoin(res_join, df, how='inner', op='overlaps')
        #     cols_to_drop = res_join.columns[res_join.columns.str.contains('index_left')]
        #     res_join.drop(cols_to_drop, axis=1, inplace=True)
        #     cols_to_drop = res_join.columns[res_join.columns.str.contains('index_right')]
        #     res_join.drop(cols_to_drop, axis=1, inplace=True)
        #
        #
        # mergetmp = os.path.join(tmp_indir, "join_Ref.shp")
        # res_join.to_file(mergetmp)
        #
        # sys.exit()
        # ------------------------------------------------------------------------------------------------------------------------------------




        # file1 = tmfiles.pop(0)
        TOLERANCE = 6
        MinSQmt = 1
        applyFilter = False

        file1 = tmfiles[0]

        print('--------')
        print(file1)
        res_union = gpd.read_file(file1)
        res_union = res_union.explode()
        print(res_union.type.unique())
        res_union.geometry = res_union.geometry.buffer(-TOLERANCE)
        res_union.geometry = res_union.geometry.apply(round_coordinates, ndigits=2)
        res_union.geometry = res_union.geometry.buffer(TOLERANCE)

        print(res_union.type.unique())
        if applyFilter:
            res_union = res_union.loc[res_union['geometry'].is_valid]
            res_union = res_union[res_union.geometry.type != 'Point' ]
            res_union = res_union[res_union.geometry.type != 'MultiPoint' ]
            res_union = res_union[res_union.geometry.type != 'LineString' ]
            res_union = res_union[res_union.geometry.type != 'MultiLineString' ]
            # res_union = res_union[res_union.geometry.type != 'GeometryCollection' ]
            print(res_union.type.unique())



        for file in tmfiles[1:]:
            print(file)
            df = gpd.read_file(file)
            print(df.type.unique())
            # df.geometry = shapely.set_precision(df.geometry.array, grid_size=0.1)
            # df = df.explode()
            df.geometry = df.geometry.buffer(TOLERANCE)
            df.geometry = df.geometry.apply(round_coordinates, ndigits=2)

            res_union = gpd.overlay(res_union, df, how='difference', keep_geom_type=True)
            print('Difference')


            res_union.geometry = res_union.geometry.apply(round_coordinates, ndigits=2)
            if applyFilter:
                res_union = res_union.loc[res_union['geometry'].is_valid]
                res_union = res_union[res_union.geometry.type != 'Point']
                res_union = res_union[res_union.geometry.type != 'MultiPoint']
                res_union = res_union[res_union.geometry.type != 'LineString']
                res_union = res_union[res_union.geometry.type != 'MultiLineString']
            # res_union = res_union[res_union.geometry.type != 'GeometryCollection']

            res_union["area"] = res_union['geometry'].area
            res_union = res_union[res_union.area > MinSQmt]



            print('difference ... done')

            df = gpd.read_file(file)


            df = df.explode()
            df.geometry = df.geometry.buffer(-TOLERANCE)
            df.geometry = df.geometry.apply(round_coordinates, ndigits=2)

            df = df.loc[df['geometry'].is_valid]
            df = df[df.geometry.type != 'Point' ]
            df = df[df.geometry.type != 'MultiPoint' ]
            df = df[df.geometry.type != 'LineString' ]
            df = df[df.geometry.type != 'MultiLineString' ]
            # df = df[df.geometry.type != 'GeometryCollection' ]
            df.geometry = df.geometry.buffer(TOLERANCE)
            res_union = gpd.overlay(res_union, df, how='union', keep_geom_type=True)
            # res_union = gpd.overlay(res_union, intersection, how='union', keep_geom_type=True)

            res_union = res_union.explode()
            print('Union ... done')
            if applyFilter:
                res_union = res_union.loc[res_union['geometry'].is_valid]
                res_union = res_union[res_union.geometry.type != 'Point']
                res_union = res_union[res_union.geometry.type != 'MultiPoint']
                res_union = res_union[res_union.geometry.type != 'LineString']
                res_union = res_union[res_union.geometry.type != 'MultiLineString']
            # res_union = res_union[res_union.geometry.type != 'GeometryCollection']

            res_union["area"] = res_union['geometry'].area
            res_union = res_union[res_union.area > MinSQmt]



        # mergetmp = os.path.join(tmp_indir, "MergePanda11.shp")
        # res_union.to_file(mergetmp)


        # Create a boolean array indicating which columns contain either "Email|"Phone"
        # print(res_union.columns)
        # cols_to_drop = res_union.columns[res_union.columns.str.contains('level_')]
        # # Drop the columns containing ****
        # res_union.drop(cols_to_drop, axis=1, inplace=True)

        res_union.columns = df.columns.droplevel()

        # res_union = res_union[res_union.area > MinSQmt]
        res_union.to_file(OUTname)
        print('OUT file: ', OUTname)



    except Exception as e:

        log.send_error("Error :")
        log.send_error(str(e))
        log.close()
    return
        

if __name__ == "__main__":

    me, INfileEXP, OUTname, overwrite = sys.argv
    log = LogStore.LogProcessing('Parcel', 'Parcel')
    try:
        #INfile = json.loads(INfile)[0]

        if overwrite in ['No', False, 0, 'NO', 'N', 'False', 'false']:
            overwrite = False
        else:
            overwrite = True


        log.set_parameter('IN File ', INfileEXP)
        log.set_parameter('OUT File ', OUTname)
        log.set_parameter('overwrite ', overwrite)

        # log.set_parameter('DBF Class Attribute ', Fields)
        # print(Fields)
        # Fields=Fields.split(',')


        files = glob.glob(INfileEXP)


        if len(files)== 0:
            print('No files')

        else:

            for file in files:
                print(file)
            dirname = os.path.dirname(files[0])
            OUTname = os.path.join(dirname,'OUT',os.path.basename(OUTname))
            if not OUTname.endswith('.shp'):
                OUTname += '.shp'

            if not os.path.exists(os.path.join(dirname,'OUT')):
                os.makedirs(os.path.join(dirname,'OUT'))


            Runparcel(files, OUTname, overwrite, tmp_indir, log)


    except Exception as e:
        print('Parcel  Error')
        log.send_error(str(e))

    finally:
        log.close()
    import sys
    sys.exit()






