#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
from math import ceil, floor
import json
import random

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import ImageProcessing
import ImageStore

gdal.TermProgress = gdal.TermProgress_nocb


def RunFishnet(outSHP, ULX, ULY, LRX, LRY, gridHeight, gridWidth, stepX, stepY, rows, columns, EPSG, template,
               overwrite, randomVal):
    try:
        # initialize output log
        log = LogStore.LogProcessing('Fishnet', 'fishnet')
        if template != "":
            log.set_input_file(template)
        else:
            log.set_parameter('ULX,ULY,LRX,LRY', str(ULX) + "," + str(ULY) + "," + str(LRX) + "," + str(LRY))

        log.set_parameter('Output', outSHP)
        log.set_parameter('Projection', EPSG)
        log.set_parameter('Grid Height,Width', str(gridHeight) + "," + str(gridWidth))
        log.set_parameter('Step X,Y', str(stepX) + "," + str(stepY))
        log.set_parameter('Rows,Columns', str(rows) + "," + str(columns))
        log.set_parameter('% of random sample', str(randomVal))

        ImageProcessing.validPathLengths(outSHP, None, 5, log)

        outDriver = ogr.GetDriverByName('ESRI Shapefile')

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        if os.path.exists(outSHP):
            if overwrite:
                try:
                    outDriver.DeleteDataSource(outSHP)
                except:
                    log.send_error('Cannot delete output file')
                    log.close()
                    return False
            else:
                log.send_warning(outSHP + ' <b> Already Exists </b>')
                log.close()
                return True

        # ----------------------------------------------------
        # ------ GET EXTENT FROM TEMPLATE   ------------------
        # ----------------------------------------------------

        sourceSR = osr.SpatialReference()

        if template != "" and os.path.exists(template):

            try:

                info = ImageStore.get_by_path(template)

                ULX = info['extent']['W']
                ULY = info['extent']['N']
                LRX = info['extent']['E']
                LRY = info['extent']['S']


                sourceSR.ImportFromProj4(info['PROJ4'])

                print('template sourceSR')
                print(info['PROJ4'])
                print(info['EPSG'])
                print('OUT')
                print(sourceSR)

            except Exception as e:
                print(e)
                log.send_error('Wrong template: ',e)
                log.close()

                return False
        # ----------------------------------------------------
        # ------ GET EPSG FROM USER INPUT ------------------
        # ----------------------------------------------------
        else:
            try:
                sourceSR.SetFromUserInput(EPSG)
                print(sourceSR.GetName())
            except Exception as e:
                print(e)
                log.send_error('Wrong EPSG code: ' + str(EPSG))
                log.close()
                return False
        print(ULX)
        print(ULY)
        if str(ULX) == "" or str(ULY) == "":
            log.send_error('Wrong coordinates: ULX / ULY')
            log.close()
            return False

        # ----------------------------------------------------
        # ------ GET MAX EXTENT FROM USER INPUT   ------------
        # ------            OR                    ------------
        # ------ CALCULATE FROM ROW/COLUMN/STEP   ------------
        # ----------------------------------------------------
        if (str(LRX) == "" or str(LRY) == "") and (str(rows) == "" or str(columns) == ""):
            log.send_error('Wrong coordinates')
            log.close()
            return False

        if (gridWidth == "" or gridHeight == "") and (str(rows) == "" or str(columns) == ""):
            log.send_error('Wrong coordinates')
            log.close()
            return False

        setENDcoordinates = False
        ULX = float(ULX)
        ULY = float(ULY)

        try:
            print("LRXY")
            LRX = float(LRX)
            LRY = float(LRY)
            gridWidth = float(gridWidth)
            gridHeight = float(gridHeight)
            MAXrows = ceil(abs(ULY - LRY) / gridHeight)
            MAXcols = ceil(abs(LRX - ULX) / gridWidth)

        except:
            print("LRXY Failure")
            MAXrows = 1000000 * 1000000
            MAXcols = 1000000 * 1000000
            setENDcoordinates = True

        try:  # in case of empty string
            columns = float(columns)
            rows = float(rows)
        except:
            columns = 1000000 * 1000000
            rows = 1000000 * 1000000

        try:
            float(stepX)
            float(stepY)
        except:
            log.send_error('Wrong Step X/Y value ' + stepX)
            log.close()
            return False

        # calculate W and H using row/col and max extent
        if (gridWidth == "" or gridHeight == ""):
            gridWidth = float(abs(LRX - ULX) / columns)
            gridHeight = float(abs(ULY - LRY) / rows)
        else:
            gridWidth = float(gridWidth)
            gridHeight = float(gridHeight)

        # start grid cell envelope
        ringXleftOrigin = float(ULX)
        ringXrightOrigin = float(ULX + gridWidth)
        ringYtopOrigin = float(ULY)
        ringYbottomOrigin = float(ULY - gridHeight)

        # ----------------------------------------------------
        # ------------- INIT OUTPUT --------------------------
        # ----------------------------------------------------

        sourceSR.MorphToESRI()
        file = open(outSHP.replace('.shp', '.prj'), 'w')
        file.write(sourceSR.ExportToWkt())
        file.close()
        print(outSHP)

        outDataSource = outDriver.CreateDataSource(outSHP)
        outLayer = outDataSource.CreateLayer(outSHP, geom_type=ogr.wkbPolygon)
        featureDefn = outLayer.GetLayerDefn()
        f1 = ogr.FieldDefn("ID", ogr.OFTInteger)
        outLayer.CreateField(f1)
        f1 = None

        # ----------------------------------------------------
        # ------------- LOOP FEATURES ------------------------
        # ----------------------------------------------------
        log.send_message("Input and Derived Parameters ")
        log.send_message("----------------------------------------------------------")
        log.send_message("MAXrows = " + str(MAXrows))
        log.send_message("MAXcols = " + str(MAXcols))
        log.send_message("cols = " + str(columns))
        log.send_message("rows = " + str(rows))
        log.send_message("ULX = " + str(ULX))
        log.send_message("ULY = " + str(ULY))
        log.send_message("LRX = " + str(LRX))
        log.send_message("LRY = " + str(LRY))
        log.send_message("ringXrightOrigin = " + str(ringXrightOrigin))
        log.send_message("ringYbottomOrigin = " + str(ringYbottomOrigin))
        log.send_message("gridWidth = " + str(gridWidth))
        log.send_message("gridHeight = " + str(gridHeight))
        log.send_message("Steps:" + str(stepX))
        log.send_message("Steps:" + str(stepY))
        log.send_message("----------------------------------------------------------")

        featureID = 0
        # create grid cells
        countcols = 0

        if str(LRX) == '':
            LRX = ringXrightOrigin

        print(setENDcoordinates)
        print("#######################")

        while (countcols < MAXcols) and (countcols < columns) and (ringXrightOrigin - gridWidth <= LRX):
            gdal.TermProgress(countcols / MAXcols)
            countcols += 1
            # reset envelope for rows
            ringYtop = ringYtopOrigin
            ringYbottom = ringYbottomOrigin
            countrows = 0

            if setENDcoordinates:
                LRY = ringYbottomOrigin
                end_test = ringYbottom <= LRY
            else:
                end_test = ringYbottom >= LRY

            while (countrows < MAXrows) and (countrows < rows) and (end_test):
                countrows += 1
                ring = ogr.Geometry(ogr.wkbLinearRing)
                ring.AddPoint(ringXleftOrigin, ringYtop)
                ring.AddPoint(ringXrightOrigin, ringYtop)
                ring.AddPoint(ringXrightOrigin, ringYbottom)
                ring.AddPoint(ringXleftOrigin, ringYbottom)
                ring.AddPoint(ringXleftOrigin, ringYtop)
                poly = ogr.Geometry(ogr.wkbPolygon)
                poly.AddGeometry(ring)

                # add new geom to layer
                outFeature = ogr.Feature(featureDefn)
                outFeature.SetGeometry(poly)
                outFeature.SetField("ID", featureID)
                outLayer.CreateFeature(outFeature)
                outFeature.Destroy()

                # new envelope for next poly
                ringYtop = float(ringYtop) - float(gridHeight) - float(stepY)
                ringYbottom = float(ringYbottom) - float(gridHeight) - float(stepY)
                featureID += 1

                if setENDcoordinates:
                    LRY = ringYbottomOrigin
                    end_test = ringYbottom <= LRY
                else:
                    end_test = ringYbottom >= LRY

            # new envelope for next poly
            ringXleftOrigin = float(ringXleftOrigin) + float(gridWidth) + float(stepX)
            ringXrightOrigin = float(ringXrightOrigin) + float(gridWidth) + float(stepX)
            if setENDcoordinates:
                LRX = ringXrightOrigin

        # Close DataSources

        # ---- apply random selection if necessary ---
        tot_feat = outLayer.GetFeatureCount()
        randomVal = float(randomVal)
        sel_feat = int(floor(tot_feat * randomVal / 100.0))
        if sel_feat <= 0:
            sel_feat = 1
        if sel_feat > tot_feat:
            sel_feat = tot_feat
        log.send_message("Selecting " + str(sel_feat) + " random samples out of  " + str(tot_feat))
        del_feat = tot_feat - sel_feat
        log.send_message("Deleting " + str(del_feat))

        if randomVal < 100:
            for featId in random.sample(range(tot_feat), del_feat):
                outLayer.DeleteFeature(featId)

        outDataSource.ExecuteSQL("REPACK " + outLayer.GetName())
        outDataSource.Destroy()
        log.send_message("Fishnet complete: " + outSHP)
        log.close()
        return True

    except Exception as e:
        log.send_error("Fishnet error: " + str(e))
        log.close()
        return False


#
if __name__ == "__main__":

    # outSHP= name of out file ; mandatory
    # ULX= upper left (longitude) coordinate; mandatory if template is not provided
    # ULY= upper left (latitude) coordinate; mandatory if template is not provided
    # LRY= lower right (latitude) coordinate; mandatory if rows and columns or template are not provided
    # template= input file .tif of .shp to be used as extent and projection reference
    # LRX= lower right (longitude) coordinate; mandatory if rows and columns or template are not provided
    # gridHeight= Height of cell
    # gridWidth = Width of cell
    # stepX= longitude gap between 2 cells
    # stepY= latitude gap between 2 cells
    # rows= number of rows ; mandatory if LRX LRY are not provided
    # columns= number of columns ; mandatory if LRX LRY are not provided
    # EPSG= projection of output file and input coordinates
    # overwrite= overwrite output file

    overwrite = False
    OUTname = ''
    stepX = ''
    stepY = ''
    rows = ''
    columns = ''
    EPSG = ''

    me, overwrite, template, ULY, ULX, LRX, LRY, EPSG, gridWidth, gridHeight, columns, rows, stepX, stepY, randomVal, OUTname = sys.argv

    try:
        template = json.loads(template)[0]
    except:
        template = ""

    if not OUTname.endswith('.shp'):
        OUTname = OUTname + ".shp"

    overwrite = True if overwrite == "Yes" else False
    if stepX == "":
        stepX = 0
    if stepY == "":
        stepY = 0

    # ULX=0
    # ULY=0
    # LRX=10
    # LRY=-10
    # stepX=1
    # stepY=1
    # gridWidth=''
    # gridHeight=''
    # rows=5
    # columns=5
    # EPSG=4326
    # overwrite=True

    # OUTname='E:\\IMPACT\\ONLINE\\IMPACT_JRC-Toolbox\\DATA\\VECTOR_data\\User_Features\\Test_afr.shp'
    RunFishnet(OUTname, ULX, ULY, LRX, LRY, gridHeight, gridWidth, stepX, stepY, rows, columns, EPSG, template,
               overwrite, randomVal)
    # RunFishnet(OUTname,ULX,ULY,LRX,LRY,gridHeight,gridWidth,stepX,stepY,rows,columns,EPSG,template,overwrite)
