#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import json, sys

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import ImageStore
import Settings


#if __name__ == '__main__':
def run(shp_name):
    #print "Content-type: text/html;\n"

    SETTINGS = Settings.load()

    ID = str(SETTINGS['vector_editing']['attributes']['ID'])
   

    try:

        shp_ds = ogr.Open(shp_name, 1)
        layer = shp_ds.GetLayer(0)
        layerName = layer.GetName()
        layerDefinition = layer.GetLayerDefn()
        feat = layer.GetNextFeature()

        while feat is not None:
            feat.SetField(ID, feat.GetFID())
            layer.SetFeature(feat)
            feat = layer.GetNextFeature()
        shp_ds.Destroy()
        shp_ds = None

        ImageStore.update_file_statistics(shp_name)

        return 'OK'

    except Exception as e:
        print('Error: '+str(e))


if __name__ == "__main__":

    me, inSHP = sys.argv
    # identify_changes(json.loads(inSHP)[0], json.loads(inIMG), nodata_list, statsList, statsBands)
    run(inSHP)