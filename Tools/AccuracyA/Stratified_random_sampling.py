#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.



# interesting reading: https://www.geeksforgeeks.org/stratified-random-sampling-an-overview/?ref=ml_lbp
# todo: when minSamplePerClass is used, adjust the weights or rebalance the tot number
# todo: use fast lib --- https://gis.stackexchange.com/questions/469503/speed-up-reading-gpkg-as-geopandas-dataframe

# Import global libraries
import os
import sys
import random
import numpy
import shutil
import itertools
import json

import geopandas as gpd
import pandas as pd
from sklearn.model_selection import train_test_split

import shapely.geometry
import pyproj

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import Settings
SETTINGS = Settings.load()

import ImageProcessing

gdal.TermProgress = gdal.TermProgress_nocb



tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "Sampling")
shutil.rmtree(tmp_indir, ignore_errors=True)
if not os.path.exists(tmp_indir):
    os.makedirs(tmp_indir)


def return_points_from_geom(geom, numPoints, inEPSG, tollerance, bufferDistance=0):
    # find area bounds
    bounds = geom.bounds
    xmin, ymin, xmax, ymax = bounds

    xext = xmax*1. - xmin
    yext = ymax*1. - ymin

    points = []

    listTxt = []
    tryLimit = 0
    print('random selection ...')
    while len(points) < numPoints and tryLimit < 100:
        # generate a random x and y
        x = round(xmin + random.random() * xext, tollerance)    # 0.000001 = 10cm in LL
        y = round(ymin + random.random() * yext, tollerance)
        # x = xmin + random.random() * xext
        # y = ymin + random.random() * yext
        if str(x)+'_'+str(y) in listTxt:
            tryLimit+=1
            print('------- duplicate -------- skip ----- try: ',tryLimit )
            continue


        p = shapely.geometry.Point(x, y)

        if geom.contains(p):  # check if point is inside geometry
            listTxt.append(str(x)+'_'+str(y))
            if bufferDistance > 0:

                transformer = pyproj.Transformer.from_crs(inEPSG, 3857, always_xy=True)
                p = shapely.ops.transform(transformer.transform, p)
                p = p.buffer(bufferDistance, cap_style = 3)

                transformer1 = pyproj.Transformer.from_crs(3857, inEPSG, always_xy=True)
                p = shapely.ops.transform(transformer1.transform, p)
                # points.append(p)


            points.append(p)
            gdal.TermProgress(len(points) / numPoints)
        else:
            # print('out of geom')
            print('o')


    return points



def getRandomPoint_noStrata_AOI(INfile, inEPSG, TotSamples, bufferDistance, classField, exclusionList):
    print('Reading in file ....')
    gdf = gpd.read_file(INfile, include_fields=[classField])

    if len(exclusionList) > 0:
        print('Selection ....')
        if gdf[classField].dtypes == 'float64':
            exclusionList = ([float(val) for val in exclusionList])
        if gdf[classField].dtypes == 'int64':
            exclusionList = ([int(float(val)) for val in exclusionList])
        # #  ---  object type = String = default val in Impact

        gdf = gdf[~gdf[classField].isin(set(exclusionList))]

    import time
    start = time.time()
    print('Prearing Index')
    if classField not in ['-','']:
        gdf = gdf.set_index()


    # print(gdf.total_bounds)
    print('Prearing geom union')

    aoi_geom = gdf.unary_union
    # much faster but what id the geom is smaller than BBox
    # aoi_geom = gdf.total_bounds

    print("Union time: " + str(time.time() - start))
    print('Generating points ...')
    #  tollerance = number of decimal position when generating random point
    #  if LL 5 decimals and 0 in MT proj - better to keep 1mt min between points
    # todo: add spacing between points if necessary
    tollerance = 0
    if inEPSG == "EPSG:4326":
        tollerance = 5

    print('Processing ...')
    points = return_points_from_geom(aoi_geom, TotSamples, inEPSG, tollerance, bufferDistance)

    # use geopandas lib to get points per geometry
    # BUT available from v1.0 and requires new python + libs
    # points = gdf.sample_points(TotSamples)
    # print(points)


    d = {
        str(SETTINGS['vector_editing']['attributes']['ID']): numpy.arange(0, len(points)),
        str(SETTINGS['vector_editing']['attributes']['class_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['cluster_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['comments']): '',
        'RawVal': numpy.zeros(len(points)),
        'Weights': 1,  #./len(points),
        'geometry': points}
    gs = gpd.GeoDataFrame(d)
    gs = gs.set_index(str(SETTINGS['vector_editing']['attributes']['ID']))
    # gs = gs.drop_duplicates('geometry')
    gs = gs.set_crs(inEPSG)


    shpBasename = os.path.basename(INfile)
    TMP_file = os.path.join(tmp_indir, shpBasename)
    print(TMP_file)
    # shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file.replace('.shp', '.prj'))
    gs.to_file(TMP_file)

    return TMP_file


def getRandomPoint_Strata_AOI(INfile, inEPSG, TotSamples, classField, exclusionList, minSamplePerClass):

    gdf = gpd.read_file(INfile)

    if len(exclusionList) > 0:
        print('Selection ....')
        if gdf[classField].dtypes == 'float64':
            exclusionList = ([float(val) for val in exclusionList])
        if gdf[classField].dtypes == 'int64':
            exclusionList = ([int(float(val)) for val in exclusionList])
        # #  ---  object type = String = default val in Impact
        gdf = gdf[~gdf[classField].isin(set(exclusionList))]


    # aoi_geom = gdf.unary_union

    # Get unique values in the stratification column
    unique_strata = gdf[classField].unique()

    # Calculate the total area of all strata
    total_area = gdf.to_crs(3857).area.sum()

    # Initialize an empty list to store the generated points
    points = []
    rawVal = []
    weights = []
    startaInfo = {}
    totPoints = 0

    #  tollerance = number of decimal position when generating random point
    #  if LL 5 decimals and 0 in MT proj - better to keep 1mt min between points
    # todo: add spacing between points if necessary
    tollerance = 0
    if inEPSG == "EPSG:4326":
        tollerance = 5


    # Generate points for each stratum
    for stratum_value in unique_strata:
        stratum_features = gdf[gdf[classField] == stratum_value]
        # Calculate the total area of this stratum
        stratum_area = stratum_features.to_crs(3857).area.sum()
        stratum_geom = stratum_features.unary_union
        # Calculate the number of points for this stratum, proportional to its area
        num_points = int(TotSamples * (stratum_area / total_area))
        raw_num_points = int(TotSamples * (stratum_area / total_area))
        share = (stratum_area / total_area)
        if num_points < minSamplePerClass:
            num_points = minSamplePerClass
            # adjust weight ? or increase num by the user ?


        # print('Strata: '+str(stratum_value)+' CT:'+ str(num_points))



        if num_points > 0:
            res = return_points_from_geom(stratum_geom, num_points, inEPSG, tollerance, bufferDistance)
            points.append(res)
            rawVal.append(numpy.full(len(res),str(stratum_value)) )
            # weights.append(numpy.full(len(res), raw_num_points*1./num_points*1.)) #share/(len(res)*1.)*num_points) )
            # weights.append(numpy.full(len(res), 1 )) # recompute after looping all strata - to get the correct tot number after point extraction
            totPoints += len(res)

        else:
            print('No points for strata '+str(stratum_value))

        startaInfo[str(stratum_value)] = {'class': str(stratum_value),'points': len(res), 'share': share, 'weights': 1}

        print(startaInfo[str(stratum_value)])

    # recomputation of weight base on the original class share
    # the % of point per class is computed on the strata area.
    # when adding points to reach the minNumPoints the same share is mantained
    # sum af all weights all classes = num points


    for stratum_value in unique_strata:
        numSample = startaInfo[str(stratum_value)]['points']
        strataShare = startaInfo[str(stratum_value)]['share'] *1.
        print('numSample ', numSample , 'StrataShare ',strataShare )
        print('weights: ', strataShare /numSample * totPoints)
        weights.append(numpy.full(numSample, strataShare / numSample * totPoints ))


    d = {
        str(SETTINGS['vector_editing']['attributes']['ID']): numpy.arange(0, totPoints),
        str(SETTINGS['vector_editing']['attributes']['class_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['cluster_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['comments']): '',
        'RawVal': list(itertools.chain(*rawVal)),
        'Weights': list(itertools.chain(*weights)),
        'geometry': list(itertools.chain(*points))
    }


    gs = gpd.GeoDataFrame(d)
    gs = gs.set_index(str(SETTINGS['vector_editing']['attributes']['ID']))
    gs = gs.set_crs(inEPSG)


    # flatten the list of list retured
    # gs = gpd.GeoSeries(list(itertools.chain(*points)))


    shpBasename = os.path.basename(INfile)
    TMP_file = os.path.join(tmp_indir, shpBasename)
    print(TMP_file)
    # shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file.replace('.shp', '.prj'))
    print('Saving GPD to Shapefile')
    gs.to_file(TMP_file)

    return TMP_file






def getRandom_noStrata_segmentation(INfile, TotSamples, classField, exclusionList):
    gdf = gpd.read_file(INfile)

    if len(exclusionList) > 0:
        print('Selection ....')
        if gdf[classField].dtypes == 'float64':
            exclusionList = ([float(val) for val in exclusionList])
        if gdf[classField].dtypes == 'int64':
            exclusionList = ([int(float(val)) for val in exclusionList])
        # #  ---  object type = String = default val in Impact
        gdf = gdf[~gdf[classField].isin(set(exclusionList))]

    # train, test = train_test_split(gdf, test_size=TotSamples, stratify=gdf[classField], random_state=None)
    train, test = train_test_split(gdf, test_size=TotSamples, stratify=None, random_state=None)



    print(test)

    d = {
        str(SETTINGS['vector_editing']['attributes']['ID']): numpy.arange(0, len(test)),
        str(SETTINGS['vector_editing']['attributes']['class_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['cluster_t1']): '',
        str(SETTINGS['vector_editing']['attributes']['comments']): '',
        'RawVal': test[classField],
        'Weights': 1,  #./len(points),    # necessary to compute weights based on AREA of poly ?
        'geometry': test.geometry}

    gs = gpd.GeoDataFrame(d)
    gs = gs.set_index(str(SETTINGS['vector_editing']['attributes']['ID']))
    gs = gs.set_crs(inEPSG)


    # gs = gpd.GeoDataFrame(test, geometry=test.geometry)

    shpBasename = os.path.basename(INfile)
    TMP_file = os.path.join(tmp_indir, shpBasename)
    print(TMP_file)
    # shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file.replace('.shp', '.prj'))
    gs.to_file(TMP_file)
    return TMP_file





def getRandom_Strata_segmentation(INfile, TotSamples, classField, exclusionList, minSamplePerClass):
    gdf = gpd.read_file(INfile)
    # total_area= gdf.to_crs(3857).area.sum()
    # print('Raw Area = '+str(total_area))



    # for stratum_value in unique_strata:
    #     stratum_features = gdf[gdf[classField] == stratum_value]
    #     print(str(stratum_value) + '---> #' + str(len(stratum_features)) + '---> %' + str(len(stratum_features)/gdf.shape[0]*100) )




    if len(exclusionList) > 0:
        print('Selection ....')
        if gdf[classField].dtypes == 'float64':
            exclusionList = ([float(val) for val in exclusionList])
        if gdf[classField].dtypes == 'int64':
            exclusionList = ([int(float(val)) for val in exclusionList])
        # #  ---  object type = String = default val in Impact
        gdf = gdf[~gdf[classField].isin(set(exclusionList))]

    total_area= gdf.to_crs(3857).area.sum()
    print('Mod Area = '+str(total_area))
    pre_unique_strata = gdf[classField].unique()
    print('Pre Strata: ', pre_unique_strata)

    # Initialize an empty list to store the generated points
    dfs = []
    rawVal = []
    weights = []
    weights1 = []
    weights2 = []
    startaInfo = {}
    totPoints = 0


    for stratum_value in pre_unique_strata:
        stratum_features = gdf[gdf[classField] == stratum_value]
        stratum_area = stratum_features.to_crs(3857).area.sum()
        print(str(stratum_value) + '---> Area  ' + str(stratum_area)+ '---> Area %  ' + str(stratum_area/total_area*100))
        share = (stratum_area / total_area*1.)
        startaInfo[str(stratum_value)] = {'share': share, 'number': len(stratum_features)}




    # train, test = train_test_split(gdf, test_size=TotSamples, stratify=gdf[classField], random_state=None)
    train, test = train_test_split(gdf, test_size=int(TotSamples), stratify=gdf[classField], random_state=None)

    post_unique_strata = test[classField].unique()
    print('Post Strata: ', post_unique_strata)

    # check if all strata are sampled
    dropDuplicates = False
    for stratum_value in pre_unique_strata:
        numSample = len(test[test[classField] == stratum_value])
        totPoints += numSample
        print(str(stratum_value),'---> Orig Point ', numSample)
        # append extracted sample to array of DF - incluning empty one - will clean later
        dfs.append(test[test[classField] == stratum_value])

        print(startaInfo[str(stratum_value)])

        maxAvailableObj = startaInfo[str(stratum_value)]['number']
        print('Number of obj available in this strata: ', maxAvailableObj )


                                        # in case all features are selected already ... nothing to do
        if numSample < minSamplePerClass and numSample != maxAvailableObj:
            # extract new/extra point per class
            print('Extracting ',int(minSamplePerClass-numSample), ' points for class ',stratum_value )
            # test if the new number is <= total number of objects in the strata

            print('Max available obj ', maxAvailableObj )
            # test if int(minSamplePerClass-numSample) is < maxAvailableObj
            # but in case of limited number of obj and running a new extraction we do not know witch obj has been already selected in the loop before
            newNumSample = int(minSamplePerClass - numSample)
            print('New number ',newNumSample)

            # better to provide the total number of obj and eventually clean possible duplication to maximize the possibility of selecting obj
            if newNumSample >= maxAvailableObj:
                newNumSample = maxAvailableObj-1 # do not subtract the already selected (since we do not have the ID now) to maximize the number of obj to be taken
                #newNumSample = numSample
                print('Extracting a corrected number ', newNumSample, ' points for class ', stratum_value)
                dropDuplicates = True
            train, extraTest = train_test_split(gdf[gdf[classField] == stratum_value], test_size=newNumSample, stratify=None, random_state=None)
            dfs.append(extraTest)
            totPoints += len(extraTest)

    # points now contains selected samples with potential duplicates in case of re-extraction
    # d = {
    #     str(SETTINGS['vector_editing']['attributes']['ID']): numpy.arange(0, totPoints),
    #     str(SETTINGS['vector_editing']['attributes']['class_t1']): '',
    #     str(SETTINGS['vector_editing']['attributes']['cluster_t1']): '',
    #     str(SETTINGS['vector_editing']['attributes']['comments']): '',
    #     'RawVal': test[classField],
    #     'Weights': 1,  #./len(points),    # necessary to compute weights based on AREA of poly ?
    #     'geometry': test.geometry}
    # d = {
    #     list(itertools.chain(*points))
    # }

    # print(points)
    gdf = gpd.GeoDataFrame(pd.concat(dfs, ignore_index=True), crs=dfs[0].crs)

    # it is possible to have some duplicates in case  numSample < minSamplePerClass - rerun test-split
    # todo:cleaning requires recalculation of counts and weights
    if dropDuplicates:
        print('Dropping duplicated geometry due to limited number of available features of some strata')
        print('Total Points before ', len(gdf))
        gdf = gdf.drop_duplicates('geometry')
        print('Total Points before ', len(gdf))

    totPoints = len(gdf)
    # up to here the sample selection is base on the proportion
    total_sampled_area = gdf.to_crs(3857).area.sum()*1.
    for stratum_value in pre_unique_strata:

        stratum_features = gdf[gdf[classField] == stratum_value].to_crs(3857)
        stratum_area = stratum_features.area.sum()
        numSample = len(stratum_features)
        strataShare = startaInfo[str(stratum_value)]['share']
        # print(str(stratum_value), '---> Orig Share ',strataShare*100 , '---> Current Share %  ',stratum_area/total_sampled_area*100, 'CT =',  len(stratum_features))  # original area proportion
        #
        print('Weight: ',numSample, (strataShare / numSample) * totPoints)
        # print("Share ratio : ", strataShare /(stratum_area/total_sampled_area))
        #
        print("Weight revised: ", ((strataShare / numSample) * totPoints) *  (strataShare  / (stratum_area/total_sampled_area))   )

        weights.append(numpy.full(numSample, (strataShare / numSample) * totPoints))
        weights1.append(numpy.full(numSample, ((strataShare / numSample) * totPoints) *  (strataShare  / (stratum_area/total_sampled_area))))

        SumOfStaratWeights = weights1[-1].sum()
        # print(SumOfStaratWeights)

        # 10 ---> Orig Share  78.6226141689502 ---> Current Share %   71.773776503224 CT = 76
        # Weight:  76 1.210374454969365
        # Share ratio :  1.0954225623813811
        # Weight revised:  1.3258714869035093
        # 11 ---> Orig Share  15.492335906898349 ---> Current Share %   13.56776832306773 CT = 16
        # Weight:  16 1.1328770631919418
        # Share ratio :  1.1418484999156786
        # Weight revised:  1.2935739751945983

        # -----------------------------------------------------------
        # ----  recompute weight according to AREA of poly ? --------
        # -----------------------------------------------------------

        # for record in gdf.itertuples():
        #    recordArea = record.geometry.area
        #    # add code here to recompute weight based on Area of Poly
        # print(stratum_features)
        mysum = 0
        for obj in stratum_features['geometry']:
            # print(obj)
            # print(obj.area )
            weights2.append( SumOfStaratWeights  *  ( obj.area / stratum_area)  )
            # print('---------------------')
            # print (obj.area / stratum_area)
            mysum += obj.area / stratum_area
        # print(mysum)
        # print(weights2)




    gdf['WeightRaw'] = list(itertools.chain(*weights))
    gdf['WeightAdj'] = list(itertools.chain(*weights1))
    gdf['WeightArea'] = list(weights2)
    # gs = gpd.GeoDataFrame(points, geometry=points.geometry)
    # gs = gs.set_index(str(SETTINGS['vector_editing']['attributes']['ID']))




    shpBasename = os.path.basename(INfile)
    TMP_file = os.path.join(tmp_indir, shpBasename)
    print(TMP_file)
    gdf.to_file(TMP_file)

    return TMP_file









# ----------------------------------------------------------------------------------------------------------------------

# def getClassArea(inSHP, classField=None):
#     shp_ds = ogr.Open(inSHP, gdal.GA_ReadOnly)
#     layer = shp_ds.GetLayer(0)
#     layerName = layer.GetName()
#
#     areaTot = 0.0
#     myClass = []
#     myArea = []
#
#     if classField is None:
#         print('OK3')
#         sql = "SELECT sum(area(geometry)) from '" + layerName + "'"
#         area_layer = shp_ds.ExecuteSQL(sql, dialect='SQLITE')
#         if area_layer != None:
#             print('Looping')
#             for i, feature in enumerate(area_layer):
#                 areaTot += float(feature.GetField(0))
#
#
#     else:
#         sql = "SELECT " + classField + ", sum(area(geometry)) from '" + layerName + "' group by " + str(classField)
#         area_layer = shp_ds.ExecuteSQL(sql, dialect='SQLITE')
#         if area_layer != None:
#             for i, feature in enumerate(area_layer):
#                 myClass.append(feature.GetField(0))
#                 myArea.append(feature.GetField(1))
#
#         areaTot = numpy.sum(myArea)
#
#     # res = dict(zip(myClass, myArea))4
#     res = dict()
#     res['TOT'] = areaTot
#     res['Class'] = myClass
#     res['Area'] = myArea
#
#
#     shp_ds.ReleaseResultSet(area_layer)
#     layer = None
#     shp_ds = None
#     # print(res)
#     # # calculate area in % and save to dictionary
#     # mydata = []
#     # for i, item in enumerate(myClass):
#     #     print(item, myArea[i] / areaTot * 100)
#
#     return res
#
# def getRandom_noStrata_segmentation_old(INfile, TotSamples, minCoveragePercent, returnPoly, bufferDistance):
#     shpBasename = os.path.basename(INfile).replace('.shp','')
#     TMP_file = os.path.join(tmp_indir, shpBasename )
#     shutil.copyfile(INfile, TMP_file+'.shp')
#     shutil.copyfile(INfile.replace('.shp','.dbf'), TMP_file+'.dbf')
#     shutil.copyfile(INfile.replace('.shp','.prj'), TMP_file+'.prj')
#     shutil.copyfile(INfile.replace('.shp','.shx'), TMP_file+'.shx')
#
#     # ----------------------------------------------------
#     # ------   OPEN TMP VECTOR FILE -----------------
#     # ----------------------------------------------------
#
#     shp_ds = ogr.Open(TMP_file+'.shp', gdal.GA_Update)
#     layer = shp_ds.GetLayer(0)
#     layerName = layer.GetName()
#     layerDefinition = layer.GetLayerDefn()
#     TotalFeatures = layer.GetFeatureCount()
#
#     SHPfield = []
#     for i in range(layerDefinition.GetFieldCount()):
#         SHPfield.append(layerDefinition.GetFieldDefn(i).GetName())
#     # general fields
#     if 'Myrank' not in SHPfield:
#         print("ADD rank")
#         f1 = ogr.FieldDefn('Myrank', ogr.OFTReal)
#         layer.CreateField(f1)
#         f1 = None
#
#
#     #-------------------------------------------------------------------
#     # ---------------- LOOP FEATURES RASTER  ---------------------------
#     #-------------------------------------------------------------------
#     i = 0
#     feat = layer.GetNextFeature()
#     while feat is not None:
#         gdal.TermProgress(i/TotalFeatures)
#         i += 1
#         feat.SetField('Myrank', numpy.random.random())
#         layer.SetFeature(feat)
#         feat.Destroy()
#         feat = layer.GetNextFeature()
#     # now that each element has a rank, sort and keep either the number or the area
#
#     print('Preparing outfile ... ')
#     # ----------------------------------------------------
#     # ------       Creating OUT shp     ------------------
#     # ----------------------------------------------------
#
#     outDriver = ogr.GetDriverByName('ESRI Shapefile')
#     # outDriver = ogr.GetDriverByName('Memory')
#     exportDataSource = outDriver.CreateDataSource(TMP_file+'_samples.shp')
#     exportLayer = exportDataSource.CreateLayer(shpBasename+'_samples.shp', geom_type=ogr.wkbPolygon)
#     shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file+'_samples.prj')
#     f1 = ogr.FieldDefn("ID", ogr.OFTInteger)
#     exportLayer.CreateField(f1)
#
#
#     # AreaToT['TOT'] = 100   # Init
#     counter = 0
#     currentCoverage = 0
#     print('OK')
#
#     # if int(minCoveragePercent) != 100:
#     #     print('-----')
#     #     AreaToT = getClassArea(INfile, None)  # read from original file to keep temp file free
#     #     print(AreaToT['TOT'])
#     # print('******************************')
#     AreaToT = getClassArea(INfile, None)  # read from original file to keep temp file free
#
#     # ------------------   shuffling -------------------------
#     random_layer = shp_ds.ExecuteSQL("SELECT * FROM " + layerName + " ORDER BY Myrank LIMIT " + str(int(TotSamples*1.1)))
#     print('Ranking done ...')
#
#     feat = random_layer.GetNextFeature()
#     while feat is not None:
#         if counter <= TotSamples and currentCoverage <= minCoveragePercent:  #or area_counter < minAreaPerClass:
#             outFeature = ogr.Feature(exportLayer.GetLayerDefn())
#             if returnPoly:
#                 outFeature.SetGeometry(feat.geometry())
#                 currentCoverage += (float(feat.geometry().Area())/AreaToT['TOT'])*100.
#             else:
#                 if bufferDistance == 0:
#                     outFeature.SetGeometry(feat.geometry().Centroid())
#                 else:
#                     outFeature.SetGeometry(feat.geometry().Centroid().Buffer(bufferDistance))
#                     currentCoverage += ((3.14*(bufferDistance**2))/AreaToT['TOT'])*100.
#
#             print('CT: ', str(counter))
#             print('PCT: ', str(currentCoverage))
#
#
#             outFeature.SetField("ID", counter )
#             exportLayer.CreateFeature(outFeature)
#             outFeature.Destroy()
#
#             counter += 1
#             feat = random_layer.GetNextFeature()
#         else:
#             # random_layer.DeleteFeature(feat.GetFID())
#             # feat = random_layer.GetNextFeature()
#             feat = None
#
#
#     shp_ds.ReleaseResultSet(random_layer)
#     random_layer = None
#     area_layer = None
#     exportDataSource = None
#     # shp_ds.ExecuteSQL("REPACK " + layerName)
#     shp_ds.Destroy()
#
#     return TMP_file+'_samples.shp'
#
#
# def getRandom_Strata_segmentation_old(INfile, TotSamples, classField, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance):
#
#     shpBasename = os.path.basename(INfile).replace('.shp','')
#     TMP_file = os.path.join(tmp_indir, shpBasename )
#     shutil.copyfile(INfile, TMP_file+'.shp')
#     shutil.copyfile(INfile.replace('.shp','.dbf'), TMP_file+'.dbf')
#     shutil.copyfile(INfile.replace('.shp','.prj'), TMP_file+'.prj')
#     shutil.copyfile(INfile.replace('.shp','.shx'), TMP_file+'.shx')
#
#     # ----------------------------------------------------
#     # ------   OPEN TMP VECTOR FILE -----------------
#     # ----------------------------------------------------
#
#     shp_ds = ogr.Open(TMP_file+'.shp', gdal.GA_Update)
#     layer = shp_ds.GetLayer(0)
#     layerName = layer.GetName()
#     layerDefinition = layer.GetLayerDefn()
#     TotalFeatures = layer.GetFeatureCount()
#     SHPfield = []
#     for i in range(layerDefinition.GetFieldCount()):
#         SHPfield.append(layerDefinition.GetFieldDefn(i).GetName())
#
#     # general fields
#
#     if 'Myrank' not in SHPfield:
#         print("ADD rank")
#         f1 = ogr.FieldDefn('Myrank', ogr.OFTReal)
#         layer.CreateField(f1)
#         f1 = None
#
#     #-------------------------------------------------------------------
#     # ---------------- LOOP FEATURES RASTER  ---------------------------
#     #-------------------------------------------------------------------
#     i = 0
#     feat = layer.GetNextFeature()
#     while feat is not None:
#         gdal.TermProgress(i/TotalFeatures)
#         i += 1
#         feat.SetField('Myrank', numpy.random.random())
#         layer.SetFeature(feat)
#         feat.Destroy()
#         feat = layer.GetNextFeature()
#     # now that each element has a rank, sort and keep either the number or the area
#
#     # ----------------------------------------------------
#     # ------       Creating OUT shp     ------------------
#     # ----------------------------------------------------
#
#     outDriver = ogr.GetDriverByName('ESRI Shapefile')
#     # outDriver = ogr.GetDriverByName('Memory')
#     exportDataSource = outDriver.CreateDataSource(TMP_file + '_samples.shp')
#     exportLayer = exportDataSource.CreateLayer(shpBasename + '_samples.shp', geom_type=ogr.wkbPolygon)
#     shutil.copyfile(INfile.replace('.shp', '.prj'), TMP_file + '_samples.prj')
#     f1 = ogr.FieldDefn("ID", ogr.OFTReal)
#     f2 = ogr.FieldDefn("Weight", ogr.OFTReal)
#     exportLayer.CreateField(f1)
#     exportLayer.CreateField(f2)
#
#
#     # Get individual class and area
#     res = getClassArea(INfile, classField)
#     Class = res['Class']
#     Area = res['Area']
#     AreaTot = res['TOT']
#     print(Class)
#     print(Area)
#     print(AreaTot)
#
#     areaProportion = (Area/AreaTot*100.)
#     sampleProportion = TotSamples * (areaProportion / 100.)
#     original_sampleProportion = TotSamples * (areaProportion / 100.)
#
#     original_weight = (areaProportion / sampleProportion) * TotSamples /100.
#
#
#     for i, num in enumerate(sampleProportion):
#         if num <= minSamplePerClass:
#             sampleProportion[i] = minSamplePerClass
#             #print('Reset minimum number of class for '+str(Class[i]))
#             #print('From ' +str(num) + ' to ' +str(minSamplePerClass))
#
#
#     print('Area distribution per class: ',)
#     print(numpy.array2string(areaProportion, precision=2, separator=',', suppress_small=False))
#
#     print(' Original samples: ')
#     print(numpy.array2string(original_sampleProportion, precision=2, separator=',', suppress_small=False))
#
#     print('Adjusted number proportion : unbalanced: ' )
#     print(numpy.array2string(sampleProportion, precision=2, separator=',', suppress_small=False))
#
#
#
#
#
#
#     extraPoints = sum(sampleProportion) - TotSamples
#     print('Exra points ', extraPoints)
#     print('Redistributing extra points')
#     minNumPoints = len(sampleProportion) * minSamplePerClass
#     if extraPoints > 0:
#         for i, num in enumerate(sampleProportion):
#             classReductionFacror = extraPoints * areaProportion[i]/100.
#             # print('classReductionFacror', classReductionFacror)
#             if num - classReductionFacror > minSamplePerClass:
#                 sampleProportion[i] -= classReductionFacror
#
#     # recompute new weights per class after distributing extra points
#     recomputed_weight = areaProportion / sampleProportion * sum(sampleProportion) / 100.
#     print('original_weight: ')
#     print(numpy.array2string(original_weight, precision=2, separator=',', suppress_small=False))
#     print('recomputed weight: ')
#     print(numpy.array2string(recomputed_weight, precision=2, separator=',', suppress_small=False))
#     print('Tot samples = ', sum(sampleProportion))
#
#
#     # LOOP over each Class
#
#     for i, myclass in enumerate(Class):
#         print(i)
#         print('Processing class : '+ str(myclass))
#
#         counter = 0
#         currentAreaCoverage = 0  # not used in this implementation
#         weight = recomputed_weight[i]
#         # ------------------   shuffling -------------------------
#         random_layer = shp_ds.ExecuteSQL(
#             "SELECT * FROM " + layerName + " WHERE CAST("+ classField +" AS Integer) = "+str(int(Class[i]))+"  ORDER BY Myrank LIMIT " + str(int(sampleProportion[i])))
#         print("SELECT * FROM " + layerName + " WHERE CAST("+ classField +" AS Integer) = "+str(int(Class[i]))+"  ORDER BY Myrank LIMIT " + str(int(sampleProportion[i])))
#         # print('Query # feat = '+str(random_layer.GetFeatureCount()))
#         feat = random_layer.GetNextFeature()
#         while feat is not None:
#
#             # if counter <= sampleProportion[i] and currentAreaCoverage <= Area[i] * minAreaPerClass/100. :  # or area_counter < minAreaPerClass:
#                 outFeature = ogr.Feature(exportLayer.GetLayerDefn())
#                 if returnPoly:
#                     outFeature.SetGeometry(feat.geometry())
#                     currentAreaCoverage += float(feat.geometry().Area())   # not used
#
#                     # if counter > sampleProportion[i]:
#                     #     print('Add new item to reach the minimum area per class '+ str(myclass))
#                     # set feat weight
#                     #  2do
#                 else:
#                     if bufferDistance == 0:
#                         outFeature.SetGeometry(feat.geometry().Centroid())
#                         # set feat weight
#                         #  2do
#                     else:
#                         outFeature.SetGeometry(feat.geometry().Centroid().Buffer(bufferDistance))
#                         currentAreaCoverage += 3.14 * (bufferDistance ** 2)   # not used
#                         # set feat weight
#                         #  2do
#
#                 # print('CT: ', counter)
#                 # print('Class: ', myclass)
#                 # print('Area: ', currentAreaCoverage)
#                 # print('PCT: ', sampleProportion[i])
#
#                 outFeature.SetField("ID", counter)
#                 outFeature.SetField("Weight", weight)
#                 exportLayer.CreateFeature(outFeature)
#                 outFeature.Destroy()
#
#                 counter += 1
#                 feat = random_layer.GetNextFeature()
#             # else:
#             #     # random_layer.DeleteFeature(feat.GetFID())
#             #     # feat = random_layer.GetNextFeature()
#             #     feat = None
#
#         shp_ds.ReleaseResultSet(random_layer)
#         random_layer = None
#         if counter <= sampleProportion[i]:
#             print('Minimum area reached per class ' + str(myclass))
#         else:
#             print('Number of samples reached per class ' + str(myclass))
#
#     # shp_ds.ExecuteSQL("REPACK " + layerName)
#     shp_ds.Destroy()
#     exportDataSource.Destroy()
#     shp_ds = None
#     exportDataSource = None
#     # add a new random value to sort and export a shuffled shp
#     # 2do
#
#     return TMP_file+'_samples.shp'
#
#
#
#



# def Random_Points_in_Polygon(polygon, number):
#     points = []
#     minx, miny, maxx, maxy = polygon.bounds
#     while len(points) < number:
#         pnt = Point(np.random.uniform(minx, maxx), np.random.uniform(miny, maxy))
#         point = ogr.Geometry(ogr.wkbPoint)
#         print(point)
#         print(random.uniform(xmin, xmax), random.uniform(ymin, ymax))
#         point.AddPoint(random.uniform(xmin, xmax),
#                        random.uniform(ymin, ymax))
#         if polygon.contains(pnt):
#             points.append(pnt)
#     return points



# def RunSampling(INfile, OUTname, overwrite, stratified, segmentation, TotSamples, minCoveragePercent, classField, exclusionList, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance):
def RunSampling(INfile, OUTname, overwrite, inEPSG, strata, outObject, TotSamples, classField, exclusionList, minSamplePerClass, bufferDistance, log):

    try:

        
        ImageProcessing.validPathLengths(OUTname,None, 5, log)

        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        outDriver = ogr.GetDriverByName('ESRI Shapefile')

        if not os.path.exists(INfile):
            log.send_error('Cannot read input file --> '+ INfile)

            return False

        if os.path.exists(OUTname):
            if overwrite:
                try:
                    outDriver.DeleteDataSource(OUTname)
                except:
                    log.send_error('Cannot delete output file')

                    return False
            else:
                log.send_warning(OUTname+' <b> Already Exists </b>')

                return True



        # ####################################################
        #      selelct original poly from input shapefile
        # ####################################################

        if INfile.endswith('.shp'):

            # ---------------------------------------------------------------------------------------------------------
            # ----------------------------  INPUT is a map with classes or simple AOI  --------------------------------
            # ---------------------------------------------------------------------------------------------------------
            if not outObject:
                print('Point')
                #  ------  case with no strata selection  -----
                #  ------  tested with and without exclusion list    ----
                if not strata:
                    print('No strata')
                    out_shp_path = getRandomPoint_noStrata_AOI(INfile, inEPSG, TotSamples, bufferDistance, classField, exclusionList)
                #  ------  case with strata selection  -----
                #  ------  tested with and without exclusion list    ----
                #  ------  minSamplePerClass: fix the weights or tot num  ----
                if strata:
                    print('Strata')
                    out_shp_path = getRandomPoint_Strata_AOI(INfile, inEPSG, TotSamples, classField, exclusionList, minSamplePerClass)

            if outObject:
                print('Objects')
                if not strata:
                    out_shp_path = getRandom_noStrata_segmentation(INfile, TotSamples, classField, exclusionList)

                if strata:
                    out_shp_path = getRandom_Strata_segmentation(INfile, TotSamples, classField, exclusionList, minSamplePerClass)

            #
            # if not stratified and segmentation:
            #     out_shp_path = getRandom_noStrata_segmentation(INfile, TotSamples, minCoveragePercent, returnPoly, bufferDistance)
            # if stratified and segmentation:
            #     out_shp_path = getRandom_Strata_segmentation(INfile, TotSamples, classField, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance)
            #




        shutil.move(out_shp_path, OUTname)
        shutil.move(out_shp_path.replace('.shp', '.dbf'), OUTname.replace('.shp', '.dbf'))
        shutil.move(out_shp_path.replace('.shp', '.prj'), OUTname.replace('.shp', '.prj'))
        shutil.move(out_shp_path.replace('.shp', '.shx'), OUTname.replace('.shp', '.shx'))
        shutil.move(out_shp_path.replace('.shp', '.cpg'), OUTname.replace('.shp', '.cpg'))
        log.send_message(' Selection completed')
        return True














        # ----------------------------------------------------
        # ------   OPEN INPUT VECTOR CLASSIF -----------------
        # ----------------------------------------------------

        shp_ds = ogr.Open(INfile, gdal.GA_ReadOnly)
        layer = shp_ds.GetLayer(0)
        layerName = layer.GetName()
        layerDefinition = layer.GetLayerDefn()
        TotalFeatures = layer.GetFeatureCount()
        targetSR = layer.GetSpatialRef()


        # ------------------------------------------------------------------------------
        #  create the samples based on the extent of the shapefile and passes parameters
        # ------------------------------------------------------------------------------
        # outDriver = ogr.GetDriverByName('ESRI Shapefile')
        outDriver = ogr.GetDriverByName('Memory')
        outDataSource = outDriver.CreateDataSource("TMP")
        outLayer = outDataSource.CreateLayer("TMP", geom_type=ogr.wkbPolygon, srs=srs)
        featureDefn = outLayer.GetLayerDefn()
        f1 = ogr.FieldDefn("PlotID", ogr.OFTInteger)
        outLayer.CreateField(f1)
        f2 = ogr.FieldDefn("SubID", ogr.OFTInteger)
        outLayer.CreateField(f2)
        f3 = ogr.FieldDefn("Random", ogr.OFTInteger)
        outLayer.CreateField(f3)
        f1 = None



        if not stratified:
            unionGeom = shp_ds.ExecuteSQL("SELECT ST_Union(geometry) from " + layerName  , dialect="SQLITE")


    except Exception as e:

        log.send_error("Error :")
        log.send_error(str(e))
        log.close()
    return
        

if __name__ == "__main__":

    me, INfile, suffix, overwrite, inEPSG, strata, outObject, TotSamples, classField, exclusionList, minSamplePerClass, bufferDistance = sys.argv
    log = LogStore.LogProcessing('Random Sampling', 'RandomSampling')
    try:
        INfile = json.loads(INfile)[0]

        if overwrite in ['No', False, 0, 'NO', 'N', 'False', 'false']:
            overwrite = False
        else:
            overwrite = True

        if strata in ['No', False, 0, 'NO', 'N', 'False', 'false']:
            strata = False
        else:
            strata = True

        if outObject in ['No', False, 0, 'NO', 'N', 'False', 'false']:
            outObject = False
        else:
            outObject = True


        if strata:
            OUTname = INfile.replace('.shp','_stratified_'+ suffix +'.shp')
        else:
            OUTname = INfile.replace('.shp', '_no_strata_'+ suffix +'.shp')
            # clean classField in case of no exclusion ids
            if exclusionList == '':
                classField = ''

        print('-------------')
        print(exclusionList)
        if exclusionList != '':
            if exclusionList.endswith(','):
                exclusionList=exclusionList[:-1]
            if exclusionList.startswith(','):
                exclusionList=exclusionList[1:]
            exclusionList = ([str(val) for val in exclusionList.split(',')])
        print(exclusionList)


        TotSamples = int(TotSamples)
        minSamplePerClass = int(minSamplePerClass)
        if bufferDistance != '':
            bufferDistance = float(bufferDistance)
        else:
            bufferDistance = 0

        if outObject:
            bufferDistance = 0

        log.set_parameter('IN File ', INfile)
        log.set_parameter('IN EPSG ', inEPSG)
        log.set_parameter('OUT File ', OUTname)
        log.set_parameter('overwrite ', overwrite)
        log.set_parameter('Stratified ', strata)
        log.set_parameter('Total Samples ', TotSamples)
        log.set_parameter('Exclusion Class ', classField)
        log.set_parameter('Exclusion List ', ','.join(exclusionList))

        # log.set_parameter('Exclusion List ', exclusionList)

        if strata:
            log.set_parameter('DBF Class Attribute ', classField)
            log.set_parameter('Min Samples per class', minSamplePerClass)
            #log.set_parameter('Min % of Area covered per class', minAreaPerClass)  # 2b confirmed
        #else:
            #log.set_parameter('% of the Total Area to be covered', minAreaPerClass)  # 2b confirmed


        RunSampling(INfile, OUTname, overwrite, inEPSG, strata, outObject, TotSamples, classField, exclusionList, minSamplePerClass, bufferDistance, log)
        # print(getClassArea(INfile,'T1_class'))


    except Exception as e:
        print('Sampling Error')
        log.send_error(str(e))

    finally:
        log.close()
    import sys
    sys.exit()

    #
    # overwrite = True
    # # ------------------------------------------------
    # segmentation = True
    #
    # # ------------------------------------------------
    # stratified = False
    # returnPoly = False
    # bufferDistance = 0   # input shp proj unit
    # TotSamples = 1000
    # minCoveragePercent = 100  # for NO STRATA              # ?? necessary ??
    #
    # # ------------------------------------------------
    #
    # returnPoly = True
    # stratified = True
    # minSamplePerClass = 10
    # minAreaPerClass = 0.5   # in %   for STRATA             # ?? necessary ??
    #
    #
    # # ----------  TEST area ---------------------------
    # segmentation = False
    # stratified = True
    # returnPoly = False
    # bufferDistance = 0  # input shp proj unit
    # TotSamples = 100
    # minSamplePerClass = 2
    # minCoveragePercent = 100  # for NO STRATA             # ?? necessary ??
    #
    # classField = 'ID'
    # exclusionList = ['Box0']
    # classField = 'T1_class'
    # exclusionList = ['1','2']
    #
    #
    # # ----------  TEST area ---------------------------
    # segmentation = False
    # stratified = True
    # # returnPoly = False
    # # bufferDistance = 0.01  # input shp proj unit
    # TotSamples = 5000
    # minSamplePerClass = 10
    # # minCoveragePercent = 100  # for NO STRATA             # ?? necessary ??
    #
    # classField = 'ID'
    # exclusionList = ['Box0']
    # classField = 'T1_class'
    # exclusionList = ['1']
    #
    # classField = 'DN'
    # exclusionList = []
    #
    #
    # #  --------  Internal test  -----------------------
    # if not stratified:
    #     returnPoly = False
    #     minSamplePerClass = 0
    #
    # if returnPoly:
    #     bufferDistance = 0
    #
    # # if bufferDistance == 0:
    # #     minCoveragePercent = 100.
    #
    # suffix = 'TestWeightsv5'
    #
    # INfile = r'D:\IMPACT5\IMPACT\DATA\HotSpot\virunga_test\Multidate_segmentation_1000.shp'    #
    #
    #
    # INfile = r'D:\IMPACT5\IMPACT\DATA\Sardegna.shp'                                    # 8 box with intersection
    #
    #
    # INfile = r'D:\IMPACT5\IMPACT\DATA\Sardegna_MultiBox.shp'  # 8 box with intersection
    # INfile = r'D:\IMPACT5\IMPACT\DATA\Segment04_fix.shp'  # PhangNah segmentation - quite big
    # INfile = r'E:\dev\IMPACT5\DATA\Z_Sampling\Segment04_fix.shp'  # PhangNah segmentation - quite big
    # INfile = r'E:\dev\IMPACT5\DATA\Sardegna\ESA\Sardegna_subset_sampling_Map.shp'  # ESA vector map
    #
    # # INfile = r'E:\dev\IMPACT5\DATA\Virunga\CAF02_vector_2014_test.shp'  # PhangNah segmentation - quite big
    #
    #
    #
    #
    # if stratified:
    #     OUTname = INfile.replace('.shp','_stratified_'+ suffix +'.shp')
    # else:
    #     OUTname = INfile.replace('.shp', '_no_strata_'+ suffix +'.shp')
    #
    #
    #
    # RunSampling(INfile, OUTname, overwrite, stratified, segmentation, TotSamples, float(minCoveragePercent), classField, exclusionList, minSamplePerClass, minAreaPerClass, returnPoly, bufferDistance)
    # # print(getClassArea(INfile,'T1_class'))
