#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import glob
import json
import LogStore

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2022')
    print('Params:')
    print('input directory as array ["Dir1","Dir2"]')
    print('out filename')
    print('overwrite output')
    sys.exit(0)

print('Loading Libs')
import sys
# import PIL
import PIL.Image as PILimage
# from PIL import ImageDraw, ImageFont, ImageEnhance
from PIL.ExifTags import TAGS, GPSTAGS

print('done')

class Worker(object):
    def __init__(self, img):
        self.img = img
        self.exif_data = self.get_exif_data()
        self.lat = self.get_lat()
        self.lon = self.get_lon()
        self.date = self.get_date_time()
        self.length = self.get_image_length()
        self.width = self.get_image_width()
        self.orientation = self.get_image_orientation()

        super(Worker, self).__init__()

    def get_image_width(self):
        if 'ImageWidth' in self.exif_data:
            return self.exif_data["ImageWidth"]
        else:
            return 500
    def get_image_length(self):
        if 'ImageLength' in self.exif_data:
            return self.exif_data["ImageLength"]
        else:
            return 500

    def get_image_orientation(self):
        if 'Orientation' in self.exif_data:
            return self.exif_data["Orientation"]
        else:
            return 0

    @staticmethod
    def get_if_exist(data, key):
        if key in data:
            return data[key]
        return None

    @staticmethod
    def convert_to_degress(value, ref):
        #Covert a tuple of coordinates in the format (degrees, minutes, seconds)

        if ref.upper() in ['W', 'S']:
            mul = -1
        elif ref.upper() in ['E', 'N']:
            mul = 1
        else:
            print("Incorrect, xpecting one of 'N', 'S', 'E' or 'W', ")
            print(ref.upper())
            return 0
        return mul * (float(value[0]) + float(value[1]) / 60. + float(value[2]) / 3600.)

    def get_exif_data(self):
        """Returns a dictionary from the exif data of an PIL Image item. Also
        converts the GPS Tags"""
        exif_data = {}
        info = self.img._getexif()
        if info:
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                if decoded == "GPSInfo":
                    gps_data = {}
                    for t in value:
                        sub_decoded = GPSTAGS.get(t, t)
                        gps_data[sub_decoded] = value[t]
                    exif_data[decoded] = gps_data
                else:
                    exif_data[decoded] = value
        return exif_data

    def get_lat(self):
        """Returns the latitude and longitude, if available, from the
        provided exif_data (obtained through get_exif_data above)"""
        # print(exif_data)
        if 'GPSInfo' in self.exif_data:
            gps_info = self.exif_data["GPSInfo"]
            gps_latitude = self.get_if_exist(gps_info, "GPSLatitude")
            gps_latitude_ref = self.get_if_exist(gps_info, 'GPSLatitudeRef')
            if gps_latitude and gps_latitude_ref:
                lat = self.convert_to_degress(gps_latitude, gps_latitude_ref)
                return lat
        else:
            return None

    def get_lon(self):
        """Returns the latitude and longitude, if available, from the
        provided exif_data (obtained through get_exif_data above)"""
        # print(exif_data)
        if 'GPSInfo' in self.exif_data:
            gps_info = self.exif_data["GPSInfo"]
            gps_longitude = self.get_if_exist(gps_info, 'GPSLongitude')
            gps_longitude_ref = self.get_if_exist(gps_info, 'GPSLongitudeRef')
            if gps_longitude and gps_longitude_ref:
                lon = self.convert_to_degress(gps_longitude, gps_longitude_ref)
                return lon
        else:
            return None

    def get_date_time(self):
        if 'DateTime' in self.exif_data:
            date_and_time = self.exif_data['DateTime']
            return date_and_time

if __name__ == '__main__':
    print('IN')
    log = LogStore.LogProcessing('Photos to KML', 'PhotosToKML')
    print('Log ok ')
    try:
        me, INDIR, out_name, overwrite = sys.argv

    except Exception as e:
        log.send_error("Photos to KML: " + str(e))
        print('General error')
        print(e)
        usage()
        log.close()
        sys.exit()

    try:
        INDIRS = json.loads(INDIR)
        print('INDIRS')
        out_name = os.path.join(INDIRS[0], out_name + '.kml')
        log.set_parameter('Input Dir ', INDIR)
        log.set_parameter('Out file', out_name)
        log.set_parameter('Overwrite', overwrite)



        if not overwrite and os.path.exists(out_name):
            log.send_warning(out_name + ' <b> Already Exists </b>')
            log.close()
            sys.exit()
        try:
            if os.path.exists(out_name):
                os.remove(out_name)
                log.send_message("Deleting existing output file.")
        except:
            log.send_error(out_name + ' cannot be deleted.')
            log.close()
            sys.exit()

        images = []
        for INDIR in INDIRS:
            log.send_message("Scanning directory")
            tmp = glob.glob(INDIR+'*.jpg')
            for i in tmp:
                images.append(i)
        log.send_message("Found "+str(len(images)) + " images")
        if len(images) == 0:
            log.send_warning("No Out file saved: " + out_name)
            log.send_message("Done")
            log.close()
            sys.exit()

        content = '<?xml version="1.0" encoding="UTF-8"?>\n'
        content += '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n'
        content += '<Document>\n'


        for imgName in images:
            if not (imgName.endswith('.jpg') or imgName.endswith('.JPG')):
                continue
            img = PILimage.open(imgName)
            image = Worker(img)
            lat = image.lat
            lon = image.lon
            if lat is None or lon is None:
                log.send_warning("No GPS: " + imgName)
                continue
            if lat == 0 and lon == 0:
                log.send_warning("No GPS: " + imgName)
                continue
            date = image.date
            length = image.length
            width = image.width
            orientation = image.orientation

            content += '<Placemark>\n'
            content += '    <name>' + os.path.basename(imgName).replace('.jpg','').replace('.JPG','') + '</name>\n'
            content += '    <description> <![CDATA[<b>Lat:</b><code> '+ str(lat) +' </code> <b>Lon:</b><code> '+ str(lon)+'</code><b></b><br/><br/><img src=\'file:///' + imgName +'\' width = \'600\' /><br/> <br/><b> File: </b> <br/> <code> '+ out_name +'</code> <hr/>]]> </description>'
            content += '   <LookAt>\n'
            content += '        <longitude>' + str(lon) + '</longitude>\n'
            content += '        <latitude>' + str(lat) + '</latitude>\n'
            content += '        <altitude>0</altitude>\n'
            content += '        <heading>' + str(orientation) + '</heading>\n'
            content += '        <tilt>10</tilt>\n'
            content += '        <range>800</range>\n'
            content += '    </LookAt>\n'
            content += '    <Icon>\n'
            content += '       <href>' + imgName + '</href>\n'
            content += '       <viewRefreshMode>onStop</viewRefreshMode>\n'
            content += '       <viewRefreshTime>1</viewRefreshTime>\n'
            content += '       <viewBoundScale>0.75</viewBoundScale>\n'
            content += '    </Icon>\n'
            content += '   <Point>\n'
            content += '        <coordinates> '+str(lon) + ', ' + str(lat) + ', 0 </coordinates>\n'
            content += '   </Point>\n'
            content += ' </Placemark>\n'

        content += '</Document>\n'
        content += '</kml>\n'
        f = open(out_name, 'w')
        f.write(content)
        f.close()
        log.send_message("Out file saved: "+out_name)
        log.send_message("Done")
        log.close()
        sys.exit()

    except Exception as e:
        log.send_error("Photos to KML: " + str(e))
        print('General error:')
        print(e)
        log.close()
        sys.exit()