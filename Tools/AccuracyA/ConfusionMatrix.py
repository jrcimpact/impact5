#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json
import numpy
import shutil

from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

from html import escape
# Import local libraries
import LogStore
from __config__ import *
import ImageProcessing
from Simonets_PROC_libs import test_overlapping

# imp.load_source(module_name, path_to_file)

sys.path.append(os.path.join(GLOBALS['Tools'], "Vector/"))
from Compute_stats_fill_shapefile import Compute_stats_fill_shapefile

def print_table_html(data):
    header = data[0]
    table = "<table>\n"
    table += "<tr>\n"
    for column in header:
        table += "<th>%8s</th>\n" % column.strip()
    table += "</tr>\n"

    # Create the table's row data
    for line in data[1:]:
        # row = line.split(",")
        table += "  <tr>\n"
        for column in line:
            table += "<td>%8s</td>\n" % column.strip()
        table += "</tr>\n"

    table += "</table>"
    return table.replace(" ",'&nbsp;')


if __name__ == "__main__":

    me, inShp, attribute, weight, map, attribTif, attribShp, noClass, out_name, overwrite = sys.argv

    log = LogStore.LogProcessing('Confusion Matrix ', 'ConfusionMatrix')
    try:
        # 2b implemented
        aggregateCLClevel2 = False

        overwrite = True if overwrite == "Yes" else False
        inShp = json.loads(inShp)[0]
        map = json.loads(map)[0]

        out_name = os.path.join(os.path.dirname(inShp), out_name) + '.htm'
        log.set_parameter('Input Vector', inShp)
        log.set_parameter('Input Layer', attribute)
        log.set_parameter('Weight Layer', weight)
        log.set_parameter('Reference Map', map)
        if map.endswith('.shp'):
            log.set_parameter('Map Layer ', attribShp)
            log.set_parameter('Exclude classess ', noClass)
        else:
            log.set_parameter('Map band ', attribTif)

        log.set_parameter('Out name ', out_name)
        log.set_parameter('Overwrite', overwrite)

        class2remove = noClass.split(',')
        # class2remove = []
        # for x in noClass:
        #     if x != '':
        #         class2remove.append(float(x))
        # print(class2remove)
        #
        # log.close()
        # sys.exit()

        ImageProcessing.validPathLengths(out_name, None, 8, log)
        # Define pixel_size and NoData value of new raster
        if not overwrite and os.path.exists(out_name):
            log.send_warning(out_name+' <b> Already Exists </b>')
            log.close()
            sys.exit()
        try:
            if os.path.exists(out_name):
                os.remove(out_name)
                log.send_message("Deleting existing output file.")

        except:
            log.send_error(out_name+' cannot be deleted.')
            log.close()
            sys.exit()


        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "ConfusionMatrix")
        shutil.rmtree(tmp_indir, ignore_errors=True)
        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)

        print(inShp)
        in_ds = ogr.GetDriverByName('ESRI Shapefile').Open(inShp)
        INSR = osr.SpatialReference()
        layer = in_ds.GetLayer(0)
        INSR = layer.GetSpatialRef()
        INProj4 = INSR.ExportToProj4()
        layerName = layer.GetName()
        in_ds = None
        layer = None


        print(INSR)
        print(INProj4)

        masterSHP = os.path.join(tmp_indir, 'master.shp')
        tmpShp = os.path.join(tmp_indir, os.path.basename(inShp).replace('.shp', '_int.shp'))

        if map.endswith('.shp'):

            out_ds = ogr.GetDriverByName('ESRI Shapefile').Open(map)
            OUTSR = osr.SpatialReference()
            layer1 = out_ds.GetLayer(0)
            OUTSR = layer1.GetSpatialRef()
            OUTProj4 = OUTSR.ExportToProj4()
            out_ds = None
            layer1 = None


            SQL = '"SELECT "%s" as refUserVal FROM "%s""' % (attribute, layerName)
            if weight not in ["", "-"]:
                SQL = '"SELECT "%s" as refUserVal, "%s" as weightVal  FROM "%s""' % (attribute, weight, layerName)

            # if INProj4 != OUTProj4:
            #     print('Changing projection')
            #     SQL += ' -t_srs ' + OUTProj4
            print(SQL)
            os.system('ogr2ogr -f "ESRI Shapefile" '+masterSHP+' '+inShp+' -sql '+ SQL)

            if not os.path.exists(masterSHP):
                log.send_error('Temporary file not created.')
                log.close()
                sys.exit()

            print('geopandas')
            import geopandas as gpd

            masterSHP_gp = gpd.read_file(masterSHP)
            map_gp = gpd.read_file(map)

            inShpReproj = masterSHP_gp.to_crs(map_gp.crs)

            i2 = gpd.overlay(df1=inShpReproj, df2=map_gp, how="intersection", keep_geom_type=True)
            i2.to_file(tmpShp, driver='ESRI Shapefile')

            i2 = None
            inShpReproj = None
            map_gp = None
            masterSHP_gp = None


            print('Processing')
            if not os.path.exists(tmpShp):
                log.send_error('Temporary file not created.')
                log.close()
                sys.exit()
            # get all values from the 2 columns
            in_ds = ogr.GetDriverByName('ESRI Shapefile').Open(tmpShp)
            layer = in_ds.GetLayer(0)
            layerName = layer.GetName()
            if weight not in ["", "-"]:
                sql = 'SELECT "%s","%s","%s" FROM "%s"' % ('refUserVal', attribShp, 'weightVal', layerName)
            else:
                sql = 'SELECT "%s","%s" FROM "%s"' % ('refUserVal', attribShp, layerName)
            print(sql)
            page = in_ds.ExecuteSQL(sql, dialect="SQLITE")



        # query layer is a TIFF file
        else:
            print('Raster mode')
            in_ds = ogr.GetDriverByName('ESRI Shapefile').Open(inShp)
            ogr.GetDriverByName('ESRI Shapefile').CopyDataSource(in_ds, tmpShp)
            in_ds = None

            # RUN_populate_shapefile_with_class(map, tmpShp, "Majority")
            gdal.TermProgress = gdal.TermProgress_nocb
            numpy.seterr(divide='ignore', invalid='ignore')

            Compute_stats_fill_shapefile(tmpShp, [map], "", 'mode', attribTif, True)
            print('reading stats from file ')
            in_ds = ogr.GetDriverByName('ESRI Shapefile').Open(tmpShp)
            layer = in_ds.GetLayer(0)
            layerName = layer.GetName()
            if weight not in ["", "-"]:
                sql = 'SELECT "%s","%s","%s" FROM "%s"' % (attribute, "F1_1_mode", weight, layerName)
            else:
                sql = 'SELECT "%s","%s" FROM "%s"' % (attribute, "F1_1_mode", layerName)
            # print(sql)
            page = in_ds.ExecuteSQL(sql, dialect="SQLITE")


    # ------------------------------------------------------------------------------------------
    # --- now we have a common dataset SHP to start with, from SHP intersection or Zonal Stats
    # ------------------------------------------------------------------------------------------

        feat = page.GetNextFeature()
        y_true = []
        y_pred = []
        y_weight = []
        if weight not in ["", "-"]:
            while feat is not None:
                y_true.append(int(feat.GetField(0) or 0))
                y_pred.append(int(feat.GetField(1) or 0))
                y_weight.append(float(feat.GetField(2) or 1.0))
                feat = page.GetNextFeature()
        else:
            while feat is not None:
                y_true.append(int(feat.GetField(0) or 0))
                y_pred.append(int(feat.GetField(1) or 0))
                y_weight.append(1.0)
                feat = page.GetNextFeature()

        # feat.Destroy()
        feat = None
        in_ds.ReleaseResultSet(page)
        in_ds.Destroy()
        in_ds = None

        #  ----------------------------------------------------------------------------
        #  -------- All info are now in a 3 arrays ------------------------------------
        #  ----------------------------------------------------------------------------

        y_true = numpy.asarray(y_true)
        y_pred = numpy.asarray(y_pred)
        y_weight = numpy.asarray(y_weight)

        if aggregateCLClevel2:
            y_true[y_true == 6] = 0
            # y_true[y_true == 242] = 0
            y_true[y_true == 212] = 0
            y_true[y_true == 33] = 0

            y_true[y_true == 111] = 11
            y_true[y_true == 112] = 11

            y_true[y_true == 121] = 12
            y_true[y_true == 122] = 12
            y_true[y_true == 123] = 12
            y_true[y_true == 124] = 12

            y_true[y_true == 131] = 13
            y_true[y_true == 132] = 13
            y_true[y_true == 133] = 13

            y_true[y_true == 141] = 14
            y_true[y_true == 142] = 14
            y_true[y_true == 1411] = 14
            y_true[y_true == 1412] = 14


            y_true[y_true == 211] = 21
            y_true[y_true == 2121] = 21
            y_true[y_true == 2122] = 21
            y_true[y_true == 2124] = 21


            y_true[y_true == 221] = 22
            y_true[y_true == 222] = 22
            y_true[y_true == 223] = 22

            y_true[y_true == 242] = 24

            y_true[y_true == 311] = 31
            y_true[y_true == 312] = 31

            y_true[y_true == 321] = 32
            y_true[y_true == 322] = 32
            y_true[y_true == 323] = 32


            y_true[y_true == 331] = 33
            y_true[y_true == 332] = 33
            y_true[y_true == 333] = 33

            y_true[y_true == 411] = 41

            y_true[y_true == 421] = 42
            y_true[y_true == 422] = 42

            y_true[y_true == 511] = 51
            y_true[y_true == 512] = 51

            y_true[y_true == 521] = 52
            y_true[y_true == 523] = 52

            #--------------------------
            y_pred[y_pred == 6] = 0
            # y_pred[y_pred == 242] = 0
            y_pred[y_pred == 212] = 0
            y_pred[y_pred == 33] = 0

            y_pred[y_pred == 111] = 11
            y_pred[y_pred == 112] = 11

            y_pred[y_pred == 121] = 12
            y_pred[y_pred == 122] = 12
            y_pred[y_pred == 123] = 12
            y_pred[y_pred == 124] = 12

            y_pred[y_pred == 131] = 13
            y_pred[y_pred == 132] = 13
            y_pred[y_pred == 133] = 13

            y_pred[y_pred == 141] = 14
            y_pred[y_pred == 142] = 14
            y_pred[y_pred == 1411] = 14
            y_pred[y_pred == 1412] = 14

            y_pred[y_pred == 211] = 21
            y_pred[y_pred == 2121] = 21
            y_pred[y_pred == 2122] = 21
            y_pred[y_pred == 2124] = 21

            y_pred[y_pred == 221] = 22
            y_pred[y_pred == 222] = 22
            y_pred[y_pred == 223] = 22

            y_pred[y_pred == 242] = 24

            y_pred[y_pred == 311] = 31
            y_pred[y_pred == 312] = 31

            y_pred[y_pred == 321] = 32
            y_pred[y_pred == 322] = 32
            y_pred[y_pred == 323] = 32

            y_pred[y_pred == 331] = 33
            y_pred[y_pred == 332] = 33
            y_pred[y_pred == 333] = 33

            y_pred[y_pred == 411] = 41

            y_pred[y_pred == 421] = 42
            y_pred[y_pred == 422] = 42

            y_pred[y_pred == 511] = 51
            y_pred[y_pred == 512] = 51

            y_pred[y_pred == 521] = 52
            y_pred[y_pred == 523] = 52

        y_true = list(y_true)
        y_pred = list(y_pred)

        y_true1 = []
        y_pred1 = []
        y_weight1 = []


        for x in range(len(y_pred)):
            if str(y_pred[x]) not in class2remove and str(y_true[x]) not in class2remove:
                y_pred1.append(y_pred[x])
                y_true1.append(y_true[x])
                y_weight1.append(y_weight[x])

        y_weight = y_weight1
        y_pred = y_pred1
        y_true = y_true1


        labels = list(set(y_true + y_pred))
        labels.sort()

        labels_true = list(set(y_true))
        labels_true.sort()
        labels_pred = list(set(y_pred))
        labels_pred.sort()

        if weight not in ["", "-"]:
            cMatrix_noWeight = confusion_matrix(y_pred, y_true)
            accScore_noWeight = accuracy_score(y_pred, y_true)
            Uacc_noWeight = (cMatrix_noWeight.diagonal() / cMatrix_noWeight.sum(axis=1))
            Pacc_noWeight = (cMatrix_noWeight.diagonal() / cMatrix_noWeight.sum(axis=0))
            Uacc_noWeight = ["{:.2f}".format(x * 100) for x in Uacc_noWeight]
            Pacc_noWeight = ["{:.2f}".format(x * 100) for x in Pacc_noWeight]
            cMatrix_noWeight_mod = numpy.column_stack((labels, cMatrix_noWeight))
            cMatrix_noWeight_mod = numpy.row_stack(([" "] + labels, cMatrix_noWeight_mod))
            cMatrix_noWeight_mod = numpy.column_stack((cMatrix_noWeight_mod, ["UA "] + Uacc_noWeight))
            cMatrix_noWeight_mod = numpy.row_stack(
                (cMatrix_noWeight_mod, ["<b>PA</b>"] + Pacc_noWeight + ["{:.2f}".format(accScore_noWeight * 100)]))
            # print(accScore_noWeight)
            # print(classification_report(y_true, y_pred))

        cMatrix = confusion_matrix(y_pred, y_true, sample_weight=y_weight)
        accScore = accuracy_score(y_pred, y_true, sample_weight=y_weight)
        Uacc = (cMatrix.diagonal() / cMatrix.sum(axis=1))
        Pacc = (cMatrix.diagonal() / cMatrix.sum(axis=0))
        Uacc = ["{:.2f}".format(x * 100) for x in Uacc]
        Pacc = ["{:.2f}".format(x * 100) for x in Pacc]

        cMatrix = numpy.round(cMatrix, 2)
        cMatrix_mod = numpy.column_stack((labels, cMatrix))
        cMatrix_mod = numpy.row_stack(([" "] + labels, cMatrix_mod))
        cMatrix_mod = numpy.column_stack((cMatrix_mod, ["UA"] + Uacc))
        cMatrix_mod = numpy.row_stack((cMatrix_mod, ["<b>PA</b> "] + Pacc + ["{:.2f}".format(accScore * 100)]))

        # # numpy.set_printoptions(precision=2)
        # if weight not in ["", "-"]:
        #     print("-----NO Weight ---------")
        #     numpy.savetxt(sys.stdout, cMatrix_noWeight_mod, '%4s', delimiter=",", header="Raw confusion matrix, no weight")
        #     print("Total samples: "+ str(len(y_true)))
        # print("------Weighted  --------")
        # numpy.savetxt(sys.stdout, cMatrix_mod, '%4s', delimiter=",", header="Raw confusion matrix, no weight")

        with open(out_name, 'w') as f:
            f.write("<html><body><div>\n")
            f.write("</br><b>Report:</b></br></br>")
            f.write("<b>True</b> classes: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; %s  in columns" % (labels_true))
            f.write("\n")
            f.write("</br></br>")
            f.write("<b>Predicted</b> classes: %s  in rows" % (labels_pred ))
            f.write("\n")
            f.write("</br></br>")

            if weight not in ["", "-"]:
                f.write("<b>Confusion matrix, no weight</b>\n")
                # numpy.savetxt(f, cMatrix_noWeight_mod,  fmt='%4s', delimiter=",", header="Raw confusion matrix, no weight", newline='</br>')
                f.write(print_table_html(cMatrix_noWeight_mod))
                f.write("\n")
                f.write("</br>")
                f.write("<b>Overall accuracy: " + str("{:.2f}".format(accScore_noWeight * 100)) + "</b></br>\n")
                f.write("<b>Total samples: " + str(len(y_true)) + "</b></br></br>\n")
                f.write("</br></div><div></br>")
                f.write("<b>Confusion matrix, with weight</b>\n")
                f.write("</br>")
                # numpy.savetxt(f, cMatrix_mod, fmt='%4s', delimiter=",", header="Raw confusion matrix, with weight", newline='</br>')
                f.write(print_table_html(cMatrix_mod))
                f.write("</br>\n")
                f.write("<b>Overall accuracy: " + str("{:.2f}".format(accScore * 100)) + "</b></br>\n")
                f.write("<b>Total samples: " + str(len(y_true)) + "</b></br>\n")
                f.write("</br>")
            else:
                f.write("<b>Confusion matrix, with no weight</b>\n")
                f.write("\n")
                f.write("</br>")
                # numpy.savetxt(f, cMatrix_mod, fmt='%4s', delimiter=",", header="Raw confusion matrix, with weight", newline='</br>')
                f.write(print_table_html(cMatrix_mod))
                f.write("\n")
                f.write("</br></br>")
                f.write("<b>Overall accuracy: " + str("{:.2f}".format(accScore * 100)) + "</b></br>\n")
                f.write("<b>Total samples: " + str(len(y_true)) + "</b></br>\n")


            f.write("</div></body></html>")

        f.close()

        in_ds = None
        in_ds = ogr.GetDriverByName('ESRI Shapefile').Open(tmpShp)
        outShp = out_name.replace('.htm','_intersection.shp')
        ogr.GetDriverByName('ESRI Shapefile').CopyDataSource(in_ds, outShp)
        in_ds = None


        print('Done')
        if sys.platform.startswith('win'):
            os.system("start %s" %(out_name))

        shutil.rmtree(tmp_indir, ignore_errors=True)
        log.send_message(
            '<a target="_blank" href=getHtmlFile.py?file=' + out_name + '><b>View report result</b></a>')

    except Exception as e:
        log.send_error("Confusion Matrix error: " + str(e))
        print('General error:')
        print(e)

    log.close()
    sys.exit()



