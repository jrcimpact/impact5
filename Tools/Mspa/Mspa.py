#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json

import numpy
import time
import shutil
import subprocess
import gdal_calc
import scipy

# from pymorph import mmorph
# import mahotas
import skimage.morphology

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *
import IMPACT



if __name__ == '__main__':
    #inputs, out_suffix, proc_mode, band, overwrite
    #print "Content-type: text/html;\n"
    log = LogStore.LogProcessing('Mspa', 'mspa')

    infiles = sys.argv[1]
    band = int(sys.argv[2])
    proc_mode = sys.argv[3]
    out_suffix = sys.argv[4]

    mspa_nodata = int(sys.argv[5])
    mspa_background = int(sys.argv[6])
    mspa_foregroud = int(sys.argv[7])

    overwrite = sys.argv[8]

    mspa_edge = sys.argv[9]
    mspa_connectivity = sys.argv[10]
    mspa_intFlag = sys.argv[11]
    mspa_transition = sys.argv[12]


    if overwrite in ["yes", "Yes", "true", "True", "Y", "y"]:
        overwrite = True
    else:
        overwrite = False

    #-----------------------------------------------------
    #---------------  INIT SHAPEFILE ---------------------
    #-----------------------------------------------------

    try:

        log.set_parameter('Input Raster', infiles)
        log.set_parameter('Band',band)
        log.set_parameter('Save to new file', overwrite)
        log.set_parameter('Suffix', out_suffix)
        log.set_parameter('Processing Mode', proc_mode)

        tool_dir = os.path.dirname(os.path.realpath(__file__))


        try:
            infiles = json.loads(infiles)
        except Exception as e:
            infiles = infiles.split(' ')

        infile = str(infiles[0])
        log.send_message(infile)

        outfile = infile.replace('.tif', "_" + out_suffix.replace('.tif', '') + '.tif')



        if overwrite and os.path.exists(outfile):
                try:
                    os.remove(outfile)
                except:
                    time.sleep(5)
                    raise Exception("Error deleting output file " + outfile)

        if not overwrite and os.path.exists(outfile):
            log.send_warning("Output file exists; set Overwrite")
            log.close()
            sys.exit()

        log.send_message("Preparing file ")
        driver = gdal.GetDriverByName("GTiff")
        src_ds = gdal.Open(infile, 0)
        raster_bands = src_ds.RasterCount
        original_GeoTransform = src_ds.GetGeoTransform()
        original_Proj = src_ds.GetProjectionRef()
        src_ds = None

        if raster_bands < band:
            log.send_error("Input file does not have selected band")
            log.close()
            sys.exit()


        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_mspa")
        try:
            shutil.rmtree(tmp_indir, ignore_errors=True)
        except Exception:
            pass

        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)


        # write a tmp file with band and mspa classes
        temp_file = os.path.join(tmp_indir, 'temp_file.tif')


        expression = "(A*0)+(A=="+str(mspa_background)+")+(A=="+str(mspa_foregroud)+")*2"
        gdal_argv = ['myself']
        gdal_argv.append('--outfile')
        gdal_argv.append(temp_file)
        gdal_argv.append("--type=Byte")
        gdal_argv.append('-A')
        gdal_argv.append(infile)

        gdal_argv.append('--A_band=' + str(band))

        gdal_argv.append('--calc=' + expression)
        gdal_argv.append('--co')
        gdal_argv.append('COMPRESS=LZW')
        gdal_argv.append('--co')
        gdal_argv.append('BIGTIFF=IF_SAFER')
        gdal_argv.append('--debug')
        sys.argv = gdal_argv

        try:
            gdal_calc.main()
            if not os.path.exists(temp_file):
                raise Exception("failure in writing temporary file")
        except Exception as e:
            log.send_error("Expression error: " + str(e).replace("'", ""))
            log.close()
            try:
                shutil.rmtree(tmp_indir, ignore_errors=True)
            except Exception:
                pass
            sys.exit()

        if proc_mode in ['3','5','6']:
            driver = gdal.GetDriverByName("GTiff")
            src_ds = gdal.Open(temp_file, 0)
            src_band = src_ds.GetRasterBand(band)

            INPUT = src_band.ReadAsArray()

            kernel8 = numpy.ones((3,3))
            kernel4 = numpy.ones((3, 3))
            kernel4[0,0] = 0
            kernel4[0,2] = 0
            kernel4[2,0] = 0
            kernel4[2,2] = 0

            # ------------------------------------------
            # erosion
            print("Building Core")
            MASK = INPUT == 2
            # MASK = ((mahotas.morph.dilate(MASK, kernel8)) * (INPUT == 0)) + MASK
            MASK = ((skimage.morphology.binary_dilation(MASK, kernel8)) * (INPUT == 0)) + MASK

            # CORE = mahotas.morph.erode(MASK, kernel8)
            CORE = skimage.morphology.binary_erosion(MASK, kernel8)
            print("Building Core Close")
            # CLOSECORE = mahotas.morph.close_holes(CORE, kernel4)
            CLOSECORE = scipy.ndimage.binary_fill_holes(CORE, structure=kernel4).astype(bool)

            #HOLES = numpy.subtract(CLOSECORE,CORE)
            HOLES = numpy.uint8(CLOSECORE) - numpy.uint8(CORE)
            print("Building Boundaries")
            #BOUNDARIES = numpy.subtract(MASK , CORE)
            BOUNDARIES = numpy.uint8(MASK) - numpy.uint8(CORE)
            OUTCLASS = numpy.int8((HOLES * 100) + (CORE * 17) + (BOUNDARIES))
            print("Masking nodata")
            OUTCLASS[OUTCLASS == 101] = 1
            if proc_mode in ['5', '6']:
                #CORE_BOUND = numpy.subtract((mahotas.morph.dilate(CORE, kernel8) * MASK), CORE)
                CORE_BOUND = (numpy.uint8(skimage.morphology.binary_dilation(CORE, kernel8) * MASK) - numpy.uint8(CORE))
                PERFOR = ((skimage.morphology.binary_dilation(HOLES, kernel8)) * MASK) > 0
                EDGES = (numpy.uint8(CORE_BOUND) - numpy.uint8(PERFOR)) > 0

                BOUNDARIES = BOUNDARIES * PERFOR * EDGES
                OUTCLASS[PERFOR] = 5
                OUTCLASS[EDGES] = 3
                OUTCLASS[BOUNDARIES] = 1

                if proc_mode in ['6']:
                    print("MODE 6")
                    change = True
                    TMP = CORE
                    while change:
                        sum1 = numpy.sum(TMP)
                        TMP = skimage.morphology.binary_dilation(TMP, kernel8) * MASK
                        sum2 = numpy.sum(TMP)
                        if sum1 == sum2:
                            change = False

                    ISLET = (numpy.uint8(MASK) - numpy.uint8(TMP)) > 0
                    OUTCLASS[ISLET] = 9

            #
            OUTCLASS[CORE] = 17
            OUTCLASS[INPUT == 0] = 129

            print("Saving result")
            driver = gdal.GetDriverByName("GTiff")
            # time.sleep(3)
            dst_ds = driver.Create(outfile, src_ds.RasterXSize, src_ds.RasterYSize, 1, gdal.GDT_Byte,
                                   options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER', 'TILED=YES'])  # not working

            dst_ds.GetRasterBand(1).WriteArray(OUTCLASS)
            dst_ds.FlushCache()

            dst_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
            c = gdal.ColorTable()
            c.SetColorEntry(0, (220, 220, 220))
            c.SetColorEntry(1, (255, 140, 1))
            c.SetColorEntry(2, (192, 242, 255))
            c.SetColorEntry(3, (1, 1, 1))
            c.SetColorEntry(4, (50, 50, 50))
            c.SetColorEntry(5, (1, 1, 255))

            c.SetColorEntry(9, (160, 60, 1))

            c.SetColorEntry(17, (1, 200, 1))
            c.SetColorEntry(100, (136, 136, 136))
            c.SetColorEntry(101, (194, 194, 194))
            c.SetColorEntry(129, (255, 255, 255))

            # for cid in range(0, len(ctable)):
            #     c.SetColorEntry(cid, ctable[cid][1])

            dst_ds.GetRasterBand(1).SetColorTable(c)

            dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
            dst_ds.SetProjection(src_ds.GetProjectionRef())
            dst_ds.SetMetadata({'Impact_product_type': 'SPA class', 'Impact_operation': 'spa '+str(proc_mode)+' classes',
                                'Impact_version': IMPACT.get_version()})

            dst_ds = None
            log.send_message("Completed")
            log.close()

            try:
                shutil.rmtree(tmp_indir, ignore_errors=True)
            except Exception:
                pass

        # ------ CALL MSPA ------
        else:
            in_file = temp_file.replace('\\','/')
            out_basename = os.path.basename(outfile)

            eew = str(mspa_edge) # Effective Edge Width;
            graphfg = str(mspa_connectivity) #Foreground  connectivity;
            internal = str(mspa_intFlag) #Internal flag;
            transition = str(mspa_transition)
            odir = tmp_indir

            log.set_parameter('Effective Edge Width', eew)
            log.set_parameter('Foreground  connectivity', graphfg)
            log.set_parameter('Internal flag', internal)
            log.set_parameter('Transition', transition)


            if not odir.endswith('/'):
                odir = odir+'/'

            # print command
            if sys.platform != "linux":
                # copy Mspa.exe into temp to save temporary file there
                shutil.copy(os.path.join(tool_dir, 'mspa_win64.exe'), os.path.join(tmp_indir, 'mspa_win64.exe'))
                command = ['mspa_win64.exe', '-disk', '-i', in_file, '-o', out_basename, '-eew', eew, '-graphfg',
                           graphfg, '-internal', internal, '-odir', odir, '-transition', transition]
                call_out = subprocess.check_call(command, cwd=tmp_indir, shell=True)
            else:
                # copy Mspa into temp to save temporary file there
                shutil.copy(os.path.join(tool_dir, 'mspa_lin64'), os.path.join(tmp_indir, 'mspa_lin64'))
                os.system('chmod 775 ' + os.path.join(tmp_indir, 'mspa_lin64'))
                command = [os.path.join(tmp_indir, 'mspa_lin64'), '-disk', '-i', in_file, '-o', out_basename, '-eew', eew, '-graphfg', graphfg,
                           '-internal', internal, '-odir', odir, '-transition', transition]
                command = ' '.join(command)
                call_out = ''
                os.system(command)


            # out = subprocess.check_output(command,cwd=working_dir, shell=True)

            if os.path.exists(os.path.join(tmp_indir,out_basename)):
                driver = gdal.GetDriverByName("GTiff")
                src_ds = gdal.Open(os.path.join(tmp_indir,out_basename), 1)
                src_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
                c = gdal.ColorTable()

                c.SetColorEntry(0, (220, 220, 220))
                c.SetColorEntry(1, (255, 140, 1))
                c.SetColorEntry(3, (1, 1, 1))
                c.SetColorEntry(5, (1, 1, 255))
                c.SetColorEntry(9, (160, 60, 0))

                c.SetColorEntry(17, (1, 200, 1))
                c.SetColorEntry(33, (255, 0, 0))
                c.SetColorEntry(35, (255, 0, 0))
                c.SetColorEntry(37, (255, 0, 0))
                c.SetColorEntry(65, (255, 255, 0))
                c.SetColorEntry(67, (255, 255, 0))
                c.SetColorEntry(69, (255, 255, 0))
                c.SetColorEntry(100, (136, 136, 136))
                c.SetColorEntry(101, (255, 140, 1))
                c.SetColorEntry(103, (0, 0, 0))
                c.SetColorEntry(105, (0, 0, 255))
                c.SetColorEntry(109, (160, 60, 0))
                c.SetColorEntry(117, (0, 200, 0))
                c.SetColorEntry(129, (255, 255, 255))
                c.SetColorEntry(133, (255, 0, 0))
                c.SetColorEntry(135, (255, 0, 0))
                c.SetColorEntry(137, (255, 0, 0))
                c.SetColorEntry(165, (255, 255, 0))
                c.SetColorEntry(167, (255, 255, 0))
                c.SetColorEntry(169, (255, 255, 0))
                c.SetColorEntry(220, (194,194,194))


                if transition == '0':
                    c.SetColorEntry(67, (1, 1, 1))
                    c.SetColorEntry(167, (1, 1, 1))
                    c.SetColorEntry(35, (1, 1, 1))
                    c.SetColorEntry(135, (1, 1, 1))
                    c.SetColorEntry(69, (1, 1, 255))
                    c.SetColorEntry(169, (1, 1, 255))



                src_ds.GetRasterBand(1).SetColorTable(c)
                src_ds.SetGeoTransform(original_GeoTransform)
                src_ds.SetProjection(original_Proj)
                metadata = src_ds.GetMetadata()
                mspa_desc = str(metadata['TIFFTAG_IMAGEDESCRIPTION'])
                mspa_version = str(metadata['TIFFTAG_SOFTWARE'])

                src_ds.SetMetadata({'Impact_product_type': 'MSPA class', 'Impact_operation': 'mspa', 'Impact_version': IMPACT.get_version(), 'Using ': mspa_version, 'Description': mspa_desc })
                src_ds = None
                shutil.move(tmp_indir+'/'+out_basename, outfile)

            else:
                log.send_error("Failure in mspa ")
                log.send_message(call_out)


            log.send_message("Completed")
            log.close()

        try:
            shutil.rmtree(tmp_indir, ignore_errors=True)
        except Exception:
            pass


    except Exception as e:

        bandval = None
        src_ds = None
        src_band = None

        log.send_error("Processing error "+str(e).replace("'",''))
        log.send_error("Please check if file size is smaller then 10k x 10k pixels as required by MSAP")
        try:
            shutil.rmtree(tmp_indir, ignore_errors=True)
        except Exception:
            pass
    log.close()

