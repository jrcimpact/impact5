#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import subprocess
import glob
import json
import time
# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr, gdalconst
except ImportError:
    import ogr
    import gdal
    import osr
    import gdalconst

# Import local libraries
from __config__ import *
from Simonets_EO_libs import *
from Simonets_PROC_libs import RUN_populate_shapefile_with_class, test_overlapping
#from PiNo_classifier import *
import LogStore
import Settings
import ImageProcessing

sys.path.append(os.path.join(GLOBALS['Tools'], "Vector/"))
import VectorConversion

sys.path.append(os.path.join(GLOBALS['Tools'], "Raster/"))
import RasterCalculator

SETTINGS = Settings.load()
gdal.PushErrorHandler('CPLQuietErrorHandler')


def usage():
    print("Description")
    sys.exit()


def getArgs(args):

    img_list = None
    if (len(sys.argv) < 18):
        usage()
    else:
        update_shp = sys.argv[1]
        multiprocessing = sys.argv[2]
        img_list = json.loads(sys.argv[3])
        preclassify = sys.argv[4]
        single_data_img = sys.argv[5]
        isodata = sys.argv[6]
        BANDS = sys.argv[7]
        MMU = sys.argv[8]
        COMPACT = sys.argv[9]
        COLOR = sys.argv[10]
        similarity = sys.argv[11]
        suffix_name = sys.argv[12]
        strategy = sys.argv[13]
        tiling = sys.argv[14]
        legend = sys.argv[15]
        old_shp = sys.argv[16]
        old_attrib = sys.argv[17]

        if img_list is None:
            usage()

        else:
            return update_shp, multiprocessing, img_list, preclassify, single_data_img, isodata, BANDS, MMU, COMPACT, COLOR, similarity, suffix_name, strategy, tiling, legend, old_shp, old_attrib

#  return list of unique values in vector attribute
def returnUniqueValues(infile, class_field ):
    try:
        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataSource = driver.Open(infile, 0)
        uniqValues = []
        # uniqValues = list(set([feature.GetFieldAsString(class_field) for feature in layer]))
        sql = 'SELECT DISTINCT ' + class_field + ' FROM ' + os.path.basename(infile).replace('.shp', '')
        layer = dataSource.ExecuteSQL(sql)
        for i, feature in enumerate(layer):
            print('{0}: {1}'.format(i, feature.GetField(0)))
            uniqValues.append(feature.GetField(0))

    except Exception as e:
        # dataSource.Destroy()
        layer = None
        feature = None

    dataSource.ReleaseResultSet(layer)
    layer = None
    dataSource.Destroy()
    dataSource = None
    feature = None

    return uniqValues



# ------------------------------------------------------------------------------------------------
# -------------  segmentation caller -------------------------------------------------------------
# ------------------------------------------------------------------------------------------------


def RUN_INPE_segmentation(IN, OUT, BANDS, MMU, COLOR, COMPACT, similarity, tiling, strategy, log):
    try:
        IN = str(IN)
        OUT = str(OUT)
        BANDS = str(BANDS)
        MMU = str(MMU)
        COMPACT = str(COMPACT)
        COLOR = str(COLOR)
        similarity = str(similarity)
        tiling = str(tiling)
        if tiling in ["Y", "y", "yes", "Yes", "True", "true"]:
            tiling = 'true'
        else:
            tiling = 'false'

        shpdriver = ogr.GetDriverByName("ESRI Shapefile")

        print("Opening file: " + IN)

        datafile = gdal.Open(IN)
        print("Opening file: OK ")
        print(int(max(BANDS.replace('"', '').split(','))))
        print(int(datafile.RasterCount - 1))

        if (int(max(BANDS.split(','))) > int(datafile.RasterCount - 1)):
            print("Failure: please ensure input images have the selected bands")
            log.send_error("Wrong band selection :please ensure input images have the selected bands")
            return "Wrong band selection :please ensure input images have the selected bands"

        if datafile.RasterXSize * datafile.RasterYSize < 1000000:
            tiling = 'false'


        datafile = None

        # try again to clean tmp folder
        if os.path.exists(OUT):
            shpdriver.DeleteDataSource(OUT)

            # ------------------  CONSTANTS -----------------
        print("Running segmentation")

        if (GLOBALS['OS'] == "unix"):

            call_out = os.system("LD_LIBRARY_PATH=/home/lib /home/bin/segmentation_terralib5_standalone %s %s %s %s %s %s %s %s %s %s" % (IN, OUT, strategy, MMU, similarity, COLOR, COMPACT, BANDS, '0', tiling))
            if(call_out == 256):
                log.send_error("Segmentation Error: wrong parameters")
                return "Wrong parameters or error while calling segmenter"
        else:

            command = ['segmentation_terralib5_standalone.exe', IN, OUT, strategy, MMU, similarity, COLOR, COMPACT, BANDS, '0', tiling]
            working_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Libs')
            call_out = subprocess.check_call(command, cwd=working_dir, shell=True)


        #out = subprocess.check_output(command,cwd=working_dir, shell=True)
        log.send_message(call_out)

        try:
            if os.path.exists(OUT):

                ID = str(SETTINGS['vector_editing']['attributes']['ID'])

                log.send_message("Converting to vector")
                raster_ds = gdal.Open(OUT)
                srcband = raster_ds.GetRasterBand(1)
                geoinformation = raster_ds.GetGeoTransform()
                prj = raster_ds.GetProjection()
                srs = osr.SpatialReference(wkt=prj)
                dst_layername = os.path.basename(OUT).replace('.tif','')

                drv = ogr.GetDriverByName("ESRI Shapefile")
                vector_ds = drv.CreateDataSource(OUT.replace('.tif','_tmp.shp'))
                dst_layer = vector_ds.CreateLayer(dst_layername, srs=srs)
                dst_layer.CreateField(ogr.FieldDefn(ID, ogr.OFTInteger))  # 2 FIX using ID from settings
                gdal.Polygonize(srcband, None, dst_layer, 0, [], callback=None)
                vector_ds.Destroy()

                vector_ds = None
                raster_ds = None
                srcband = None
                log.send_message("Dissolving by ID")

                os.system('ogr2ogr ' + OUT.replace('.tif','.shp') + ' ' + OUT.replace('.tif','_tmp.shp') + ' -dialect sqlite -sql "SELECT ST_Union(geometry) AS geometry,'+ID+' FROM \'' + os.path.basename(OUT).replace('.tif','_tmp') + '\' GROUP BY ' + ID + '"')





        except Exception as e:
            # print res1
            log.send_error(str(e))
            vector_ds = None
            raster_ds = None
            srcband = None


    except Exception as e:
        log.send_error("Segmentation Error: "+ str(e))
        vector_ds = None
        raster_ds = None
        srcband = None



#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file 
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def Segmentation(update_shp, multiprocessing, img_list, preclassify, single_data_img, isodata, BANDS, MMU, COMPACT, COLOR, similarity, suffix_name, strategy, tiling, AggregationRules, old_shp, old_attrib):


    # initialize output log
    log = LogStore.LogProcessing('Segmentation', 'segmentation')
    try:
        images = []
        classifications = []
        for item in img_list:
            print(item)
            images.append(item['input'])
            classifications.append(item['classification'])

    except Exception as e:
        print(str(e))
        log.send_error("Error "+str(e).replace("'",''))
        log.close()
        sys.exit()

    if old_shp != '':
        old_shp = json.loads(old_shp)
        old_shp = old_shp[0]
        log.set_input_file("---- OLD MAP -------")
        log.set_input_file(old_shp)
        log.set_parameter('Old vector attribute', old_attrib)
        single_data_img = 'No'


    log.set_parameter('Overwrite output', update_shp)
    log.set_input_file(images)
    log.set_input_file("-----------")
    log.set_input_file(classifications)
    log.set_parameter('Overwrite output', update_shp)
    log.set_parameter('Single date segmentation', single_data_img)
    log.set_parameter('Bands', BANDS+" starting from 0")
    log.set_parameter('MMU', MMU)
    log.set_parameter('Compactness', COMPACT)
    log.set_parameter('Color', COLOR)
    log.set_parameter('Similarity', similarity)
    log.set_parameter('Suffix name', suffix_name)
    log.set_parameter('Strategy', strategy)
    log.set_parameter('Use tiling', tiling)
    log.set_parameter('Stats used for pre-classification', AggregationRules)


    #ImageProcessing.validPathLengths(img_list, None,len(suffix_name)+10, log)

    #------------------  CONSTANTS ---------------------------
    # class_extension='_class.tif'
    # cluster_extension='_cluster.tif'
    # PBS_extension= '_PBS.tif'
    # #AggregationRules=[AggregationRules]  # contains the keywords representing the aggregation rules to be used when filling the shapefile ['PINO','CLUSTER','PBS']
    shpdriver = ogr.GetDriverByName("ESRI Shapefile")

    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "segmentation")
    # ------------------  CONSTANTS ---------------------------
    # invert similarity
    similarity = 1.0 - float(similarity)

    # ------------------  CLEAN PROCESSING FOLDER -----------------
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)

    # ------------------------------------------------------------------------------------------------------------------
    # ------------------- SINGLE DATE ----------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    if single_data_img == 'Yes':
        log.send_message("SINGLE DATE segmentation")
        for index, IN in enumerate(images):
            # ----------------------------------------------------------------------------
            # delete temporary files after each execution
            # ----------------------------------------------------------------------------
            try:
                r = glob.glob(TMPDIR + "/*.*")
                for i in r:
                    os.remove(i)
            except:
                log.send_warning("Error deleting " + i)

            img_extension = '.tif'

            if suffix_name == '""' or len(suffix_name) == 0:
                OUT=IN.replace(img_extension,'')
            else:
                suffix_name=suffix_name.replace('"','')
                OUT=IN.replace(img_extension,'')+'_'+suffix_name

            log.send_message("Output: " + OUT + '.shp')

            if (os.path.exists(OUT+'.shp') & (update_shp == 'Yes')):
                log.send_message("Deleting output")
                shpdriver.DeleteDataSource(OUT+'.shp')

            if (os.path.exists(OUT+'.shp') & (update_shp == 'No')):
                log.send_warning(OUT+'.shp file <b>Already Processed </b>')
                continue


            TmpOutRaster = os.path.join(TMPDIR, os.path.basename(OUT) + '.tif')
            TmpOutVector = TmpOutRaster.replace('.tif', '.shp')

            #-----------------------------  CAL EXE file --------------------------------------------------
            try:
                RUN_INPE_segmentation(IN,TmpOutRaster,BANDS,MMU,COLOR,COMPACT,similarity,tiling,strategy,log)
            except Exception as e:
                log.send_error("Unexpected error: "+str(e).replace("'",''))
                continue
            #-----------------------------------------------------------------------------------------------
            CLASS = classifications[index]
            if os.path.exists(TmpOutVector):
                if CLASS != '':
                    if not os.path.exists(CLASS):
                        log.send_error("Classification file " + CLASS + 'does not exists')
                        continue

                    log.send_message("Populating shapefile")
                    if not test_overlapping(TmpOutVector, CLASS):
                       log.send_warning("Input raster " + CLASS + " does not intersect the reference raster")
                    else:
                        RUN_populate_shapefile_with_class(CLASS,TmpOutVector,AggregationRules)

                print("Moving")
                TmpOutVector=TmpOutVector.replace('.shp','')
                for ext in ['.shp','.shx','.dbf','.prj']:
                    os.rename(TmpOutVector + ext, OUT + ext)

                log.send_message("Completed")

            else:
                log.send_error("Unexpected error: no output")

    # ------------------------------------------------------------------------------------------------------------------
    # ------------------- MULTI  DATE ----------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    if (single_data_img == 'No'):
        log.send_message("MULTIDATE segmentation")
        # ----------------------------------------------------------------------------
        # delete temporary files
        # ----------------------------------------------------------------------------
        try:
            r = glob.glob(TMPDIR + "/*.*")
            for i in r:
                os.remove(i)
        except:
            log.send_warning("Error deleting " + i)

        # test if images are intersecting old segmentation
        if old_shp != '':
            for IN in images:
                if (test_overlapping(old_shp, IN)):
                    continue
                else:
                    log.send_error("Input raster " + IN + " does not intersect the old map")
                    log.close()
                    sys.exit()



        # loop input imgs to find the first valid sat image to be used as master (proj, name and directory )
        IN = images[0]
        #IN=IN.replace('"','')
        OUTDIR = os.path.dirname(IN)
        suffix_name = suffix_name.replace('"','').replace('.shp','')
        OUTvector = os.path.join(OUTDIR,'Multidate_segmentation_'+suffix_name)
        TmpOutRaster = os.path.join(TMPDIR, 'Multidate_segmentation_'+suffix_name)
        TmpOutVector = TmpOutRaster+'.shp'

        log.send_message("Preparing report file")
        text_file = open(OUTvector+'.txt', "w")

        TmpOutClass = os.path.join(TMPDIR,"merge_class.vrt")
        #INplace=os.path.join(indir, IN)
        mybands = BANDS.split(",")
        log.send_message("Bands number:"+str(len(mybands)))
        log.send_message("Original bands:"+str(BANDS))

        if (os.path.exists(OUTvector+'.shp') & (update_shp == "No")):
            text_file.close()
            log.send_warning(OUTvector+'.shp'+' file <b>Already Processed </b>')
            log.close()
            sys.exit()

        if (os.path.exists(OUTvector+'.shp') & (update_shp == "Yes")):
            log.send_message("Deleting output")
            try:
                shpdriver.DeleteDataSource(OUTvector+'.shp')
            except Exception as e:
                text_file.close()
                log.send_warning("Error deleting file: "+OUTvector+'.shp '+str(e))
                log.close()
                sys.exit()


        ct=0
        num_img=0

        for IN in images:
            print(IN)
            datafile = gdal.Open(IN)
            if ( int(max(mybands)) > int(datafile.RasterCount-1)  ) :
                print("Failure: please ensure input images have the selected bands")
                log.send_warning("Wrong band selection :SET "+str(BANDS)+" while file has "+str(datafile.RasterCount)+" bands.")
                log.send_warning("Skipping file "+IN)
                datafile = None
                continue

            if (ct==0):
                REFIMG=IN
                text_file.write("Master Image: %s" % IN+'\n')
                master_PRJ = datafile.GetProjectionRef()
                master_resolutionsX = float(datafile.GetGeoTransform()[1])
                master_resolutionsY = abs(float(datafile.GetGeoTransform()[5]))

                for z in range(len(mybands)):
                    res1 = os.system('gdal_translate -of GTiff "'+IN+'" "'+TMPDIR+'/tmp_b'+str(ct)+'.tif" -b '+ str(int(mybands[z])+1))
                    ct = ct+1	 # increase image counter
                num_img+=1
            else:
                if (test_overlapping(IN,REFIMG)):
                    text_file.write("Other image: %s" % IN+'\n')
                    for z in range(len(mybands)):
                        res1 = os.system('gdal_translate -of VRT "'+IN+'" "'+TMPDIR+'/tmp_b'+str(ct)+'.tmp" -b '+ str(int(mybands[z])+1))
                        gdal.Warp(TMPDIR+'/tmp_b'+str(ct)+'.tif', TMPDIR+'/tmp_b'+str(ct)+'.tmp', dstSRS=master_PRJ, format='Gtiff',xRes=master_resolutionsX, yRes=master_resolutionsY, targetAlignedPixels=True, options=['compress=lzw', 'bigtiff=IF_SAFER','multi'])
                        ct = ct+1	 # increase image counter
                    num_img+=1

                else:
                    text_file.write("No intersection: %s" % IN+'\n')
                    log.send_warning("No intersection: between MASTER: "+REFIMG+" and " +IN)

        if num_img == 0:
            text_file.close()
            log.send_error("No images")
            log.close()
            sys.exit()
        else:
            log.send_message("Found "+str(num_img)+" intersecting images")

        datafile = None
        res1 = os.system('gdalbuildvrt -separate "'+TmpOutRaster+'.vrt" "'+TMPDIR+'/tmp_b*.tif"')

        BANDS=''
        for i in range(num_img*len(mybands)):
            BANDS=BANDS+str(i)+','

        BANDS=BANDS[:-1]
        log.send_message("Stack Bands:"+str(BANDS))

        # ------------------------------------------------------------------------------------------------
        # --------------- Original Multidate Segmentation  - NO import of old classif --------------------
        # ------------------------------------------------------------------------------------------------
        if old_shp == '':
            #-----------------------------  CAL EXE file --------------------------------------------------
            try:
                log.send_message("calling segmenter")
                print('HERE --------------'
                      )
                res = RUN_INPE_segmentation(TmpOutRaster+'.vrt', TmpOutRaster+'.tif', BANDS, MMU, COLOR, COMPACT, similarity, tiling, strategy, log)
                log.send_message("Details saved into "+TmpOutRaster+".txt")

            except Exception as e:
                #----------------------------------------------------------------------------
                # delete temporary files
                #----------------------------------------------------------------------------
                print("ERROR running segmentation")
                log.send_error("ERROR running segmentation:"+str(e).replace("'",''))
                text_file.close()
                log.close()
                sys.exit()
                #-----------------------------------------------------------------------------------------------


        #  -----------------------------------------------------------------------------------------------
        #  ----------------Import old segmentation and mask input rasters by using each class ------------
        #  ----------------Segment and fill the new poly with old class + new                 ------------
        #  -----------------------------------------------------------------------------------------------
        else:

            uniqValues = returnUniqueValues(old_shp, old_attrib)
            print('Unique val')
            print(uniqValues)

            nodata = 0

            if (0 in uniqValues) or ('0' in uniqValues ):
                possibleNodata = [9999,999,255,254,200,100,99]
                for v in possibleNodata:
                    if (v not in uniqValues) and (str(v) not in uniqValues):
                        nodata = v
                        break

            print('TmpOutRaster')
            print(TmpOutRaster)

            #  use the VRT containing the stack of rasters and bands
            old_dissolve_tif = VectorConversion.run_VectorConversion('rasterized', [old_shp], TmpOutRaster+'.vrt',
                                                                     old_attrib, 'GDT_UInt16', 'No', 'Yes', nodata, ExternalLogFile=log)

            print('old_dissolve_tif')
            print(old_dissolve_tif)
            if not os.path.exists(old_dissolve_tif):
                log.send_error("Error while dissolving features")
                text_file.close()
                log.close()
                sys.exit()


            # loop all classes and run the segmentation
            for val in uniqValues:

                raster_out_name = TmpOutRaster + '_class_' + str(val)
                img_name_bands = [{"label": "A", "path": TmpOutRaster+'.vrt', "band": "All"},
                                  {"label": "B", "path": old_dissolve_tif, "band": 1}]
                expression = 'A*(B==' + str(val) + ')'
                res = RasterCalculator.run_RasterCalculator(raster_out_name, img_name_bands, nodata, expression, 'UInt16',
                                                            'True', 'True', ExternalLogFile=log)
                raster_out_name = raster_out_name + '.tif'

                # #  mask input image
                # mask_out_name = TmpOutRaster + '_mask_' + str(val)
                # img_name_bands = [{"label": "A", "path": old_dissolve_tif, "band": 1}]
                # expression = 'A*(A==' + str(val) + ')'
                # res = RasterCalculator.run_RasterCalculator(mask_out_name, img_name_bands, nodata, expression, 'UInt16',
                #                                             'True', 'True', ExternalLogFile=log)
                # mask_out_name = mask_out_name + '.tif'
                res = RUN_INPE_segmentation(raster_out_name, raster_out_name.replace('.tif','_seg.tif'), BANDS, MMU, COLOR, COMPACT,
                                            similarity, tiling, strategy, log)
                seg_out_name = raster_out_name.replace('.tif','_seg.shp')
                if os.path.exists(seg_out_name):

                #  use the original rasterized layer 2x to fill T1_class and T2_class
                    res1 = os.system('gdalbuildvrt -separate -resolution highest "' + TmpOutClass + '" ' + old_dissolve_tif + ' ' + old_dissolve_tif)

                    RUN_populate_shapefile_with_class(TmpOutClass, seg_out_name, AggregationRules)

                    #     select only polygons with correct values

                    clean_out_name = seg_out_name.replace('.shp', '_clean.shp')
                    os.system('ogr2ogr -sql "SELECT * from ' + os.path.basename(seg_out_name).replace('.shp','') + ' WHERE ' + str(SETTINGS['vector_editing']['attributes']['class_t1']) + '=\'' + str(val) + '\' " ' + clean_out_name + ' ' + seg_out_name)
                    print('Class '+ str(val) +' DONE -----------------')
                else:
                    log.send_error("Error while segmenting class " + str(val))
                    log.close()
                    sys.exit()

            # ----------------------------- MERGE individual segmentations -----------------------------------------
            ct = 0
            shps = glob.glob(TMPDIR + "/*_clean.shp")
            for f in shps:
                print(f)
                # out_name = os.path.join(TMPDIR,'Merged.shp')
                if ct == 0:
                    os.system('ogr2ogr ' + TmpOutVector + ' ' + f )
                else:
                    os.system('ogr2ogr -append -update ' + TmpOutVector + ' ' + f)

                ct+=1

        # ----------------------------- Prepare old and new classification stacked file for the Populate
        if old_shp == '':
            CLASS = []
        else:
            CLASS = [old_dissolve_tif, old_dissolve_tif]

        for cl in classifications:
            if cl != '':
                if os.path.exists(cl):
                    CLASS.append(cl)
                else:
                    log.send_error("Classification "+str(cl)+" does not exists")


        text_file.write("Classification order in DBF :"+'\n')
        INPUT_class = ''
        for cl in CLASS:
            text_file.write("Classification: %s" % cl+'\n')
            cl_out = TMPDIR + '/' + os.path.basename(cl) + '.vrt'
            gdal.Warp(cl_out, cl, dstSRS=master_PRJ,
                      format='VRT', xRes=master_resolutionsX, yRes=master_resolutionsY, targetAlignedPixels=True)
            INPUT_class = INPUT_class + '"' + cl_out + '" '
        text_file.close()

        if INPUT_class != '':
            print(INPUT_class)
            res1 = os.system('gdalbuildvrt -separate "'+TmpOutClass+'" '+INPUT_class)


        if os.path.exists(TmpOutVector):
            log.send_message("Populating shapefile")
            RUN_populate_shapefile_with_class(TmpOutClass, TmpOutVector, AggregationRules)
        else:

            log.send_error("Error while filling the segmentation ")
            log.close()
            return

        print("Moving")
        TmpOutVector = TmpOutVector.replace('.shp', '')
        for ext in ['.shp', '.shx', '.dbf', '.prj']:
            os.rename(TmpOutVector + ext, OUTvector + ext)
        log.send_message("Completed")
    # ----------------------------- END -----------------------------------------

    log.close()

    #----------------------------------------------------------------------------
    # delete temporary files
    #----------------------------------------------------------------------------
    try:
        r = glob.glob(TMPDIR+"/*.*")
        for i in r:
            os.remove(i)
    except Exception as e:

        print(str(e))
        pass

    return 0

if __name__ == "__main__":

    update_shp, multiprocessing, img_list, preclassify, single_data_img, isodata, BANDS, MMU, COMPACT, COLOR, similarity, suffix_name, strategy, tiling, legend, old_shp, old_attrib = getArgs(sys.argv)
    res=''
    try:
        res = Segmentation(update_shp, multiprocessing, img_list, preclassify, single_data_img, isodata, BANDS, MMU, COMPACT, COLOR, similarity, suffix_name, strategy, tiling, legend, old_shp, old_attrib)
    except Exception as e:
        print("Error " + str(e))


