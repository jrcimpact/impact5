BIN file
tar.gz file containing binary code and libs is located under IMPACT4/Libs/Unix/build-docker/impact
Docker-compose is extracting and creating symlink to /home/


MAKE FROM SOURCE

Download TerraView-5.6.1-Ubuntu-18.04.tar.gz
cp libjpeg* png* pcr*  from TerraView-5.6.1-Ubuntu-18.04.tar.gz to /home/lib/

# copy Lib from Terraview to baatz/ (keep only necessary one)
# copy Share from Terraview to baatz/bin/ (keep only necessary one)

CMAKE FILE 

SET correct path for VAR 
set(TERRALIB_INCLUDE_DIR "/home/Downloads/TerraView561/include")
set(TERRALIB_BUILD_DIR "/home")

# copy Lib from Terraview to baatz/ (keep only necessary one)
set(TERRALIB_LIB_DIR "/home/lib")

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING="Release" -DCMAKE_INSTALL_PREFIX:PATH="/home" -DCMAKE_PREFIX_PATH:PATH="/home/lib" ../cmake
make
make install

RUN 
LD_LIBRARY_PATH=/home/lib /home/bin/segmentation_terralib5_standalone IN.tif OUT.tif baatz 500 0.9 0.5 0.5 0.8 0 true

Following Link to reuse system var and limit the size of the distributable pkg
RUN ln -s /usr/lib/x86_64-linux-gnu/libcurl-gnutls.so.4 /home/lib/libcurl.so.4
RUN ln -s /usr/lib/libgdal.so.26.0.4 /home/lib/libgdal.so.27
RUN ln -s /usr/lib/x86_64-linux-gnu/libgeos-3.8.0.so /home/lib/libgeos-3.8.1.so



















