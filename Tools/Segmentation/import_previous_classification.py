#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os

import glob
import json
import time
# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr, gdalconst
except ImportError:
    import ogr
    import gdal
    import osr
    import gdalconst

# Import local libraries
from __config__ import *
from Simonets_EO_libs import *
from Simonets_PROC_libs import RUN_populate_shapefile_with_class, test_overlapping


sys.path.append(os.path.join(GLOBALS['Tools'], "Vector/"))
import VectorConversion

sys.path.append(os.path.join(GLOBALS['Tools'], "Raster/"))
import RasterCalculator

import Segmentation

import LogStore
import Settings





SETTINGS = Settings.load()
gdal.PushErrorHandler('CPLQuietErrorHandler')


def usage():
    print("Description")
    sys.exit()


def getArgs(args):

    img_list = None
    if (len(sys.argv) < 6):
        usage()
    else:

        old_classification = sys.argv[1]
        class_field = sys.argv[2]
        new_raster = sys.argv[3]
        suffix_name = sys.argv[4]
        overwrite = sys.argv[5]


        return old_classification, class_field, new_raster, suffix_name, overwrite



#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input file
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def returnUniqueValues(infile, class_field ):
    try:
        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataSource = driver.Open(infile, 0)
        uniqValues = []
        # uniqValues = list(set([feature.GetFieldAsString(class_field) for feature in layer]))
        sql = 'SELECT DISTINCT ' + class_field + ' FROM ' + os.path.basename(old_classification).replace('.shp', '')
        layer = dataSource.ExecuteSQL(sql)
        for i, feature in enumerate(layer):
            print('{0}: {1}'.format(i, feature.GetField(0)))
            uniqValues.append(feature.GetField(0))
    except Exception as e:
        # dataSource.Destroy()
        layer = None
        feature = None
    dataSource.ReleaseResultSet(layer)
    layer = None
    dataSource.Destroy()
    dataSource = None
    feature = None

    # uniqValues = ['1', '10', '14', '12', '11', '13', '0', '9', '7']

    return uniqValues


def merge_classification(old_classification, class_field, new_raster, suffix_name, overwrite, AggregationRules, recent_class):


    # initialize output log
    log = LogStore.LogProcessing('Intersect', 'intersect')
    try:
        print("")

    except Exception as e:
        print(str(e))
        log.send_error("Error "+str(e).replace("'",''))
        log.close()
        sys.exit()

    log.set_input_file(old_classification)
    log.set_input_file("-----------")
    log.set_input_file(new_raster)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Class Field', class_field)
    log.set_parameter('Suffix name', suffix_name)


    shpdriver = ogr.GetDriverByName("ESRI Shapefile")
    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "intersection")

    # ------------------  CLEAN PROCESSING FOLDER -----------------
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)

    # ------------------------------------------------------------------------------------------------------------------
    # ------------------- Load old ----------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    if old_classification.endswith('.tif'):
        print("TO BE DEV ")

    if old_classification.endswith('.shp'):
        log.send_message("Importing old class from vector file")

        try:
            r = glob.glob(TMPDIR + "/*.*")
            for i in r:
                os.remove(i)

        except Exception as e:
            log.send_warning("Error deleting " + i)
            log.close()
            sys.exit()

        if not test_overlapping(old_classification, new_raster):
                log.send_error("No overlap between: "+old_classification+" "+new_raster)
                log.close()
                return
        print("Overlap OK")


        suffix_name=suffix_name.replace('"','')
        OUT = old_classification.replace('.shp','')+'_'+suffix_name+'.shp'
        log.send_message("Output: " + OUT )

        if (os.path.exists(OUT+'.shp') & (overwrite == 'No')):
            log.send_warning(OUT+'.shp file <b>Already Processed </b>')
            log.close()
            sys.exit()

        if (os.path.exists(OUT) & (overwrite == 'Yes')):
            log.send_message("Deleting output")
            shpdriver.DeleteDataSource(OUT)



        #old_dissolve_tif = 'D:/IMPACT5/IMPACT/DATA/HotSpot/Multidate_segmentation_brazil_rasterized.tif'
        #old_dissolve_tif = ''
        old_dissolve_tif = VectorConversion.run_VectorConversion('rasterized', [old_classification], new_raster, class_field, 'GDT_UInt16', 'No', 'Yes')
        # print(old_dissolve_tif)
        if not os.path.exists(old_dissolve_tif):
            log.send_error("Error while dissolving features")
            log.close()
            sys.exit()

        uniqValues = returnUniqueValues(old_classification, class_field)

        # loop all classes and run the segmentation
        for val in uniqValues:
            #  mask input image
            raster_out_name = os.path.join(TMPDIR,'class_'+str(val))
            img_name_bands = [{"label":"A","path":new_raster,"band":"All"},{"label":"B","path":old_dissolve_tif,"band":1}]
            expression = 'A*(B=='+str(val)+')'
            res = RasterCalculator.run_RasterCalculator(raster_out_name, img_name_bands, 0, expression, 'UInt16', 'True', 'True')
            raster_out_name = raster_out_name+'.tif'

            #  mask input image
            mask_out_name = os.path.join(TMPDIR,'mask_'+str(val))
            img_name_bands = [{"label":"A","path":old_dissolve_tif,"band":1}]
            expression = 'A*(A=='+str(val)+')'
            res = RasterCalculator.run_RasterCalculator(mask_out_name, img_name_bands, 0, expression, 'UInt16', 'True', 'True')
            mask_out_name = mask_out_name+'.tif'

            img_class = [{"input":raster_out_name, "classification":'', "classification_band": 1}]
            single_data_img = 'Yes'
            BANDS = '5,4,3'
            MMU = 500
            COMPACT = 0.8
            COLOR = 0.9
            similarity = 0.9
            suffix_name = 'raw_seg'
            strategy = 'baatz'
            tiling = 'Yes'
            seg_out_name = raster_out_name.replace('.tif','_'+suffix_name+'.shp')
            print('----------------')
            print(seg_out_name)
            try:
                res = Segmentation.Segmentation(True, '', img_class, '', single_data_img, '', BANDS, MMU, COMPACT, COLOR, similarity, suffix_name, strategy, tiling, '')
            except Exception as e:
                print('Error in segmentation: try to reduce the MMU value')

            if os.path.exists(seg_out_name):

                log.send_message("Populating shapefile")
                # prepare onld and new classification stacked file for the Populate
                TmpOutClass = os.path.join(TMPDIR, 'Merged_classif_'+str(val)+'.vrt')
                if recent_class != '':
                    res1 = os.system(
                        'gdalbuildvrt -separate -resolution highest "' + TmpOutClass + '" ' + mask_out_name + ' ' + recent_class)
                else:
                    res1 = os.system(
                        'gdalbuildvrt -separate -resolution highest "' + TmpOutClass + '" ' + mask_out_name + ' ' + mask_out_name)
                RUN_populate_shapefile_with_class(TmpOutClass, seg_out_name, AggregationRules)

            #     select only polygons with correct values
                print('OK')
                clean_out_name = raster_out_name.replace('.tif','_clean.shp')
                print(os.path.basename(seg_out_name).replace('.shp',''))
                print(clean_out_name)
                print(seg_out_name)
                print('ogr2ogr -sql "SELECT * from ' + os.path.basename(seg_out_name).replace('.shp','') +' WHERE '+str(SETTINGS['vector_editing']['attributes']['class_t1'])+ '=\'' + str(val) +'\' " '+clean_out_name + ' '+ seg_out_name)
                os.system('ogr2ogr -sql "SELECT * from ' + os.path.basename(seg_out_name).replace('.shp','') +' WHERE '+str(SETTINGS['vector_editing']['attributes']['class_t1'])+ '=\'' + str(val) +'\' " '+clean_out_name + ' '+ seg_out_name)
                print('------------   DONE -----------------')
            else:
                print('To be done ... Take orig shp and copy poly ')

        ct = 0
        shps = glob.glob(TMPDIR + "/*_clean.shp")
        for f in shps:
            print(f)
            out_name = os.path.join(TMPDIR,'Merged.shp')
            if ct == 0:
                os.system('ogr2ogr ' + out_name + ' ' + f )
            else:
                os.system('ogr2ogr -append -update ' + out_name + ' ' + f)

            ct+=1

        # prepare old and new classification stacked file for the Populate
        TmpOutClass = os.path.join(TMPDIR,'Merged_classif.vrt')
        if recent_class != '':
            res1 = os.system('gdalbuildvrt -separate -resolution highest "' + TmpOutClass + '" ' + old_dissolve_tif + ' ' + recent_class)
        else:
            res1 = os.system('gdalbuildvrt -separate -resolution highest "' + TmpOutClass + '" ' + old_dissolve_tif + ' ' + old_dissolve_tif)


        if os.path.exists(out_name):
            if out_name != '':
                log.send_message("Populating shapefile")

                RUN_populate_shapefile_with_class(TmpOutClass, out_name, AggregationRules)
        else:
            log.send_error("Error while merging individual segmentation ")
            log.close()
            return





        log.send_message("Completed")


    log.close()

    #----------------------------------------------------------------------------
    # delete temporary files
    #----------------------------------------------------------------------------
    # try:
    #     r = glob.glob(TMPDIR+"/*.*")
    #     for i in r:
    #         print("del tmp file")
    #         os.remove(i)
    # except Exception as e:
    #     print("Error "+str(e))
    #     pass

    return 0





if __name__ == "__main__":

    #old_classification, class_field, new_classification, suffix_name = getArgs(sys.argv)
    res=''
    try:
        old_classification = 'D:/IMPACT5/IMPACT/DATA/HotSpot/Multidate_segmentation_brazil.shp'
        old_classification = 'D:/IMPACT5/IMPACT/DATA/HotSpot/virunga_test/CAF02_vector_2014_test.shp'
        class_field = 'T1_class'
        class_field = 'map_code'
        new_raster = 'D:/IMPACT5/IMPACT/DATA/HotSpot/virunga_test/virunga_test_S2A_MSIL2A_20230107T081321_N0509_R078_T35MRV_20230107T111855.tif'
        suffix_name = 'new_segm'
        overwrite = "Yes"
        AggregationRules = 'Majority'
        recent_class = ''
        res = merge_classification(old_classification, class_field, new_raster, suffix_name, overwrite, AggregationRules , recent_class)

    except Exception as e:
        print("Error "+str(e))


    print('All done')



