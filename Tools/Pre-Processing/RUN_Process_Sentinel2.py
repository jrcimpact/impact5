#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import json

# Import local libraries
from __config__ import *
import LogStore
from Process_Sentinel2 import *
import ImageProcessing


def usage():
    print()
    print('Author: Simonetti Dario for European Commission  2015')
    print('Wrapper for Satellite data Pre processing ---> RAW ZIP Landsat data')
    print('RUN_Process_Sentinel2.py overwrite output_directory images')
    print()
    sys.exit(0)


def getArgs(args):

    infiles = None

    if (len(sys.argv) < 10):
        usage()
    else:
        overwrite=sys.argv[1]
        outdir=sys.argv[2]
        bands=sys.argv[3]
        projection=sys.argv[4]
        resolution=sys.argv[5]
        outtype=sys.argv[6]
        aoi=sys.argv[7]
        outname=sys.argv[8]
        infiles=json.loads(sys.argv[9])
        if ( infiles is None ):
            usage()

        else:
            return overwrite,outdir,bands,projection,resolution,outtype,aoi,outname,infiles


# ----------------------------------------------------------------------------------------------------------------	
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input DIR and process file according to flags (2 implement)
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    overwrite,outdir,bands,projection,resolution,outtype,aoi,outname,infiles = getArgs(sys.argv)

    # initialize output log
    log = LogStore.LogProcessing('Zip to TOARefl', 'sentinel2_to_toa')

    log.set_input_file(infiles)
    log.set_parameter('Overwrite output', overwrite)
    log.set_parameter('Output directory', outdir)
    log.set_parameter('Selected bands', bands)
    log.set_parameter('Selected projection', projection)
    log.set_parameter('Selected resolution', resolution)
    log.set_parameter('Selected type', outtype)
    log.set_parameter('Selected AOI', aoi)
    log.set_parameter('Selected output name format', outname)

    ImageProcessing.validPathLengths(infiles, outdir, len(aoi)+15, log)

    tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_sentinel")

    try:

        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)

        bands = bands.split(",")
        print(aoi)
        if not aoi == '[]':
            aoi=json.loads(aoi)
            print(aoi)
            aoi = aoi[0]

        for IN in infiles:
            try:
                xx = clean_tmp_file(tmp_indir)
                if not os.path.exists(IN):
                    log.send_error("File does not exist; please reload the processing tool.")
                    continue

                fname = os.path.basename(IN)
                newband = []
                for item in bands:
                    if 'MSIL2A' in fname:
                        if item == 'B01':
                            item = 'AOT_20m'
                        if item == 'B09':
                            item = 'WVP_20m'
                        if item == 'B10':
                            item = 'CLD_20m'
                        if item in ['B02', 'B03','B04','B08']:
                            item = item+'_10m'
                        if item in ['B05', 'B06','B07','B8A','B11','B12']:
                            item = item+'_20m'
                        newband.append("_" + item + ".jp2")
                    else:
                        newband.append("_" + item + ".jp2")

                log.send_message("Processing "+IN)

                res = process_sentinel2_file(overwrite, outdir, newband, projection, resolution, outtype, aoi, IN, outname, tmp_indir, log)
                xx = clean_tmp_file(tmp_indir)

            except Exception as e:
                log.send_error(str(e))

        try:
            clean_tmp_file(tmp_indir)
            os.rmdir(tmp_indir)
        except Exception as e:
            log.send_error("Error cleaning tmp folder:"+str(e))

    except Exception as e:
        log.send_error(e)

    log.close()

