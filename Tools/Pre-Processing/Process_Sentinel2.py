#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import shutil
import zipfile
import glob
import math
from collections import Counter

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

import gdal_calc

# Import local libraries
from __config__ import *
import IMPACT
from FileSystem import list_files
from Simonets_PROC_libs import test_overlapping

import subprocess
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            MAIN CODE : from zip file to layerstaked
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def process_sentinel2_file(overwrite, outdir, bands, projection, resolution, outtype, aoi, file, outname, tmp_indir, log):

    try:
            print("Processing " + file)

            user_projection=projection
            filename=os.path.basename(file).replace('.zip','').replace('.Zip','')
            USGS = False
            JP2butNoINTERSECT = False

            if filename.startswith('L1C_T'):
                USGS = True
                with zipfile.ZipFile(file) as z:
                    for name in z.namelist():
                        if name.endswith('.SAFE/MTD_MSIL1C.xml'):
                            filename = name.split('.')[0]
                            metadata_name = filename.replace('MSIL1C','MTD_MSIL1C')+'.xml'

                            print('USGS format:')
                            print('New name: ' + filename)
                            print('New metadata: ' + metadata_name)
                            break



            try:
                if outname=="Short" and not USGS:
                    print(filename)
                    tmp=filename.split("_")
                    tmparr=(tmp[0],tmp[3],tmp[5],tmp[6],tmp[7])
                    filename='_'.join(tmparr)
                    metadata_name=tmp[0]+'_MTD_'+tmp[5]+'_'+tmp[6]+'_'+tmp[7]+'.xml'
            except:
                outname=="Original"
                metadata_name = 'MTD_MSIL1C.xml'
                print("Input name might be already in short mode")

            if not aoi == '' and os.path.isfile(aoi):
                print('Using AOI')
                out_name=os.path.basename(aoi).replace('.shp','')+"_"+os.path.basename(filename)
            else:
                out_name=os.path.basename(filename)

            print(os.path.join(outdir, out_name, out_name + '.tif'))
            print("FILE EXISTS?:" + str(os.path.exists(os.path.join(outdir, out_name, out_name + '.tif'))))
            if (overwrite == 'Yes'):
                if os.path.exists(os.path.join(outdir,out_name,out_name+'.tif')):
                    os.remove(os.path.join(outdir,out_name,out_name+'.tif'))
                if os.path.exists(os.path.join(outdir,out_name,out_name+'.tif.aux.xml')):
                        os.remove(os.path.join(outdir,out_name,out_name+'.tif.aux.xml'))

            if overwrite == 'No' and os.path.exists(os.path.join(outdir,out_name,out_name+'.tif')):
                log.send_warning(" File <b>Already processed</b>")
                return

            smartResolution=''
            userresolution=0
            S2_projections=[]
            S2_resolutions=[]

            print('*--------------------------------*')
            print(resolution)
            print(resolution.split(" "))
            if len(resolution.split()) == 4:
                userresolution=resolution.split(" ")[-1]

            #  convert user proj to
            outProj4 = user_projection
            try:
                if 'EPSG' in user_projection or 'ESRI' in user_projection:
                    print(' ******************   Converting EPSG / ESRI code to Proj4   **************************')

                    outProj4 = subprocess.getoutput('gdalsrsinfo -o proj4 ' + user_projection).replace("\n", '')
                    if 'ERROR' in outProj4:
                        log.send_error(" Error while converting " + user_projection + " to Proj4")
                        outProj4 = user_projection
            except Exception as e:
                log.send_error(" Error while converting " + user_projection + " to Proj4")
                log.close()
                return

            print('Target PROJ:',outProj4)

            # in case of low resolution for qklooks, force it during warping. On higher res it will introduce artifacts
            if (user_projection == 'UTM' and float(userresolution) > 99) or \
                ('EPSG' in user_projection or 'ESRI' in user_projection) and ( \
                        ('+units=dd' in outProj4 and float(userresolution) >= 0.005 )  or \
                        ('+units=m' in outProj4 and float(userresolution) >= 99 )  ):

                smartResolution=resolution.replace('user','')
                print('------------')
                print(userresolution)
                print(smartResolution)
                print('------------')

            log.send_message(" unzipping bands ")
            with zipfile.ZipFile(file) as z:


                for name in z.namelist():
                                                                                # old 2015 name                  # new met MTD_MSIL1C.xml
                    if sum([n in name for n in bands]) > 0 or name.endswith(file[-53:].replace('.zip','.xml')) or name.endswith('MTD_MSIL1C.xml'):

                        outjp2name = os.path.join(tmp_indir, os.path.basename(name))

                        # ensure the layerstak is done keeping the original band order (name)
                        outjp2name = outjp2name.replace('AOT_20m.jp2','B01.jp2')\
                            .replace('WVP_20m.jp2','B09.jp2')\
                            .replace('CLD_20m.jp2','B10.jp2') \
                            .replace('_20m', '').replace('_10m', '')



                        with z.open(name) as zf, open(outjp2name, 'wb') as f:

                            # avoid extracting new 09/2021 quality masks
                            if "MSK_QUALIT_" in outjp2name or "MSK_DETFOO_" in outjp2name:
                                print('Skipping ', outjp2name)
                                f.close()
                                os.remove(outjp2name)

                                continue
                            shutil.copyfileobj(zf, f)
                            f.close()
                            print(outjp2name)
                            if outjp2name.endswith('.xml') and outname == "Short":
                                os.rename(outjp2name,os.path.join(tmp_indir,metadata_name))
                            if outjp2name.endswith('MTD_MSIL1C.xml'):

                                tmp = outjp2name.split('.')[0]
                                metadata_name = out_name.replace('MSIL1C', 'MTD_MSIL1C') + '.xml'
                                os.rename(outjp2name,os.path.join(tmp_indir,metadata_name))

                            if outjp2name.endswith('.jp2'):
                                #print outjp2name
                                JP2butNoINTERSECT = False

                                if os.path.isfile(aoi) and not test_overlapping(aoi,outjp2name):
                                    os.remove(outjp2name)
                                    JP2butNoINTERSECT=True
                                    print("removing jp2: no intersection")
                                    break
                                else:
                                    in_ds = gdal.Open(outjp2name)
                                    prj=in_ds.GetProjection()
                                    srs=osr.SpatialReference(wkt=prj)
                                    S2_projections.append( srs.GetAuthorityCode(None))
                                    in_ds=None

            # reset the bands ID array: from now on MSIL2A files are renamed to MSIL1C format
            for i in range(0, len(bands)):
                bands[i] = bands[i].replace('AOT_20m','B01').replace('WVP_20m','B09').replace('CLD_20m','B10').replace('_20m','').replace('_10m','')

            if len(S2_projections) == 0 and JP2butNoINTERSECT:
                log.send_warning("AOI does not intersect input image")
                return

            # ----------------------------  ZIP containing TOA tif file from WEB ----------------------
            if len(S2_projections) == 0:
                log.send_message("Zip file does not contain raw .jp2 S2 data; importing as TOA reflectance product")
                with zipfile.ZipFile(file) as z:
                    for name in z.namelist():
                        if name.endswith('.tif') or name.endswith('.xml'):
                            outjp2name = os.path.join(tmp_indir, os.path.basename(name))
                            with z.open(name) as zf, open(outjp2name, 'wb') as f:
                                shutil.copyfileobj(zf, f)
                                f.close()
                                print(outjp2name)
                log.send_warning("S2 TOA imported correctly " + str(",".join(S2_projections)))
                if (not os.path.exists(os.path.join(outdir, out_name))):
                    os.mkdir(os.path.join(outdir, out_name))

                try:
                    os.chdir(tmp_indir)
                    for INfile in glob.glob('*.*'):
                        shutil.move(INfile, os.path.join(outdir, out_name, INfile))
                    os.chdir('..')
                except Exception as e:
                    log.send_error("Error importing S2 as processed TOA archive")
                    return
                return






            log.send_message("S2 Projections LIST: " + str(",".join(S2_projections)))
            data = Counter(S2_projections)
            projection="EPSG:"+str(data.most_common(1)[0][0])  # Returns the highest occurring item
            log.send_message("Most covered projection: " + str(projection))

            os.chdir(tmp_indir)
            jp2list=list_files('./','.jp2')
            print(jp2list)
            if len(jp2list)==0:
                print("No Images")
                os.chdir('..')
                log.send_warning("No intersection between AOI and JP2")
                return


            CLIP=''
            if os.path.isfile(aoi):
                print("Clipping")
                CLIP=' -cutline '+aoi  # -crop_to_cutline adds 0.5 shift in v2.0 2016 still unsolved
                shp_ds = ogr.Open(aoi,update=0)
                layer = shp_ds.GetLayer(0)
                x_min, x_max, y_min, y_max = layer.GetExtent()
                print("SHP extent")
                print(layer.GetExtent())

                sourceSR = layer.GetSpatialRef()
                shp_ds=None
                layer=None


            for jp2 in jp2list:
                print(jp2)
                outfile=jp2.replace('.jp2','.tif')

                # user_projection = projection
                # not optimal :
                # warp to out prj
                # get the bbox coordinate 4 clipping since -crop_to_cutline adds shift
                # -crop_to_cutline works fine with -tap and -tr but we need to convert -tr to out proj; from gdal 2.0 conversion is done onthefly
                # Set default out resolution to maintain 10-20-60 mt after warping to a different UTM
                if smartResolution == "" :
                        print(jp2[-7:-4])
                        if jp2[-7:-4] in ['B01','B09','B10'] :
                            smartResolutionUTM =' -tap -tr 60 60'
                        elif jp2[-7:-4] in ['B02','B03','B04','B08']:
                            smartResolutionUTM =' -tap -tr 10 10'
                        elif jp2[-7:-4] in ['B05','B06','B07','B8A','B11','B12']:
                            smartResolutionUTM =' -tap -tr 20 20'
                        else:
                            print("Band not found: using 10mt resolution")
                            smartResolutionUTM =' -tap -tr 10 10'
                else :
                    smartResolutionUTM = smartResolution

                print("SmartResolution:")
                print(smartResolution)
                print("Smart Resolution UTM:")
                print(smartResolutionUTM)
                print('Projection')
                print(projection)
                print('User Projection')
                print(user_projection)


                if user_projection == 'UTM':

                    res1 = os.system('gdalwarp -of GTiff -multi -srcnodata 0 -dstnodata 0 -ot UInt16 -t_srs '+ projection +" "+smartResolutionUTM+" "+jp2+ ' '+outfile+' '+CLIP)
                    os.remove(jp2)

                if 'EPSG' in user_projection or 'ESRI' in user_projection:


                    print("//////////////////  User resolution:")
                    print(userresolution)
                    if float(userresolution) > 0:
                        print(userresolution)

                        smartResolutionLL = '-tr '+str(userresolution)+' '+str(userresolution)
                        print("Smart Resolution LL:")
                        print(smartResolutionLL)
                        res1 = os.system('gdalwarp -of GTiff -multi -srcnodata 0 -dstnodata 0 -ot UInt16 -t_srs "'+ outProj4 +'" -tap '+smartResolutionLL+' '+jp2+' '+outfile)
                        os.remove(jp2)
                    else:
                        print(" ??????????????????????  HERE ????????????????????????????")
                        print("Process using original resolution and resample to H or L accordingly")
                        print(outProj4)
                        res1 = os.system('gdalwarp -of GTiff -multi -srcnodata 0 -dstnodata 0 -ot UInt16 -t_srs "'+ outProj4 +'" '+jp2+ ' '+outfile+' '+CLIP)
                        os.remove(jp2)


                # ---------------  GET OUT RESOLUTION after warping to set the highest/lowest ---------------
                # ---------------- -tr is necessary when using -tap to allign out pixels --------------------
                # ---------------- -resolution low/high not working properly   ------------------------------
                if resolution.find('highest') or resolution.find('lowest'):
                    in_ds = gdal.Open(outfile)
                    current_res = float(in_ds.GetGeoTransform()[1])
                    print("RES in H or L = " + str(current_res))
                    S2_resolutions.append(current_res)
                    in_ds=None


                if not CLIP == '':

                    raster_ds = gdal.Open(outfile)
                    transform = raster_ds.GetGeoTransform()
                    xOrigin = transform[0]
                    yOrigin = transform[3]
                    pixelWidth = transform[1]
                    pixelHeight = transform[5]
                    xEnd=xOrigin+pixelWidth*raster_ds.RasterXSize
                    yEnd=yOrigin+pixelHeight*raster_ds.RasterYSize
                    print("Xorigin = " + str(xOrigin))
                    print("Yorigin = " + str(yOrigin))
                    print("Xend = " + str(xEnd))
                    print("Yend = " + str(yEnd))
                    targetSR = osr.SpatialReference()
                    targetSR.ImportFromWkt(raster_ds.GetProjectionRef())
                    raster_ds=None
                    sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                    targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
                    coordTrans = osr.CoordinateTransformation(sourceSR,targetSR)

                    shpExtent=ogr.CreateGeometryFromWkt("POLYGON (("+str.format('{0:.25f}',x_min)+' '+str.format('{0:.25f}',y_max)+','+\
                                                                     str.format('{0:.25f}',x_max)+' '+str.format('{0:.25f}',y_max)+','+\
                                                                     str.format('{0:.25f}',x_max)+' '+str.format('{0:.25f}',y_min)+','+\
                                                                     str.format('{0:.25f}',x_min)+' '+str.format('{0:.25f}',y_min)+','+\
                                                                     str.format('{0:.25f}',x_min)+' '+str.format('{0:.25f}',y_max)+"))")
                    shpExtent.Transform(coordTrans)
                    ULX,LRX,LRY,ULY = shpExtent.GetEnvelope()

                    if ULX <  xOrigin:
                            ULX = xOrigin
                            print("ULX out ")
                    if ULY > yOrigin:
                            ULY = yOrigin
                            print("ULY out")
                    if LRX >  xEnd:
                            LRX = xEnd
                            print("LRX out")
                    if LRY < yEnd:
                            LRY = yEnd
                            print("LRY out")

                    #  Recompute BBOX coordinate of IMG using shp extent
                    #  in gdal2.1 -projwin shift img
                    px = math.floor((float(ULX) - float(xOrigin))/float(pixelWidth))
                    py = math.floor((float(ULY) - float(yOrigin))/float(pixelHeight))
                    px1 = math.ceil((float(LRX) - float(xOrigin))/float(pixelWidth))
                    py1 = math.ceil((float(LRY) - float(yOrigin))/float(pixelHeight))
                    print('Pixels')
                    print(px, py, px1, py1)

                    ULX = xOrigin+px*pixelWidth
                    ULY = yOrigin+py*pixelHeight
                    LRX = xOrigin+px1*pixelWidth
                    LRY = yOrigin+py1*pixelHeight

                    print(ULX, ULY, LRX, LRY)

                    shutil.move(outfile,outfile+'_tmp')
                    res = os.system('gdal_translate -of GTiff -co TILED=YES -co COMPRESS=LZW -co BIGTIFF=IF_SAFER -projwin '+str.format('{0:.25f}',ULX)+' '+str.format('{0:.25f}',ULY)+' '+str.format('{0:.25f}',LRX)+' '+str.format('{0:.25f}',LRY)+' '+outfile+'_tmp '+outfile)
                    os.remove(outfile+'_tmp')

                # prepare gdal_cal input
                gdal_argv = ['myself']
                gdal_argv.append('--outfile='+outfile)
                gdal_argv.append('--NoDataValue=0')
                gdal_argv.append('-A')
                gdal_argv.append(outfile+'_tmp')
                gdal_argv.append('--allBands=A')
                gdal_argv.append('--co')
                gdal_argv.append('COMPRESS=LZW')
                gdal_argv.append('--co')
                gdal_argv.append('BIGTIFF=IF_SAFER')
                gdal_argv.append('--quiet')

                if outtype == "Byte":
                    shutil.move(outfile,outfile+'_tmp')
                    gdal_argv.append('--type=Byte')
                    gdal_argv.append('--calc=(A.astype(numpy.float32)*0.0255)')

                    # if (GLOBALS['OS'] == "unix" ):
                    #     res1 = os.system('gdal_calc.py --NoDataValue=0 --type=Byte --allBands=A --co COMPRESS=LZW --co BIGTIFF=IF_SAFER --outfile="'+outfile+'" -A "'+outfile+'_tmp" --calc="(A.astype(numpy.float32)*0.0255)"')
                    # else:
                    #     res1 = os.system('python '+GLOBALS['root_path']+'/Libs/win64/GDAL/gdal_calc.py --NoDataValue=0 --type=Byte --allBands=A --co COMPRESS=LZW --co BIGTIFF=IF_SAFER --outfile="'+outfile+'" -A "'+outfile+'_tmp" --calc="(A.astype(numpy.float32)*0.0255)"')
                    sys.argv = gdal_argv
                    try:
                        gdal_calc.main()
                    except Exception as e:
                        log.send_error("Expression error: " + str(e).replace("'", ""))
                    os.remove(outfile+'_tmp')


                if outtype == "Float32":
                    shutil.move(outfile,outfile+'_tmp')
                    gdal_argv.append('--type=Float32')
                    gdal_argv.append('--calc=(A.astype(numpy.float32)/10000)')

                    # if (GLOBALS['OS'] == "unix" ):
                    #     res1 = os.system('gdal_calc.py --NoDataValue=0 --type=Float32 --allBands=A --co COMPRESS=LZW --co BIGTIFF=IF_SAFER --outfile="'+outfile+'" -A "'+outfile+'_tmp" --calc="(A.astype(numpy.float32)/10000)"')
                    # else:
                    #     res1 = os.system('python '+GLOBALS['root_path']+'/Libs/win64/GDAL/gdal_calc.py --NoDataValue=0 --type=Float32 --allBands=A --co COMPRESS=LZW --co BIGTIFF=IF_SAFER --outfile="'+outfile+'" -A "'+outfile+'_tmp" --calc="(A.astype(numpy.float32)/10000)"')
                    sys.argv = gdal_argv
                    try:
                        gdal_calc.main()
                    except Exception as e:
                        log.send_error("Expression error: " + str(e).replace("'", ""))
                    os.remove(outfile+'_tmp')







            tiflist=list_files('./','.tif')
            listavrt=''


            if resolution.find('highest') >= 0:
                print("H:")
                resolution = '-tr '+str(min(S2_resolutions))+" "+str(min(S2_resolutions))
            elif resolution.find('lowest') >= 0:
                print("L:")
                resolution = '-tr '+str(max(S2_resolutions))+" "+str(max(S2_resolutions))

            else:
                print("User:")
                resolution=resolution.replace('user','')

            log.send_message("Selected: " + str(resolution))
            print('========================================')
            for ID in bands:
                print(ID)
                ID = ID.replace('.jp2','.tif')
                tif2merge = list_files('./',ID)
                if len(tif2merge) == 0:
                    print("No Intersecting Images or bands not available")
                    log.send_message("No intersecting images")
                    os.chdir('..//')
                    return

                lista=''
                for img in tif2merge:
                    lista=lista+img+' '

                res1 = os.system("gdalbuildvrt -srcnodata 0 -hidenodata -tap "+resolution+" "+ID+".vrt "+lista )
                listavrt=listavrt+ID+".vrt "

            res1 = os.system("gdalbuildvrt -srcnodata 0 -hidenodata -separate mosa.vrt -tap " +resolution+" "+listavrt)  # -r cubic
            if 'EPSG' in user_projection or 'ESRI' in user_projection:
                # only now convert to LL ; warp compression does not work , run translate as well
                res1 = os.system("gdalwarp -of GTiff -t_srs "+user_projection+" -multi -co BIGTIFF=IF_SAFER mosa.vrt " +out_name+'_tmp.tif')
                res1 = os.system("gdal_translate -of GTiff -strict -ot "+outtype +" -co TILED=YES -co COMPRESS=LZW -co BIGTIFF=IF_SAFER " +out_name+'_tmp.tif ' +out_name+'.tif')
                os.remove( out_name+'_tmp.tif')
            else:
                res1 = os.system("gdal_translate -of GTiff -strict -ot "+outtype +" -co TILED=YES -co COMPRESS=LZW -co BIGTIFF=IF_SAFER mosa.vrt " +out_name+'.tif')

            strBands=",".join(bands).replace(".jp2","").replace("_","")

            gdal_edit = 'gdal_edit'
            if (GLOBALS['OS'] == "unix" ):
                gdal_edit = 'gdal_edit.py'

            res1 = os.system(gdal_edit+' '+out_name+'.tif -mo "Impact_product_type=calibrated" -mo "Impact_bands='+strBands+'" -mo "Impact_version='+IMPACT.get_version()+'"')
            # else:
            #     res1 = os.system("python "+GLOBALS['root_path']+'/Libs/win64/GDAL/gdal_edit.py '+out_name+'.tif -mo "Impact_product_type=calibrated" -mo "Impact_bands='+strBands+'" -mo "Impact_version='+IMPACT.get_version()+'"')

            res1 = os.system("gdaladdo " + out_name +'.tif 2 4')

            for img in list_files('./','.vrt'):
                os.remove(img)
            for img in tiflist:
                os.remove(img)

            if (not os.path.exists(os.path.join(outdir,out_name))):
                os.mkdir(os.path.join(outdir,out_name))

            for INfile in glob.glob('*.*'):
                shutil.move(INfile,os.path.join(outdir,out_name,INfile))

            try:
                os.chdir('..//')       # get out ftom TMP directory
            except:

                os.chdir('..//')       # get out ftom TMP directory in case of fail
                log.send_error("Error processing bands ")
                return

            log.send_message("Complete")
            return

    except Exception as e:
            log.send_error("generic Error: " + str(e))
            return


# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------


# -----------   CLEAN TM FILE --------------
def create_tmp_file(tmpindir):
    if not os.path.exists(tmpindir):
        os.mkdir(tmpindir)
    return "1"


def clean_tmp_file(tmpindir):
    
    for root, dirs, files in os.walk(tmpindir, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    return "1"
