#!/bin python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import numpy
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import LogStore
import Settings
import IMPACT
from Simonets_EO_libs import *
from Simonets_PROC_libs import get_image_medians_from_mask
import ImageProcessing


SETTINGS = Settings.load()
numpy.seterr(divide='ignore', invalid='ignore')


def usage():
    print('Author: Simonetti Dario for European Commission  2015')
    print('Forest normalization ')
    print('image_normalization.py overwrite indir')
    print()
    sys.exit(0)

def getArgs(args):

    infiles = None
    if (len(sys.argv) < 3):
        usage()
    else:
        overwrite = sys.argv[1]

    infiles = json.loads(sys.argv[2])
    if infiles is None:
        usage()

    else:
        return overwrite, infiles


def getEndMembers(index):
    params = SETTINGS['processing_classification'][index]
    values = []
    values.append(float(params['b']))
    values.append(float(params['g']))
    values.append(float(params['r']))
    if "rededge" in params:
        values.append(float(params['rededge']))
    if "near" in params:
        values.append(float(params['near']))
    if "swir1" in params:
        values.append(float(params['swir1']))
    if "swir2" in params:
        values.append(float(params['swir2']))
    return values




# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            LOOP input list and process file according to flags
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

def run_normalization(overwrite, IN, FMASK, log):

    ImageProcessing.validPathLengths(IN, None,5, log)

    if myfile.endswith('_fnorm.tif') or "_fnorm_" in myfile or "_class.tif" in myfile or "_cluster.tif" in myfile or "_unmix.tif" in myfile:
        log.send_warning(myfile+' file not supported')
        return

    try:
        OUT=""
        sensor=''

        #### LandSat ####
        if IN.endswith('_calrefbyt.tif'):
            OUT=IN.replace('_calrefbyt.tif','_fnorm_calrefbyt.tif')
            sensor='oli'
            fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization'))*255		#### LandSat ####

        if IN.endswith('_calrefflt.tif'):
            OUT=IN.replace('_calrefflt.tif','_fnorm_calrefflt.tif')
            sensor='oli'
            fnorm_median_reference = getEndMembers('Landsat_forest_normalization')


        #### RapidEye ####
        if IN.endswith('_calrefx10k.tif'):
            OUT=IN.replace('_calrefx10k.tif','_fnorm_calrefx10k.tif')
            sensor='RE-1'
            fnorm_median_reference = numpy.array(getEndMembers('RapidEye_forest_normalization'))*10000  # rededge is in but not used (0)


        #### SkyBox ####
        if (('_pansharp_bgrn_' in IN) or ("_ms_"  in IN)) :
            OUT=IN.replace('_calrefx10k.tif','_fnorm_calrefx10k.tif')
            sensor='SKYBOX'
            fnorm_median_reference=[0.086,0.062,0.043,0.247]

        #### SPOT ####
        if ('SPOT' in IN) :
            OUT=IN.replace('.tif','_fnorm.tif')
            sensor='SPOT'
            fnorm_median_reference=[0.247*255,0.043*255,0.062*255,0.109*255]

        #### Sentinel 2A ####
        if ('S2A' in IN or 'S2B' in IN) and ("_MSI" in IN) and ("L1C_" in IN or "L2A_" in IN):
            OUT=IN.replace('.tif','_fnorm.tif')
            sensor='S2A_L1C'
            img_ds = gdal.Open(IN)
            DataType = img_ds.GetRasterBand(1).DataType
            DataType = gdal.GetDataTypeName(DataType)
            img_ds = None
            print("Type: " + DataType)
            if DataType == "Byte":
                fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization'))*255
            elif DataType == "UInt16":
                fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization')) * 10000
            elif DataType == "Int16":
                fnorm_median_reference = numpy.array(getEndMembers('Landsat_forest_normalization')) * 10000
            else:
                fnorm_median_reference = getEndMembers('Landsat_forest_normalization')

        if (OUT == '') :
            log.send_warning("File "+IN+" not supported. Unable to determine sensor type or not a *_calref*")
        else:

            log.send_message("------")
            log.send_message("Input:"+IN)
            log.send_message("Output:"+OUT)
            log.send_message("Sensor:"+sensor)
            log.send_message("------")


            if (os.path.exists(OUT) and overwrite == 'No'):
                log.send_warning(OUT+' file <b>Already Processed </b>')
                return
            try:
                fnorm_median_delta=[]
                if os.path.exists(OUT):
                    os.remove(OUT)
                if os.path.exists(OUT+'.aux.xml'):
                    os.remove(OUT+'.aux.xml')

                try:
                    log.send_message("Reading Forest Normalization Values")

                    # compute bands median value inside a Mask (evergreen forest)
                    # result is in the IMAGE dataType
                    fnorm_median=get_image_medians_from_mask(IN,sensor,REFmask)#[0.086,0.062,0.043,0.247,0.109,0.039]
                    log.send_message("Reference: -->" + str(fnorm_median_reference))
                    log.send_message("Extracted: -->" + str(fnorm_median))

                    if len(fnorm_median) > 1:
                        fnorm_median_delta=numpy.subtract(fnorm_median_reference,fnorm_median)
                        log.send_message("DELTA: -->"+str(fnorm_median_delta))
                    else:
                        log.send_error("Forest Normalization Failure: median extraction error ")
                        log.send_error(str(fnorm_median))
                        return

                except Exception as e:
                    log.send_error("Forest Normalization Failure: memory error ")
                    log.send_error(str(e))
                    return

                log.send_message("Opening file ...")

                try:
                    src_ds=gdal.Open(IN)
                except :
                    log.send_error("Error opening file: "+IN)
                    return

                driver = gdal.GetDriverByName("GTiff")
                src_ds = gdal.Open(IN)
                dst_ds = driver.CreateCopy( OUT+'_tmp', src_ds,1,[ 'COMPRESS=LZW' ,'BIGTIFF=IF_SAFER']) #GDT_Byte
                dst_ds.SetMetadata({'Impact_product_type': 'calibrated','Impact_operation':"EVG Forest Normalization" ,'Impact_version':IMPACT.get_version()})


                for x in range(dst_ds.RasterCount):

                    outband = dst_ds.GetRasterBand(x+1)
                    band = outband.ReadAsArray().astype(numpy.float32)
                    mask = band == 0
                    #print band.min()
                    #print band.max()
                    numpy.add(band,fnorm_median_delta[x],band)
                    DataType = dst_ds.GetRasterBand(1).DataType
                    DataType = gdal.GetDataTypeName(DataType)

                    band[band < 0] = 1
                    if DataType == 'Byte' :
                        band[band > 255] = 255

                    if DataType == 'Float32' :
                        band[band>1] = 1

                    band[mask] = 0
                    outband.WriteArray(band)
                    outband = None

                dst_ds.FlushCache()
                dst_ds = None
                os.rename(OUT+'_tmp',OUT)
                if os.path.exists(OUT+'_tmp.aux.xml'):
                    os.remove(OUT+'_tmp.aux.xml')

            # main block error
            except Exception as e:
                if os.path.exists(OUT+'_tmp'):
                    dst_ds = None
                    src_ds = None
                    OUTCLASS=None
                    os.remove(OUT+'_tmp')

                log.send_error("Error 0:"+str(e))


    except Exception as e:
        log.send_error("Error on Normalization :"+str(e))
        if os.path.exists(OUT+'_tmp'):
            dst_ds=None
            src_ds=None
            OUTCLASS=None
            os.remove(OUT+'_tmp')


if __name__ == "__main__":

    # initialize output log
    log = LogStore.LogProcessing('EVG Forest Normalization', 'Fnorm')
    overwrite,infiles = getArgs(sys.argv)

    log.set_input_file(infiles)
    log.set_parameter('Overwrite output', overwrite)
    REFmask = GLOBALS['root_path']+'/Gui/maps/Global_EVG_map_ll.tif'
    log.set_parameter('Forest mask', REFmask)

    for myfile in infiles:

        log.send_message("Opening file ..."+myfile)
        try:
            res=run_normalization(overwrite,myfile,REFmask,log)
        except Exception as e:
            log.send_error("Processing Error: "+str(e))


    log.send_message("Done")
    log.close()
