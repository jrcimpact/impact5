import ast
import warnings
import numpy as np


from affine import Affine
from .sview import Raster, ViewFinder, View
try:
    from osgeo import ogr, gdal

except ImportError:
    import ogr, gdal

# from . import projection

def read_raster(data, band=1, window=None, window_crs=None, mask_geometry=False, nodata=None, metadata={}, **kwargs):
    """
    Reads data from a raster file and returns a Raster object.

    Parameters
    ----------
    data : str
           File name or path.
    band : int
           The band number to read if multiband.
    window : tuple
             If using windowed reading, specify window (xmin, ymin, xmax, ymax).
    window_crs : pyproj.Proj instance
                 Coordinate reference system of window. If None, use the raster file's crs.
    mask_geometry : iterable object
                    Geometries indicating where data should be read. The values must be a
                    GeoJSON-like dict or an object that implements the Python geo interface
                    protocol (such as a Shapely Polygon).
    nodata : int or float
             Value indicating 'no data' in raster file. If None, will attempt to read
             intended 'no data' value from raster file.
    metadata : dict
                Other attributes describing dataset, such as direction
                mapping for flow direction files. e.g.:
                metadata={'dirmap' : (64, 128, 1, 2, 4, 8, 16, 32),
                            'routing' : 'd8'}

    Additional keyword arguments are passed to rasterio.open()

    Returns
    -------
    out : Raster
          Raster object containing loaded data.
    """
    mask = None
    img_ds = gdal.Open(data)
    print('GDAL')
    print(img_ds.GetGeoTransform())
    affine = Affine.from_gdal(*img_ds.GetGeoTransform())
    crs = img_ds.GetProjectionRef()

    # mynodata = img_ds.GetRasterBand(band).GetNoDataValue()

    #
    data = img_ds.GetRasterBand(band).ReadAsArray()
    shape = data.shape
    # print(shape)
    # data = np.ma.filled(mydata)
    # print(data.shape)
    # data = data.reshape(shape)
    # print(data.shape)
    # if nodata == None and mynodata and mynodata != '':
    #     data[data==mynodata]=np.nan
    #
    # if mynodata is not None:
    #     nodata = data.dtype.type(mynodata)


    #  INPUT DIRECTION MAP has 0 as nodata
    #dirmap = (64, 128, 1, 2, 4, 8, 16, 32)
    nodata = 0
    viewfinder = ViewFinder(affine=affine, shape=shape, mask=mask, nodata=nodata, crs=crs)
    out = Raster(data, viewfinder=viewfinder, metadata=metadata)
    return out
