#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>

import numpy
import os

import sys
import json
import shutil

import scipy


from Simonets_PROC_libs import *
import IMPACT

from __config__ import *
import GdalOgr
import LogStore

import subprocess

try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr, gdal, osr

import gdalconst

import Settings
SETTINGS = Settings.load()


os.environ["PATH"] += os.pathsep.join((os.environ.get("PATH", ""), GLOBALS['Tools']+"/Rusle/my_pysheds/"))


driver = gdal.GetDriverByName("GTiff")

DEBUG = False

outEPSG = 'ESRI:102022'
outRes = 100
outProj4 = subprocess.getoutput('gdalsrsinfo -o proj4 ' + outEPSG).replace("\n", '')
if 'ERROR' in outProj4:
    # log.send_error(" Error while converting " + outEPSG + " to Proj4")
    print(outProj4)
else:
    outEPSG = outProj4



# -------------------------------------------------------------------
# ------------------------- TMP directory  ---------------------------
# -------------------------------------------------------------------
def clean_tmp_file(tmpindir):
    try:
        for root, dirs, files in os.walk(tmpindir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        return "1"
    except Exception as e:
        print(e)
        return "1"

def RUN_RUSLE(aoi, DEM, demFill, demMedian, rusleSand, rusleSilt, rusleClay, rusleCarbon, R_file_raw, rainStart, rainEnd, Rstrategy, LC, LC_YY, LC_type, LC_Ids_Cfact, Mfact, Agri_Pfact, out_name, out_epsg, overwrite, save_tmp, resolution):


    try:
        log = LogStore.LogProcessing('Rusle model', 'Rusle')
        log.set_input_file(aoi)
        log.set_input_file(DEM)

        log.set_input_file(rusleSand)
        log.set_input_file(rusleSilt)
        log.set_input_file(rusleClay)
        log.set_input_file(rusleCarbon)

        log.set_parameter('Fill DEM', demFill)
        log.set_parameter('Median DEM', demMedian)
        log.set_parameter('Processing resolution', resolution)

        log.set_input_file(R_file_raw)

        if resolution == '1ha':
            resolution = 100


        # import only here to avoid double loading with multiprocessing in win
        # start log before to have the log ID on the db 2 avoid duplicated since NUMBA takes time to load
        from my_pysheds.sgrid import sGrid as Grid
        import Download_dataset_from_JRC


        if R_file_raw == '':
            log.set_parameter('Rainfall ', 'CHIRPS')
            log.set_parameter('Rainfall start', rainStart)
            log.set_parameter('Rainfall end', rainEnd)
            log.set_parameter('R factor formula', Rstrategy)
        else:
            Rstrategy = ''

        log.set_input_file(LC)
        if LC == '':
            log.set_parameter('Land Cover Type ', LC_type)
            log.set_parameter('Land Cover year ', LC_YY)
            # LC_file = assigend later after download
        else:
            LC_file = LC
        log.set_parameter('Output EPSG', out_epsg)
        log.set_parameter('Output prefix ', out_name)
        log.set_parameter('Overwrite output', overwrite)



        # log.set_parameter('LC id and C factor', ' '.join(map(str,LC_Ids_Cfact)))
        # log.set_parameter('M factor', ' '.join(map(str,Mfact)))
        # log.set_parameter('Agri and P factor', ' '.join(map(str,Agri_Pfact)))

        # ------------------  preparing tmp folder  ---------------------------------------------
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "Rusle")

        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)
        else:
            clean_tmp_file(tmp_indir)

        out_prefix = os.path.join(tmp_indir, out_name)

        OUT_PATH = os.path.dirname(aoi)
        log.send_message('Output folder : '+ OUT_PATH)
        # TMP OUT
        RUSLE_file = out_prefix+'_RUSLE.tif'
        RUSLE_file_1ha = RUSLE_file.replace('.tif', '_1ha.tif')

        report_file = os.path.join(OUT_PATH, os.path.basename(RUSLE_file)).replace('.tif', '.html')


        # LC_file  # assigned later after download

        median_dem_file = out_prefix + '_dem_median.tif'
        filled_dem_file = out_prefix + '_dem_filled.tif'
        aspect_file = out_prefix + '_aspect.tif'
        direction_file = out_prefix + '_direction.tif'
        slope_percent_file = out_prefix + '_slope_percent.tif'
        slope_file = out_prefix + '_slope.tif'
        slope_length_file = out_prefix + '_slope_length.tif'
        accum_file = out_prefix + '_accumulation.tif'
        M_factor_file = out_prefix + '_M_factor.tif'
        LS_file = out_prefix + '_LS.tif'

        # R_file = 'D:\\IMPACT5\\IMPACT\\DATA\\KWT\\DEM\\1988-1990_meanAnnual_erosiveFactor_5566mt.tif'
        R_file = out_prefix + '_R_factor.tif'
        P_file = out_prefix + '_P_factor.tif'
        K_file = out_prefix + '_K_factor.tif'
        K_file_mask = out_prefix + '_K_factor_MASK.tif'
        C_file = out_prefix + '_C_factor.tif'

        fcsand_file  = out_prefix + '_fcsand.tif'
        fhisand_file = out_prefix + '_fhisand.tif'
        fcsilt_file  = out_prefix + '_fclsilt.tif'
        forgc_file   = out_prefix + '_forgc.tif'


        tmp_files = [RUSLE_file,RUSLE_file_1ha, M_factor_file, LS_file, R_file, P_file, K_file, K_file_mask, C_file]

        if save_tmp:
            tmp_files.append(median_dem_file)
            tmp_files.append(filled_dem_file)
            tmp_files.append(aspect_file)
            tmp_files.append(direction_file)
            tmp_files.append(slope_percent_file)
            tmp_files.append(slope_file)
            tmp_files.append(slope_length_file)
            tmp_files.append(accum_file)
            tmp_files.append(fcsand_file)
            tmp_files.append(fhisand_file)
            tmp_files.append(fcsilt_file)
            tmp_files.append(forgc_file)


        block = False
        for f in tmp_files:
            outfile = os.path.join(OUT_PATH, os.path.basename(f))
            if os.path.exists(outfile):
                if overwrite:
                    os.remove(outfile)
                else:
                    log.send_warning(outfile + ' already there')
                    block = True

        if block:
            log.send_warning("Files already processed, change out name or enable Overwrite option ")
            log.close()
            sys.exit()

        print(report_file)
        if os.path.exists(report_file):
            os.remove(report_file)

        print('Starting ....')


        # ------------------  preparing BBOX in LL projection if needed  ------------------------
        shp_info = GdalOgr.get_info_from_shapefile(aoi)
        extent = json.loads(shp_info.get('extent'))
        PROJ4 = shp_info.get('PROJ4')

        print("SHP PROJ4")
        print(PROJ4)
        if PROJ4 != '+proj=longlat +datum=WGS84 +no_defs':
            print('reprojecting ...')
            source = osr.SpatialReference()
            source.ImportFromProj4(PROJ4)

            target = osr.SpatialReference()
            target.ImportFromEPSG(4326)

            transform = osr.CoordinateTransformation(source, target)
            shpExtent=ogr.CreateGeometryFromWkt("POLYGON ((" + str.format('{0:.10f}',extent.get('W'))+' '+str.format('{0:.10f}',extent.get('N'))+','+\
                                                               str.format('{0:.10f}',extent.get('E'))+' '+str.format('{0:.10f}',extent.get('N'))+','+\
                                                               str.format('{0:.10f}',extent.get('E'))+' '+str.format('{0:.10f}',extent.get('S'))+','+\
                                                               str.format('{0:.10f}',extent.get('W'))+' '+str.format('{0:.10f}',extent.get('S'))+','+\
                                                               str.format('{0:.10f}',extent.get('W'))+' '+str.format('{0:.10f}',extent.get('N'))+"))")
            shpExtent.Transform(transform)
            extent = shpExtent.GetEnvelope()
            bbox = str(extent[0]-0.4) + ',' + str(extent[2]-0.4) + ',' + str(extent[1]+0.4) + ',' + str(extent[3]+0.4)

            print('computing LL bbox' )
            print(bbox)

        else:
            bbox = str(extent.get('S')-0.045) + ',' + str(extent.get('W')-0.045) + ',' + str(extent.get('N')+0.045) + ',' + str(extent.get('E')+0.045)

        log.send_message('BBOX: '+ bbox)

        # ------------------  preparing list of products do be downloaded from JRC   --------------------------------
        prod_codes = []

        if DEM == '':
            prod_codes.append('srtm-de')
        if rusleSand == '':
            prod_codes.append('af-sltppt')
            prod_codes.append('af-orcdrc')
            prod_codes.append('af-sndppt')
            prod_codes.append('af-clyppt')
        if R_file_raw == '':
            prod_codes.append('chirps-dekad')
        if LC == '':
            if LC_type == 'esa-cci':
                prod_codes.append('esa-cci')
            if LC_type == 'copernicus':
                prod_codes.append('cop-lc')  # to be defined


        jrc_data = Download_dataset_from_JRC.run_downloader(prod_codes, bbox, [rainStart,rainEnd], LC_YY, out_prefix)



        for data in jrc_data:
            print(data)
            if DEM == '' and 'srtm-de' in data:
                DEM = data
                if save_tmp:
                    tmp_files.append(DEM)
            if rusleSand == '' and 'af-sndppt' in data:
                rusleSand = data
                if save_tmp:
                    tmp_files.append(rusleSand)
            if rusleSilt == '' and 'af-sltppt' in data:
                rusleSilt = data
                if save_tmp:
                    tmp_files.append(rusleSilt)
            if rusleClay == '' and 'af-clyppt' in data:
                rusleClay = data
                if save_tmp:
                    tmp_files.append(rusleClay)
            if rusleCarbon == '' and 'af-orcdrc' in data:
                rusleCarbon = data
                if save_tmp:
                    tmp_files.append(rusleCarbon)


            if LC == '' and 'esa-cci' in data:
                LC_file = data
                # no need to keep this file, the subset is already saved and copied from tmp to user after proc

        if DEM == '':
            log.send_error("Error downloading DEM")
            log.close()
            return False
        if rusleSand == '':
            log.send_error("Error downloading Sand")
            log.close()
            return False
        if rusleSilt == '':
            log.send_error("Error downloading Silt")
            log.close()
            return False
        if rusleClay == '':
            log.send_error("Error downloading Clay")
            log.close()
            return False
        if rusleCarbon == '':
            log.send_error("Error downloading Carbon")
            log.close()
            return False
        if LC_file == '':
            log.send_error("Error downloading Land cover")
            log.close()
            return False


        # ------- get all rain data and compute the average   -------
        rainImgs=[]
        print('####################################################')
        print(jrc_data)



        Rain_Mean_Month = []
        rain_label_chart = ""
        R_file_from_user = True
        if R_file_raw == '':
            R_file_from_user = False
            for data in jrc_data:
                if 'chirps-dekad' in data or 'tamsat-rfe' in data:
                    rainImgs.append(data)

            log.send_message('Processing rain data: computing Erosive Factor')
            print()
            rain_months = []
            ct = 0


            months = {'01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul',
                      '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec'}



            # image name ends with *_chirps-dekad_202012.tif
            for img in rainImgs:
                yyyymm=str(img[-10:])
                yyyy = str(yyyymm[0:4])
                mm = str(yyyymm[4:6])
                rain_label_chart+="'"+months[mm]+" "+yyyy+"',"


                log.send_message('Processing Rainfall data : '+ img)
                img_ds = gdal.Open(img)
                if ct == 0:
                    img_transform = img_ds.GetGeoTransform()
                    img_crs = img_ds.GetProjectionRef()
                    img_cols = img_ds.RasterXSize
                    img_rows = img_ds.RasterYSize
                    # rain_data = img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
                    rain_months.append(img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32))
                    rain_data_Cum = img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
                    # rain_data_Cum = img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
                    # R_factor = img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32) * 0.
                    # MFI = img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32) * 0.
                    # rain_data = None
                else:
                    # rain_data =
                    rain_months.append(img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32))
                    rain_data_Cum += img_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
                    # rain_data = None

                img_ds = None
                ct +=1


            if ct == 0:
                log.send_error("Failure in downloading rainfall data")
                log.close()
                return False

            # rain_data_Mean = rain_data_Cum
            if ct < 12:
                log.send_warning(
                    'Detected only ' + str(ct) + ' months of rain, annual cumulation might not be accurate')

            if ct >= 13:
                log.send_warning('Detected '+ str(ct) +' months of rain') #, annual cumulation is rescaled to 1 year : cum_rain / months * 12. Ensure full years are taken')
                # log.send_warning('Detected ' + str(ct) + ' months of rain, annual cumulation might not be accurate')
                if Rstrategy == "Wischmeier and Smith (1978)":
                     log.send_warning('Multiple Years R Factor not reliable for Wischmeier and Smith (1978), result is very low compared to other approaches using MFI')
                if Rstrategy == "Renard and Freimund (1994)":
                     log.send_warning('In Renard and Freimund (1994), the multi annual cumulative rainfall are rescaled to 1 year : cum_rain / months * 12. Ensure full years are taken')
                #     log.close()
                #     sys.exit()
                # R_factor /= float(ct)
                # R_factor *= 12.
                # rain_data_Mean = rain_data_Cum/ct*12
                # MFI /= float(ct-1)
                # MFI *= 12.

            # rusleRainMean = out_prefix + '_mean_annual_precipitation.tif'
            # tmp_ds = driver.Create(rusleRainMean, img_cols, img_rows, 1, gdal.GDT_Float32,
            #                        options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            # tmp_ds.SetGeoTransform(img_transform)
            # tmp_ds.SetProjection(img_crs)
            # outband = tmp_ds.GetRasterBand(1)
            # outband.WriteArray(rain_data_Mean)
            # tmp_ds.SetMetadata(
            #     {'Impact_product_type': 'Mean annual precipitation ' + str(rainStart) + ' ' + str(rainEnd)})
            # tmp_ds = None


            # ---------------------------------------------------------------
            # ----  here we have the cumulative, now compute the SUM of *****
            # ----  p is the annual cumulative. using the mean annual we get
            # ----  10 times higher values
            # ---------------------------------------------------------------
            ct1 = 0
            print('Compute R fact')
            print(Rstrategy)

            # enable the R factor here to test the approach 5 og GEE
            print('Preparing MFI ....')
            for img in rain_months:
                img[img < 0]= numpy.nan
                if ct1 == 0:
                    MFI = (img**2) / rain_data_Cum
                else:
                    MFI += (img**2) / rain_data_Cum
                # Rain_Mean_Month.append(img.mean())
                Rain_Mean_Month.append(numpy.nanmean(img))

                img = None
                ct1 += 1

            print('Preparing R  ....')
            if Rstrategy == "Renard and Freimund (1994)":

                # ct is the number of months selected
                if ct >= 13:
                    rain_data_Cum = rain_data_Cum / (ct-1) * 12
                R_factor = (0.0483 * (rain_data_Cum**1.61) * (rain_data_Cum <= 850)) + \
                           ((587.8 - 1.219*rain_data_Cum + 0.004105*(rain_data_Cum**2)) * (rain_data_Cum > 850))

            if Rstrategy == "Arnoldus (1977)":
                # not the best approach with MFI and multiple YY
                # MFI has to be computed by year and lately average the individual MFI
                R_factor = 17.02 * (10 ** (1.5 * numpy.log10(MFI) - 0.8188))


            if Rstrategy == "Wischmeier and Smith (1978)":


                # SUM of the monthly R factor as er formula.
                # however using multple YY the final sum is very LOW .
                # a solution can be using the formula and moving the SUM into the exponent as arnoldus
                # R_factor = 1.735 * (10 ** (1.5 * numpy.log10(MFI) - 0.08188))  # not too bad

                ct1 = 0
                for img in rain_months:
                    if ct1 == 0:
                        R_factor = 1.735 * (10**(1.5 * numpy.log10((img**2)/rain_data_Cum) - 0.08188))    #   not too bad
                    else:
                        R_factor += 1.735 * (10**(1.5 * numpy.log10((img**2)/rain_data_Cum) - 0.08188))   #    not too bad

                    # print('Mean Rainfall for month ' + str(ct1) + ' = ' + str(img.mean()))
                    # print('Mean Rfact for month ' + str(ct1) + ' = ' + str(R_factor.mean()))
                    #print('Mean rain_data_Cum for month '+str(ct1)+' = '+ str(rain_data_Cum.mean()))
                    img = None
                    ct1+=1

            if Rstrategy == "Ferro et al. (1999)":
                # ct is the number of months selected
                # not the best approach with MFI: see Arnoldus
                # MFI has to be computed by year and lately average the individual MFI
                # This current approach uses the multi annual cumulative instead of yearly cum

                # if ct >= 13:
                #     MFI = MFI / (ct-1) * 12
                R_factor = 0.6120 * (MFI ** 1.56)  # in line with D3  GOOD  a bit lower   # From Samela : Multi-Decadal Assess. in Mediterranean sea ...




            #   -----   select the formula to approximate the R Factor from MFI

            # R_factor = 27.8*MFI-189.2     # too high

            # Comment the R factor here to test the approach 5 og 1.735 * ****
            # ----------------------------------------------------------------

            # ----------------------------------------------------------------


            rusleRainCumm = out_prefix + '_cumulative_rain_' + str(rainStart) + '_' + str(rainEnd) + '.tif'

            tmp_ds = driver.Create(rusleRainCumm, img_cols, img_rows, 1, gdal.GDT_UInt16,
                                   options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            tmp_ds.SetGeoTransform(img_transform)
            tmp_ds.SetProjection(img_crs)
            outband = tmp_ds.GetRasterBand(1)
            outband.WriteArray(rain_data_Cum)
            tmp_ds.SetMetadata({'Impact_product_type': 'Rain accumulation ' + str(rainStart) + ' ' + str(rainEnd),'Impact_version': IMPACT.get_version()})
            tmp_ds = None
            rain_data_Cum = None
            print('Cumulative Done')

            rusleRainMFI = out_prefix + '_MFI_' + str(rainStart) + '_' + str(rainEnd) + '.tif'

            tmp_ds = driver.Create(rusleRainMFI, img_cols, img_rows, 1, gdal.GDT_Float32,
                                   options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            tmp_ds.SetGeoTransform(img_transform)
            tmp_ds.SetProjection(img_crs)
            outband = tmp_ds.GetRasterBand(1)
            outband.WriteArray(MFI)
            tmp_ds.SetMetadata({'Impact_product_type': ' MFI ' + str(rainStart) + ' ' + str(rainEnd),'Impact_version': IMPACT.get_version()})
            tmp_ds = None
            MFI = None
            print('MFI Done')

            R_file_raw = out_prefix + '_R_fact_raw_' + str(rainStart) + '_' + str(rainEnd) + '.tif'

            tmp_ds = driver.Create(R_file_raw, img_cols, img_rows, 1, gdal.GDT_UInt16,
                                   options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
            tmp_ds.SetGeoTransform(img_transform)
            tmp_ds.SetProjection(img_crs)
            outband = tmp_ds.GetRasterBand(1)
            outband.WriteArray(R_factor)
            tmp_ds.SetMetadata({'Impact_product_type': 'R_factor_raw_' + str(rainStart) + ' ' + str(rainEnd),'Impact_version': IMPACT.get_version()})
            tmp_ds = None
            R_factor = None
            print('R Factor Done')

            # append the temp rain cumulative file to array to be moved after processing only if downloaded
            if save_tmp:
                tmp_files.append(rusleRainMFI)
                tmp_files.append(rusleRainCumm)

        else:
            # rusleRain contains user input Cum rain data
            print('Using User R Factor')
            #R_file_raw

        if R_file_raw == '':
            log.send_error("Error downloading Rainfall")
            log.close()
            return False


        #  - ----------------------------- overlap test ------------------------------
        if not test_overlapping(aoi, DEM):
            log.send_error("No overlap between AOI and DEM")
            log.close()
            return False

        if not test_overlapping(aoi, LC_file):
            log.send_error("No overlap between AOI and Land cover map")
            log.close()
            return False

        if not test_overlapping(aoi, rusleSand):
            log.send_error("No overlap between AOI and Sand")
            log.close()
            return False

        if not test_overlapping(aoi, rusleSilt):
            log.send_error("No overlap between AOI and Silt")
            log.close()
            return False

        if not test_overlapping(aoi, rusleClay):
            log.send_error("No overlap between AOI and Clay")
            log.close()
            return False

        if not test_overlapping(aoi, rusleCarbon):
            log.send_error("No overlap between AOI and Carbon")
            log.close()
            return False

        if not test_overlapping(aoi, R_file_raw):
            log.send_error("No overlap between AOI and Rain")
            log.close()
            return False

        # -------------------------------------------------------------------------------------
        # reading LC MAP
        # -------------------------------------------------------------------------------------

        LCmap_file = os.path.join(tmp_indir, os.path.basename(LC_file).replace('.tif','_aea.tif'))
        # print(LCmap_file)
        # print(LC_file)
        # print(str(outEPSG))
        map_ds = gdal.Warp(LCmap_file, LC_file, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=outRes, yRes=outRes,
                           multithread=True,
                           # targetAlignedPixels=True,
                           cutlineDSName=aoi, cropToCutline=True,
                           options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'],
                           warpOptions = (['CUTLINE_ALL_TOUCHED', 'TRUE']))

        Map_transform = map_ds.GetGeoTransform()
        masterXorigin = Map_transform[0]
        masterYorigin = Map_transform[3]
        masterPixelWidth = Map_transform[1]
        masterPixelHeight = Map_transform[5]
        MasterXEnd = masterXorigin + masterPixelWidth * map_ds.RasterXSize
        MasterYEnd = masterYorigin + masterPixelHeight * map_ds.RasterYSize
        EXTENT = (masterXorigin, MasterYEnd, MasterXEnd, masterYorigin)
        print(EXTENT)


        map_ds.GetRasterBand(1).SetRasterColorInterpretation(gdal.GCI_PaletteIndex)
        c = gdal.ColorTable()

        if LC == '':
            # ESA CCI
            for item in SETTINGS['map_legends']['raster']:
                if item['type'] == 'esa-cci-lc':
                    for cl in item['classes']:
                        rgb = cl[3].split(',')
                        c.SetColorEntry(int(cl[0]), (int(rgb[0]),int(rgb[1]),int(rgb[2])) )

        else:
            # WORLD Cover
            for item in SETTINGS['map_legends']['raster']:
                if item['type'] == 'esaworldcover':
                    for cl in item['classes']:
                        rgb = cl[3].split(',')
                        c.SetColorEntry(int(cl[0]), (int(rgb[0]),int(rgb[1]),int(rgb[2])) )

        map_ds.GetRasterBand(1).SetColorTable(c)

        LCmap_filePNG = os.path.join(tmp_indir, os.path.basename(LC_file).replace('.tif', '_aea.png'))
        png_ds = gdal.Translate(LCmap_filePNG, map_ds, format='PNG', width=500, height=0)
        png_ds = None
        map_ds = None

        if LC == '':
            if save_tmp:
                tmp_files.append(LCmap_file)

        tmp_files.append(LCmap_filePNG)






        # -------------------------------------------------------------------------------------
        # reading and subsetting DEM
        # -------------------------------------------------------------------------------------
        print('Warping  DEM')
        DEM_tmp = os.path.join(tmp_indir, os.path.basename(DEM).replace('.tif','_aea.tif'))
        try:
            # test if DEM has been downloaded and has a size
            print('DEM SIZE')
            print(os.stat(DEM).st_size)
            if os.stat(DEM).st_size > 500:
                if resolution == 'max':
                    dem_ds = gdal.Warp(DEM_tmp, DEM, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=250, yRes=250,
                           multithread=True, resampleAlg='cubic',
                           # cutlineDSName=aoi, # cropToCutline=True,
                           outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                           creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'],
                       )
                else:
                    dem_ds = gdal.Warp(DEM_tmp, DEM, dstSRS=str(outEPSG), format='Gtiff',
                                       xRes=resolution, yRes=resolution,
                                       multithread=True, resampleAlg='cubic',
                                       # cutlineDSName=aoi, # cropToCutline=True,
                                       outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                                       creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'],
                                       )
            else:
                print('Error downloading DEM')
                log.send_error("Error downloading DEM")
                log.close()
                return False
        except Exception as e:
            log.send_error("Error downloading DEM")
            log.close()
            return False

        dem_ds = None
        print('DEM saved ...')


        DEM = DEM_tmp

        if demMedian :
            if not os.path.exists(median_dem_file):
                tmp_ds = gdal.Open(DEM)
                transform = tmp_ds.GetGeoTransform()
                crs = tmp_ds.GetProjectionRef()
                dem = tmp_ds.GetRasterBand(1).ReadAsArray()
                tmp_ds = None
                master_rows, master_cols = dem.shape
                median_dem = scipy.ndimage.median_filter(dem, size=(3,3))
                # median_dem = scipy.ndimage.spline_filter(dem, order=3)
                # median_dem = scipy.ndimage.filters.gaussian_filter(dem, sigma=5)
                tmp_ds = driver.Create(median_dem_file, master_cols, master_rows, 1, gdal.GDT_UInt32,
                                       options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                tmp_ds.SetGeoTransform(transform)
                tmp_ds.SetProjection(crs)
                outband = tmp_ds.GetRasterBand(1)
                outband.WriteArray(median_dem)
                tmp_ds.SetMetadata({'Impact_product_type': 'median dem','Impact_version': IMPACT.get_version()})
                tmp_ds = None
        else:
            median_dem_file = DEM

        if demFill:
            if not os.path.exists(filled_dem_file):
                tmp_ds = gdal.Open(median_dem_file)
                transform = tmp_ds.GetGeoTransform()
                crs = tmp_ds.GetProjectionRef()
                tmp_ds = None

                grid = Grid.from_raster(median_dem_file)
                fdem = grid.read_raster(median_dem_file)
                print('Fill pits')
                pit_filled_dem = grid.fill_pits(fdem)
                print('Fill depression')
                flooded_dem = grid.fill_depressions(pit_filled_dem)
                # print('Resolve flats')
                # creates artifacts
                # inflated_dem = grid.resolve_flats(flooded_dem)
                # print('Inflate dem')
                master_rows, master_cols = flooded_dem.shape
                tmp_ds = driver.Create(filled_dem_file, master_cols, master_rows, 1, gdal.GDT_Float32,
                                       options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
                tmp_ds.SetGeoTransform(transform)
                tmp_ds.SetProjection(crs)
                outband = tmp_ds.GetRasterBand(1)
                outband.WriteArray(flooded_dem)
                tmp_ds.SetMetadata({'Impact_product_type': 'Flow_Acc from Pysheds','Impact_version': IMPACT.get_version()})
                tmp_ds = None
        else:
            filled_dem_file = median_dem_file

        print('Opening DEM in processing ....')
        dem_ds = gdal.Open(filled_dem_file)
        dem_transform = dem_ds.GetGeoTransform()
        dem_pixelSize = dem_transform[1]
        dem_crs = dem_ds.GetProjectionRef()
        dem_cols = dem_ds.RasterXSize
        dem_rows = dem_ds.RasterYSize
        options = gdal.DEMProcessingOptions(alg='slope', slopeFormat ='percent', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        gdal.DEMProcessing(slope_percent_file, dem_ds, processing='slope', options=options)
        print('Slope % ... done')
        options = gdal.DEMProcessingOptions(creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        gdal.DEMProcessing(slope_file, dem_ds, processing='slope', options=options)
        print('Slope ... done')
        options = gdal.DEMProcessingOptions(creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        gdal.DEMProcessing(aspect_file, dem_ds, processing='aspect', options=options)
        print('Aspect  ... done')
        dem_ds = None
        print('Slope, aspect, slope% ... done')






        # if not os.path.exists(accum_file):
        print('Processing aspect')
        aspect_ds = gdal.Open(aspect_file)
        aspect_ds = None

        grid = Grid.from_raster(filled_dem_file)
        fdem = grid.read_raster(filled_dem_file)

        # Convert Aspect to Standard d8 direction values
        fdir = grid.flowdir(fdem)
        # fdir= 64*((fdir>= 0)  &  (fdir< 22.5)) + \
        # 128*((fdir>= 22.5) & (fdir<67.5)) + \
        # 1*((fdir>=67.5) & (fdir<112.5)) + \
        # 2*((fdir>=112.5) & (fdir<157.5)) + \
        # 4*((fdir>=157.5) & (fdir<202.5)) + \
        # 8*((fdir>=202.5) & (fdir<247.5)) + \
        # 16*((fdir>=247.5) & (fdir<292.5)) + \
        # 32*((fdir>=292.5) & (fdir<337.5)) + \
        # 64*((fdir>=337.5) & (fdir<=360))

        master_rows, master_cols = fdir.shape
        tmp_ds = driver.Create(direction_file, dem_cols, dem_rows, 1, gdal.GDT_Byte,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(dem_transform)
        tmp_ds.SetProjection(dem_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(fdir)
        tmp_ds.SetMetadata({'Impact_product_type': 'Flow_Direction from Gdal-Impact', 'Impact_operation': "FlowDir",'Impact_version': IMPACT.get_version()})
        tmp_ds = None



        # Compute Flow Accumulation d8
        print('Processing accumulation')
        accum = grid.accumulation(fdir=fdir)
        print('Saving accumulation')

        tmp_ds = driver.Create(accum_file, dem_cols, dem_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(dem_transform)
        tmp_ds.SetProjection(dem_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(accum)
        tmp_ds.SetMetadata({'Impact_product_type': 'Flow_Acc from Pysheds','Impact_version': IMPACT.get_version()})
        tmp_ds = None

        print('Accumulation file ... done')



        # slope length = Acc * pixel size
        print('Pixel size:', dem_pixelSize)
        SlopeLen = accum * dem_pixelSize
        SlopeLen[SlopeLen>122]=122
        SlopeLen[SlopeLen<0]=0

        tmp_ds = driver.Create(slope_length_file, dem_cols, dem_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(dem_transform)
        tmp_ds.SetProjection(dem_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(SlopeLen)
        tmp_ds.SetMetadata({'Impact_product_type': 'SlopeLen from Pysheds','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        accum = None

        slope_ds = gdal.Open(slope_file)
        slope = slope_ds.GetRasterBand(1).ReadAsArray()
        slope_ds = None

        slope[slope < 0] = 0
        slope[slope > 89] = 89

        # -------------------------------------------------------------------------------------
        # M factor
        # -------------------------------------------------------------------------------------
        slopePc_ds = gdal.Open(slope_percent_file)
        slopePC = slopePc_ds.GetRasterBand(1).ReadAsArray()
        slopePc_ds = None
        M_factor = (slopePC >= 5) * float(Mfact[3]) + numpy.logical_and(slopePC > 3, slopePC <= 5) * float(Mfact[2]) + numpy.logical_and(slopePC > 1, slopePC <= 3) * float(Mfact[1]) + numpy.logical_and(slopePC > 0, slopePC <= 1) * float(Mfact[0])
        # slope = None   RE-ENABLE
        slopePC = None
        tmp_ds = driver.Create(M_factor_file, dem_cols, dem_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(dem_transform)
        tmp_ds.SetProjection(dem_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(M_factor)
        tmp_ds.SetMetadata({'Impact_product_type': 'M_factor','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        acc = None

        # -------------------------------------------------------------------------------------
        # LS factor
        # -------------------------------------------------------------------------------------
        # slopePc_ds = gdal.Open(slope_percent_file)
        # slopePC = slopePc_ds.GetRasterBand(1).ReadAsArray()
        # slopePc_ds = None
        #  NOT used in LS but in Agri fact where > 50 is already implemented
        # slopePC[slopePC < 0] = 0
        # slopePC[slopePC > 51] = 50  #99   # CAP to 50 as in literature ?



        #L = ((SlopeLen/22.13)**M_factor)
        #S = (10.8*numpy.sin(numpy.deg2rad(slope))+0.03)*(slopePC < 9) +(18.8*numpy.sin(numpy.deg2rad(slope))-0.05)*( (slopePC >= 9)*(slopePC < 17.6) ) +  (21.9*numpy.sin(numpy.deg2rad(slope))+0.03)*(slopePC >= 17.6)
        # S = (10.8 * numpy.sin(slope) + 0.03) * (slope < 9) + (18.8 * numpy.sin(slope) - 0.05) * (
        #             (slope >= 9) * (slope < 17.6)) + (21.9 * numpy.sin(slope) + 0.03) * (slope >= 17.6)
        #LS = L*S
        # using slope% the LS val are too high
        # LS = ((SlopeLen/22.13)**M_factor) * (0.065 + 0.045 * slopePC + (0.0065*(slopePC**2)))


        LS = ((SlopeLen/22.13)**M_factor) * (0.065 + 0.045 * slope + (0.0065*(slope**2)))

        # LS = ((SlopeLen / 22.13)**0.5) * ((numpy.sin(numpy.deg2rad(slope)) / 0.0896)**1.4)

        slope = None

        tmp_ds = driver.Create(LS_file, dem_cols, dem_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(dem_transform)
        tmp_ds.SetProjection(dem_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(LS)
        tmp_ds.SetMetadata({'Impact_product_type': 'LS Factor','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        M_factor = None
        LS = None
        # slopePC = None


        # --------------------------------------------------------------------------------------
        #   ----       start warping and intersecting maps                             ---------
        # --------------------------------------------------------------------------------------

        TMP_Map_SlopePC = os.path.join(tmp_indir, 'LC_map_SlopePc.vrt')
        if resolution == 'max':
            os.system(
                'gdalbuildvrt -resolution highest -separate -b 1 ' + TMP_Map_SlopePC + ' ' + LCmap_file + ' ' + slope_percent_file)
        else:
            os.system(
                'gdalbuildvrt -resolution user -tr '+str(resolution)+' '+str(resolution)+' -separate -b 1 ' + TMP_Map_SlopePC + ' ' + LCmap_file + ' ' + slope_percent_file)
        tmp_ds = gdal.Open(TMP_Map_SlopePC, gdal.GA_ReadOnly)
        MAP = tmp_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)
        # DATAMASK = (MAP > 0)
        slopePC = tmp_ds.GetRasterBand(2).ReadAsArray().astype(numpy.float32)

        #  NOT used in LS but in Agri fact where > 50 is already implemented
        # slopePC[slopePC < 0] = 0
        # slopePC[slopePC > 51] = 50  #99   # CAP to 50 as in literature ?


        Map_transform = tmp_ds.GetGeoTransform()
        Map_crs = tmp_ds.GetProjectionRef()
        Map_Proj4 = osr.SpatialReference(wkt=tmp_ds.GetProjection()).ExportToProj4()
        Map_master_rows, Map_master_cols = MAP.shape
        print("MAP new shape: ", MAP.shape)
        tmp_ds = None

        # -------------------------------------------------------------------------------------
        # C factor
        # -------------------------------------------------------------------------------------

        # Land cover class IDs as provided by the user, order is fixed

        # Forest_class = [10, 95]
        # Shrub_class = [20]
        # Grass_class = [30]
        # Agri_class = [40]
        # Bare_class = [60]
        # Sparse_class = []
        # Urban_class = [50]
        # Aquatic_class = [90]

        Forest_class = LC_Ids_Cfact[0][0]
        Shrub_class = LC_Ids_Cfact[1][0]
        Grass_class = LC_Ids_Cfact[2][0]
        Sparse_class = LC_Ids_Cfact[3][0]
        Bare_class = LC_Ids_Cfact[4][0]
        Agri_class = LC_Ids_Cfact[5][0]
        Urban_class = LC_Ids_Cfact[6][0]
        Aquatic_class = LC_Ids_Cfact[7][0]


        if LC == '' and LC_type == 'esa-cci':
            # [0] = "No Data";
            # [10] = "Cropland, rainfed";
            # [11] = "Cropland, rainfed, herbaceous cover";
            # [12] = "Cropland, rainfed, tree or shrub cover";
            # [20] = "Cropland, irrigated or postflooding";
            # [30] = "Mosaic cropland (>50%) / natural vegetation (tree, shrub,herbaceous cover) (<50%)";
            # [40] = "Mosaic natural vegetation (tree, shrub, herbaceous cover) (>50%) / cropland (<50%)";
            # [50] = "Tree cover, broadleaved, evergreen, closed to open (>15%)";
            # [60] = "Tree cover, broadleaved, deciduous, closed to open (>15%)";
            # [61] = "Tree cover, broadleaved, deciduous, closed (>40%)";
            # [62] = "Tree cover, broadleaved, deciduous, open (15-40%)";
            # [70] = "Tree cover, needleleaved, evergreen, closed to open (>15%)";
            # [71] = "Tree cover, needleleaved, evergreen, closed (>40%)";
            # [72] = "Tree cover, needleleaved, evergreen, open (15-40%)";
            # [80] = "Tree cover, needleleaved, deciduous, closed to open (>15%)";
            # [81] = "Tree cover, needleleaved, deciduous, closed (>40%)";
            # [82] = "Tree cover, needleleaved, deciduous, open (15-40%)";
            # [90] = "Tree cover, mixed leaf type (broadleaved and needleleaved)";
            # [100] = "Mosaic tree and shrub (>50%) / herbaceous cover (<50%)";
            # [110] = "Mosaic herbaceous cover (>50%) / tree and shrub (<50%)";
            # [120] = "Shrubland";
            # [121] = "Evergreen shrubland";
            # [122] = "Deciduous shrubland";
            # [130] = "Grassland";
            # [140] = "Lichens and mosses";
            # [150] = "Sparse vegetation (tree, shrub, herbaceous cover) (<15%)";
            # [151] = "Sparse tree (<15%)";
            # [152] = "Sparse shrub (<15%)";
            # [153] = "Sparse herbaceous cover (<15%)";
            # [160] = "Tree cover, flooded, fresh or brakish water";
            # [170] = "Tree cover, flooded, saline water";
            # [180] = "Shrub or herbaceous cover, flooded, fresh/saline/brakish water";
            # [190] = "Urban areas";
            # [200] = "Bare areas";
            # [201] = "Consolidated bare areas";
            # [202] = "Unconsolidated bare areas";
            # [210] = "Water bodies";
            # [220] = "Permanent snow and ice";

            Forest_class = [50,60,61,62,70,71,72,80,81,82,90]
            Shrub_class = [40,100,110,120,121,122]
            Grass_class = [130]
            Sparse_class = [140,150,151,152,153]
            Bare_class = [200,201,202]
            Agri_class = [10,11,12,20,30]
            Urban_class = [190]
            Aquatic_class = [160,170,180]


        C_factor = numpy.zeros(MAP.shape, dtype=numpy.float32)

        log.send_message('Building C factor layer')
        # log.send_message('Forest classes and C factor')
        for cl in Forest_class:
            if cl != '':
                log.send_message(str(cl)+ ' -> '+str(LC_Ids_Cfact[0][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[0][1]) if LC_Ids_Cfact[0][1] != '' else 0)
        # log.send_message('Shrub classes and C factor')
        for cl in Shrub_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[1][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[1][1]) if LC_Ids_Cfact[1][1] != '' else 0)
        # log.send_message('Grass classes and C factor')
        for cl in Grass_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[2][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[2][1]) if LC_Ids_Cfact[2][1] != '' else 0)
        # log.send_message('Sparse Veg classes and C factor')
        for cl in Sparse_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[3][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[3][1]) if LC_Ids_Cfact[3][1] != '' else 0)
        # log.send_message('Bare classes and C factor')
        for cl in Bare_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[4][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[4][1]) if LC_Ids_Cfact[4][1] != '' else 0)
        # log.send_message('Agri classes and C factor')
        for cl in Agri_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[5][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[5][1]) if LC_Ids_Cfact[5][1] != '' else 0)
        # log.send_message('Urban classes and C factor')
        for cl in Urban_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[6][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[6][1]) if LC_Ids_Cfact[6][1] != '' else 0)
        # log.send_message('Aquatic classes and C factor')
        for cl in Aquatic_class:
            if cl != '':
                log.send_message(str(cl) + ' -> ' + str(LC_Ids_Cfact[7][1]))
                C_factor += (MAP == int(cl)) * (float(LC_Ids_Cfact[7][1]) if LC_Ids_Cfact[7][1] != '' else 0)

        tmp_ds = driver.Create(C_file, Map_master_cols, Map_master_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Map_transform)
        tmp_ds.SetProjection(Map_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(C_factor)
        tmp_ds.SetMetadata({'Impact_product_type': 'C Factor','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        C_factor = None

        # -------------------------------------------------------------------------------------
        # P factor
        # -------------------------------------------------------------------------------------
        log.send_message('Building P factor layer')
        AGRI = numpy.zeros(MAP.shape, dtype=numpy.int8)
        for cl in Agri_class:
            print('Agri class: ',cl)
            if cl !=0:
                AGRI += MAP == int(cl)
        OTHER = (MAP > 0) & (AGRI == 0)
        MAP = None

        # P_factor = float(Agri_Pfact[0]) * (AGRI * (slopePC >= 0) * (slopePC <= 5)) + \
        #            float(Agri_Pfact[1]) * (AGRI * (slopePC > 5)  * (slopePC <= 10)) + \
        #            float(Agri_Pfact[2]) * (AGRI * (slopePC > 10) * (slopePC <= 20)) + \
        #            float(Agri_Pfact[3]) * (AGRI * (slopePC > 20) * (slopePC <= 30)) + \
        #            float(Agri_Pfact[4]) * (AGRI * (slopePC > 30) * (slopePC <= 50)) + \
        #            float(Agri_Pfact[5]) * (AGRI * (slopePC > 50)) + \
        #            OTHER
        # #
        P_factor = float(Agri_Pfact[0]) * (AGRI * (slopePC >= 0) * (slopePC <= 2)) + \
                   float(Agri_Pfact[1]) * (AGRI * (slopePC > 2) * (slopePC <= 8)) + \
                   float(Agri_Pfact[2]) * (AGRI * (slopePC > 8) * (slopePC <= 12)) + \
                   float(Agri_Pfact[3]) * (AGRI * (slopePC > 12) * (slopePC <= 16)) + \
                   float(Agri_Pfact[4]) * (AGRI * (slopePC > 16) * (slopePC <= 20)) + \
                   float(Agri_Pfact[5]) * (AGRI * (slopePC > 20) * (slopePC <= 25)) + \
                   1.0 * (AGRI * (slopePC > 25)) + \
                   OTHER

                   # extra agri val > 25%


        print('P-Factor done')
        #print(P_factor.max())
        slopePC = None
        AGRI = None
        tmp_ds = driver.Create(P_file, Map_master_cols, Map_master_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Map_transform)
        tmp_ds.SetProjection(Map_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(P_factor)
        tmp_ds.SetMetadata({'Impact_product_type': 'P Factor','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        P_factor = None
        print('P Factor .... done')


        # Determination of Soil Erodibility Factor (K)
        # DATA SOURCE  https://files.isric.org/public/afsis250m/
        # K = %csand * %clay * %orgC * %hisand
        #  FORMULA from : Spatial-Temporal Variability of Future Rainfall Erosivity and Its Impact on Soil Loss Risk in Kenya
        #                 Watene G.  2021
        #                 Appl. Sci. 2021, 11(21), 9903; https://doi.org/10.3390/app11219903
        # warp to target projection
        log.send_message('Building Soil Erodibility Factor')

        import time
        start = time.time()
        print('reading Sand')
        sand_file_tmp = os.path.join(tmp_indir, os.path.basename(rusleSand).replace('.tif','_aea.tif'))
        sand_ds = gdal.Warp(sand_file_tmp, rusleSand, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=outRes, yRes=outRes,
                           multithread=True,
                           cutlineDSName=aoi, #cropToCutline=True,
                           outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                           creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        print("Warp time: " + str(time.time() - start))

        start = time.time()
        Sand_transform = sand_ds.GetGeoTransform()
        Sand_crs = sand_ds.GetProjectionRef()
        csand = sand_ds.GetRasterBand(1).ReadAsArray()
        print("Read time: " + str(time.time() - start))

        start = time.time()
        csandMask = (csand == 255) | (csand == -9999)
        csand[csandMask] = 0
        print("Mask time: " + str(time.time() - start))
        # csand[csand == -9999] = 0
        csand_rows, csand_cols = csand.shape
        sand_ds = None
        print("Execution time: " + str(time.time() - start))


        start = time.time()
        # warp to target projection
        print('Warping Silt')
        silt_file_tmp = os.path.join(tmp_indir, os.path.basename(rusleSilt).replace('.tif','_aea.tif'))
        silt_ds = gdal.Warp(silt_file_tmp, rusleSilt, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=outRes, yRes=outRes,
                           multithread=True,
                           cutlineDSName=aoi, #cropToCutline=True,
                           outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                           creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        print('Reading Silt')
        silt = silt_ds.GetRasterBand(1).ReadAsArray()
        siltMask = (silt == 255) | (silt == -9999)
        silt[siltMask] = 0
        # silt[silt == -9999] = 0
        silt_rows, silt_cols = silt.shape
        silt_ds = None
        print("Execution time: " + str(time.time() - start))

        start = time.time()
        # warp to target projection
        print('Warping Clay')
        clay_file_tmp = os.path.join(tmp_indir, os.path.basename(rusleClay).replace('.tif','_aea.tif'))
        clay_ds = gdal.Warp(clay_file_tmp, rusleClay, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=outRes, yRes=outRes,
                           multithread=True,
                           cutlineDSName=aoi, #cropToCutline=True,
                           outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                           creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        print('Reading Clay')
        clay = clay_ds.GetRasterBand(1).ReadAsArray()
        clayMask = (clay == 255) | (clay == -9999)
        clay[clayMask] = 0
        # clay[clay == -9999] = 0
        clay_rows, clay_cols = clay.shape
        clay_ds = None

        print("Execution time: " + str(time.time() - start))

        start = time.time()
        # warp to target projection
        organic_file_tmp = os.path.join(tmp_indir, os.path.basename(rusleCarbon).replace('.tif','_aea.tif'))
        organic_ds = gdal.Warp(organic_file_tmp, rusleCarbon, dstSRS=str(outEPSG), format='Gtiff',
                           # xRes=outRes, yRes=outRes,
                           multithread=True,
                           cutlineDSName=aoi, #cropToCutline=True,
                           outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                           creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])

        orgC = organic_ds.GetRasterBand(1).ReadAsArray()
        orgCMask = (orgC == 255) | (orgC == -9999)
        # todo simulate global product in 1/10000 where value are x10 compared to AFR
        # orgC *=100
        orgC[orgCMask] = 0
        # orgC[orgC == -9999] = 0
        orgC_rows, orgC_cols = orgC.shape
        organic_ds = None

        print("Execution time orgC: " + str(time.time() - start))

        start = time.time()
        # fcsand = (0.2 + 0.3 * numpy.exp(1*(-0.256*csand*(1-(silt/100.)))))   #// DA
        fcsand = (0.2 + 0.3 * numpy.exp(-0.256*csand*(1.-(silt/100.))))

        csand = 1- (csand/100.)
        fhisand = 1. - ( (0.7 * csand) / (csand + numpy.exp(-5.51 + 22.9 * csand)) )
        # fhisand = 1. - ( (0.7 *( 1. - csand/100.)) / (( 1. - csand/100.) + numpy.exp(-5.51 + 22.9 * ( 1. - csand/100.))) )
        csand = None

        CL_SI = clay + silt
        MASK = CL_SI == 0
        CL_SI[MASK] = 1
        fclsilt = (silt/CL_SI)**0.3

        MASK[ fclsilt == 0 ] = 1
        fclsilt[MASK == 1] = 1



        # print(sum(MASK))
        # if (sum(MASK) > 0):
        tmp_ds = driver.Create(K_file_mask, csand_cols, csand_rows, 1, gdal.GDT_Byte, options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(~MASK)
        tmp_ds.SetMetadata({'Impact_product_type': 'K Factor MASK','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        MASK = None
        # tmp_files.append(K_file_mask)



        MASK = None
        clay = None
        silt = None
        forgc = 1. - ((0.256*orgC) / (orgC + numpy.exp(3.72-(2.95*orgC))))
        orgC = None

        print("Execution time: " + str(time.time() - start))

        start = time.time()
        if (csand_rows != silt_rows or silt_rows != clay_rows or clay_rows != orgC_rows) or \
           (csand_cols != silt_cols or silt_cols != clay_cols or clay_cols != orgC_cols):
            print('Soil data has different resolutions')
            # todo : force resampling
            # forgc = scipy.ndimage.zoom(forgc,(clay_rows, clay_cols),mode='nearest')
            # fclsilt = scipy.ndimage.zoom(fclsilt,(clay_rows, clay_cols),mode='nearest')
            # fcsand = scipy.ndimage.zoom(fcsand,(clay_rows, clay_cols),mode='nearest')
            return


        # K_factor = fcsand * fhisand * fclsilt * forgc   // DA

        K_factor = fcsand * fhisand * fclsilt * forgc  * 0.1317  # see why not to use it https://www.sciencedirect.com/science/article/pii/S2095633916300648




        print("Execution time: " + str(time.time() - start))


        print('Saving file')

        tmp_ds = driver.Create(fcsand_file, csand_cols, csand_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(fcsand)
        tmp_ds.SetMetadata({'Impact_product_type': 'fcsand','Impact_version': IMPACT.get_version()})
        tmp_ds = None

        tmp_ds = driver.Create(fhisand_file, csand_cols, csand_rows,  1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(fhisand)
        tmp_ds.SetMetadata({'Impact_product_type': 'fhisand','Impact_version': IMPACT.get_version()})
        tmp_ds = None

        tmp_ds = driver.Create(fcsilt_file, csand_cols, csand_rows, 1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(fclsilt)
        tmp_ds.SetMetadata({'Impact_product_type': 'fclsilt','Impact_version': IMPACT.get_version()})
        tmp_ds = None

        tmp_ds = driver.Create(forgc_file, csand_cols, csand_rows,  1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(forgc)
        tmp_ds.SetMetadata({'Impact_product_type': 'forgc','Impact_version': IMPACT.get_version()})
        tmp_ds = None



        tmp_ds = driver.Create(K_file, csand_cols, csand_rows,  1, gdal.GDT_Float32,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        tmp_ds.SetGeoTransform(Sand_transform)
        tmp_ds.SetProjection(Sand_crs)
        outband = tmp_ds.GetRasterBand(1)
        outband.WriteArray(K_factor)
        tmp_ds.SetMetadata({'Impact_product_type': 'K Factor','Impact_version': IMPACT.get_version()})
        tmp_ds = None
        K_factor = None

        print("Execution time Saving : " + str(time.time() - start))
        # R Factor --------------------------  DERIVED from MFI --------
        # code is above

        # warp to target projection
        R_factor_ds = gdal.Warp(R_file, R_file_raw, dstSRS=str(outEPSG), format='Gtiff',
                                xRes=100, yRes=100,
                                # do not cut: due to the low resolution you can lose pixel on the edge of the AOI
                                cutlineDSName=aoi, #cropToCutline=False,
                                multithread=True,
                                # targetAlignedPixels=True,

                                outputType=gdal.GDT_UInt16,
                                outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                                creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'],
                                warpOptions=(['CUTLINE_ALL_TOUCHED', 'TRUE']))




        # R_factor_ds.GetRasterBand(1).WriteArray(R_factor)

        #R_factor = R_factor_ds.GetRasterBand(1).ReadAsArray()
        # colors = gdal.ColorTable()
        # colors.CreateColorRamp(0, (0, 0, 0, 255), 0, (0, 0, 0, 255))
        # colors.CreateColorRamp(1,  (7, 158, 175, 255), 1000, (100, 68, 167, 255))
        # colors.CreateColorRamp(1000, (100, 68, 167, 255), 2000, (123, 46, 165, 255))
        # colors.CreateColorRamp(2000, (123, 46, 165, 255), 3000, (147, 24, 163, 255))
        # colors.CreateColorRamp(3000, (147, 24, 163, 255), 4000, (139, 12, 111, 255))
        # colors.CreateColorRamp(4000, (139, 24, 111, 255), 65536, (250, 250, 250, 255))
        # outband = R_factor_ds.GetRasterBand(1)
        #
        # outband.SetColorTable(colors)

        R_factor_ds=None
        outband = None


        TMP_R_K_LS_C_P_file = os.path.join(tmp_indir, 'TMP_R_K_LS_C_P.vrt')
        list_of_file = R_file + ' '+K_file +' '+LS_file+ ' '+C_file +' '+P_file
        if resolution == 'max':
            os.system(
                #'gdalbuildvrt -resolution user -tr 100 100 -separate -b 1 ' + TMP_R_K_LS_C_P_file + ' ' + list_of_file)
                'gdalbuildvrt -resolution highest -separate -b 1 ' + TMP_R_K_LS_C_P_file + ' ' + list_of_file)
        else:
            os.system(

                'gdalbuildvrt -resolution user -tr '+str(resolution)+' '+str(resolution)+' -separate -b 1 ' + TMP_R_K_LS_C_P_file + ' ' + list_of_file)

        tmp_ds = gdal.Open(TMP_R_K_LS_C_P_file, gdal.GA_ReadOnly)

        R_transform = tmp_ds.GetGeoTransform()
        RPixelWidth = R_transform[1]
        RPixelHeight = R_transform[5]
        # RconversionFact = 5566.*5566. / (RPixelWidth*RPixelHeight)

        print('RUSLE')
        # R_K_LS_C_P
        # RUSLE = R_factor.astype(numpy.float32) * K_factor.astype(numpy.float32) * LS.astype(numpy.float32) * C_factor.astype(numpy.float32) * P_factor.astype(numpy.float32)
        RUSLE = tmp_ds.GetRasterBand(1).ReadAsArray().astype(numpy.float32)  #R
        RUSLE *= tmp_ds.GetRasterBand(2).ReadAsArray()  #K
        RUSLE *= tmp_ds.GetRasterBand(3).ReadAsArray()  #LS
        RUSLE *= tmp_ds.GetRasterBand(4).ReadAsArray()  #C
        RUSLE *= tmp_ds.GetRasterBand(5).ReadAsArray()  #P

        # RUSLE /= 10000./abs(RPixelWidth*RPixelHeight)     #  Convet Tons x Ha into pixels
        # RUSLE *=100

        # RUSLE[(RUSLE >= 0.000001) * (RUSLE < 1)] = 1
        RUSLE[RUSLE < 0.000001] = 0
        RUSLE[RUSLE > 200] = 200

        MASK = tmp_ds.GetRasterBand(4).ReadAsArray().astype(numpy.float32)
        RUSLE[MASK==0] = 255
        MASK =  None


        Rusle_ds = driver.Create(RUSLE_file, RUSLE.shape[1], RUSLE.shape[0], 1, gdal.GDT_Byte,
                               options=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        Rusle_ds.SetGeoTransform(tmp_ds.GetGeoTransform())
        Rusle_ds.SetProjection(tmp_ds.GetProjectionRef())
        outband = Rusle_ds.GetRasterBand(1)

        outband.WriteArray(RUSLE)
        outband.SetNoDataValue(255)
        colors = gdal.ColorTable()
        colors.CreateColorRamp(254, (255, 255, 255, 255), 255, (255, 255, 255, 255))
        colors.CreateColorRamp(0, (1, 90, 1, 255), 0, (1, 90, 1, 255))
        colors.CreateColorRamp(1, (1, 100, 1, 255), 2, (1, 100, 1, 255))
        colors.CreateColorRamp(2, (73, 140, 1, 255), 5, (73, 140, 1, 255))
        colors.CreateColorRamp(5, (140, 180, 5, 255), 10, (140, 180, 5, 255))
        colors.CreateColorRamp(10, (220, 233, 1, 255), 20, (220, 233, 1, 255))
        colors.CreateColorRamp(20, (240, 230, 15, 255), 50, (240, 230, 15, 255))
        colors.CreateColorRamp(50, (250, 170, 20, 255), 100, (250, 170, 20, 255))
        colors.CreateColorRamp(100, (250, 100, 5, 255), 150, (250, 100, 5, 255))
        colors.CreateColorRamp(150, (254, 30, 10, 255), 201, (254, 30, 10, 255))

        outband.SetColorTable(colors)
        Rusle_ds.SetMetadata({'Impact_product_type': 'RUSLE Mg per Ha per Year','Impact_version': IMPACT.get_version()})

        RuslePNG = RUSLE_file.replace('.tif', '.png')
        png_ds = gdal.Translate(RuslePNG, Rusle_ds, format='PNG', width=500, height=0)
        tmp_files.append(RuslePNG)
        png_ds = None
        Rusle_ds = None
        tmp_ds = None

        # warp to target projection

        # NOTE RUSLE_1ha_ds contains original values that might differ from the one produced after the intersection with the LC map
        # masking of possible pixel of water (210)
        RUSLE_1ha_ds = gdal.Warp(RUSLE_file_1ha, RUSLE_file, dstSRS=str(outEPSG), format='Gtiff',
                                xRes=100, yRes=100,
                                # do not cut: due to the low resolution you can lose pixel on the edge of the AOI
                                cutlineDSName=aoi, cropToCutline=True,

                                multithread=True,
                                resampleAlg=gdalconst.GRIORA_Average,
                                outputType=gdal.GDT_Byte,
                                dstNodata=255,
                                outputBounds=EXTENT, outputBoundsSRS=str(outEPSG),
                                creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'],
                                warpOptions=(['CUTLINE_ALL_TOUCHED', 'TRUE']))

        RUSLE_1ha_ds = None

 # ------------------   compute STATS    ---------------------------------------

        LC_RUSLE_1ha = os.path.join(tmp_indir, 'LC_RUSLE.vrt')
        os.system(
            'gdalbuildvrt -resolution user -tr 100 100 -separate -b 1 -srcnodata 255 -vrtnodata 255 ' + LC_RUSLE_1ha + ' ' + LCmap_file + ' ' + RUSLE_file_1ha)


        tmp_ds = gdal.Open(LC_RUSLE_1ha, gdal.GA_ReadOnly)

        # tmp_files.append(LC_RUSLE_1ha+'mod.tif')
        # tmp1ha_ds = gdal.Translate(LC_RUSLE_1ha+'mod.tif', LC_RUSLE_1ha, bandList=[2], format='GTiff', creationOptions=['COMPRESS=LZW', 'BIGTIFF=IF_SAFER'])
        MAP = tmp_ds.GetRasterBand(1).ReadAsArray().astype(numpy.uint16)
        RUSLE = tmp_ds.GetRasterBand(2).ReadAsArray().astype(numpy.uint16)

        RUSLE[RUSLE == 255] = 0
        tmp_ds = None

        if LC == '':
            # remove ESA water / snow class
            RusleSUM = (RUSLE*((MAP!=0) * (MAP!=210) * (MAP!=220))).sum()

        else:
            # remove WORLDCOVER  water / snow class
            RusleSUM = (RUSLE*((MAP!=0) * (MAP!=80) * (MAP!=70))).sum()

        Rusle_forest = 0
        Forest_sum = 0
        for cl in Forest_class:
            print(cl)
            if cl != '':
                Forest_sum += (MAP == int(cl)).sum()
                Rusle_forest += ((MAP == int(cl)) * (RUSLE)).sum()
        print(Rusle_forest)

        Rusle_shrub = 0
        Shrub_sum = 0
        for cl in Shrub_class:
            if cl != '':
                Shrub_sum += (MAP == int(cl)).sum()
                Rusle_shrub += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_grass = 0
        Grass_sum = 0
        for cl in Grass_class:
            if cl != '':
                Grass_sum += (MAP == int(cl)).sum()
                Rusle_grass += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_sparse = 0
        Sparse_sum = 0
        for cl in Sparse_class:
            if cl != '':
                Sparse_sum += (MAP == int(cl)).sum()
                Rusle_sparse += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_bare = 0
        Bare_sum = 0
        for cl in Bare_class:
            if cl != '':
                Bare_sum +=(MAP == int(cl)).sum()
                Rusle_bare += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_agri = 0
        Agri_sum = 0
        for cl in Agri_class:
            if cl != '':
                Agri_sum += (MAP == int(cl)).sum()
                Rusle_agri += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_urban = 0
        Urban_sum = 0
        for cl in Urban_class:
            if cl != '':
                Urban_sum += (MAP == int(cl)).sum()
                Rusle_urban += ((MAP == int(cl)) * (RUSLE)).sum()
        Rusle_aquatic = 0
        Aquatic_sum = 0
        for cl in Aquatic_class:
            if cl != '':
                Aquatic_sum += (MAP == int(cl)).sum()
                Rusle_aquatic += ((MAP == int(cl)) * (RUSLE)).sum()
        # print(Forest_sum , Shrub_sum , Grass_sum , Sparse_sum, Bare_sum , Agri_sum , Urban_sum , Aquatic_sum)
        LC_ClassSum = (Forest_sum + Shrub_sum + Grass_sum + Sparse_sum + Bare_sum + Agri_sum + Urban_sum + Aquatic_sum)
        print('LC tot pixel :', LC_ClassSum)
        print('Rusle tot : ', RusleSUM)
        print(Rusle_forest+Rusle_shrub+Rusle_grass+Rusle_sparse+Rusle_bare+Rusle_agri+Rusle_urban+Rusle_aquatic)








        # -------------------------------------------------------------------------------------------------
        # ------------------------------------   REPORT   -------------------------------------------------
        # -------------------------------------------------------------------------------------------------

        try:
            report = '''
                <html>
                <head>
                <title>Report on soil erosion based on RUSLE model </title>
                <style>
                    body{font-family: Arial, sans-serif; font-size:14px; width: 660px;}
                    h1{font-family: Arial, sans-serif; font-size:20px; font-weight:bold}
                    h2{font-family: Arial, sans-serif; font-size:18px; font-weight:bold; margin-top:2px; margin-bottom:0px}
                    h3{font-family: Arial, sans-serif; font-size:14px; font-weight:bold; margin-bottom:0; margin-top:2px; padding:0}
                    h4{font-family: Arial, sans-serif; font-size:14px; margin-bottom:0; margin-top:0px; padding:0}
                    table {border-collapse: collapse; width:100%}
                    table, td, th {border: 1px solid black; padding:3}
                </style>

                <script src="/libs_javascript/echarts-4.4.0/echarts.min.js"></script>

                </head>
                <body>
                <div id="intro">
                    <h1> Report on soil erosion based on RUSLE model </h1>
                </div>
                <div id="GeoInfo">
                    <h2>Geographic location</h2>
                    Country: [Country name] </br>
                    Geographic window: longitude: [--] to [--], latitude: [--] to [--] </br>
                    Zone of interest: [Zone of interest] </br></br>
                </div>
                '''
            report += '<div><h2> User input parameters:</h2>'

            if R_file_from_user == False:
                report += 'Rainfall: monthly CHIRPS v2.0' + '</br>'
                report += 'Selected rainfall dates: ' + str(rainStart) + ' - ' + str(rainEnd) + '</br>'
                report += 'R factor formula: ' + str(Rstrategy) + '</br>'
            else:
                report += 'R factor provided by user </br>'

            if LC == '':
                report += 'Selected Land Cover map: ' + str(LC_type) + '</br>'
                report += 'Selected Land Cover date : ' + str(LC_YY) + '</br>'
            else:
                report += 'Selected Land Cover map: ' + str(LC) + '</br>'
                report += 'Selected Land Cover date :  -----  </br>'
            report += '</div>'

            # -------------------------------------------------------------------------------------------------
            report += '</br><div><h2> Analysis of the input data </h2></br>'

            if resolution == 'max':
                report += 'Processing done at highest spatial resolution among the provided input datasets </br>'
            else:
                report += 'Processing done at a spatial resolution of '+str(resolution) +'m </br>'
            report += '</br> Rain per month mm/ha: '
            if len(Rain_Mean_Month) == 0:
                report += ' not available, R Factor file provided by the user </br>'


            rain_data_chart = ''
            for r in Rain_Mean_Month:
                # report += str(int(r)) + ' '
                rain_data_chart +=str(int(r))+','

            chart_options = '''
                           {
                                    title: {text:'CHIRPS 1 month RFE', x: 'center'},
                                    tooltip: {},
                                    grid: {
                                        left: '15%',
                                        right: '10%',
                                        bottom: '5%',
                                        containLabel: true
                                    },
                                   xAxis: {
                                              type: 'category',
                                              axisLabel: {rotate:45, fontSize:10},
                                              data: ['''+  rain_label_chart[:-1] +''']
                                   },
                                   yAxis: {
                                              type: 'value',
                                              name: 'Rainfall (mm)',
                                              nameLocation:'middle',
                                              yAxisIndex: 0
                                          },
                                   series: [{
                                              type: 'bar',
                                              color: '#1656AD',
                                              name: 'Rainfall',
                                              data: [''' +  rain_data_chart[:-1]  +''']
                                           }],
                                   toolbox: {
                                       show: true,
                                       orient:'vertical',
                                       feature: {
                                           magicType: {show: true, title: ['bar', 'line'], type: ['bar', 'line']},
                                           restore: {show: true, title: 'Restore'},
                                           saveAsImage: {show: true, title: "Save As Image"}
                                       }
                                   }
                           }'''


            report += '<div id="chartRainDiv" style="width: 600px; height: 500px;"></div>'
            report += "<script> var chartRain = echarts.init(document.getElementById('chartRainDiv')); chartRain.setOption(" + chart_options + ");</script>"
            report += ' </br> </br>'

            report += '<div><h2> Land Cover Map </h2></br>'

            if LC == '':
                report += 'Adopted legend: ESA CCI</br>'
                classColor = "['#00A000', '#966400', '#FFCC33', '#FFEBAF', '#FFF5D7', '#FFFF64', '#C31400', '#00DC83']"
            else:
                report += 'Adopted legend: ESA WORLDCOVER </br>'
                classColor = "['#006400', '#ffbb22', '#ffff4c', '#e2e6ca', '#b4b4b4', '#f096ff', '#ff0000', '#0096a0']"

            report += '<div></br><img style="border:1px solid black; margin-left:150px" src="getImageFile.py?filename=' + os.path.join(OUT_PATH,os.path.basename(LCmap_filePNG)) + '&format=png" alt="Land Cover map" width=300></div></br></br>'

            chart_options = '''
                            {
                                chart:{
                                    type: 'pie',
                                    },
                                title: {
                                    text: 'Land Cover',
                                    x: 'center'
                                },
                                legend: {
                                    top: 'bottom',
                                    width:400
                                },
                                 tooltip: {
                                    formatter : function (params){
                                      return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                    },
                                },
                                series: [{
                                    type:'pie',
                                    name: 'Land Cover',
                                    colorByPoint: true,
                                    radius: '50%',
                                    itemStyle : {
                                          normal : {
                                               label : {
                                                  formatter : function (params){
                                                        return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                                  },
                                                  textStyle: {
                                                    color: 'black'
                                                  }
                                              }
                                          }
                                   },
                                   data: [{name: 'Forest', value: '''+str(Forest_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Shrub', value: '''+str(Shrub_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Grass', value: '''+str(Grass_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Sparse Veg.', value: '''+str(Sparse_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Bare Soil', value: '''+str(Bare_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Agriculture', value: '''+str(Agri_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Urban', value: '''+str(Urban_sum/LC_ClassSum*100.)+'''},
                                           {name: 'Aquatic Veg.', value: '''+str(Aquatic_sum/LC_ClassSum*100.)+'''}
                                   ]
                                }],
                                color: ''' + classColor +''',
                                toolbox: {
                                       show: true,
                                       orient:'vertical',
                                       feature: {
                                           saveAsImage: {show: true, title: "Save As Image"}
                                       }
                                }
                            }'''


            report += '<div id="pieLCDiv" style="width: 600px; height: 500px;"></div>'
            report += "<script> var pieLC = echarts.init(document.getElementById('pieLCDiv')); pieLC.setOption(" + chart_options + ");</script>"
            report += '</br></br>\n'




            #--------------------------------------------------------------------------------------------------
            # C factor per LC classes
            report += '''<table id="C_Table" style='font-size:90%' >
                        <thead>
                            <tr>
                                <th>Class ID </th>
                                <th>Class Name</th>
                                <th> C Factor</th>                                                        
                            </tr>
                        </thead>
                        <tbody align="left">
                            <tr>
                                <td>{}</td>
                                <td>Forest</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Shrub</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Grass</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Sparse Vegetation</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Bare</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Agriculture</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Urban</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>{}</td>
                                <td>Aquatic Vegetation</td>
                                <td>{}</td>
                            </tr>
                        </tbody>
                    </table></br>'''.format( Forest_class, LC_Ids_Cfact[0][1],
                                             Shrub_class, LC_Ids_Cfact[1][1],
                                             Grass_class, LC_Ids_Cfact[2][1],
                                             Sparse_class, LC_Ids_Cfact[3][1],
                                             Bare_class, LC_Ids_Cfact[4][1],
                                             Agri_class, LC_Ids_Cfact[5][1],
                                             Urban_class, LC_Ids_Cfact[6][1],
                                             Aquatic_class, LC_Ids_Cfact[7][1]
                                             )
            # P factor per slope %
            print('P FACTOR ')
            report += '''<table id="P_Table" style='font-size:90%' >
                        <thead>
                            <tr>
                                <th>Slope %</th>
                                <th> P value</th>                                                        
                            </tr>
                        </thead>
                        <tbody align="left">
                            <tr>
                                <td>[0-2]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]2-8]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]8-12]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]12-16]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]16-20]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]20-25]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>[25-90] </td>
                                <td>1</td>
                            </tr>
    
                        </tbody>
                    </table></br>'''.format(Agri_Pfact[0],
                                            Agri_Pfact[1],
                                            Agri_Pfact[2],
                                            Agri_Pfact[3],
                                            Agri_Pfact[4],
                                            Agri_Pfact[5]
                                            )
            # M factor per slope %
            print('m FACTOR ')
            report += '''<table id="M_Table" style='font-size:90%' >
                        <thead>
                            <tr>
                                <th>Slope %</th>
                                <th> M value</th>                                                        
                            </tr>
                        </thead>
                        <tbody align="left">
                            <tr>
                                <td>[0-1]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]1-3]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]3-5]</td>
                                <td>{}</td>
                            </tr>
                            <tr>
                                <td>]5-90]</td>
                                <td>{}</td>
                            </tr>
                        </tbody>
                    </table></br>'''.format(Mfact[0],
                                            Mfact[1],
                                            Mfact[2],
                                            Mfact[3]
                                            )


            # -------------------------------------------------------------------------------------------------
            report += '<div><h2> RUSLE output:</h2></br>'
            # ---------------------------------  RUSLE MAP  ---------------------------------------------------

            report += '<div></br><img style="border:1px solid black; margin-left:150px" src="getImageFile.py?filename=' + os.path.join(OUT_PATH,os.path.basename(RuslePNG)) + '&format=png" alt="RUSLE map" width=300></div></br></br>'






            report += '<div><b>'+ str(LC_YY) + '</b> reports a total soil erosion of <b> {:,.2f} * 10^6 Mg/y </b></br>'.format(RusleSUM/1000000.)
            report += ' with an average loss of <b> {:,.2f}  Mg/ha/y </b>'.format(RusleSUM / LC_ClassSum * 1.) + '</br></br>(Note: Water and Snow classes are masked while computing the average)</br></br></br>'

            # --------------------------------------------------------------------------------------------------
            # Rusle per LC classes
            report += '''<table id="Rusle_Table" style='font-size:90%' >
                                    <thead>
                                        <tr>
                                            <th>Class Name</th>
                                            <th>Extent (Ha)</th>
                                            <th> Soil Erosion in 10^3 Mg</th>                                                        
                                        </tr>
                                    </thead>
                                    <tbody align="left">
                                        <tr>
                                            <td>Forest</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Shrub</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Grass</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Sparse Vegetation</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Bare</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Agriculture</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Urban</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                        <tr>
                                            <td>Aquatic Vegetation</td>
                                            <td>{:,.0f}</td>
                                            <td>{:,.0f}</td>
                                        </tr>
                                    </tbody>
                                </table></br>'''.format(Forest_sum, Rusle_forest/1000.,
                                                        Shrub_sum, Rusle_shrub/1000. ,
                                                        Grass_sum, Rusle_grass/1000. ,
                                                        Sparse_sum, Rusle_sparse/1000.,
                                                        Bare_sum, Rusle_bare/1000.,
                                                        Agri_sum, Rusle_agri/1000.,
                                                        Urban_sum, Rusle_urban/1000.,
                                                        Aquatic_sum, Rusle_aquatic/1000.
                                                        )
            # report += ' Soil loss per class (% of tot loss): </br>'
            # report += ' Forest            : {:,.1f} </br>'.format(Rusle_forest/RusleSUM*100.)
            # report += ' Shrub             : {:,.1f} </br>'.format(Rusle_shrub/RusleSUM*100.)
            # report += ' Grass             : {:,.1f} </br>'.format(Rusle_grass/RusleSUM*100.)
            # report += ' Sparse vegetation : {:,.1f} </br>'.format(Rusle_sparse/RusleSUM*100.)
            # report += ' Bare              : {:,.1f} </br>'.format(Rusle_bare/RusleSUM*100.)
            # report += ' Agriculture       : {:,.1f} </br>'.format(Rusle_agri/RusleSUM*100.)
            # report += ' Urban             : {:,.1f} </br>'.format(Rusle_urban/RusleSUM*100.)
            # report += ' Aquatic vegetation: {:,.1f} </br>'.format(Rusle_aquatic/RusleSUM*100.)



            chart_options = '''
                            {
                                chart:{
                                    type: 'pie',
                                    },
                                title: {
                                    text: 'Soil erosion % per Land Cover ',
                                    x: 'center'
                                },
                                legend: {
                                    top: 'bottom',
                                    width:400
                                },
                                 tooltip: {
                                    formatter : function (params){
                                      return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                    },
                                },
                                series: [{
                                    type:'pie',
                                    name: 'Land Cover',
                                    colorByPoint: true,
                                    radius: '50%',
                                    itemStyle : {
                                          normal : {
                                               label : {
                                                  formatter : function (params){
                                                        return  params.data.name + '  '+ params.data.value.toFixed(2) + ' %'
                                                  },
                                                  textStyle: {
                                                    color: 'black'
                                                  }
                                              }
                                          }
                                   },
                                   data: [{name: 'Forest', value: '''+str(Rusle_forest/RusleSUM*100.)+'''},
                                           {name: 'Shrub', value: '''+str(Rusle_shrub/RusleSUM*100.)+'''},
                                           {name: 'Grass', value: '''+str(Rusle_grass/RusleSUM*100.)+'''},
                                           {name: 'Sparse Veg.', value: '''+str(Rusle_sparse/RusleSUM*100.)+'''},
                                           {name: 'Bare Soil', value: '''+str(Rusle_bare/RusleSUM*100.)+'''},
                                           {name: 'Agriculture', value: '''+str(Rusle_agri/RusleSUM*100.)+'''},
                                           {name: 'Urban', value: '''+str(Rusle_urban/RusleSUM*100.)+'''},
                                           {name: 'Aquatic Veg.', value: '''+str(Rusle_aquatic/RusleSUM*100.)+'''}
                                   ]
                                }],
                                color: ''' + classColor +''',
                                toolbox: {
                                       show: true,
                                       orient:'vertical',
                                       feature: {
                                           saveAsImage: {show: true, title: "Save As Image"}
                                       }
                                }
                            }'''


            report += '<div id="pieRusleDiv" style="width: 600px; height: 500px;"></div>'
            report += "<script> var pieRulse = echarts.init(document.getElementById('pieRusleDiv')); pieRulse.setOption(" + chart_options + ");</script>"
            report += '</br></br>\n'



            report += '</div>'

            report += "</body></html>"

            text_file = open(report_file, "w")
            text_file.write(report)
            text_file.close()

            log.send_message('<a target="_blank" href=getHtmlFile.py?file=' + report_file + '><b>View report result</b></a>')

        except Exception as e:
            print(str(e))
            log.send_error('Error preparing report file')




        for f in tmp_files:
            if os.path.exists(f):
                print(f)
                shutil.move(f, os.path.join(OUT_PATH,os.path.basename(f)))


        log.send_message('Done')
        time.sleep(3)  # sleep a bit to give time to unlink file
        # clean_tmp_file(tmp_indir)
        log.close()

        if sys.platform.startswith('win'):
            os.system("start %s" %(GLOBALS['url']+'/getHtmlFile.py?file='+report_file))


    except Exception as e:
        log.send_error(str(e))

    finally:
        log.close()
        clean_tmp_file(tmp_indir)

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('RUSLE model implementation')
    print()
    sys.exit(1)


def getArgs(args):
    indir = None
    if (len(sys.argv) < 23):
        usage()
    else:
        try:
            aoi = json.loads(sys.argv[1])
        except:
            aoi = sys.argv[1]
        try:
            dem = json.loads(sys.argv[2])
        except:
            dem = sys.argv[2]

        if aoi != '' and type(aoi) == type([]):
            aoi = aoi[0]
        if dem != '' and type(dem) == type([]):
            dem = dem[0]

        demFill = sys.argv[3]
        demMedian = sys.argv[4]

        try:
            rusleSand = json.loads(sys.argv[5])
            rusleSilt = json.loads(sys.argv[6])
            rusleClay = json.loads(sys.argv[7])
            rusleCarbon = json.loads(sys.argv[8])
        except:
            rusleSand = sys.argv[5]
            rusleSilt = sys.argv[6]
            rusleClay = sys.argv[7]
            rusleCarbon = sys.argv[8]

        if rusleSand != '' and type(rusleSand) == type([]):
            rusleSand = rusleSand[0]
        if rusleSilt != '' and type(rusleSilt) == type([]):
            rusleSilt = rusleSilt[0]
        if rusleClay != '' and type(rusleClay) == type([]):
            rusleClay = rusleClay[0]
        if rusleCarbon != '' and type(rusleCarbon) == type([]):
            rusleCarbon = rusleCarbon[0]


        try:
            R_file_raw = json.loads(sys.argv[9])
        except:
            R_file_raw = sys.argv[9]
        if R_file_raw != '' and type(R_file_raw) == type([]):
            R_file_raw = R_file_raw[0]
        rainStart = sys.argv[10]
        rainEnd   = sys.argv[11]
        Rstrategy = sys.argv[12]

        try:
            LC = json.loads(sys.argv[13])
        except:
            LC = sys.argv[13]
        if LC != '' and type(LC) == type([]):
            LC = LC[0]

        LC_YY = sys.argv[14]
        LC_type = sys.argv[15]

        try:
            LC_Ids_Cfact = json.loads(sys.argv[16])
        except:
            LC_Ids_Cfact = sys.argv[16]
        try:
            Mfact = json.loads(sys.argv[17])
        except:
            Mfact = sys.argv[17]
        try:
            Agri_Pfact = json.loads(sys.argv[18])
        except:
            Agri_Pfact = sys.argv[18]

        out_name = sys.argv[19]
        out_epsg = sys.argv[20]
        overwrite = sys.argv[21]
        save_tmp = sys.argv[22]
        resolution = sys.argv[23]

        return aoi, dem, demFill, demMedian, rusleSand, rusleSilt, rusleClay, rusleCarbon, R_file_raw, rainStart, rainEnd, Rstrategy, LC, LC_YY, LC_type, LC_Ids_Cfact, Mfact, Agri_Pfact, out_name, out_epsg, overwrite, save_tmp, resolution


if __name__ == "__main__":

    #RUN_RUSLE('','','','','','','','','','','','','','','','','','var','','')

    aoi, dem, demFill, demMedian, rusleSand, rusleSilt, rusleClay, rusleCarbon, R_file_raw, rainStart, rainEnd, Rstrategy, LC, LC_YY, LC_type, LC_Ids_Cfact, Mfact, Agri_Pfact, out_name, out_epsg, overwrite, save_tmp, resolution= getArgs(sys.argv)

    if demFill in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        demFill = False
    else:
        demFill = True

    if demMedian in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        demMedian = False
    else:
        demMedian = True

    if overwrite in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        overwrite = False
    else:
        overwrite = True

    if save_tmp in ['No', False, 0, 'NO', 'N', 'False', 'false']:
        save_tmp = False
    else:
        save_tmp = True

    RUN_RUSLE(aoi, dem, demFill, demMedian, rusleSand, rusleSilt, rusleClay, rusleCarbon, R_file_raw, rainStart, rainEnd, Rstrategy, LC, LC_YY, LC_type, LC_Ids_Cfact, Mfact, Agri_Pfact, out_name, out_epsg, overwrite, save_tmp, resolution)
