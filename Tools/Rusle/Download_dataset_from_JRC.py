#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>


import os

import multiprocessing
from __config__ import *
from osgeo import gdal
# import Settings


# -------------------------------------------------------------------
# ------------------------- TMP directory  ---------------------------
# -------------------------------------------------------------------
def clean_tmp_file(tmpindir):
    for root, dirs, files in os.walk(tmpindir, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    return "1"

def download_product(name, URL):
    import urllib.request as request
    import urllib
    from urllib.error import URLError, HTTPError
    # print(URL)
    # response = requests.get(URL)
    #print(response)
    # 3. Open the response into a new file called instagram.ico
    #open(URL, "wb").write(response.content)

    # if not name.endswith('.tif'):
    #     name = name + '.tif'

    try:

        # data = urllib.request.urlopen(JRC_URL+query_string)
        print(URL)
        res, header = urllib.request.urlretrieve(URL, filename=name) #  data=query_string
    except HTTPError as e:
        print('The server couldn\'t fulfill the request.')
        print('Error code: ', e.code)
        return ''
    except URLError as e:
        print('We failed to reach a server.')
        print('Reason: ', e.reason)
        return ''
    else:
        # everything is fine
        if os.path.exists(name):
            return name
        else:
            return ''
        print('Done')







#              array of products , BBOX in LL, date range for rain, LC yy, outprefix
def run_downloader(prod_codes, bbox, rain_dates, lc_date, out_prefix):
    import os
    import urllib


    # productcode=esa-cci&productversion=V2.1.1&subproductcode=esa-cci-lc
    # chirps-dekad&productversion=2.0&subproductcode=1moncum
    # tamsat-rfe&productversion=3.1&subproductcode=1moncum
    # isric-wsi&productversion=1.0&subproductcode=af-orcdrc
    # isric-wsi&productversion=1.0&subproductcode=af-sndppt
    # isric-wsi&productversion=1.0&subproductcode=af-sltppt
    # isric-wsi&productversion=1.0&subproductcode=af-clyppt
    # srtm-de&productversion=V1.0&subproductcode=de

    URLs = []
    date = ''
    outname = ''

    # JRC_URL = 'http://s-jrciprap262p.jrc.it:8080/datamanagement/getproductlayerlite?'
    # JRC_URL = 'https://ies-ows.jrc.ec.europa.eu/iforce/rusle/download.py?'
    JRC_URL = 'https://estation.jrc.ec.europa.eu/eStation3/datamanagement/getproductlayerlite?'

    SRTM_singleTile = True

    if 's-jrciprap262p' in JRC_URL:
        print('Removing proxy if JRC internal server')
        os.environ['HTTP_PROXY'] = ''
        os.environ['HTTPS_PROXY'] = ''

    print("---------------------------------------")
    print(prod_codes)
    print("---------------------------------------")

    for productcode in prod_codes:

        if productcode=='chirps-dekad':
            continue
        if productcode=='tamsat-rfe':
            continue



        if productcode!='srtm-de':

            #  add buffer to keep the edge in case of large areas
            # bboxArray = bbox.split(',')
            # buffered_bbox = str(float(bboxArray[0])-1) + ',' + str(float(bboxArray[1])-1) + ',' + str(float(bboxArray[2])+1) + ',' + str(float(bboxArray[3])+1)
            # print('bufferetd BBox')
            # print(buffered_bbox)
            if productcode=='esa-cci':
                productversion='V2.1.1'
                subproductcode='esa-cci-lc'
                outname = out_prefix +'_'+ productcode +'_'+ str(lc_date)
                date = str(lc_date)

            if productcode=='af-orcdrc':
                productcode='isric-wsi'
                productversion='1.0'
                subproductcode='af-orcdrc'
                outname = out_prefix +'_'+ subproductcode

            if productcode=='af-sndppt':
                productcode = 'isric-wsi'
                productversion='1.0'
                subproductcode='af-sndppt'
                outname = out_prefix +'_'+ subproductcode

            if productcode=='af-sltppt':
                productcode = 'isric-wsi'
                productversion='1.0'
                subproductcode='af-sltppt'
                outname = out_prefix +'_'+ subproductcode

            if productcode=='af-clyppt':
                productcode = 'isric-wsi'
                productversion='1.0'
                subproductcode='af-clyppt'
                outname = out_prefix +'_'+ subproductcode

            if len(date) == 4:
                date += '0101'
            if len(date) == 6:
                date += '01'

            params = {
                "productcode": productcode,
                "productversion": productversion,
                "subproductcode": subproductcode,
                "BBOX": bbox
                }
            if date != '':
                params['date'] = date


            # urllib.request.urlcleanup()
            params = urllib.parse.urlencode(params)
            URL = JRC_URL+params
            URLs.append({'URL':URL,'name':outname+'.tif'})
            params = None

        # if productcode == 'srtm-de':
        else:
            # dedicated section to SRTM
            # if the size is too big, split into chunks

            productversion = 'V1.0'
            subproductcode = 'de'
            outname = out_prefix + '_' + productcode
            master_SRTM_outname = out_prefix + '_' + productcode
            SRTM_singleTile = True
            print('BBOX')
            print(bbox)

            bboxArray=bbox.split(',')
            print(bboxArray)


            # bbox is provided in LL
            if abs(float(bboxArray[3]) - float(bboxArray[1])) <= 4:
                SRTM_singleTile = True
                params = {
                    "productcode": productcode,
                    "productversion": productversion,
                    "subproductcode": subproductcode,
                    "BBOX": bbox
                }

                params = urllib.parse.urlencode(params)
                URL = JRC_URL+params
                URLs.append({'URL':URL,'name':outname+'.tif'})
            else:
                SRTM_singleTile = False
                tiles=[]
                UL = float(bboxArray[1])
                ct = 0
                while UL < float(bboxArray[3]):
                    ct +=1
                    params = {
                        "productcode": productcode,
                        "productversion": productversion,
                        "subproductcode": subproductcode,
                        "BBOX": bboxArray[0]+','+str(UL)+','+bboxArray[2]+','+ str(UL + 1)
                    }

                    params = urllib.parse.urlencode(params)
                    URL = JRC_URL + params
                    URLs.append({'URL': URL, 'name': outname+'_'+str(ct) + '.tif'})
                    tiles.append(outname+'_'+str(ct) + '.tif')
                    UL += 1


    # special case with rainfall data: need to get monthly data to compute the annual average in case of multiple YY
    for productcode in prod_codes:
        if productcode == 'chirps-dekad' or productcode == 'tamsat-rfe':
            #  loop all months
            date = int(rain_dates[0])
            YY = int(str(rain_dates[0])[0:4])
            MM = int(str(rain_dates[0])[4:])

            bboxArray = bbox.split(',')
            print(bbox)
            bbox1 = str(float(bboxArray[0]) - 0.2) + ',' + str(float(bboxArray[1]) - 0.2) + ',' + str(float(bboxArray[2]) + 0.2) + ',' + str(float(bboxArray[3]) + 0.2)
            print(bbox1)
            while date <= int(rain_dates[1]):

                outname = out_prefix + '_' + productcode + '_' + str(date)
                params = {
                    "productcode": productcode,
                    "productversion": '2.0' if productcode == 'chirps-dekad' else '3.1',
                    "subproductcode": '1moncum',
                    "BBOX": bbox1,
                    "date": str(date)+'01'
                }
                params = urllib.parse.urlencode(params)
                URL = JRC_URL + params
                URLs.append({'URL': URL, 'name': outname + '.tif'})


                # increase MM and YY counter

                if MM >= 12:
                    MM = 0
                    YY += 1
                MM += 1
                date = YY * 100 + MM



    try:
        pool = multiprocessing.Pool(2)
        results = []
        for URL in URLs:
            results.append(pool.apply_async(func=download_product, args=(URL.get('name'), URL.get('URL'))))
        pool.close()
        pool.join()

        jrc_files = []

        if SRTM_singleTile == False:
            # merge all tiles and return the single image
            # gdal_merge = "gdal_merge"
            # if (GLOBALS['OS'] == "unix"):
            #     gdal_merge = "gdal_merge.py"
            # tileStr = " ".join(tiles)
            # res1 = os.system(gdal_merge + ' -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES -o ' + master_SRTM_outname + '_tmp.tif ' + tileStr)
            # res1 = os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES ' + master_SRTM_outname + '_tmp.tif ' + master_SRTM_outname + '.tif')
            # os.remove(master_SRTM_outname + '_tmp.tif')
            gdal.BuildVRT(master_SRTM_outname+'.vrt',tiles)
            res1 = os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES ' + master_SRTM_outname + '.vrt ' + master_SRTM_outname+ '.tif ')


            for f in tiles:
                os.remove(f)

            if os.path.exists(master_SRTM_outname + '.tif'):
                jrc_files.append(master_SRTM_outname + '.tif')


        for URL in URLs:
            if os.path.exists(URL.get('name')):
                jrc_files.append(URL.get('name'))
        # results contains list of downloaded files including all monthly rain data




        return jrc_files


    except Exception as e:
        print(str(e))
        return jrc_files

if __name__ == "__main__":
    run_downloader()