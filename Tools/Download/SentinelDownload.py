#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import time
import json
import requests
from datetime import date

# Import local libraries
from __config__ import *
import LogStore

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Download Sentinel data from CDSE')
    print()
    sys.exit(1)


def getArgs(args):
    indir = None
    if (len(sys.argv) < 7):
        usage()
    else:
        uuids = json.loads(sys.argv[1])
        names = json.loads(sys.argv[2])
        outpath = sys.argv[3]
        usr = sys.argv[4]
        pwd = sys.argv[5]
        overwrite = sys.argv[6]

        return uuids, names, outpath, usr, pwd, overwrite


def get_access_token(username: str, password: str) -> str:
    data = {
        "client_id": "cdse-public",
        "username": username,
        "password": password,
        "grant_type": "password",
    }
    try:
        r = requests.post(
            "https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token",
            data=data,
        )
        r.raise_for_status()
    except Exception as e:
        return(str(e))
    return r.json()["access_token"]

def run(uuids, names, outpath, usr, pwd, overwrite):
    # initialize output log
    if outpath == '':
        outpath = GLOBALS['data_paths']['data']
    else:
        outpath = os.path.dirname(outpath)


    log = LogStore.LogProcessing('Sentinel Download', 'sentinelDownload')
    log.set_input_file(names)
    log.set_parameter('Output folder', outpath)
    log.set_parameter('Overwrite output', overwrite)


    try:
        # login not needed to query data
        try:
            access_token = get_access_token(usr, pwd)
            # print(access_token)
            if '401 Client' in  access_token or 'Unauthorized' in access_token:
                log.send_error(f"Access token creation failed. Please verify Username and Password")
                log.close()
                return
        except Exception as e :
            log.send_error(f"Access token creation failed. Response from the server was: {e}")
            log.close()
            return

        headers = {"Authorization": f"Bearer {access_token}"}
        session = requests.Session()
        session.headers.update(headers)

        ct=0
        ext = '.zip'

        for id in uuids:

            log.send_message("Downloading " + names[ct])
            name = names[ct].replace('.SAFE',ext)
            if os.path.exists(os.path.join(outpath, name)):
                if overwrite in ['No', False, 0, 'NO', 'N']:
                    log.send_warning("File already on disk: " + name)
                else:
                    os.remove(os.path.join(outpath,name))

            try:
                #is_online = api.is_online(id)
                is_online = True
                if not is_online:
                    log.send_error("File " + name + ' is not available online. Please visit the Copernicus website')
                    continue


                url = f"https://zipper.dataspace.copernicus.eu/odata/v1/Products({id})/$value"
                response = session.get(url, headers=headers, stream=True)
                with open(os.path.join(outpath,name), "wb") as file:
                    for chunk in response.iter_content(chunk_size=8192):
                        if chunk:
                            file.write(chunk)


            except Exception as e:
                log.send_error("Error downloading file: "+ names[ct] + ' '+str(e))

            finally:
                ct+=1
        #

        # api.download_all(products, directory_path=TMPDIR)

        log.send_message("Completed.")
    except Exception as e:
        log.send_error("Error downloading files:" + str(e))



    # time.sleep(1)
    log.close()


if __name__ == "__main__":
    uuids, names, outpath, usr, pwd, overwrite = getArgs(sys.argv)
    run(uuids, names, outpath, usr, pwd, overwrite)
