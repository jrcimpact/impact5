#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import time
import json

from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date

# Import local libraries
from __config__ import *
import LogStore

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Download Sentinel data from ESA HUB using sentinelsat libs')
    print()
    sys.exit(1)


def getArgs(args):
    indir = None
    if (len(sys.argv) < 10):
        usage()
    else:
        shp_names = json.loads(sys.argv[1])
        sat = sys.argv[2]
        startDate = sys.argv[3]
        endDate = sys.argv[4]
        clouds = sys.argv[5]
        quicklooks = sys.argv[6]
        usr = sys.argv[7]
        pwd = sys.argv[8]
        overwrite = sys.argv[9]

        return shp_names, sat, startDate, endDate, clouds, quicklooks, usr, pwd, overwrite


def run(shp_names, sat, startDate, endDate, clouds, quicklooks, usr, pwd, overwrite):
    # initialize output log
    log = LogStore.LogProcessing('SentinelDownload', 'SentinelDownload')
    log.set_input_file(shp_names)
    log.set_parameter('Satellite', sat)
    log.set_parameter('Start Date', startDate)
    log.set_parameter('End Date', endDate)
    log.set_parameter('Max Clouds %', clouds)
    log.set_parameter('Only Quicklooks', quicklooks)
    log.set_parameter('Overwrite output', overwrite)

    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "download")
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)
    else:
        result = [print(file) for file in
                  (os.path.join(path, file) for path, _, files in os.walk(TMPDIR) for file in files) if
                  os.stat(file).st_mtime < time() - 1 * 20]
        print(result)

    try:
        for shp in shp_names:
            OUTDir = os.path.dirname(shp)
            shpname = os.path.basename(shp)
            shpbasename = shpname.replace(".shp", "")
            log.send_message("Using SHP:" + shpbasename)

            tmpshp = os.path.join(TMPDIR,shpbasename+'.json')
            if os.path.exists(tmpshp):
                os.remove(tmpshp)
            # ---------- OPEN SHP -----------
            os.system('ogr2ogr -f GeoJSON -t_srs EPSG:4326 -simplify 0.01 ' + tmpshp + ' '+ shp)


            api = SentinelAPI(usr, pwd, 'https://apihub.copernicus.eu/apihub/')
            # api = SentinelAPI(usr, pwd, 'https://scihub.copernicus.eu/dhus/')

            # search by polygon, time, and SciHub query keywords
            footprint = geojson_to_wkt(read_geojson(tmpshp))
            products = api.query(footprint,
                                 date=(startDate, endDate),
                                 platformname='Sentinel-2,',
                                 cloudcoverpercentage=(0, int(clouds)),
                                 # show_progressbars=True,
                                 order_by='cloudcoverpercentage',
                                 # area_relation({'Intersects', 'Contains', 'IsWithin'}
                                 )

            for i in products:
                # print(type(products[i]))
                print(products[i]['identifier'])
                if os.path.exists(os.path.join(OUTDir,products[i]['identifier'])+'.zip') :
                    if overwrite in ['No', False, 0, 'NO', 'N']:
                        log.send_warning("File already on disk: " + products[i]['identifier'] )
                    else:
                        os.remove(os.path.join(OUTDir,products[i]['identifier'])+'.zip')
                    continue
                try:
                    if quicklooks in ['No', False, 0, 'NO', 'N']:
                        api.download(products[i]['uuid'], directory_path=OUTDir)
                    else:
                        api.download_quicklook(products[i]['uuid'], directory_path=OUTDir)

                except Exception as e:
                    log.send_error("Error downloading file: "+ products[i]['identifier'] + ' '+str(e))
            #

            # api.download_all(products, directory_path=TMPDIR)

        log.send_message("Completed.")
    except Exception as e:
        log.send_error("Error downloading files:" + str(e))
        try:
            if os.path.exists(tmpshp):
                os.remove(tmpshp)
        except Exception as e:
            pass

    # time.sleep(1)
    log.close()


if __name__ == "__main__":
    shp_names, sat, startDate, endDate, clouds, quicklooks, usr, pwd, overwrite = getArgs(sys.argv)
    run(shp_names, sat, startDate, endDate, clouds, quicklooks, usr, pwd, overwrite)
