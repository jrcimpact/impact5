# Application / software name
IMPACT Toolbox offers a combination of remote sensing, photo interpretation and processing technologies in a portable and stand-alone GIS environment, allowing non specialist users to easily accomplish all necessary pre-processing steps while giving a fast and user-friendly environment for visual editing and map validation. No installation or virtual machines are required.

# Features
Satellite data processing, classification, spectral unmixing, editing, image segmentation, statistics, land-cover/use mapping, deforestation, degradation and carbon emission estimation

# Documentation
Web: http://forobs.jrc.ec.europa.eu/products/software/impact.php

# Libraries

Mapserver 7.6.1	MIT-style license (X11 License? http://www.gnu.org/licenses/license-list.html#X11License ) 	http://www.mapserver.org/
Openlayers 2.13.1 	Modified BSD (http://www.gnu.org/licenses/license-list.html#ModifiedBSD) 	http://openlayers.org/
ExtJs 7.2 	GPLv3+ (https://directory.fsf.org/wiki/Ext_JS#tab=Details ) 	https://www.sencha.com/
Python 3.6	GPL compatible 	https://www.python.org/
Scipy	BSD License Terms	https://www.scipy.org/
Mahotas MIT https://pypi.org/project/mahotas/
Numpy 	BSD 3Clause (https://directory.fsf.org/wiki/NumPy#tab=Details ) 	http://www.numpy.org/
Proj4js 	https://github.com/proj4js/proj4js/blob/master/LICENSE.md 	http://proj4js.org/
GDAL 3.1.3	X/MIT style Open Source 	http://www.gdal.org/
Terra Lib 5	LGPL (http://www.gnu.org/licenses/lgpl.html) 	http://www.dpi.inpe.br/terralib5/wiki/doku.php
Git  v.2.9.2 windows	GPL v2	https://git-scm.com
Google Earth Engine Python Api Apache License, Version 2.0 https://developers.google.com/earth-engine/guides/python_install
Geopandas BSD-licensed  https://geopandas.org/


# Contact
Please refer to contacts available on our website http://forobs.jrc.ec.europa.eu/products/software/impact.php

# Funding

# Disclaimer
The JRC, or as the case may be the European Commission, shall not be  held liable for any direct or indirect, incidental,
consequential or  other damages, including but not limited to the loss of data, loss of  profits,
or any other financial loss arising from the use of this application, or inability to use it,
even if the JRC is notified of the possibility of such damages.

# License
This version of IMPACT Toolbox is licensed under the terms of the Open Source GPL 3.0 license.
This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
NON-INFRINGEMENT OF THIRD-PARTY INTELLECTUAL PROPERTY RIGHTS. See the GNU General Public License
for more details.