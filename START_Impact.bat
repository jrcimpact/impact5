@echo OFF

REM
REM Copyright (c) 2016, European Union
REM All rights reserved.
REM Authors: Simonetti Dario, Marelli Andrea
REM
REM This file is part of IMPACT toolbox.
REM
REM IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
REM GNU General Public License as published by the Free Software Foundation, either version 3 of
REM the License, or (at your option) any later version.
REM
REM IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
REM without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
REM See the GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along with IMPACT toolbox.
REM If not, see <http://www.gnu.org/licenses/>.

echo.
echo ####################################################################
echo ####################################################################
echo ##############                                        ##############
echo ##############             IMPACT Toolbox             ##############
echo ##############                                        ##############
echo ##############   Copyright (c) 2016, European Union   ##############
echo ##############          All rights reserved           ##############
echo ##############                                        ##############
echo ####################################################################
echo ####################################################################
echo.
echo Starting IMPACT Toolbox...

    set IMPACT_ROOT=%~sdp0
    set IMPACT_ROOT=%IMPACT_ROOT:~0,-1%

	CALL :GetLongPathname %IMPACT_ROOT% IMPACT_ROOT

	echo "--------------------------------------"
	echo %IMPACT_ROOT%
	echo "--------------------------------------"

REM ########################################################
REM ######  Check Requirements and clean environment  ######
REM ########################################################

	REM ####### Check if path does not contain spaces #######
    If NOT "%IMPACT_ROOT%"=="%IMPACT_ROOT: =%" (
        echo Error in starting IMPACT ...
        echo Please ensure IMPACT path does not contain spaces.
        echo.
        set /p=Press ENTER to continue ...
        EXIT /b
    )

    REM #####  Remove installation file (if exists) #####
    if exist %IMPACT_ROOT%\INSTALL.bat del %IMPACT_ROOT%\INSTALL.bat


    REM #####  Close running instances  #####
    if exist %IMPACT_ROOT%\Gui\tmp\impact.pid (
        for /f "delims=" %%x in ('type %IMPACT_ROOT%\Gui\tmp\impact.pid') do (
            taskkill /fi "IMAGENAME eq cmd.exe" /fi "WINDOWTITLE eq IMPACT*" /fi "PID eq %%x" >nul 2>&1
        )
    )
    taskkill /fi "IMAGENAME eq cmd.exe" /fi "WINDOWTITLE eq IMPACT Toolbox*" >nul 2>&1
    taskkill /fi "IMAGENAME eq cmd.exe" /fi "WINDOWTITLE eq Updating IMPACT*" >nul 2>&1

    REM taskkill /fi "IMAGENAME eq firefox.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq chrome.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq opera.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq msedge.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq iexplore.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1

    taskkill /F /IM nginx.exe >nul 2>&1


    REM #####  Clean temporary files and folders  #####
    REM del %IMPACT_ROOT%\resources\db\logs.db /Q
    REM CALL :clean_folder %IMPACT_ROOT%\Gui\tmp\data
    REM CALL :clean_folder %IMPACT_ROOT%\resources\logs
    CALL :clean_folder %IMPACT_ROOT%\DATA\processing_tmp


REM #####################################################
REM #######  Set paths and environment variables  #######
REM #####################################################

    set PATH=%IMPACT_ROOT%\Libs\win64\Git\bin\


    set SERVER_URL=127.0.0.1
    set IMPACT_NGINX_PORT=8899
    REM CHANGE IMPACT_NGINX_PORT  IN NGINX .conf file too if not 99999


    REM Tornado
    set IMPACT_HTTP_HOST=%SERVER_URL%:%IMPACT_NGINX_PORT%
    REM NGINx load balancer for WMS requests


    REM IMPACT_HTTP_INTERNAL_PORT is not exposed outside but used py NGINX proxy_pass
    set IMPACT_HTTP_INTERNAL_PORT=9999

    REM set remode data disk
    set REMOTE_DATA_VOLUME=D:/REMOTE_DATA
    echo Remote DATA volume mapped in %REMOTE_DATA_VOLUME%

    REM #####  Set GDAL paths  #####
	REM ### python38 PATH dll loader
	REM set USE_PATH_FOR_GDAL_PYTHON=YES


	REM #####  Set PYTHON params  #####
    set PATH=%IMPACT_ROOT%\Libs\win64\Python36;%IMPACT_ROOT%\Libs\win64\Python36\Scripts;%PATH%
    set PATH=%IMPACT_ROOT%\Tools\Segmentation\libs\;%PATH%

    set PYTHONPATH=%IMPACT_ROOT%\Libs\win64\Python36;%IMPACT_ROOT%\Libs\win64\Python36\Lib\site-packages\gitdb;%IMPACT_ROOT%\rLibs\win64\Python36\Lib\site-packages;
    set PYTHONHOME=%IMPACT_ROOT%\Libs\win64\Python36
    set PYTHONIOENCODING="utf-8"


    REM #####  Set Python paths  #####

    set PYTHONPATH=%IMPACT_ROOT%\Gui\libs_python\modules\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Gui\libs_python\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\Segmentation\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\Pre-Processing\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\TOA_Reflectance\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Libs\win64\GDAL\;%PYTHONPATH%

    set PATH=%IMPACT_ROOT%\Libs\win64\GDAL;%IMPACT_ROOT%\Libs\win64\GDAL\proj6\apps;%PATH%
    set GDAL_DATA=%IMPACT_ROOT%\Libs\win64\GDAL\gdal-data
    set PROJ_LIB=%IMPACT_ROOT%\Libs\win64\GDAL\projlib6

    set GDAL_DRIVER_PATH=%IMPACT_ROOT%\Libs\win64\GDAL\gdalplugins
    set OGR_SQLITE_CACHE=1024
    REM #####  Set WINDOWS paths  #####
    set PATH=%PATH%;%WINDIR%;%WINDIR%\System32\;%WINDIR%\System32\WBem;

    rem Enable support to VRT processing functions
    set GDAL_VRT_ENABLE_PYTHON=YES

@echo OFF

REM #########################################################
REM #######  Initialize environment and start IMPACT  #######
REM #########################################################

    REM #####  Retrieve process own PID and save to file   #####
    title iMpCtTbX
    for /f "tokens=2 delims==; " %%a in ('tasklist /v /nh /fi "IMAGENAME eq cmd.exe*" /fi "WINDOWTITLE eq iMpCtTbX"') do (
        echo %%a
        echo %%a>%IMPACT_ROOT%\Gui\tmp\impact.pid
    )

    title IMPACT Toolbox

    REM #####  Tell git to ignore test DATA  #####
    echo Ignoring DATA folder
    CALL :ignore_tracked %IMPACT_ROOT%\DATA\Landsat_test_data

    echo Starting NGINx
    REM ### start NGIHX
    cd %IMPACT_ROOT%\Libs\win64\nginx\
    mkdir temp >nul 2>&1
    start /B "nginx" nginx.exe
    cd %IMPACT_ROOT%
    REM del START_impact.sh >nul 2>&1


    REM ### start web server
    REM Tornado server for http events and processing listening on internal port 8888
    start /B "tornado" python %IMPACT_ROOT%/Gui/libs_python/my_server.py %IMPACT_HTTP_INTERNAL_PORT%

    REM Tornado servers for wms tiling listening on different ports (coded into nginx conf file as well)
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9998
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9997
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9996
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9995
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9994
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9993
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9992
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9991
    start /B "mapscript" python %IMPACT_ROOT%/Gui/libs_python/my_server_wms.py 127.0.0.1 9990


    timeout 10
    REM #####  Start Browser  #####

    set IE_browser=%ProgramFiles%\Internet Explorer\iexplore.exe
    set EDGE_browser=%ProgramFiles(x86)%\Microsoft\Edge\Application\msedge.exe 
    set FX_browser=%ProgramFiles%\Mozilla Firefox\firefox.exe
    set CR_browser=%ProgramFiles(x86)%\Google\Chrome\Application\chrome.exe

  
	
    if exist "%EDGE_browser%" (
		echo Starting Impact with Edge
		echo "%EDGE_browser%"
        start microsoft-edge:"http://%SERVER_URL%:%IMPACT_NGINX_PORT%/IMPACT"
    ) else if exist "%FX_browser%" (
        echo Starting Impact with Firefox
        "%FX_browser%" --new-window http://%SERVER_URL%:%IMPACT_NGINX_PORT%/IMPACT
    ) else if exist "%CR_browser%" (
        echo Starting Impact with Chrome
        "%CR_browser%" --new-window http://%SERVER_URL%:%IMPACT_NGINX_PORT%/IMPACT
    ) else (
        echo Starting Impact with default browser
        start http://%SERVER_URL%:%IMPACT_NGINX_PORT%/IMPACT 
    )


    echo IMPACT Toolbox successfully started.
    echo NGINx    :  http://%SERVER_URL%:%IMPACT_NGINX_PORT%/.
    echo.

    set HTTP_PROXY=
    set HTTPS_PROXY=

    cmd


REM #################################################
REM ############  Functions definitions  ############
REM #################################################

	:GetLongPathname
	REM %1=PATHNAME %2=Output
	setlocal EnableDelayedExpansion
	set "FULL_SHORT=%~fs1"           &:# Make sure it really is short all the way through
	set "FULL_SHORT=%FULL_SHORT:~3%" &:# Remove the drive and initial \
	set "FULL_LONG=%~d1"             &:# Begin with just the drive
	if defined FULL_SHORT for %%x in ("!FULL_SHORT:\=" "!") do ( :# Loop on all short components
	  set "ATTRIB_OUTPUT=" &:# If the file does not exist, filter-out attrib.exe error message on stdout, with its - before the drive.
	  for /f "delims=" %%l in ('attrib "!FULL_LONG!\%%~x" 2^>NUL ^| findstr /v /c:" - %~d1"') do set "ATTRIB_OUTPUT=%%l"
	  if defined ATTRIB_OUTPUT ( :# Extract the long name from the attrib.exe output
		for %%f in ("!ATTRIB_OUTPUT:*\=\!") do set "LONG_NAME=%%~nxf"
	  ) else (                   :# Use the short name (which does not exist)
		set "LONG_NAME=%%~x"
	  )
	  set "FULL_LONG=!FULL_LONG!\!LONG_NAME!"
	) else set "FULL_LONG=%~d1\"
	endlocal & if not "%~2"=="" (set "%~2=%FULL_LONG%") else echo %FULL_LONG%
	exit /b

    REM ---- Tell git to ignore tracked files in given path ----
    :ignore_tracked
    for /f "delims=" %%i in ('dir %1 /b') do (
        git update-index --assume-unchanged %1\%%i >nul 2>&1
    )
    EXIT /b


    REM ---- Clean folder ----
    :clean_folder
    echo "Cleaning temp folder ......"
    forfiles /p %1 /s /m *.* /D -2 /C "cmd /c del @path /Q" >nul 2>&1
    EXIT /b
