@echo OFF

REM
REM Copyright (c) 2016, European Union
REM All rights reserved.
REM Authors: Simonetti Dario, Marelli Andrea
REM
REM This file is part of IMPACT toolbox.
REM
REM IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
REM GNU General Public License as published by the Free Software Foundation, either version 3 of
REM the License, or (at your option) any later version.
REM
REM IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
REM without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
REM See the GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along with IMPACT toolbox.
REM If not, see <http://www.gnu.org/licenses/>.

echo.
echo ####################################################################
echo ####################################################################
echo ##############                                        ##############
echo ##############             IMPACT Toolbox             ##############
echo ##############                                        ##############
echo ##############   Copyright (c) 2016, European Union   ##############
echo ##############          All rights reserved           ##############
echo ##############                                        ##############
echo ####################################################################
echo ####################################################################
echo.
echo Starting IMPACT Toolbox Commandline ...

    set IMPACT_ROOT=%~sdp0
    set IMPACT_ROOT=%IMPACT_ROOT:~0,-1%
	echo %IMPACT_ROOT%
	set IMPACT_NGINX_PORT=8899

	CALL :GetLongPathname %IMPACT_ROOT% IMPACT_ROOT

	echo "--------------------------------------"
	echo %IMPACT_ROOT%
	echo "--------------------------------------"

REM ########################################################
REM ######  Check Requirements and clean environment  ######
REM ########################################################

	REM ####### Check if path does not contain spaces #######
    If NOT "%IMPACT_ROOT%"=="%IMPACT_ROOT: =%" (
        echo Error in starting IMPACT ...
        echo Please ensure IMPACT path does not contain spaces.
        echo.
        set /p=Press ENTER to continue ...
        EXIT /b
    )

    
REM #####################################################
REM #######  Set paths and environment variables  #######
REM #####################################################

    set PATH=%IMPACT_ROOT%\Libs\win64\Git\bin\

  	REM #####  Set PYTHON params  #####
    set PATH=%IMPACT_ROOT%\Libs\win64\Python36;%IMPACT_ROOT%\Libs\win64\Python36\Scripts;%PATH%
    set PATH=%IMPACT_ROOT%\Tools\Segmentation\libs\;%PATH%

    set PYTHONPATH=%IMPACT_ROOT%\Libs\win64\Python36;%IMPACT_ROOT%\Libs\win64\Python36\Lib\site-packages\gitdb;%IMPACT_ROOT%\rLibs\win64\Python36\Lib\site-packages;
    set PYTHONHOME=%IMPACT_ROOT%\Libs\win64\Python36
    set PYTHONIOENCODING="utf-8"


    REM #####  Set Python paths  #####

    set PYTHONPATH=%IMPACT_ROOT%\Gui\libs_python\modules\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Gui\libs_python\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\Segmentation\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\Pre-Processing\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Tools\TOA_Reflectance\;%PYTHONPATH%
    set PYTHONPATH=%IMPACT_ROOT%\Libs\win64\GDAL\;%PYTHONPATH%

    set PATH=%IMPACT_ROOT%\Libs\win64\GDAL;%IMPACT_ROOT%\Libs\win64\GDAL\proj6\apps;%PATH%
    set GDAL_DATA=%IMPACT_ROOT%\Libs\win64\GDAL\gdal-data
    set PROJ_LIB=%IMPACT_ROOT%\Libs\win64\GDAL\projlib6

    set GDAL_DRIVER_PATH=%IMPACT_ROOT%\Libs\win64\GDAL\gdalplugins
    set OGR_SQLITE_CACHE=1024
    REM #####  Set WINDOWS paths  #####
    set PATH=%PATH%;%WINDIR%;%WINDIR%\System32\;%WINDIR%\System32\WBem;

@echo OFF


    title IMPACT Toolbox Commandline
    cd %IMPACT_ROOT%
	CMD


REM #################################################
REM ############  Functions definitions  ############
REM #################################################

	:GetLongPathname %1=PATHNAME %2=Output variable name
	setlocal EnableDelayedExpansion
	set "FULL_SHORT=%~fs1"           &:# Make sure it really is short all the way through
	set "FULL_SHORT=%FULL_SHORT:~3%" &:# Remove the drive and initial \
	set "FULL_LONG=%~d1"             &:# Begin with just the drive
	if defined FULL_SHORT for %%x in ("!FULL_SHORT:\=" "!") do ( :# Loop on all short components
	  set "ATTRIB_OUTPUT=" &:# If the file does not exist, filter-out attrib.exe error message on stdout, with its - before the drive.
	  for /f "delims=" %%l in ('attrib "!FULL_LONG!\%%~x" 2^>NUL ^| findstr /v /c:" - %~d1"') do set "ATTRIB_OUTPUT=%%l"
	  if defined ATTRIB_OUTPUT ( :# Extract the long name from the attrib.exe output
		for %%f in ("!ATTRIB_OUTPUT:*\=\!") do set "LONG_NAME=%%~nxf"
	  ) else (                   :# Use the short name (which does not exist)
		set "LONG_NAME=%%~x"
	  )
	  set "FULL_LONG=!FULL_LONG!\!LONG_NAME!"
	) else set "FULL_LONG=%~d1\"
	endlocal & if not "%~2"=="" (set "%~2=%FULL_LONG%") else echo %FULL_LONG%
	exit /b

   