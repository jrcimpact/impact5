#!/usr/bin/env bash
#
# -------------------------------------------------
# USER  IMPACT  conf
if ! which realpath &> /dev/null
then
    function realpath()
    {
        if [[ "${1}" == /* ]]
        then
            echo "${1}"
        else
            echo "${PWD}/${1#./}"
        fi
    }
fi

#---------------------------------------------------------
# ---------------------    START     ---------------------
#---------------------------------------------------------

# directory of the script, replace with other path if needed
IMPACT_VOLUME="$(realpath "$(dirname "${0}")")"
echo $IMPACT_VOLUME
# directory containing DATA folder, default is under current directory
IMPACT_DATA_VOLUME=${IMPACT_VOLUME}/DATA/

# directory pointing to network drive or other mapped directory containing
# raw sat data (Sentinel / Landsat ) e.g. Download etc as downloaded by
# other services e.g. JRC Climate Station
REMOTE_DATA_VOLUME=${IMPACT_VOLUME}/REMOTE_DATA/

# Server ulr or 127.0.0.1 for local use
# JRC dev url: s-jrciprap262p.jrc.it
# If not secified the url+port is taken from the window.location.href in Application.js
SERVER_URL=

# NGINx proxy_pass and load balancer for WMS requests
IMPACT_NGINX_PORT=8899

# Proxy url:port  JRC:  http://10.168.209.73:8012
HTTP_PROXY=
NO_PROXY="127.0.0.1, localhost"

# HTTP PORT listening for IMPACT requests
# inside the container, non need to change
IMPACT_HTTP_INTERNAL_PORT=9999


# -------------------------------------------------
# BUILDING VARIABLES

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

if [[ $OSTYPE == 'darwin'* ]]; then
  echo 'macOS'
  export USER_ID=12345
  export GROUP_ID=12345
fi
export HTTP_PROXY=$HTTP_PROXY
export http_proxy=$HTTP_PROXY
export HTTPS_PROXY=$HTTP_PROXY
export https_proxy=$HTTP_PROXY
export NO_PROXY=$NO_PROXY


# dafault data location in Impact/DATA
export IMPACT_DATA_VOLUME=$IMPACT_DATA_VOLUME
export REMOTE_DATA_VOLUME=$REMOTE_DATA_VOLUME

# if SERVER_URL is not provided the url and port is taken from the JS caller/parent in Application.js
IMPACT_HTTP_HOST=''
if [[ $SERVER_URL != '' ]]; then
  echo 'IMPACT_HTTP_HOST = SERVER_URL : IMPACT_NGINX_PORT '
  IMPACT_HTTP_HOST=$SERVER_URL:$IMPACT_NGINX_PORT
fi




ENV_FILE=".env"

mkdir -p $IMPACT_DATA_VOLUME
mkdir -p $IMPACT_DATA_VOLUME/db

mkdir -p $REMOTE_DATA_VOLUME
# -------------------------------------------------
# BUILDING  .ENV  file

echo "# Volumes

IMPACT_DATA_VOLUME=$IMPACT_DATA_VOLUME
REMOTE_DATA_VOLUME=$REMOTE_DATA_VOLUME

SERVER_URL=$SERVER_URL
IMPACT_NGINX_PORT=$IMPACT_NGINX_PORT

IMPACT_HTTP_HOST=$IMPACT_HTTP_HOST

IMPACT_HTTP_INTERNAL_PORT=$IMPACT_HTTP_INTERNAL_PORT

USER_ID=$USER_ID
GROUP_ID=$GROUP_ID

# Proxy settings
HTTP_PROXY=$HTTP_PROXY
http_proxy=$HTTP_PROXY
HTTPS_PROXY=$HTTP_PROXY
https_proxy=$HTTP_PROXY
NO_PROXY=\"$NO_PROXY\"


" > $IMPACT_VOLUME/$ENV_FILE


#docker pull mydronedocker/impact5
#docker stop impact5
#docker rm impact5
docker run -d --restart unless-stopped --env-file .env -v $IMPACT_DATA_VOLUME:/data -v $REMOTE_DATA_VOLUME:/remote_data -p $IMPACT_NGINX_PORT:8899 --name impact5 mydronedocker/impact5:latest 


