@echo OFF
REM
REM Copyright (c) 2015, European Union
REM All rights reserved.
REM Authors: Simonetti Dario, Marelli Andrea
REM
REM This file is part of IMPACT toolbox.
REM
REM IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
REM GNU General Public License as published by the Free Software Foundation, either version 3 of
REM the License, or (at your option) any later version.
REM
REM IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
REM without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
REM See the GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along with IMPACT toolbox.
REM If not, see <http://www.gnu.org/licenses/>.

title Updating IMPACT...
echo.
echo ####################################################################
echo ####################################################################
echo ##############                                        ##############
echo ##############             IMPACT Toolbox             ##############
echo ##############                                        ##############
echo ##############   Copyright (c) 2016, European Union   ##############
echo ##############          All rights reserved           ##############
echo ##############                                        ##############
echo ####################################################################
echo ####################################################################
echo.

REM ##### Set paths #####
    set IMPACT_ROOT=%~sdp0
    set IMPACT_ROOT=%IMPACT_ROOT:~0,-12%
    echo %IMPACT_ROOT%
    set PATH=%IMPACT_ROOT%\Libs\win64\Git_tmp\bin\;%PATH%;;%WINDIR%;%WINDIR%\System32\;%WINDIR%\WBem

REM #####  Close running instances  #####
    echo Closing running instances ...
    if exist %IMPACT_ROOT%\Gui\tmp\impact.pid (
        for /f "delims=" %%x in ('type %IMPACT_ROOT%\Gui\tmp\impact.pid') do (
            taskkill /fi "IMAGENAME eq cmd.exe" /fi "WINDOWTITLE eq IMPACT*" /fi "PID eq %%x" >nul 2>&1
        )
    )

    taskkill /fi "IMAGENAME eq cmd.exe" /fi "WINDOWTITLE eq IMPACT Toolbox*" >nul 2>&1

	REM taskkill /fi "IMAGENAME eq firefox.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq chrome.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq opera.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq msedge.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1
    REM taskkill /fi "IMAGENAME eq iexplore.exe" /fi ^"WINDOWTITLE eq IMPACT Toolbox*^" >nul 2>&1


    taskkill /F /IM nginx.exe >nul 2>&1
    taskkill /F /IM python.exe >nul 2>&1
    taskkill /F /IM segmentation_terralib5_standalone.exe >nul 2>&1

    echo All process closed.
    echo.
    timeout 5
REM #####  Create temporary git copy #####
    echo Creating temporary git copy ...
    XCOPY %IMPACT_ROOT%\Libs\win64\Git %IMPACT_ROOT%\Libs\win64\Git_tmp\* /s /q /y
    echo Done
    echo.
	timeout 5
REM #####  Update IMPACT  #####
    echo Updating IMPACT  ...
    git reset --hard
    git pull
    echo.
    timeout 5
REM #####  Remove temporary git copy #####
    echo Removing temporary git copy ...
    RMDIR %IMPACT_ROOT%\Libs\win64\Git_tmp /s /q
    echo Done
    echo.


REM #####  Start IMPACT #####
    echo Starting IMPACT ...
    echo.
    @timeout 5 >nul
    start %IMPACT_ROOT%\START_Impact.bat


REM ---- Clean folder ----
    :clean_folder
    for %%i in (%1\*) do (
        if not %%i == %1\.gitkeep (
            del "%%i" /Q
        )
    )
    EXIT /b