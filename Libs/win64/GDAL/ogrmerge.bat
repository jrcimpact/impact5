@echo OFF

REM
REM Copyright (c) 2015, European Union
REM All rights reserved.
REM Authors: Simonetti Dario, Marelli Andrea
REM
REM This file is part of IMPACT toolbox.
REM
REM IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
REM GNU General Public License as published by the Free Software Foundation, either version 3 of
REM the License, or (at your option) any later version.
REM
REM IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
REM without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
REM See the GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along with IMPACT toolbox.
REM If not, see <http://www.gnu.org/licenses/>.


set HERE=%~sdp0
python %HERE%/ogrmerge.py %*