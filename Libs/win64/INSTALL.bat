@echo OFF
REM
REM Copyright (c) 2015, European Union
REM All rights reserved.
REM Authors: Simonetti Dario, Marelli Andrea
REM
REM This file is part of IMPACT toolbox.
REM
REM IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
REM GNU General Public License as published by the Free Software Foundation, either version 3 of
REM the License, or (at your option) any later version.
REM
REM IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
REM without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
REM See the GNU General Public License for more details.
REM
REM You should have received a copy of the GNU General Public License along with IMPACT toolbox.
REM If not, see <http://www.gnu.org/licenses/>.

title Installing IMPACT...
echo.
echo ####################################################################
echo ####################################################################
echo ##############                                        ##############
echo ##############             IMPACT Toolbox             ##############
echo ##############                                        ##############
echo ##############   Copyright (c) 2016, European Union   ##############
echo ##############          All rights reserved           ##############
echo ##############                                        ##############
echo ####################################################################
echo ####################################################################
echo.

REM ##### Set paths #####
    set IMPACT_ROOT=%~sdp0
    set IMPACT_ROOT=%IMPACT_ROOT:~0,-1%
    set PATH=%IMPACT_ROOT%\Libs\win64\Git_tmp\bin\;%PATH%;;%WINDIR%;%WINDIR%\System32\;%WINDIR%\WBem

REM #####  Install IMPACT (from git)  #####
    echo Installing IMPACT...
    git reset --hard
    echo Installation completed
    echo.

REM #####  Remove temporary git copy #####
    echo Removing temporary git copy ...
    RMDIR %IMPACT_ROOT%\Libs\win64\Git_tmp /s /q
    echo Done
    echo.

REM #####  Start IMPACT #####
    echo Starting IMPACT ...
    echo.
    @timeout 3
    if exist START_Impact.bat start call START_Impact.bat








