#!/usr/bin/env bash
#
# -------------------------------------------------
# USER  IMPACT  conf
# Volumes

# directory of the script, replace if needed
IMPACT_VOLUME="$(dirname "$(readlink -f "$0")")"
IMPACT_VOLUME=$(echo $IMPACT_VOLUME | sed "s/\/Libs\/unix//")

echo $IMPACT_VOLUME

IMPACT_DATA_VOLUME=${IMPACT_VOLUME}/DATA/

# directory pointing to network drive or other mapped directory containing raw sat data (Sentinel / Landsat ) e.g. Download etc
REMOTE_DATA_VOLUME=${IMPACT_VOLUME}/DATA/

# Server ulr or 127.0.0.1 for local use
# JRC dev url: s-jrciprap262p.jrc.it
SERVER_URL=127.0.0.1

# NGINx proxy_pass and load balancer for WMS requests
IMPACT_NGINX_PORT=8899

# Proxy url:port  JRC:  http://10.168.209.73:8012
HTTP_PROXY=
NO_PROXY="127.0.0.1, localhost"

# HTTP PORT listening for IMPACT requests
#IMPACT_HTTP_INTERNAL_PORT=9999

# image REGISTRY JRC: d-prd-registry.jrc.it/  # NOTE ends with /
IMAGE_PREFIX=""

# contry ID of the local mirror of Ubuntu sources : [it,fr,en, ...]
# local mirror should speed up the download
# default http://archive* will be replaced with http://[code].archive*
UBUNTU_SOURCE_MIRROR_CODE=it
# END  OF  USER  IMPACT  conf
# -------------------------------------------------


# -------------------------------------------------
# BUILDING VARIABLES

export USER_ID=12345
export GROUP_ID=12345
export HTTP_PROXY=$HTTP_PROXY
export http_proxy=$HTTP_PROXY
export HTTPS_PROXY=$HTTP_PROXY
export https_proxy=$HTTP_PROXY
export NO_PROXY=$NO_PROXY

export UBUNTU_SOURCE_MIRROR_CODE=$UBUNTU_SOURCE_MIRROR_CODE
export IMAGE_PREFIX=$IMAGE_PREFIX
# dafault data location in Impact/DATA
export IMPACT_DATA_VOLUME=$IMPACT_DATA_VOLUME
export REMOTE_DATA_VOLUME
IMPACT_HTTP_HOST=$SERVER_URL:$IMPACT_NGINX_PORT


ENV_FILE=".env"

# -------------------------------------------------
# BUILDING  .ENV  file

echo "# Volumes
IMPACT_VOLUME=$IMPACT_VOLUME
IMPACT_DATA_VOLUME=$IMPACT_DATA_VOLUME
REMOTE_DATA_VOLUME=$REMOTE_DATA_VOLUME

SERVER_URL=$SERVER_URL
IMPACT_NGINX_PORT=$IMPACT_NGINX_PORT

IMPACT_HTTP_HOST=$SERVER_URL:$IMPACT_NGINX_PORT

IMPACT_HTTP_INTERNAL_PORT=$IMPACT_HTTP_INTERNAL_PORT


USER_ID=$USER_ID
GROUP_ID=$GROUP_ID

# Proxy settings
HTTP_PROXY=$HTTP_PROXY
http_proxy=$HTTP_PROXY
HTTPS_PROXY=$HTTP_PROXY
https_proxy=$HTTP_PROXY
NO_PROXY=\"$NO_PROXY\"

ENV_FILE=$ENV_FILE
IMAGE_PREFIX=$IMAGE_PREFIX
UBUNTU_SOURCE_MIRROR_CODE=$UBUNTU_SOURCE_MIRROR_CODE
" > $IMPACT_VOLUME/Libs/unix/$ENV_FILE

# replace the NGINX PORT on CONF file
# replace the NGINX PORT on CONF file
#sed -i -- 's/listen [0-9]*[0-9]; #REPLACE/listen '$IMPACT_NGINX_PORT'; #REPLACE/g' "${IMPACT_VOLUME}/Libs/unix/build-docker/impact/nginx.conf"
#sed -i -- 's/127.0.0.1:[0-9]*[0-9]; #REPLACE/127.0.0.1:'$IMPACT_HTTP_INTERNAL_PORT'; #REPLACE/g' "${IMPACT_VOLUME}/Libs/unix/build-docker/impact/nginx.conf"
#sed -i -- 's/127.0.0.1:[0-9]*[0-9]\/; #REPLACE/127.0.0.1:'$IMPACT_HTTP_INTERNAL_PORT'\/; #REPLACE/g' "${IMPACT_VOLUME}/Libs/unix/build-docker/impact/nginx.conf"
# -------------------------------------------------

cp ./Unix_docker-compose.sh ./Unix_docker-compose_local.sh
docker-compose --env-file $IMPACT_VOLUME/Libs/unix/$ENV_FILE -f $IMPACT_VOLUME/Libs/unix/docker-compose.yml --project-name "impact" build --no-cache

