#!/usr/bin/env bash
#
echo 'Starting'

[[ ! -z ${IMPACT_HTTP_INTERNAL_PORT} ]] && echo "IMPACT_HTTP_INTERNAL_PORT = ${IMPACT_HTTP_INTERNAL_PORT}" || export IMPACT_HTTP_INTERNAL_PORT=9999
[[ ! -z ${IMPACT_NGINX_PORT} ]] && echo "IMPACT_NGINX_PORT = ${IMPACT_NGINX_PORT}" || export IMPACT_NGINX_PORT=8899
#[[ ! -z ${SERVER_URL} ]] && echo "SERVER_URL = ${SERVER_URL}" || export SERVER_URL=127.0.0.1
[[ ! -z ${IMPACT_HTTP_HOST} ]] && echo "IMPACT_HTTP_HOST = ${IMPACT_HTTP_HOST}" || export IMPACT_HTTP_HOST='' #${SERVER_URL}:${IMPACT_NGINX_PORT}

# IMPACT_HTTP_HOST is loaded from __config__ into GLOBALS['url']. If empty Application.js reads it from browser URL

echo $(id -un)
echo "USERID"
echo ${USER_ID}
echo ${GROUP_ID}
echo ${IMPACT_HTTP_HOST}
echo ${IMPACT_NGINX_PORT}
echo ${IMPACT_HTTP_INTERNAL_PORT}

echo "Setting UserId and data folder permissions"
echo "Operation might take a while ..."
# create a docker user with same uid and gid of host, www-data is just an internal alias
usermod -u ${USER_ID} www-data \
 && groupmod -g ${GROUP_ID} www-data \
 && touch /run/nginx.pid \
 && touch /var/log/nginx/error.log \
 && mkdir -p /var/lib/nginx/body \
 && mkdir -p /var/lib/nginx/proxy \
 && mkdir -p /var/www/.config/earthengine \
 && mkdir -p /data \
 && mkdir -p /data/db \
 && mkdir -p /remote_data \
 && chown -R www-data:www-data \
          /data \
          /docker-entrypoint.sh \
          /var/log/nginx \
          /etc/nginx/nginx.conf \
          /var/lib/nginx \
          /run/nginx.pid \
          /var/log/nginx/error.log \
          /var/lib/nginx/body \
          /var/www/.config/earthengine
echo "Setting Nginx"		  
sed -i -- 's/listen [0-9]*[0-9]; #REPLACE/listen '${IMPACT_NGINX_PORT}'; #REPLACE/g' "/etc/nginx/nginx.conf"

#use 9999
sed -i -- 's/127.0.0.1:[0-9]*[0-9]; #REPLACE/127.0.0.1:'${IMPACT_HTTP_INTERNAL_PORT}'; #REPLACE/g' "/etc/nginx/nginx.conf"
sed -i -- 's/127.0.0.1:[0-9]*[0-9]\/; #REPLACE/127.0.0.1:'${IMPACT_HTTP_INTERNAL_PORT}'\/; #REPLACE/g' "/etc/nginx/nginx.conf"

echo "Starting servers " 
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server.py ${IMPACT_HTTP_INTERNAL_PORT} &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9998 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9997 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9996 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9995 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9994 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9993 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9992 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9991 &
gosu www-data python -u /var/www/impact/Gui/libs_python/my_server_wms.py 127.0.0.1 9990 &
echo "Starting  Nginx"
gosu www-data service nginx start

