/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.Requests', {

    singleton: true,

    launchProcessing: function(params){
        Ext.Ajax.request({
            url : "python_tool_launcher.py?"+IMPACT.Utilities.Common.pairsToURL(params),
            timeout: 1000 // close connection, no need to wait
        });
    },

    fileHandler: function(operation, input, output, content){

        output = typeof output === "undefined" ? '' : output;
        content = typeof content === "undefined" ? '' : content;

        var msg = '';
        if(operation==='delete'){ msg = 'Error deleting file' }
        else if(operation==='move'){ msg = 'Error moving file' }
        else if(operation==='rename'){ msg = 'Error renaming file' }
        else if(operation==='copy'){ msg = 'Error copying file' }
        else if(operation==='GoogleEarth'){ msg = 'Error creating GoogleEarth KML+WMS file' }
        else if(operation==='shift'){msg = 'Error shifting file'}
        else if(operation==='download'){msg = 'Error preparing files for download'}
        else if(operation==='convert'){msg = 'Error converting files '}

        Ext.Ajax.request({
            url : "fileHandler.py",
            method: "POST",
            //timeout: 1000,
            params: {
                "input": input,
                "output" : output,
                "operation" : operation,
                "content" : content
            },
            success : function(response) {
                if(operation === 'shift'){
                    Ext.ComponentQuery.query('#ImageShift')[0].fireEvent('shift_layer_changed', response.responseText);
                };
                if(operation === 'undo'){
                    if(response.responseText != '1'){
                        alert('Error while restoring file '+response.responseText);
                    }
                    Ext.ComponentQuery.query('#EditingVector')[0].fireEvent('undo_completed');
                };
                if(operation === 'download'){
                    if(response.responseText == ''){
                        alert('Error in preparing file');
                    } else{
                            Ext.create('Ext.window.Window', {
                                            title: 'Download',
                                            closable: true,
                                            closeAction: 'destroy',
                                            width: 350,
                                            //minWidth: 250,
                                            height: 100,
                                            animCollapse:false,
                                            border: false,
                                            modal: true,
                                            layout: {
                                                type: 'border',
                                                padding: 5
                                            },
                                           items: [
                                                    {
                                                        html: '<div> File is now ready for <a target="_blank" href='+'download_file?file_path='+response.responseText+'>download</a> </div>'
                                                    }
                                           ],
                                           buttons: [
                                                {
                                                    text: 'Close',
                                                    handler: function () { this.up('window').close(); }
                                                }
                                            ],
                                           autoShow: true
                            });


                    }
                }
            },
            failure : function(response) {
                console.log(msg + input +": "+response.responseText);
                if(operation === 'shift'){
                    Ext.ComponentQuery.query('#ImageShift')[0].fireEvent('shift_layer_changed', false);
                };

                if(operation === 'undo' && response.responseText != '1'){
                    alert('Error while restoring file '+response.responseText);
                    Ext.ComponentQuery.query('#EditingVector')[0].fireEvent('undo_completed');
                };

                if(operation === 'download' && response.responseText == ''){
                    alert(msg);

                };
            }
        });
    },

    add_missing_fields_to_shp: function (shp_path, mapPanel, node, fieldList){
     // add missing fields into DB and reload the shp;

        Ext.Ajax.request({
            url :"add_missing_fields_to_shp.py",
            method: "GET",
            params: {
                shp_name: shp_path,
                fields: JSON.stringify(fieldList)
            },
            timeout: 300*1000,
            beforeSend: document.getElementById('Impact_map_div').style.cursor = 'wait',
            success : function(response) {
                setTimeout(function(){
                    node.set('cls','x-tree-enabled');
                    node.set('text',node.data.qtip);

                    document.getElementById('Impact_map_div').style.cursor = 'default';
                    if (response.responseText.substr(0,2) == "OK"){
                        Ext.Msg.show({
                            title: 'Shapefile updated',
                            msg: 'Done.<br />Shapefile will be reloaded in the layers panel. <br />Please re-assign the legend to the layer.',
                            buttons: Ext.Msg.OK
                        });
                    }else{
                        alert(response.responseText);
                    }

                }, 3000);
            },
            failure : function(response) {
                document.getElementById('Impact_map_div').style.cursor = 'default';
                alert("Error while adding fields");
                node.set('cls','x-tree-enabled');
                node.set('text',+node.data.qtip);
            }
        })
        return 1
    },

    show_shp_stats : function(mapPanel,infile,layer,legend_type){
        Ext.Ajax.request({

            timeout: 300*1000,
            beforeSend: document.getElementById('Impact_map_div').style.cursor = 'wait',

            url : "get_vector_stats_by_layer.py?data_path="+infile+"&layer="+layer+"&legend_type="+legend_type,
            success : function(response) {
                document.getElementById('Impact_map_div').style.cursor = 'default';
                if( Ext.ComponentQuery.query('#StatsWindow').length>0 ){
                    Ext.ComponentQuery.query('#StatsWindow')[0].destroy();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.StatsWindow', {
                    data: Ext.JSON.decode(response.responseText)
                });
            },
            failure : function(response) {
                console.log("Error building_stats: "+ infile+": "+response.responseText);
                alert("Error building_stats: "+ infile+": timeout")
                document.getElementById('Impact_map_div').style.cursor = 'default';
            }
        })
    },
    show_raster_stats : function(mapPanel,infile,band){
        if( Ext.ComponentQuery.query('#StatsWindow').length>0 ){
                    Ext.ComponentQuery.query('#StatsWindow')[0].destroy();
        };

        Ext.Ajax.request({

            timeout: 300*1000,
            beforeSend: document.getElementById('Impact_map_div').style.cursor = 'wait',

            url : "get_raster_stats_by_band.py?data_path="+infile+"&band="+band,
            success : function(response) {
                document.getElementById('Impact_map_div').style.cursor = 'default';
                Ext.create('IMPACT.view.Workspace.Main.component.StatsWindow', {
                    data: Ext.JSON.decode(response.responseText)
                });
            },
            failure : function(response) {
                console.log("Error building_stats: "+ infile+": "+response.responseText);
                alert("Error building_stats: "+ infile+": timeout")
                document.getElementById('Impact_map_div').style.cursor = 'default';
            }
        })
    },

    query_by_point: function(layer_files, lon, lat, click_evt, additional_params, mapWidth){
         if(layer_files.length == 0){
            var InfoWin = Ext.ComponentQuery.query('#LayerQuery')[0];
            if(typeof InfoWin != "undefined"){
                var old_pos = InfoWin.getPosition();
                InfoWin.destroy();
            }
            var info_window = Ext.create('IMPACT.view.Workspace.Main.component.LayerQuery');
            info_window.parseInfo([]);
            if(typeof old_pos != "undefined"){
                    info_window.showAt(old_pos);
            } else {
                    info_window.show();
            }
            info_window.toFront();
            return;
        }

        var params = {
            layer_files: JSON.stringify(layer_files),
            lat: lat,
            lon: lon,
            mapWidth
        };
        if(typeof(additional_params)!="undefined"){
            for (var key in additional_params) {
                params[key] = additional_params[key];
            }
        }

        Ext.Ajax.request({
            url : "query_file.py",
            method: "GET",
            params: params,
            success : function(response) {
                var response = JSON.parse(response.responseText);
                var InfoWin = Ext.ComponentQuery.query('#LayerQuery')[0];
                if(typeof InfoWin != "undefined"){
                    var old_pos = InfoWin.getPosition();
                   InfoWin.destroy();
                };
                var info_window = Ext.create('IMPACT.view.Workspace.Main.component.LayerQuery');
                info_window.parseInfo(response);
                if(typeof old_pos != "undefined"){
                    info_window.showAt(old_pos);
                } else {
                    info_window.show();
                    info_window.toFront();
                }
            },
            failure: function(response) {
                console.log('query_by_point failure: ',response.responseText)
            }

        });
    },

//    copyDBFfields: function(treeNode, mapPanel, infile, outfield, infield){
//        var layer = treeNode.data.layer;
//        var map = mapPanel.map;
//
//            Ext.Ajax.request({
//                timeout: 300*1000,
//                beforeSend: mapPanel.map.viewPortDiv.style.cursor='wait',
//
//                url : "copyDBFfields.py?data_path="+infile+"&infield="+infield+"&outfield="+outfield,
//                success : function(response) {
//
//                    layer.redraw(true);
//                    mapPanel.map.viewPortDiv.style.cursor = 'default';
//                    alert("Copy completed");
//
//                },
//                failure : function(response) {
//                    mapPanel.map.viewPortDiv.style.cursor = 'default';
//                    alert("Error: "+ response.responseText);
//                }
//        })
//    },

    updateDBFrecord: function(mapPanel, layerNode, infile, sql){

            Ext.Ajax.request({
                timeout: 300*1000,
                beforeSend: document.getElementById('Impact_map_div').style.cursor = 'wait',

                url : "updateDBFrecord.py?data_path="+infile+"&sql="+sql,
                success : function(response) {

                    layerNode.cascadeBy(function (n) {
                                if(n.data.layer.get('type') === 'attributes'){
                                   n.data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                                   n.data.layer.getSource().refresh();
                                }
                            });
                    document.getElementById('Impact_map_div').style.cursor = 'default';

                },
                failure : function(response) {

                    document.getElementById('Impact_map_div').style.cursor = 'default';
                    alert("Error: "+ response.responseText);


                }
            })
    },

    getGeeThumbUrl: function(GEEWindow, lon, lat, date, timeFrame, R, G, B, minVal, maxVal, inBuff, outBuff, execMode, sensor){
            var url = IMPACT.GLOBALS['url']+"/getGeeThumbUrl.py?";
            url += "lat="+lat+"&lon="+lon+"&date="+date+"&timeFrame="+timeFrame;
            url += "&R="+R;
            url += "&G="+G;
            url += "&B="+B;
            url += "&minVal="+minVal;
            url += "&maxVal="+maxVal;
            url += "&inBuff="+inBuff;
            url += "&outBuff="+outBuff;
            url += "&sensor="+sensor;
            url += "&execMode="+execMode;
            //url += "&planetUrl="+planetUrl;

            Ext.Ajax.request({
                timeout: 300*1000,
                cors: true,
                useDefaultXhrHeader: false,
                url : url,
                success : function(response) {
                    //console.log(response.responseText);
                    var myImage = {
                            xtype: 'image',
                            src: response.responseText,
                            userCls:'gee_style_img',
                            listeners: {
                                afterrender: function(c) {
                                    Ext.create('Ext.tip.ToolTip', {
                                        target: c.getEl(),
                                        html: date
                                    });
                                }
                            },
                            renderTo: 'GeeDiv_'+date                        };

                    //GEEWindow.add(myImage)
                    var DivID = "GEE_"+lon+"_"+lat;
                    if(document.getElementById(DivID) !== null || document.getElementById('GeeDiv_'+date !== null )){
                        Ext.create(myImage);
                    }
                },
                failure : function(response) {
                    //console.log(response)
                    var myImage = {
                            xtype: 'image',
                            src: '/resources/icons/gee.png',
                            userCls:'gee_style_img',
                            listeners: {
                                afterrender: function(c) {
                                    Ext.create('Ext.tip.ToolTip', {
                                        target: c.getEl(),
                                        html: idORdate
                                    });
                                }
                            },
                            renderTo: 'GeeDiv_'+idORdate
                        };

                    //GEEWindow.add(myImage)
                    var DivID = "GEE_"+lon+"_"+lat;
                    if(document.getElementById(DivID) !== null || document.getElementById('GeeDiv_'+idORdate) !== null ){
                        Ext.create(myImage);
                    }
                }
            })
    }



});