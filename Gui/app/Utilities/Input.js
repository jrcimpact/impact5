/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.Input', {

    singleton: true,

    /**
     * Get and parse values from inputs field (ensure to output only STRING)
     * @returns {string|Array}
     */
    parseField: function(inputField){

        var value = '';
        var inputFieldType = inputField.getXType();

        // ### Numeric fields ###
        if(inputFieldType==="numberfield"
                || inputFieldType==="impactFieldFloat"
                || inputFieldType==="impactFieldInteger"){
            if(inputField.getValue()!==null){
                if(inputField.decimalPrecision>2){
                    value = IMPACT.Utilities.Common.noExponents(inputField.getValue());
                } else {
                    value = inputField.getValue();
                }
            }
        }
        // ### Radio buttons ###
        else if(inputFieldType==="radiogroup"
                || inputFieldType==="impactFieldRadioYesNo"){
            var radioName = inputField.items.first().getName();
//            console.log(radioName);
//            console.log(inputField.getValue());
//            console.log(inputField.getValue()[radioName]);
            value = inputField.getValue()[radioName];
        }
        // ### Checkboxes ###
        else if(inputFieldType==="checkboxgroup"){
            var values = [];
            Ext.Array.each(inputField.getChecked(), function(item){
                values.push(item.inputValue);
            });
            return values;
        }
        // ### FileInput (file tree selector) ###
        else if(inputFieldType==="ProcessingWindowFileInput"
                || inputFieldType==="ProcessingWindowFileByTypeInput"){
            return JSON.stringify(inputField.getValues());
        }
        // ### ALL other (textual fields) ###
        else if(inputFieldType==="textfield"
                || inputFieldType==="combobox"
                || inputFieldType==="slider"
                || inputFieldType==="impactFieldCombo"
                || inputFieldType==="impactFieldText"
                || inputFieldType==="impactFieldFloatAsText"
                || inputFieldType==="impactFieldTextArea"
                || inputFieldType==="EPSGCodesCombo"
                || inputFieldType==="impactFieldSlider"
                ){
            value = inputField.getValue();
        }
        // ### Labels ###
        else if(inputFieldType==="label"){
            value = inputField.text;
        }

//        console.log(inputField);
//        console.log(inputFieldType);
//        console.log(value);

        value = value===null ? '': value;
        return value.toString();
    }

});