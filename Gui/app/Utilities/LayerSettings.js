/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.LayerSettings', {

    singleton: true,

    /**
     *  methods to read and write layer settings
     */


    harmonizeDefaultLayerParams: function(data_from_DB, settings_from_DB){
        var DefParams = {};
        // common
        DefParams['opacity'] = 1;

        // Vector attributes ??? raster
        DefParams['legendType'] = '';

        // vector
        if(data_from_DB['extension'] === '.shp' || data_from_DB['extension'] === '.geojson' || data_from_DB['extension'] === '.json' ){

            DefParams['outline_size'] = 0.5;
            DefParams['color'] = '000000';
            DefParams['simplificationRequired'] = data_from_DB['simplificationRequired'];

            for (key in DefParams) {
                if(settings_from_DB[key]){ DefParams[key] = settings_from_DB[key]}
            };

            DefParams['legendType'] = '';  // each time is set by user before loading attributes
        }

        if(data_from_DB['extension'] === '.tif' || data_from_DB['extension'] === '.vrt'){
            // raster
            //  DefParams['bands']
            //  DefParams['stretch']
            //  DefParams['scale1']
            //  DefParams['scale2']
            //  DefParams['scale3']


            for (key in DefParams) {
                if(settings_from_DB[key]){ DefParams[key] = settings_from_DB[key]}
            };

            DefParams['apply_data_range'] = [false, 0, 250];

            var num_bands = data_from_DB['num_bands'];
            DefParams['bands'] = this.set_band_combination(num_bands);   // use function;

            if(settings_from_DB['bands']){

                var savedBands =  settings_from_DB['bands'].split(',');
                for (x in savedBands ) {
                    savedBands[x] = parseInt(savedBands[x]);
                }

                if (Math.max(...savedBands) <= num_bands){
                    DefParams['bands'] = settings_from_DB['bands']
                }
                else{
                    alert(data_from_DB['full_path']+' bands setting are not correct, using default');
                }
            };

            DefParams['stretch'] = this.set_stretch_type(data_from_DB);   // use function

            if(settings_from_DB.hasOwnProperty('stretch')){
                DefParams['stretch'] = settings_from_DB['stretch'];
            }

            if(settings_from_DB.hasOwnProperty('rgb')){
                DefParams['rgb'] = settings_from_DB['rgb'];
            }else{
                DefParams['rgb'] = num_bands >= 3 ? true : false;
            }


            //  scale params
            if(settings_from_DB['scale1']){
                DefParams['scale1'] = settings_from_DB['scale1'];
            }else{
                DefParams['scale1'] = this.set_scale_per_band(DefParams['bands'].split(',')[0],DefParams['stretch'],data_from_DB);
            }
            if(settings_from_DB['scale2']){
                DefParams['scale2'] = settings_from_DB['scale2'];
            }else{
                if(num_bands >= 3){
                    DefParams['scale2'] = this.set_scale_per_band(DefParams['bands'].split(',')[1],DefParams['stretch'],data_from_DB);
                } else {
                   DefParams['scale2'] = DefParams['scale1']
                }
            }
            if(settings_from_DB['scale3']){
                DefParams['scale3'] = settings_from_DB['scale3'];
            }else{
                if(num_bands >= 3){
                    DefParams['scale3'] = this.set_scale_per_band(DefParams['bands'].split(',')[2],DefParams['stretch'],data_from_DB);
                } else {
                   DefParams['scale3'] = DefParams['scale1']
                }
            }
        }

        //DefParams['editing'] = false;

        return DefParams
    },


    set_band_combination: function(num_bands){
        var bands;
        if(num_bands == 3){
            bands = '1,2,3';
        } else if(num_bands == 4){
            bands = '4,3,2';
        } else if(num_bands == 5){
            bands = '2,5,3';
        } else if(num_bands > 5){
            bands = '5,4,3';
        } else if(num_bands > 12){
            bands = '12,8,3';
        } else {
            bands = '1';
        }

        return bands;
    },

    set_stretch_type: function(data_from_DB){
        var me = this;
        var statistics = Ext.decode(data_from_DB['statistics']);  //  for each band: minimum, maximum, mean and std
        var stretch_type = -1;
        // Set default stretch type  -1 for auto 0-255 onload without stats

        if (typeof(statistics.length) === 'undefined'){
                // stretch will be reset on update node after stats are done
                stretch_type = -1;
        }else{
                stretch_type = 1;
                if (data_from_DB['num_bands'] > 2 && (statistics[0][0] >= 0 && statistics[0][1] <= 255)) {
                        stretch_type = 4;
                }
        }
        return stretch_type
    },


    set_scale_per_band: function(band_index, stretch_type, data_from_DB){
        var scale = '0,255';
        var coefficient = 0;
        // Deduct statistics values
        var raw_statistics = Ext.decode(data_from_DB['statistics'] );   // for each band: minimum, maximum, mean and stDev
        var band_statistics = raw_statistics[band_index - 1];

        // #####  Stretch 0-255  #####
        if (stretch_type == 0 || stretch_type == -1 ){
            scale = '0,255'
            return scale;
        }
        if (stretch_type == 1){
            scale = String(band_statistics[0]) + "," + String(band_statistics[1]);
            return scale;
        }
        // #####  Stretch 0.5x StDev  #####
        if (stretch_type == 2){
            coefficient = 0.5;
        }
        // #####  Stretch 1x StDev  #####
        else if (stretch_type == 3){
            coefficient = 1;
        }
        // #####  Stretch 2x StDev  #####
        else if (stretch_type == 4){
            coefficient = 2;
        }
        // #####  Stretch 3x StDev  #####
        else if (stretch_type == 5){
            coefficient = 3;
        }

            // calculate scale
        var min = String(band_statistics[2] - coefficient * band_statistics[3]);
        var max = String(band_statistics[2] + coefficient * band_statistics[3]);
        if(min < band_statistics[0]){
            min = band_statistics[0]
        }
        if(max > band_statistics[1]){
            max = band_statistics[1]
        }
        scale = String(min) + "," + String(max);

        return scale;
    },

    update_settings_from_DB: function(layerNode, key, value, redraw){
        layerNode.data.settings_from_DB[key] = value;
        if(layerNode.data.layer){
            if(key === 'opacity'){
                layerNode.data.layer.setOpacity(value);
            }else{

                layerNode.data.layer.getSource().updateParams({[key]:value});
            }

            if(redraw){

                layerNode.data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                layerNode.data.layer.getSource().refresh();
            }
        }
        // do not save info if vector attribute layers - reloaded each time
        if(layerNode.data.layer && layerNode.data.layer.get('type') === 'attributes'){

            return
        }
        // do not save legendType if vector reloaded each time since legend is loaded by user
        if(layerNode.data.layer && layerNode.data.layer.get('type') === 'vector' && key === 'legendType'){
            //console.log('Not saving  legendType');
            return
        }
        this.save_settings_from_DB('save', layerNode.data.data_from_DB['full_path'], null, layerNode.data.settings_from_DB);

    },

    save_settings_from_DB: function(toDo, source_path, destination_path = '', new_settings = ''){
        var imageSettingsStore = Ext.data.StoreManager.lookup('ImageSettingsStore');
        // update imagestore

        if(toDo == 'delete'){
            imageSettingsStore.deleteRecord(source_path);
        };
        if(toDo == 'save'){
            new_settings = Ext.JSON.encode(new_settings);  // settings from Node from dict to string
            imageSettingsStore.saveRecord(source_path, new_settings);
        };

        if(toDo == 'copy'){
            var layerSettings = imageSettingsStore.getRecordBy('full_path',source_path);
             // settings from store
            if(typeof layerSettings[0] !== 'undefined'){
                new_settings = layerSettings[0].settings;
                imageSettingsStore.saveRecord(destination_path, new_settings)
                toDo = 'save';
                source_path = destination_path;
                destination_path = '';

            } else {
                // if no settings does nothing
                return
            }
        };

        if(toDo == 'move'){
            var layerSettings = imageSettingsStore.getRecordBy('full_path',source_path);
             // settings from store
            if(typeof layerSettings[0] !== 'undefined'){
                new_settings = layerSettings[0].settings;
                imageSettingsStore.deleteRecord(source_path);
                imageSettingsStore.saveRecord(destination_path, new_settings)
            } else {
                // if no settings does nothing
                return
            }
        };




        // change call with listener on store
        // update DB
        Ext.Ajax.request({
            url: 'layer_settings_manager.py',
            method: "POST",
            params: {
                toDo: toDo,
                source_path: source_path,
                destination_path: destination_path,
                settings : new_settings
            },
            scope: this,
            success: function(response){
                //console.log(response.responseText);
            },
            failure: function(response){
                console.log(response.responseText);
            }
        });

    }



});