/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.Common', {

    singleton: true,

    /**
     * A JavaScript equivalent of PHP’s dirname()
     * @param path
     * @returns {string}
     */
    dirname: function(path) {
        return path.replace(/\\/g, '/')
            .replace(/\/[^\/]*\/?$/, '');
    },

    /**
     * A JavaScript equivalent of PHP’s basename()
     * @param path
     * @param suffix
     * @returns {*}
     */
    basename: function(path, suffix) {
        var b = path;
        var lastChar = b.charAt(b.length - 1);
        if (lastChar === '/' || lastChar === '\\') {
            b = b.slice(0, -1);
        }
        b = b.replace(/^.*[\/\\]/g, '');
        if (typeof suffix === 'string' && b.substr(b.length - suffix.length) == suffix) {
            b = b.substr(0, b.length - suffix.length);
        }
        return b;
    },

    /**
     * Convert a javascript object (associative array) to an URL parameters list string
     * @param params
     * @returns {string}
     */
    pairsToURL: function(params){
        //return params
        var params_url='';
        for(var p in params){
            params_url += p+'='+params[p]+'&';
        }
        //remove last '&'
        params_url = params_url.substr(0,params_url.length-1);
        return params_url;
    },

    /**
     * Convert hexidecimal color string to RGB
     * @param hex
     * @returns {*[]}
     */
    hexToRgb: function(hex){
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16)
        ] : null;
    },

    /**
     * Convert RGB to hexidecimal color string
     * @param r
     * @param g
     * @param b
     */
    rgbToHex: function(r, g, b){
        var bin = r << 16 | g << 8 | b;
        return (function(h){
            return new Array(7-h.length).join("0")+h
        })(bin.toString(16).toUpperCase())
    },

    /**
     * Insert the given char at the specified index of the string
     * @param char
     * @param index
     * @param string
     * @returns {*}
     */
    insertAt: function(char, index, string) {
        return string.substr(0, index) + char + string.substr(index);
    },

    /**
     * Pad (Left) a string to a certain length with the given character
     * @param string
     * @param width
     * @param character
     * @returns {string}
     */
    leftPad: function(string, width, character) {
        character = character || '0';
        string = string + '';
        return string.length >= width ? string : new Array(width - string.length + 1).join(character) + string;
    },


    /**
     * Format Date object to string
     * @param date
     * @param format
     * @returns {string}
     */
    formatDate: function(date, format){
        if(format=='YYYYMMDD'){
            return String(date.getFullYear())
                + IMPACT.Utilities.Common.leftPad(String(date.getMonth()+1), 2, '0')
                + IMPACT.Utilities.Common.leftPad(String(date.getDate()), 2, '0');
        }
        else if(format=='YYYY-MM-DD'){
            return String(date.getFullYear())
                + '-'
                + IMPACT.Utilities.Common.leftPad(String(date.getMonth()+1), 2, '0')
                + '-'
                + IMPACT.Utilities.Common.leftPad(String(date.getDate()), 2, '0');
        }
        else if(format=='YYYY-MM-DD HH:MM:SS'){
            return IMPACT.Utilities.Common.formatDate(date, 'YYYY-MM-DD')
                + ' '
                + IMPACT.Utilities.Common.leftPad(String(date.getHours()), 2, '0')
                + ':'
                + IMPACT.Utilities.Common.leftPad(String(date.getMinutes()), 2, '0')
                + ':'
                + IMPACT.Utilities.Common.leftPad(String(date.getSeconds()), 2, '0');
        }
        else if(format=='YYYYMMDDHHMMSS'){
            return IMPACT.Utilities.Common.formatDate(date, 'YYYYMMDD')
                + IMPACT.Utilities.Common.leftPad(String(date.getHours()), 2, '0')
                + IMPACT.Utilities.Common.leftPad(String(date.getMinutes()), 2, '0')
                + IMPACT.Utilities.Common.leftPad(String(date.getSeconds()), 2, '0');
        }
        else if(format=='YYYYMMDDHHMMSSmm'){
            return IMPACT.Utilities.Common.formatDate(date, 'YYYYMMDDHHMMSS')
                + IMPACT.Utilities.Common.leftPad(String(date.getMilliseconds()), 3, '0');
        }
    },

    /**
     * Generate HASH integer code from string
     * @param str
     * @returns {string}
     */
    generateHash: function(str){
        var hash = 0, i, chr, len;
        if (str.length === 0) return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr   = str.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    },

    /**
     * Convert from scientific notation to normal float
     * @param value
     * @returns {string}
     */
    noExponents: function(value){
        var data = String(value).split(/[eE]/);
        if(data.length == 1) return data[0];
        var z= '', sign = value < 0 ? '-' : '',
        str = data[0].replace('.', ''),
        mag = Number(data[1])+ 1;
        if(mag<0){
            z= sign + '0.';
            while(mag++) z += '0';
            return z + str.replace(/^\-/,'');
        }
        mag -= str.length;
        while(mag--) z += '0';
        return str + z;
    }

});
