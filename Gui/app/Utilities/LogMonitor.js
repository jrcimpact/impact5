/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Logs' Monitor Controller
 *
 */
Ext.define('IMPACT.Utilities.LogMonitor', {

    logTabID: 'logTab',
    logPanelID: 'logPanel',
    store: {},

    numRunning: 0,
    numErrors: 0,
    numWarnings: 0,

    constructor: function(){
        var me = this;
        me.SendMessage('IMPACT Toolbox started. (version <b>'+IMPACT.GLOBALS['version']+'</b>)');
        // ## New version available ##
        //me.__alertNewVersion();
        // ## Connection mode ##
        me.__alertOffline();
    },

//    __alertNewVersion: function(){
//        var me = this;
//        if(IMPACT.GLOBALS['repository']['up-to-date']==false){
//            var msgLog = 'Version '+IMPACT.GLOBALS['repository']['remote']['version']+' of IMPACT Toolbox is available!';
//            var msgAlert = '<b>'+msgLog+'</b>';
//            if(typeof(IMPACT.GLOBALS['repository']['remote']['changelog']) != "undefined" && IMPACT.GLOBALS['repository']['remote']['changelog'] != ''){
//                var changeLog = IMPACT.GLOBALS['repository']['remote']['changelog'].replace(/\n/g, '<br />');
//                msgAlert += '<br /><br /><i>Changelog:</i><br />'+changeLog;
//                msgLog += '<div class="sectionHeader">Version '+IMPACT.GLOBALS['repository']['remote']['version']+' changelog</div>' +
//                            '<div class="sectionBody">'+changeLog+'</div>';
//            }
//            Ext.Msg.show({
//                title: 'New version available!',
//                msg: msgAlert,
//                buttonText: {
//                    ok: 'Update now',
//                    cancel: 'Update later'
//                },
//                fn: function(btn){
//                    if(btn == 'ok'){
//                        IMPACT.Utilities.Requests.launchProcessing({toolID: 'update'});
//                    }
//                }
//            });
//            me.SendWarning(msgLog);
//        }
//    },

    __alertOffline: function(){
        var me = this;
        if(IMPACT.GLOBALS['online']=='True'){
            me.SendMessage('Connection available: ONLINE mode.');
        } else {
            var msg = "Connection not available: IMPACT is running in OFFLINE mode.";
            Ext.Msg.show({
                title: 'Offline Mode',
                msg: msg,
                buttons: Ext.Msg.OK
            });
            me.SendWarning(msg);
        }
    },

    SendMessage: function(message, warning){
        var me = this;
        var item = {};
        var today =
        item.logType = 'simple';
        item.message = message;
        item.warning = (typeof(warning)!="undefined" && warning==true) ? true : false;
        item.date = IMPACT.Utilities.Common.formatDate(new Date(), 'YYYY-MM-DD HH:MM:SS');
        item.id = IMPACT.Utilities.Common.generateHash(item.date+message)
        me.store[item.id] = item;
    },

    SendWarning: function(message){
        this.SendMessage(message, true);
    },

    updateStore: function(logs){
        var me = this;
        logs.forEach(function(log){
            var item = (log['id'] in me.store) ? me.store[log['id']] : {};
            item.logType = 'processing';
            item.id = log['id'];
            item.toolID = log['toolID'];
            item.status = log['status'];
            item.numErrors = log['numErrors'];
            item.numWarnings = log['numWarnings'];
            item.title = log['title'];
            item.date = log['date'];
            item.inputs = log['inputs'];
            item.params = log['params'];
            item.messages = log['messages'];
            item.errors = log['errors'];
            me.store[item.id] = item; // sorting to be fixed
        });
    },

    printLogs: function(){
        var me = this;
        var output = '';
        output = '<div class="logContainer">';
        output += '<div class="topButton">';
        output += '<i class="fa fa-lg fa-red fa-exclamation-circle"></i> ';
        output += '<button onclick="IMPACT.LogMonitor.clearLog();"><span class="fa fa-lg fa-red far fa-trash-alt"></span>Remove All Log</button>';
        output += '</div>';
        output += '</div>';

        me.numRunning = 0;
        me.numErrors = 0;
        me.numWarnings = 0;
        var reversedStore = '';
        for(var id in me.store){
            if(me.store[id].logType == 'processing'){
                reversedStore = me.__printProcessingLog(id) + reversedStore;
            } else {
                // keep info on version and updates on top
                output += me.__printSimpleLog(id);
            }
        }
        output += reversedStore;



        var tabTitleClass;
        var numRunningLabel = me.numRunning>0 ? ' ('+me.numRunning+')' : '';
        if(me.numErrors>0){
            tabTitleClass = "logError";
        } else if(me.numRunning>0){
            tabTitleClass = "logRunning";
        } else if(me.numWarnings>0){
            tabTitleClass = "logWarning";
        } else {
            tabTitleClass = "logCompleted";
        }

        Ext.ComponentQuery.query('#'+me.logTabID)[0].setTitle('<span class="'+tabTitleClass+'">Logs Monitor'+numRunningLabel+'</span>');
        Ext.ComponentQuery.query('#'+me.logPanelID)[0].update(output);
    },

    __printSimpleLog: function(id){
        var me = this;
        var item = me.store[id];
        output = '<div id="'+item.id+'" class="logContainer">';
            output += '<div class="logHeader">';
                output += '[ <i>'+item.date+'</i> ]&nbsp;&nbsp;';
                if(item.warning==true){
                    output += '<i class="fa fa-lg fa-red fa-exclamation-circle"></i> ';
                } else {
                    output += '<i class="fa fa-lg fa-blue fa-info-circle"></i> ';
                }
                output += item.message;
            output += '</div>';
        output += '</div>';
        return output
    },

    __printProcessingLog: function(id){
        var me = this;
        output = '';
        var item = me.store[id];
        var display = typeof(item.display)!="undefined" ? item.display : 'none';
        switch(item.status) {
            case "success":
                statusLabel = '<span class="logStatus logCompleted">Completed.</span>';
                break;
            case "warning":
                statusLabel = '<span class="logStatus logWarning">Completed with warnings.</span>';
                me.numWarnings++;
                break;
            case "error":
                statusLabel = '<span class="logStatus logError">Failed !!</span>';
                me.numErrors++;
                break;
            default:
                statusLabel = '<span class="logStatus logRunning">Running...</span>';
                me.numRunning++;
                break;
        }
        output += '<div id="'+item.id+'" class="logContainer">';
            // ####  Header  ####
            output += '<div class="logHeader">';
                output += '[ <i>'+item.date+'</i> ]&nbsp;&nbsp;<b>'+item.title+'</b>'+statusLabel;
                output += '<div class="right">';
                    output += '<button onclick="IMPACT.LogMonitor.showHide(\''+item.id+'\');"><span class="fa fa-lg fa-blue fa-info-circle"></span>Show info</button>';
                    if(item.status=="running"){
                        output += '<button onclick="IMPACT.LogMonitor.stopProcess(\''+item.id+'\');"><span class="fa fa-lg fa-red fas fa-stop-circle"></span>Stop Process</button>';
                    } else {
                        output += '<button onclick="IMPACT.LogMonitor.removeLog(\''+item.id+'\');"><span class="fa fa-lg fa-red far fa-trash-alt"></span>Remove Log</button>';
                    }
                output += '</div>';
            output += '</div>';
            // ####  Body  ####
            output += '<div class="logBody" style="display: '+display+';">';
                // Input files
                if( item.inputs.length > 0){
                    output += '<div class="sectionHeader">Input Files</div>';
                    output += '<div class="sectionBody">';
                        for(var i in item.inputs){
                            output += item.inputs[i]+'<br />';
                        }
                    output += '</div>'
                }
                // params
                if( item.params.length > 0){
                    output += '<div class="sectionHeader">Parameters</div>';
                    output += '<div class="sectionBody">';
                        output += '<table>'
                        for(var i in item.params){
                            output += '<tr><td style="padding-right: 10px;"><i>'+item.params[i]['label']+'</i></td><td>'+item.params[i]['value']+'</td></tr>';
                        }
                         output += '</table>'
                    output += '</div>'
                }
                // content
                output += '<div class="sectionHeader">Log Messages</div>';
                    output += '<div class="sectionBody">';
                    for(var i in item.messages){
                        var message = item.messages[i];
                        if(message.indexOf('__ERROR__') > -1){
                            message = '<span class="logError">'+message.replace("__ERROR__", "")+'</span>';
                        }
                        if(message.indexOf('__WARNING__') > -1){
                            message = '<span class="logWarning">'+message.replace("__WARNING__", "")+'</span>';
                        }
                        output += message+'<br />';
                    }
                output += '</div>'
                // errors & warnings
                if( item.numErrors > 0 && item.status!="running" ){
                    output += '<div class="sectionHeader">Errors</div>';
                    output += '<div class="sectionBody logError">';
                        for(var i in item.errors){
                            var message = item.errors[i];
                            if(message.indexOf('__ERROR__') > -1){
                                message = '<span class="logError">'+message.replace("__ERROR__", " ERROR: ")+'</span>';
                            }
                            if(message.indexOf('__WARNING__') > -1){
                                message = '<span class="logWarning">'+message.replace("__WARNING__", " WARNING: ")+'</span>';
                            }
                            output += message+'<br />';
                        }
                    output += '</div>';
                }
            output += '</div>';
        output += '</div>';
        return output
    },

    showHide: function(id){
        var me = this;
        var logBody = document.getElementById(id).childNodes[1];
        if(logBody.style.display=='none'){
            logBody.style.display = 'block';
            me.store[id]['display'] = 'block';
        } else {
            logBody.style.display='none';
            me.store[id]['display'] = 'none';
        }
    },

    removeLog: function(id){
        var me = this;
        Ext.Ajax.request({
            url: 'python_tool_launcher.py',
            method: 'POST',
            params: {
                "toolID": "processLogRemove",
                "log_id" : id
            },
            success: function(){
                document.getElementById(id).parentElement.removeChild(document.getElementById(id));
                delete me.store[id];
            },
            failure: function(){
                alert("Error in removing log.");
            }
        });
    },
    clearLog: function(id){
        var me = this;
        Ext.Ajax.request({
            url: 'python_tool_launcher.py',
            method: 'POST',
            params: {
                "toolID": "processLogRemove",
                "log_id" : 'all'
            },
            success: function(){
                for(var id in me.store){
                     delete me.store[id];
                }
            },
            failure: function(){
                alert("Error in removing logs.");
            }
        });
    },

    stopProcess: function(id){
        var me = this;
        Ext.Msg.show({
            title:'Stop process',
            msg: 'Are you sure you want to stop the running process?',
            buttons: Ext.Msg.YESNO,
            fn: function(e){
                if(e=='yes'){
                    Ext.Ajax.request({
                        url: 'python_tool_launcher.py',
                        method: 'POST',
                        params: {
                            "toolID": "processKill",
                            "log_id": id
                        },
                        success: function(){},
                        failure: function(e){
                            console.log(e);
                            alert("Error in stopping log.");
                        }
                    });
                }
            }
        });
    },

    toolIsAlreadyRunning: function(toolID){
        var me = this;
        for (var key in me.store){
            if(me.store[key]['toolID']==toolID
                && me.store[key]['status']=="running"){
                return true;
            }
        }
        return false;
    }

});
