/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.OpenLayers', {

    singleton: true,

    baseLayersText: 'Base Layers',


    createWMSLayer: function(id){
        var layer = null;
        if(id=='TMF'){
            layer = new ol.layer.Tile({
                title:'JRC Tropical Moist Forest (TMF)',
                name:'JRC Tropical Moist Forest (TMF)',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/TMF" target="_blank">JRC Source: TMF</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/tmf_v1/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'TransitionMap_Subtypes',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });
            //layer.setVisible(false);
        }

        if(id=='sentinel2'){
            layer = new ol.layer.Tile({
                title:'JRC Sentinel2 composite 10-2015 10-2017',
                name:'JRC Sentinel2 composite 10-2015 10-2017',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_FalseColor_Composite_2017',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });

        }
        if(id=='sentinel2_2018'){
            layer = new ol.layer.Tile({
                title:'JRC Sentinel2 composite 2018',
                name:'JRC Sentinel2 composite 2018',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_FalseColor_Composite_2018',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });

        }
        if(id=='sentinel2_2015-2018'){
            layer = new ol.layer.Tile({
                title:'JRC Sentinel2 SWIR difference 2015-17 vs 2018',
                name:'JRC Sentinel2 SWIR difference 2015-17 vs 2018',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_change_2017_2018',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });

        }
        if(id=='sentinel2_2018-2019'){
            layer = new ol.layer.Tile({
                title:'JRC Sentinel2 SWIR difference 2018 vs 2019',
                name:'JRC Sentinel2 SWIR difference 2018 vs 2019',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_change_2018_2019',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });

        }
         if(id=='sentinel2_2019'){
            layer = new ol.layer.Tile({
                title:'JRC Sentinel2 composite 2019',
                name:'JRC Sentinel2 composite 2019',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_FalseColor_Composite_2019',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });


        }
        if(id=='sentinel2_2019-2020'){
           layer = new ol.layer.Tile({
                title:'JRC Sentinel2 SWIR difference 2019 vs 2020',
                name:'JRC Sentinel2 SWIR difference 2019 vs 2020',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_change_2019_2020',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });
        }
        if(id=='sentinel2_2020'){
             layer = new ol.layer.Tile({
                title:'JRC Sentinel2 composite 2020',
                name:'JRC Sentinel2 composite 2020',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/sentinel/sentinel2_composite" target="_blank">JRC Source: S2</a> ',
                    url:"https://ies-ows.jrc.ec.europa.eu/iforce/sentinel2_composite/wms.py?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'S2_FalseColor_Composite_2020',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });
        }
        if(id=='GFC2020'){
             layer = new ol.layer.Tile({
                title:'JRC GFC 2020 v2',
                name:'JRC GFC 2020 v2',
                source: new  ol.source.TileWMS({
                    attributions:'<a href="https://forobs.jrc.ec.europa.eu/GFC" target="_blank">JRC Source: GFC</a> ',
                    url:"https://forest-observatory.ec.europa.eu/wms/RMAP?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'cover_v2',
                        layerId: 'cover_v2undefined',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });

        }
        if(id=='ortofoto_2019'){
           layer = new ol.layer.Tile({
                title:'Sardegna ortofoto 2019',
                name:'Sardegna ortofoto 2019',
                source: new  ol.source.TileWMS({
                    url:"https://webgis.regione.sardegna.it/geoserverraster/raster/wms?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'raster:ortofoto_2019',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });
        }
        if(id=='ortofoto_2016'){
           layer = new ol.layer.Tile({
                title:'Sardegna ortofoto 2016',
                name:'Sardegna ortofoto 2016',
                source: new  ol.source.TileWMS({
                    url:"https://webgis.regione.sardegna.it/geoserverraster/raster/wms?SRS=EPSG:3857",
                    serverType: 'mapserver',
                    ratio: 1,
                    params: {
                        layers: 'raster:ortofoto_2016',
                        format: 'image/png'
                    },
                }),

                isBaseLayer: false,
                singleTile: false,
                visible: false,
                type:'wms',
                group: IMPACT.Utilities.OpenLayers.baseLayersText
            });
        }

        return layer;
    },


    /**
      *  Create and return background layers
      */
    createBackgroundLayer: function(id){

        var layer = null;


        if(id=='Esri'){
            layer = new ol.layer.Tile({
                title: 'Esri World Imagery',
                name: 'Esri World Imagery',
                isBaseLayer: true,
                group: IMPACT.Utilities.OpenLayers.baseLayersText,
                source: new ol.source.XYZ({
                    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                          //https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer
                    maxZoom: 19,
                    attributions: 'Esri, Maxar, Earthstar Geographics, and the GIS User Community'
                }),

                visible: false,
            });
        }
        else if(id=='google_satellite'){
            layer = new ol.layer.WebGLTile(
                {
                    title: 'Google Satellite',
                    name: 'Google Satellite',
                    isBaseLayer: true,
                    group: IMPACT.Utilities.OpenLayers.baseLayersText,
                    source: new ol.source.Google({
                            key:IMPACT.SETTINGS['apiKey_options']['googleKey'],
                            scale: 'scaleFactor2x',
                            highDpi: true,
                          }),
                    visible: false
                });
             //layer.setVisible(false);
        }
        else if(id=='Planet_NICFI'){
            var product='latest-series/45d01564-c099-42d8-b8f2-a0851accf3e7';
            var product='planet-tiles/planet_medres_normalized_analytic_2024-01_mosaic';
            var key = IMPACT.SETTINGS['apiKey_options']['planetKey'];
            layer = new ol.layer.WebGLTile({
                    style: {
                        exposure: ['var', 'exposure'],
                        contrast: ['var', 'contrast'],
                        saturation: ['var', 'saturation'],
                        variables:{
                                    contrast:0,
                                    exposure:0,
                                    saturation:0
                                }
                    },
                      title: 'Planet NICFI',
                      name: 'Planet NICFI',
                      isBaseLayer: true,
                      group: IMPACT.Utilities.OpenLayers.baseLayersText,
                      source: new ol.source.XYZ({
                        attributions: 'Planet NICFI',
                        url: 'https://tiles.planet.com/basemaps/v1/'+product+'/gmap/{z}/{x}/{y}.png?api_key='+key
                       }),
                      visible:false
                    });


             //layer.setVisible(false);
        }

        else if(id=='blue_marble'){
          //  single tile, it is fast to render, image is small
          layer = new ol.layer.Image({
              title: 'Blue Marble',
              name: 'Blue Marble',
              source: new ol.source.ImageWMS({
                      url: IMPACT.GLOBALS['url']+'/mapscript_img_fn_tornado.py?',
                      serverType: 'mapserver',
                      ratio: 1,
                      params: {
                        layers : ['IMG'],
                        transparent : 'true',
                        full_path: IMPACT.GLOBALS.root_path+'Gui/maps/land_shallow_topo_8192.tif',
                      }
            }),
            singleTile : IMPACT.SETTINGS['UseSingleTile'],
            transitionEffect : "resize",
            isBaseLayer: true,
            projection: IMPACT.SETTINGS['WGS84_mercator'],
            group: IMPACT.Utilities.OpenLayers.baseLayersText,
            visible:true
          });
          //layer.setVisible(true);
        }

        else if(id=='OSM'){
            layer = new ol.layer.Tile(
                {
                    title: 'OSM',
                    name: 'OSM',
                    isBaseLayer: true,
                    group: IMPACT.Utilities.OpenLayers.baseLayersText,
                    source: new ol.source.OSM(),
                    visible:false
                });
            //layer.setVisible(false);


        }

        else if(id=='none'){
            layer = new ol.layer.Vector({
                  title: 'None',
                  name: 'none',
                  isBaseLayer: true,
                  group: IMPACT.Utilities.OpenLayers.baseLayersText,
                  source: new ol.source.Vector({
                    attributions: 'Empty layer'
                  }),
                  visible:false
                });
            //layer.setVisible(false);
        }

        return layer;
    },

    /**
      *  Check if the the layer (with given name and "type") exists
      */
    layerExists: function(map, layerName, layerType){
        var allLayers = map.getAllLayers();
        if (typeof(allLayers) === 'undefined'){
            return false;
        }

        for(var i = 0; i < allLayers.length; i++){
            if (allLayers[i].get('type') == layerType && allLayers[i].get('name') == layerName ){
                return true;
            }
        }

        return false;
    },

    /**
      *  Return layer by full_path if exists
      */
    getLayerByFullPath: function(map, full_path){
        var allLayers = map.getAllLayers();

        if (typeof(allLayers) === 'undefined'){
            return false;
        }

        for(var i = 0; i < allLayers.length; i++){
             if ( typeof allLayers[i].get('type') != 'undefined'
                    && ['vector','raster','class'].includes(allLayers[i].get('type'))
                    && allLayers[i].getSource().getParams()['full_path'] == full_path
                    //&& allLayers[i].get('type') !== "hidden")
                    ){
                return allLayers[i];
            }
        }
        return false;
    },


        /**
      *  Return layer by name if exists
      */

    getAttributesLayer: function(map){
        var layers = [];
        var allLayers = map.getAllLayers();
        for(var i = 0; i < allLayers.length; i++){
            if(allLayers[i].get('type') == "attributes"){
                layers.push(allLayers[i]);
            }
        }
        return layers;
    },

    getTemporaryLayer: function(map){
        var allLayers = map.getAllLayers();
        for(var i = 0; i < allLayers.length; i++){
            if(allLayers[i].get('name') == "Temporary Features Layer"){
                return allLayers[i]
            }
        }
        return false;
    },
    getEditingAOI: function(map){
        var allLayers = map.getAllLayers();
        for(var i = 0; i < allLayers.length; i++){
            if(allLayers[i].get('name') == "editing_selection_aoi"){
                return allLayers[i]
            }
        }
        return false;
    },
    getDBFAOI: function(map){
        var allLayers = map.getAllLayers();
        for(var i = 0; i < allLayers.length; i++){
            if(allLayers[i].get('name') == "grid_selection"){
                return allLayers[i]
            }
        }
        return false;
    },

    /**
     *  Retrieve the tile size form map size
     */
    getTileSize: function(map){
        return [
            Math.floor(map.getSize()[0]/1.5),
            Math.floor(map.getSize()[1]/1.5)
            ]

    },

    getLayerNameList: function(map){
       var names = [];
       var mapLayers = map.getAllLayers();
       for(var i=0; i<mapLayers.length; i++){
            //names.push(mapLayers[i].name);
            names.push(mapLayers[i].get('name'));
       }
       return names;
    },

    getBaseLayers: function(map){
       var layers = [];

       var mapLayers = map.getAllLayers();
       for(var i=0; i<mapLayers.length; i++){
            if(mapLayers[i].get('isBaseLayer')==true){
                layers.push(mapLayers[i]);
            }
       }
       return layers;
    },

    getPlanetLayer: function(map){
       var layers = [];

       var mapLayers = map.getAllLayers();
       for(var i=0; i<mapLayers.length; i++){
            if(mapLayers[i].get('isBaseLayer')==true && mapLayers[i].get('name')=='Planet NICFI'){
                return mapLayers[i]
            }
       }
       return layers;
    },


    getWMSLayers: function(map){
       var layers = [];
       var mapLayers = map.getAllLayers();
       for(var i=0; i<mapLayers.length; i++){
            if(typeof mapLayers[i].get('type') != 'undefined' && mapLayers[i].get('type') === 'wms') {
                layers.push(mapLayers[i]);
            }
       }
       return layers;
    },

     /**
     * Retrieve class Layers from shapefile
     * @param Layer
     * @returns list of class fields
     */
    getEditingLayers: function(layer){

        var layers_available =  layer.data_from_DB['attributes'].split(',');

        var matching_layers = [];
        for(var i=0; i<layers_available.length; i++){
            if(layers_available[i] === IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                    || layers_available[i] === IMPACT.SETTINGS['vector_editing']['attributes']['class_t2']
                    || layers_available[i] === IMPACT.SETTINGS['vector_editing']['attributes']['class_t3']

                    || layers_available[i] === "T4_class"
                    || layers_available[i] === "T5_class"
                    || layers_available[i] === "T6_class"
                    || layers_available[i] === "T7_class"
                    || layers_available[i] === "T8_class"
                    || layers_available[i] === "T9_class"
                    || layers_available[i] === "T10_class"
               ){

                matching_layers.push(layers_available[i]);
            }
         }
        return matching_layers;
    }


});