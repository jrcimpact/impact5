/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.Utilities.Impact', {

    singleton: true,

    /**
     * Return the IMPACT tool version in a readable format
     * @returns {string}
     */
    version_readable: function(version){
        if (typeof(version)==="undefined"){
            version = IMPACT.GLOBALS['version'];
        }
        version = version.endsWith("a") ? version.replace("a", ' alpha') : version;
        version = version.endsWith("b") ? version.replace("b", ' beta') : version;
        version = version.startsWith("v.") ? version.replace("v.", '') : version;
        return version;
    },


    alertNewVersion: function(){
        var me = this;

        Ext.Ajax.request({
            method: 'GET',
            url: 'check4update.py',
            success: function(response){
                var responseJSON = JSON.parse(response.responseText);
                if (responseJSON === null){
                    console.log('Update error');
                    return
                }
                IMPACT.GLOBALS['repository'] = responseJSON;
                if(IMPACT.GLOBALS['repository']['up-to-date']==false){
                    var msgLog = 'Version '+IMPACT.GLOBALS['repository']['remote']['version']+' of IMPACT Toolbox is available!';
                    var msgAlert = '<b>'+msgLog+'</b>';
                    if(typeof(IMPACT.GLOBALS['repository']['remote']['changelog']) != "undefined" && IMPACT.GLOBALS['repository']['remote']['changelog'] != ''){
                        var changeLog = IMPACT.GLOBALS['repository']['remote']['changelog'].replace(/\n/g, '<br />');
                        msgAlert += '<br /><br /><i>Changelog:</i><br />'+changeLog;
                        msgLog += '<div class="sectionHeader">Version '+IMPACT.GLOBALS['repository']['remote']['version']+' changelog</div>' +
                                    '<div class="sectionBody">'+changeLog+'</div>';
                    }

                    if(IMPACT.GLOBALS['OS'] == 'win32'){

                            Ext.Msg.show({
                                title: 'New version available!',
                                msg: msgAlert,
                                buttonText: {
                                    ok: 'Update now',
                                    cancel: 'Update later'
                                },
                                fn: function(btn){
                                    if(btn == 'ok'){
                                        IMPACT.Utilities.Requests.launchProcessing({toolID: 'update'});
                                    }
                                }
                            });
                    } else {
                            msgAlert += '<br /><br /> Execute <b> docker pull </b> from your host machine / Admin console; restart the container';
                            Ext.Msg.show({
                                title: 'New version available!',
                                msg: msgAlert,
                                buttonText: {
                                    ok: 'Ok',

                                }
                            });


                    }
//                    me.SendWarning(msgLog);
                }
            },
            failure : function(response){
                //alert('error');
                console.log('Ajax error on update');
            }
        });
    },

    getLayerType: function(data_from_DB){
        var type = 'other';

        if(data_from_DB.hasOwnProperty('extension') && data_from_DB.hasOwnProperty('has_colorTable')){
            if(data_from_DB['extension']==='.tif' && data_from_DB['has_colorTable']===1){
                type = 'class';
            } else if(data_from_DB['extension']==='.tif' && data_from_DB['has_colorTable']===0){
                type = 'raster';
            } else if(data_from_DB['extension']==='.vrt'){
                type = 'virtual';
            } else if(data_from_DB['extension']==='.shp'){
                 type = 'vector';
            } else if(data_from_DB['extension']==='.kml'){
                 type = 'kml';
            } else if(data_from_DB['extension']==='.geojson' || data_from_DB['extension']==='.json'){
                 type = 'geojson';
            }
        }
        return type;
    },

    getBreadcrumbs: function(full_path, rootNodeValue){
        var path = IMPACT.Utilities.Common.dirname(full_path);
        path += '/';
        path = path
            .replace(rootNodeValue, '')
            .replace("\\", '/')
            .replace(/\/$/, '');
        if(path!==''){
            return path.split('/');
        }
        return [];
    },

    getReadableUnits: function(data_from_DB){
        var units = '';
        if(data_from_DB['PROJ4'].indexOf("+units=dd")>=0
                || data_from_DB['PROJ4'].indexOf("+proj=longlat")>=0) {
            units = 'Decimal Degree';
        } else {
            units = 'Meters';
        }
        return units;
    },

    getReadableEPSG: function(data_from_DB){
        var epsg = data_from_DB['EPSG'];
        if((epsg === null || epsg === "")
                && IMPACT.Utilities.Impact.getReadableUnits(data_from_DB) === 'Decimal Degree'){
            epsg = "EPSG:4326"
        }
        return epsg;
    },


     parse_bbox: function(data_from_DB){

        var rawextent = Ext.decode(data_from_DB['extent']);
        extent = [rawextent["W"], rawextent["S"], rawextent["E"], rawextent["N"]];

        // OpenLayers.Bounds in map projection
        //proj4.defs["temp_proj"] = data_from_DB['PROJ4'];
        var tmpCode="temp_proj:"+Date.now().toString();
        proj4.defs(tmpCode, data_from_DB['PROJ4']);
        ol.proj.proj4.register(proj4);
        var layer_projection = new ol.proj.Projection({code:tmpCode});

//        var bbox = new OpenLayers.Bounds(extent["W"], extent["S"], extent["E"], extent["N"])
//            .transform(new OpenLayers.Projection('temp_proj'), IMPACT.SETTINGS['WGS84_mercator']);




        var bbox1 = ol.proj.transformExtent(extent, layer_projection, IMPACT.SETTINGS['WGS84_mercator']);
        bbox={};
        bbox['left'] = bbox1[0]
        bbox['bottom'] = bbox1[1]
        bbox['right'] = bbox1[2]
        bbox['top'] =  bbox1[3]
        // projection
        bbox['PROJ4'] = data_from_DB['PROJ4'];
        bbox['EPSG'] = data_from_DB['EPSG'];
        // RAW extent
        bbox['Raw_N'] = rawextent["N"];
        bbox['Raw_S'] = rawextent["S"];
        bbox['Raw_E'] = rawextent["E"];
        bbox['Raw_W'] = rawextent["W"];

        return bbox;
    },

    token: function(modification_date){
        return modification_date
                .replace(' ', '')
                .replace('-', '')
                .replace(':', '');
    },

});