/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

//https://api.planet.com/basemaps/v1/mosaics/wmts?api_key=PLAK74e4bb271c634753975388f17be74ce7
Ext.define('IMPACT.view.Workspace.Main.component.PlanetWindow', {
    extend: 'IMPACT.view.component.Window',
    itemId: 'PlanetWindow',
    title: "Planet imagery",
    width: 500,
    height: 620,
    resizable: true,
    closeAction: 'hide',
    constrain: true,
    layout: {
        type: 'border',
    },
    map: null,
    view: null,
    items:[ {
            region:'north',
            //layout:'fit',
            height: 400,
            xtype:'panel',
            itemId: 'PlanetPanelN',
            html:'<div id="PlanetMap" style="height: 400px; width: 500px"></div>',
            listeners:{
                afterrender: function(){

                   var view = new ol.View({
                        center: [0, 0],
                        zoom: 2,
                        multiWorld: true,
                      })
                    var map = new ol.Map({
                      layers: [this.up('#PlanetWindow').createPlanetLayer('normalized','2024-01')],
                      target: 'PlanetMap',
                      view: view
                      });
                    this.up('#PlanetWindow').map = map;
                    this.up('#PlanetWindow').view = view;
//                    map.on('moveend', this.up('#PlanetWindow').addCenterPoint);

                 }
                 }
            },{
                xtype:'panel',
                itemId: 'PlanetPanelC',
                region:'center',
                layout:'vbox',
                //width:390,
                bodyStyle: 'padding:5px;',
                height: 100,
                items:[
                    {
                        xtype: 'radiogroup',
                        fieldLabel: 'Type',
                        labelWidth: 100,
                        labelStyle: 'font-weight: bold;',
                        columns: 3,
                        itemId: 'PlanetType',
                        items: [

                            {
                                xtype: 'radiofield',
                                boxLabel: 'Normalized',
                                labelWidth: 80,
                                name: 'Ptype',
                                inputValue: 'normalized',
                                checked: true,
                                width: 100
                            },
                            {
                                xtype: 'radiofield',
                                boxLabel: 'Visual',
                                labelWidth: 100,
                                name: 'Ptype',
                                inputValue: 'visual',
                                width: 100
                            },
                            {
                                xtype: 'radiofield',
                                labelWidth: 150,
                                boxLabel: 'Latest Monthly',
                                name: 'Ptype',
                                inputValue: 'latest',
                                width:150

                            },


                        ],
                        listeners : {
                               change : function(radio, newValue) {
                                    this.up('#PlanetWindow').sliderAction();
                                    if(newValue.Ptype == 'latest'){
                                        this.up('#PlanetWindow').down('#PlanetYY').setDisabled(true);
                                        this.up('#PlanetWindow').down('#PlanetMM').setDisabled(true);

                                    }else {
                                        this.up('#PlanetWindow').down('#PlanetYY').setDisabled(false);
                                        this.up('#PlanetWindow').down('#PlanetMM').setDisabled(false);
                                    }

                               }
                        }
                    },
                    {
                        xtype:'panel',
                        itemId: 'PlanetColor',
                        region:'center',
                        layout:'hbox',
                        //width:390,
                        bodyStyle: 'padding-left: 80px;',
                        height: 70,
                        items:[

                            {
                                xtype: 'slider',
                                itemId: 'PlanetExp',
                                //labelWidth: 30,
                                fieldLabel: 'Exp',
                                labelAlign: 'top',
                                width: 100,
                                //store: [2020,2021,2022,2023,2024],  //2015,2016,2017,2018,2019,
                                value: 0,
                                minValue: -1,
                                maxValue: 1,
                                increment: 0.1,
                                decimalPrecision: 1,
                                listeners: {
                                    changecomplete: function() {
                                       this.up('#PlanetWindow').sliderColorAction();
                                    }
                                }

                            },
                            {
                                xtype: 'slider',
                                itemId: 'PlanetCont',
                               // labelWidth: 30,
                                fieldLabel: 'Cont',
                                labelAlign: 'top',
                                width: 100,
                                //store: [2020,2021,2022,2023,2024],  //2015,2016,2017,2018,2019,
                                value: 0,
                                minValue: -1,
                                maxValue: 1,
                                increment: 0.1,
                                decimalPrecision: 1,
                                listeners: {
                                    changecomplete: function() {
                                       this.up('#PlanetWindow').sliderColorAction();
                                    }
                                }

                            },
                            {
                                xtype: 'slider',
                                itemId: 'PlanetSat',
                                fieldLabel: 'Sat',
                                labelAlign: 'top',
                                //labelWidth: 30,
                                width: 100,
                                //store: [2020,2021,2022,2023,2024],  //2015,2016,2017,2018,2019,
                                value: 0,
                                minValue: -1,
                                maxValue: 1,
                                increment: 0.1,
                                decimalPrecision: 1,
                                listeners: {
                                    changecomplete: function() {
                                       this.up('#PlanetWindow').sliderColorAction();
                                    }
                                }

                            }
                        ]
                    }

                ]


            },{
                    xtype:'panel',
                    region:'south',
                    itemId: 'PlanetPanelS',
                    layout:'hbox',
                    items:[
                            {
                                xtype: 'impactFieldCombo',
                                itemId: 'PlanetYY',
                                labelWidth: 60,
                                width: 80,
                                store: [2015,2016,2017,2018,2019,2020,2021,2022,2023,2024],  //2015,2016,2017,2018,2019,
                                value: 2024,
                                listeners: {
                                    change: function() {
                                        this.up('#PlanetWindow').sliderAction();
                                    }
                                }

                            },{
                                xtype: 'impactFieldSlider',
                                itemId: 'PlanetMM',
                                labelWidth: 60,
                                width: 400,
                                //store: [2020,2021,2022,2023,2024],  //2015,2016,2017,2018,2019,
                                value: 1,
                                minValue: 1,
                                maxValue: 12,
                                increment: 1,
                                listeners: {
                                    changecomplete: function() {
                                       this.up('#PlanetWindow').sliderAction();
                                    }
                                }

                            }

                    ]


               }

    ],


    sliderAction: function(id){
        var me = this;
        var valyy = Ext.ComponentQuery.query('#PlanetYY')[0].getValue();
        var valmm = Ext.ComponentQuery.query('#PlanetMM')[0].getValue();
        var valtype = Ext.ComponentQuery.query('#PlanetType')[0].getValue().Ptype;
        valmm = String(valmm).padStart(2, '0')
        var myDate = valyy+'-'+valmm;

        if( parseInt(valyy)*100 + parseInt(valmm) < 202007 ){
            if( parseInt(valmm) < 7){
                valmm = '06';
                var myDate = valyy+'-'+valmm+'_'+valyy+'-11';
            }else{
                valmm = '12';
                var myDate = valyy+'-'+valmm+'_'+ String(parseInt(valyy)+1)+'-05';
            }

            Ext.ComponentQuery.query('#PlanetMM')[0].down('#slider').setValue(parseInt(valmm));

        }


        me.map.getLayers().forEach(function(layer) {
            if(layer && layer.get('name') == 'Planet'){
                me.map.removeLayer(layer);
            }
        });
        me.map.addLayer(me.createPlanetLayer(valtype, myDate));

        var PlanetbaseLayers = IMPACT.Utilities.OpenLayers.getPlanetLayer(IMPACT.main_map);
        if (PlanetbaseLayers != [] && PlanetbaseLayers != 'undefined'){


            PlanetbaseLayers.getSource().setUrl(me.buildPlanetURL(valtype, myDate));
            PlanetbaseLayers.getSource().refresh();
        }


    },

    sliderColorAction: function(id){
        var me = this;
        var contr = Ext.ComponentQuery.query('#PlanetCont')[0].getValue();
        var expos = Ext.ComponentQuery.query('#PlanetExp')[0].getValue();
        var satur = Ext.ComponentQuery.query('#PlanetSat')[0].getValue();

        var updates = {};
        updates['exposure'] = expos;
        updates['contrast'] = contr;
        updates['saturation'] = satur;

        me.map.getLayers().forEach(function(layer) {
            if(layer && layer.get('name') == 'Planet'){
                layer.updateStyleVariables(updates);
            }
        });

        var PlanetbaseLayers = IMPACT.Utilities.OpenLayers.getPlanetLayer(IMPACT.main_map);
        if (PlanetbaseLayers != [] && PlanetbaseLayers != 'undefined'){
            PlanetbaseLayers.updateStyleVariables(updates);
            PlanetbaseLayers.getSource().refresh();
        }




    },

    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        if (typeof IMPACT.SETTINGS['apiKey_options']['planetKey'] == 'undefined' || IMPACT.SETTINGS['apiKey_options']['planetKey'] == ''){
            alert("Enter your Planet.com API KEY in the Settings Tab to visualize the basemaps")
        }

    },
//    afterLoad: function() {
//        var me = this;
//        me.callParent(arguments);
//        //document.getElementById("range_2022").addEventListener("change", me.sliderAction("range_2022"), false);
//        //document.getElementById("range_2021").addEventListener("change", me.sliderAction("range_2021"), false);
//    },

    buildPlanetURL: function(ptype,id){
        var url ='';
        var key = IMPACT.SETTINGS['apiKey_options']['planetKey'];
        var product = '';
        if (ptype == 'normalized'){
            product='planet-tiles/planet_medres_normalized_analytic_'+id+'_mosaic'
        }
        if (ptype == 'visual'){
            product='planet-tiles/planet_medres_visual_'+id+'_mosaic'
        }
        if (ptype == 'latest'){
            product='latest-series/45d01564-c099-42d8-b8f2-a0851accf3e7'
        }

        url = 'https://tiles.planet.com/basemaps/v1/'+product+'/gmap/{z}/{x}/{y}.png?api_key='+key

        return url


    },

    createPlanetLayer: function(ptype,id){
        var me = this;
        var variables = {
          contrast:Ext.ComponentQuery.query('#PlanetCont')[0].getValue(),
          exposure:Ext.ComponentQuery.query('#PlanetExp')[0].getValue(),
          saturation:Ext.ComponentQuery.query('#PlanetSat')[0].getValue(),
        };


        var layer = new ol.layer.WebGLTile({
          style: {
            exposure: ['var', 'exposure'],
            contrast: ['var', 'contrast'],
            saturation: ['var', 'saturation'],
            variables: variables,
          },
          source: new ol.source.XYZ({
            //attributions: attributions,
            url: me.buildPlanetURL(ptype,id)

            //maxZoom: 20,
          }),
        });
//        var layer = new ol.layer.Tile({
//              declutter: true,
//              renderMode: 'vector',
//              source: new ol.source.XYZ({
//                url: 'https://tiles.planet.com/basemaps/v1/'+product+'/gmap/{z}/{x}/{y}.png?api_key='+key
//              })
//        });
        layer.set('name', 'Planet');
        return layer
    },
    showHide: function(){
        var me = this;
        if(me.isHidden()===true){
            me.show();
        } else {
            me.hide();
        }
    },

    addCenterPoint: function(lonLat) {
             var me = this;
             me.map.getLayers().forEach(function (layer) {
                        if(layer && layer.get('name') == 'Marker'){
                            me.map.removeLayer(layer);
                        }
                    });


             var vectorLayer = new ol.layer.Vector({
                source: new ol.source.Vector()
              });
              var marker = new ol.Feature({
                geometry: new ol.geom.Point(lonLat)
              });
            var stroke = new ol.style.Stroke({color: 'black', width: 2});
            var fill = new ol.style.Fill({color: 'red'});
              var style = new ol.style.Style({
                image: new ol.style.RegularShape({
                  //fill: fill,
                  stroke: stroke,
                  points: 4,
                  radius: 4,
                  radius2: 0,
                  angle: 0,
                })
               });

              vectorLayer.getSource().addFeature(marker);
              vectorLayer.set('name', 'Marker');
              vectorLayer.setStyle(style);
              vectorLayer.setZIndex(100);
              me.map.addLayer(vectorLayer);
    },


    drawGeometry: function(wkt){
            var me = this;
            me.map.getLayers().forEach(function (layer) {
                    if(layer && layer.get('name') == 'Geom'){
                        me.map.removeLayer(layer);
                    }
                });
            const format = new ol.format.WKT();

            var feature = format.readFeature(wkt, {
              dataProjection: IMPACT.SETTINGS['WGS84_mercator'],
              featureProjection: IMPACT.SETTINGS['WGS84_mercator'],
            });


            var vectorLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [feature],
                })
            });

            var polygon_style = new ol.style.Style({
              stroke: new ol.style.Stroke({
                width: 2,
                color: "#ff0000"
              })
            });
            var style = new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'violet',
                width: 1
              })
            });


          vectorLayer.set('name', 'Geom');
          vectorLayer.setZIndex(101);
          vectorLayer.setStyle(style);
          me.map.addLayer(vectorLayer);
    }



})

