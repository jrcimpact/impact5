/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.StatsWindow', {
    extend: 'Ext.window.Window',

    itemId: 'StatsWindow',
    closeAction: 'destroy',
    autoShow: true,
    resizable: true,
    constrain: true,
    layout: 'fit',
    //overflowY: 'scroll',
    width: 500,
    height: 600,
    maxHeight: 600,

    data: null,
    values: [],
    names: [],

   constructor : function(data){
            this.data = data;
            this.callParent();
    },
    items: [
        {
            html:'<div id="chart" style="width: 450px; height: 350px;"></div>',
            maxHeight: 350
        },
        {
            xtype: 'gridpanel',
            //width:700,
            itemId: 'statsReport',
            //layout: 'fit',
            maxHeight: 180,
            //width:380,
            autoScroll: false,
            //overflowY: 'scroll',
            //title:"Report",
            store:{
                    xtype: 'store',
                    itemId:'statsStore',
                    fields: ['id', 'name', 'count'],
                    data : [],
                    autoLoad: false
                  },
            columns:[
                        {id: 'id', header: "Id", width: '10%', sortable: true, dataIndex: 'id'},
                        {id: 'name', header: "Name", width: '30%', sortable: true, dataIndex: 'name'},
                        {id: 'value', header: "Count", width: '59%', sortable: true, dataIndex: 'count'}
                     ]

        }
    ],

    buttons: [
        {
            text     : 'Close',
            handler  : function(){ this.up('#StatsWindow').destroy();}
        }
    ],



    afterShow: function(){
        this.populateStats();
        this.createEcharts();
    },
    createEcharts: function(){
        var me = this;

        var myChart = echarts.init(document.getElementById('chart'));

        // specify chart configuration item and data
        var option = {
            title: me.data.data['title'],
            tooltip: {},
            grid: {
                left: '5%',
                right: '10%',
                bottom: '10%',
                containLabel: true
            },
            //color: [me.data['seriesColor']],
//            legend: {
//                    type: 'scroll',
//                    orient: 'vertical',
//                    right: 10,
//                    top: 20,
//                    bottom: 20,
//                    data: me.names
//            },
            xAxis: {
                    type: 'value',
                    //xAxisIndex: 0,
                    //data: me.data['seriesData'].name
            },
            yAxis: {
                    type: 'value',
                    yAxisIndex: 0
            },

            series: [{
                //name: 'Values',
                type: 'bar',
                //barWidth: '40%',
                data: me.data.data['seriesData'], //[[0,10],[4,8],[7,1]]//
                //barGap:'-90%',
                //barCategoryGap:'10%',
                //large: true
                //
            }],

            dataZoom: [
                        {
                            id: 'dataZoomX',
                            type: 'slider',
                            xAxisIndex: [0],
                            filterMode: 'filter',
                            //start: 1,
                            //end: 99
                        },
                        {
                            id: 'dataZoomY',
                            type: 'slider',
                            yAxisIndex: [0],
                            filterMode: 'empty',
                            //start: 0,
                            //end: 99
                        },
//                         {
//                            type: 'inside',
//                            xAxisIndex: 0,
//                            filterMode: 'empty'
//                        },
                        {
                            type: 'inside',
                            yAxisIndex: 0,
                            filterMode: 'empty'
                        }


            ],

            toolbox: {
                show : true,
                orient:'vertical',
                feature : {
                             dataView : {show: true, title:'Data View', readOnly: true},
                             magicType : {show: true, title:['bar','line', 'pie'], type: ['bar','line','pie']},
                             restore : {show: true, title:'Restore'},
                             saveAsImage : {show: true, title:"Save As Image"}
                            }
            }
        };

        // use configuration item and data specified to show chart
        myChart.setOption(option);

    },

//

    populateStats: function(){
        var me = this;
        me.down('#statsReport').getStore().loadData(me.data.data['seriesData'], false);

    }

});