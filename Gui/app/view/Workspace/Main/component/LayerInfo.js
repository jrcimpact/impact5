/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.LayerInfo', {
    extend: 'IMPACT.view.component.Window',

    closeAction: 'destroy',

    layout: 'fit',
    width: 500,
    height: 400,
    overflowY: 'scroll',
    bodyStyle: 'padding: 5px; background-color: white; line-height: 15px; font-size:11px;',
    resizable: true,

    title: 'Layer Info',
    data_from_DB: null,
    text: '',

    buttons: null,

    /**
     * initComponent
     */
    initComponent: function(){
        var me = this;

        me.parseInfo();
        me.html = me.text;
        me.callParent();
    },

    parseInfo: function(){
        var me = this;

        var type = IMPACT.Utilities.Impact.getLayerType(me.data_from_DB);

        // File related information
        me.text += '<p class="layerInfoHeader">File information</p>';
        me.text += '<p class="layerInfoContent">';
        me.text += me.printAttribute('File name', '<span class="layerInfoName">'+me.data_from_DB['basename']+me.data_from_DB['extension']+'</span>');
        me.text += me.printAttribute('Path', me.data_from_DB['path']+'/');
        me.text += me.printAttribute('Last modification', me.data_from_DB['modification_date']);
        me.text += me.printAttribute('File size (Mb)', (parseFloat(me.data_from_DB['fileSize'])).toFixed(2).toString());
        me.text += '</p>';

        me.text += '<p class="layerInfoHeader">Spatial information</p>';
        me.text += '<p class="layerInfoContent">';
        me.text += me.getProj();
        me.text += '</p>';

        // Raster info
        if (type === 'raster'
                || type === 'class'
                || type === 'other' ){
            me.text += '<p class="layerInfoHeader">Raster information</p>';
            me.text += '<p class="layerInfoContent">';
            me.text += me.printAttribute('Number of bands', me.data_from_DB['num_bands']);
            me.text += me.printAttribute('Pixel Size', me.data_from_DB['pixel_size']);
            me.text += me.printAttribute('Data type', me.data_from_DB['data_type']);
            me.text += me.printAttribute('No data value', me.data_from_DB['no_data']);
            me.text += me.printAttribute('Number of columns (width)', me.data_from_DB['num_columns']);
            me.text += me.printAttribute('Number of rows (height)', me.data_from_DB['num_rows']);
            me.text += me.printStatistics();
            me.text += '</p>';
            me.text += me.printMetadata();
        }
        // Vector info
        else if(type === 'vector'){
            me.text += '<p class="layerInfoHeader">Vector information</p>';
            me.text += '<p class="layerInfoContent">';
            me.text += me.printAttribute('Geometry type', me.data_from_DB['data_type']);
            me.text += me.printAttribute('Number of features', me.data_from_DB['numFeatures']);
            me.text += me.printAttribute('Attributes', me.data_from_DB['attributes'].replace(/,/g, ', '));
            me.text += '</p>';
        }

    },

    printAttribute: function(label, value){
        if( typeof(value)!=="undefined"
                && value!==null
                && value!==''){
             return "<b>"+label+": </b>&nbsp;"+value.toString()+"</br>";
        }
        return '';
    },

    getProj: function(){
        var me = this;
        var text = '';
        // Extent
        var extent = JSON.parse(me.data_from_DB['extent']);
        text += '<b>Extent</b>:&nbsp;';
        text += '<i>west</i>='+extent.W.toFixed(2).toString();
        text += ' <i>north</i>='+extent.N.toFixed(2).toString();
        text += ' <i>east</i>='+extent.E.toFixed(2).toString();
        text += ' <i>south</i>='+extent.S.toFixed(2).toString()+'<br />';

        text += me.printAttribute('PROJ4', me.data_from_DB['PROJ4']);
        text += me.printAttribute('EPSG', IMPACT.Utilities.Impact.getReadableEPSG(me.data_from_DB));
        text += me.printAttribute('Unit', IMPACT.Utilities.Impact.getReadableUnits(me.data_from_DB));
        return text!=='' ? text : '';
    },

    printStatistics: function(){
        var me = this;
        var text = '';
        var statistics = JSON.parse(me.data_from_DB['statistics']);
        var simplificationMethod = me.data_from_DB['simplificationMethod'];

        for (var band_index in statistics) {
            if(statistics.hasOwnProperty(band_index)){
                text += '<b>Band '+(parseInt(band_index)+1).toString()+'</b> : ';
                text += '<i>Min:</i> '+statistics[band_index][0].toString()+'&nbsp;&nbsp;';
                text += '<i>Max:</i> '+statistics[band_index][1].toString()+'&nbsp;&nbsp;';
                text += '<i>Mean:</i> '+statistics[band_index][2].toString()+'&nbsp;&nbsp;';
                text += '<i>Std dev:</i> '+statistics[band_index][3].toString()+';<br />';
            }
        }
        if(text!==''){
            if(simplificationMethod !== '') {
                return '<br /><i>Statistics (<b>!- approximate -! </b>)</i><br />' + text;
            } else  {
                return '<br /><i>Statistics</i><br />' + text;
            }

        }
        return '';
    },

    printMetadata: function(){
        var me = this;
        var text = '';
        var metadata = JSON.parse(me.data_from_DB['metadata']);
        for (var property in metadata) {
            if(metadata.hasOwnProperty(property)){
                text += me.printAttribute(property, metadata[property]);
            }
        }
        if(text!==''){
            return'<p class="layerInfoHeader">Metadata</p>' +
                '<p class="layerInfoContent">' +
                    text +
                '</p>';
        }
        return '';
    }

});