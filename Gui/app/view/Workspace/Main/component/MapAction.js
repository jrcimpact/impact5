/*
 * Copyright (c) 2008-2015 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */

/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.MapAction', {
    extend: 'Ext.Action',

    /**
     * The OpenLayers control wrapped in this action.
     *
     * @cfg {OpenLayers.Control}
     */
    control: null,

    /**
     * Activate the action's control when the action is enabled.
     *
     * @property {Boolean} activateOnEnable
     */
    /**
     * Activate the action's control when the action is enabled.
     *
     * @cfg {Boolean} activateOnEnable
     */
    activateOnEnable: false,

    /**
     * Deactivate the action's control when the action is disabled.
     *
     * @property {Boolean} deactivateOnDisable
     */
    /**
     * Deactivate the action's control when the action is disabled.
     *
     * @cfg {Boolean} deactivateOnDisable
     */
    deactivateOnDisable: false,

    /**
     * The OpenLayers map that the control should be added to. For controls that
     * don't need to be added to a map or have already been added to one, this
     * config property may be omitted.
     *
     * @cfg {OpenLayers.Map}
     */
    map: null,

    /**
     * The user-provided scope, used when calling uHandler, uToggleHandler,
     * and uCheckHandler.
     *
     * @property {Object}
     * @private
     */
    uScope: null,

    /**
     * References the function the user passes through the "handler" property.
     *
     * @property {Function}
     * @private
     */
    uHandler: null,

    /**
     * References the function the user passes through the "toggleHandler"
     * property.
     *
     * @property {Function}
     * @private
     */
    uToggleHandler: null,

    /**
     * References the function the user passes through the "checkHandler"
     * property.
     *
     * @property {Function}
     * @private
     */
    uCheckHandler: null,

    /**
     * Create a MapAction instance. A MapAction is created to insert
     * an OpenLayers control in a toolbar as a button or in a menu as a menu
     * item. A MapAction instance can be used like a regular Ext.Action,
     * look at the Ext.Action API doc for more detail.
     *
     * @param {Object} config (optional) Config object.
     * @private
     */
    constructor: function(config){
        // store the user scope and handlers
//        this.uScope = config.scope;
//        this.uHandler = config.handler;
//        this.uToggleHandler = config.toggleHandler;
//        this.uCheckHandler = config.checkHandler;
//
        config.scope = this;
        config.handler = this.pHandler;
        config.toggleHandler = this.pToggleHandler;
        config.checkHandler = this.pCheckHandler;

        // set control in the instance, the Ext.Action
        // constructor won't do it for us
        this.control = config.control;
        var ctrl = this.control;
        delete config.control;
//
//        this.activateOnEnable = !!config.activateOnEnable;
//        delete config.activateOnEnable;
//        this.deactivateOnDisable = !!config.deactivateOnDisable;
//        delete config.deactivateOnDisable;


        this.disableAllInteractions();


        // register "activate" and "deactivate" listeners
        // on the control
        if (ctrl) {
            if (typeof(IMPACT.main_map) != 'undefined') {
                // add on construction, no need to activate now
                //ctrl.setActive(true);
                IMPACT.main_map.addInteraction(ctrl);
            }

//
//            if((config.pressed || config.checked)) {
//                console.log('ACTIVATE');
//                ctrl.setActive(true);
//            }
//            if (ctrl.active) {
//                console.log('pressed');
//                config.pressed = true;
//                config.checked = true;
//            }
//            ctrl.events.on({
//                activate: this.onCtrlActivate,
//                deactivate: this.onCtrlDeactivate,
//                scope: this
//            });

        }

        this.callParent(arguments);

    },

    /**
     * The private handler.
     *
     * @param {Ext.Component} The component that triggers the handler.
     * @private
     */
    pHandler: function(cmp){
        var ctrl = this.control;
//        if (ctrl &&
//        ctrl.type == OpenLayers.Control.TYPE_BUTTON) {
//            ctrl.trigger();
//        }
        if (this.uHandler) {
            this.uHandler.apply(this.uScope, arguments);
        }
    },

    /**
     * The private toggle handler.
     *
     * @param {Ext.Component} cmp The component that triggers the toggle
     *     handler.
     * @param {Boolean} state The state of the toggle.
     * @private
     */
    pToggleHandler: function(cmp, state){
        this.changeControlState(state);
        if (this.uToggleHandler) {
            this.uToggleHandler.apply(this.uScope, arguments);
        }
    },

    /**
     * The private check handler.
     *
     * @param {Ext.Component} cmp The component that triggers the check handler.
     * @param {Boolean} state The state of the toggle.
     * @private
     */
    pCheckHandler: function(cmp, state){
        this.changeControlState(state);
        if (this.uCheckHandler) {
            this.uCheckHandler.apply(this.uScope, arguments);
        }
    },

    /**
     * Change the control state depending on the state boolean.
     *
     * @param {Boolean} state The state of the toggle.
     * @private
     */
    changeControlState: function(state){
        this.disableAllInteractions();
        if (typeof(this.control) != 'undefined') {
            this.control.setActive(state);
        }


    },

   disableAllInteractions : function() {
//            var activeInteractions = IMPACT.main_map.getInteractions();
            IMPACT.main_map.getInteractions().forEach((i) => {

                        if (i instanceof ol.interaction.Draw ||
                            i instanceof ol.interaction.Select ||
                            i instanceof ol.interaction.Modify ||
                            i instanceof ol.interaction.KeyboardPan

                           ) {
                            i.setActive(false);
                          }
                    });


   }

});