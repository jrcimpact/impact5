/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.component.MapPanGui', {
    //extend: 'IMPACT.view.component.Window',  // ERROR if extending template
    extend: 'Ext.window.Window',
    requires: [
                'IMPACT.view.component.Field.Integer',
                'IMPACT.Utilities.Input'
              ],
    itemId: 'MapPanGui',

    title: '',

    autoShow: true,
    constrain: true,
    closeAction: 'destroy',
    closable: false,
    layout:'fit',
    resizable: false,
    bodyStyle: 'background-color: white;',

    width: 140,
    height: 190,
    buttons: [
        {
            text: 'Close',
            handler: function(){
                if(this.closeAction === 'hide'){
                    this.up('.window').hide();
                } else {
                    this.up('.window').close();
                }
            }
        }
    ],



    initComponent: function(){
        var me = this;
        me.callParent();

        me.title = 'Map Pan';// ('+me.data_from_DB['basename']+')';
        // add geom field in stor but not visible in GRID

        me.add(
            {
                xtype: 'panel',
                border: false,
                collapsible: false,
                margin: '5 1 1 30',
                items: [{
                        xtype: 'button',
                        border: false,
                        itemId: 'MapPanGui_Left',
                        tooltip: 'Shortcut: left arrow',
                        width: 20,
                        iconCls: 'fa fa-lg fa-red fa-arrow-left',
                        margin: '35 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('ArrowLeft');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'MapPanGui_Up',
                        width: 20,
                        tooltip: 'Shortcut: up arrow',
                        iconCls: 'fa fa-lg fa-red fa-arrow-up',
                        margin: '10 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('ArrowUp');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'MapPanGui_Right',
                        width: 20,
                        tooltip: 'Shortcut: right arrow',
                        iconCls: 'fa fa-lg fa-red fa-arrow-right',
                        margin: '35 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('ArrowRight');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'MapPanGui_Down',
                        width: 20,
                        tooltip: 'Shortcut: down arrow',
                        iconCls: 'fa fa-lg fa-red fa-arrow-down',
                        margin: '57 1 1 -43',
                        listeners: {
                            click: function() {
                                me.__applyShift('ArrowDown');
                            }
                        }
                    },
                     {  xtype: 'impactFieldInteger',
                        itemId: 'MapPanGui_shift_multiplier',
                        fieldLabel: 'Map %',
                        value: 50,
                        labelWidth: 60,
                        width: 110,
                        minValue: 1,
                        maxValue: 100,
                        validator: function(value){

                            if(value < 1 || value > 100){
                                return 'Values: 1-100';
                            }
                             return true;
                        }
                     }
                ]
            }
        );





    },

    __applyShift: function(where){
        var me = this;
        var shift_multiplier = IMPACT.Utilities.Input.parseField(me.down('#MapPanGui_shift_multiplier'));
        if (shift_multiplier == 0 ){
            return
        }
        var map = Ext.ComponentQuery.query('#MapPanel')[0].map;

        var extent = map.getView().calculateExtent(map.getSize());
        var CX = map.getView().getCenter()[0];
        var CY = map.getView().getCenter()[1];

        dimWE = Math.abs(extent[0] - extent[2]) / 100 * shift_multiplier *2;
        dimNS = Math.abs(extent[1] - extent[3]) / 100 * shift_multiplier *2;


        // Extent does not work properly, sometimes it jups or zooms


        if(where == 'ArrowUp'){
//            extent.top = extent.top - dimNS;
//            extent.bottom = extent.bottom - dimNS;
            CY = CY + dimNS;
        }
        if(where == 'ArrowDown'){
//            extent.top  = extent.top + dimNS
//            extent.bottom = extent.bottom + dimNS
            CY = CY - dimNS;
        }
        if(where == 'ArrowLeft'){
//            extent.left  = extent.left - dimWE;
//            extent.right = extent.right - dimWE;
            CX = CX - dimNS;
        }
        if(where == 'ArrowRight'){
//            extent.left = extent.left + dimWE;
//            extent.right = extent.right + dimWE;
            CX = CX + dimNS;
        }

        //map.zoomToExtent(extent);<
        map.getView().setCenter([CX,CY]);
    },


});