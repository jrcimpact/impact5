/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.GeeWindow', {
    extend: 'IMPACT.view.component.Window',
    itemId: 'GEEWindow',
    title: "Gee Tiles",
    width: 900,
    height: 630,
    resizable: true,
    closeAction: 'destroy',
    constrain: true,
    layout: {
        type: 'border',
    },
    lastRequest: '',
    lat: null,
    lon: null,
    GEEInfo: null,
    R: 'SWIR1',
    G: 'NIR',
    B: 'BLUE',
    minVal: 0.1,
    maxVal: 0.4,
    sensor: 'Landsat (all)',
    timeFrame: 'day',
    inBuff: 30,
    outBuff: 120,
    dateStart: Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -2), "Y-m-d"),
    dateEnd: Ext.Date.format(new Date(), "Y-m-d"),
    execMode: '',         // set to empty, GSW or TMF to retrieve info from predefined Shapefile --- TMF validation on specific date
    disable_gui: false,

    //dateGap: 1,
    //layout:'column',
    items: [{
                xtype: 'panel',
                title: 'Plot',
                itemId: 'Gee_plot',
                region: 'north',
                border: true,
                //flex: 1,
                //overflowX: 'scroll',
                //scrollable: true,
                height: 150,
                items: [{html:'<div id="myChartId" style="width: 850px; height: 130px;"></div>'}]
            },{
                defaultType: 'container',
                layout: 'fit',
                title: 'Images',
                itemId: 'Gee_tiles',
                border: true,
                region: 'center',
                //flex: 3,
                overflowY: 'scroll',
                //autoScroll: true,
                //scrollable: true,
                items: []
            },{
                xtype: 'panel',
                region: 'west',
                width: 130,
                itemId: 'GEE_container',
                border: true,
                //collapsible: false,
                title: 'Parameters',
                //bodyStyle: 'background-color: rgba(0, 0, 0, 0);',
                layout: 'column',
                items: [
                        {
                            xtype: 'datefield',
                            label: 'Start',
                            labelAlign:'top',
                            itemId: 'GEE_startDate',
                            format: 'd/m/Y',
                            //name: 'Start',
                            width: 120,
                            height: 20,
                            //border: false,
                            value: Ext.Date.add(new Date(), Ext.Date.MONTH, -2),
                            disabled: this.disable_gui,
                            listeners : {
                                'change' : function(field, newValue, oldValue) {
                                     this.up('#GEEWindow').dateStart = Ext.Date.format(newValue, "Y-m-d")
                                }
                            }

                        },
                        {
                            xtype: 'datefield',
                            label: 'End',
                            labelAlign:'top',
                            itemId: 'GEE_endDate',
                            format: 'd/m/Y',
                            //name: 'End',
                            width: 120,
                            height: 20,
                            //border: false,
                            value: new Date(),
                            disabled: this.disable_gui,
                            listeners : {
                                'change' : function(field, newValue, oldValue) {
                                    this.up('#GEEWindow').dateEnd = Ext.Date.format(newValue, "Y-m-d");
                                }
                            }

                        },
                        {
                            xtype: 'impactFieldCombo',
                            itemId: 'GEE_sensor',     // band_r_combo band_g_combo band_b_combo
                            //fieldLabel: 'B',
                            //labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
                            //labelWidth: 10,
                            width: 120,
                            style: 'margin-right: 1px;',
                            store: ['Landsat (all)','Sentinel2'],
                            value: 'Landsat (all)',
                            listeners: {
                                    select: function (combo) {
                                       this.up('#GEEWindow').sensor = combo.getValue();
                                    }
                            },
                            disabled: this.disable_gui
                        },
                        {
                            xtype: 'impactFieldCombo',
                            itemId: 'GEE_bestImage',     // band_r_combo band_g_combo band_b_combo
                            fieldLabel: 'Reducer',
                            //labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
                            labelWidth: 47,
                            width: 120,
                            //style: 'margin-right: 1px;',
                            store: ['Year','Month','Day'],
                            value: 'Day',
                            listeners: {
                                    select: function (combo) {
                                       this.up('#GEEWindow').timeFrame = combo.getValue();
                                    }
                            },
                            disabled: this.disable_gui
                        },
                        {
                            xtype: 'impactFieldCombo',
                            itemId: 'GEE_band_R',     // band_r_combo band_g_combo band_b_combo
                            fieldLabel: 'R',
                            labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
                            labelWidth: 10,
                            width: 120,
                            style: 'margin-right: 10px;',
                            store: ['BLUE','GREEN','RED','NIR','SWIR1','SWIR2'],
                            value: 'SWIR1',
                            listeners: {
                                    select: function (combo) {
                                       this.up('#GEEWindow').R = combo.getValue();
                                    }
                            }
                        },
                        {
                            xtype: 'impactFieldCombo',
                            itemId: 'GEE_band_G',     // band_r_combo band_g_combo band_b_combo
                            fieldLabel: 'G',
                            labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
                            labelWidth: 10,
                            width: 120,
                            style: 'margin-right: 10px;',
                            store: ['BLUE','GREEN','RED','NIR','SWIR1','SWIR2'],
                            value: 'NIR',
                            listeners: {
                                    select: function (combo) {
                                       this.up('#GEEWindow').G = combo.getValue();
                                    }
                            }
                        },
                        {
                            xtype: 'impactFieldCombo',
                            itemId: 'GEE_band_B',     // band_r_combo band_g_combo band_b_combo
                            fieldLabel: 'B',
                            labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
                            labelWidth: 10,
                            width: 120,
                            style: 'margin-right: 10px;',
                            store: ['BLUE','GREEN','RED','NIR','SWIR1','SWIR2'],
                            value: 'BLUE',
                            listeners: {
                                    select: function (combo) {
                                       this.up('#GEEWindow').B = combo.getValue();
                                    }
                            }
                        },
                         {html:' RGB Min'},
                        {   xtype: 'impactFieldSlider',
                                //fieldLabel: 'RGB Min',
                                itemId: 'GEE_min',
                                labelWidth: 0,
                                width: 120,
                                value: 0.1,
                                maxValue: 1,
                                minValue: 0,
                                increment: 0.01,
                                decimalPrecision: 2,
                                disable_textField: true,
                                listeners: {
                                    changecomplete: function(){
                                        this.up('#GEEWindow').minVal=this.getValue();
                                    }
                                }
                        },
                            {html:' RGB Max'},
                             {  xtype: 'impactFieldSlider',
                                //fieldLabel: 'RGB Max',
                                itemId: 'GEE_max',
                                labelWidth: 0,
                                width: 120,
                                value: 0.4,
                                maxValue: 1,
                                minValue: 0,
                                increment: 0.01,
                                decimalPrecision: 2,
                                disable_textField: true,
                                listeners: {
                                    changecomplete: function(){
                                        this.up('#GEEWindow').maxVal=this.getValue();
                                    }
                                }
                        },
                        {
                            xtype: 'impactFieldInteger',
                            itemId: 'GEE_inner',
                            margin: '5 0 0 0',
                            fieldLabel: 'Buffer In (mt)',
                            labelWidth: 75,
                            width: 120,
                            value: 30,
                            minValue: 1,
                            maxValue: 10000,
                            listeners: {
                                change: function(field, value){
                                        this.up('#GEEWindow').inBuff= value;
                                }
                            }
                        },{
                            xtype: 'impactFieldInteger',
                            itemId: 'GEE_outer',
                            margin: '5 0 0 0',
                            fieldLabel: 'Buffer Out (mt)',
                            labelWidth: 75,
                            width: 120,
                            value: 120,
                            minValue: 100,
                            maxValue: 10000,
                            listeners: {
                                change: function(field, value){
                                    this.up('#GEEWindow').outBuff = value;
                                }
                            }
                        },{
                            xtype: 'button',
                            itemId: 'GEE_applyChanges',
                            tooltip: 'Apply changes',
                            fieldLabel:'Apply',
                            width: 120,
                            iconCls: 'fa-lg fa-green fas fa-sync',
                            cls: 'impact-icon-button',
                            handler: function () {
                                var GEEWindow = this.up('#GEEWindow');
                                GEEWindow.clearPanel();
                                GEEWindow.retrieveDataFromClick(GEEWindow.lon, GEEWindow.lat, GEEWindow.GEEInfo);
                            }
                        }



                ]
            }




            ],

    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.on('refreshFromShapefileClick', function(lon, lat, GEEInfo){
            me.clearPanel();
            me.retrieveDataFromClick(lon, lat, GEEInfo);
        });

    },

    clearPanel: function(){
        var me = this;
        me.down('#Gee_tiles').removeAll();
        me.populatePLOT([[0],[0]],['0']);
        //me.down('#Gee_plot').removeAll();

    },

    retrieveDataFromClick : function(lon, lat, GEEInfo){
        var me = this;
        me.lat = lat;
        me.lon = lon;
        me.GEEInfo = GEEInfo;
        me.execMode = "";


        var categories = [];
        var ndvi = [];
        var nbr = [];
        var dates = [];


        if(GEEInfo != '' && GEEInfo != null){
            me.execMode = "TMF";
            sceneDates = GEEInfo['sceneDates'];

            nbr = GEEInfo['nbr'];
            ndvi = GEEInfo['ndvi'];

//            console.log('NDVI NBR');
//            console.log(nbr);
//            console.log(ndvi);

            if(nbr.length == 0 && ndvi.length == 0){
                me.down('#Gee_plot').hide();
            } else{
                me.down('#Gee_plot').show();
            }

//            console.log('---------------------');
//            console.log(sceneDates)
//            console.log(sceneDates.length)
//            console.log(nbr)
//            console.log(ndvi)
//            console.log('---------------------');




            var data4series=[nbr,ndvi];

            me.populatePLOT(data4series,sceneDates);
            me.populateIMG(lon, lat, sceneDates);



/*
            var params = {
                layer_files: JSON.stringify([shp]),
                lat: lat,
                lon: lon
            };
            Ext.Ajax.request({
                url : "query_file.py",
                method: "GET",
                params: params,
                success : function(response) {
                    var response = JSON.parse(response.responseText);
                    var dates = response[0].features[0].date.split(";");
                    if (dates.slice(-1)[0] == ''){
                        dates.pop()
                    }
                    var years = response[0].features[0].year.split(";");
                    if (years.slice(-1)[0] == ''){
                        years.pop()
                    }
                    for (i in dates){
                        categories.push(years[i]+'-'+dates[i])
                    }
                    if (response[0].features[0]["ndvi"] !== undefined){
                        hue = response[0].features[0].hue.split(";").map(Number);
                        if (hue.slice(-1)[0] == ''){
                            ndvi.pop()
                        }
                    }
                    if (response[0].features[0]["nbr"] !== undefined){
                        nbr = response[0].features[0].nbr.split(";").map(Number);
                        if (nbr.slice(-1)[0] == ''){
                            nbr.pop()
                        }
                    }

                    var planet = '';
                    if (response[0].features[0]["Planet"] !== undefined){
                        planet = response[0].features[0].Planet.split(";");
                    }

                    var data4series=[nbr,hue];
                    me.populatePLOT(data4series,categories);
                    me.populateIMG(lon, lat, categories, planet);


                },
                failure: function(response) {
                    console.log('query_by_point failure: ', response.responseText)
                }
            })*/

        }
        else{
            me.down('#Gee_plot').hide();
            var LonLat = ol.proj.transform([lon,lat], IMPACT.SETTINGS['WGS84_mercator'], IMPACT.SETTINGS['WGS84']);
            var url = IMPACT.GLOBALS['url']+"/getGeeDateList.py?";
            url += "lat="+LonLat[1]+"&lon="+LonLat[0];
            url += "&dateStart=" + me.dateStart;
            url += "&dateEnd=" + me.dateEnd;
            url += "&timeFrame=" + me.timeFrame;
            url += "&sensor=" + me.sensor;
            Ext.Ajax.request({
                timeout: 300*1000,
                cors: true,
                useDefaultXhrHeader: false,
                url : url,
                success : function(response) {
                    sceneDates = JSON.parse(response.responseText);
                    if (response.responseText == '"Authentication_expired"'){
                        alert('GEE Authentication token not valid, please Authenticate using the GEE option in the Settings panel.')
                    } else {
                        if (response.responseText == '[]'){
                            alert('NO images available here.');
                        }
                        me.populateIMG(lon, lat, sceneDates);
                    }
                },
                failure: function(response) {
                    console.log('getGeeDateList failure: ', response.responseText)
                }
            })

            }
    },

    populateIMG: function(lon, lat, sceneDates=[]){
        var me = this;
        me.lat = lat;
        me.lon = lon;

        var LonLat = ol.proj.transform([lon,lat], IMPACT.SETTINGS['WGS84_mercator'], IMPACT.SETTINGS['WGS84']);


        var DivID = "GEE_"+LonLat[0]+"_"+LonLat[1];
        var content = '<div id="'+DivID+'" class="gee_style_wrapper">'


        for (date in sceneDates){
//            console.log('GeeDiv_'+sceneDates[date]);
            content += '<div class="gee_style_div" id="GeeDiv_'+sceneDates[date]+'"></div>'
        }


        content += '</div>';
        me.down('#Gee_tiles').update({html: content});


        // SceneDates are provided from SHP or from getGEEDateList for normal use
        for (date in sceneDates){
            IMPACT.Utilities.Requests.getGeeThumbUrl(me, LonLat[0], LonLat[1], sceneDates[date], me.timeFrame, me.R, me.G, me.B, me.minVal, me.maxVal, me.inBuff, me.outBuff, me.execMode, me.sensor)
        }


    },

    populatePLOT: function(series_data, category){
        var me = this;
        if( typeof(series_data)!="undefined"
            && series_data!=null
            && series_data!=''){

                var data4series=[{
                                    name: 'NBR',
                                    type: 'line',
                                    symbol: 'triangle',
                                    symbolSize: 6,
                                    lineStyle: { color: '#5470C6', width: 1, type: 'dashed'},
                                    itemStyle: {borderWidth: 1, borderColor: '#EE6666', color: 'yellow'},
                                    data: series_data[0]
                                },{
                                    name: 'NDVI',
                                    type: 'line',
                                    symbol: 'circle',
                                    symbolSize: 6,
                                    lineStyle: {color: '#5470C6',width: 1,type: 'dashed'},
                                    itemStyle: {borderWidth: 1,borderColor: '#EE6666',color: 'green' },
                                    data: series_data[1]
                                }];

                var option = {
//                                title: {
//                                    text: 'Profiles'
//                                },
                                legend: {
                                    data: ['NBR', 'NDVI'],
                                },
                                xAxis: {
                                    type: 'category',
                                    data: category
                                },
                                yAxis: {
                                    type: 'value',
                                    //min: 0
                                    //max: 1
                                },
                                dataZoom: [{
                                                show: false
                                           },
//                                           {
//                                                type: 'inside',
//                                           },
                                           {
                                                show: true,
                                                yAxisIndex: 0
                                }],
                                series: data4series,
                                tooltip: {trigger: 'axis'},
                                grid: {
                                    top:'10%',
                                    left: '3%',
                                    right: '2%',
                                    bottom: '5%',
                                    containLabel: true
                                }
                            };
                var myChart = echarts.init(document.getElementById('myChartId'));
                myChart.setOption(option);
        }
    }

});