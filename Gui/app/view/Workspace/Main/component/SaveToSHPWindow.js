/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.SaveToSHPWindow', {
    extend: 'Ext.window.Window',

    itemId: 'SaveToSHPWindow',

    title: "Save features to Shapefile",
    resizable: false,
    constrain: true,
    closeAction: 'destroy',

    width: 250,

    items: [
        {
            xtype: 'form',
            id: 'SaveToSHPForm',
            height: 50,
            layout: 'column',
            border: false,
            items: [
                {
                    xtype: 'textfield',
                    name: 'SaveToSHPName',
                    fieldLabel: 'Shapefile name',
                    labelAlign: 'top',
                    labelStyle: 'margin-bottom: 3px; font-weight: bold;',
                    width: 225,
                    style: 'margin: 0 0 0 5px;',
                    value: '',
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false,
                    allowBlank: false,
                    regex: /^[0-9a-z_A-Z]+$/
                }
            ]
        }


    ],

    buttons: [
        {
            text: 'Save',
            id: 'shp_save_button_id',
            handler: function(th){
                var form = this.up('#SaveToSHPWindow').down('#SaveToSHPForm').getForm();
                if (form.isValid()) {
                    this.up('#SaveToSHPWindow').SaveToSHP(form.getValues().SaveToSHPName);
                }

            }
        }
    ],

    SaveToSHP: function(shp_filename){
        var me = this;

        var layer = IMPACT.Utilities.OpenLayers.getTemporaryLayer(IMPACT.main_map).getSource();//.getFeaturesCollection();
        var i=0;
        layer.getFeatures().forEach(feat => {
                feat.setProperties({ "ID" : i.toString() , "Name" : "Box" + i.toString() });
                feat.setId(i);
                // no need to reproject, done by OL Json Format
                //feat.setGeometry(feat.getGeometry().transform(IMPACT.SETTINGS['WGS84_mercator'], IMPACT.SETTINGS['WGS84']));
                i = i+1;
        })
        var geoJSON = new ol.format.GeoJSON().writeFeatures(layer.getFeatures(), {dataProjection: 'EPSG:4326', featureProjection: IMPACT.SETTINGS['WGS84_mercator']});

        Ext.Ajax.request({
            url : "save_user_feature_to_shapefile.py",
            method: 'POST',
            params: {
                shp_filename: shp_filename,
                geoJSON: geoJSON
            },
            success : function(response) {
                if(response.responseText.indexOf('OK') !== -1){

                    // Remove features from layer
                    layer.clear();

                     // Close window and disable save button
                    Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(false);
                    Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
                    me.destroy();

                } else {
                    alert("Error in saving features");
                    return false;
                }
            },
            failure : function(response) {
                alert("Error in saving features");
                return false;
            }
        });
    }




});