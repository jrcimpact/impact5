/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.MapScale', {
    requires:[
        'IMPACT.view.Workspace.Main.component.ToolbarLabel'
    ],
    extend: 'Ext.container.Container',

    itemId: 'MapScale',

    layout: {
        type: 'hbox'
    },

    numDigits: 0,

    initComponent: function () {
        var me = this;

        me.items = [
            // ## X coordinate: Longitude ##
            Ext.create('IMPACT.view.Workspace.Main.component.ToolbarLabel', {
                text: 'Scale: '
            }),
            Ext.create('IMPACT.view.Workspace.Main.component.ToolbarLabel', {
                itemId: 'mapScale',
                margin: '0 0 0 5px',
                width: 80,
                mode: 'highlight'
            })
        ];

        me.callParent();
    },
    setScale: function(scale){
        var me = this;
        scale = scale.toFixed(me.numDigits);
        me.down('#mapScale').setText("1:"+scale);
    }

});