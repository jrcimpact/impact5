/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.component.DbfAttributeTable', {
    //extend: 'IMPACT.view.component.Window',  // ERROR if extending template
    extend: 'Ext.window.Window',

    itemId: 'DbfAttributeTable',

    title: '',

    autoShow: true,
    constrain: true,
    closeAction: 'destroy',
    layout:'fit',
    resizable: true,
    bodyStyle: 'background-color: white;',
    buttons: null,
    width: 500,
    height: 500,
    maxHeight: 900,
    // autoScroll: true,

    data_from_DB: null,
    editingMode: null,
    items: [],
    store: null,

    layerNode: null,

    selection_layer: null,
    editingContainer: null,


    listeners:{
        'beforedestroy':function(win){
            var me = this;
            //if (me.editingMode == false){
                var map = IMPACT.main_map;
                me.selection_layer.getSource().clear();
                //me.selection_layer.destroyFeatures({ silent: true });
                map.removeLayer(me.selection_layer);
                me.selection_layer = null;
            //}

            me.store.removeAll();
            me.store = null;
            //me.data_from_DB = null;  clean the dropdown menu
        }
    },


    initComponent: function(){
        var me = this;
        var map = Ext.ComponentQuery.query('#MapPanel')[0].map;
        me.callParent();

        me.data_from_DB = me.layerNode.data.data_from_DB;


        //if (me.editingMode != true){
            // create a local selection layer
            source = new ol.source.Vector({
                wrapX: false,
                features: new ol.Collection(),
                params : {
                    buffer: 0,
                }
            });
            me.selection_layer = new ol.layer.Vector({

                title: "grid_selection",
                name: "grid_selection",

                source: source,
                visible: true,
                isBaseLayer: false,
                buffer: 0,
                type: 'hidden',
                style: new ol.style.Style({
                      stroke: new ol.style.Stroke({
                        color: '#ff0080',
                        width: 1.5
                      })
                    })

//                style: new ol.style.Style({
//                    fill: false,
//                    strokeColor: "#0050ff",
//                    strokeWidth: 1.5
//                })
            });
            //me.selection_layer.setVisible(true);



//            me.selection_layer = new OpenLayers.Layer.Vector("grid_selection", {
//                buffer: 0,
//                displayInLayerSwitcher: false,
//                visibility: true,
//                type: 'hidden',
//                style: {
//                    fill: false,
//                    strokeColor: "#0050ff",
//                    strokeWidth: 1.5
//                }
//            }),

            // clean old layer id DBF is destroyed by a new DBF call
            // Add selection layer to map
            map.addLayer(me.selection_layer);
            me.selection_layer.setZIndex(1000);

        //} else {
            // add reference to editingContainer
          //  me.editingContainer = Ext.ComponentQuery.query('#EditingVector')[0];
          //  me.selection_layer = me.editingContainer.selection_layer;
        //}
    if (me.editingMode){
        me.editingContainer = Ext.ComponentQuery.query('#EditingVector')[0];
    }


        me.title = 'Shapefile attributes ('+me.data_from_DB['basename']+')';
        // add geom field in stor but not visible in GRID
        var fields = me.data_from_DB['attributes'].split(',');
        fields.push('geom');
        //fields.push('BBox');
        me.store = new Ext.data.Store({
            fields: fields,
            pageSize: 1000,
            remoteSort: true,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'get_vector_DBF_in_Json.py',
                extraParams: {
                    filepath: me.data_from_DB['full_path']
                },
                reader: {
                    type: 'array',   // array and no item to be faster
                    rootProperty: 'items',
                    totalProperty: 'total'
                }
            }
        });

        me.add(
            {
                xtype: 'gridpanel',
                border: false,
                itemId: 'dbfGridpanel',
                store: me.store,
                columns: me.__columns(),
                selModel: new Ext.selection.RowModel({
                    mode: "MULTI"
                }),
                plugins: [
                        {
                            ptype: 'rowediting',
                            clicksToEdit: 2,
                            autoCancel: true,
                            errorSummary: false,
                            listeners: {
                                beforeedit: function(editor, event){
                                    if (!me.editingMode){
                                        alert('Please start Editing Mode to modify fields');
                                        return false;
                                    }
                                },
                                edit: function(editor, event) {
                                    me.__SaveRecord(event);
                                    return false;
                                }
                            }
                        }
                ],
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: me.store,
                    displayInfo: true,
                    dock: 'bottom'
                }],
                // ------  add  context menu -----
                viewConfig: {

                    preserveScrollOnRefresh: true,
                    preserveScrollOnReload: true,

                    stripeRows: true,
                    listeners: {
                        itemcontextmenu: function(view, rec, node, index, event) {
                            event.stopEvent();
//                            var geom = OpenLayers.Geometry.fromWKT(me.store.getAt(index).get('geom')).getBounds();
                            var format = new ol.format.WKT();
                            var Geom = me.store.getAt(index).get('geom');
                            if(Geom.includes("POINT")){
                                if (Geom.split(' ').length > 3){
                                    //Geom = Geom.replace('POINTS', 'POINT');
                                    // in case of 3D points
                                    Geom = 'POINT '+Geom.split(' ')[1]+' '+ Geom.split(' ')[2]+')'
                                }
                            };
                            var geom =  format.readGeometry(Geom,
                                                           //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                                                           ).getExtent();
                            me.__DBFcontextMenu(event, geom);
                            return false;
                        },
                        itemclick : function(view, rec, node, index, event) {
                            event.stopEvent();
                            me.__ShowSelected(this);
                            return false;
                        }
                    }
                }
            }
        );

    },

    __columns: function(){
        var columns = [];
        this.data_from_DB['attributes'].split(',')
            .forEach(function(attribute){
            columns.push(
                {
                    text: attribute,
                    dataIndex:  attribute,
                    editor: {
                                // defaults to textfield if no xtype is supplied
                                //allowBlank: true
                            }
                }
            );
        });
        return columns;
    },


    __DBFcontextMenu : function(e, bbox){

        if(typeof Ext.ComponentQuery.query('#DbfContextMenu')[0] != "undefined"){
                Ext.ComponentQuery.query('#DbfContextMenu')[0].destroy();
        }
        var DBFcontextMenu = new Ext.menu.Menu({
            itemId: 'DbfContextMenu',
            items: [{
                    text: 'Zoom to',
                    handler: function () {Ext.ComponentQuery.query('#MapPanel')[0].map.getView().fit(bbox);}
                    },
                    {
                    text: 'Pan to',
                    handler: function () {
                        //console.log(bbox);
                        Ext.ComponentQuery.query('#MapPanel')[0].map.getView().setCenter(ol.extent.getCenter(bbox))}
                    }]
        }).showAt(e.getXY());
    },

    __ShowSelected : function(grid){
        var me = this;

        me.selection_layer.getSource().clear(); //({ silent: true });
        //me.selection_layer.destroyFeatures({ silent: true });

        var selected = grid.getSelectionModel().getSelection();

        if (me.editingMode){
            me.editingContainer.selectionStore.removeAll();
            selected.forEach(function(item){
                me.editingContainer.__addSelectedItemToStore(item.data);
            });
//            // todo __addSelectedItemToStore is used by the DBF editor - conflict during editng TMF but no buttons from Vector- adding if but need to be fixed
//     --------  moved to vector GUI
//            if(typeof Ext.ComponentQuery.query('#NextFeatId')[0] !== 'undefined'){
//                Ext.ComponentQuery.query('#NextFeatId')[0].enable();
//                Ext.ComponentQuery.query('#PrevFeatId')[0].enable();
//                Ext.ComponentQuery.query('#clearSelectionButton')[0].addCls('highlightButton');
//            }

        } //else {
                // fill local features only
            var format = new ol.format.WKT();
            selected.forEach(function(item){
                var Geom = item.get('geom');
                if(Geom.includes("POINT")){
                    // replace point geom with buffered one
                    if (Geom.split(' ').length > 3){
                        //Geom = Geom.replace('POINTS', 'POINT');
                        // in case of 3D points
                        Geom = 'POINT '+Geom.split(' ')[1]+' '+ Geom.split(' ')[2]+')'
                    }
                    //geometry = geometry.buffer(20);
                }
                var feat =  format.readFeature(Geom,
                                               //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                                               );
                feat.setProperties({'id': item.get(IMPACT.SETTINGS['vector_editing']['attributes']['ID']) })
                me.selection_layer.getSource().addFeature( feat );
            });





    },

    __SaveRecord : function (event){
        var me = this;
        var full_path = me.data_from_DB['full_path'];
        var fieldList = me.data_from_DB['attributes'].split(',');

        var sql = ' UPDATE \"'+me.data_from_DB['basename']+'\" SET ';

        for(var item in fieldList){
            var values = '\"\"';
            if (event.newValues[fieldList[item]] != null ){
                values = event.newValues[fieldList[item]].toString();
            }
            if (fieldList[item].toString() === IMPACT.SETTINGS['vector_editing']['attributes']['ID'].toString()){
                continue
            }else{
                sql = sql + fieldList[item] + ' = \"' + values.toString() + '\" ,'
            }
        }
        sql = sql.slice(0,-1) + ' WHERE '+ IMPACT.SETTINGS['vector_editing']['attributes']['ID'] +' = \"'+event.newValues[IMPACT.SETTINGS['vector_editing']['attributes']['ID']]+'\"'

        IMPACT.Utilities.Requests.updateDBFrecord(
                Ext.ComponentQuery.query('#MapPanel')[0],
                me.layerNode,
                me.data_from_DB['full_path'],
                sql
        )
    }

});