/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.component.ImageShift', {
    //extend: 'IMPACT.view.component.Window',  // ERROR if extending template
    extend: 'Ext.window.Window',
    requires: [
                'IMPACT.view.component.Field.Integer',
                'IMPACT.Utilities.Input'
              ],
    itemId: 'ImageShift',

    title: '',

    autoShow: true,
    constrain: true,
    closeAction: 'destroy',
    closable: false,
    layout:'fit',
    resizable: true,
    bodyStyle: 'background-color: white;',

    width: 140,
    height: 190,
    buttons: [
        {
            text: 'Close',
            handler: function(){

                this.up('.window').stopEditingMode();
                if(this.closeAction === 'hide'){
                    this.up('.window').hide();
                } else {
                    this.up('.window').close();
                }
            }
        }
    ],
    // autoScroll: true,

    treeNode: null,

    editingMode: {
        active: false,
        layer: null,
    },

    startEditingMode: function(){

        var me = this;

        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', true, false);
        me.treeNode.set('cls','red-bold-node');
        me.treeNode.set('checked', true);
        me.treeNode.active_editing = true;

        // Activate editing MODE
        //me.editingMode.active = true;
        me.editingMode.layer = me.treeNode.data.layer;

        // Set editing flag to selected node
        //layer.params.editing = true;
        me.editingMode.layer.setVisible(true);

        me.on('shift_layer_changed', function(result){
            me.__refresh(result);
        });
        // Disable workspace tools
//        workspace.startEditingMode();

    },

    stopEditingMode: function(){

        var me = this;
        //var layer = me.treeNode.data.layer;
//        var workspace = me.up('#MainWorkspace');
//        var map = workspace.down('#MapPanel').map;

        me.treeNode.set('cls','');
        me.treeNode.active_editing = false;
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', false, false);

        // Reset editing flag to selected node
        //layer.params.editing = false;

        // Deactivate editing MODE
        //me.editingMode.active = false;
        //me.editingMode.layer = null;

        // Disable workspace tools
//        workspace.stopEditingMode();

    },


    initComponent: function(){
        var me = this;
        var map = Ext.ComponentQuery.query('#MapPanel')[0].map;
        me.callParent();
        me.data_from_DB = me.treeNode.data.data_from_DB;

        //me.addEvents('shift_layer_changed');

        me.startEditingMode();

        me.title = 'Image Shift';// ('+me.data_from_DB['basename']+')';
        // add geom field in stor but not visible in GRID

        me.add(
            {
                xtype: 'panel',
                border: false,
                collapsible: false,
                margin: '5 1 1 30',
                items: [{
                        xtype: 'button',
                        border: false,
                        itemId: 'shiftLeft',
                        width: 20,
                        iconCls: 'fa fa-lg fa-red fa-arrow-left',
                        margin: '35 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('left');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'shiftUp',
                        width: 20,
                        iconCls: 'fa fa-lg fa-red fa-arrow-up',
                        margin: '10 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('up');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'shiftRight',
                        width: 20,
                        iconCls: 'fa fa-lg fa-red fa-arrow-right',
                        margin: '35 1 1 1',
                        listeners: {
                            click: function() {
                                me.__applyShift('right');
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        border: false,
                        itemId: 'shiftDown',
                        width: 20,
                        iconCls: 'fa fa-lg fa-red fa-arrow-down',
                        margin: '57 1 1 -43',
                        listeners: {
                            click: function() {
                                me.__applyShift('down');
                            }
                        }
                    },
                     {  xtype: 'impactFieldInteger',
                        itemId: 'shift_multiplier',
                        margin: '5 0 0 0',
                        fieldLabel: 'Multiplier (px)',
                        value: 1,
                        labelWidth: 60,
                        width: 97,
                        validator: function(value){
                            if(value === '0' ){
                                return 'Only positive value';
                            }
                                return true;
                        }
                     }
                ]
            }
        );





    },

    __applyShift: function(where){
        var me = this;
        var shift_multiplier = IMPACT.Utilities.Input.parseField(me.down('#shift_multiplier'));
        if (shift_multiplier == 0 ){
            return
        }

        var extent = JSON.parse(me.data_from_DB['extent']);
        var ps = parseFloat(me.data_from_DB['pixel_size']) * shift_multiplier;

        if(where == 'up'){
            extent['N'] = parseFloat(extent['N']) + ps
            extent['S'] = parseFloat(extent['S']) + ps
        }
        if(where == 'down'){
            extent['N'] = parseFloat(extent['N']) - ps
            extent['S'] = parseFloat(extent['S']) - ps
        }
        if(where == 'left'){
            extent['E'] = parseFloat(extent['E']) - ps
            extent['W'] = parseFloat(extent['W']) - ps
        }
        if(where == 'right'){
            extent['E'] = parseFloat(extent['E']) + ps
            extent['W'] = parseFloat(extent['W']) + ps
        }
        // keep a copy of mod extent to prevent using node info while refreshing
        // update: set after response
        //me.editingMode.layer.data_from_DB['extent'] = JSON.stringify(extent);
        me.disable();
        //operation, input, output, content
        IMPACT.Utilities.Requests.fileHandler(
                'shift',
                me.data_from_DB['full_path'],
                '',
                JSON.stringify(extent)
                //me.editingMode.layer
        )

    },

    __refresh: function(result){
        me = this;
        // update the editing layer info
        // treepanel layer is updated via store
        me.data_from_DB['extent'] = result
        //me.editingMode.layer.data_from_DB['extent'] = result
        me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
        me.editingMode.layer.getSource().refresh();
        me.enable();
    }

});