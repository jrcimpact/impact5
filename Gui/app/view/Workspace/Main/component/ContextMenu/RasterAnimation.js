/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.RasterAnimation', {
    extend: 'IMPACT.view.component.Window',

    requires: ['IMPACT.view.component.Field.Text',
               'IMPACT.view.component.Field.Slider',
               'IMPACT.view.Processing.Window.component.Info'
    ],

    itemId: 'RasterAnimation',

    title: "Raster animation",
    closeAction: 'destroy',
    width: 450,
    items: [],


    extension: '.tif',
    layerNode: null,
    nodes: [],
    pos:0,
    intervall: 2,
    intervalId: null,
    treePanel: null,
    map: null,
    running: null,

    buttons: [
        {
            text: 'Start',
            handler: function(){
                this.up('#RasterAnimation').start();
            }
        },
        {
            text: 'Pause',
            handler: function(){
                this.up('#RasterAnimation').pause();
            }
        }
    ],

    listeners: {
        'destroy': function () {
            // off all layers to prevent issues of cache or when moving data
            var me = this;
            for (var pos in me.nodes) {
                if (me.nodes[pos].data.checked){
                    me.nodes[pos].set('checked', false);
                    me.treePanel.fireEvent('checkchange',me.nodes[pos], false);
                }
            }

        }
    },

    initComponent: function() {


        var me = this;
        // leaf of Folder node for expanded Shapefile
        me.nodes = [];
        me.pos = 0;
        me.running = false;

        if(me.layerNode.hasChildNodes()){
            me.layerNode.eachChild(function(childNode){
                  if( childNode.data.data_from_DB && childNode.data.data_from_DB['extension'] == '.tif'){
                    me.nodes.push(childNode);
                  }
            });
        }


        me.items = [];
        me.items.push({
            xtype: 'form',
            border: false,
            items: [
                {
                    xtype: 'slider',
                    labelAlign: 'bottom',
                    itemId: 'rasterAnimationSlider',
                    value:  1,
                    minValue: 1,
                    maxValue: me.nodes.length,
                    increment: 1,
                    width: 400,
                    decimalPrecision: 0,
                    listeners: {
                        changecomplete: function(item) {
                            for (var pos in me.nodes) {
                                    if (me.nodes[pos].data.checked){
                                        me.nodes[pos].set('checked', false);
                                        me.treePanel.fireEvent('checkchange',me.nodes[pos], false);
                                    }
                            }
                            var node = me.nodes[item.getValue()-1];
                            me.down('#rasterAnimationImgNames').setValue(node.data.data_from_DB['basename']);
                            me.nodes[item.getValue()-1].set('checked', true);
                            me.treePanel.fireEvent('checkchange',node, true);
                            me.pos = item.getValue()-1;
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    itemId: 'rasterAnimationImgNames',
                    //fieldLabel: 'Image name',
                    labelAlign: 'left',
                    //labelStyle: 'margin-bottom: 3px; font-weight: bold;',
                    width: 400,
                    style: 'margin: 5 5 5 5px;',
                    value: me.nodes[me.pos].data.data_from_DB['basename'],
                    disabled: true
                },
                {
                    xtype: 'numberfield',
                    itemId: 'rasterAnimationTimer',
                    fieldLabel: 'Intervall (sec)',
                    labelWidth: 100,
                    width: 170,
                    value: me.intervall,
                    step: 0.5,
                    minValue: 0.5,
                    maxValue: 20,
                    listeners: {
                        change: function(field, value) {
                            me.intervall = value;
                            if (me.intervalId !== null){
                                clearInterval(me.intervalId);
                                me.start();
                            }
                        }
                    }
                },
                {
                    xtype: 'ProcessingInfo',
                    html: 'Visualization parameters (bands, stretch and colors) are taken from current layer settings'
                }
            ]
        });

        me.callParent(arguments);
    },





    start: function(){
        var me = this;

        if(me.running){
            return
        } else {
            me.running = true;
        }

        // sett layer to off
        for (var pos in me.nodes) {
                if (me.nodes[pos].data.checked){
                    me.nodes[pos].set('checked', false);
                    me.treePanel.fireEvent('checkchange',me.nodes[pos], false);
                }
        }


        me.nodes[me.pos].set('checked', true);
        me.treePanel.fireEvent('checkchange',me.nodes[me.pos], true);
        me.down('#rasterAnimationImgNames').setValue(me.nodes[me.pos].data.data_from_DB['basename']);
        me.down('#rasterAnimationSlider').setValue(me.pos+1);


        me.intervalId = setInterval(showLayer,me.intervall*1000);

        function showLayer(){
            if (me.nodes == null || me.treePanel == null ){
                return
            }

            var node = me.nodes[me.pos];
            node.set('checked', true);
            me.treePanel.fireEvent('checkchange',node, true);
            if (typeof(me.down('#rasterAnimationImgNames')) !== 'undefined'){
                me.down('#rasterAnimationImgNames').setValue(me.nodes[me.pos].data.data_from_DB['basename']);
            }
            if (typeof(me.down('#rasterAnimationSlider')) !== 'undefined'){
                me.down('#rasterAnimationSlider').setValue(me.pos+1);
            }
            var myTimer = setTimeout(function(){
                    node.set('checked', false);
                    // when Gui is destroyed while running, timeout might run another time
                    if (me.treePanel != null){
                        me.treePanel.fireEvent('checkchange', node, false);
                    }
            }, me.intervall*1000+500);

            if (me.pos + 1 < me.nodes.length){
                me.pos = me.pos + 1
            } else {me.pos = 0}

         }


//        if(me.layerNode.hasChildNodes()){
//            me.layerNode.eachChild(function(childNode){
//                  childNode.set('checked', true);
//                  me.treePanel.fireEvent('checkchange', childNode, true);
//                  me.showLayer(childNode);
////                var layer = IMPACT.Utilities.OpenLayers.getLayerByFullPath(me.map,childNode.data.data_from_DB['full_path']);
////                layer.setVisibility(true);
////                childNode.set('checked', true);
////                console.log(childNode.data.data_from_DB['full_path']);
////                me.treePanel.fireEvent('checkchange', childNode, true);
//            });
//        }

//        if(me.layerNode.hasChildNodes()){
//            me.layerNode.eachChild(function(childNode){
//                var layer = IMPACT.Utilities.OpenLayers.getLayerByFullPath(me.map,childNode.data.data_from_DB['full_path']);
//                layer.setVisibility(true);
//                childNode.set('checked', true);
//            });
//        }


        //me.layerNode.cascadeBy(function(n){
        //console.log(n);
//                if( n.isLeaf() && n.data.data_from_DB && n.data.data_from_DB['extension'] == me.extension){
//                        n.set('checked',false);
//                }
//            })
//
//        me.layerNode.cascadeBy(function(n){
//                if( n.isLeaf() && n.data.data_from_DB && n.data.data_from_DB['extension'] == me.extension){
//                        n.set('checked',true);
//                        setInterval(function () {console.log('Set ON');}, 5000);
//                }
//            })
//
//        me.layerNode.cascadeBy(function(n){
//                if( n.isLeaf() && n.data.data_from_DB && n.data.data_from_DB['extension'] == me.extension){
//                        n.set('checked',false);
//                        setInterval(function () {console.log('Set OFF');}, 5000);
//                }
           // })
    },




    pause: function(){
        var me = this;
        if(me.running){
            clearInterval(me.intervalId);
            me.nodes[me.pos].set('checked', true);
            me.treePanel.fireEvent('checkchange',me.nodes[me.pos], true);
            me.running = false;
        }

    }
});