/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.CopyWindow', {
    extend: 'IMPACT.view.component.Window',

    requires: ['IMPACT.view.component.Field.Text'],

    itemId: 'CopyWindow',

    title: "Copy file",
    closeAction: 'destroy',
    width: 465,
    items: [],

    path: null,
    src_filename: null,
    extension: null,

    buttons: [
        {
            text: 'Copy',
            handler: function(){
                this.up('#CopyWindow').rename();
            }
        }
    ],

    initComponent: function() {
        var me = this;
        // Change OUT dir to default data dir since remote drive (e.g. Climate Station) is read only
//        me.path = IMPACT.Utilities.Common.dirname(me.src_filename.replace(IMPACT.GLOBALS['data_paths']['unix_raw_data'], IMPACT.GLOBALS['data_paths']['data']));
        me.path = IMPACT.GLOBALS['data_paths']['data'];
        me.extension = "." + me.src_filename.split('.').pop();
        me.items = [];
        me.items.push({
            xtype: 'form',
            border: false,
            items: [
                {
                    xtype: 'impactFieldText',
                    itemId: 'new_filename',
                    name: 'new_filename',
                    fieldLabel: 'New file name',
                    labelAlign: 'top',
                    labelStyle: 'margin-bottom: 3px;',
                    width: 440,
                    value: IMPACT.Utilities.Common.basename(me.src_filename, me.extension)+"_copy",
                    allowBlank: false,
                    regex: /^[0-9a-z_A-Z-]+$/
                }
            ]
        });

        me.callParent(arguments);
    },

    rename: function(){
        var me = this;
        var form = me.down('form').getForm();
        if (form.isValid()) {
            var dest_filename = [me.path, me.down('#new_filename').getValue() + me.extension].join("/");
            IMPACT.Utilities.LayerSettings.save_settings_from_DB('copy', me.src_filename, dest_filename);
            IMPACT.Utilities.Requests.fileHandler('copy', me.src_filename, dest_filename);
            me.destroy();
        }
    }

});