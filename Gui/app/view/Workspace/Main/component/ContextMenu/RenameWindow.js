/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.RenameWindow', {
    extend: 'IMPACT.view.component.Window',

    requires: ['IMPACT.view.component.Field.Text'],

    itemId: 'RenameWindow',

    title: "Rename file",
    closeAction: 'destroy',
    width: 465,
    items: [],

    path: null,
    src_filename: null,
    extension: null,
    map: null,
    layernode: null,

    buttons: [
        {
            text: 'Rename',
            handler: function(){
                this.up('#RenameWindow').rename();
            }
        }
    ],

    listeners: {
        'destroy': function () {
        }
    },

    initComponent: function() {
        var me = this;
        me.path = IMPACT.Utilities.Common.dirname(me.src_filename);
        me.extension = "." + me.src_filename.split('.').pop();

        me.items = [];
        me.items.push({
            xtype: 'form',
            border: false,
            items: [
                {
                    xtype: 'impactFieldText',
                    itemId: 'new_filename',
                    name: 'new_filename',
                    fieldLabel: 'New file name',
                    labelAlign: 'top',
                    labelStyle: 'margin-bottom: 3px;',
                    width: 440,
                    value: IMPACT.Utilities.Common.basename(me.src_filename, me.extension),
                    allowBlank: false,
                    regex: /^[0-9a-z_A-Z-]+$/
                }
            ]
        });

        me.callParent(arguments);
    },

    rename: function(){
        var me = this;
        var form = me.down('form').getForm();
        if (form.isValid()) {
            var dest_filename = [me.path, me.down('#new_filename').getValue() + me.extension].join("/");

            //me.layerNode.set('cls','x-tree-disabled');
            if(me.layerNode.data.layer){
                me.map.removeLayer(me.layerNode.data.layer);
            }
            me.layerNode.cascadeBy(function (n) {
                if(n.data.layer && n.data.layer.get('type') === 'attributes'){
                    //n.set('cls','x-tree-disabled');
                    me.map.removeLayer(n.data.layer);
                }
            });

            me.layerNode.remove();

            IMPACT.Utilities.LayerSettings.save_settings_from_DB('move', me.src_filename, dest_filename);
            IMPACT.Utilities.Requests.fileHandler('rename', me.src_filename, dest_filename);
            me.destroy();
        }
    }

});