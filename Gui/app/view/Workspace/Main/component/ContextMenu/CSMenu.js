/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.CSMenu', {
    extend: 'Ext.menu.Menu',

    requires: [
        'IMPACT.view.component.Field.Combo',
        'IMPACT.view.component.Field.Slider',
        'IMPACT.view.component.Field.MultiSlider'
    ],

    itemId: 'CSContextMenu',
    width: 300,
//    closeAction: 'destroy',
//    destroyMenu: true,
    showSeparator: true,
    //hideOnClick: false,

    layerNode: null,
    layer: null,
    data_from_DB: null,
    settings_from_DB: null,
    layerType: null,
    workspace: null,

    climateStation: false,

    items: [],

    initComponent:function() {
        var me = this;
        me.layer = (me.layerNode.data.layer ? me.layerNode.data.layer : null );

        me.data_from_DB = me.layerNode.data.data_from_DB;
        me.settings_from_DB = me.layerNode.data.settings_from_DB;
        me.layerType = IMPACT.Utilities.Impact.getLayerType(me.layerNode.data.data_from_DB);
        // change to attribute if attribute vector node
        if(me.layer && me.layer.get('type') === 'attributes'){
            me.layerType = 'attributes';
        }
        me.menuItems();
        me.callParent(arguments);
    },

    menuItems: function(){
        var me = this;
        me.items = [];
        //  ##########   Layer Info   ##########
        me.items.push({
            text: "Show file info",
            iconCls: 'fa fa-16 fa-blue fa-info-circle',
            handler: function(){
                Ext.create('IMPACT.view.Workspace.Main.component.LayerInfo', {
                    data_from_DB: me.data_from_DB
                }).show();
            }
        });

        //  ##########   Zoom to layer extent   ##########
        me.items.push({
            text: "Zoom to Layer Extent",
            iconCls: 'x-fa fa-16 fa-black fa-search',
            handler: function () {
                var bbox = IMPACT.Utilities.Impact.parse_bbox(me.data_from_DB);
                me.workspace.down('#MapPanel').map.getView().fit([bbox.left,bbox.bottom,bbox.right,bbox.top]);
            }
        });

        if(me.data_from_DB['ready'] === 'true'){

            //  ##########   Visualisation options   ##########
            me.items.push({
                text: "Visualization options",
                itemId: 'visualisation_options_menu',
                iconCls: 'fa fa-16 fa-blue fa-eye',
                menu: me.visualizationOptions(),
                disabled: !me.layerNode.data.checked,
                hideOnClick: false,
                showSeparator: true
            });


            if(me.layerType === 'raster' || me.layerType === 'class'){

                var bandsList = [];
                for (var b = 1; b <= parseInt(me.data_from_DB['num_bands']); b++){
                    bandsList.push({
                        text: 'Band '+String(b),
                        value: b,
                        handler: function (b){
                            IMPACT.Utilities.Requests.show_raster_stats(
                            me.workspace.down('#MapPanel'),
                            me.data_from_DB['full_path'],
                            b.value)
                        }
                    });
                };


                me.items.push({
                    text: "Show Histogram and Statistics",
                    value: 'xxx ',
                    iconCls: 'fa-black far fa-chart-bar',
                    menu: bandsList,
                    hideOnClick: false
                });


            }

            // --------  EXCEPTION on ATTRIBUTES LAYER --------------
            // --- attributes contains layer since loaded by user ---
             //------------------------------------------------------
            if(me.layerType === 'attributes'){
                //  ##########   Show class statistics   ##########
                me.items.push({
                    text: "Show class statistics",
                    iconCls: 'fa fa-lg fa-black fa-pie-chart',
                    handler: function (){
                        IMPACT.Utilities.Requests.show_shp_stats(
                            me.workspace.down('#MapPanel'),
                            me.data_from_DB['full_path'],
                            me.layerNode.data.text,
                            me.settings_from_DB['legendType']);
                    }
                });
                //  ##########   Convert to Raster   ##########
                me.items.push({
                    text: "Convert to Binary Mask",
                    icon: './resources/icons/vector2raster.png',
                    handler: function (){
                        var units = 'meters';
                        if(me.data_from_DB['PROJ4'].indexOf("+units=dd")>=0
                                || me.data_from_DB['PROJ4'].indexOf("+proj=longlat")>=0
                                || me.data_from_DB['EPSG'] === 'EPSG:4326') {
                            units='decimal degree';
                        }
                        if(typeof Ext.ComponentQuery.query('#ProcessingWindowClassExport')[0] !== "undefined"){
                            Ext.ComponentQuery.query('#ProcessingWindowClassExport')[0].destroy();
                        }
                        Ext.create('IMPACT.view.Processing.Window.ClassExport', {
                            legendType: me.settings_from_DB['legendType'],
                            full_path: me.data_from_DB['full_path'],
                            layername: me.layer.getSource().get(classItem), // attributes contains layer since loaded by user
                            units: units
                        }).show();
                    }
                });
            }   // and attributes

            if(me.layerType === 'vector' || me.layerType === 'attributes'){
                //  ##########   Vector Editing   ##########
                //me.items = me.vectorEditingOptions(me.items);
            }

            //  ##########   Apply Raster Styles   ##########
            if(me.layerType === 'raster' || me.layerType === 'class'){

                if(me.data_from_DB['num_bands'] === 1){

                    var palettesFromStore = Ext.data.StoreManager.lookup('RasterStyles').data.items;
                    var palette = [];

                    // Add PCT from file getting Metadata info
                    var pct_info = (JSON.parse(me.data_from_DB['metadata']).Impact_product_type !== 'undefined') ? JSON.parse(me.data_from_DB['metadata']).Impact_product_type : 'from file' ;
                    if (me.data_from_DB['has_colorTable'] === 1){
                        palette.push({
                            text: 'Pct: ' + pct_info,
                            value: '',
                            iconCls: ('' === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        },
                        "-"
                        );
                    }
                    // load raster style from settings
                    for (var i in palettesFromStore){
                        palette.push({
                            text: palettesFromStore[i].data.name,
                            value: palettesFromStore[i].data.type,
                            iconCls: (palettesFromStore[i].data.type === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        });
                    }
                    // add option to disable pct either from settings of pct or define a new one
                    palette.push({
                            text: "None",
                            value: "none",
                            iconCls: ("none" === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        },
                        '-',
                        {
                            text: "Define new style",
                            value: "new",
                            iconCls: 'fa fa-lg fa-green fa-plus',
                            handler: function (){
                                        Ext.ComponentQuery.query('#viewportTab')[0].setActiveTab(2);
                                        Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].show();
                                        Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].down('#addNew').fireHandler();
//                                if(Ext.ComponentQuery.query('#UserSettings').length === 0){
//                                    Ext.create('IMPACT.view.UserSettings.UserSettings');
//                                    Ext.ComponentQuery.query('#UserSettingsTabpanelId')[0].setActiveTab(2);
//                                    Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].down('#addNew').fireHandler();
//                                }
//                                }
                            }
                        },
                        {
                            text: "Seve select style to file (pct)",
                            value: "Save",
                            // set disabled till new test on nonFloat images : pct not possible
                            disabled : false, //(me.layer.params.legendType === 'none' || me.layer.params.legendType === '') ? true : false ,
                            iconCls: 'fa fa-16 fa-blue fa-save',
                            handler: function (){
                                // Request : Save pct to file
                                Ext.MessageBox.show({
                                    msg: 'Save selected style as default palette color table ?',
                                    buttons: Ext.MessageBox.YESNO,
                                    buttonText:{
                                        yes: "Yes",
                                        no: "No"
                                    },
                                    fn: function(btn){
                                        if (btn === "yes"){
                                            var params = {
                                                toolID:  "rasterPCTmanager",
                                                infile: me.data_from_DB['full_path'],
                                                pct: me.settings_from_DB['legendType']
                                            };
                                            IMPACT.Utilities.Requests.launchProcessing(params);
                                        }
                                    }
                                });
                            }
                        }

                    );

                    // add option to remove PCT if exists
                    if (me.data_from_DB['has_colorTable'] === 1){
                        palette.push({
                            text: "Remove pct from file",
                            value: "Remove",
                            iconCls: 'fa fa-16 fa-red fa-times',
                            handler: function (){
                                // Request : Remove pct from file
                                Ext.MessageBox.show({
                                        msg: 'Remove internal palette color table from file ?',
                                        buttons: Ext.MessageBox.YESNO,
                                        buttonText:{
                                            yes: "Yes",
                                            no: "No"
                                        },
                                        fn: function(btn){
                                            if (btn === "yes"){
                                                var params = {
                                                    toolID:  "rasterPCTmanager",
                                                    infile: me.data_from_DB['full_path'],
                                                    pct: ''
                                                };
                                                IMPACT.Utilities.Requests.launchProcessing(params);
                                            }
                                        }
                                    });
                            } // handler
                        });
                    }

                    me.items.push({
                        text: "Apply Raster Styles to layer",
                        iconCls: 'fa fa-16 fa-violet fa-th',
                        menu: palette
                    });
                }
            }

            //  ##########   File options   ##########
            if(me.layerType === 'raster' || me.layerType === 'class' || me.layerType === 'vector'){
                me.items.push({
                    text: "Copy to local drive",
                    itemId: 'file_options_menu',
                    iconCls: 'fa fa-16 fa-black fas fa-save',
                    menu: me.fileOptions(),
                    disabled: me.layerNode.active_editing,
                    hideOnClick: false
                });
            }

        }

    },

    visualizationOptions: function(){
        var me = this;

        var items = [];

        var statistics = JSON.parse(me.data_from_DB['statistics']);

        items.push({
            text: "Opacity [0-1]",
            iconCls: 'fa fa-16 fa-black fas fa-adjust',
            disabled: true,
            cls: 'full_opacity'
        });
        items.push(
            {
                xtype: 'impactFieldSlider',
                labelAlign: 'top',
                value: me.settings_from_DB['opacity'],
                minValue: 0,
                maxValue: 1,
                increment: 0.1,
                decimalPrecision: 1,
                disable_textField: true,
                listeners: {
                    changecomplete: function(value) {
                        IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'opacity', value, false)
                    }
                }
            }
        );

        if(me.layerType==='vector'){

            items.push({
                text: "Outline size [pixel]",
                iconCls: 'fa fa-16 fa-black fas fa-minus',
                disabled: true,
                cls: 'full_opacity'
            });
            items.push(
                {
                    xtype: 'impactFieldSlider',
                    labelAlign: 'top',
                    value:  me.settings_from_DB['outline_size'],
                    minValue: 0.1,
                    maxValue: 5,
                    increment: 0.1,
                    decimalPrecision: 1,
                    listeners: {
                        changecomplete: function(value) {

                            IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'outline_size', value, true)
                        }
                    }
                }
            );

            items.push({
                text: "Outline color",
                iconCls: 'fa fa-16 fa-black fas fa-paint-brush',
                disabled: true,
                cls: 'full_opacity'
            });
            items.push(
                {
                    xtype: 'colorpicker',
                    allowReselect: true,
                    value: me.settings_from_DB['color'],
                    listeners: {
                        select: function(item, color){
                            var rgbColor = IMPACT.Utilities.Common.hexToRgb(color).join(',');

                            IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'color', color, true)
                        }
                    }
                }
            );

            items.push({
                text: ( me.settings_from_DB['simplificationRequired'] === 'true' ? 'Disable Features Simplification' : 'Enable Features Simplification'),
                iconCls: 'fa fa-lg fa-orange far fa-check-square',
                handler: function () {
                    var value = me.settings_from_DB.simplificationRequired === 'true' ? 'false' : 'true';
                    IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'simplificationRequired', value, true)
                 }
            });

        } else if(me.layerType === 'raster'){

            var current_bands = me.settings_from_DB['bands'].split(",");

            // add fake band G B to build the invisible bands and sliders ... faster the build and check each time
            // settings scale2 anc scale3 are set by dafault in the LayerSetting functions
            if(current_bands.length == 1){
                current_bands.push(current_bands[0]);
                current_bands.push(current_bands[0]);
            }


            var hide_GB_band = me.data_from_DB['num_bands'] < 2 || me.settings_from_DB['rgb'] == false ? true : false;
            var R_field_label = hide_GB_band ? '' : 'R';

            items.push({
                text: "Visible bands",
                disabled: true,
                iconCls: 'fa fa-16 fa-black fas fa-sliders-h'
            });

            // Gray or RGB option
            items.push({
                    xtype: 'radiogroup',
                    width: 100,
                    style: 'margin-left: 60px;',
                    labelStyle: 'font-weight: bold;',
                    columns: 2,
                    itemId: 'visualization_mode',
                    items: [
                        {
                            xtype: 'radiofield',
                            boxLabel: 'RGB',
                            labelWidth: 50,
                            name: 'vis_mode',
                            inputValue: 'RGB',
                            checked: me.settings_from_DB['rgb'],
                            disabled: me.data_from_DB['num_bands'] < 2
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Gray',
                            name: 'vis_mode',
                            checked: !me.settings_from_DB['rgb'],
                            inputValue: 'Gray'

                        }
                    ],
                    listeners: {
                        change: function (item,newValue,oldValue) {
                            if (newValue['vis_mode'] === 'RGB') {
                                me.settings_from_DB['rgb'] = true;

                                me.down('#band_r_combo').setFieldLabel('R');
                                me.down('#band_r_combo').setVisible(true);
                                me.down('#band_g_combo').setVisible(true);
                                me.down('#band_b_combo').setVisible(true);

                                me.down('#minMax_r_slider').down('#slider').setFieldLabel('R');
                                me.down('#minMax_r_slider').setVisible(true);
                                me.down('#minMax_g_slider').setVisible(true);
                                me.down('#minMax_b_slider').setVisible(true);

                                //me.down('#slider_lock').setVisible(true);

                            } else{
                                me.settings_from_DB['rgb'] = false;

                                me.down('#band_r_combo').setFieldLabel('');
                                me.down('#band_g_combo').setVisible(false);
                                me.down('#band_b_combo').setVisible(false);

                                me.down('#minMax_r_slider').down('#slider').setFieldLabel('');
                                me.down('#minMax_g_slider').setVisible(false);
                                me.down('#minMax_b_slider').setVisible(false);

                                //me.down('#slider_lock').setVisible(false);

                            }
                        me.__updateStretch();
                        }
                    }
            });
            items.push({
                xtype: 'panel',
                itemId: 'bands_container',
                border: false,
                bodyStyle: 'background-color: rgba(0, 0, 0, 0);',
                layout: 'column',
                items: [
                    me.__band_combo(R_field_label, current_bands[0], false),
                    me.__band_combo('G', current_bands[1], hide_GB_band),
                    me.__band_combo('B', current_bands[2], hide_GB_band)
                ]
            });
            items.push({
                text: "Stretch type",
                iconCls: 'fa fas fa-palette'
            });

            items.push({
                xtype: 'impactFieldCombo',
                itemId: 'stretch_combo',
                width: 80,
                store: new Ext.data.SimpleStore({
                    fields: ['id','name'],
                    data : [
                        ['0','0-255'],
                        ['1','Min-Max'],
                        ['2','.5x StDev'],
                        ['3','1x StDev'],
                        ['4','2x StDev'],
                        ['5','3x StDev']
                    ]
                }),
                displayField: 'name',
                valueField: 'id',
                value: String(me.settings_from_DB['stretch']),
                listeners: {
                    change: function(combo, newValue){
                        me.__updateStretch();
                    }
                }
            });

            items.push({
                xtype: 'panel',
                itemId: 'bands_min_max_slider',
                border: false,
                bodyStyle: 'background-color: rgba(0, 0, 0, 0);',
                layout: 'column',
                width: 200,
                //hidden: me.settings_from_DB['stretch']!='1',  // onload visibility
                items:[
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_r_slider',
                        respect_boundaries: false,
                        fieldLabel: R_field_label,
                        labelWidth: 10,
                        values: me.settings_from_DB['scale1'].split(','),
                        minValue: parseFloat(statistics[current_bands[0]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[0]-1][1]),
                        increment: me.__setSliderIncrement(),
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            },
                        }
                    },
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_g_slider',
                        respect_boundaries: false,
                        fieldLabel: 'G',
                        labelWidth: 10,
                        values: me.settings_from_DB['scale2'].split(','),
                        minValue: parseFloat(statistics[current_bands[1]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[1]-1][1]),
                        increment: me.__setSliderIncrement(),
                        hidden: hide_GB_band,
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            },
                        }
                    },
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_b_slider',
                        respect_boundaries: false,
                        fieldLabel: 'B',
                        labelWidth: 10,
                        values: me.settings_from_DB['scale3'].split(','),
                        minValue: parseFloat(statistics[current_bands[2]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[2]-1][1]),
                        increment: me.__setSliderIncrement(),
                        hidden: hide_GB_band,
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            }
                        }
                    },
                ]
            });

        } else if(me.layerType === 'class'){

            items.push({
                text: "Class range",
                iconCls: 'fa fa-16 fa-black fa-sliders'
            });

            items.push({
                xtype: 'multislider',
                itemId: 'class_range_slider',
                labelAlign: 'top',
                width: 200,
                values: [me.settings_from_DB['apply_data_range'][1], me.settings_from_DB['apply_data_range'][2]],
                minValue: statistics[0][0],
                maxValue: statistics[0][1],
                increment: 1,
                listeners: {
                    changecomplete: function(value) {
                        IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'apply_data_range', [true, value.getValues()[0], value.getValues()[1]], true)
                    }
                }
            });


            if (me.data_from_DB['has_colorTable'] === 1
                    && me.data_from_DB['num_bands'] === 1){
                items.push({
                    text: "Save range as mask",
                    iconCls: 'fa fa-16 fa-blue fa-save',
                    handler: function(){


                        var class_range = me.layerNode.data.settings_from_DB['apply_data_range'];

                        Ext.MessageBox.show({
                            msg: 'Save '+me.layerNode.data.text +" using value from "+ class_range[1]+ ' to '+ class_range[2]+' ?',
                            buttons: Ext.MessageBox.YESNO,
                            buttonText:{
                                yes: "Yes",
                                no: "No"
                            },
                            fn: function(btn){
                                if (btn === "yes"){
                                    var params = {
                                        toolID:  "rasterMask",
                                        infile: me.data_from_DB['full_path'],
                                        minrange: class_range[1],
                                        maxrange: class_range[2]
                                    };
                                    IMPACT.Utilities.Requests.launchProcessing(params);
                                }
                            }
                        });
                    }
                });
            }
        }

        return items;
    },

    fileOptions: function(){
        var me = this;
        var items = [];



        items.push({
            text: "Copy file to " + IMPACT.GLOBALS['data_paths']['data'],
            iconCls: 'fa fa-16 fa-black far fa-copy',
            handler: function(){
                if(typeof Ext.ComponentQuery.query('#CopyWindow')[0] !== "undefined"){
                    Ext.ComponentQuery.query('#CopyWindow')[0].destroy();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.CopyWindow', {
                    src_filename: me.data_from_DB['full_path']
                }).show();
            }
        });

        return items;
    },

    __band_combo: function(band, selected_band, hidden){
        var me = this;
        var bands = [];
        for (var i=0; i < me.data_from_DB['num_bands']; i++) {
            bands.push(String(i+1));
        }
        bandID = band == '' ? 'R' : band;


        return {
            xtype: 'impactFieldCombo',
            itemId: 'band_'+bandID.toLowerCase()+'_combo',     // band_r_combo band_g_combo band_b_combo
            fieldLabel: band,
            labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
            labelWidth: 10,
            width: 60,
            style: 'margin-right: 10px;',
            store: bands,
            value: selected_band,
            hidden: hidden,
            listeners: {
                select: function(){
                    me.__updateStretch();
                }
            }
        }

    },

    __updateStretch: function(MinMaxFromSlider){
        var me = this;
        var statistics = JSON.parse(me.data_from_DB['statistics']);
        var newSettings = {};
        newSettings['stretch'] = me.down('#stretch_combo').getValue();
        var r_Band = parseInt(me.down('#band_r_combo').getValue());
        var g_Band = parseInt(me.down('#band_g_combo').getValue());
        var b_Band = parseInt(me.down('#band_b_combo').getValue());

        if(me.down('#band_g_combo').isVisible()){
            newSettings['bands'] = String(r_Band)+','+String(g_Band)+','+String(b_Band);
        }
        else{
            newSettings['bands'] = String(r_Band);
        }

        var r_MinMax = me.down('#minMax_r_slider');
        var g_MinMax = me.down('#minMax_g_slider');
        var b_MinMax = me.down('#minMax_b_slider');

        // Update slider minValue & maxValue according to selected bands
        r_MinMax.updateMinMax(statistics[r_Band-1]);
        g_MinMax.updateMinMax(statistics[g_Band-1]);
        b_MinMax.updateMinMax(statistics[b_Band-1]);


        if(!MinMaxFromSlider){
            newSettings['scale1'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(r_Band, newSettings['stretch'], me.data_from_DB, r_MinMax.values);
            newSettings['scale2'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(g_Band, newSettings['stretch'], me.data_from_DB, g_MinMax.values);
            newSettings['scale3'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(b_Band, newSettings['stretch'], me.data_from_DB, b_MinMax.values);

            // move sliders
            var minMax = [newSettings['scale1'].split(',')[0],newSettings['scale1'].split(',')[1]];
            r_MinMax.__setSlider(minMax[0],minMax[1]);
            r_MinMax.__setTextField(null,minMax[0],{index:0});
            r_MinMax.__setTextField(null,minMax[1],{index:1});

            minMax = [newSettings['scale2'].split(',')[0],newSettings['scale2'].split(',')[1]];
            g_MinMax.__setSlider(minMax[0],minMax[1]);
            g_MinMax.__setTextField(null,minMax[0],{index:0});
            g_MinMax.__setTextField(null,minMax[1],{index:1});

            minMax = [newSettings['scale3'].split(',')[0],newSettings['scale3'].split(',')[1]];
            b_MinMax.__setSlider(minMax[0],minMax[1]);
            b_MinMax.__setTextField(null,minMax[0],{index:0});
            b_MinMax.__setTextField(null,minMax[1],{index:1});

        }else{
            newSettings['scale1'] = String(r_MinMax.values[0])+','+String(r_MinMax.values[1])
            newSettings['scale2'] = String(g_MinMax.values[0])+','+String(g_MinMax.values[1])
            newSettings['scale3'] = String(b_MinMax.values[0])+','+String(b_MinMax.values[1])
        }

        // save settings
        // STRETCH AND SLIDER are interlinked, save and redraw manualy to not propagate multiple save
        for (key in me.settings_from_DB) {
            if(newSettings[key] && newSettings[key] !== me.settings_from_DB[key]){
                me.settings_from_DB[key] = newSettings[key];
                me.layerNode.data.layer.getSource().updateParams({[key] : newSettings[key]});
            }
        };

        IMPACT.Utilities.LayerSettings.save_settings_from_DB('save', me.data_from_DB['full_path'], '', me.settings_from_DB);
        me.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
        me.layer.getSource().refresh();

    },

    __applyColorPalette: function(type, colorPalette){
        var me = this;
        if(type === 'raster'){
             IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'legendType', colorPalette, true)
        }
    },


    __setSliderIncrement: function(){
        var me = this;
        var data_type = me.data_from_DB['data_type'];
        var increment = 1/255;
        if(data_type === 'Byte' || data_type === 'UInt16' || data_type === 'UInt32'){
            increment = 1;
        }
        return increment;
    },


});