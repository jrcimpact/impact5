/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.Menu', {
    extend: 'Ext.menu.Menu',

    requires: [
        'IMPACT.view.component.Field.Combo',
        'IMPACT.view.component.Field.Slider',
        'IMPACT.view.component.Field.MultiSlider'
    ],

    itemId: 'ContextMenu',
    width: 300,

    closeAction: 'destroy',
    autoDestroy: true,
    destroyMenu: true,
    //hideOnClick: true,

    showSeparator: true,


    // input params
    layerNode: null,
    workspace: null,

    layer: null,
    data_from_DB: null,
    settings_from_DB: null,
    layerType: null,
    // add Destroy method since Menu is still there and create conflicts with layerSettings
    // need 0.5 sec before destroying to start spawn the command  
    listeners: {
        hide:function(menu, opt){
            // Destroy items with event listener
            // Following Items are alive after destroying the Menu
            // Issue in changing band combination from Gray ro RGB : change is reflected on previous layer
            Ext.destroy(Ext.ComponentQuery.query('#visualization_mode')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#bands_container')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#stretch_combo')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#bands_min_max_slider')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#minMax_r_slider')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#minMax_g_slider')[0]);
            Ext.destroy(Ext.ComponentQuery.query('#minMax_b_slider')[0]);

            setTimeout(function (menu) {
                Ext.destroy(menu);
            }, 500);

         }
    },
    items: [],

    initComponent:function() {
        var me = this;
        me.layer = (me.layerNode.data.layer ? me.layerNode.data.layer : null );

        me.data_from_DB = me.layerNode.data.data_from_DB;
        me.settings_from_DB = me.layerNode.data.settings_from_DB;

        me.layerType = IMPACT.Utilities.Impact.getLayerType(me.layerNode.data.data_from_DB);
        // change to attribute if attribute vector node
        if(me.layer && me.layer.get('type') === 'attributes'){
            me.layerType = 'attributes';
        }
        me.menuItems();
        me.callParent(arguments);
    },

    menuItems: function(){
        var me = this;
        me.items = [];
        //  ##########   Layer Info   ##########
        me.items.push({
            text: "Show file info",
            iconCls: 'fa fa-16 fa-blue fa-info-circle',
            handler: function(){
                Ext.create('IMPACT.view.Workspace.Main.component.LayerInfo', {
                    data_from_DB: me.data_from_DB
                }).show();
            }
        });

        //  ##########   Zoom to layer extent   ##########
        me.items.push({
            text: "Zoom to Layer Extent",
            iconCls: 'x-fa fa-16 fa-black fa-search',
            handler: function () {
                var bbox = IMPACT.Utilities.Impact.parse_bbox(me.data_from_DB);
                me.workspace.down('#MapPanel').map.getView().fit([bbox.left,bbox.bottom,bbox.right,bbox.top]);
            }
        });

        if(me.data_from_DB['ready'] === 'true'){

            //  ##########   Visualisation options   ##########
            me.items.push({
                text: "Visualization options",
                itemId: 'visualisation_options_menu',
                iconCls: 'fa fa-16 fa-blue fa-eye',
                menu: me.visualizationOptions(),
                disabled: !me.layerNode.data.checked,
                hideOnClick: false,
                showSeparator: true
            });


            if(me.layerType === 'raster' || me.layerType === 'virtual' || me.layerType === 'class'){
                //  ##########   Image Shift      ##########
                //  ##########   Convert to Raster   ##########
                me.items.push({
                    text: "Adjust Geolocation",
                    iconCls: 'fa-16 fas fa-arrows-alt',
                    disabled: !me.layerNode.data.checked || me.layerNode.active_editing,
                    handler: function (){
                        if(typeof Ext.ComponentQuery.query('#ImageShift')[0] != "undefined"){
                            Ext.ComponentQuery.query('#ImageShift')[0].destroy();
                        }
                        Ext.create('IMPACT.view.Workspace.Main.component.ImageShift', {
                            treeNode: me.layerNode
                        });
                        alert("Shift is immediately applied; be sure to have a backup of the original file.")
                    }
                });

                //  ##########   Build pyramids   ##########
                me.items.push({
                    text: "Build pyramids",
                    iconCls: 'fa fa-lg fa-green fa-signal',
                    handler: function (){
                        var params = {
                            toolID:  "buildPyramids",
                            infile : me.data_from_DB['full_path']
                        };
                        IMPACT.Utilities.Requests.launchProcessing(params);
                    }
                });

                //  ##########   Pie Chart   ##########
                var bandsList = [];
                for (var b = 1; b <= parseInt(me.data_from_DB['num_bands']); b++){
                    bandsList.push({
                        text: 'Band '+String(b),
                        value: b,
                        handler: function (b){
                            IMPACT.Utilities.Requests.show_raster_stats(
                            me.workspace.down('#MapPanel'),
                            me.data_from_DB['full_path'],
                            b.value)
                        }
                    });
                }
                me.items.push({
                    text: "Show Histogram and Statistics",
                    value: 'xxx ',
                    iconCls: 'fa-black far fa-chart-bar',
                    menu: bandsList,
                    hideOnClick: false
                });

                //  ##########   Raster Recode  ##########
                me.items = me.rasterEditingOptions(me.items);
            }


            // --------  EXCEPTION on ATTRIBUTES LAYER --------------
            // --- attributes contains layer since loaded by user ---
             //------------------------------------------------------
            if(me.layerType === 'attributes'){
                //  ##########   Show class statistics   ##########
                me.items.push({
                    text: "Show class statistics",
                    iconCls: 'fa fa-lg fa-black fa-pie-chart',
                    handler: function (){
                        IMPACT.Utilities.Requests.show_shp_stats(
                            me.workspace.down('#MapPanel'),
                            me.data_from_DB['full_path'],
                            me.layerNode.data.text,
                            me.settings_from_DB['legendType']);
                    }
                });
                //  ##########   Convert to Raster   ##########
                me.items.push({
                    text: "Convert to Binary Mask",
                    icon: './resources/icons/vector2raster.png',
                    handler: function (){
                        var units = 'meters';
                        if(me.data_from_DB['PROJ4'].indexOf("+units=dd")>=0
                                || me.data_from_DB['PROJ4'].indexOf("+proj=longlat")>=0
                                || me.data_from_DB['EPSG'] === 'EPSG:4326') {
                            units='decimal degree';
                        }
                        if(typeof Ext.ComponentQuery.query('#ProcessingWindowClassExport')[0] !== "undefined"){
                            Ext.ComponentQuery.query('#ProcessingWindowClassExport')[0].destroy();
                        }
                        Ext.create('IMPACT.view.Processing.Window.ClassExport', {
                            legendType: me.settings_from_DB['legendType'],
                            full_path: me.data_from_DB['full_path'],
                            layername: me.layer.getSource().get(classItem), // attributes contains layer since loaded by user
                            units: units
                        }).show();
                    }
                });

                me.items.push({
                            text: "Google Earth KML (WMS) overlay",
                            iconCls: 'fas fa-blue fa-globe-africa',
                            disabled: (me.layer === null ? true : false),
                            tooltip: (me.layer === null ? 'Visualize layer first' : ''),

                            handler: function () {
                                if(typeof Ext.ComponentQuery.query('#GoogleEarthKmlWindow')[0] !== "undefined"){
                                    Ext.ComponentQuery.query('#GoogleEarthKmlWindow')[0].destroy();
                                }
                                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.GoogleEarthKmlWindow', {
                                     classItem: me.layer.getSource().get(classItem),
                                     layerNode: me.layerNode


                                }).show();
                            },

                        });

            }   // and attributes

            if(me.layerType === 'vector' || me.layerType === 'geojson' || me.layerType === 'attributes'){
                //  ##########   Vector Editing   ##########
                me.items = me.vectorEditingOptions(me.items);
            }


            //  ##########   Apply Raster Styles   ##########
            if(me.layerType === 'raster' || me.layerType === 'class'){

                if(me.data_from_DB['num_bands'] === 1){

                    var palettesFromStore = Ext.data.StoreManager.lookup('RasterStyles').data.items;
                    var palette = [];

                    // Add PCT from file getting Metadata info
                    var pct_info = (JSON.parse(me.data_from_DB['metadata']).Impact_product_type !== 'undefined') ? JSON.parse(me.data_from_DB['metadata']).Impact_product_type : 'from file' ;
                    if (me.data_from_DB['has_colorTable'] === 1){
                        palette.push({
                            text: 'Pct: ' + pct_info,
                            value: '',
                            iconCls: ('' === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        },
                        "-"
                        );
                    }
                    // load raster style from settings
                    for (var i in palettesFromStore){
                        palette.push({
                            text: palettesFromStore[i].data.name,
                            value: palettesFromStore[i].data.type,
                            iconCls: (palettesFromStore[i].data.type === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        });
                    }
                    // add option to disable pct either from settings of pct or define a new one
                    palette.push({
                            text: "None",
                            value: "none",
                            iconCls: ("none" === me.settings_from_DB['legendType']) ? 'fa fa-check" aria-hidden="true"' : "",
                            handler: function (item){
                               me.__applyColorPalette('raster', item.value);
                            }
                        },
                        '-',
                        {
                            text: "Define new style",
                            value: "new",
                            iconCls: 'fa fa-lg fa-green fa-plus',
                            handler: function (){
                                        Ext.ComponentQuery.query('#viewportTab')[0].setActiveTab(2);
                                        Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].show();
                                        Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].down('#addNew').fireHandler();
//                                if(Ext.ComponentQuery.query('#UserSettings').length === 0){
//                                    Ext.create('IMPACT.view.UserSettings.UserSettings');
//                                    Ext.ComponentQuery.query('#UserSettingsTabpanelId')[0].setActiveTab(2);
//                                    Ext.ComponentQuery.query('#UserSettingsTabLegendRaster')[0].down('#addNew').fireHandler();
//                                }
//                                }
                            }
                        },
                        {
                            text: "Seve select style to file (pct)",
                            value: "Save",
                            // set disabled till new test on nonFloat images : pct not possible
                            disabled : false, //(me.layer.params.legendType === 'none' || me.layer.params.legendType === '') ? true : false ,
                            iconCls: 'fa fa-16 fa-blue fa-save',
                            handler: function (){
                                // Request : Save pct to file
                                Ext.MessageBox.show({
                                    msg: 'Save selected style as default palette color table ?',
                                    buttons: Ext.MessageBox.YESNO,
                                    buttonText:{
                                        yes: "Yes",
                                        no: "No"
                                    },
                                    fn: function(btn){
                                        if (btn === "yes"){
                                            var params = {
                                                toolID:  "rasterPCTmanager",
                                                infile: me.data_from_DB['full_path'],
                                                pct: me.settings_from_DB['legendType']
                                            };
                                            IMPACT.Utilities.Requests.launchProcessing(params);
                                        }
                                    }
                                });
                            }
                        }

                    );

                    // add option to remove PCT if exists
                    if (me.data_from_DB['has_colorTable'] === 1){
                        palette.push({
                            text: "Remove pct from file",
                            value: "Remove",
                            iconCls: 'fa fa-16 fa-red fa-times',
                            handler: function (){
                                // Request : Remove pct from file
                                Ext.MessageBox.show({
                                        msg: 'Remove internal palette color table from file ?',
                                        buttons: Ext.MessageBox.YESNO,
                                        buttonText:{
                                            yes: "Yes",
                                            no: "No"
                                        },
                                        fn: function(btn){
                                            if (btn === "yes"){
                                                var params = {
                                                    toolID:  "rasterPCTmanager",
                                                    infile: me.data_from_DB['full_path'],
                                                    pct: ''
                                                };
                                                IMPACT.Utilities.Requests.launchProcessing(params);
                                            }
                                        }
                                    });
                            } // handler
                        });
                    }

                    me.items.push({
                        text: "Apply Raster Styles to layer",
                        iconCls: 'fa fa-16 fa-violet fa-th',
                        menu: palette
                    });
                }
            }

            //  ##########   File options   ##########
            if(me.layerType === 'raster' || me.layerType === 'virtual' || me.layerType === 'class' || me.layerType === 'vector' || me.layerType === 'kml' || me.layerType === 'geojson' ){
                me.items.push({
                    text: "Export To",
                    itemId: 'file_export_menu',
                    iconCls: 'fa fa-16 fas fa-file-export',
                    menu: me.layerExportOptions(),
                    hideOnClick: false
                });

                me.items.push({
                    text: "File options",
                    itemId: 'file_options_menu',
                    iconCls: 'fa fa-16 fa-black fas fa-save',
                    menu: me.fileOptions(),
                    disabled: me.layerNode.active_editing,
                    hideOnClick: false
                });

                me.items.push({
                    text: "Download",
                        iconCls: 'fa fa-16 fa-blue fa-download',
                        handler: function(){
                            IMPACT.Utilities.Requests.fileHandler('download', me.data_from_DB['full_path']);
                        }
                });
            }

        }

    },

    visualizationOptions: function(){
        var me = this;

        var items = [];

        var statistics = JSON.parse(me.data_from_DB['statistics']);

        items.push({
            text: "Opacity [0-1]",
            iconCls: 'fa fa-16 fa-black fas fa-adjust',
            disabled: true,
            cls: 'full_opacity'
        });
        items.push(
            {
                xtype: 'impactFieldSlider',
                labelAlign: 'top',
                value: me.settings_from_DB['opacity'],
                minValue: 0,
                maxValue: 1,
                increment: 0.1,
                decimalPrecision: 1,
                disable_textField: true,
                listeners: {
                    changecomplete: function(value) {
                        IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'opacity', value, false)
                    }
                }
            }
        );

        if(me.layerType==='vector'|| me.layerType === 'kml' || me.layerType === 'geojson'){

            items.push({
                text: "Outline size [pixel]",
                iconCls: 'fa fa-16 fa-black fas fa-minus',
                disabled: true,
                cls: 'full_opacity'
            });
            items.push(
                {
                    xtype: 'impactFieldSlider',
                    labelAlign: 'top',
                    value:  me.settings_from_DB['outline_size'],
                    minValue: 0.1,
                    maxValue: 5,
                    increment: 0.1,
                    decimalPrecision: 1,
                    listeners: {
                        changecomplete: function(value) {

                            IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'outline_size', value, true)
                        }
                    }
                }
            );

            items.push({
                text: "Outline color",
                iconCls: 'fa fa-16 fa-black fas fa-paint-brush',
                disabled: true,
                cls: 'full_opacity'
            });
            items.push(
                {
                    xtype: 'colorpicker',
                    allowReselect: true,
                    value: me.settings_from_DB['color'],
                    listeners: {
                        select: function(item, color){
                            var rgbColor = IMPACT.Utilities.Common.hexToRgb(color).join(',');

                            IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'color', color, true)
                        }
                    }
                }
            );

            items.push({
                text: ( me.settings_from_DB['simplificationRequired'] === 'true' ? 'Disable Features Simplification' : 'Enable Features Simplification'),
                iconCls: 'fa fa-lg fa-orange far fa-check-square',
                handler: function () {
                    var value = me.settings_from_DB.simplificationRequired === 'true' ? 'false' : 'true';
                    IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'simplificationRequired', value, true)
                 }
            });

        } else if(me.layerType === 'raster' || me.layerType === 'virtual'){

            var current_bands = me.settings_from_DB['bands'].split(",");

            // add fake band G B to build the invisible bands and sliders ... faster the build and check each time
            // settings scale2 anc scale3 are set by dafault in the LayerSetting functions
            if(current_bands.length == 1){
                current_bands.push(current_bands[0]);
                current_bands.push(current_bands[0]);
            }


            var hide_GB_band = me.data_from_DB['num_bands'] < 2 || me.settings_from_DB['rgb'] == false ? true : false;
            var R_field_label = hide_GB_band ? '' : 'R';

            items.push({
                text: "Visible bands",
                disabled: true,
                iconCls: 'fa fa-16 fa-black fas fa-sliders-h'
            });

            // Gray or RGB option
            items.push({
                    xtype: 'radiogroup',
                    width: 100,
                    style: 'margin-left: 60px;',
                    labelStyle: 'font-weight: bold;',
                    columns: 2,
                    itemId: 'visualization_mode',
                    items: [
                        {
                            xtype: 'radiofield',
                            boxLabel: 'RGB',
                            labelWidth: 50,
                            name: 'vis_mode',
                            inputValue: 'RGB',
                            checked: me.settings_from_DB['rgb'],
                            disabled: me.data_from_DB['num_bands'] < 2
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Gray',
                            name: 'vis_mode',
                            checked: !me.settings_from_DB['rgb'],
                            inputValue: 'Gray'

                        }
                    ],
                    listeners: {
                        change: function (item,newValue,oldValue) {
                            if (newValue['vis_mode'] === 'RGB') {
                                me.settings_from_DB['rgb'] = true;

                                me.down('#band_r_combo').setFieldLabel('R');
                                me.down('#band_r_combo').setVisible(true);
                                me.down('#band_g_combo').setVisible(true);
                                me.down('#band_b_combo').setVisible(true);

                                me.down('#minMax_r_slider').down('#slider').setFieldLabel('R');
                                me.down('#minMax_r_slider').setVisible(true);
                                me.down('#minMax_g_slider').setVisible(true);
                                me.down('#minMax_b_slider').setVisible(true);

                                //me.down('#slider_lock').setVisible(true);

                            } else{
                                me.settings_from_DB['rgb'] = false;

                                me.down('#band_r_combo').setFieldLabel('');
                                me.down('#band_g_combo').setVisible(false);
                                me.down('#band_b_combo').setVisible(false);

                                me.down('#minMax_r_slider').down('#slider').setFieldLabel('');
                                me.down('#minMax_g_slider').setVisible(false);
                                me.down('#minMax_b_slider').setVisible(false);

                                //me.down('#slider_lock').setVisible(false);

                            }
                        me.__updateStretch();
                        }
                    }
            });
            items.push({
                xtype: 'panel',
                itemId: 'bands_container',
                border: false,
                bodyStyle: 'background-color: rgba(0, 0, 0, 0);',
                layout: 'column',
                items: [
                    me.__band_combo(R_field_label, current_bands[0], false),
                    me.__band_combo('G', current_bands[1], hide_GB_band),
                    me.__band_combo('B', current_bands[2], hide_GB_band)
                ]
            });
            items.push({
                text: "Stretch type",
                iconCls: 'fa fas fa-palette'
            });

            items.push({
                xtype: 'impactFieldCombo',
                itemId: 'stretch_combo',
                width: 80,
                store: new Ext.data.SimpleStore({
                    fields: ['id','name'],
                    data : [
                        ['0','0-255'],
                        ['1','Min-Max'],
                        ['2','.5x StDev'],
                        ['3','1x StDev'],
                        ['4','2x StDev'],
                        ['5','3x StDev']
                    ]
                }),
                displayField: 'name',
                valueField: 'id',
                value: String(me.settings_from_DB['stretch']),
                listeners: {
                    change: function(combo, newValue){
                        me.__updateStretch();
                    }
                }
            });

            items.push({
                xtype: 'panel',
                itemId: 'bands_min_max_slider',
                border: false,
                bodyStyle: 'background-color: rgba(0, 0, 0, 0);',
                layout: 'column',
                width: 200,
                //hidden: me.settings_from_DB['stretch']!='1',  // onload visibility
                items:[
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_r_slider',
                        respect_boundaries: false,
                        fieldLabel: R_field_label,
                        labelWidth: 10,
                        values: me.settings_from_DB['scale1'].split(','),
                        minValue: parseFloat(statistics[current_bands[0]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[0]-1][1]),
                        increment: me.__setSliderIncrement(),
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            },
                        }
                    },
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_g_slider',
                        respect_boundaries: false,
                        fieldLabel: 'G',
                        labelWidth: 10,
                        values: me.settings_from_DB['scale2'].split(','),
                        minValue: parseFloat(statistics[current_bands[1]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[1]-1][1]),
                        increment: me.__setSliderIncrement(),
                        hidden: hide_GB_band,
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            },
                        }
                    },
                    {
                        xtype: 'impactFieldMultiSlider',
                        itemId: 'minMax_b_slider',
                        respect_boundaries: false,
                        fieldLabel: 'B',
                        labelWidth: 10,
                        values: me.settings_from_DB['scale3'].split(','),
                        minValue: parseFloat(statistics[current_bands[2]-1][0]),
                        maxValue: parseFloat(statistics[current_bands[2]-1][1]),
                        increment: me.__setSliderIncrement(),
                        hidden: hide_GB_band,
                        listeners: {
                            'changecomplete': function(){
                                me.__updateStretch(true);
                            }
                        }
                    },
                ]
            });

        } else if(me.layerType === 'class'){

            items.push({
                text: "Class range",
                iconCls: 'fa fa-16 fa-black fa-sliders'
            });

            items.push({
                xtype: 'multislider',
                itemId: 'class_range_slider',
                labelAlign: 'top',
                width: 200,
                values: [me.settings_from_DB['apply_data_range'][1], me.settings_from_DB['apply_data_range'][2]],
                minValue: statistics[0][0],
                maxValue: statistics[0][1],
                increment: 1,
                listeners: {
                    changecomplete: function(value) {
                        IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'apply_data_range', [true, value.getValues()[0], value.getValues()[1]], true)
                    }
                }
            });


            if (me.data_from_DB['has_colorTable'] === 1
                    && me.data_from_DB['num_bands'] === 1){
                items.push({
                    text: "Save range as mask",
                    iconCls: 'fa fa-16 fa-blue fa-save',
                    handler: function(){


                        var class_range = me.layerNode.data.settings_from_DB['apply_data_range'];

                        Ext.MessageBox.show({
                            msg: 'Save '+me.layerNode.data.text +" using value from "+ class_range[1]+ ' to '+ class_range[2]+' ?',
                            buttons: Ext.MessageBox.YESNO,
                            buttonText:{
                                yes: "Yes",
                                no: "No"
                            },
                            fn: function(btn){
                                if (btn === "yes"){
                                    var params = {
                                        toolID:  "rasterMask",
                                        infile: me.data_from_DB['full_path'],
                                        minrange: class_range[1],
                                        maxrange: class_range[2]
                                    };
                                    IMPACT.Utilities.Requests.launchProcessing(params);
                                }
                            }
                        });
                    }
                });
            }
        }

        return items;
    },

    fileOptions: function(){
        var me = this;
        var items = [];

        items.push({
            text: "Rename file",
            iconCls: 'fa fa-16 fa-black fa-edit',
            handler: function(){
                if(typeof Ext.ComponentQuery.query('#RenameWindow')[0] !== "undefined"){
                    Ext.ComponentQuery.query('#RenameWindow')[0].destroy();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.RenameWindow', {
                    src_filename: me.data_from_DB['full_path'],
                    map: me.workspace.down('#MapPanel').map,
                    layerNode: me.layerNode
                }).show();
            }
        });

        items.push({
            text: "Copy file",
            iconCls: 'fa fa-16 fa-black far fa-copy',
            handler: function(){
                if(typeof Ext.ComponentQuery.query('#CopyWindow')[0] !== "undefined"){
                    Ext.ComponentQuery.query('#CopyWindow')[0].destroy();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.CopyWindow', {
                    src_filename: me.data_from_DB['full_path']
                }).show();
            }
        });

        items.push({
            text: "Delete file",
            iconCls: 'fa fa-16 fa-red fa-times',
            handler: function(){
                Ext.MessageBox.show({
                    msg: 'Delete '+me.layerNode.data.text+" from disk?",
                    buttons: Ext.MessageBox.YESNO,
                    fn: function(btn){
                        if (btn === "yes"){
                            //me.layerNode.set('cls','x-tree-disabled');
                            if(me.layer){
                                me.workspace.down('#MapPanel').map.removeLayer(me.layer);
                            }
                            me.layerNode.cascadeBy(function (n) {
                                        if(n.data.layer && n.data.layer.get('type') === 'attributes'){
                                            //n.set('cls','x-tree-disabled');
                                            me.workspace.down('#MapPanel').map.removeLayer(n.data.layer);
                                        }
                                    });
                            me.layerNode.remove();
                            IMPACT.Utilities.Requests.fileHandler('delete', me.data_from_DB['full_path']);
                            IMPACT.Utilities.LayerSettings.save_settings_from_DB('delete', me.data_from_DB['full_path']);
                        }
                    }
                });
            }
        });

        return items;
    },


    layerExportOptions: function(items){
        var me = this;
        var items = [];

        items.push({
            text: "Google Earth KML (WMS) overlay",
            iconCls: 'fas fa-blue fa-globe-africa',
            disabled: (me.layer === null ? true : false),
            tooltip: (me.layer === null ? 'Visualize layer first' : ''),

            handler: function () {
                if(typeof Ext.ComponentQuery.query('#GoogleEarthKmlWindow')[0] !== "undefined"){
                    Ext.ComponentQuery.query('#GoogleEarthKmlWindow')[0].destroy();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.GoogleEarthKmlWindow', {
                     layerNode: me.layerNode
                }).show();
            },

        });
        if(me.layerType === 'geojson'){
             items.push({
                text: "ESRI SHP file",
                iconCls: 'fa fa-16 fa-black far fa-copy',
                handler: function(){
                   if(typeof Ext.ComponentQuery.query('#ConvertVectorWindow')[0] !== "undefined"){
                        Ext.ComponentQuery.query('#ConvertVectorWindow')[0].destroy();
                    }
                    Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.ConvertVectorWindow', {
                        src_filename: me.data_from_DB['full_path'],
                        outextension: '.shp',
                        title: 'GeoJSON to ESRI'
                    }).show();
                }
            });
        };


        if(me.layerType === 'raster' || me.layerType === 'virtual' || me.layerType === 'class'){
            items.push({
                text: "Other formats",
                iconCls: "fa fa-lg fa-black fa-file-o",
                handler: function () {
                    if( Ext.ComponentQuery.query('#ProcessingWindowRasterConversion').length == 0 ){
                        Ext.create('IMPACT.view.Processing.Window.RasterConversion');
                    }
                    var window = Ext.ComponentQuery.query('#ProcessingWindowRasterConversion')[0];
                    var data = [me.data_from_DB];
                    window.down('#raster_inputs').refresh(data);
                    window.show();
                },
                disabled: false
            });
        }
        if(me.layerType === 'vector'){
            items.push({
                text: "Raster format",
                iconCls: "fa fa-lg fa-black far fa-file",
                handler: function () {
                        if(typeof Ext.ComponentQuery.query('#ProcessingWindowVectorConversion')[0] !== "undefined"){
                            Ext.ComponentQuery.query('#ProcessingWindowVectorConversion')[0].destroy();
                        }
                        var window = Ext.create('IMPACT.view.Processing.Window.VectorConversion');
                        var data = [me.data_from_DB];
                        window.down('#vector_conversion_input').refresh(data);
                        window.show();
                },
                disabled: false
            });

             items.push({
                text: "GeoJSON file",
                iconCls: 'fa fa-16 fa-black far fa-copy',
                handler: function(){
                    if(typeof Ext.ComponentQuery.query('#ConvertVectorWindow')[0] !== "undefined"){
                        Ext.ComponentQuery.query('#ConvertVectorWindow')[0].destroy();
                    }
                    Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.ConvertVectorWindow', {
                        src_filename: me.data_from_DB['full_path'],
                        outextension: '.geojson',
                        title: 'ESRI to GeoJSON',
                    }).show();
                }
            });


        }

        return items;

    },
    rasterEditingOptions: function(items){
        var me = this;
        // ### Start Editing Mode ###
        if (me.layerNode.active_editing === false && me.workspace.editingMode === false){
            items.push({
                text: "Start Raster Recode",
                iconCls: 'fa fa-lg fa-red fa-edit',
                handler: function () {
                    Ext.ComponentQuery.query('#EditingRaster')[0].startEditingMode(me.layerNode);
                    //Ext.ComponentQuery.query('#MainToolbar')[0].toggleEditingMode(false);
                },
                disabled: !me.layerNode.data.checked
            });
        }
        // ### Stop Editing Mode ###
        else if (me.layerNode.active_editing  === true && me.workspace.editingMode === true){
            items.push({
                text: "Stop Raster Recode",
                iconCls: 'fa fa-lg fa-red fa-times-rectangle',
                handler: function () {
                    Ext.ComponentQuery.query('#EditingRaster')[0].stopEditingMode();
                }
            });
        }
        // ### Editing disabled ###  (Enabled on other file)
        else if (me.layerNode.active_editing  === false && me.workspace.editingMode === true){
            items.push({
                text: "Raster Recode disabled",
                iconCls: 'fa fa-lg fa-black fa-times',
                disabled: true
            });
        }
        return items;
    },

    vectorEditingOptions: function(items){
        var me = this;



        // show DBF in gridpanel
        items.push({
            text: "Open DBF attribute table",
            itemId: 'OpenAttributeTable',
            iconCls: 'fa fa-lg fa-table fa-black ',
            handler: function () {
                    if(typeof Ext.ComponentQuery.query('#DbfAttributeTable')[0] !== "undefined"){
                        Ext.ComponentQuery.query('#DbfAttributeTable')[0].destroy();
                    }
                    Ext.create('IMPACT.view.Workspace.Main.component.DbfAttributeTable',{
                        editingMode: me.layerNode.active_editing ,
                        layerNode :  me.layerNode
                    });
                }
        });

        // show Editing options
        var ColorPalettesFromStore = Ext.data.StoreManager.lookup('VectorLegends').data.items;
        var palettes = [];
        for (var i in ColorPalettesFromStore){
            palettes.push({
                text: ColorPalettesFromStore[i].data.name,
                // iconCls: "legend_icon_" + ColorPalettesFromStore[i].data.type,
                value: ColorPalettesFromStore[i].data.type,
                handler: function (item){
                    me.__applyColorPalette('vector', item.value);
                }
            });
        }

        items.push({
            text: "Apply Classification Legend to layer",
            icon: './resources/icons/class.png',
            menu: palettes,
            hideOnClick: false
        });


        // --------------------------------- ADD Editing Options to SHP only ---------------------------
        if(me.layerType === 'vector' ){

            items.push({
                itemId: 'buildSpatialIndex',
                text: "Build Spatial Index (fast rendering)",
                iconCls: 'fa fa-lg fa-green fa-signal',
                handler: function () {
                    var params = {
                        toolID:  "buildSpatialIndex",
                        infile : me.data_from_DB['full_path']
                    };
                    IMPACT.Utilities.Requests.launchProcessing(params);
                }
            });

            // ### Start Editing Mode ###
            if (me.layerNode.active_editing === false ){


                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
                    items.push({
                        itemId: 'QualityAssessment_menu',
                        text: "TMF Assessment Tool ",
                        iconCls: 'fa fa-lg fa-red fa-edit',
                        handler: function () {
                            if (me.settings_from_DB['legendType'] == ""){
                                me.__applyColorPalette('vector', 'REDD_Copernicus', execMode="TMF");
                            }
                            // in case user does not want to add new field , stop
                            if (me.settings_from_DB['legendType'] != ""){
                                Ext.ComponentQuery.query('#QualityAssessmentContainer')[0].startEditingMode(me.layerNode);
                            }

                        }
                    });

                };
                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
                    items.push({
                        itemId: 'QualityAssessment_menu',
                        text: "GSW Assessment Tool",
                        iconCls: 'fa fa-lg fa-red fa-edit',
                        handler: function () {
                            if (me.settings_from_DB['legendType'] == ""){
                                me.__applyColorPalette('vector', 'Land_Cover', execMode="GSW");
                            }
                            // in case user does not want to add new field , stop
                            if (me.settings_from_DB['legendType'] != ""){
                                Ext.ComponentQuery.query('#QualityAssessmentContainer')[0].startEditingMode(me.layerNode);
                            }
                        }


                    });

                };


                items.push({
                    itemId: 'StartVectorEditing',
                    text: "Start Editing",
                    iconCls: 'fa fa-lg fa-red fa-edit',
                    handler: function () {
                        if (me.settings_from_DB['legendType'] != ""){
                            // update_setting is done in the startEditingMode as well as the stop
                            //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'editing', true, false)
                            Ext.ComponentQuery.query('#EditingVector')[0].startEditingMode(me.layerNode);

                        } else {
                            alert('Apply Classification Legend before editing');
                        }
                    }
                });
            }
            // ### Stop Editing Mode ###
            else if (me.layerNode.active_editing === true){
                //  ##########   COPY DBF field    ##########

                items.push({
                    text: "Stop Editing",
                    iconCls: 'fa fa-lg fa-red fa-stop',
                    handler: function () {
                        // update_setting is done in the startEditingMode as well as the stop
                        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'editing', false, false)

                        Ext.ComponentQuery.query('#EditingVector')[0].stopEditingMode();
                        if(Ext.ComponentQuery.query('#QualityAssessmentContainer')[0]){
                            Ext.ComponentQuery.query('#QualityAssessmentContainer')[0].stopEditingMode();
                        }

                    }
                });
            }
            // ### Editing disabled ###  (Enabled on other file)
            else if (me.layerNode.active_editing === false && me.workspace.editingMode.active === true){
                items.push({
                    text: "Editing disabled",
                    iconCls: 'fa fa-lg fa-black fa-times',
                    disabled: true
                });
            }


        }

        return items;
    },

    __band_combo: function(band, selected_band, hidden){
        var me = this;
        var bands = [];
        for (var i=0; i < me.data_from_DB['num_bands']; i++) {
            bands.push(String(i+1));
        }
        bandID = band == '' ? 'R' : band;


        return {
            xtype: 'impactFieldCombo',
            itemId: 'band_'+bandID.toLowerCase()+'_combo',     // band_r_combo band_g_combo band_b_combo
            fieldLabel: band,
            labelStyle: 'font-size: 11px; color: #222; margin-bottom: 4px;',
            labelWidth: 10,
            width: 60,
            style: 'margin-right: 10px;',
            store: bands,
            value: selected_band,
            hidden: hidden,
            listeners: {
                select: function(){
                    me.__updateStretch();
                }
            }
        }

    },

    __updateStretch: function(MinMaxFromSlider){
        var me = this;
        var statistics = JSON.parse(me.data_from_DB['statistics']);
        var newSettings = {};
        newSettings['stretch'] = me.down('#stretch_combo').getValue();
        var r_Band = parseInt(me.down('#band_r_combo').getValue());
        var g_Band = parseInt(me.down('#band_g_combo').getValue());
        var b_Band = parseInt(me.down('#band_b_combo').getValue());

        if(me.down('#band_g_combo').isVisible()){
            newSettings['bands'] = String(r_Band)+','+String(g_Band)+','+String(b_Band);
        }
        else{
            newSettings['bands'] = String(r_Band);
        }

        var r_MinMax = me.down('#minMax_r_slider');
        var g_MinMax = me.down('#minMax_g_slider');
        var b_MinMax = me.down('#minMax_b_slider');

        // Update slider minValue & maxValue according to selected bands
        r_MinMax.updateMinMax(statistics[r_Band-1]);
        g_MinMax.updateMinMax(statistics[g_Band-1]);
        b_MinMax.updateMinMax(statistics[b_Band-1]);

        //var MinMaxValues = [];
        // if stretch_type is Min-Max
        //if (newSettings['stretch'] === '1'){
//        if(me.down('#slider_lock').checked){
//            newSettings['scale1'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(r_Band, newSettings['stretch'], me.data_from_DB, r_MinMax.values);
//            newSettings['scale2'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(g_Band, newSettings['stretch'], me.data_from_DB, r_MinMax.values);
//            newSettings['scale3'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(b_Band, newSettings['stretch'], me.data_from_DB, r_MinMax.values);
//        } else {
        if(!MinMaxFromSlider){
            newSettings['scale1'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(r_Band, newSettings['stretch'], me.data_from_DB, r_MinMax.values);
            newSettings['scale2'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(g_Band, newSettings['stretch'], me.data_from_DB, g_MinMax.values);
            newSettings['scale3'] = IMPACT.Utilities.LayerSettings.set_scale_per_band(b_Band, newSettings['stretch'], me.data_from_DB, b_MinMax.values);

            // move sliders
            var minMax = [newSettings['scale1'].split(',')[0],newSettings['scale1'].split(',')[1]];
            r_MinMax.__setSlider(minMax[0],minMax[1]);
            r_MinMax.__setTextField(null,minMax[0],{index:0});
            r_MinMax.__setTextField(null,minMax[1],{index:1});

            minMax = [newSettings['scale2'].split(',')[0],newSettings['scale2'].split(',')[1]];
            g_MinMax.__setSlider(minMax[0],minMax[1]);
            g_MinMax.__setTextField(null,minMax[0],{index:0});
            g_MinMax.__setTextField(null,minMax[1],{index:1});

            minMax = [newSettings['scale3'].split(',')[0],newSettings['scale3'].split(',')[1]];
            b_MinMax.__setSlider(minMax[0],minMax[1]);
            b_MinMax.__setTextField(null,minMax[0],{index:0});
            b_MinMax.__setTextField(null,minMax[1],{index:1});

        }else{
            newSettings['scale1'] = String(r_MinMax.values[0])+','+String(r_MinMax.values[1])
            newSettings['scale2'] = String(g_MinMax.values[0])+','+String(g_MinMax.values[1])
            newSettings['scale3'] = String(b_MinMax.values[0])+','+String(b_MinMax.values[1])
        }

        // save settings
        // STRETCH AND SLIDER are interlinked, save and redraw manualy to not propagate multiple save
        for (key in me.settings_from_DB) {
            if(newSettings[key] && newSettings[key] !== me.settings_from_DB[key]){
                me.settings_from_DB[key] = newSettings[key];
                me.layerNode.data.layer.getSource().updateParams({[key] : newSettings[key]});
            }
        };

        IMPACT.Utilities.LayerSettings.save_settings_from_DB('save', me.data_from_DB['full_path'], '', me.settings_from_DB);
        me.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
        me.layer.getSource().refresh();


    },

    __applyColorPalette: function(type, colorPalette, execMode){
        var me = this;
        if(type === 'vector'){
            me.__applyColorPaletteSHP(colorPalette, execMode)
        }
        else if(type === 'raster'){
             IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'legendType', colorPalette, true)
        }
    },

    __applyColorPaletteSHP: function(colorPalette, execMode){

        var me = this;

        var mapPanel = me.workspace.down('#MapPanel');
        var treePanel = me.workspace.down('#LayerTreePanel');

        if (!me.layer){
            treePanel.addLayerToMap(me.layerNode);
            me.layer = IMPACT.Utilities.OpenLayers.getLayerByFullPath(mapPanel.map,me.data_from_DB['full_path']);
            me.layer.setVisible(false);
         }

        // set parent layer legend in params
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'legendType', colorPalette, false)

        //  Clean sub-nodes
        var layer2del = me.layerNode.childNodes;
        for (var i in layer2del){
             mapPanel.map.removeLayer(layer2del[i].data.layer)
        }
        me.layerNode.removeAll('destroy');

        // Look for missing attributes (add if necessary)

        var missingFields = me.__get_editing_missing_attributes(execMode);


        if(missingFields.length>0){
            Ext.Msg.show({
                title: 'Missing attributes',
                msg: 'Shapefile does not contain required attributes. <br />The following attributes will be added:<br />- '+missingFields.join(',<br />- '),
                buttons: Ext.MessageBox.OKCANCEL,
                buttonText:{
                    ok: "Continue",
                    cancel: "Cancel"
                },
                fn: function(btn){
                    if(btn === 'ok') {
                        me.layerNode.set('cls','x-tree-disabled');
                        me.layerNode.set('text','<span class="fa fa-lg fa-black fa-spinner fa-pulse"></span> '+me.layerNode.data.text);
                        IMPACT.Utilities.Requests.add_missing_fields_to_shp(
                            me.data_from_DB['full_path'],
                            mapPanel,
                            me.layerNode,
                            missingFields
                        );
                    }
                }
            });
            return;
        }


        // set parent layer legend in params
        IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.layerNode, 'legendType', colorPalette, false)

        // Switch node from folder to leaf
        me.layerNode.set('leaf', false);
        me.layerNode.set('allowChildren', true);
        me.layerNode.set('expanded', true);
        me.layerNode.set('allowDrop', false);

        // Create and append one sub-node for each attribute
        //var attributes = me.__get_editing_layers();
        var attributes = IMPACT.Utilities.OpenLayers.getEditingLayers(this);

        for(var i=0; i<attributes.length; i++){
            //var attribute_layer = me.layer.clone();  // not working on OL10,
            //var attribute_layer = new ol.layer.Tile(me.layer.getProperties());  // not working on OL10, double layer on map
            var attribute_layer = IMPACT.OpenLayers.Vector.createLayer(
                    me.layerNode.data.data_from_DB,
                    me.layerNode.data.settings_from_DB,
                    IMPACT.Utilities.OpenLayers.getTileSize(mapPanel.map)
                );

            attribute_layer.set('name', attributes[i]);
            attribute_layer.name = attributes[i];
            attribute_layer.getSource().updateParams({'layers' : ['MySHP']});
            attribute_layer.getSource().updateParams({'legendType' : colorPalette});
            attribute_layer.getSource().updateParams({'classItem' : attributes[i]});
            attribute_layer.getSource().updateParams({'classColors' : Ext.data.StoreManager.lookup('VectorLegends').getClassColors(colorPalette)});
            attribute_layer.getSource().updateParams({'classIDs' : Ext.data.StoreManager.lookup('VectorLegends').getClassIDs(colorPalette)});
            attribute_layer.getSource().updateParams({'classVisible' : attribute_layer.getSource().getParams()['classIDs']});
            attribute_layer.set('type','attributes');
            //attribute_layer.options.group = me.layer.options.group+"/"+me.layer.get('name');
            //attribute_layer.group = me.layer.get('group')+"/"+me.layer.get('name');
            //attribute_layer.set('group', me.layer.get('group')+"/"+me.layer.get('name'));
            attribute_layer.setVisible(false);
            me.layerNode.appendChild({
                text: attribute_layer.name,
                leaf: true,
                checked: false, //attribute_layer.isVisible(),
                allowDrag:false

            });

            var Node = treePanel.__getNodeBy(me.layerNode, 'text', attribute_layer.get('name'));   // in the Node the name is called 'text'

            Node.set('layer', attribute_layer);
            Node.set('data_from_DB', me.data_from_DB);
            Node.set('settings_from_DB', me.settings_from_DB);
            mapPanel.map.addLayer(attribute_layer);
        }

        treePanel.sortMapLayers();
    },

    __setSliderIncrement: function(){
        var me = this;
        var data_type = me.data_from_DB['data_type'];
        var increment = 1/255;
        if(data_type === 'Byte' || data_type === 'UInt16' || data_type === 'UInt32'){
            increment = 1;
        }
        return increment;
    },

    __get_editing_missing_attributes: function(execMode=""){
        var missingFields = [];
        // dafault validation fields
        var requiredFields = [
                IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
    //            IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
    //            IMPACT.SETTINGS['vector_editing']['attributes']['comments']
        ];

        //  -------------------  TMF or GSW  -----------------------------
        if (execMode == "TMF" ){
            var requiredFields = [
                IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                //IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
                'user','editdate','verify'
                ,'YYYY','MMDD'
                ,'nbr','ndvi'
                , "traj", "dist", "regr", "YYdist", "YYregr"
                , "trajconf", "distconf", "regconf"
            ];

        }        //  -------------------  TMF or GSW  -----------------------------
        if (execMode == "GSW"){
            var requiredFields = [
                IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                //IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
                'user','editdate','verify'
                ,'ndvi','nbr'
                ,'YYYY','MMDD'];

        }

        var layerFields = this.data_from_DB['attributes'].split(',');
        for(var i = 0; i < requiredFields.length; i++){
            var reqField = requiredFields[i];
            if(layerFields.indexOf(reqField) === -1){
                missingFields.push(reqField);
            }
        }
        return missingFields;


    },



});