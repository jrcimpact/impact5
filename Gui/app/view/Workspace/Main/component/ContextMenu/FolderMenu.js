/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.FolderMenu', {
    extend: 'Ext.menu.Menu',

    requires: [
        'IMPACT.view.component.Field.Combo',
    ],

    itemId: 'FolderContextMenu',
    width: 300,
    layerNode: null,
    workspace: null,
    treePanel: null,

    items: [],


    initComponent:function() {
        var me = this;
        me.menuItems();
        me.callParent(arguments);
    },

    menuItems: function(){
        var me = this;
        me.items = [];
        if( typeof me.layerNode.rasterSelect === 'undefined'){
            me.layerNode.rasterSelect = false;
        }
        if( typeof me.layerNode.vectorSelect === 'undefined'){
            me.layerNode.vectorSelect = false;
        }

//  ######################   SELECT LAYER   ###########################
        me.items.push({
            text: "Raster animation",
            iconCls: 'fa fa-16 fa-black fa-edit',
            handler: function(){
                if(typeof Ext.ComponentQuery.query('#RasterAnimation')[0] !== "undefined"){
                    Ext.ComponentQuery.query('#RasterAnimation')[0].close();
                }
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.RasterAnimation', {
                    map: me.workspace.down('#MapPanel').map,
                    treePanel: me.treePanel,
                    layerNode: me.layerNode
                }).show();
            }
        });


        if(me.layerNode.rasterSelect == false){
            me.items.push({
                text: "Select all raster",
                iconCls: 'fa fa-green fa-toggle-on',
                handler: function () {
                    me.layerNode.rasterSelect = true;
                    me.__selectAll('.tif', true);
                }
            });
        }else{
            me.items.push({
                text: "Deselect all raster",
                iconCls: 'fa fa-red fa-toggle-on',
                handler: function () {
                    me.layerNode.rasterSelect = false;
                    me.__selectAll('.tif', false);
                }
            });
        };

        if(me.layerNode.vectorSelect == false){
             me.items.push({
                text: "Select all vector",
                iconCls: 'fa fa-green fa-toggle-on',
                handler: function () {
                    me.layerNode.vectorSelect = true;
                    me.__selectAll('.shp', true);
                }
            });
        } else {
             me.items.push({
                text: "Deselect all vector",
                iconCls: 'fa fa-red fa-toggle-on',
                handler: function () {
                    me.layerNode.vectorSelect = false;
                    me.__selectAll('.shp', false);
                }
             });
        };

        //  ######################   EXPORT GE LAYER   ###########################
        me.items.push({
                text: "Export all raster to Google Earth KML (WMS) overlay ",
                iconCls: 'fas fa-blue fa-globe-africa',
                handler: function () {
                    Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.GoogleEarthKmlWindow', {
                                layerNode: me.layerNode
                    }).show();
                }
        });

        //  ######################   EXPORT GE LAYER   ###########################
        me.items.push({
                text: "Build Spatial Index (all vector files)",
                iconCls: 'fa fa-lg fa-green fa-signal',
                handler: function () {
                        var params = {
                            toolID:  "buildSpatialIndex",
                            infile : me.layerNode.data.breadcrumbs
                        };
                        IMPACT.Utilities.Requests.launchProcessing(params);
                    }
        });
                //  ######################   EXPORT GE LAYER   ###########################
        me.items.push({
                text: "Build pyramids (all raster files)",
                iconCls: 'fa fa-lg fa-blue fa-signal',
                handler: function (){
                    var params = {
                        toolID:  "buildPyramids",
                        infile : me.layerNode.data.breadcrumbs
                    };
                    IMPACT.Utilities.Requests.launchProcessing(params);
                }
        });

    },

    __selectAll: function(type, status){
        var me = this;


        me.layerNode.cascadeBy(function(n){
                if( n.isLeaf() && n.data.data_from_DB && n.data.data_from_DB['extension'] == type){
                        //|| (n.isLeaf()==false && n.data.layer)  // vector folder

                        n.set('checked',status);
                        //n.fireEvent('checkchange', n, status);
                        me.treePanel.fireEvent('checkchange', n, status);
                }
            })
    }


});