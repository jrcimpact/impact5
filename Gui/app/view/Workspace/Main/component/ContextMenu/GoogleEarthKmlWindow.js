/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.ContextMenu.GoogleEarthKmlWindow', {
    extend: 'IMPACT.view.component.Window',

    requires: ['IMPACT.view.component.Field.Text',
               'IMPACT.view.Processing.Window.component.Info'
    ],

    itemId: 'GoogleEarthWindow',

    title: "Create Google Earth KML+WMS file",
    closeAction: 'destroy',
    width: 465,
    items: [],

    path: null,
    src_filename: null,
    extension: null,
    layerNode: null,
    classItem: null,

    buttons: [
        {
            text: 'Create',
            handler: function(){
                this.up('#GoogleEarthWindow').create();
            }
        }
    ],

    listeners: {
        'destroy': function () {
        }
    },

    initComponent: function() {
        var me = this;
        // leaf of Folder node for expanded Shapefile
        if (me.layerNode.isLeaf() || (me.layerNode.isLeaf()== false && me.layerNode.data.data_from_DB) ){
            me.src_filename = me.layerNode.data.data_from_DB['full_path'],
            me.path = IMPACT.Utilities.Common.dirname(me.src_filename);
            me.extension = "." + me.src_filename.split('.').pop();
        } else {
            // use all raster in directory

            me.src_filename = me.layerNode.data['breadcrumbs']
            me.path = me.layerNode.data['breadcrumbs']
            me.extension = ''

        }

        me.items = [];
        me.items.push({
            xtype: 'form',
            border: false,
            items: [
                {
                    xtype: 'impactFieldText',
                    itemId: 'new_filename',
                    name: 'new_filename',
                    fieldLabel: 'KML file name',
                    labelAlign: 'top',
                    labelStyle: 'margin-bottom: 3px;',
                    width: 440,
                    value: IMPACT.Utilities.Common.basename(me.src_filename, me.extension),
                    allowBlank: false,
                    regex: /^[0-9a-z_A-Z-]+$/
                },
                {
                    xtype: 'ProcessingInfo',
                    html: 'Visualization parameters (bands, stretch and colors) are taken from current layer settings'
                }
            ]
        });

        me.callParent(arguments);
    },

    create: function(){
        var me = this;


        var form = me.down('form').getForm();
        if (form.isValid()) {
            var dest_filename = [me.path, me.down('#new_filename').getValue()+'.kml'].join("/");
            if(me.layerNode.isLeaf() || (me.layerNode.isLeaf()== false && me.layerNode.data.data_from_DB)){

                    var extent = JSON.parse(me.layerNode.data.data_from_DB['extent']);

                    extent = [extent["W"], extent["S"], extent["E"], extent["N"]];


                    var tmpCode="temp_proj:"+Date.now().toString();
                    proj4.defs(tmpCode, me.layerNode.data.data_from_DB['PROJ4']);
                    ol.proj.proj4.register(proj4);
                    var layer_projection = new ol.proj.Projection({code:tmpCode});


                    var bbox = ol.proj.transformExtent(extent, layer_projection, IMPACT.SETTINGS['WGS84']);

                    var BANDS = '';
                    var SCALE1 = '';
                    var SCALE2 = '';
                    var SCALE3 = '';
                    var has_colorTable = 0;
                    var legendType = '';

                    if (me.layerNode.data.layer.get('type') != 'vector'){
                        BANDS =  me.layerNode.data.layer.getSource().getParams()['bands'];
                        SCALE1 = me.layerNode.data.layer.getSource().getParams()['scale1'];
                        SCALE2 = me.layerNode.data.layer.getSource().getParams()['scale2'];
                        SCALE3 = me.layerNode.data.layer.getSource().getParams()['scale3'];
                        has_colorTable = me.layerNode.data.layer.getSource().getParams()['has_colorTable'];
                        legendType = typeof(me.layerNode.data.layer.getSource().getParams()['legendType']) === "undefined" ? '' : me.layerNode.data.layer.getSource().getParams()['legendType'];

                    } else {
                        if (me.classItem == null){
                            legendType = me.layerNode.data.layer.getSource().getParams()['color'];
                            me.classItem='';
                        } else {
                            legendType = me.layerNode.data.layer.getSource().getParams()['legendType'];
                        }


                    }

                    var content  = '<?xml version="1.0" encoding="UTF-8"?>\n';
                        content += '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n';
                        content += '<GroundOverlay>\n';
                        content += '    <name>'+me.down('#new_filename').getValue()+'</name>\n';
                        content += '   <LookAt>\n';
                        content += '        <longitude>'+(bbox[0]+Math.abs(bbox[2]-bbox[0])/2.).toString()+'</longitude>\n';
                        content += '        <latitude>'+(bbox[1]+Math.abs(bbox[3]-bbox[1])/2.).toString()+'</latitude>\n';
                        content += '        <altitude>0</altitude>\n';
                        content += '        <heading>1.444272911417784</heading>\n';
                        content += '       <tilt>0</tilt>\n';
                        content += '        <range>151689.3459947589</range>\n';
                        content += '        <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n';
                        content += '    </LookAt>\n';
                        content += '    <Icon>\n';
                        content += '        <href>'+IMPACT.GLOBALS['url']+'/geotiff_to_KML_wms.py?VERSION=1.1.1&amp;REQUEST=GetMap&amp;SRS=EPSG:4326&amp;WIDTH=1500&amp;HEIGHT=1500&amp;STYLES=default&amp;TRANSPARENT=TRUE&amp;FORMAT=image/png&amp;LAYERS='+me.src_filename.replace(IMPACT.GLOBALS['data_paths']['data'],'')+'&amp;BANDS='+BANDS+'&amp;SCALE1='+SCALE1+'&amp;SCALE2='+SCALE2+'&amp;SCALE3='+SCALE3+'&amp;has_colorTable='+has_colorTable+'&amp;legendType='+legendType+'&amp;classItem='+me.classItem+'&amp;</href>\n';
                        content += '       <viewRefreshMode>onStop</viewRefreshMode>\n';
                        content += '       <viewRefreshTime>1</viewRefreshTime>\n';
                        content += '       <viewBoundScale>0.75</viewBoundScale>\n';
                        content += '    </Icon>\n';
                        content += '   <LatLonBox>\n';
                        content += '        <north>'+bbox[3].toString()+'</north>\n';
                        content += '        <south>'+bbox[1].toString()+'</south>\n';
                        content += '        <east>'+bbox[2].toString()+'</east>\n';
                        content += '        <west>'+bbox[0].toString()+'</west>\n';
                        content += '    </LatLonBox>\n';
                        content += ' </GroundOverlay>\n';
                        content += '</kml>\n';
            // loop all files in dir
            } else {

                var content  = '<?xml version="1.0" encoding="UTF-8"?>\n';
                    content += '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n';
                    content += '<Folder>\n<name>'+me.down('#new_filename').getValue()+'</name>\n<open>1</open>\n'


                me.layerNode.cascadeBy(function(n){

                    if( n.isLeaf() && n.data.data_from_DB && n.data.data_from_DB['extension'] == '.tif'){

                            var extent = JSON.parse(n.data.data_from_DB['extent']);
                            extent = [extent["W"], extent["S"], extent["E"], extent["N"]]
                            var tmpCode="temp_proj:"+Date.now().toString();
                            proj4.defs(tmpCode, n.data.data_from_DB['PROJ4']);
                            ol.proj.proj4.register(proj4);

                            var layer_projection = new ol.proj.Projection({code:tmpCode});
                            var bbox = ol.proj.transformExtent(extent, layer_projection, IMPACT.SETTINGS['WGS84']);


                            var filename =  n.data.data_from_DB['full_path']
                            var BANDS = '1,1,1';
                            var SCALE1 = '0,50';
                            var SCALE2 = '0,50';
                            var SCALE3 = '0,50';
                            var has_colorTable = 0;
                            var legendType = '';

                            if (n.data.layer){
                                BANDS = n.data.layer.getSource().getSource().getParams()['bands'];
                                SCALE1 = n.data.layer.getSource().getParams()['scale1'];
                                SCALE2 = n.data.layer.getSource().getParams()['scale2'];
                                SCALE3 = n.data.layer.getSource().getParams()['scale3'];
                                has_colorTable = n.data.layer.getSource().getParams()['has_colorTable'];
                                //has_colorTable = n.data.layer.getSource().get(has_colorTable);
                                legendType = typeof(n.data.layer.getSource().getParams()['legendType']) === "undefined" ? '' : n.data.layer.getSource().getParams()['legendType'];

                            } else {
                                 var imageSettingsStore = Ext.data.StoreManager.lookup('ImageSettingsStore');
                                 var record = imageSettingsStore.getRecordBy('full_path',n.data.data_from_DB['full_path']);
                                 var values = {};

                                 if(record && record[0] && record[0].settings){
                                        values = Ext.decode(record[0].settings);
                                 }

                                var settings = IMPACT.Utilities.LayerSettings.harmonizeDefaultLayerParams(n.data.data_from_DB, values);
                                BANDS = settings['bands'];
                                SCALE1 = settings['scale1'];
                                SCALE2 = settings['scale2'];
                                SCALE3 = settings['scale3'];
                                has_colorTable = n.data.data_from_DB['has_colorTable'];
                            }

                            content += '<GroundOverlay>\n';
                            content += '<visibility>0</visibility>\n';
                            content += '    <name>'+IMPACT.Utilities.Common.basename(filename)+'</name>\n';
                            content += '   <LookAt>\n';
                            content += '        <longitude>'+(bbox[0]+Math.abs(bbox[2]-bbox[0])/2.).toString()+'</longitude>\n';
                            content += '        <latitude>'+(bbox[1]+Math.abs(bbox[3]-bbox[1])/2.).toString()+'</latitude>\n';
                            content += '        <altitude>0</altitude>\n';
                            content += '        <heading>1.444272911417784</heading>\n';
                            content += '       <tilt>0</tilt>\n';
                            content += '        <range>151689.3459947589</range>\n';
                            content += '        <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n';
                            content += '    </LookAt>\n';
                            content += '    <Icon>\n';
                            content += '        <href>'+IMPACT.GLOBALS['url']+'/geotiff_to_KML_wms.py?VERSION=1.1.1&amp;REQUEST=GetMap&amp;SRS=EPSG:4326&amp;WIDTH=1500&amp;HEIGHT=1500&amp;STYLES=default&amp;TRANSPARENT=TRUE&amp;FORMAT=image/png&amp;LAYERS='+filename.replace(IMPACT.GLOBALS['data_paths']['data'],'')+'&amp;BANDS='+BANDS+'&amp;SCALE1='+SCALE1+'&amp;SCALE2='+SCALE2+'&amp;SCALE3='+SCALE3+'&amp;has_colorTable='+has_colorTable+'&amp;legendType='+legendType+'&amp;</href>\n';
                            content += '       <viewRefreshMode>onStop</viewRefreshMode>\n';
                            content += '       <viewRefreshTime>1</viewRefreshTime>\n';
                            content += '       <viewBoundScale>0.75</viewBoundScale>\n';
                            content += '    </Icon>\n';
                            content += '   <LatLonBox>\n';
                            content += '        <north>'+bbox[3].toString()+'</north>\n';
                            content += '        <south>'+bbox[1].toString()+'</south>\n';
                            content += '        <east>'+bbox[2].toString()+'</east>\n';
                            content += '        <west>'+bbox[0].toString()+'</west>\n';
                            content += '    </LatLonBox>\n';
                            content += ' </GroundOverlay>\n';

                    }
                })

                content += '</Folder>\n</kml>\n';

            }


            IMPACT.Utilities.Requests.fileHandler('GoogleEarth', me.src_filename, dest_filename, content);
            me.destroy();
        }
    }

});