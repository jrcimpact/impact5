/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.component.LayerQuery', {
    extend: 'IMPACT.view.component.Window',

    itemId: 'LayerQuery',

    closeAction: 'destroy',

    layout: 'fit',
    //width: 350,
    height: 400,
    overflowY: 'scroll',
    overflowX: 'scroll',
    bodyStyle: 'padding: 5px; background-color: white; line-height: 15px; font-size:11px;',

    title: 'Query Result',
    buttons: null,
    chartLists: [],

    parseInfo: function(info){
        var me = this;
        var text = '';
        me.chartLists = [];
        var numFiles = info.length;
        if(numFiles==0){
            me.html = 'No results.'
            return;
        }
        for (var i=0; i<numFiles; i++) {
            text += me.parseFileInfo(info[i]);
        }
        me.html = text;
    },
    listeners:{
        'beforedestroy':function(win){
                Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
        },
        'afterrender':function(win){
                Ext.ComponentQuery.query('#MainToolbar')[0].setQueryMode();
                var me = this;
                for (var i=0; i< me.chartLists.length; i++) {
                    var myChart = echarts.init(document.getElementById(me.chartLists[i]['id']));
                    myChart.setOption(me.chartLists[i]['option']);
                }


        },

    },
    parseFileInfo: function(info){
        var me = this;


        var filename = info['data_from_DB']['basename']+info['data_from_DB']['extension'];
        var text = '<p class="layerInfoHeader">'+filename+'</p>';
        text += '<p class="layerInfoContent">';
        text += me.printAttribute('File name', filename);
        text += me.printAttribute('Path', info['data_from_DB']['path']+'/');
        text += me.printAttribute('Lat in file proj', info['lat']);
        text += me.printAttribute('Lon in file proj', info['lon']);
        text += me.printAttribute('Col', info['col']);
        text += me.printAttribute('Row', info['row']);
        text += '</p>';

        if(typeof(info['bands'])!="undefined"){
            text += me.__printBands(info['bands']);
            if (info['bands'].length > 1){
                var chartId = 'chart_'+Math.random().toString(16).substring(2, 15);
                text += '<div id="'+chartId+'" style="width: 300px; height: 300px;"></div>'
                me.initChart(info['bands'], chartId);
            }
        }

        if(typeof(info['features'])!="undefined"){
            text += me.__printFeatures(info['features']);
        }

        text += '<br />';

        return text;
    },

    __printBands: function(bands_info){
        var me = this;
        var text = '<b>Band info</b><br />';
        var num_bands = bands_info.length;
        for(var i=0; i<num_bands; i++){
           text += '<i>Band ' + bands_info[i]['band'] + '</i>&nbsp;&nbsp;pixel value: ' + bands_info[i]['value'] + '<br />'
        }
        return text;
    },

    __printFeatures: function(features_info){
        var me = this;
        var text = '';
        var num_features = features_info.length;
        for(var i=0; i<num_features; i++){
            text += '<b>Feature info</b><br />';
            var attributes = features_info[i];
            for (var key in attributes) {
                text += '<i>' + key + '</i>: '+attributes[key]+'<br />';
            }
        }
        return text;
    },

    printAttribute: function(label, value){
        var me = this;
        if( typeof(value)!="undefined"
                && value!=null
                && value!=''){
             return "<b>"+label+": </b>&nbsp;"+value.toString()+"</br>";
        }
        return '';
    },

    initChart: function(data,id){
        var me = this;
        if( typeof(data)!="undefined"
                && data!=null
                && data!=''){
            var bandValues = [];
            for(var i=0; i< data.length; i++){
               bandValues.push([i,data[i]['value']]);
            }
             //var myChart = echarts.init(document.getElementById('line_chart'));

             var option = {
                        xAxis: {
                            type: 'value',
                            //data:[1,2,3,4,5,6,7,8,9,10]
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            data: bandValues,
                            type: 'line'
                        }],
                        tooltip: {},
                        grid: {
                            left: '5%',
                            right: '15%',
                            bottom: '15%',
                            containLabel: true
                        },
                        dataZoom: [
                                    {
                                        id: 'dataZoomX',
                                        type: 'slider',
                                        xAxisIndex: [0],
                                        filterMode: 'filter',
                                        //start: 1,
                                        //end: 99
                                    },
                                    {
                                        id: 'dataZoomY',
                                        type: 'slider',
                                        yAxisIndex: [0],
                                        filterMode: 'empty',
                                        //start: 0,
                                        //end: 99
                                    },
            //                         {
            //                            type: 'inside',
            //                            xAxisIndex: 0,
            //                            filterMode: 'empty'
            //                        },
                                    {
                                        type: 'inside',
                                        yAxisIndex: 0,
                                        filterMode: 'empty'
                                    }


                        ],
                        toolbox: {
                            show : true,
                            //orient:'orizontal',
                            feature : {
                                         magicType : {show: true, title:['bar','line'], type: ['bar','line']},
                                         restore : {show: true, title:'Restore'},
                                         saveAsImage : {show: true, title:"Save As Image"}
                                        }
                        }

                    };
             //myChart.setOption(option);
             //console.log(option);
             //return myChart;
             me.chartLists.push({'option':option, 'id': id})
        }

    },


});