/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.CSTreePanel', {

    extend: 'IMPACT.view.component.ImageTree.Tree',
    alias: 'widget.CSLayerTreePanel',

    itemId: 'CSLayerTreePanel',
    cls: 'impact-LayerTreePanel',

    border: false,
    allowDrag: true,
    //disableSelection: false,   // ensure or prevent dragDrop

    map: null,
    imageSettingsStore: null,

    initComponent: function(){
        this.fileStore = Ext.data.StoreManager.lookup('CSImageStore');
        this.imageSettingsStore = Ext.data.StoreManager.lookup('ImageSettingsStore');

        this.callParent();
        //this.addEvents('refreshLayer');

        this.on('refreshLayer', function(leafNode){
            this.__refresh(leafNode);

        });
    },

//    // --- link map into tree; not possible in initComponent since not yet ready --
    afterRender: function(){
        this.callParent();
        this.map = IMPACT.main_map;

        this.fileStore.on('updateCSStore_finished', function(){
            Ext.ComponentQuery.query('#CSLayerTreePanel')[0].__buildTree(IMPACT.GLOBALS['data_paths']['unix_raw_data']);
        });
    },

    viewConfig: {
        plugins: {
           ptype: 'treeviewdragdrop',
           //dragText: 'Drag and drop to reorganize'
        },

        listeners: {
            "beforedrop": function(node, data, overModel, dropPosition ){
                return this.up('#CSLayerTreePanel').node_drop_manager(data, overModel, dropPosition );
            },
            "drop": function(){
                 this.up('#CSLayerTreePanel').sortMapLayers();
            }
        }
    },

    listeners : {
        "itemdblclick" : function(tree, record, index){

            if(record.data.data_from_DB){
                var bbox = IMPACT.Utilities.Impact.parse_bbox(record.data.data_from_DB);
                this.map.getView().fit([bbox.left,bbox.bottom,bbox.right,bbox.top]);
            }
        },
        "itemcontextmenu": function(dv, record, item, index, e) {
            if(record.data.data_from_DB){
                //e.stopEvent();   // Prevent the browser to show its default contextmenu
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.CSMenu',{
                    workspace: this.up('#MainWorkspace'),
                    layerNode: record
                }).showAt(e.getXY());
            } else {
                //e.stopEvent();
                Ext.create('IMPACT.view.Workspace.Main.component.ContextMenu.FolderMenu',{
                    workspace: this.up('#MainWorkspace'),
                    treePanel: this,
                    layerNode: record
                }).showAt(e.getXY());


            }
        },
        "checkchange": function(node, checked){

                  if(checked){
                    this.addLayerToMap(node);
                  } else {
                    this.removeLayerFromMap(node);
                  }
        }
    },

     /**
     *  #######  Extend __createLeaf from Tree.js adding settings to node if existing #######
     */
     __createLeaf: function(parentNode, item){
        var me = this;
        var leafNode = this.superclass.__createLeaf(parentNode, item);

        if (leafNode === null){
            // node exists and only updated

            var updated_node = me.__getNodeBy_data_from_DB(parentNode,'full_path',item.data['full_path']);

            if (updated_node.data.settings_from_DB['stretch'] == -1){
                // change default 0-255 stretch with image min max once stats are ready
                //updated_node.data.settings_from_DB['stretch'] = 1;
                updated_node.set('settings_from_DB', IMPACT.Utilities.LayerSettings.harmonizeDefaultLayerParams(updated_node.data.data_from_DB, {}));

            }

            // update data from DB in Layer if updated
            // layer can be created while calculating stats. If update then reset stats

            if (updated_node.data.layer){
                updated_node.data.layer.data_from_DB = item.data;
                updated_node.data.layer.has_colorTable = item.data['has_colorTable'];

            }
            // reset label in case type changes
           if(updated_node.dirty){
                updated_node.set('iconCls',me.__setLayerIcon(item));
           }


            // redraw only raster if not in editing mode (processing etc etc)
            // while editing there is a layer.redraw() to not wait the sseStreamer
            // why not vector ?? can be done but usually update affects DBF and not geom. If DBF change = editing mode with self-redraw

            if(updated_node.dirty && updated_node.data.layer && !updated_node.active_editing && updated_node.data.data_from_DB['extension'] == '.tif'){
                updated_node.data.layer.getSource().refresh();
                updated_node.dirty = false;
            }

            return
        }

        var record = me.imageSettingsStore.getRecordBy('full_path',leafNode.data.data_from_DB['full_path']);
        var values = {};

        if(record && record[0] && record[0].settings){
            values = Ext.decode(record[0].settings);
        }

        leafNode.set('settings_from_DB', IMPACT.Utilities.LayerSettings.harmonizeDefaultLayerParams(leafNode.data.data_from_DB, values));
        leafNode.set('allowDrag',me.allowDrag);
        leafNode.active_editing = false;
        leafNode.active_editing = false;


        //return leafNode
     },

    /**
     *  ###################################
     *  #######  NODE Drop manager  #######
     *  ###################################
     */
    node_drop_manager: function (data, overModel, dropPosition){
        var me = this;
        // NOTE: drag and drop of Vector Attributes is not allow (easier)

        var draggedNode = data.records[0];
        var dropOverNode = overModel;
        var destinationFolderNode = overModel.parentNode;

        var draggedNodeIsLeaf = draggedNode.isLeaf();
        //var draggedNodeLayer = draggedNode.data.layer;
        //var draggedNode_data_from_DB = draggedNode.data.data_from_DB;
        var draggedFolderPath = draggedNode.data.breadcrumbs ? draggedNode.data.breadcrumbs : false; // false if leaf/layer

        var sourcePath = draggedNode.parentNode.data.breadcrumbs;
        var destinationPath = destinationFolderNode.data.breadcrumbs;

        // drop before root not allowed
        if (typeof destinationPath === "undefined"){
            return false
        }

        if(!destinationPath.endsWith('/')){
            destinationPath = destinationPath+'/';
        }
        if(!sourcePath.endsWith('/')){
            sourcePath = sourcePath+'/';
        }

        // #####  Manage Drop  #####
        if(sourcePath !== destinationPath
                || (sourcePath === destinationPath    // drop on folder node
                    && dropOverNode.isLeaf() === false  // data_from_DB is not in folder node
                    && typeof dropOverNode.data.data_from_DB === "undefined")){


            var sourceFullPath = null;
            if(draggedNodeIsLeaf === false                                   // move folder
                    && (typeof draggedNode.data.data_from_DB === "undefined"
                        || draggedNode.data.data_from_DB === '')){
                //sourceFullPath = IMPACT.GLOBALS['root_path']+draggedFolderPath;
                sourceFullPath = draggedFolderPath;
            } else if (draggedNodeIsLeaf                                   // move file
                    || (draggedNodeIsLeaf === false
                        && draggedNode.data.data_from_DB !== '')){
                sourceFullPath = draggedNode.data.data_from_DB['full_path'];
            }

            var destinationFullPath = null;
            // drop on folder node
            if(dropOverNode.isLeaf() === false){

                if (typeof dropOverNode.data.breadcrumbs === "undefined"){
                    // not necessary but here to be clear
                    // if attributes on shapefile the layer is a folder but without breadcrumbs
                    return true
                }

                if (dropPosition === 'append'){
                    destinationFullPath = dropOverNode.data.breadcrumbs+'/'+IMPACT.Utilities.Common.basename(sourceFullPath);
                }else{
                       destinationFullPath = destinationPath + IMPACT.Utilities.Common.basename(sourceFullPath)
                }
            }
            // drop on leaf node
            else if(dropOverNode.isLeaf()){
                destinationFullPath = destinationPath + IMPACT.Utilities.Common.basename(sourceFullPath);
            } else {
                return false
            }

            var target = me.fileStore.getRecordBy('full_path',destinationFullPath);
            if(target && target[0] && sourceFullPath !== destinationFullPath){
                alert('Target directory contains a file with same name, rename it first');
                return false;
            }

            if(sourceFullPath !== null && destinationFullPath !== null && sourceFullPath !== destinationFullPath){
                // Ask Confirmation
                if(draggedNode.editing){
                    alert('Stop editing before moving file');
                    return false
                }

                Ext.MessageBox.confirm({
                    title: 'Move confirmation',
                    msg: "Move files to: <br />"+destinationFullPath+ " ?",
                    buttons: Ext.Msg.YESNO,
                    fn: function(e){
                        if(e === 'yes'){
                            if(destinationFullPath.indexOf('undefined')===-1){
                                // remove layer from map
                                if(draggedNode.data.layer){
                                    me.map.removeLayer(draggedNode.data.layer);
                                }
                                draggedNode.cascadeBy(function (n) {
                                        if(n.data.layer && n.data.layer.get('type') === 'attributes'){
                                            //n.set('cls','x-tree-disabled');
                                            me.map.removeLayer(n.data.layer);
                                        }
                                    });
                                draggedNode.remove();

                                IMPACT.Utilities.LayerSettings.save_settings_from_DB('move', sourceFullPath, destinationFullPath);
                                IMPACT.Utilities.Requests.fileHandler('move', sourceFullPath, destinationFullPath);

                                //alert('sync settings store');
                            } else {
                                alert('Error in moving files');
                            }
                        }
                    }
                });
                // MessageBox is async; return false so as the layer is not moved; ImageStore will detect changes automatically
                return false
            }

        } //else  return true === drop ok

    },


    addLayerToMap: function(node){

        // ######  Create layer  ######
        var me = this;
        var newLayer;
        // if layer does not exists -> create (expired after timeout)
        if (!IMPACT.Utilities.OpenLayers.getLayerByFullPath(me.map,node.data.data_from_DB['full_path'])){
            var extension = node.data.data_from_DB['extension'];
            if(extension === '.shp'){

                newLayer = IMPACT.OpenLayers.Vector.createLayer(
                    node.data.data_from_DB,
                    node.data.settings_from_DB,
                    IMPACT.Utilities.OpenLayers.getTileSize(me.map)
                );

            } else if (extension === '.tif' || extension === '.vrt') {

                newLayer = IMPACT.OpenLayers.Raster.createLayer(
                    node.data.data_from_DB,
                    node.data.settings_from_DB,
                    IMPACT.Utilities.OpenLayers.getTileSize(me.map)
                );
            }

            //newLayer.setVisibility(true);  // set on creation
            me.map.addLayer(newLayer);
            node.set('layer', newLayer);

        } else {
            // set visibility
            node.data.layer.setVisible(true);
        }

        me.sortMapLayers();

    },

     sortMapLayers: function(){
        var me = this;

        var maxIndex = Ext.ComponentQuery.query('#LayerTreePanel')[0].getRootNode().childNodes[0].childNodes.length +100;
        var CSmaxIndex = me.getRootNode().childNodes[0].childNodes.length + maxIndex;  // 100 is arbitrary to account for baselayers + temporary etc. However
        if(me.getRootNode().childNodes[0].childNodes.length > 0){      // original val == 1 , change when adding baselayers

            var map = IMPACT.main_map;

            me.getRootNode().cascadeBy(function(n){
                if( (n.isLeaf() && n.data.layer && n.data.layer.get('isBaseLayer')==false && n.data.layer.get('type') !== 'wms')
                        || (n.isLeaf()==false && n.data.layer)  // vector folder
                        ){
//                            map.setLayerIndex(n.data.layer, index);
                            n.data.layer.setZIndex(CSmaxIndex);
                            CSmaxIndex -=1

                }
            })

            // Put "hidden" layers on top
            // (not necessary)
        }
    },

    removeLayerFromMap: function(node){
        // hide and remove after 10/50 seconds
        node.data.layer.setVisible(false);
    },

    __refresh: function(leafNode){
        console.log('Need to refresh');
        //leafNode.data.layer.redraw(true);
    }




 });