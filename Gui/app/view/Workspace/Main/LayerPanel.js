/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.LayerPanel', {
    requires: [
        'IMPACT.view.Workspace.Main.TreePanel',
        'IMPACT.view.Workspace.Main.CSTreePanel',
    ],
    extend: 'Ext.panel.Panel',
    alias: 'widget.MainLayerPanel',

    itemId: 'MainLayerPanel',

    //title: 'Layers',
    header: false,
    width: 300,

    split: true,
    collapsible: true,
    map: null,
    border: true,

    layout:{
        type:'vbox',
        align:'stretch'
    },


    items:[
            {
                xtype: 'panel',
                itemId:'Background',
                title: 'Background',
                border: true,
                collapsible: true,
                //flex:1,
                items:[
                ]

            },
            {
                xtype: 'CSLayerTreePanel',
                title: 'Remote Data Overlays',
                header: true,
                collapsible: true,
                autoScroll: true,
                height:70,
                flex: 20,
                resizable: true,
                resizeHandles: 's',

                listeners: {
                    single: true,
                    boxready: function() {
                        delete this.flex;
                    }
                }
            },
            {
                xtype: 'LayerTreePanel',
                title: 'User Overlays',
                header: true,
                collapsible: false,
                autoScroll: true,
                flex: 80
            }

    ],
    //    // --- link map into tree; not possible in initComponent since not yet ready --
    afterRender: function(){
        this.callParent();
        var me = this;
        me.map = IMPACT.main_map;
        var baseLayers = IMPACT.Utilities.OpenLayers.getBaseLayers(me.map);
        var base_item = [];
        var wmsLayers = IMPACT.Utilities.OpenLayers.getWMSLayers(me.map);
        var wms_item=[];

        for(var i=0; i<baseLayers.length; i++){
            base_item.push(
                {boxLabel: baseLayers[i].get('name'), name: 'baseLayers', inputValue: baseLayers[i], checked: baseLayers[i].isVisible()}
            )
        };

        for(var i=0; i<wmsLayers.length; i++){
            wms_item.push(
                {boxLabel: wmsLayers[i].get('name'), name: 'wms', inputValue: wmsLayers[i], checked: false, cls:'impact-wms-panel', listeners:{change: function(field){field.inputValue.setVisible(field.checked)}}  }
            )
        };



        me.down("#Background").add(
               {
                xtype: 'panel',
                itemId: 'wmslayerGroup',
                columns: 1,
                fontSize: 10,
                //style: 'margin-left: 1px; margin-top: 1px;',
                //style: 'margin-left: 1px; margin-top: 1px; fontSize: 10',
                cls: 'impact-wms-panel',
                //height: 130,
                border: false,
                items: [
                        {
                            xtype: 'fieldcontainer',
                            defaultType: 'checkboxfield',
                            cls: 'impact-wms-panel',
                            items: wms_item
                        }
                ]
            }
            ,
            {
                xtype: 'radiogroup',
                itemId: 'baselayerGroup',
                columns: 1,
               // style: 'margin-left: 5px; margin-top: 5px;',
                cls: 'impact-wms-panel',
                items: base_item,
                listeners: {
                    change: function (item,newValue,oldValue) {

                        newValue.baseLayers.setVisible(true);
                        oldValue.baseLayers.setVisible(false);
                        if (newValue.baseLayers.get('name') == 'Planet NICFI'){
                            if( Ext.ComponentQuery.query('#PlanetWindow').length == 0 ){
                                var PlanetWindow = Ext.create('IMPACT.view.Workspace.Main.component.PlanetWindow');
                                PlanetWindow.show();
                            } else {
                            if (Ext.ComponentQuery.query('#PlanetWindow')[0].hidden){
                                    Ext.ComponentQuery.query('#PlanetWindow')[0].show();
                            }
                        }
                        };

                        if (newValue.baseLayers.get('name') == 'Google Satellite' && (typeof IMPACT.SETTINGS['apiKey_options']['googleKey'] == 'undefined' || IMPACT.SETTINGS['apiKey_options']['googleKey'] == '')){
                            alert("Enter your Google API KEY in the Settings Tab to visualize the basemaps");
                        }
                        //me.map.setBaseLayer(newValue.baseLayers);

                    }
                }
            }
        );
    },


 });