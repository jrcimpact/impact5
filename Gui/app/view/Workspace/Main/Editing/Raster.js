/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.Raster', {
    extend: 'Ext.container.Container',

    itemId: 'EditingRaster',

    treeNode: null,

    editingMode: {
        active: false,
        layer: null,
    },

    previewLayer: null,

    items: [
        Ext.create('Ext.button.Button',{
            text: "Editing Tools",
            itemId: "EditingToolsButton",
            disabled: true,
            hidden: true,
            listeners: {
                click: function() {
                    Ext.ComponentQuery.query('#EditingOptionWindowRaster')[0].showHide();
                }
            }
        })
    ],


    startEditingMode: function(node){

        var me = this;
        me.treeNode = node;
        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0]; //me.up('#MainWorkspace');
        var map = IMPACT.main_map;
        // set Node settings
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', true, false);
        me.treeNode.set('cls','red-bold-node');
        me.treeNode.active_editing = true;


        // Activate editing MODE
        me.editingMode.active = true;
        me.editingMode.layer = layer;

        // Set editing flag to selected node
        //layer.params.editing = true;

        // Add selection layer to map

        me.previewLayer = IMPACT.OpenLayers.EditingRasterPreview.createLayer(
            me.treeNode.data.data_from_DB,
            IMPACT.Utilities.OpenLayers.getTileSize(map)
        );
        map.addLayer(me.previewLayer);
        me.previewLayer.setVisible(false);
        me.previewLayer.setZIndex(1000);


        // Enable editing options button & window
        //me.down('#EditingToolsButton').setDisabled(true);
        //me.down('#EditingToolsButton').setVisible(false);
        Ext.create('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.Raster', {
            layer: me.previewLayer,
            map: map
        }).show();


        Ext.data.StoreManager.lookup('ImageStore').on('raster_editing_layer_changed', function(record){
            if(record['ready'] === 'true' && record['full_path'] === me.treeNode.data.data_from_DB['full_path']){
                me.editingLayerChanged();
            }
        });

        //Ext.data.StoreManager.lookup('ImageStore').on('raster_editing_layer_changed', me.editingLayerChanged );
        //Ext.ComponentQuery.query('#LayerTreePanel')[0].on('raster_editing_layer_changed', function(){alert()} );


        // Disable workspace tools
        workspace.startEditingMode();
        Ext.ComponentQuery.query('#MainToolbar2')[0].toggleEditingMode(false);
    },

    stopEditingMode: function(node){

        var me = this;
        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = IMPACT.main_map;
        me.treeNode.set('cls','');

        //Ext.data.StoreManager.lookup('ImageStore').un('raster_editing_layer_changed', me.editingLayerChanged );
        Ext.ComponentQuery.query('#EditingOptionWindowRaster')[0].destroy();

        // Deactivate editing MODE
        me.editingMode.active = false;

        me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
        me.editingMode.layer.getSource().refresh();

        me.editingMode.layer = null;

        // Remove selection layer from map
        map.removeLayer(me.previewLayer);

        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', false, false);
        me.treeNode.active_editing = false;
        // Disable workspace tools
        workspace.stopEditingMode();

    },

    editingLayerChanged: function(){
        var me = this;
        var window = Ext.ComponentQuery.query('#EditingOptionWindowRaster')[0];

        if(typeof window !== "undefined"){
            if(window.__parseInputValues('save_to_new')=='No'){
                window.resetUI();
                window.initUI();
            } else {
                window.waitUI(false);
            }
            // redraw layer while in editing mode
            me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()}); // fake params to force reload
            me.editingMode.layer.getSource().refresh();

        }

    },

//    showPreview: function(band, min, max){
//        var me = this;
//        alert('Eccomi');
//        me.previewLayer.updateRange(band, min, max);
//    },
//
//    leftClick: function(evt){
//        alert('left click');
//        var me = this;
//        var recode_window = Ext.ComponentQuery.query('#EditingOptionWindowRaster')[0];
//        var map = IMPACT.main_map;
//        var lon = map.getLonLatFromPixel(evt.xy)['lon'];
//        var lat = map.getLonLatFromPixel(evt.xy)['lat'];
//
//        var additional_params = {
//            band: recode_window.down('#band').getValue()
//        };
//
//        IMPACT.Utilities.Requests.query_by_point(
//            [me.treeNode.data.data_from_DB['full_path']],
//            lon,
//            lat,
//            evt,
//            additional_params);
//    },
//
//    rightClick: function(e){
//
//        var me = this;
//        alert('right click.');
//    },

});