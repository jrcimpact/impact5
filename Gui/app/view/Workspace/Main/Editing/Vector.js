/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.Vector', {
    extend: 'Ext.container.Container',

    itemId: 'EditingVector',

    items: [
        Ext.create('Ext.button.Button',{
            text: "Editing Tools",
            itemId: "EditingToolsButton",
            disabled: true,
            hidden: true,
            listeners: {
                click: function() {
                    Ext.ComponentQuery.query('#EditingOptionWindowVector')[0].showHide();
                }
            }
        })
    ],

    treeNode: null,
    attributeNodes: null,

    selectionStore: new Ext.data.ArrayStore({
        autoDestroy: true,
        fields: ['id'],
        data : []
    }),

    selection_layer: null,
    drawFeature_layer: null,

//    selection_layer: new OpenLayers.Layer.Vector("editing_selection", {
//        buffer: 0,
//        displayInLayerSwitcher: false,
//        visible: true,
//        type: 'hidden',
//        style: {
//            fill: false,
//            strokeColor: "#ff0080",
//            strokeWidth: 2
//        }
//    }),

    initComponent: function () {
        var me = this;

        me.editingMode = {
            active: false,
            legendType: null,
            layer: null,
            leftClickMode: 'id',
            clusterItem: IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
            clusterValue:'',
            refreshLayer: false,

            selectOnlyActiveClass: false,
            ActiveClasses:'',
            NotActiveClasses: '',
            modeActiveClasses: 'on',
            selectOnlyActiveClass_Layer: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],


        };




//        me.selection_layer.set({'title': 'editing_selection_aoi'});
//        me.selection_layer.set({'name': 'editing_selection_aoi'});
//        me.selection_layer.set({'type': 'hidden'});
        // add Style





        me.backup = {
            active: false,
            ID: -1,
            max: 5,
            dateArray: new Array(5).fill('')
        };

        me.callParent();
    },


    draw_feature_change: function(){
               var me = Ext.ComponentQuery.query('#EditingVector')[0];

               if(me.drawFeature_layer && me.drawFeature_layer.getSource().getFeaturesCollection().getLength() >0){


                var format = new ol.format.WKT();
                var wkt_selection =  format.writeFeatures(me.drawFeature_layer.getSource().getFeatures(),
                                                   //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                                                   );

                me.drawFeature_layer.getSource().getFeaturesCollection().clear();
                me.selectPolygon(wkt_selection, false);
            }
    },

    startEditingMode: function(node){

        var me = this;
        me.backup.active=true;
        me.treeNode = node;
        me.attributeNodes = node.childNodes;
        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = IMPACT.main_map;



        // set Node settings
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', true, false);
        me.treeNode.set('cls','red-bold-node');
        me.treeNode.active_editing = true;

        for (var i in me.attributeNodes){
            // set default class visualiz
            me.attributeNodes[i].active_editing = true;
            me.attributeNodes[i].data.layer.getSource().updateParams({'selectOnlyActiveClass' : false});

        }

        // Activate editing MODE
        me.editingMode.active = true;
        me.editingMode.layer = layer;
        me.editingMode.legendType = me.treeNode.data.settings_from_DB['legendType'];

        me.editingMode.leftClickMode = 'id';
        me.editingMode.clusterItem = IMPACT.SETTINGS['vector_editing']['attributes']['ID'];
        // replicated on each layer as well but the global one is used on AOI drawing
        me.editingMode.classList = me.attributeNodes[0].data.layer.getSource().getParams()['classIDs'].split(';');
        me.editingMode.selectOnlyActiveClass = false;
        me.editingMode.ActiveClasses = me.attributeNodes[0].data.layer.getSource().getParams()['classIDs'];  // get list of class ID from one layer ; updated when changing legend visibility
        me.editingMode.NotActiveClass = '';  // get list of class ID from one layer ; updated when changing legend visibility
        me.editingMode.modeActiveClasses = 'on';
        //me.editingMode.selectOnlyActiveClass_Layer = IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']; // set by default




        var OptionWindow = Ext.create('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.Vector',{
            legendType: me.editingMode.legendType,
            treeNode: me.treeNode
        });

        OptionWindow.show();
        OptionWindow.toFront();


        Ext.create('IMPACT.view.Workspace.Main.Editing.component.RecodeTo', {
            editingContainerID: me.itemId,
            legendType: me.editingMode.legendType
        });
        // Add selection layer to map


        OptionWindow.down('#ColorPaletteVectorGrid').on('ColorPaletteVectorGrid_CheckChange', function(ids){
            me.showHideClass(ids);
        });

        me.on('undo_completed', function(){
            me.__refresh_after_undo();
        });



                // Enable editing options button & window in Toolbar
        Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
        me.down('#EditingToolsButton').setDisabled(false);
        me.down('#EditingToolsButton').setVisible(true);


         var source = new ol.source.Vector({
                wrapX: false,
                features: new ol.Collection(),
                params : {
                    buffer: 0,
                }
            });

         me.selection_layer = new ol.layer.Vector({

                title: "editing_selection",
                name: "editing_selection",

                source: source,
                visible: true,
                isBaseLayer: false,
                buffer: 0,
                type: 'hidden',

                style: new ol.style.Style({
                      stroke: new ol.style.Stroke({
                        color: '#ff0080',
                        width: 1.5
                      })
                    })
            });


        map.addLayer(me.selection_layer);
        me.selection_layer.setZIndex(1000);

       me.drawFeature_layer = IMPACT.Utilities.OpenLayers.getTemporaryLayer(IMPACT.main_map);
       me.drawFeature_layer.on('change', me.draw_feature_change);

       IMPACT.main_map.on('singleclick', me.leftClick);

       map.getViewport().addEventListener('contextmenu', function (evt) {
            evt.preventDefault();
            evt = evt || window.event;
            if (Ext.ComponentQuery.query('#RecodeTo')[0]){
                Ext.ComponentQuery.query('#RecodeTo')[0].showAt(parseInt(evt.clientX)-5, parseInt(evt.clientY)-5);
            }

       })

        // Disable workspace tools
        workspace.startEditingMode();
    },





    stopEditingMode: function(){

        var me = this;
        me.backup.active=false;
        if(me.editingMode.active == false){ return}   // after intruduction of REDDCopernicus, stop is in common, test to skip


        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = IMPACT.main_map;

        // Disable editing options button & window
        me.clearSelection();
        me.down('#EditingToolsButton').setDisabled(true);
        me.down('#EditingToolsButton').setVisible(false);
        Ext.ComponentQuery.query('#EditingOptionWindowVector')[0].destroy();
        Ext.ComponentQuery.query('#RecodeTo')[0].destroy();
        var DBF = Ext.ComponentQuery.query('#DbfAttributeTable')[0];
        if(typeof DBF !== "undefined"){
            DBF.destroy();
        }

        // Deactivate editing MODE
        me.editingMode.active = false;
        me.editingMode.layer = null;
        me.editingMode.legendType = null;

        // Remove selection layer from map
        map.removeLayer(me.selection_layer);

        for (var i in me.attributeNodes){
            // set default class visualiz
            me.attributeNodes[i].data.layer.getSource().updateParams({'classVisible' : me.attributeNodes[i].data.layer.getSource().getParams()['classIDs']});
            me.attributeNodes[i].data.layer.getSource().updateParams({'selectOnlyActiveClass' : false});


            //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.attributeNodes[i], 'editing', false, true);
            me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
            me.attributeNodes[i].data.layer.getSource().refresh();
            me.attributeNodes[i].active_editing = false;
        }
        me.treeNode.set('cls','');
        me.treeNode.active_editing = false;
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', false, false);



        Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
        //me.selection_layer.un('change', function (ft) {});
        me.drawFeature_layer.un('change', me.draw_feature_change);
        IMPACT.main_map.un('singleclick', me.leftClick);

        // Disable workspace tools
        workspace.stopEditingMode();

    },

    leftClick: function(evt){

        if (typeof evt == 'undefined'){ return}

        var CTRL_pressed = ol.events.condition.platformModifierKeyOnly(evt) || ol.events.condition.shiftKeyOnly(evt);

        var me =  Ext.ComponentQuery.query('#EditingVector')[0];
        //var lon = IMPACT.main_map.getCoordinateFromPixel(evt.coordinate)[0];
        //var lat = IMPACT.main_map.getCoordinateFromPixel(evt.coordinate)[1];
        var wkt = "POINT (" + evt.coordinate[0] + ' ' + evt.coordinate[1] + ")";

        Ext.ComponentQuery.query('#RecodeTo')[0].close(); // in case is open

        // #####  Show/Hide class  #####
        if(me.editingMode.leftClickMode == 'id'){

           // #####  Select by Polygon  #####
           if(me.editingMode.clusterItem == IMPACT.SETTINGS['vector_editing']['attributes']['ID']){
                me.selectPolygon(wkt, CTRL_pressed);
                if(me.editingMode.refreshLayer==true){
                    me.clearSelection();
                    me.editingMode.refreshLayer = false;
                }
           }
           // #####  Select by Class or other attribute  #####
           else {
                me.selectClass(wkt);
           }
        }

    },

//    rightClick: function(evt){
//        var me = this;
//        evt = evt || window.event;
//        Ext.ComponentQuery.query('#RecodeTo')[0].showAt(parseInt(evt.clientX)-5, parseInt(evt.clientY)-5);
//
//    },
    // called by the legen greipanel into Editing Window
    showHideClass: function(ids){
        var me = this;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = IMPACT.main_map;

        for (var i in me.attributeNodes){
            me.editingMode.ActiveClasses = ids.join(';');
            me.attributeNodes[i].data.layer.getSource().updateParams({'classVisible' : ids.join(';')});
            me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
            me.attributeNodes[i].data.layer.getSource().refresh();
        }

        var tmplist = [];
        for (var i in me.editingMode.classList){
           if(!ids.includes(me.editingMode.classList[i])){
                tmplist.push(me.editingMode.classList[i])
           }
        }
        me.editingMode.NotActiveClasses = tmplist.join(';');

    },

    selectPolygon: function(wkt, CTRL_pressed){
        var me = this;

        Ext.Ajax.request({
            url: 'retrieve_cluster_class_by_click.py',
            params: {
                "shp": me.editingMode.layer.getSource().getParams()['full_path'],
                "wkt" : wkt,
                "EPSG": '3857',
                "items" : JSON.stringify([IMPACT.SETTINGS['vector_editing']['attributes']['ID']]),
                "geom" : true,
                "selectOnlyActiveClass": me.editingMode.selectOnlyActiveClass,
                "selectOnlyActiveClass_Layer": me.editingMode.selectOnlyActiveClass_Layer,
                "activeClasses": me.editingMode.modeActiveClasses === 'on' ? me.editingMode.ActiveClasses : me.editingMode.NotActiveClasses

            },
            success: function(response){
                var responseJSON = JSON.parse(response.responseText);

                if(!CTRL_pressed){
                    // Empty prev selection if not CTRL
                    me.selection_layer.getSource().getFeaturesCollection().clear();
//                    me.selection_layer.removeAllFeatures({ silent: true });
//                    me.selection_layer.destroyFeatures({ silent: true });
                    me.selectionStore.removeAll();
                    // None selected
                    if (responseJSON.length == 0){
                        return;
                    }
                }

                var ids = '';
                responseJSON.forEach(function(item){
                    var ID = item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']];
                    ids +=  ID+',';
                    if (CTRL_pressed){
                        var store_index = me.selectionStore.findExact('id', ID);
                        // Add to store if not already selected
                        if(store_index == -1){
                            me.__addSelectedItemToStore(item);
                            Ext.ComponentQuery.query('#clearSelectionButton')[0].addCls('highlightButton');
                        }
                        // Remove from store if already selected
                        else {

                            me.selection_layer.getSource().removeFeature(me.selection_layer.getSource().getFeatureById(ID));
                            me.selectionStore.removeAt(store_index);
                            if(me.selectionStore.getCount()==0) {
                                Ext.ComponentQuery.query('#clearSelectionButton')[0].removeCls('highlightButton');
                            }
                        }
                    } else {
                        me.__addSelectedItemToStore(item);
                        Ext.ComponentQuery.query('#clearSelectionButton')[0].addCls('highlightButton');
                    }
                });


                if (me.editingMode.refreshLayer){
                    alert('refresh map needed ?');
                    me.editingMode.layer.getSource().updateParams({'ids' : ids});
                    me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                    me.editingMode.layer.getSource().refresh();
                }

                //me.selection_layer.addFeature() //.refresh();
            },
            failure : function(response){
                //alert('error');
                console.log('ERROR retrieving cluster or ID');
                console.log(response);
            }
        });
    },

    __goToFeature: function(prevNext){
        var me = this;
        var currentID = me.selectionStore.getAt(me.selectionStore.getCount()-1).data.id;

        Ext.Ajax.request({
            url: 'goToPrevNextFeature.py',
            params: {
                "shp": me.editingMode.layer.getSource().getParams()['full_path'],
                "items" : JSON.stringify([IMPACT.SETTINGS['vector_editing']['attributes']['ID']]),
                "currFeatureID" : currentID,
                "direction" : prevNext,
                "geom" : true
            },
            success: function(response){
                var responseJSON = JSON.parse(response.responseText);
                if (responseJSON === null || typeof responseJSON[0][IMPACT.SETTINGS['vector_editing']['attributes']['ID']] === 'undefined'){
                        if (prevNext == 'prev'){alert("Begin")} else {alert("End")}
                        return;
                }

                    // Empty prev selection if not CTRL
                  me.selection_layer.getSource().getFeaturesCollection().clear();
//                me.selection_layer.removeAllFeatures({ silent: true });
//                me.selection_layer.destroyFeatures({ silent: true });
//                me.selectionStore.removeAll();
                    // None selected



                var ids = '';
                var item = responseJSON[0];
                    // only 1
                    //var ID = item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']];
                me.__addSelectedItemToStore(item, true);

                var dbfGridpanel = Ext.ComponentQuery.query('#dbfGridpanel')[0];
                if(typeof dbfGridpanel !== "undefined"){
                    var selectionModel = dbfGridpanel.getSelectionModel();
                    var store = dbfGridpanel.getStore();
                    var ID = store.findRecord(IMPACT.SETTINGS['vector_editing']['attributes']['ID'], item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']]);
                    // test if in store, if not is on other page
                    if (ID !== null){
                        selectionModel.select(ID);
                    }
                }


            },
            failure : function(response){
                //alert('error');
                console.log(response);
            }
        });

    },

    __addSelectedItemToStore: function(item, panTo){
        var me = this;
        var ID = item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']];


        var format = new ol.format.WKT();
        var Geom = item['geom'];
        if(Geom.includes("POINT")){
            if (Geom.split(' ').length > 3){
                //Geom = Geom.replace('POINTS', 'POINT');
                // in case of 3D points
                Geom = 'POINT '+Geom.split(' ')[1]+' '+ Geom.split(' ')[2]+')'
            }
        };
        var feat =  format.readFeature(Geom,
                                   //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                                   );
        feat.setProperties({'id': ID })
        feat.setId(ID);  // used by getFeatureById in selection + ctrl
        me.selectionStore.loadData([[ID]], true);



        // add for click ---- OL10
        var DBF = Ext.ComponentQuery.query('#DbfAttributeTable')[0];
        if(typeof DBF == "undefined"){
            //  add feature to layer
            me.selection_layer.getSource().addFeature( feat );

        }


        // todo __addSelectedItemToStore is used by the DBF editor - conflict with Vector editing - adding if but need to be fixed
        if(typeof Ext.ComponentQuery.query('#NextFeatId')[0] !== 'undefined'){
            Ext.ComponentQuery.query('#NextFeatId')[0].enable();
            Ext.ComponentQuery.query('#PrevFeatId')[0].enable();

            Ext.ComponentQuery.query('#LastselectedID')[0].setValue(ID);
            Ext.ComponentQuery.query('#clearSelectionButton')[0].addCls('highlightButton');
        }

        if (panTo){
            // case of Next or Prev button , pan to feat on change
            var bbox =  format.readGeometry(Geom,
                           //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                           ).getExtent();
            IMPACT.main_map.getView().setCenter(ol.extent.getCenter(bbox));
        }


        if(typeof Ext.ComponentQuery.query('#GEEWindow')[0] !== 'undefined'){
            var LonLat = ol.extent.getCenter(feat.getGeometry().getExtent());
            //console.log(LonLat);
            //LonLat.transform(IMPACT.SETTINGS['WGS84_mercator'], IMPACT.SETTINGS['WGS84']);
            //console.log(LonLat);
//            Ext.ComponentQuery.query('#GEEWindow')[0].fireEvent('refreshFromShapefileClick', LonLat[0], LonLat[1], me.editingMode.layer.getSource().getParams()['full_path']);
            Ext.ComponentQuery.query('#GEEWindow')[0].fireEvent('refreshFromShapefileClick', LonLat[0], LonLat[1], null);
        }

    },

    selectClass: function(wkt){
        var me = this;
         // Empty prev selection
        me.selection_layer.getSource().getFeaturesCollection().clear()
//        removeAllFeatures({ silent: true });
//        me.selection_layer.destroyFeatures({ silent: true });
        me.selectionStore.removeAll();

        Ext.ComponentQuery.query('#NextFeatId')[0].disable();
        Ext.ComponentQuery.query('#PrevFeatId')[0].disable();

        Ext.Ajax.request({
            url: 'retrieve_cluster_class_by_click.py',
            params: {
                "shp": me.editingMode.layer.getSource().getParams()['full_path'],
                "wkt" : wkt,
                "items" : JSON.stringify([me.editingMode.clusterItem]),
                "EPSG": '3857',
                "selectOnlyActiveClass": me.editingMode.selectOnlyActiveClass,
                "selectOnlyActiveClass_Layer": me.editingMode.selectOnlyActiveClass_Layer,
                "activeClasses": me.editingMode.modeActiveClasses === 'on' ? me.editingMode.ActiveClasses : me.editingMode.NotActiveClasses
            },
            success: function(response){
                if (response.responseText == '[]'){
                    me.editingMode.clusterValue = '';
                } else{
                    var responseJSON = JSON.parse(response.responseText)[0];
                    me.editingMode.clusterValue = responseJSON[me.editingMode.clusterItem];
                }


                if (typeof(me.editingMode.clusterValue) === 'undefined' || me.editingMode.clusterValue == '' ) {

                      me.clearSelection();

                } else{
                    me.editingMode.clusterValue = me.editingMode.clusterValue == '' ? "\"\"" : me.editingMode.clusterValue;

                    for (var i in me.attributeNodes){
                        me.attributeNodes[i].data.layer.getSource().updateParams({'show_outline_on_NotActiveClass' : me.editingMode.modeActiveClasses === 'on' ? '' : me.editingMode.NotActiveClasses });
                        me.attributeNodes[i].data.layer.getSource().updateParams({'clusterItem' : me.editingMode.clusterItem});
                        me.attributeNodes[i].data.layer.getSource().updateParams({'show_outline_on_selection' : me.editingMode.clusterValue});
                        me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                        me.attributeNodes[i].data.layer.getSource().refresh();
                    }
                    Ext.ComponentQuery.query('#clearSelectionButton')[0].addCls('highlightButton');
                    Ext.ComponentQuery.query('#NextFeatId')[0].disable();
                    Ext.ComponentQuery.query('#PrevFeatId')[0].disable();
                    Ext.ComponentQuery.query('#LastselectedVAL')[0].setValue(me.editingMode.clusterValue);

                }


            },
            failure : function(response){
                alert('error retrieving cluster');
                console.log(response);
            }
        });
    },

    clearSelection: function(){
        var me = this;
        // clean editing store; not necessary but ... better to clean in cas of pending items
        me.selectionStore.removeAll();
//        me.selection_layer.removeAllFeatures({ silent: true });
//        me.selection_layer.destroyFeatures({ silent: true });
        me.selection_layer.getSource().getFeaturesCollection().clear();

        Ext.ComponentQuery.query('#clearSelectionButton')[0].removeCls('highlightButton');
        Ext.ComponentQuery.query('#NextFeatId')[0].disable();
        Ext.ComponentQuery.query('#PrevFeatId')[0].disable();

        Ext.ComponentQuery.query('#LastselectedID')[0].setValue('-');
        Ext.ComponentQuery.query('#LastselectedVAL')[0].setValue('-');

        var DBF = Ext.ComponentQuery.query('#DbfAttributeTable')[0];
        if(typeof DBF !== "undefined"){
            DBF.down('#dbfGridpanel').getSelectionModel().deselectAll();
            DBF.selection_layer.getSource().clear();
        }

        me.editingMode.clusterValue = '';

        for (var i in me.attributeNodes){
            // redraw only if NOT POLYGON SELECTION
            if(me.attributeNodes[i].data.layer.getSource().getParams()['show_outline_on_selection'] != '---'){

                me.attributeNodes[i].data.layer.getSource().updateParams({'clusterItem' : ''});
                me.attributeNodes[i].data.layer.getSource().updateParams({'show_outline_on_selection' : '---'});
                me.attributeNodes[i].data.layer.getSource().updateParams({'ids' : ''});
                me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                me.attributeNodes[i].data.layer.getSource().refresh();
            }
        }
    },

    __activateUndo: function () {
        var me = this;
        me.backup.ID = -1;
        me.backup.dateArray.fill('');
        if (!me.backup.active){
            me.backup.active = true;
            Ext.ComponentQuery.query('#NextUndoId')[0].enable();
            Ext.ComponentQuery.query('#PrevUndoId')[0].enable();
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
            Ext.ComponentQuery.query('#UndoItemId')[0].setText('Disable Undo');

        } else {
            me.backup.active = false;
            Ext.ComponentQuery.query('#NextUndoId')[0].disable();
            Ext.ComponentQuery.query('#PrevUndoId')[0].disable();
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
            Ext.ComponentQuery.query('#UndoItemId')[0].setText('Enable Undo');
        }

    },

    __Undo: function(direction) {
        var me = this;
        me.disable();
        var destinationFullPath = me.editingMode.layer.getSource().getParams()['full_path'].replace('.shp','.dbf');

        if (direction == 'next') {
            if(me.backup.ID == me.backup.max-1 || me.backup.dateArray[me.backup.ID+1] == '') {
                alert('Last');
                return
            }

            me.backup.ID +=1;
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
            var sourceName = me.editingMode.layer.get('name') + '_' + me.backup.dateArray[me.backup.ID]+'.dbf'
        };

        if (direction == 'prev') {
            if(me.backup.ID == -1 ){
                alert('First');
                return
            }
            var sourceName = me.editingMode.layer.get('name') + '_' + me.backup.dateArray[me.backup.ID]+'.dbf'
            me.backup.ID -=1;
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);

        };

//        REQUEST is ASYNC so __refresh_after_undo is fired inside
        var res = IMPACT.Utilities.Requests.fileHandler('undo', sourceName, destinationFullPath);
//
//        for (var i in me.attributeNodes){
//                            me.attributeNodes[i].data.layer.redraw(true);
//                    }
    },

     __refresh_after_undo: function(result){
        me = this;
        for(var i in me.attributeNodes){
                    me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                    me.attributeNodes[i].data.layer.getSource().refresh();
            }
        me.enable();

    },

    recodeTo: function(saveValue){

        var me = this;
        var map = IMPACT.main_map;

        if (me.editingMode.active.isON == false || saveValue == null){
            return;
        }

        if (me.backup.active){

            if (me.backup.ID+1 < me.backup.max){
                me.backup.ID += 1;                //   Increment undo ID
                me.backup.dateArray[me.backup.ID] = String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s'));
            } else {
                me.backup.dateArray.shift();
                me.backup.dateArray.push(String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s')));
            }
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
//            console.log(me.backup.ID);
//            console.log(me.backup.dateArray);
        }



        var tmpCode="temp_proj:"+Date.now().toString();
        proj4.defs(tmpCode,me.editingMode.layer.get('PROJ4'));
        ol.proj.proj4.register(proj4);

        //proj4.defs["temp_proj"] = me.editingMode.layer.data_from_DB['PROJ4'];

        // collecting params
        var optionWindow = Ext.ComponentQuery.query('#EditingOptionWindowVector')[0];
        var params = {
            selectFrom: optionWindow.down('#selectionMode').getChecked()[0].inputValue,
            saveTo: optionWindow.down('#SaveAttribute').getValue(),
            saveValue: saveValue,
            extent: optionWindow.down('#vectorRecodeExtent').getChecked()[0].inputValue,
            };


        // ----------      empty extent to recode entire shapefile  ----------
        var extent = '';

        if (params.extent != 'full'){

            var layer_projection = new ol.proj.Projection({code:tmpCode});
            mapExtent = map.getView().calculateExtent(map.getSize());
            extent = ol.proj.transformExtent(mapExtent, IMPACT.SETTINGS['WGS84_mercator'], layer_projection);
        }


        // -------------------------   SELECTION STORE is NOT EMPTY -------------------------------
        // selection by ID, poly or DBF
        // ----------------------------------------------------------------------------------------
        var list_ids=[];
        Ext.each(me.selectionStore.data.items, function(value){
            list_ids.push(value.data.id)
        });

        // -----------------------------------------------------------------------------------------
        // SUB selection on visible classes only
        // -----------------------------------------------------------------------------------------
//        var activeClasses = ''
//        if (me.editingMode.selectOnlyActiveClass){
//
//        }

        var newParams = {
                "shp_name": me.editingMode.layer.getSource().getParams()['full_path'],
                "extent" : JSON.stringify(extent),
                "saveValue": params.saveValue,
                "saveTo": params.saveTo,
                "whereItem": me.editingMode.clusterItem,
                "whereValue": me.editingMode.clusterValue,

                "activeClasses": me.editingMode.modeActiveClasses === 'on' ? me.editingMode.ActiveClasses : me.editingMode.NotActiveClasses,
                "selectOnlyActiveClass": me.editingMode.selectOnlyActiveClass,
                "selectOnlyActiveClass_Layer": me.editingMode.selectOnlyActiveClass_Layer,


                "list_ids": JSON.stringify(list_ids),
                "fid": IMPACT.SETTINGS['vector_editing']['attributes']['ID'],

                "backupID": me.backup.active ? me.backup.dateArray[me.backup.ID] : '',
                "bkupIsActive" : me.backup.active ? 'True' : 'False'


        };

        Ext.Ajax.request({
            url: 'save_info_on_shapefile.py',
            timeout: 1200000,
            params: newParams,
            success: function(response){
                var responseText = response.responseText.replace(/(\r\n|\n|\r)/gm,"");
                if(responseText == "-1"){
                    alert("Recode failed")
                } else{
                    for (var i in me.attributeNodes){
                            // refresh only visible?  not safe in case info is saved on OFF layer
                                me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                                me.attributeNodes[i].data.layer.getSource().refresh();


                    }

                    var dbfGridpanel = Ext.ComponentQuery.query('#dbfGridpanel')[0];
                    if(typeof dbfGridpanel !== "undefined"){
                        var selectionModel = dbfGridpanel.getSelectionModel();
                        var selection = selectionModel.getSelection();

                        var rows = [];
                        var store = dbfGridpanel.getStore();
                        for(var i = 0, len = selection.length; i < len; i++){
                            var ID = selection[i].get(IMPACT.SETTINGS['vector_editing']['attributes']['ID']);
                            rows.push(ID);
                        }
                        dbfGridpanel.getStore().reload({

                        });

                    }
               }

            },
            failure : function(response){
                alert('error');
                console.log(response);
            }
        });


    },

    mergeFeatures: function(){

        var me = this;
        var map = IMPACT.main_map;

        if (me.editingMode.active.isON == false){
            return;
        }

        if (me.backup.active){

            if (me.backup.ID+1 < me.backup.max){
                me.backup.ID += 1;                //   Increment undo ID
                me.backup.dateArray[me.backup.ID] = String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s'));
            } else {
                me.backup.dateArray.shift();
                me.backup.dateArray.push(String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s')));
            }
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
        }


        //proj4.defs["temp_proj"] = me.editingMode.layer.data_from_DB['PROJ4'];

        // collecting params
        var optionWindow = Ext.ComponentQuery.query('#EditingOptionWindowVector')[0];

        // -------------------------   SELECTION STORE is NOT EMPTY -------------------------------
        // selection by ID, poly or DBF
        // ----------------------------------------------------------------------------------------
        var list_ids=[];
        Ext.each(me.selectionStore.data.items, function(value){
            list_ids.push(value.data.id)
        });


        var newParams = {
                "shp_name": me.editingMode.layer.getSource().getParams()['full_path'],
                "list_ids": JSON.stringify(list_ids),
                "fid": IMPACT.SETTINGS['vector_editing']['attributes']['ID'],

                "backupID": me.backup.active ? me.backup.dateArray[me.backup.ID] : '',
                "bkupIsActive" : me.backup.active ? 'True' : 'False'

        };

        Ext.Ajax.request({
            url: 'dissolve_features.py',
            timeout: 1200000,
            params: newParams,
            success: function(response){
                var responseText = response.responseText.replace(/(\r\n|\n|\r)/gm,"");
                if(responseText == "-1"){
                    alert("Recode failed")
                } else{
                    for (var i in me.attributeNodes){
                            me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                            me.attributeNodes[i].data.layer.getSource().refresh();
                    }

               }

            },
            failure : function(response){
                alert('error');
                console.log(response);
            }
        });
    },

    CopyColumn: function(InCol, OutCol){

        var me = this;
        var map = IMPACT.main_map;

        if (InCol == OutCol){
            alert('Same columns');
            return
        }

        if (me.editingMode.active.isON == false){
            return;
        }

        if (me.backup.active){

            if (me.backup.ID+1 < me.backup.max){
                me.backup.ID += 1;                //   Increment undo ID
                me.backup.dateArray[me.backup.ID] = String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s'));
            } else {
                me.backup.dateArray.shift();
                me.backup.dateArray.push(String(Ext.Date.format(new Date(), 'Y-m-d-h-i-s')));
            }
            Ext.ComponentQuery.query('#LastUndo')[0].setValue(me.backup.ID);
        }


        //proj4.defs["temp_proj"] = me.editingMode.layer.data_from_DB['PROJ4'];

        // collecting params
        var optionWindow = Ext.ComponentQuery.query('#EditingOptionWindowVector')[0];

        // -------------------------   SELECTION STORE is NOT EMPTY -------------------------------



        var newParams = {
                "data_path": me.editingMode.layer.getSource().getParams()['full_path'],
                "infield": InCol,
                "outfield": OutCol,

                "backupID": me.backup.active ? me.backup.dateArray[me.backup.ID] : '',
                "bkupIsActive" : me.backup.active ? 'True' : 'False'

        };

        Ext.Ajax.request({
            url: 'copyDBFfields.py',
            timeout: 1200000,
            params: newParams,
            success: function(response){
                var responseText = response.responseText.replace(/(\r\n|\n|\r)/gm,"");
                if(responseText == "-1"){
                    alert("Recode failed")
                } else{
                    for (var i in me.attributeNodes){
                            me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                            me.attributeNodes[i].data.layer.getSource().refresh();
                    }
                    Ext.MessageBox.confirm({
                        title: 'Copy DBF',
                        msg: "Done",
                        buttons: Ext.Msg.CANCEL,
                        buttonText: {
                            cancel: 'Close'
                        },
                        })

               }

            },
            failure : function(response){
                alert('Error while saving attributes, See console.log');
                console.log(response);
            }
        });
    }



});