/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.component.RecodeTo', {
    extend: 'Ext.window.Window',

    requires: ['IMPACT.view.component.ColorPalette.Vector.Grid'],

    itemId: 'RecodeTo',
    config: {
        legendType: null,
        editingContainerID: null
    },

    closable : false,
    header: false,
    collapsible: false,
    maximizable: false,
    constrain: true,
    layout: 'fit',
    width: 400,
    closeAction: 'hide',

    maxHeight: 600,
    overflowY: 'scroll',
    timer:0,

    initComponent: function(){
        var me = this;
        me.callParent();
        me.add({
            xtype: 'ColorPaletteVectorGrid',
            legendType: me.legendType,
            listeners: {
                "itemclick": function (grid, row) {
                    Ext.ComponentQuery.query('#'+me.editingContainerID)[0].recodeTo(row.data.class);
                    me.close();
                }
            }
        });
    },

    showAt: function(x, y){
        var me = this;
        clearTimeout(me.timer);
        me.callParent(arguments);
        me.timer = setTimeout(function(){
            me.close();
        }, 15000);

    }

});