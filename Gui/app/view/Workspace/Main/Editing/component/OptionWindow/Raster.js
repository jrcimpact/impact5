/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.Raster', {
    extend: 'IMPACT.view.component.Window',

    requires: [
        'IMPACT.view.Processing.Window.component.ValidationMsg',
        'IMPACT.Utilities.Input'
    ],

    itemId: 'EditingOptionWindowRaster',

    resizable: false,
    layout: 'fit',
    bodyStyle: 'padding: 0;',
    width: 500,

    title: 'Raster Editing Tools',
    items: [{
        xtype: 'panel',
        itemId: 'EditingOptionWindowRasterPanel',
        border: false,
        items: []
    }],


    layer: null,
    map: null,
    data_type: null,            // int or float
    unique_values: null,
    recode_mode: 'manual',      // manual or preFilled
    num_unique_values: 0,
    num_unique_values_threshold: 50,
    errorMsg: [],

    previewLayer: null,

    buttons: [
        {
            text: 'Stop Editing',
            handler: function(){
                 this.up('window').close();

            }
        },
        {
            text: 'Recode',
            itemId: 'recode_button',
            disabled: true,
            handler: function(){
                this.up('window').__launchRecode();
            }
        }
    ],

    close: function(){
//        this.superclass.hide();
        this.hide();
        Ext.ComponentQuery.query('#EditingRaster')[0].stopEditingMode();
    },

    /**
     *  Initialize component
     */
    initComponent: function(){
        var me = this;

        // Initialize recode mode
        me.data_type = me.layer.get('data_from_DB')['data_type'];
        if(me.data_type=='Unknown'
                || me.data_type=='Float32'
                || me.data_type=='Float64'
                || me.data_type=='CFloat32'
                || me.data_type=='CFloat64'){
            me.data_type = 'float';
        } else {
            me.data_type = 'int';
        }

        me.callParent();

        me.initUI();
        me.previewLayer = Ext.ComponentQuery.query('#EditingRaster')[0].previewLayer;

    },

    /**
     * Set UI to initial status
     */
    initUI: function(){
        var me = this;
        var panel = me.down('#EditingOptionWindowRasterPanel');
        panel.removeAll();
        panel.add({
            itemId: 'BandContainer',
            bodyStyle: 'padding: 3px 5px;',
            border: false,
            items: me.__bandsSelection()
        });

        panel.add({
            itemId: 'RecodeValuesContainer',
            title: 'Recode values',
            bodyStyle: 'padding: 3px 5px;',
            hidden: true,
            maxHeight: 300,
            overflowY: 'scroll',
            overflowX: 'hidden',
            items: [{
                bodyStyle: 'padding: 3px 5px;',
                border: false,
                html: 'hello'
            }],
            dockedItems: [{
                xtype: 'toolbar',
                itemId: 'switch_recode_mode',
                dock: 'top',
                items: [
                    '->',
                    {
                        text: 'Use unique values',
                        itemId: 'switch_to_unique_values',
                        handler: function(){
                            if(me.num_unique_values==0){
                                Ext.Msg.show({
                                    title: 'Warning',
                                    msg: 'Too many unique values!',
                                    buttons: Ext.Msg.OK
                                });
                                return;
                            }
                            me.switchMode('preFilled');
                        }
                    },
                    {
                        text: 'Set values manually',
                        itemId: 'switch_to_manual',
                        handler: function(){
                            me.switchMode('manual');
                        },
                        hidden: true
                    }
                ]
            }],
        });

         panel.add({
            itemId: 'RecodeOptionsContainer',
            title: 'Options',
            bodyStyle: 'padding: 3px 5px;',
            hidden: true,
            items: [
                {   xtype: 'radiogroup',
                    itemId: 'rasterRecodeExtent',
                    fieldLabel: '<b>Recoding area<b/>',
                    labelWidth: 160,
                    width: 460,
                    style: 'margin-bottom: 10px;',
                    items: [
                        {boxLabel: 'Entire image ', name: 'rrecodeMode', inputValue: 'No', checked: true},
                        {boxLabel: 'Current view', name: 'rrecodeMode', inputValue: 'Yes', checked: false}

                    ]
                },
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Recode only within AOI </b>',
                    labelWidth:200,
                    width: 400,
                    radioName: 'recodeAOI',
                    defaultValue: "No",
//                    listeners: {
//                        change : function(value,newValue) {
//                            if(newValue['recodeAOI_val']=='Yes'){
//                                Ext.ComponentQuery.query('#MainToolbar')[0].toggleEditingMode(false);
//                                //Ext.ComponentQuery.query('#queryModeButton')[0].setDisabled(true);
//                                //Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
//                            }else{
//                                Ext.ComponentQuery.query('#MainToolbar')[0].toggleEditingMode(true);
//                                //Ext.ComponentQuery.query('#queryModeButton')[0].setDisabled(true);
//                            }
//                            }
//                    }

                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Save recoded band to new file</b>',
                    labelWidth:200,
                    width: 400,
                    radioName: 'save_to_new',
                    defaultValue:"Yes",
                    listeners: {
                        change : function(radio, newValue) {
                            if(newValue['save_to_new_val']=='Yes'){
                                this.nextSibling('#out_suffix').setVisible(true);
                            } else {
                                this.nextSibling('#out_suffix').setValue('');
                                this.nextSibling('#out_suffix').setVisible(false);
                            }
                        }
                    }
                }),
                 Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_suffix',
                    fieldLabel: 'Output filename Suffix',
                    hidden: false,
                    listeners: {
                        change: function(item){
                            item.setAsMissing();
                        }
                    },
                    setAsMissing: function(){
                        if (this.value.match(/^[0-9a-zA-Z_-]+$/)===null){
                            this.setFieldStyle('background: pink; border-color: Red');
                        } else {
                            this.setFieldStyle('background: white; border-color: #B5B8C8');
                        }
                     }
                }),

            ],
        });

        panel.add({
            itemId: 'WaitContainer',
            bodyStyle: 'padding: 5px 10px; font-size: 1.2em; font-weight: bold;',
            border: false,
            hidden: true,
            html: 'Recoding ...'
        });
    },

    /**
      * Clean UI
      */
    resetUI: function(){
        var me = this;
        var recodeValuesContainer = me.down('#RecodeValuesContainer');
        recodeValuesContainer.removeAll();
        recodeValuesContainer.setVisible(false);
        me.down('#recode_button').disable();
        me.down('#RecodeOptionsContainer').setVisible(false);
    },

    /**
     * Set UI in "waiting" mode
     */
    waitUI: function(toggle){
        var me = this;
        toggle = typeof toggle !== "undefined" ? toggle : true
        if(toggle){
            me.down('#BandContainer').disable();
            me.down('#RecodeValuesContainer').disable();
            me.down('#recode_button').disable();
            me.down('#RecodeOptionsContainer').setVisible(false);
            me.down('#WaitContainer').setVisible(true);
        } else {
            me.down('#BandContainer').enable();
            me.down('#RecodeValuesContainer').enable();
            me.down('#recode_button').enable();
            me.down('#RecodeOptionsContainer').setVisible(true);
            me.down('#WaitContainer').setVisible(false);
        }
    },

    switchMode: function(mode){
        var me = this;
        if(mode=='manual'){
            me.recode_mode = 'manual';
            me.down('#switch_to_unique_values').setVisible(true);
            me.down('#switch_to_manual').setVisible(false);
        } else if(mode=='preFilled'){
            me.recode_mode = 'preFilled';
            me.down('#switch_to_unique_values').setVisible(false);
            me.down('#switch_to_manual').setVisible(true);
        }
        me.resetUI();
        me.setRecodeUI();
        me.previewLayer.setVisible(false);
        me.previewLayer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
        me.previewLayer.getSource().refresh();

    },

    /**
     *  Set recoding UI
     */
    setRecodeUI: function(){
        var me = this;
        var recodeValuesContainer = me.down('#RecodeValuesContainer');

        // Force manual recoding mode
        if(me.num_unique_values==0 || me.num_unique_values>me.num_unique_values_threshold){
            me.recode_mode = 'manual';
        }

        // ### Manual recoding mode UI ###
        if(me.recode_mode=='manual'){
            recodeValuesContainer.add({
                xtype: 'panel',
                collapsible: false,
                border: false,
                layout:'column',
                style: 'margin: 5px 0;',
                items: [
                    {
                        html: 'from [>=]',
                        border: false,
                        width: 75,
                        bodyStyle: 'text-align: center; font-weight: bold;'
                    },
                    {
                        html: 'to [<=]',
                        border: false,
                        width: 75,
                        bodyStyle: 'text-align: center; font-weight: bold;'
                    },
                    {
                        html: '-->',
                        border: false,
                        width: 60,
                        bodyStyle: 'text-align: center;'
                    },
                    {
                        html: 'recode to',
                        border: false,
                        width: 75,
                        bodyStyle: 'text-align: center; font-weight: bold;'
                    }
                ]}
            );

            recodeValuesContainer.add( me.__manualRecodeSelection() );
            recodeValuesContainer.setVisible(true);
            me.down('#recode_button').enable();
            me.down('#RecodeOptionsContainer').setVisible(true);
        }

        // ### Pre-filled recoding mode UI ###
        else if(me.recode_mode=='preFilled'){
            recodeValuesContainer.add({
                xtype: 'panel',
                collapsible: false,
                border: false,
                layout:'column',
                style: 'margin: 5px 0;',
                items: [
                    {
                        html: '&nbsp;',
                        border: false,
                        width: 30
                    },
                    {
                        html: 'value',
                        border: false,
                        width: 60,
                        bodyStyle: 'text-align: center; font-weight: bold;'
                    },
                    {
                        html: '-->',
                        border: false,
                        width: 60,
                        bodyStyle: 'text-align: center;'
                    },
                    {
                        html: 'recode to',
                        border: false,
                        width: 75,
                        bodyStyle: 'text-align: center; font-weight: bold;'
                    }
                ]}
            );
            Ext.each(me.unique_values, function (item) {
                recodeValuesContainer.add(me.__preFilledRecodeSelection(item));
            });
            recodeValuesContainer.setVisible(true);
            me.down('#recode_button').enable();
            me.down('#RecodeOptionsContainer').setVisible(true);
        }
    },

    /**
     *  Raster's band selection
     */
    __bandsSelection: function (){
        var me = this;
        return [
            Ext.create('Ext.form.field.ComboBox', {
                queryMode: 'local',
                editable: false,
                labelWidth: 100,
                width: 150,
                fieldLabel: 'Band to recode',
                labelStyle: 'font-weight: bold;',
                itemId: 'band',
                style: 'margin-top: 3px;',
                store: Array.apply(0, Array(me.layer.get('data_from_DB')['num_bands']))
                        .map(function (element, index) {
                          return index + 1;
                      }),
                value: '',
                listeners: {
                    change: function(combo, newValue, oldValue){
                        if(newValue!=0 && newValue!='' && newValue!=null){
                            var statistics = Ext.JSON.decode(me.layer.get('data_from_DB')['statistics']);
                            var band_statistics = statistics[newValue-1];
                            var html = '<span class="fa fa-lg fa-blue fa-info-circle"></span>&nbsp;<b>Band statistics</b>: ';
                            var num_decimals = me.data_type=='int' ? 0 : 3;
                            me.num_unique_values = Ext.JSON.decode(me.layer.get('data_from_DB')['num_unique_values'])[newValue-1];
                            html += ' <i>Min:</i> '+band_statistics[0].toFixed(num_decimals).toString();
                            html += ' <i>Max:</i> '+band_statistics[1].toFixed(num_decimals).toString();
                            html += ' <i>Mean:</i> '+band_statistics[2].toFixed(3).toString();
                            html += ' <i>Std dev:</i> '+band_statistics[3].toFixed(3).toString();
                            combo.nextSibling('#band_statistics').update(html);

                            if(me.data_type=='int'){
                                var html = 0;
                                if(me.num_unique_values!=0){
                                    html = '<span class="fa fa-lg fa-blue fa-info-circle"></span>&nbsp;<b>N. of unique values</b>: '+ me.num_unique_values;
                                }
                                combo.nextSibling('#num_unique_values').update(html);
                                me.unique_values = Ext.JSON.decode(me.layer.get('data_from_DB')['unique_values'])[newValue-1];
                            }

                            me.switchMode('manual');
                        }
                    }
                }
            }),
            {
                html: '<span class="fa fa-lg fa-blue fa-info-circle"></span>&nbsp;<b>Data type</b>: '+me.layer.get('data_from_DB')['data_type'],
                border: false,
            },
            {
                itemId: 'band_statistics',
                html: '',
                border: false,
            },
            {
                itemId: 'num_unique_values',
                html: '',
                border: false,
            }
        ];
    },

    /**
     *  Generate INPUT forms for manual recoding
     */
    __manualRecodeSelection: function(){
        var me = this;
        return {
            xtype: 'panel',
            collapsible: false,
            border: false,
            layout:'column',
            style: 'margin: 5px 0;',
            items: [
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'start_value',
                    width: 75,
                    listeners: {
                        change: function(item){
                            me.__showPreviewButton(item);
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'end_value',
                    width: 75,
                    listeners: {
                        change: function(item){
                            me.__showPreviewButton(item);
                        }
                    }
                }),
                {
                    html: '&nbsp;-->&nbsp;',
                    border: false,
                    width: 60,
                    bodyStyle: 'text-align: center;'
                },
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'recode_value',
                    width: 75
                }),
                Ext.create('Ext.Button', {
                    itemId: 'delete_button',
                    iconCls: 'fa fa-lg fa-red fa-times-circle',
                    cls: 'impact-icon-button',
                    //style: 'margin-left: 10px;',
                    hidden: true,
                    handler: function() {
                        var itemId = this.up('panel', 1).getId();
                        me.down('#RecodeValuesContainer').remove(itemId);
                    }
                }),
                Ext.create('Ext.Button', {
                    itemId: 'add_button',
                    iconCls: 'fas fa-lg fa-green fa-plus-circle',
                    //style: 'margin-left: 10px;',
                    cls: 'impact-icon-button',
                    handler: function() {
                        this.hide();
                        this.previousSibling('#delete_button').show();
                        me.down('#RecodeValuesContainer').add(
                            me.__manualRecodeSelection()
                        );
                        me.doLayout();
                    }
                }),
                Ext.create('Ext.Button', {
                    itemId: 'show_preview_button',
                    iconCls: 'fa fa-lg fa-black fa-eye',
                    style: 'margin-left: 4px;',
                    disabled: true,
                    enableToggle: true,
                    toggleGroup: 'show_preview_toggle',
                    toggleHandler: function(button, state){
                        me.__showPreview(button, state);
                    }
                }),
            ]
        }
    },

    /**
     *  Generate INPUT forms for pre-filled recoding
     */
    __preFilledRecodeSelection: function(start_value){
        var me = this;
        return {
            xtype: 'panel',
            collapsible: false,
            border: false,
            layout:'column',
            style: 'margin: 0;',
            items: [
                {
                    html: me.__showColor(start_value),
                    border: false,
                    width: 30,
                    bodyStyle: 'text-align: center; padding-top: 5px; padding-left: 10px;'
                },
                {
                    xtype: 'label',
                    text: start_value.toString(),
                    itemId: 'start_value',
                    border: false,
                    width: 60,
                    style: 'text-align: center; font-weight: bold; padding-top: 5px'
                },
                {
                    html: '&nbsp;-->&nbsp;',
                    border: false,
                    width: 60,
                    bodyStyle: 'text-align: center; padding-top: 3px'
                },
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'recode_value',
                    width: 75,
                    value: start_value
                })
            ]
        }
    },

    __showColor: function(value){
        var me = this;
        if(me.layer.data_from_DB['lookup_table']!='{}' && me.layer.get('data_from_DB')['num_bands']==1){
            var lookup_table = Ext.JSON.decode(me.layer.get('data_from_DB')['lookup_table']);
            if(value in lookup_table){
                var color = lookup_table[value];
                 var colorHex = IMPACT.Utilities.Common.rgbToHex(color[0], color[1], color[2]);
                return '<span style="width: 16px; height: 16px; background-color: #'+colorHex+'; display: block;"></span>';
            }
        }
        return ''
    },

    __showPreviewButton: function(item){
        var me = this;
        me.previewLayer.setVisible(false);
//        me.previewLayer.getSource().refresh();  // needed ?
        var container = item.up('panel');
        var min = container.down('#start_value').getValue();
        var max = container.down('#end_value').getValue();
        var preview_button = container.down('#show_preview_button');
        if(min!=null && max!=null){
            preview_button.toggle(false);
            preview_button.enable();
        } else {
            preview_button.toggle(false);
            preview_button.disable();
        }
    },

    /**
     * Show preview layer
     */
    __showPreview: function(button, state){
        var me = this;
        if(state==true){
            var container = button.up('panel');
            var band = me.down('#band').getValue();
            var min = container.down('#start_value').getValue();
            var max = container.down('#end_value').getValue();
            if(min==null || max==null){
                Ext.Msg.show({
                    title: '"Missing parameters',
                    msg: 'Please select "from" and "to" values.',
                    buttons: Ext.Msg.OK
                });
                return
            }
//            me.previewLayer.updateRange(band, min, max);
            me.previewLayer.getSource().updateParams({'bands':band});
            me.previewLayer.getSource().updateParams({'apply_data_range' : [false, min, max]});
            me.previewLayer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
            me.previewLayer.getSource().refresh();
            if(me.previewLayer.getVisible()==false){
                me.previewLayer.setVisible(true);
            }


        } else {
            me.previewLayer.setVisible(false);
            //me.previewLayer.getSource().refresh();
        }
    },

    /**
     *  Apply recode to file
     */
    __launchRecode: function(){
        var me = this;
        me.errorMsg = [];
        var tmpCode="temp_proj:"+Date.now().toString();
        proj4.defs(tmpCode, me.layer.get('PROJ4'));
        ol.proj.proj4.register(proj4);

        var extent = me.map.getView().calculateExtent(IMPACT.main_map.getSize());//.transformExtent(IMPACT.SETTINGS['WGS84_mercator'], layer_projection);
        var layer_projection = new ol.proj.Projection({code:tmpCode});

        extent = ol.proj.transformExtent(extent, IMPACT.SETTINGS['WGS84_mercator'], layer_projection);

        var params = {
            toolID: 'raster_recode',
            input_file: me.layer.get('data_from_DB')['full_path'],
            band: me.__parseInputValues('band'),
            recode_values: JSON.stringify(me.__parseRecodeValues()),
            recode_extent: (me.__parseInputValues('rasterRecodeExtent') === 'Yes') ? extent : '',  // pass empty string in case entire raster has to be recoded
            recode_AOI: me.__parseInputValues('recodeAOI'),  // pass empty string in case entire raster has to be recoded
            save_to_new: me.__parseInputValues('save_to_new'),
            out_suffix: me.__parseInputValues('out_suffix')
        };

        if (params.recode_AOI == 'Yes'){
            var layer = IMPACT.Utilities.OpenLayers.getTemporaryLayer(me.map);
            if (layer == false){
                alert('Unexpected error retrieving Temporary Feature Layer');
                alert('Please deselect AOI');
            }
            if ( layer.getSource().getFeaturesCollection().getLength() == 0){
                me.errorMsg.push('No AOI found, please draw one');
            }else{

                //  geom are passed in Mercator by default, conversion done via python
                var geoJSON = new ol.format.GeoJSON().writeFeatures(layer.getSource().getFeatures(), {dataProjection: IMPACT.SETTINGS['WGS84_mercator'], featureProjection: IMPACT.SETTINGS['WGS84_mercator']});
                params.recode_AOI = geoJSON;

            }
        }




        if(params.band=='') { me.errorMsg.push('Band is required') }
        if(params.save_to_new=='Yes' && params.out_suffix==''){
            me.down('#out_suffix').setAsMissing();
            me.errorMsg.push('Output suffix is required');
        }

        if(me.errorMsg.length>0){
            IMPACT.view.Processing.Window.component.ValidationMsg.show(me.errorMsg);
            return;
        }


        if(params.save_to_new=='No'){
            me.recode_mode = 'manual';
            me.previewLayer.setVisible(false);
            //me.previewLayer.getSource().refresh();
            me.waitUI();
        } else{
            me.waitUI();
            setTimeout(function(){
                me.waitUI(false);
            }, 5000);
        }

        IMPACT.Utilities.Requests.launchProcessing(params);

    },

    /**
     *  Parse recode values
     */
    __parseRecodeValues: function(){
        var me = this;
        var recode_values = []
        var recodeValuesContainer = me.down('#RecodeValuesContainer');

        // Manual recoding
        if(me.recode_mode=='manual'){
            var index = 0;
            recodeValuesContainer.items.each(function(item){
                if(index>0){
                    var start_value = IMPACT.Utilities.Input.parseField(item.down('#start_value'));
                    var end_value = IMPACT.Utilities.Input.parseField(item.down('#end_value'));
                    var recode_value = IMPACT.Utilities.Input.parseField(item.down('#recode_value'));
                    if(me.errorMsg.length==0){
                        if(start_value=='') { me.errorMsg.push('Start value is required') }
                        if(end_value=='') { me.errorMsg.push('End value is required') }
                        if(recode_value=='') { me.errorMsg.push('Recode value is required') }
                    }
                    recode_values.push([start_value, end_value, recode_value]);
                }
                index++;
            });
        }
        // PreFilled recoding
        else {
            var index = 0;
            recodeValuesContainer.items.each(function(item){
                if(index>0){
                    if(item.down('#start_value')){
                        var start_value = IMPACT.Utilities.Input.parseField(item.down('#start_value'));
                        var recode_value = IMPACT.Utilities.Input.parseField(item.down('#recode_value'));
                        if(me.errorMsg.length==0){
                            if(recode_value=='') { me.errorMsg.push('Recode value is required') }
                        }
                        recode_values.push([start_value, start_value, recode_value]);
                    }
                }
                index++;
            });
        }
        return recode_values;
    },

    /**
     *  Parse INPUT value
     */
    __parseInputValues: function(inputFieldId){
        return IMPACT.Utilities.Input.parseField(this.query('#'+inputFieldId)[0]);
    },

});
