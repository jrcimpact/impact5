/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.Vector', {
    extend: 'IMPACT.view.component.Window',

    requires: ['IMPACT.view.component.ColorPalette.Vector.Grid',
        'IMPACT.view.component.ColorPalette.Vector.ComboBox'
    ],

    title: 'Vector Editing Tools',

    itemId: 'EditingOptionWindowVector',

    config: {
        legendType: null,
        treeNode: null,
        map: null,
        editingContainer: null
    },
    resizable: false,
    width: 370,

    //layout: 'vbox',
    collapsible: true,

    layout: {
        type: 'vbox',       // Arrange child items vertically
        align: 'stretch',    // Each takes up full width
        padding: 0
    },

    bodyStyle: 'background-color: white;',
    border: false,


    buttons: [
        {
            text: 'Stop Editing',
            handler: function () {
                this.up('#EditingOptionWindowVector').close();
            }
        }
    ],

    items: [],
    listeners: {
        'afterrender': function () {
            var me = this;
            setTimeout(function () {
                me.down('#vectorEditingShowHide').collapse();
            }, 5000);
        }
    },


//    initComponent: function () {
    constructor: function (cfg) {
        var me = this;
        me.superclass.constructor.call(me, cfg);
        //me.callParent();
        //console.log(me.legendType);
        me.map = IMPACT.main_map;
        me.editingContainer = Ext.ComponentQuery.query('#EditingVector')[0];

        var layer = me.editingContainer.editingMode.layer;


        var attributes = layer.get('attributes').split(",");
//        var attributesWithoutID = Ext.ComponentQuery.query('#EditingVector')[0].editingMode.layer.get('attributes').split(",");
        var attributesWithoutID = layer.get('attributes').split(",");
        attributesWithoutID.splice(attributes.indexOf(IMPACT.SETTINGS['vector_editing']['attributes']['ID']), 1);

        var comments = [];
        IMPACT.SETTINGS['vector_editing']['predefined_comments'].forEach(function (val) {
            comments.push(val[0]);
        });



//        var ClassAttributes = IMPACT.Utilities.OpenLayers.getEditingLayers(layer);
//
//        console.log(attributes);
//        console.log(ClassAttributes);
        // -------------------   SELECTION -------------------------------
        me.add([
            {
                layout: 'vbox',
                title: '<b>Select by</b>',
                height: 180,
                bodyStyle: 'padding: 5px; background-color: #EFFBEF;',
                cls: 'impact-wms-panel',
                items: [
                    {
                        layout: 'hbox',
                        //title: 'Select by',
                        border: false,
                        bodyStyle: 'background-color: #EFFBEF;',
                        height: 80,
                        items: [
                            {
                                xtype: 'radiogroup',
                                itemId: 'selectionMode',
                                bodyStyle: 'margin-top: 5px;',
                                columns: 1,
                                items: [
                                    {boxLabel: 'Point (click)', name: 'select_mode', inputValue: "id", checked: true},
                                    {boxLabel: 'Drawing AOI', name: 'select_mode', inputValue: "poly"},
                                    {boxLabel: 'DBF attributes', name: 'select_mode', inputValue: "dbf"},
                                ],
                                listeners: {
                                    change: function (combo, newValue) {
                                        var EditingVector = Ext.ComponentQuery.query('#EditingVector')[0];
                                        var DBF = Ext.ComponentQuery.query('#DbfAttributeTable')[0];
                                        EditingVector.editingMode.leftClickMode = newValue.select_mode;
                                        EditingVector.clearSelection();
                                        EditingVector.editingMode.clusterValue = '';
                                        me.__toggleDrawMode(false);
                                        me.down('#vectorRecodeExtent').enable();
                                        me.down('#SelectAttribute').disable();

                                        if (newValue.select_mode == "id") {
                                            me.down('#SelectAttribute').enable();
                                            var selectedVal = me.down('#SelectAttribute').getValue();
                                            EditingVector.editingMode.clusterItem = selectedVal;
                                            if (typeof DBF !== "undefined") {
                                                DBF.destroy();
                                            };
                                            //IMPACT.main_map.on('singleclick', EditingVector.leftClick);
                                        };

                                        if (newValue.select_mode == "poly") {
                                            if (typeof DBF !== "undefined") {
                                                DBF.destroy();
                                            };

                                            me.__toggleDrawMode(true);
                                            //IMPACT.main_map.un('singleclick', EditingVector.leftClick);
                                        }
                                        ;

                                        if (newValue.select_mode == "dbf") {
                                            me.down('#vectorRecodeExtent').disable();
                                            //IMPACT.main_map.on('singleclick', EditingVector.leftClick);

                                            if (typeof DBF !== "undefined") {
                                                // rebuild if not open in editing mode
                                                if (!DBF.editingMode) {
                                                    DBF.destroy();
                                                    Ext.create('IMPACT.view.Workspace.Main.component.DbfAttributeTable', {
                                                        editingMode: true,
                                                        layerNode: me.treeNode
                                                    });
                                                }
                                            } else {
                                                Ext.create('IMPACT.view.Workspace.Main.component.DbfAttributeTable', {
                                                    editingMode: true,
                                                    layerNode: me.treeNode
                                                });
                                            }
                                        }

                                    }
                                }
                            },
                            {
                                xtype: 'impactFieldCombo',
                                fieldLabel: 'on',
                                itemId: 'SelectAttribute',
                                labelWidth: 20,
                                labelAlign: 'left',
                                margin: '1 0 0 0',
                                width: 120,
                                store: attributes,
                                value: IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                                allowBlank: false,
                                listeners: {
                                    select: function (combo) {
                                        var EditingVector = Ext.ComponentQuery.query('#EditingVector')[0];
                                        EditingVector.editingMode.clusterItem = combo.getValue();
                                        EditingVector.clearSelection();
                                    }
                                }
                            },
                            {
                                xtype: 'button',
                                itemId: 'PrevFeatId',
                                width: 50,
                                labelWidth: 40,
                                text: 'Prev',
                                margin: '1 0 0 10',
                                disabled: true,
                                listeners: {
                                    click: function () {
                                        me.editingContainer.__goToFeature('prev')
                                    }
                                }
                            },
                            {
                                xtype: 'button',
                                itemId: 'NextFeatId',
                                text: 'Next',
                                width: 50,
                                labelWidth: 40,
                                disabled: true,
                                margin: '1 0 0 3',
                                listeners: {
                                    click: function () {
                                        me.editingContainer.__goToFeature('next')
                                    }
                                }
                            },
                            {
                                xtype: 'textfield',
                                name: 'LastselectedID',
                                itemId: 'LastselectedID',
                                fieldLabel: 'Last ' + IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                                width: 190,
                                disabled: true,
                                labelWidth: 50,
                                margin: '25 0 0 -170',
                                value: '-'
                            },
                            {
                                xtype: 'textfield',
                                name: 'LastselectedVAL',
                                itemId: 'LastselectedVAL',
                                fieldLabel: 'Last Val',
                                width: 190,
                                disabled: true,
                                labelWidth: 60,
                                margin: '50 0 0 -200',
                                value: '-'
                            }

                        ]
                    }, // end if item 1 in Vbox
                       // Item2 on Vbox
                    {
                        layout: 'column',
                        border: false,
                        bodyStyle: 'background-color: #EFFBEF;',
                        items: [
                            {

                                xtype: 'checkboxfield',
                                itemId: 'selectOnlyActiveClass',
                                border: false,
                                bodyStyle: 'background-color: #EFFBEF;',
                                width: 120,
                                boxLabel: 'Select only within ',
                                name: 'selectOnActive',
                                inputValue: false,

                                margin: '8 0 0 0',
                                listeners: {
                                    change: function (checkbox, newValue) {
                                        var EditingVector = Ext.ComponentQuery.query('#EditingVector')[0];
                                        EditingVector.editingMode.selectOnlyActiveClass = newValue;
                                        if (newValue) {
                                            me.down('#selectOnlyActiveClass_Layer').enable();
                                            me.down('#subSelectionMode').enable();
                                        } else {
                                            me.down('#selectOnlyActiveClass_Layer').disable();
                                            me.down('#subSelectionMode').disable();
                                        }

                                        for (var i in EditingVector.attributeNodes) {
                                            // set sub-selection only of visible class T/F
                                            EditingVector.attributeNodes[i].data.layer.getSource().updateParams({'selectOnlyActiveClass' : newValue});
                                        }
                                    }
                                }
                            },
                            {
                                xtype: 'radiogroup',
                                itemId: 'subSelectionMode',
                                bodyStyle: 'margin-top: 8px;',
                                columns: 1,
                                disabled: true,
                                items: [
                                    {boxLabel: 'Active classes', name: 'active_mode', inputValue: "on", checked: true},
                                    {boxLabel: 'Disabled classes', name: 'active_mode', inputValue: "off"}
                                ],
                                listeners: {
                                    change: function (combo, newValue) {
                                    var EditingVector = Ext.ComponentQuery.query('#EditingVector')[0];
                                        EditingVector.editingMode.modeActiveClasses = newValue.active_mode;
                                    }
                                }
                            },
                            {
                                xtype: 'impactFieldCombo',
                                fieldLabel: 'in',
                                itemId: 'selectOnlyActiveClass_Layer',
                                labelWidth: 15,
                                labelAlign: 'left',
                                margin: '8 0 0 0',
                                width: 105,
                                store: attributes,
                                disabled: true,

                                value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                                allowBlank: false,
                                listeners: {
                                    select: function (combo) {
                                        var EditingVector = Ext.ComponentQuery.query('#EditingVector')[0];
                                        EditingVector.editingMode.selectOnlyActiveClass_Layer = combo.getValue();
                                    }
                                }
                            }
                        ]
                    }, // end of column
                       // last row with clear button on right
                    {
                        xtype: 'button',
                        itemId: 'clearSelectionButton',
                        text: '<b>Clear selections</b>',

                        margin: '5 0 0 100',
                        listeners: {
                            click: function () {
                                Ext.ComponentQuery.query('#EditingVector')[0].clearSelection();
                            }
                        }
                    }
                ]
            }
        ]);
        // -------------------   SAVE TO OPTIONS --------------------------------
        me.add([
            {
                layout: 'vbox',
                height: 90,
                bodyStyle: 'padding: 5px; background-color: #F7F2E0;',
                title: '<b>Recode options</b>',
                collapsible: true,
                //border: true,
                //flex: 1,
                items: [
                    {
                        xtype: 'impactFieldCombo',
                        fieldLabel: 'Save To attribute',
                        itemId: 'SaveAttribute',
                        labelWidth: 180,
                        width: 300,
                        store: attributesWithoutID,
                        value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                        allowBlank: false
                    },
                    {
                        xtype: 'radiogroup',
                        itemId: 'vectorRecodeExtent',
                        fieldLabel: 'Recoding area',
                        labelWidth: 100,
                        width: 250,
                        columns: 2,
                        width: '100%',
                        vertical: false,
                        items: [
                            {boxLabel: 'Entire image ', name: 'vrecodeMode', inputValue: 'full', checked: false},
                            {boxLabel: 'Current view', name: 'vrecodeMode', inputValue: 'view', checked: true}

                        ]
                    }
                ]
            }
        ]);

        me.add([
            {
                layout: 'vbox',
                height: 200,
                bodyStyle: 'padding: 5px; background-color: #FBEFF5;',
                title: '<b>Recode Value</b>',
                collapsible: true,
                items: [
                    {
                        xtype: 'radiogroup',
                        itemId: 'recodeMode',
                        fieldLabel: 'Using ',
                        labelWidth: 60,
                        width: 330,
                        columns: 2,
                        items: [
                            {boxLabel: 'Active Legend', name: 'recode_mode', inputValue: "legend", checked: true},
                            {boxLabel: 'Comments', name: 'recode_mode', inputValue: "comments"}

                        ],
                        listeners: {
                            change: function (combo, newValue) {
                                if (newValue.recode_mode == "legend") {
                                    me.down('#recodeToClassCombo').enable();
                                    me.down('#predefinedComments').disable();
                                } else {
                                    me.down('#predefinedComments').enable();
                                    me.down('#recodeToClassCombo').disable();
                                }
                            }
                        }
                    },
                    {
                            xtype: 'LegendCombo',
                            itemId: 'recodeToClassCombo',
                            legendType: me.legendType,
                            labelAlign: 'left',
                            fieldLabel: 'select',
                            labelWidth: 80,
                            width: 250,
                            editable: false
                    },
                    {
                        xtype: 'impactFieldCombo',
                        itemId: 'predefinedComments',
                        labelAlign: 'left',
                        fieldLabel: 'select/write',
                        labelWidth: 80,
                        width: 250,
                        store: comments,
                        allowBlank: true,
                        disabled: true,
                        editable: true

                    },
                    // ------------------------------------  UNDO -----------------------------------
                    {
                        xtype: 'button',
                        itemId: 'UndoItemId',
                        width: 100,
                        text: 'Disable Undo',
                        margin: '5 0 0 5',
                        listeners: {
                            click: function () {
                                me.editingContainer.__activateUndo()
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        itemId: 'PrevUndoId',
                        width: 40,
                        labelWidth: 40,
                        text: 'Prev',
                        margin: '1 0 0 5',
                        disabled: false,
                        listeners: {
                            click: function () {
                                me.editingContainer.__Undo('prev')
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        itemId: 'NextUndoId',
                        text: 'Next',
                        width: 40,
                        labelWidth: 40,
                        disabled: false,
                        margin: '-20 0 0 45',
                        listeners: {
                            click: function () {
                                me.editingContainer.__Undo('next')
                            }
                        }
                    },
                    {
                        xtype: 'textfield',
                        name: 'LastUndo',
                        itemId: 'LastUndo',
                        //fieldLabel: 'Undo',
                        width: 15,
                        disabled: false,
                        labelWidth: 15,
                        margin: '-20 0 0 90',
                        value: '-'
                    },
                    //-------------------------------------------------------------------------------------
                    {
                        xtype: 'button',
                        width: 100,
                        text: '<b> Recode </b>',
                        margin: '-35 0 0 220',
                        listeners: {
                            click: function () {
                                me.__recodeTo()
                            }
                        }
                    }
                ]
            }
        ]);
        me.add([
            {
                layout: 'vbox',
                height: 160,
                bodyStyle: 'padding: 5px; background-color: #FFFFF5;',
                title: '<b>Columns Options</b>',
                collapsible: true,
                collapsed: true,
                items: [
                    {
                    html: 'Copy the entire content (all rows / no selection)'
                    },
                    {
                        xtype: 'impactFieldCombo',
                        fieldLabel: 'From ->',
                        itemId: 'CopyInColumn',
                        labelWidth: 180,
                        width: 300,
                        margin: '5 0 0 0',
                        store: attributesWithoutID,
                        value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                        allowBlank: false
                    },
                    {
                        xtype: 'impactFieldCombo',
                        fieldLabel: 'To ->',
                        itemId: 'CopyOutColumn',
                        labelWidth: 180,
                        width: 300,
                        store: attributesWithoutID,
                        value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
                        allowBlank: false
                    },
                    {
                        xtype: 'button',
                        width: 70,
                        text: '<b> Copy </b>',
                        //margin: '-20 0 0 275',
                        listeners: {
                            click: function () {
                                me.__CopyColumn()
                            }
                        }
                    }
                ]
             }
         ]);
        me.add([
            {
                layout: 'vbox',
                height: 80,
                bodyStyle: 'padding: 5px; background-color: #FFFFF5;',
                title: '<b>Geometry Options</b>',
                collapsible: true,
                collapsed: true,
                items: [
                    {
                    html: 'Merges a list of features into the 1st selected.<br /> Only the attributes of the 1st one are preserved'
                    },
                    {
                        xtype: 'button',
                        width: 70,
                        text: '<b> Merge </b>',
                        margin: '-20 0 0 275',
                        listeners: {
                            click: function () {
                                me.__mergeFeatures()
                            }
                        }
                    }
                ]
             }
         ]);
        // -------------------   Class VISIBILITY -------------------------------
        me.add([
            {
                xtype: 'button',
                //width: 100,
                //margin: '1 0 0 77',
                text: ' ------>  <b> Hide all classifications </b>  <------',
                listeners: {
                    click: function () {
                        me.__hideClassLayer()
                    }
                }
            },
            {
                layout: 'fit',
                itemId: 'vectorEditingShowHide',
                title: '<b> Expand to activate/deactivate classes</b>',
                header: {
                    style: {
                        backgroundColor: '#f6aecf'
                    }
                },
                maxHeight: 400,
                flex: 1,
                collapsible: true,
                border: false,
                items: [
                    {
                        xtype: 'ColorPaletteVectorGrid',
                        legendType: me.legendType,
                        showCheckbox: true
                    }
                ]
            }
        ]);
        //me.callParent();
        // -----------------------  init functions on objects  -----------------------

        me.down("#recodeToClassCombo").setValue(me.down("#recodeToClassCombo").getStore().getAt(0));


    },

    __toggleDrawMode: function(activate){
        var me = this;
        //var drawLayer = IMPACT.Utilities.OpenLayers.getEditingAOI(IMPACT.main_map);  // 'editing_selection_aoi'

        if (activate) {
           IMPACT.main_map.getInteractions().forEach((i) => {
                if (i instanceof ol.interaction.Draw &&
                    i.type_ == 'Polygon'
                   ) {
                    i.setActive(true);
                  }
            });

//            me.interactionDraw = new ol.interaction.Draw({
//                    source: me.editingContainer.selection_layer.getSource(),
//                    type: 'Polygon',                     // type of draw
//                    //geometryFunction:ol.interaction.Draw.createBox()
//               });
//
//            IMPACT.main_map.addInteraction(me.interactionDraw);


//            // Add Vector layer for drawing features
//            var drawLayer = new OpenLayers.Layer.Vector("editing_selection_aoi", {
//                displayInLayerSwitcher: false,
//                type: 'hidden',
//                eventListeners: {
//                    'featureadded': function (event) {
//                        var wkt = new OpenLayers.Format.WKT();
//                        var wkt_selection = wkt.write(event.feature);
//                        me.editingContainer.selectPolygon(wkt_selection, false);
//                        this.clear();
//                    }
//                }
//            });
//            me.map.addLayer(drawLayer);
//            // Add DrawFeature control
//            var aoi_control = new OpenLayers.Control.DrawFeature(
//                drawLayer,
//                OpenLayers.Handler.Polygon,
//                {
//                    name: 'editing_selection_aoi_control',
//                }
//            )
//            me.map.addControl(aoi_control)
//            aoi_control.activate();
        }
        // ## disable draw ##
        else {
            // remove layer

            IMPACT.main_map.getInteractions().forEach((i) => {

                if (i instanceof ol.interaction.Draw &&
                      i.type_ == 'Polygon'
                   ) {
                    i.setActive(false);
                  }
            });

//             IMPACT.main_map.getInteractions().forEach((i) => {
//
//                        if (i instanceof vectorEditInteraction) {
//                            i.setActive(false);
//                            alert('deactivate Inter')
//                          }
//                    });



//            var aoi_control = me.map.getControlsBy("name", 'editing_selection_aoi_control');
//            if (aoi_control.length > 0) {
//                aoi_control[0].deactivate();
//                me.map.removeControl(aoi_control[0])
//                aoi_control[0].destroy();
//            }
            // remove layer

//            if (drawLayer != false) {
//                //me.map.removeLayer(drawLayer[0]);
//                drawLayer.getSource().clear();
//            }
        }
    },

    __recodeTo: function (activate) {
        var me = this;
        var saveValue = me.down('#recodeToClassCombo').disabled ? me.down('#predefinedComments').getValue() : me.down('#recodeToClassCombo').getValue();
        if (saveValue === null) {
            saveValue = "";
        }
        Ext.ComponentQuery.query('#EditingVector')[0].recodeTo(saveValue);
    },

    __mergeFeatures: function () {
        var me = this;
//        add test on selection mode: only by id must be on
        Ext.ComponentQuery.query('#EditingVector')[0].mergeFeatures();
    },

    __CopyColumn: function () {
        var me = this;
//        add test on selection mode: only by id must be on
        var InCol = me.down('#CopyInColumn').getValue();
        var OutCol = me.down('#CopyOutColumn').getValue();
        Ext.ComponentQuery.query('#EditingVector')[0].CopyColumn(InCol, OutCol);
    },

    __hideClassLayer: function () {
        var me = this;

        var classification_layers = IMPACT.Utilities.OpenLayers.getAttributesLayer(IMPACT.main_map);
        var active_layers = Array(classification_layers.length);
        for (var i = 0; i < classification_layers.length; i++) {
            if (classification_layers[i].isVisible() == true) {
                active_layers[i] = true;
                classification_layers[i].setVisible(false);
            }
        }
        ;
        setTimeout(function () {
            for (var i = 0; i < classification_layers.length; i++) {
                if (active_layers[i] == true) {
                    classification_layers[i].setVisible(true);
                }
            }
        }, 1000);
    },


    close: function () {
        var me = this;
        me.__toggleDrawMode(false);
        this.hide();
        this.editingContainer.stopEditingMode();
    }
});
