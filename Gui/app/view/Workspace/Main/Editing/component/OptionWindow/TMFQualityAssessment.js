/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.TMFQualityAssessment', {
    extend: 'IMPACT.view.component.Window',

    requires: [
        'IMPACT.view.component.Field.TextArea'
    ],

    title: 'TMF Quality Assessment Interface',
    itemId: 'TMFwindow',
    resizable: true,
    bodyStyle: 'padding:0px;',
    width: 1050,
    height: 350,
    maxHeight: 350,
    maxWidth: 1050,
    layout: 'fit',
    y: 100,
    collapsible: true,
    closeAction: 'destroy',
    buttons: [],
    border: true,

    legendType: null,
    treeNode: null,
    map: null,
    editingContainer: null,

    attributeIDS: ['TMFtrajectory', 'TMFdisturbance', "TMFregrowth", "TMFtrajconf" , "TMFdistconf", "TMFregconf"
               ,"TMFyearDistPanel", "TMFyearRegrowPanel"  ],
              // reset the confidence

     initComponent: function () {
        var me = this;

        me.editingContainer = Ext.ComponentQuery.query('#QualityAssessmentContainer')[0];
        var item = Ext.data.StoreManager.lookup('VectorLegends').__filterByType(me.legendType);
        me.legendStore =  [];
        if(item!==null){
            Ext.Array.forEach(item.classes, function(id) {
                me.legendStore.push({
                    inputValue:  id[0],
                    boxLabel:  id[1],
                    name:  'cl',
                    style: {
                                //'background-color': 'lightgreen',
                                //'border': '1px solid black'
                            }
                });
            });
        };
        me.callParent();



        me.add(
                {
                    xtype: 'panel',
                    width: '100%',
                    itemId: 'AssessmentPanel',
                    disabled: false,
                    layout: 'hbox',
                    //overflowY: 'scroll',
                    //overflowX: 'scroll',
                    border: false,
                    items:[
                            {
                                xtype: 'panel',
                                itemId: 'optionPanel',
                                layout: 'vbox',
                                border: false,
                                items:[
                                        {
                                            xtype: 'panel',
                                            itemId: 'directionPanel',
                                            width: 200,
                                            height: 100,
                                            title: "Navigation",
                                            layout: 'vbox',
                                            border: true,
                                            disabled: false,
                                            //margins: '5 0 0 0',
                                            padding: 5,
                                            //bodyStyle: 'background-color: #cce6ff;',
                                            items: [
                                                    {
                                                        xtype: 'button',
                                                        itemId: 'TMFPrevFeatId',
                                                        width: 80,
                                                        labelWidth: 40,
                                                        text: 'Prev',
                                                        margin: '5 0 0 10',
                                                        disabled: false,
                                                        listeners: {
                                                            click: function () {
                                                                this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                this.up('#TMFwindow').resetFlagsOnGUI();
                                                                this.up('#TMFwindow').editingContainer.__goToFeature('prev', showOnly = this.up('#TMFwindow').down('#TMFshow').getValue().show)
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        itemId: 'TMFNextFeatId',
                                                        text: 'Next',
                                                        width: 80,
                                                        labelWidth: 40,
                                                        disabled: false,
                                                        margin: '-20 0 0 100',
                                                        listeners: {
                                                            click: function () {
                                                                this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                this.up('#TMFwindow').resetFlagsOnGUI();
                                                                this.up('#TMFwindow').editingContainer.__goToFeature('next', showOnly = this.up('#TMFwindow').down('#TMFshow').getValue().show)
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        name: 'Lastselected ID',
                                                        itemId: 'AssessmentLastselectedID',
                                                        fieldLabel: 'Last ID', //+ IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                                                        width: 160,
                                                        disabled: false,
                                                        labelWidth: 50,
                                                        margin: '10 0 0 20',
                                                        value: '-'
                                                    }
                                            ]
                                        },{
                                            xtype: 'panel',
                                            itemId: 'navigationPanel',
                                            width: 200,
                                            height: 200,
                                            title: "Options",
                                            layout: 'vbox',
                                            border: true,
                                            disabled: true,
                                            //margins: '5 0 0 0',
                                            padding: 5,
                                            //bodyStyle: 'background-color: #cce6ff;',
                                            items: [
                                                    {
                                                            xtype: 'radiogroup',
                                                            itemId:'TMFshow',
                                                            columns: 1,
                                                            border: false,
                                                            width: 280,
                                                            labelWidth: 50,
                                                            margin: '20 0 0 10',
                                                            hideLabel: false,
                                                            fieldLabel: 'Show',
                                                            items:[
                                                                {inputValue: 'All', boxLabel: 'All', name:'show', checked:true },
                                                                {inputValue: 'Empty', boxLabel: 'Empty', name:'show' },
                                                                {inputValue: 'Verify', boxLabel: 'Verify', name:'show' }
                                                            ]
                                                    },
                                                    {
                                                            xtype: 'textfield',
                                                            itemId: 'TMFusername',
                                                            fieldLabel: 'User',
                                                            width: 150,
                                                            labelWidth: 50,
                                                            value: '',
                                                            allowBlank: true,
                                                            margin: '20 0 0 10',
                                                            regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                                                    },
                                                    {
                                                        xtype: 'checkbox',
                                                        hideLabel: false,
                                                        itemId: 'TMFverify',
                                                        margin: '10 0 0 10',
                                                        fieldLabel: 'Verify ',
                                                        labelWidth: 80,
                                                        width: 160,
                                                        listeners: {
                                                                change: function(checkbox, newValue) {
                                                                       this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                }
                                                            }

                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: "Clear form",
                                                        margin: '10 0 0 60',
                                                        listeners: {
                                                            click: function() {
                                                                        this.up('#TMFwindow').down('#navigationPanel').suspendEvents();
                                                                        this.up('#TMFwindow').resetFlagsOnGUI();
                                                                        this.up('#TMFwindow').down('#navigationPanel').resumeEvents();
                                                                        this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange'); // save clear items to DB
                                                                }
                                                         }
                                                    },

                                            ]
                                        }
                                ]
                        },  // -----------------------------  END of Navigation - options PANEL items -------------------------------------

                        {
                            xtype: 'panel',
                            itemId: 'trajectoryPanel',
                            width: 210,
                            height: 300,
                            title: "Trajectory",
                            layout: 'vbox',
                            border: true,
                            padding: 5,
                            disabled: true,
                            //bodyStyle: 'background-color: #cce6ff;',
                            items: [
                                     {
                                        xtype: 'radiogroup',
                                        itemId: 'TMFtrajectory',
                                        title: "Trajectory",
                                        columns: 1,
                                        border: true,
                                        padding: 5,
                                        width: 210,
                                        hideLabel: false,
                                        items: [

                                            { boxLabel: 'stable forest', name: 'tval', inputValue: 1, margin:'0 0 0 2', style: {'background-color': 'lightgrey'}},
                                            { boxLabel: 'stable non-forest', name: 'tval', inputValue: 2, margin:'0 0 0 2', style: {'background-color': 'lightgrey'}},
                                            { boxLabel: 'forest to disturbance to forest', name: 'tval', inputValue: 3, margin: '0 0 0 2', style: {'background-color': 'lightgreen'}},
                                            { boxLabel: 'forest to deforested', name: 'tval', inputValue: 4, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'forest to non-forest to forest', name: 'tval', inputValue: 5, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'non-forest to forest', name: 'tval', inputValue: 6, margin: '0 0 0 2', style: {'background-color': 'lightblue'}},
                                            { boxLabel: 'insufficient data', name: 'tval', inputValue: 7, margin: '0 0 0 2', style: {'background-color': 'grey'}}

                                        ],
                                        //margin: '74 0 0 -330',
                                        listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                        }
                                     },
                                     {
                                       xtype: 'radiogroup',
                                       itemId:'TMFtrajconf',
                                       columns: 1,
                                       border: false,
                                       width: 210,
                                       labelWidth: 70,
                                       margin: '23 0 0 10',
                                       hideLabel: false,
                                       fieldLabel: 'Confidence',
                                       items:[
                                           {inputValue: 0, boxLabel: 'high', name: 'tcval'},
                                           {inputValue: 1, boxLabel: 'low', name: 'tcval'},
                                            ],
                                       listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                       }
                                    }
                                ]

                        }, // -----------------------------  END of Trajectory PANEL items -------------------------------------
                        {
                            xtype: 'panel',
                            itemId: 'driversDistPanel',
                            width: 200,
                            height: 300,
                            title: "Drivers: disturbance",
                            layout: 'vbox',
                            border: true,
                            padding: 5,
                            disabled: true,
                            //bodyStyle: 'background-color: #cce6ff;',
                            items: [
                                    {
                                        xtype: 'radiogroup',
                                        itemId:'TMFdisturbance',
                                        title: "Drivers: disturbance",
                                        columns: 1,
                                        border: true,
                                        padding: 5,
                                        width: 150,
                                        hideLabel: false,
                                        items: [

                                            { boxLabel: 'fire', name:  'dval', inputValue: 1, margin: '0 0 0 2', style: {'background-color': 'lightgreen'}},
                                            { boxLabel: 'logging', name:  'dval', inputValue: 2, margin: '0 0 0 2', style: {'background-color': 'lightgreen'}},
                                            { boxLabel: 'drought', name: 'dval', inputValue: 3, margin: '0 0 0 2', style: {'background-color': 'lightgreen'}},
                                            { boxLabel: 'farmland', name: 'dval', inputValue: 4, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'mining', name: 'dval', inputValue: 5, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'infrastructure ', name: 'dval', inputValue: 6, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'other ', name: 'dval', inputValue: 7, margin: '0 0 0 2', style: {'background-color': 'lightgrey'}}

                                        ],

                                        //margin: '74 0 0 -330',
                                        listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                        }
                                    },
                                    {
                                       xtype: 'radiogroup',
                                       itemId:'TMFdistconf',
                                       columns: 1,
                                       border: false,
                                       width: 280,
                                       labelWidth: 70,
                                       margin: '5 0 0 10',
                                       hideLabel: false,
                                       fieldLabel: 'Confidence',
                                       items:[
                                           {inputValue: 0, boxLabel: 'high', name: 'dcval'},
                                           {inputValue: 1, boxLabel: 'low', name: 'dcval'},
                                            ],
                                       listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                       }
                                    }
                                    ],
                        },//-----------------------------  END of Driver Disturbance PANEL items -------------------------------------

                        {
                            xtype: 'panel',
                            itemId: 'driversRegrowPanel',
                            width: 200,
                            height: 300,
                            title: "Drivers: regrowth",
                            layout: 'vbox',
                            border: true,
                            padding: 5,
                            disabled: true,
                            //bodyStyle: 'background-color: #cce6ff;',
                            items: [
                                    {
                                        xtype: 'radiogroup',
                                        itemId:'TMFregrowth',
                                        title: "Drivers: regrowth",
                                        columns: 1,
                                        border: true,
                                        padding: 5,
                                        width: 150,
                                        hideLabel: false,
                                        items: [

                                            { boxLabel: 'land abandonment', name:  'rval', inputValue: 1, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'shifting cultivation', name:  'rval', inputValue: 2, margin: '0 0 0 2', style: {'background-color': 'lightcoral'}},
                                            { boxLabel: 'forest plantation', name: 'rval', inputValue: 3, margin: '0 0 0 2', style: {'background-color': 'lightblue'}},
                                            { boxLabel: 'other ', name: 'rval', inputValue: 4, margin: '0 0 0 2', style: {'background-color': 'lightgrey'}}

                                        ],

                                        //margin: '74 0 0 -330',
                                        listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                        }
                                    },
                                    {
                                       xtype: 'radiogroup',
                                       itemId:'TMFregconf',
                                       columns: 1,
                                       border: false,
                                       width: 280,
                                       labelWidth: 70,
                                       margin: '82 0 0 10',
                                       hideLabel: false,
                                       fieldLabel: 'Confidence',
                                       items:[
                                           {inputValue: 0, boxLabel: 'high', name: 'rcval'},
                                           {inputValue: 1, boxLabel: 'low', name: 'rcval'},
                                            ],
                                       listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                       }
                                    }
                                    ],
                        },//-----------------------------  END of Driver Regrowth PANEL items -------------------------------------

                        {
                            xtype: 'panel',
                            itemId: 'TMFyearPanel',
                            width: 200,
                            height: 140,
                            title: "Year",
                            layout: 'vbox',
                            border: true,
                            padding: 5,
                            disabled: true,
                            items: [
                                    {
                                        xtype: 'impactFieldCombo',
                                        itemId: 'TMFyearDistPanel',
                                        fieldLabel: 'Disturbance:',
                                        border: true,
                                        padding: 5,
                                        labelWidth: 80,
                                        width: 160,
                                        store: ["-"].concat(Array(30).fill(1990).map((x, y) => x + y)),
                                        value: '-',

                                        //margin: '74 0 0 -330',
                                        listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'impactFieldCombo',
                                        itemId: 'TMFyearRegrowPanel',
                                        fieldLabel: 'Regrowth:',
                                        border: true,
                                        padding: 5,
                                        labelWidth: 80,
                                        width: 160,
                                        store: ["-"].concat(Array(30).fill(1990).map((x, y) => x + y)),
                                        value: '-',

                                        //margin: '74 0 0 -330',
                                        listeners: {
                                            change: function(checkbox, newValue) {
                                                   this.up('#TMFwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                            }
                                        }
                                    }
                                    ]
                        },//-----------------------------  END of Year Disturbance PANEL items -------------------------------------

                    ],
                    // -----------------------------  END of AssessmentPanel PANEL items -------------------------------------
                    listeners: {
                                QualityAssessmentChange: function () {
                                    var timer = setTimeout(function() {
                                        Ext.ComponentQuery.query('#TMFwindow')[0].parseFlagsFromGui();
                                    }, 500);

                                }
                    }

                }
        );


     },



    parseFlagsFromGui: function () {
        var me = this;
        //alert('Parsing');
        // IDs are stored into the selection store - can be > 1
//        var ID = IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']




        var usr = Ext.ComponentQuery.query('#TMFusername')[0].value;
        var verify = Ext.ComponentQuery.query('#TMFverify')[0].value;  // true or false



        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/"
                + currentdate.getFullYear() + " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"


        // ----- Trajectory ------
        var val = Ext.ComponentQuery.query('#TMFtrajectory')[0].getValue().tval;

        val = typeof (val) != 'undefined' ? val : 0;
        if(val == 1 || val == 2 || val == 7){
            Ext.ComponentQuery.query('#TMFdisturbance')[0].reset();
            Ext.ComponentQuery.query('#TMFdisturbance')[0].setDisabled(true);
            Ext.ComponentQuery.query('#TMFregrowth')[0].reset();
            Ext.ComponentQuery.query('#TMFregrowth')[0].setDisabled(true);
            Ext.ComponentQuery.query('#TMFyearDistPanel')[0].reset();
            Ext.ComponentQuery.query('#TMFyearDistPanel')[0].setDisabled(true);
            Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].reset();
            Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].setDisabled(true);
        };
        // force val to avoid no data or None and enable the GUI on start
        if(val == 3 || val == 4 || val == 5 || val == 6){
                    Ext.ComponentQuery.query('#TMFdisturbance')[0].setDisabled(false);
                    Ext.ComponentQuery.query('#TMFregrowth')[0].setDisabled(false);
                    Ext.ComponentQuery.query('#TMFyearDistPanel')[0].setDisabled(false);
                    Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].setDisabled(false);
        }



        var traj = val;
        var dist = Ext.ComponentQuery.query('#TMFdisturbance')[0].getValue().dval;
        dist = typeof (dist) != 'undefined' ? dist : '';
        var regr = Ext.ComponentQuery.query('#TMFregrowth')[0].getValue().rval;
        regr = typeof (regr) != 'undefined' ? regr : '';

        var YYdist = Ext.ComponentQuery.query('#TMFyearDistPanel')[0].getValue();
        YYdist = typeof (YYdist) != 'None' ? YYdist : '';
        var YYregr = Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].getValue();
        YYregr = typeof (YYregr) != 'None' ? YYregr : '';


        var trajconf = Ext.ComponentQuery.query('#TMFtrajconf')[0].getValue().tcval;
        trajconf = typeof (trajconf) != 'undefined' ? trajconf : '';
        var distconf = Ext.ComponentQuery.query('#TMFdistconf')[0].getValue().dcval;
        distconf = typeof (distconf) != 'undefined' ? distconf : '';
        var regconf = Ext.ComponentQuery.query('#TMFregconf')[0].getValue().rcval;
        regconf = typeof (regconf) != 'undefined' ? regconf : '';
        me.editingContainer.editingMode.flags=[
                        {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': val},
                        {'key':'user', 'value': usr},
                        {'key':'verify', 'value': verify.toString()},
                        {'key':'editdate', 'value': datetime.toString()},
                        //{'key':'sceneids', 'value': sceneids.toString()},
                        {'key':'traj', 'value': val},
                        {'key':'dist', 'value': dist},
                        {'key':'regr', 'value': regr},
                        {'key':'YYdist', 'value': YYdist},
                        {'key':'YYregr', 'value': YYregr},
                        {'key':'trajconf', 'value': trajconf},
                        {'key':'distconf', 'value': distconf},
                        {'key':'regconf', 'value': regconf},
        ];

        //console.log(me.editingContainer.editingMode.flags[0]);
        me.editingContainer.saveForm();


    },

    resetFlagsOnGUI: function () {
        var me = this;
        me.down('#AssessmentPanel').suspendEvents();
        for (var i = 0; i < me.attributeIDS.length; i++) {
            //console.log(me.attributeIDS[i]);
            Ext.ComponentQuery.query('#' + me.attributeIDS[i])[0].reset();
        }
        me.down('#AssessmentPanel').resumeEvents();
    },

    setFlagsToGui: function () {
       var me = this;

        var tmp = '';
        var flag = me.editingContainer.editingMode.flags;
        me.down('#AssessmentPanel').suspendEvents();
        // User
        tmp = flag[1].value;
        if (tmp != '' && tmp != 'None'){
            Ext.ComponentQuery.query('#TMFusername')[0].setValue(tmp);
        };

        // Verify
        tmp = flag[2].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFverify')[0].setValue(tmp);
        };

        // Trajectory
        tmp = flag[3].value;
        //        console.log("--------------  FLAGS  ----------------");
        if (tmp != '' && tmp != 'None' && (isNaN(parseInt(tmp))==false)){
            Ext.ComponentQuery.query('#TMFtrajectory')[0].setValue({tval:parseInt(tmp)});
            if(parseInt(tmp) == 1 || parseInt(tmp) == 2 || parseInt(tmp) == 7){
                Ext.ComponentQuery.query('#TMFdisturbance')[0].reset();
                Ext.ComponentQuery.query('#TMFdisturbance')[0].setDisabled(true);
                Ext.ComponentQuery.query('#TMFregrowth')[0].reset();
                Ext.ComponentQuery.query('#TMFregrowth')[0].setDisabled(true);
                Ext.ComponentQuery.query('#TMFyearDistPanel')[0].reset();
                Ext.ComponentQuery.query('#TMFyearDistPanel')[0].setDisabled(true);
                Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].reset();
                Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].setDisabled(true);
            };
            if(parseInt(tmp) == 0 || parseInt(tmp) == 3 || parseInt(tmp) == 4 || parseInt(tmp) == 5 || parseInt(tmp) == 6){
                        Ext.ComponentQuery.query('#TMFdisturbance')[0].setDisabled(false);
                        Ext.ComponentQuery.query('#TMFregrowth')[0].setDisabled(false);
                        Ext.ComponentQuery.query('#TMFyearDistPanel')[0].setDisabled(false);
                        Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].setDisabled(false);
            };

        };

        // Drivers disturbance
        tmp = flag[4].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFdisturbance')[0].setValue({dval:parseInt(tmp)});
        };

        // Drivers regrowth
        tmp = flag[5].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFregrowth')[0].setValue({rval:parseInt(tmp)});
        };

        // Year disturbance
        tmp = flag[6].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFyearDistPanel')[0].setValue(tmp);
        };

        // Year regrowth
        tmp = flag[7].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFyearRegrowPanel')[0].setValue(tmp);
        };

        // Trajectory confidence
        tmp = flag[8].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFtrajconf')[0].setValue({tcval:parseInt(tmp)});
        };

        // Disturbance confidence
        tmp = flag[9].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFdistconf')[0].setValue({dcval:parseInt(tmp)});
        };

        // Regrowth confidence
        tmp = flag[10].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#TMFregconf')[0].setValue({rcval:parseInt(tmp)});
        };

        me.down('#AssessmentPanel').resumeEvents();
    },

   disableGUI: function (TorF) {
            this.down('#navigationPanel').setDisabled(TorF);
            this.down('#trajectoryPanel').setDisabled(TorF);
            this.down('#driversDistPanel').setDisabled(TorF);
            this.down('#driversRegrowPanel').setDisabled(TorF);
            this.down('#TMFyearPanel').setDisabled(TorF);

   },



   close: function () {
        this.editingContainer.stopEditingMode();
   },


});
