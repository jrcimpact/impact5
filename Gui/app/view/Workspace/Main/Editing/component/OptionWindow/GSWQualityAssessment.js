/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.GSWQualityAssessment', {
    extend: 'IMPACT.view.component.Window',

    requires: [
        'IMPACT.view.component.Field.TextArea'
    ],

    title: 'GSW Quality Assessment Interface',
    itemId: 'GSWwindow',
    resizable: true,
    bodyStyle: 'padding:0px;',
    width: 410,
    height: 360,
    maxHeight: 350,
    maxWidth: 1200,
    layout: 'hbox',
    y: 100,
    collapsible: true,
    closeAction: 'destroy',
    buttons: [],
    border: true,

    legendType: null,
    treeNode: null,
    map: null,
    editingContainer: null,
    legendStore: null,
    items:[
    ],

    //layout: 'vbox',
    collapsible: true,

    attributeIDS: ['GSWverify', 'classVals' ],


     initComponent: function () {
        var me = this;

        me.editingContainer = Ext.ComponentQuery.query('#QualityAssessmentContainer')[0];
        var item = Ext.data.StoreManager.lookup('VectorLegends').__filterByType(me.legendType);
        me.legendStore =  [];
        if(item!==null){
            Ext.Array.forEach(item.classes, function(id) {
                me.legendStore.push({
                    inputValue:  id[0],
                    boxLabel:  id[1],
                    name:  'val',
                    style: {
                                //'background-color': 'lightgreen',
                                //'border': '1px solid black'
                            }
                });
            });
        };
        me.callParent();



        me.add(
                {
                    xtype: 'panel',
                    width: '100%',
                    itemId: 'AssessmentPanel',
                    disabled: false,
                    layout: 'hbox',
                    //overflowY: 'scroll',
                    //overflowX: 'scroll',
                    border: false,
                    items:[
                            {
                                xtype: 'panel',
                                itemId: 'optionPanel',
                                layout: 'vbox',
                                border: false,
                                items:[
                                        {
                                            xtype: 'panel',
                                            itemId: 'directionPanel',
                                            width: 200,
                                            height: 100,
                                            title: "Navigation",
                                            layout: 'vbox',
                                            border: true,
                                            //margins: '5 0 0 0',
                                            padding: 5,
                                            //bodyStyle: 'background-color: #cce6ff;',
                                            items: [
                                                    {
                                                        xtype: 'button',
                                                        itemId: 'GSWPrevFeatId',
                                                        width: 80,
                                                        labelWidth: 40,
                                                        text: 'Prev',
                                                        margin: '5 0 0 10',
                                                        disabled: false,
                                                        listeners: {
                                                            click: function () {
                                                                this.up('#GSWwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                this.up('#GSWwindow').resetFlagsOnGUI();
                                                                this.up('#GSWwindow').editingContainer.__goToFeature('prev', showOnly = this.up('#GSWwindow').down('#GSWshow').getValue().show)
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        itemId: 'GSWNextFeatId',
                                                        text: 'Next',
                                                        width: 80,
                                                        labelWidth: 40,
                                                        disabled: false,
                                                        margin: '-20 0 0 100',
                                                        listeners: {
                                                            click: function () {
                                                                this.up('#GSWwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                this.up('#GSWwindow').resetFlagsOnGUI();
                                                                this.up('#GSWwindow').editingContainer.__goToFeature('next', showOnly = this.up('#GSWwindow').down('#GSWshow').getValue().show)
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        name: 'Lastselected ID',
                                                        itemId: 'AssessmentLastselectedID',
                                                        fieldLabel: 'Last ID', //+ IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
                                                        width: 160,
                                                        disabled: true,
                                                        labelWidth: 50,
                                                        margin: '10 0 0 20',
                                                        value: '-'
                                                    }
                                            ]
                                        },{
                                                xtype: 'panel',
                                                itemId: 'navigationPanel',
                                                width: 200,
                                                height: 200,
                                                title: "Options",
                                                layout: 'vbox',
                                                border: true,
                                                disabled: true,
                                                //margins: '5 0 0 0',
                                                padding: 5,
                                                //bodyStyle: 'background-color: #cce6ff;',
                                                items: [
                                                        {
                                                            xtype: 'radiogroup',
                                                            itemId:'GSWshow',
                                                            columns: 1,
                                                            border: false,
                                                            width: 280,
                                                            labelWidth: 50,
                                                            margin: '20 0 0 10',
                                                            hideLabel: false,
                                                            fieldLabel: 'Show',
                                                            items:[
                                                                {inputValue: 0, boxLabel: 'All', name:'show', checked:true },
                                                                {inputValue: 1, boxLabel: 'Empty', name:'show' },
                                                                {inputValue: 2, boxLabel: 'Verify', name:'show' }
                                                            ]
                                                    },
                                                    {
                                                            xtype: 'textfield',
                                                            itemId: 'GSWusername',
                                                            fieldLabel: 'User',
                                                            width: 150,
                                                            labelWidth: 50,
                                                            value: '',
                                                            allowBlank: true,
                                                            margin: '20 0 0 10',
                                                            regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                                                    },
                                                    {
                                                        xtype: 'checkbox',
                                                        hideLabel: false,
                                                        itemId: 'GSWverify',
                                                        margin: '10 0 0 10',
                                                        fieldLabel: 'Verify ',
                                                        labelWidth: 80,
                                                        width: 160,
                                                        listeners: {
                                                                change: function(checkbox, newValue) {
                                                                       this.up('#GSWwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                                }
                                                            }

                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: "Clear form",
                                                        margin: '10 0 0 60',
                                                        listeners: {
                                                            click: function() {
                                                                        this.up('#GSWwindow').down('#navigationPanel').suspendEvents();
                                                                        this.up('#GSWwindow').resetFlagsOnGUI();
                                                                        this.up('#GSWwindow').down('#navigationPanel').resumeEvents();
                                                                        this.up('#GSWwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange'); // save clear items to DB
                                                                }
                                                         }
                                                    },
                                               ]
                                        }
                                ]



                                            // -----------------------------  END of Navigation PANEL items -------------------------------------
                            },
                            {
                                xtype: 'panel',
                                itemId: 'classPanel',
                                width: 200,
                                height: 300,
                                title: "Classes",
                                layout: 'vbox',
                                border: true,
                                padding: 5,
                                disabled: true,
                                //bodyStyle: 'background-color: #cce6ff;',
                                items: [
                                         {
                                            xtype: 'radiogroup',
                                            itemId:'classVals',
                                            columns: 1,
                                            border: false,
                                            width: 280,
                                            hideLabel: true,
                                            items: me.legendStore,
                                            listeners: {
                                                change: function(checkbox, newValue) {

                                                       this.up('#GSWwindow').down('#AssessmentPanel').fireEvent('QualityAssessmentChange');
                                                }
                                            }
                                        }
                                ]

                        } // -----------------------------  END of Class PANEL items -------------------------------------
                    ],
                    // -----------------------------  END of AssessmentPanel PANEL items -------------------------------------
                    listeners: {
                                QualityAssessmentChange: function () {
                                    var timer = setTimeout(function() {
                                        Ext.ComponentQuery.query('#GSWwindow')[0].parseFlagsFromGui();
                                    }, 500);

                                }
                    }

                }
        );


     },



    parseFlagsFromGui: function () {
        var me = this;
        //alert('Parsing');
        // IDs are stored into the selection store - can be > 1
//        var ID = IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']

        var val = Ext.ComponentQuery.query('#classVals')[0].getValue().val;

        val = typeof (val) != 'undefined' ? val : '';

        var usr = Ext.ComponentQuery.query('#GSWusername')[0].value;
        var verify = Ext.ComponentQuery.query('#GSWverify')[0].value;  // true or false


        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/"
                + currentdate.getFullYear() + " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"

        // columns name are listed in the __get_GSW_editing_missing_attributes()
//            IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
//            IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
//            //IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
//            'lat','lon','user','date','verify','sceneIDs'];


        me.editingContainer.editingMode.flags=[
                        {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': val},
                        {'key':'user', 'value': usr},
                        {'key':'verify', 'value': verify.toString()},
                        {'key':'editdate', 'value': datetime.toString()}
        ];

        me.editingContainer.saveForm();

        //alert('Parsing');
    },

    resetFlagsOnGUI: function () {
        var me = this;
        me.down('#AssessmentPanel').suspendEvents();
        for (var i = 0; i < me.attributeIDS.length; i++) {
            Ext.ComponentQuery.query('#' + me.attributeIDS[i])[0].reset();
        }
        me.down('#AssessmentPanel').resumeEvents();
    },

    setFlagsToGui: function () {
       var me = this;

        var tmp = '';
        var flag = me.editingContainer.editingMode.flags;
        me.down('#AssessmentPanel').suspendEvents();

        // CLASS
        tmp = flag[0].value
        if (tmp != '' && tmp != 'None'){
            Ext.ComponentQuery.query('#classVals')[0].setValue({val:parseInt(tmp)});
        };
        // GSWusername
        tmp = flag[1].value
        if (tmp != '' && tmp != 'None'){
            Ext.ComponentQuery.query('#GSWusername')[0].setValue(tmp);
        };
        // verify
        tmp = flag[2].value
        if (tmp != ''&& tmp != 'None'){
            Ext.ComponentQuery.query('#GSWverify')[0].setValue(tmp);
        }

        me.down('#AssessmentPanel').resumeEvents();
    },

   disableGUI: function (TorF) {
            this.down('#navigationPanel').setDisabled(TorF);
            this.down('#classPanel').setDisabled(TorF);
   },


    close: function () {
        this.editingContainer.stopEditingMode();
    },


});
