/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Workspace.Main.Editing.QualityAssessment', {
    extend: 'Ext.container.Container',

    itemId: 'QualityAssessmentContainer',


    treeNode: null,
    attributeNodes: null,
    QualityAssessmentOptionWindow: null,

    selectionStore: new Ext.data.ArrayStore({
        autoDestroy: true,
        fields: ['id'],
        data: []
    }),
//    Lat : 0,
//    Lon : 0,

    selection_layer: null,

//    selection_layer: new OpenLayers.Layer.Vector("editing_selection", {
//        buffer: 0,
//        displayInLayerSwitcher: false,
//        visible: true,
//        type: 'hidden',
//        style: {
//            fill: false,
//            strokeColor: "#ff0080",
//            strokeWidth: 2
//        }
//    }),

    initComponent: function () {
        var me = this;
        me.editingMode = {
            active: false,
            legendType: null,
            layer: null,
            leftClickMode: 'id',
            clusterItem: IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
            clusterValue: '',
            refreshLayer: false,
            selectOnlyActiveClass: false,
            ActiveClasses: '',
            selectOnlyActiveClass_Layer: IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
            flags: [],
            lat:0,
            lon:0
        };

        me.callParent();
    },

    startEditingMode: function (node) {
        var me = this;
        me.treeNode = node;
        me.attributeNodes = node.childNodes;
        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = workspace.down('#MapPanel').map;
        var treePanel = workspace.down('#LayerTreePanel');

        // set Node settings
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', true, false);
        me.treeNode.set('cls', 'red-bold-node');
        me.treeNode.active_editing = true;

        // in theo only 1
        for (var i in me.attributeNodes) {
            // set default class visualiz
            me.attributeNodes[i].set('checked', true);
            treePanel.fireEvent('checkchange', me.attributeNodes[i], true);
            me.attributeNodes[i].active_editing = true;
            me.attributeNodes[i].data.layer.getSource().updateParams({'selectOnlyActiveClass' : false});

        }


        // Activate editing MODE
        me.editingMode.active = true;
        me.editingMode.layer = layer;
        me.editingMode.legendType = me.treeNode.data.settings_from_DB['legendType'];
        me.editingMode.leftClickMode = 'id';
        me.editingMode.clusterItem = IMPACT.SETTINGS['vector_editing']['attributes']['ID'];
        // replicated on each layer as well but the global one is used on AOI drawing
        me.editingMode.selectOnlyActiveClass = false;

        me.editingMode.ActiveClasses = me.attributeNodes[0].data.layer.getSource().getParams()['classIDs'];  // get list of class ID from one layer ; updated when changing legend visibility
        //me.editingMode.selectOnlyActiveClass_Layer = IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']; // set by default


        if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
            me.QualityAssessmentOptionWindow = Ext.create('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.TMFQualityAssessment', {
                legendType: me.editingMode.legendType,
                treeNode: me.treeNode,
            });

        }

        if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
            me.QualityAssessmentOptionWindow = Ext.create('IMPACT.view.Workspace.Main.Editing.component.OptionWindow.GSWQualityAssessment', {
                legendType: me.editingMode.legendType,
                treeNode: me.treeNode,
            });
        }


        me.QualityAssessmentOptionWindow.show();
        me.QualityAssessmentOptionWindow.toFront();

        me.initFlags();


        // Enable editing options button & window in Toolbar
        Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
//        me.down('#EditingToolsButton').setDisabled(false);
//        me.down('#EditingToolsButton').setVisible(true);


         var source = new ol.source.Vector({
                wrapX: false,
                features: new ol.Collection(),
                params : {
                    buffer: 0,
                }
            });

         me.selection_layer = new ol.layer.Vector({

                title: "Assessment_editing_selection",
                name: "Assessment_editing_selection",

                source: source,
                visible: true,
                isBaseLayer: false,
                buffer: 0,
                type: 'hidden',

                style: new ol.style.Style({
                      stroke: new ol.style.Stroke({
                        color: '#ff0080',
                        width: 1.5
                      })
                    })
            });


        map.addLayer(me.selection_layer);
        me.selection_layer.setZIndex(1000);


        // Disable workspace tools
        workspace.startEditingMode();
        IMPACT.main_map.on('singleclick', me.leftClick);


    },

    stopEditingMode: function () {
        var me = this;
        if (me.editingMode.active == false) {
            return
        }  // after intruduction of REDDCopernicus, stop is in common, test to skip
        var layer = me.treeNode.data.layer;
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];
        var map = workspace.down('#MapPanel').map;

        // Disable editing options button & window
        me.clearSelection();

//        me.down('#EditingToolsButton').setDisabled(true);
//        me.down('#EditingToolsButton').setVisible(false);
        //Ext.ComponentQuery.query('#TMFwindow')[0].destroy();
        me.QualityAssessmentOptionWindow.destroy();

        // Deactivate editing MODE
        me.editingMode.active = false;
        me.editingMode.layer = null;
        me.editingMode.legendType = null;

        // Remove selection layer from map
        map.removeLayer(me.selection_layer);

        for (var i in me.attributeNodes) {
            // set default class visualiz
            me.attributeNodes[i].data.layer.getSource().updateParams({'classVisible' : me.attributeNodes[i].data.layer.getSource().getParams()['classIDs']});
            me.attributeNodes[i].data.layer.getSource().updateParams({'selectOnlyActiveClass' : false});


            //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.attributeNodes[i], 'editing', false, true);
            me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
            me.attributeNodes[i].data.layer.getSource().refresh();
            me.attributeNodes[i].active_editing = false;
        }

        me.treeNode.set('cls', '');
        me.treeNode.active_editing = false;
        //IMPACT.Utilities.LayerSettings.update_settings_from_DB(me.treeNode, 'editing', false, false);
        // Disable workspace tools



        Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
        IMPACT.main_map.un('singleclick', me.leftClick);

        workspace.stopEditingMode();

    },

    leftClick: function (evt) {

        if (typeof evt == 'undefined'){ return}

        var CTRL_pressed = ol.events.condition.platformModifierKeyOnly(evt) || ol.events.condition.shiftKeyOnly(evt);

        var me =  Ext.ComponentQuery.query('#QualityAssessmentContainer')[0];
        //var lon = IMPACT.main_map.getCoordinateFromPixel(evt.coordinate)[0];
        //var lat = IMPACT.main_map.getCoordinateFromPixel(evt.coordinate)[1];
        var wkt = "POINT (" + evt.coordinate[0] + ' ' + evt.coordinate[1] + ")";

        //Ext.ComponentQuery.query('#RecodeTo')[0].close(); // in case is open

        // #####  Show/Hide class  #####
        if(me.editingMode.leftClickMode == 'id'){

           // #####  Select by Polygon  #####
           if(me.editingMode.clusterItem == IMPACT.SETTINGS['vector_editing']['attributes']['ID']){
                me.selectPolygon(wkt, CTRL_pressed);
                if(me.editingMode.refreshLayer==true){
                    me.clearSelection();
                    me.editingMode.refreshLayer = false;
                }
           }
           // #####  Select by Class or other attribute  #####
           else {
                me.selectClass(wkt);
           }
        }




    },


    selectPolygon: function (wkt, CTRL_pressed) {
        var me = this;

        //var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],'lat','lon'];
        if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
            var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                        ,'user','verify','YYYY','MMDD', 'ndvi','nbr'];
        };
        if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {

            var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                        ,'user','verify'
                        , "traj", "dist","regr", "YYdist","YYregr"
                        , "trajconf", "distconf", "regconf"
                        ,'YYYY','MMDD', 'nbr', 'ndvi'
                        ];

        };

        //console.log(items);

        Ext.Ajax.request({
            url: 'retrieve_cluster_class_by_click.py',
            params: {
                "shp": me.editingMode.layer.getSource().getParams()['full_path'],
                "wkt": wkt,
                "EPSG": '3857',
                "items": JSON.stringify(items),
                "geom": true,
                "selectOnlyActiveClass": me.editingMode.selectOnlyActiveClass,
                "selectOnlyActiveClass_Layer": me.editingMode.selectOnlyActiveClass_Layer,
                "activeClasses": me.editingMode.ActiveClasses

            },
            success: function (response) {
                var responseJSON = JSON.parse(response.responseText);
                me.QualityAssessmentOptionWindow.resetFlagsOnGUI();
                if (responseJSON.length == 0) {
                    me.clearSelection();
                    me.QualityAssessmentOptionWindow.disableGUI(true);
                    return;
                }

                me.QualityAssessmentOptionWindow.disableGUI(false);


                me.selectionStore.removeAll();
                me.selection_layer.getSource().getFeaturesCollection().clear();
                // None selected


                var item = responseJSON[0];
                var ID = parseInt(item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']]);


                var YYYYY = item['YYYY'].split(';');
                var MMDD = item['MMDD'].split(';');
                var sceneDates=[];
                for (var i in YYYYY) {
                    sceneDates.push(YYYYY[i]+"-"+MMDD[i]);
                }


                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
                        var flags = [
                                {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': item[IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']]},
                                {'key':'user', 'value': item['user']},
                                {'key':'verify', 'value': item['verify']},
                                {'key':'traj', 'value': item['traj']},
                                {'key':'dist', 'value': item['dist']},
                                {'key':'regr', 'value': item['regr']},
                                {'key':'YYdist', 'value': item['YYdist']},
                                {'key':'YYregr', 'value': item['YYregr']},
                                {'key':'trajconf', 'value': item['trajconf']},
                                {'key':'distconf', 'value': item['distconf']},
                                {'key':'regconf', 'value': item['regconf']},
                                {'key':'sceneDates', 'value': sceneDates }
                        ];
                };


                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
                    var flags = [
                            {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': item[IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']]},
                            {'key':'user', 'value': item['user']},
                            {'key':'verify', 'value': item['verify']},
                            {'key':'sceneDates', 'value': sceneDates }

                    ];
                };


                me.editingMode.lat = item['lat'];
                me.editingMode.lon = item['lon'];

                me.__addSelectedItemToQAStore(item);
                me.loadFlagsFromSHP(flags);
                me.QualityAssessmentOptionWindow.setFlagsToGui();

                me.editingMode.layer.getSource().updateParams({'ids' : ID});
                me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload;
                me.editingMode.layer.getSource().refresh();
            },
            failure: function (response) {
                alert('Error selecting ID');
                me.QualityAssessmentOptionWindow.resetFlagsOnGUI();
                console.log(response);
            }
        });
    },


    selectClass: function (wkt) {
        var me = this;
        // Empty prev selection
        alert("Class? Why here? ")
    },

    clearSelection: function () {
        var me = this;
        // clean editing store; not necessary but ... better to clean in cas of pending items
        me.selectionStore.removeAll();
        me.selection_layer.getSource().getFeaturesCollection().clear();


        me.editingMode.clusterValue = '';

        for (var i in me.attributeNodes) {
            // redraw only if NOT POLYGON SELECTION
            //if (me.attributeNodes[i].data.layer.getSource().get(show_outline_on_selection) != '---') {
            if(me.attributeNodes[i].data.layer.getSource().getParams()['show_outline_on_selection'] != '---'){

                me.attributeNodes[i].data.layer.getSource().updateParams({'clusterItem' : ''});
                me.attributeNodes[i].data.layer.getSource().updateParams({'show_outline_on_selection' : '---'});
                me.attributeNodes[i].data.layer.getSource().updateParams({'ids' : ''});

                me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                me.attributeNodes[i].data.layer.getSource().refresh();
            }
        }
    },

    __addSelectedItemToQAStore: function (item) {
        var me = this;
        var ID = parseInt(item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']]);

        var format = new ol.format.WKT();
        var Geom = item['geom'];
        if(Geom.includes("POINT")){
            if (Geom.split(' ').length > 3){
                //Geom = Geom.replace('POINTS', 'POINT');
                // in case of 3D points
                Geom = 'POINT '+Geom.split(' ')[1]+' '+ Geom.split(' ')[2]+')'
            }
        };
        var feat =  format.readFeature(Geom,
                                   //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                                   );
        feat.setProperties({'id': ID })
        feat.setId(ID);  // used by getFeatureById in selection + ctrl
        me.selectionStore.loadData([[ID]], true);


        // add for click ---- OL10
        var DBF = Ext.ComponentQuery.query('#DbfAttributeTable')[0];
        if(typeof DBF == "undefined"){
            //  add feature to layer
            me.selection_layer.getSource().addFeature( feat );

        }

        // re-enable
        var bbox =  format.readGeometry(Geom,
                           //{ dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857',}
                           ).getExtent();
        IMPACT.main_map.getView().setCenter(ol.extent.getCenter(bbox));
        if (ID == 1){
            IMPACT.main_map.getView().setZoom(15);
        }
        Ext.ComponentQuery.query('#AssessmentLastselectedID')[0].setValue(parseInt(ID));

        if(typeof Ext.ComponentQuery.query('#GEEWindow')[0] !== 'undefined'){
            var LonLat = ol.extent.getCenter(feat.getGeometry().getExtent());
            var GEEInfo = null;
//            if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
//
//                // TO BE merged with GSW
//                GEEInfo = me.editingMode.layer.getSource().getParams()['full_path']
//            };
//            if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
//                //shpWithInfo = me.editingMode.layer.getSource().getParams()['full_path']

                var YYYY = item['YYYY'].split(';');
                 var MMDD = item['MMDD'].split(';');
                var ndvi = item['ndvi'].split(';');
                var nbr = item['nbr'].split(';');
                 var sceneDates=[];
                for (var i in YYYY) {
                    sceneDates.push(YYYY[i]+"-"+MMDD[i]);
                }

                if(sceneDates.slice(-1) == '-'){
                    drop = sceneDates.pop();
                }

                GEEInfo = {'sceneDates':sceneDates,'nbr':nbr,'ndvi':ndvi}
//            };

            Ext.ComponentQuery.query('#GEEWindow')[0].fireEvent('refreshFromShapefileClick', LonLat[0], LonLat[1], GEEInfo);
        }



    },



    __goToFeature: function(prevNext, show="All"){
        var me = this;


        if (me.selectionStore.getCount() == 0 ){
            alert('No feature selected, moving to first feature');
            var currentID = 0;
        } else {
            var currentID = me.selectionStore.getAt(me.selectionStore.getCount()-1).data.id;
        }

        filter='';
        if (show == 'Empty'){
            filter = 'and cast(\"'+IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'] + '\" as integer) == 0';  // 2 fix
        }
        if (show == 'Verify'){
            filter = 'and verify == "true" ';  // 2 fix
        }

//        var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],'lat','lon'];
        if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
            var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                        ,'user','editdate','verify','YYYY','MMDD','nbr','ndvi'];
        };

       if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
            var items = [IMPACT.SETTINGS['vector_editing']['attributes']['ID'], IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                        ,'user','verify'
                        , "traj", "dist","regr", "YYdist","YYregr"
                        , "trajconf", "distconf", "regconf"
                        ,'YYYY','MMDD', 'nbr', 'ndvi'
                        ];
        };



        Ext.Ajax.request({
            url: 'goToPrevNextFeature.py',
            params: {
                "shp": me.editingMode.layer.getSource().getParams()['full_path'],
                "items" : JSON.stringify(items),
                "currFeatureID" : currentID,
                "direction" : prevNext,
                "geom" : true,
                "filter": filter
            },
            success: function(response){

                var responseJSON = JSON.parse(response.responseText);

                if (responseJSON === null || typeof responseJSON[0][IMPACT.SETTINGS['vector_editing']['attributes']['ID']] === 'undefined'){
                        if (prevNext == 'prev'){alert("Begin")} else {alert("End")}
                        return;
                }

                me.selectionStore.removeAll();
                me.selection_layer.getSource().getFeaturesCollection().clear();
                    // None selected

                var ids = '';
                var item = responseJSON[0];
                var ID = parseInt(item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']]);

                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
                    var flags = [
                               {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': item[IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']]},
                                {'key':'user', 'value': item['user']},
                                {'key':'verify', 'value': item['verify']},
                                {'key':'traj', 'value': item['traj']},
                                {'key':'dist', 'value': item['dist']},
                                {'key':'regr', 'value': item['regr']},
                                {'key':'YYdist', 'value': item['YYdist']},
                                {'key':'YYregr', 'value': item['YYregr']},
                                {'key':'trajconf', 'value': item['trajconf']},
                                {'key':'distconf', 'value': item['distconf']},
                                {'key':'regconf', 'value': item['regconf']}
                                //{'key':'sceneDates', 'value': sceneDates }

                    ];
                };
                if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
                    var flags = [
                            {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': item[IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']]},
                            {'key':'user', 'value': item['user']},
                            {'key':'verify', 'value': item['verify']}

                    ];
                };

                    // only 1
                    //var ID = item[IMPACT.SETTINGS['vector_editing']['attributes']['ID']];
                me.__addSelectedItemToQAStore(item);

                me.loadFlagsFromSHP(flags);
                me.QualityAssessmentOptionWindow.setFlagsToGui();

                me.editingMode.layer.getSource().updateParams({'ids' : ID});
                me.editingMode.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                me.editingMode.layer.getSource().refresh();
            },
            failure : function(response){
                 console.log(response);
            }
        });

    },

    initFlags: function () {
        var me = this;
        //alert('into Init, is it necessary ?');
        me.editingMode.flags = [];

       if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'TMF') {
                        //  IDS:   dateHR, pixelHRradio, contextHRRadio, PixelTempRadio, duration,      A B C D
                        //           obsDist1st, obsDist2st,obsDist3st,obsDist4st ,     E
                        //           obsDist1end,obsDist2end,obsDist3end,obsDist4end,
                        //           contextRadio,                                      F
                        //           cntxDist1,cntxDist2,cntxDist3,cntxDist4            G

                        me.editingMode.flags.push(
                                            {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': ''},
                                            {'key':'user', 'value': ''},
                                            {'key':'verify', 'value': ''},
                                            {'key':'editdate', 'value': ''},
                                            //{'key':'sceneids', 'value': sceneids.toString()},
                                            {'key':'traj', 'value': ''},
                                            {'key':'dist', 'value': ''},
                                            {'key':'regr', 'value': ''},
                                            {'key':'YYdist', 'value': ''},
                                            {'key':'YYregr', 'value': ''},
                                            {'key':'trajconf', 'value': ''},
                                            {'key':'distconf', 'value': ''},
                                            {'key':'regconf', 'value': ''},
                                        );
       };

       if (IMPACT.SETTINGS['tool_options']['validationGUI'] == 'GSW') {
            me.editingMode.flags.push(
                        {'key':IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'], 'value': ''},
                        {'key':'user', 'value': ''},
                        {'key':'verify', 'value': ''},
                        {'key':'editdate', 'value': ''}
                        );
       };

    },


    loadFlagsFromSHP: function (flags) {
        var me = this;
        //console.log('Loading')
        me.initFlags();
        if (flags == '' || flags == '0') {
            console.log('No data')
            return
        }


//        flags = JSON.parse(flags)
//
        me.editingMode.flags=flags;
    },


//    saveFormTMF: function () {
//
//        var me = this;
//        var map = IMPACT.main_map;
//        var saveValue = me.editingMode.flags[0];
//        saveToClassValue = 100;
//
//        if (saveValue.length == 0) {
//            console.log('NOTHING to SAVE');
//            return;
//        }
//
//
//        // -------------------------   SELECTION STORE is NOT EMPTY -------------------------------
//        // selection by ID, poly or DBF
//        // ----------------------------------------------------------------------------------------
//        var list_ids = [];
//        Ext.each(me.selectionStore.data.items, function (value) {
//            list_ids.push(value.data.id)
//        });
//
//
//        var extent = '';
//
//        var newParams = {
//            "shp_name": me.editingMode.layer.getSource().getParams()['full_path'],
//            "extent": JSON.stringify(extent),
//            "saveValue": JSON.stringify(saveValue),
//            "saveToClass": IMPACT.SETTINGS['vector_editing']['attributes']['class_t1'],
//            "saveToClassValue": saveToClassValue,
//            "saveTo": IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1'],
//            "whereItem": me.editingMode.clusterItem,
//            "whereValue": me.editingMode.clusterValue,
//
//            "activeClasses": me.editingMode.ActiveClasses,
//            "selectOnlyActiveClass": me.editingMode.selectOnlyActiveClass,
//            "selectOnlyActiveClass_Layer": me.editingMode.selectOnlyActiveClass_Layer,
//
//
//            "list_ids": JSON.stringify(list_ids),
//            "fid": IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
//
//
//        };
//
//
//        Ext.Ajax.request({
//            url: 'save_info_on_shapefile_REDDCopernicus.py',
//            params: newParams,
//            success: function (response) {
//                var responseText = response.responseText.replace(/(\r\n|\n|\r)/gm, "");
//                if (responseText == "-1") {
//                    alert("Recode failed")
//                } else {
//                    for (var i in me.attributeNodes) {
//                        me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
//                        me.attributeNodes[i].data.layer.getSource().refresh();
//                    }
//
//                }
//            },
//            failure: function (response) {
//                alert('error');
//                console.log(response);
//            }
//        });
//
//
//    },

    saveForm: function () {

        var me = this;
        var map = IMPACT.main_map;
        var saveValue = me.editingMode.flags;


        if (saveValue.length == 0) {
            console.log('NOTHING to SAVE');
            return;
        }


        // -------------------------   SELECTION STORE is NOT EMPTY -------------------------------
        // selection by ID, poly or DBF
        // ----------------------------------------------------------------------------------------
        var list_ids = [];
        Ext.each(me.selectionStore.data.items, function (value) {
            list_ids.push(value.data.id)
        });


        var extent = '';

        var newParams = {
            "shp_name": me.editingMode.layer.getSource().getParams()['full_path'],
            "saveFieldValue": JSON.stringify(saveValue),
            "list_ids": JSON.stringify(list_ids),
            "fid": IMPACT.SETTINGS['vector_editing']['attributes']['ID'],
        };


        Ext.Ajax.request({
            url: 'save_info_on_shapefile_QAssessment.py',
            params: newParams,
            success: function (response) {
                var responseText = response.responseText.replace(/(\r\n|\n|\r)/gm, "");
                if (responseText == "-1") {
                    alert("Recode failed")
                } else {
                    for (var i in me.attributeNodes) {
                        me.attributeNodes[i].data.layer.getSource().updateParams({'modification_date': Date.now()});  // fake params to force reload
                        me.attributeNodes[i].data.layer.getSource().refresh();
                    }

                }
            },
            failure: function (response) {
                alert('error');
                console.log(response);
            }
        });


    }


});