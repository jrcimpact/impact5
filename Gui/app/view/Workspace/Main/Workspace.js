/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.Workspace', {
    requires: [
        'IMPACT.view.Workspace.Main.LayerPanel',
        //'IMPACT.view.Workspace.Main.TreePanel',
        'IMPACT.view.Workspace.Main.MapPanel'
    ],

    extend: 'Ext.panel.Panel',

    itemId: 'MainWorkspace',
    id : 'MainWorkspace',
    title : 'Main Panel',

    layout: 'border',

    items: [
        {
            xtype: 'MainMapPanel',
            region: 'center',
        },
        {
            xtype: 'MainLayerPanel',
            region: 'west',
        }
    ],

    editingMode: false,

    startEditingMode: function(){
//          alert('Workspace Editing Start');
        var me = this;
//        var viewport = me.up('#IMPACTViewport');
//
        me.editingMode = true;
//        me.down('#MainToolbar').toggleEditingMode(true);
//
//        viewport.down('#UserSettingsTab').setDisabled(true);
//        if( viewport.down('#validation_tab')){
//            viewport.down('#validation_tab').setDisabled(true);
//        }
//        viewport.down('#logTab').setDisabled(true);
//        viewport.down('#ProcessingPanel').collapse();
    },

    stopEditingMode: function(){
//    alert('Workspace Editing Stop');
        var me = this;
//        var viewport = me.up('#IMPACTViewport');
//
        me.editingMode = false;
//        me.down('#MainToolbar').toggleEditingMode(false);
//
//        viewport.down('#UserSettingsTab').setDisabled(false);
//        if( viewport.down('#validation_tab')){
//            viewport.down('#validation_tab').setDisabled(false);
//        }
//        viewport.down('#logTab').setDisabled(false);
//        viewport.down('#ProcessingPanel').expand();
    }

});