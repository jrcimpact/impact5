/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.Toolbar', {
    requires:[

         'IMPACT.view.Workspace.Main.Editing.Vector',
         'IMPACT.view.Workspace.Main.Editing.Raster',
    ],

    extend: 'Ext.toolbar.Toolbar',

    itemId: 'MainToolbar',

    overflowHandler: 'menu',

    map: null,
    dock: 'top',

    drawLayer: null,
    lastPressed: 'pan',

    planetWindow: null,

    beforeRender: function() {

        var me = this;
        me.map = IMPACT.main_map;

        // ############# Pan & Query MODE  #############
        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    itemId: 'panModeButton',
                    tooltip: "Pan Map",
                    // empty to keep the default as active
                    //control: new ol.interaction.DragPan(),
                    //map: me.map,
                    iconCls: 'xfa fa fa-hand-rock',
                    group: "draw",
                    toggleGroup: "draw",
                    allowDepress: false,
                    pressed: true,
                    listeners: {
                        'click': function(button, e){
                            me.setPanMode();
                        },
                    }
                })
            )
        );

        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction',{
                    itemId: 'queryModeButton', // id: 'main_info_butt_id',
                    tooltip: 'Query layer by location (Shortcut: (i) on pointer)',

                    listeners: {
                        'click': function(button, e){
                            me.setQueryMode();
                        }
                    },
                    //map: me.map,
                    iconCls: 'fa fa-lg fa-blue fa-info-circle',
                    group: "draw",
                    toggleGroup: "draw",
                    allowDepress: false,
                    pressed: false,
                })
            )
        );

        me.add("-");

       // Add controls
        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    iconCls: 'fa fa-lg fa-blue fa-arrow-left',
                    tooltip: "Previous zoom",
                    itemId: 'navPrevButt',
                    disabled: true,
                    listeners: {
                        'click': function(evt){
                            IMPACT.main_mapPanel.handleNavHistory(null, 'Prev');
                        }
                    },
                })
            )
        );
        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    iconCls: 'fa fa-lg fa-blue fa-arrow-right',
                    tooltip: "Next zoom",
                    itemId: 'navNextButt',
                    disabled: true,
                    listeners: {
                        'click': function(evt){
                            IMPACT.main_mapPanel.handleNavHistory(null, 'Next');
                        }
                    },
                })
            )
        );
        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    iconCls: 'fas fa-arrows-alt',
                    tooltip: "Systematic Pan",
                    disabled: false,
                    listeners: {
                        click: function() {

                            if( Ext.ComponentQuery.query('#MapPanGui').length == 0 ){
                                var SystematicPanWin = Ext.create('IMPACT.view.Workspace.Main.component.MapPanGui');
                            } else {
                                Ext.ComponentQuery.query('#MapPanGui')[0].destroy();
                            }


                        }
                    }
                })
            )
        );

        me.add({
            xtype: 'textfield',
            name: 'LLtext',
            //tooltip: 'Goto Lat, Lon, Zoom (optional) ',
            width: 130,
            itemId: 'GOTO_coord_id',
            allowBlank: true  // requires a non-empty value
        });
        me.add({
            xtype: 'button',
            itemId: 'GOTO_applyChanges',
            tooltip: 'Goto Lat,Lon, Zoom (optional)',

            text:'Go',
            width: 50,

            handler: function () {

                var map = Ext.ComponentQuery.query('#MapPanel')[0].map;

                var val = Ext.ComponentQuery.query('#GOTO_coord_id')[0].getValue();
                var Lat = val.split(',')[0];
                var Lon = val.split(',')[1];
                var Z = val.split(',')[2];

                if(typeof(Z)==='undefined' || Z.length == 0){
                    Z = me.map.getView().getZoom.toFixed(0);
                }

                //var LL = new OpenLayers.LonLat(Lon,Lat);
                var LL2 = ol.proj.transform([Lon,Lat], IMPACT.SETTINGS['WGS84'], IMPACT.SETTINGS['WGS84_mercator']);
                map.getView().setCenter([LL2[0],LL2[1]]);
                map.getView().setZoom(Z);

            }
        });


        me.add("-");
        me.add("-");
        // ##############  GEE Profiler  ##############
        me.add(
            Ext.create('Ext.button.Button', {
                text: "GEE Time series",
                iconCls: 'gee_logo',
                tooltip: "Show GEE Imagery",
                itemdId: 'GEEButton',
                listeners: {
                    click: function() {
                        //var GEEWindow = Ext.ComponentQuery.query('#GEEWindow')[0] || Ext.create('IMPACT.view.Workspace.Main.component.GeeWindow');
                        if( Ext.ComponentQuery.query('#GEEWindow').length == 0 ){
                            var GEEWindow = Ext.create('IMPACT.view.Workspace.Main.component.GeeWindow');
                            GEEWindow.show();
                            GEEWindow.fireEvent('refreshFromShapefileClick', IMPACT.main_map.getView().getCenter()[0], IMPACT.main_map.getView().getCenter()[1], null);

                        } else {
                            Ext.ComponentQuery.query('#GEEWindow')[0].destroy()
                        }


                    }
                }
            })
        );

        // ##############  Planet Profiler  ##############
        me.add(
            Ext.create('Ext.button.Button', {
                text: "Planet Imagery",
                iconCls: 'planet_logo',
                tooltip: "Show Planet Data",
                itemdId: 'PlanetButton',
                listeners: {
                    click: function() {

                        if( Ext.ComponentQuery.query('#PlanetWindow').length == 0 ){
                            var PlanetWindow = Ext.create('IMPACT.view.Workspace.Main.component.PlanetWindow');
                            PlanetWindow.show();

                        } else {
                            if (Ext.ComponentQuery.query('#PlanetWindow')[0].hidden){
                                    Ext.ComponentQuery.query('#PlanetWindow')[0].show();
                            } else {
                                    Ext.ComponentQuery.query('#PlanetWindow')[0].hide();
                            }
                        }

                    }
                }
            })
        );
        //me.add("-");
        me.add("-");
        me.add("-");

        // ##############  Color Palettes  ##############
        me.add(
            Ext.create('Ext.button.Button', {
                text: "Vector Legends",
                itemdId: 'VectorLegendsButton',
                listeners: {
                    click: function() {
                        var ColorPaletteVectorWindow = Ext.ComponentQuery.query('#ColorPaletteVectorWindow')[0] || Ext.create('IMPACT.view.component.ColorPalette.Vector.Window');
                        ColorPaletteVectorWindow.showHide();
                    }
                }
            })
        );
        me.add(
            Ext.create('Ext.button.Button', {
                text: "Raster Styles",
                itemdId: 'RasterStylesButton',
                listeners: {
                    click: function() {
                        var ColorPaletteRasterWindow = Ext.ComponentQuery.query('#ColorPaletteRasterWindow')[0] || Ext.create('IMPACT.view.component.ColorPalette.Raster.Window');
                        ColorPaletteRasterWindow.showHide();
                    }
                }
            })
        );
        //me.add("-");






        // ##############  EDITING  ##############
        me.add(
            Ext.create("IMPACT.view.Workspace.Main.Editing.Vector")

        );
        me.add(
            Ext.create("IMPACT.view.Workspace.Main.Editing.QualityAssessment")
        );
        me.add(
            Ext.create("IMPACT.view.Workspace.Main.Editing.Raster")
        );

        me.add("->");

        me.callParent();

    },



    /**
      * Remove a given (or all) feature
      */

    /**
     * Set mode to pan
     */

    setPanMode: function(){
        var me = this;
//        console.log('--------------  set pan mode');
        if (typeof IMPACT.main_mapPanel != 'undefined') {IMPACT.main_mapPanel.mapClickMode = 'pan';}

        me.down('#panModeButton').toggle(true);
        me.down('#queryModeButton').toggle(false);

        // ------------------------------------------------------------------------------------------------
        // -------------------------------normal map PAN + feat draw --------------------------------------
        // ------------------------------------------------------------------------------------------------
        if (typeof Ext.ComponentQuery.query('#EditingOptionWindowVector')[0] == 'undefined') {
            Ext.ComponentQuery.query('#MainToolbar2')[0].removeFeature();
            Ext.ComponentQuery.query('#MainToolbar2')[0].toggleDrawFeatures(false);
            Ext.ComponentQuery.query('#drawRectangleButton')[0].setDisabled(false);
            Ext.ComponentQuery.query('#drawPolygonButton')[0].setDisabled(false);
            Ext.ComponentQuery.query('#drawLineButton')[0].setDisabled(false);

        } else {
        // ------------------------------------------------------------------------------------------------
        // ------------------------------- Vector Editing, disable feat draw  -----------------------------
        // ------------------------------------------------------------------------------------------------


            IMPACT.main_mapPanel.mapClickMode = 'editing';
            Ext.ComponentQuery.query('#MainToolbar2')[0].removeFeaturebyForce();

    //        manually force toolbar off
            Ext.ComponentQuery.query('#drawRectangleButton')[0].setDisabled(true);
            Ext.ComponentQuery.query('#drawPolygonButton')[0].setDisabled(true);
            Ext.ComponentQuery.query('#drawLineButton')[0].setDisabled(true);
            Ext.ComponentQuery.query('#MainToolbar2')[0].toggleDrawFeatures(false);




        }


    },

    /**
     * Set mode to query
     */
    setQueryMode: function(){
        var me = this;
//        console.log('--------------  set query mode');
        if (typeof IMPACT.main_mapPanel != 'undefined') {IMPACT.main_mapPanel.mapClickMode = 'query';}
        Ext.ComponentQuery.query('#MainToolbar2')[0].removeFeature();
        Ext.ComponentQuery.query('#MainToolbar2')[0].toggleDrawFeatures(false);
        me.down('#panModeButton').toggle(false);
        me.down('#queryModeButton').toggle(true);
    },

});



Ext.define('IMPACT.view.Workspace.Main.Toolbar2', {
    requires:[
        'IMPACT.view.Workspace.Main.component.SaveToSHPWindow',
    ],

    extend: 'Ext.toolbar.Toolbar',

    itemId: 'MainToolbar2',
    overflowHandler: 'menu',

    map: IMPACT.main_map,
    dock: 'top',

    drawLayer: null,   // not used now

    drawingFeatures: null,
    drawSource: null,
    //drawInteraction: null,
    selectInteraction: null,



    lastPressed: 'pan',

    beforeRender: function() {

        var me = this;
        //me.map = IMPACT.main_map;
        // ##############  Draw  ##############

        me.drawingFeatures = IMPACT.Utilities.OpenLayers.getTemporaryLayer(IMPACT.main_map).getSource().getFeaturesCollection();
        me.drawSource = IMPACT.Utilities.OpenLayers.getTemporaryLayer(IMPACT.main_map).getSource();

        me.selectInteraction = new ol.interaction.Select({
                      condition: ol.events.condition.click,
                      layers:[IMPACT.Utilities.OpenLayers.getTemporaryLayer(IMPACT.main_map)],
                      //style: selectStyle,
                      features: new ol.Collection()
        });
        me.selectInteraction.on('select', function(e) {
            var selectCollection = me.selectInteraction.getFeatures();
            if (selectCollection.getLength() > 0) {
              me.removeFeature(selectCollection.item(0));
            }

        });



        me.add(
               { xtype: 'tbtext', text: 'Draw -> ' }
        );

        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    id: 'drawRectangleButton',
                    iconCls: 'fa fa-vector-square fa-orange',
                    text: "Rectangle",
                    tooltip: "Draw Rectangle",
                    group: "draw",
                    toggleGroup: "draw",
                    control: new ol.interaction.Draw({
                                source: me.drawSource,
                                type: 'Circle',                     // type of draw
                                stopClick: true,
                                geometryFunction:ol.interaction.Draw.createBox()
                           }),
                    //map: me.map,
                    allowDepress: false,
                    listeners: {
                        'click': function(button, e){

                            if ( me.drawingFeatures.getLength() > 0 && me.lastPressed == 'line'){
                                     // user may want to keep features on layer
                                     Ext.Msg.show({
                                        title:'Remove features',
                                        msg: 'Different topology not allowed. Remove all features?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: function(e){
                                            if(e == 'yes'){
                                                me.drawingFeatures.clear();
                                            }
                                            //me.toggleSaveFeatures(false); // do not remove but do not save
                                        }
                                    });
                            }
                            me.toggleDrawFeatures(true);
                            me.lastPressed = 'poly';
                        },
                    }
                })
            )
        );

        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    id: 'drawPolygonButton',
                    iconCls: 'fa fa-draw-polygon fa-orange',
                    text: "Polygon",
                    tooltip: "Draw Polygon",
                    group: "draw",
                    toggleGroup: "draw",
                    control: new ol.interaction.Draw({
                                source: me.drawSource,
                                type: 'Polygon',
                                stopClick: true,
//                                condition: (e) => {
//                                        console.log(e);
//                                        return false;
//                                      },
                                                    // type of draw
                                //geometryFunction:ol.interaction.Draw.createBox()
                           }),
                    //map: me.map,
                    allowDepress: false,
                    listeners: {
                        'click': function(button, e){
                            if ( me.drawingFeatures.getLength() > 0 &&  me.lastPressed == 'line'){
                                     // user may want to keep features on layer
                                     Ext.Msg.show({
                                        title:'Remove features',
                                        msg: 'Different topology not allowed. Remove all features?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: function(e){
                                            if(e == 'yes'){
                                                me.drawingFeatures.clear();
                                            }
                                            //me.toggleSaveFeatures(false); // do not remove but do not save
                                        }
                                    });
                            }
                            me.lastPressed = 'poly';
                            me.toggleDrawFeatures(true);
                        },
                    }
                })
            )
        );

        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    id: 'drawLineButton',
                    iconCls: 'fa fa-lg fa-orange fa-minus',
                    text: "Line",
                    tooltip: "Draw Line",
                    group: "draw",
                    toggleGroup: "draw",
                    control: new ol.interaction.Draw({
                                source: me.drawSource,
                                stopClick: true,
                                type: 'LineString',                     // type of draw
                                //geometryFunction:ol.interaction.Draw.createBox()
                           }),


                    //map: me.map,
                    allowDepress: false,
                    listeners: {
                        'click': function(button, e){

                            if ( me.drawingFeatures.getLength() > 0 && me.lastPressed == 'poly'){
                                 // user may want to keep features on layer
                                 Ext.Msg.show({
                                    title:'Remove features',
                                    msg: 'Different topology not allowed. Remove all features?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: function(e){
                                        if(e == 'yes'){
                                            me.drawingFeatures.clear();
                                        };
                                    }
                                });
                            }
                            me.lastPressed = 'line';

                        },
                        'bdlclick':function(button, e){
                            console.log('close');
                        }
                    }
                })
            )
        );


        //me.add("-");
        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    id: 'editPolygonButton',
                    iconCls: 'fa fa-lg fa-orange fa-edit',
                    text: "Edit",
                    tooltip: "Edit Geometry",
                    group: "draw",
                    toggleGroup: "draw",
                    control: new ol.interaction.Modify({
                                source: me.drawSource,
                                //type: 'LineString',                     // type of draw
                                //geometryFunction:ol.interaction.Draw.createBox()
                           }),
                    //map: me.map,
                    allowDepress: false,
                    hidden: true
                })
            )
        );

        me.add(
            Ext.create('Ext.button.Button',
                Ext.create('IMPACT.view.Workspace.Main.component.MapAction', {
                    id: 'removePolygonButton',
                    iconCls: 'fa fa-lg fa-red fa-times',
                    text: "Del One",
                    tooltip: "Delete One",
                    group: "draw",
                    toggleGroup: "draw",
                    control: me.selectInteraction,

                    //map: me.map,
                    allowDepress: false,
                    hidden: true
                })
            )
        );

        me.add(
            Ext.create('Ext.button.Button',{
                iconCls: 'fa fa-lg fa-red fa-trash',
                id: 'removeAllButton',
                text: "Del All",
                tooltip: "Delete All Features",
                listeners: {
                    'click': function(e) {
                        me.removeFeature();
                    }
                },
                disabled: false,
                allowDepress: false,
                hidden: true
            })
        );



        me.add(
            Ext.create('Ext.button.Button',{
                id: 'saveFeaturesButton',
                iconCls: 'fa fa-lg fa-blue fa-save',
                text: "Save to Shapefile",
                tooltip: "Save features to Shapefile",
                disabled: true,
                hidden: true,
                listeners: {
                    click: function(button, e) {
                        if(typeof Ext.ComponentQuery.query('#SaveToSHPWindow')[0] == "undefined"){
                            Ext.create('IMPACT.view.Workspace.Main.component.SaveToSHPWindow').showAt(parseInt(e.getXY()[0])-50, parseInt(e.getXY()[1])+20);
                        }
                    }
                }
            })
        );

        me.add("->");

        me.callParent();

    },


    /**
      * Remove a given (or all) feature
      */

    removeFeaturebyForce:function(){
        var me = this;
        if(typeof me.drawingFeatures != "undefined"){
            me.drawingFeatures.clear();
        }
    },

    removeFeature: function(feature){
        var me = this;
        if(typeof feature != "undefined"){
            me.drawSource.removeFeature(feature);

        } else {
            // remove all features
            if (me.drawingFeatures.getLength() > 0 ){

                 // user may want to keep features on layer
                 Ext.Msg.show({
                    title:'Remove features',
                    msg: 'Remove all temporary features?',
                    buttons: Ext.Msg.YESNO,
                    //modal: true,
                    fn: function(e){
                        if(e == 'yes'){
                            me.drawingFeatures.clear();
                        }
                    }
                });
            } else {
                me.drawingFeatures.clear();
            }

        }
    },


    /**
     * Enable/Disable editing mode
     */
    toggleEditingMode: function(enable){
        var me = this;
        enable = enable || false;
        var toolbar = Ext.ComponentQuery.query('#MainToolbar')[0];
        toolbar.setPanMode();
        toolbar.down('#panModeButton').setDisabled(enable);
        toolbar.down('#queryModeButton').setDisabled(enable);
        me.down('#drawRectangleButton').setDisabled(enable);
        me.down('#drawPolygonButton').setDisabled(enable);
        me.down('#drawLineButton').setDisabled(enable);

        if(enable){ IMPACT.main_mapPanel.mapClickMode='pan';}
    },

    /**
     * Enable/Disable additional draw feature (edit, remove and save features)
     */
    toggleDrawFeatures: function(enable){
        var me = this;
        enable = enable || false;
        me.down('#editPolygonButton').setVisible(enable);
        me.down('#removePolygonButton').setVisible(enable);
        me.down('#removeAllButton').setVisible(enable);
        me.down('#saveFeaturesButton').setVisible(enable);

        if(enable){ IMPACT.main_mapPanel.mapClickMode='pan';}

        if(!enable && typeof Ext.ComponentQuery.query('#SaveToSHPWindow')[0] != "undefined"){
            Ext.ComponentQuery.query('#SaveToSHPWindow')[0].close();
        }

    },

    /**
     * Enable/Disable save features button
     */
    toggleSaveFeatures: function(enable){
        var me = this;

        if(enable == true || enable == false){
            me.down('#saveFeaturesButton').setDisabled(!enable);
        }
        if( me.drawingFeatures.getLength() == 0){
            me.down('#saveFeaturesButton').setDisabled(true);
        } else {me.down('#saveFeaturesButton').setDisabled(false);}


    },



});


