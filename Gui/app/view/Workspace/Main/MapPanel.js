/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Workspace.Main.MapPanel', {
    requires: [
        'IMPACT.view.Workspace.Main.Toolbar',
        'IMPACT.view.Workspace.Main.component.MouseCoordinates'
    ],
    extend: 'Ext.panel.Panel',
    alias: 'widget.MainMapPanel',

    itemId: 'MapPanel',
    html:'<div id="Impact_map_div" style="height: 100%; width: 100%"></div>',

    layout: 'fit',
    border: false,

    map: null,
    mapRendered: false,

    systematicPanWin: null,
    lastMouseEvt:null,

    mapClickMode: null,

    navHistoryList: null,
    navHistoryPos: null,
    navHistoryMax: null,


    initComponent:function() {
        var me = this;

        me.mapClickMode = 'pan';
        me.navHistoryList = [];
        me.navHistoryMax = 20;
        me.navHistoryPos = -1;


        me.mapcoordinates = Ext.create('IMPACT.view.Workspace.Main.component.MouseCoordinates');

        view = new ol.View({
              center: IMPACT.SETTINGS['initialCenter'],
              zoom: IMPACT.SETTINGS['initialZoom'],
              extent: IMPACT.SETTINGS['maxExtent'],
              constrainResolution: true
            });

        scaleControl = new ol.control.ScaleLine({
          units: 'metric',
          bar: true,
          steps: 4,
          text: true,
          minWidth: 100,
          maxWidth: 200,
          className: 'ol-scale-bar',
        });

        zoomMaxExtentControl = new ol.control.ZoomToExtent({
                  extent: IMPACT.SETTINGS['maxExtent'],
        });

        zoomSlider = new ol.control.ZoomSlider();

        me.mousePositionControl = new ol.control.MousePosition({
              coordinateFormat: function(coordinate) {
                                  return ol.coordinate.format(coordinate, '{y}, {x}', 4);  // return Lat Lon instead of Lon Lat
                                },
              projection: 'EPSG:4326',
              // comment the following two lines to have the mouse position
              // be placed within the map.
              className: 'custom-mouse-position',
              // not ready now, create in afterrender
              //target: document.getElementById('myCoordinatesId')
            });

        me.map = new ol.Map({
            // not ready now, set in afterrender
            //target: 'Impact_map_div',
            keyboardEventTarget: document,
            controls:  ol.control.defaults.defaults().extend([
                                                               scaleControl,
                                                               //me.mousePositionControl,
                                                               zoomMaxExtentControl,
                                                               zoomSlider
                                                              ]),
            renderer: 'canvas',
            layers: [
            ],
            view: view
        });






        var TMPLayer = IMPACT.OpenLayers.Draw.createLayer();
        TMPLayer.setVisible(true);
        TMPLayer.setZIndex(1000);  // initial fake value - reset by the function at every layer add / remove
        me.map.addLayer(TMPLayer);




        me.dockedItems = [];

        me.bbar = [
            '->',
            this.mapcoordinates
        ];


        // set here after change to OL10 due to target into DIV
        IMPACT.main_map = me.map;


        me.callParent(arguments);
    },

    listeners:{
        afterrender:function(){

            var me = this;

            me.map.setTarget('Impact_map_div');
            me.mousePositionControl.setTarget(document.getElementById('myCoordinatesId'));
            me.map.addControl(me.mousePositionControl);

            me.dockedItems.add([Ext.create('IMPACT.view.Workspace.Main.Toolbar'), Ext.create('IMPACT.view.Workspace.Main.Toolbar2')]);


            if(IMPACT.GLOBALS['online']==="True"){
                if(IMPACT.SETTINGS['wms_options']['ortofoto']==="true"){
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('ortofoto_2019'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('ortofoto_2016'));
                }
                if(IMPACT.SETTINGS['wms_options']['tmf']==="true"){
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('TMF'));
                }
                if(IMPACT.SETTINGS['wms_options']['s2']==="true"){
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('GFC2020'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2019-2020'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2020'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2018-2019'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2019'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2015-2018'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2_2018'));
                    me.map.addLayer(IMPACT.Utilities.OpenLayers.createWMSLayer('sentinel2'));
                 }
                me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('Esri'));
                me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('google_satellite'));
                me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('Planet_NICFI'));
                me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('OSM'));
            }
            me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('blue_marble'));
            me.map.addLayer(IMPACT.Utilities.OpenLayers.createBackgroundLayer('none'));




            // defined as fn to access it from ToolBar and disable event on Perv Next History button
            me.activateMoveEndFn();
//            me.map.on('moveend', me.moveEndFn);

            // -----------  Flag to control the position of mouse while query the map or pan with arrows ----------
            me.map.getViewport().addEventListener('mouseout', function(evt){
                me.lastMouseEvt = false;
            }, false);

            me.map.getViewport().addEventListener('mousemove', function(evt){
                me.lastMouseEvt = evt;
            }, false);


            me.map.on('click', function(evt){
                // valid for mouse click but not for KeyI
                if( me.mapClickMode == 'query'){
                    me.queryLayersByLocation(evt);
                }

            });


            me.map.on('keydown', function(evt) {
                if( Ext.ComponentQuery.query('#MapPanGui').length != 0 ){
                    var myMapKeys = ["ArrowUp", "ArrowDown", "ArrowRight", "ArrowLeft"];
                    if(myMapKeys.includes(evt.originalEvent.code) && me.lastMouseEvt){
                        if( Ext.ComponentQuery.query('#MapPanGui').length == 0 ){
                            var systematicPanWin = Ext.create('IMPACT.view.Workspace.Main.component.MapPanGui');
                        }else{
                            var systematicPanWin = Ext.ComponentQuery.query('#MapPanGui')[0];
                        }
                        systematicPanWin.__applyShift(evt.originalEvent.code);

                    }
                };
                if(evt.originalEvent.code == 'KeyI' && me.lastMouseEvt){
                      // add the map coordinates to the event from key (without mouse position) to reuse the same structure evt
                      me.lastMouseEvt.coordinate = IMPACT.main_map.getCoordinateFromPixel([me.lastMouseEvt.layerX,me.lastMouseEvt.layerY]);
                      me.queryLayersByLocation(me.lastMouseEvt);
                      // reset Pan mode - by default on close of query Window
                      Ext.ComponentQuery.query('#MainToolbar')[0].setPanMode();
                };

            });





        },
        resize: function () {
            //console.log('resize');
            this.map.updateSize();
        }

    },



    moveEndFn: function(evt){
            var me = this;

            // triggered by event so me is not referring to MapPanel
            // event has me.map referrer but not the rest
            if( typeof evt == 'undefined') return
            if( typeof IMPACT.main_map == 'undefined') return
            var extent = IMPACT.main_map.getView().calculateExtent(IMPACT.main_map.getSize());
            IMPACT.main_mapPanel.handleNavHistory(extent, null);

            if( Ext.ComponentQuery.query('#PlanetWindow').length > 0 && Ext.ComponentQuery.query('#PlanetWindow')[0].hidden == false){

                var PlanetItem = Ext.ComponentQuery.query('#PlanetWindow')[0];
                PlanetItem.view.fit([extent[0],extent[1],extent[2],extent[3]],IMPACT.main_map.getSize());
                PlanetItem.addCenterPoint([IMPACT.main_map.getView().getCenter()[0],IMPACT.main_map.getView().getCenter()[1]]);
            }

            // -----   update LL and Zoom in GoTo text item  -------
            var zoomLevel =IMPACT.main_map.getView().getZoom().toFixed(0);
            var LL2 = IMPACT.main_map.getView().getCenter();
            var LL = ol.proj.transform([LL2[0],LL2[1]], IMPACT.SETTINGS['WGS84_mercator'], IMPACT.SETTINGS['WGS84']);
            var GoToButt = Ext.ComponentQuery.query('#GOTO_coord_id')[0];
            if(typeof(GoToButt)!="undefined"){
                GoToButt.setValue(LL[1].toFixed(4).toString()+','+LL[0].toFixed(4).toString()+','+zoomLevel);
            }

        },

    activateMoveEndFn: function(){
        var me = this;
        me.map.on('moveend', me.moveEndFn);
    },

    deactivateMoveEndFn: function(){
        var me = this;
        me.map.un('moveend', me.moveEndFn);
    },


    queryLayersByLocation: function(evt){
        var me = this;
        var LLcoord = ol.proj.transform(evt.coordinate, IMPACT.SETTINGS['WGS84_mercator'],IMPACT.SETTINGS['WGS84']);
        var visible_layers = [];
        var workspace = Ext.ComponentQuery.query('#MainWorkspace')[0];

        var recode_window = Ext.ComponentQuery.query('#EditingOptionWindowRaster')[0];
        // query  while editing mode
        // to be extended to Vector editing
        if (workspace.editingMode == true && (typeof recode_window != 'undefined')){
            // if the band is rovided ' in raster editing mode, the query is done only on that file and that band
            visible_layers.push(Ext.ComponentQuery.query('#EditingRaster')[0].editingMode.layer.getSource().getParams()['full_path']);
            var additional_params = {
                band: recode_window.down('#band').getValue()
            };
        }else{

            me.map.getLayers().forEach(function(layer) {
                 if (layer.get('isBaseLayer') == false
                        && layer.isVisible() == true
                        && layer.get('type') != 'wms'
                        && layer.get('type') != 'feature'
                        && layer.get('type') != 'hidden') {
                    visible_layers.push(layer.getSource().getParams()['full_path']);
                }
            });
        }
        var mapExtent = me.map.getView().calculateExtent(me.map.getSize());
        var mapWidth = Math.abs(mapExtent[2] - mapExtent[0])

        if(typeof Ext.ComponentQuery.query('#GEEWindow')[0] !== 'undefined'){
//            var lat = LLcoord[0];
//            var lon = LLcoord[1];
            var lat = evt.coordinate[1];
            var lon = evt.coordinate[0];
            Ext.ComponentQuery.query('#GEEWindow')[0].fireEvent('refreshFromShapefileClick', lon, lat, null);
        } else {

            IMPACT.Utilities.Requests.query_by_point(visible_layers, evt.coordinate[0], evt.coordinate[1], evt, additional_params, mapWidth);

        }
    },

    handleNavHistory: function(extent, direction){
        var me = this;

        if(me.navHistoryPos > 1){
            Ext.ComponentQuery.query('#navPrevButt')[0].setDisabled(false);
        } else {
            Ext.ComponentQuery.query('#navPrevButt')[0].setDisabled(true);
        }


        if(me.navHistoryPos+1 == me.navHistoryList.length){
            Ext.ComponentQuery.query('#navNextButt')[0].setDisabled(true);
        } else{
            Ext.ComponentQuery.query('#navNextButt')[0].setDisabled(false);
        }

        if(extent){
                    if(me.navHistoryPos < me.navHistoryMax){
                        me.navHistoryPos +=1;
                    } else {
                            Ext.ComponentQuery.query('#navNextButt')[0].setDisabled(true);
                    }
                    me.navHistoryList[me.navHistoryPos] = extent;
        } else {
                // ---------------  handle Prev   Next ---------------------
                me.deactivateMoveEndFn();
                if (direction == 'Prev'){
                        if(me.navHistoryPos > 1 ){
                            me.navHistoryPos -=1;
                        }
                } else {
                        if(me.navHistoryPos < me.navHistoryList.length-1 ){
                            me.navHistoryPos +=1;
                        }
                }
                me.map.getView().fit(me.navHistoryList[me.navHistoryPos]);
                setTimeout(function() { me.activateMoveEndFn(); }, 500);
        }
    }



});