/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.EPSGCodesCombo', {
    extend: 'IMPACT.view.component.Field.Combo',
    alias: 'widget.EPSGCodesCombo',

    itemId: 'EPSGCodesCombo',

    fieldLabel: 'EPSG',
    labelWidth: 40,
    width: 300,

    editable: true,
    forceSelection: true,
    //typeAhead: true,
    queryMode: 'local',
    minChars: 1,
    initFilter : null,
    //remoteFilter: true,
    //enableRegEx: true,
    caseSensitive: false,
    anyMatch: true,


    store: {
            xtype: 'store',
            fields: ['value', 'name'],
            data : IMPACT.Utilities.GDAL.EPSG_codes,
            sorters: [{
                property: 'name',
                direction: 'ASC'
            }]
            },

    displayField: 'name',
    valueField: 'value',


//    listeners: {
//        change: function(combo, newValue) {
//            var store = this.store;
//            if (newValue === "" || newValue === null){
//                store.clearFilter();
//                if (this.initFilter != null){
//                    this.store.filter({
//                        property: 'name',
//                        anyMatch: true,
//                        value: this.initFilter
//                    });
//                }
//                return;
//            };
//            store.clearFilter();
//            if (this.initFilter != null){
//                    this.store.filter({
//                        property: 'name',
//                        anyMatch: true,
//                        value: this.initFilter
//                    })
//            };
//            store.filter({
//                property: 'name',
//                anyMatch: true,
//                value: newValue
//            });
//        }
//    },
    initComponent : function(){
        this.callParent(arguments);
        if (this.initFilter != null){
            this.store.filter({
                property: 'name',
                anyMatch: true,
                value: this.initFilter
            });
        }

    }


});