/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.Vector.ComboBox', {
    extend: 'IMPACT.view.component.Field.Combo',

    alias: 'widget.LegendCombo',

    displayField: 'label',
    valueField: 'class',

    config: {
        legendType: null,
        id: null,
        fieldLabel: null
    },

    emptyText: ' - ',

    triggerAction: 'all',
    selectOnFocus: false,

    constructor: function(cfg){
        var me = this;
        me.superclass.constructor.call(me, cfg);
        //me.callParent();
        me.populate();
    },

    populate: function(){
        var me = this;
        if(me.legendType!==''){
//            me.bindStore(Ext.create('IMPACT.store.ColorPalette.Grid.Vector', {
//                legendType: me.legendType,
//                storeId: 'LegendComboBox_'+me.legendType
//            }));
            me.store = Ext.create('IMPACT.store.ColorPalette.Grid.Vector', { legendType: me.legendType, storeId: 'LegendComboBox_'+me.legendType });
        }
    }


});