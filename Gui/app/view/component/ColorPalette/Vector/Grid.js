/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.Vector.Grid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.ColorPaletteVectorGrid',

    itemId: 'ColorPaletteVectorGrid',

    config: {
        legendType: null,
        showCheckbox: false
    },

    enableColumnHide: false,
    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: true,
    resizable: false,

    columns: [
        {
            xtype: 'checkcolumn',
            text: 'All Off',
            width: 70,
            dataIndex: 'checkbox',
            value: true,
            sortable: false,
            hidden: true,
            listeners: {
              headerclick:function(header, column, e, t,eOpts){
                     // header:Header Container of grid
                     // column: The Column header Component
                    if (column.text == 'All Off'){
                        column.setText('All On');
                        var setChecked = false;

                    } else{
                        column.setText('All Off');
                         var setChecked = true;
                    }

                    var grid = this.up('#ColorPaletteVectorGrid');
                    grid.getStore().each(function(record){
                        record.set("checkbox", setChecked);
                    })

                    grid.fireEvent('ColorPaletteVectorGrid_CheckChange', grid.getChecked());

                },
                checkchange: function(checkcolumn){
                    var grid = this.up('#ColorPaletteVectorGrid');
                    grid.fireEvent('ColorPaletteVectorGrid_CheckChange', grid.getChecked());
                }
            }
        },
        {
            text: 'ID',
            width : 50,
            dataIndex: 'class'
        },
        {
            text: 'Name',
            flex: 1,
            minWidth: 130,
            dataIndex: 'label'
        },
        {
            text: 'Color',
            width : 60,
            sortable: false,
            renderer: function(value, metaData, record) {
                var tmp = record.get('color').split(',');
                metaData.style = 'background-color:#' + IMPACT.Utilities.Common.rgbToHex(tmp[0],tmp[1],tmp[2])+';';
                return value;
            }
        }
    ],

    constructor: function(cfg){
        var me = this;
        me.superclass.constructor.call(me, cfg);
        var store = Ext.create('IMPACT.store.ColorPalette.Grid.Vector', { legendType: me.legendType });
        if(me.showCheckbox===true){
            store.each(function(record){
                record.set('checkbox', true);
            });
        }

        me.reconfigure(store);

        // ####  Add custom event  ####
        //me.addEvents('ColorPaletteVectorGrid_CheckChange');
    },

    afterLayout: function(){
        var me = this;
        if(me.showCheckbox===true){
            me.columns[0].setVisible(true);
        }
        me.callParent();
    },

    getChecked: function() {
        var me = this;
        var ids = [];
        me.getStore().each(function(record){
            if(record.get('checkbox') === true){
                ids.push((record.data.class).toString());
            }
        });
        return ids;
    }



});