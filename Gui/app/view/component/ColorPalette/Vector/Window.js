/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.Vector.Window', {
    extend: 'Ext.window.Window',

    requires: ['IMPACT.view.component.ColorPalette.Vector.Grid'],

    itemId: 'ColorPaletteVectorWindow',

    title: 'Vector Legends',
    collapsible: false,
    maximizable: false,
    constrain: true,
    layout: 'fit',
    width: 500,
    y: 200,
    closeAction: 'hide',

    buttons: [{
        text: 'Edit',
        handler: function(){
            this.up('window').close();
            Ext.ComponentQuery.query('#viewportTab')[0].setActiveTab(2);
            Ext.ComponentQuery.query('#UserSettingsTabLegendVector')[0].show()
        }},{
        text: 'Close',
        handler: function(){
            this.up('.window').close();
        }
    }],
    items: [
        {
            xtype: 'tabpanel',
            maxHeight: 400,
            itemId: 'ColorPaletteVectorWindowTabpanel',
            items: []
        }
    ],

    initComponent: function(){
        var me = this;
        me.callParent();

        var legends = Ext.data.StoreManager.lookup('VectorLegends').data.items;
        var tabPanel = me.down('#ColorPaletteVectorWindowTabpanel');
        legends.forEach(function(item) {

            tabPanel.add(
                {
                    xtype: 'ColorPaletteVectorGrid',
                    itemId: 'ColorPaletteVectorGrid'+item.data.type,
                    title: item.data.name,
                    legendType: item.data.type
                }
            )
        });
        tabPanel.setActiveTab(0);
    },

    showHide: function(){
        var me = this;
        if(me.isHidden()===true){
            me.show();
        } else {
            me.hide();
        }
    }

});