/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.component.GridColumn.Color', {
    extend: 'Ext.grid.column.Column',

    alias: 'widget.columncolor',

    dataIndex: null, // to be defined
    text: 'Color',

    renderer: function(value, metaData, record) {
        var me = this;

        var row_index = me.getStore().indexOf(record);
        var data_index = metaData.column.dataIndex;

        var rgbColor = record.get(data_index).split(',');
        var htmlColor = IMPACT.Utilities.Common.rgbToHex(rgbColor[0], rgbColor[1], rgbColor[2]);

        return '<input name="color" type="color" value="#'+htmlColor+'" onchange="Ext.getCmp(\'' + me.id + '\').updateColor(this.value, '+row_index+', \''+data_index+'\')"  />';
    }

});