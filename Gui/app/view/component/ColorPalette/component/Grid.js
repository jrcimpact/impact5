/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.component.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'IMPACT.view.component.ColorPalette.component.GridColumn.Color',
        'IMPACT.view.component.ColorPalette.component.GridColumn.RemoveRow',
        'IMPACT.view.component.ColorPalette.component.GridColumn.Opacity'
    ],

    config: {
        legendType: null
    },

    plugins:[
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        }
    ],

    enableColumnHide: false,
    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: true,
    resizable: false,

    border: true,
    style: {
        border: '1px solid #d0d0d0'
    },


    margin: '0 20px 10px 0',

    bbar: [{
        xtype: 'button',
        text: 'Add new class',
        iconCls: 'fas fa-lg fa-green fa-plus-circle',
        cls: 'impact-icon-button',
        handler: function(){
            var grid = this.up('gridpanel');
            grid.store.insert(0, Ext.create(grid.rowModel));
        }
    }],

    initComponent: function(config){
        var me = this;
        me.store = Ext.create(me.gridStore, {
            legendType: me.legendType,
            storeId: me.xtype+'_'+me.legendType
        });
        me.callParent(arguments);
    },

    updateColor: function(color, row_index, data_index){
        var me = this;
        var rgbColor = IMPACT.Utilities.Common.hexToRgb(color).join(',');
        me.getStore().getAt(row_index).set(data_index, rgbColor);
    }

});