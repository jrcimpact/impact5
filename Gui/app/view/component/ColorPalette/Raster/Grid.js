/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ColorPalette.Raster.Grid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.ColorPaletteRasterGrid',

    itemId: 'ColorPaletteRasterGrid',

    config: {
        legendType: null
    },

    enableColumnHide: false,
    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: true,
    resizable: false,

    columns: [
        {
            dataIndex: 'label',
            text: 'Label',
            flex: 1
        },
        {
            dataIndex: 'range_min',
            text: 'Range Min [>=]',
            width : 100
        },
        {
            dataIndex: 'range_max',
            text: 'Range Max [<]',
            width : 100
        },
        {
            text: 'Color Min',
            width : 70,
            sortable: false,
            renderer: function(value, metaData, record) {
                var tmp = record.get('color_min').split(',');
                metaData.style = 'background-color:#' + IMPACT.Utilities.Common.rgbToHex(tmp[0],tmp[1],tmp[2])+';';
                return value;
            }
        },
        {
            text: 'Color Max',
            width : 70,
            sortable: false,
            renderer: function(value, metaData, record) {
                var tmp = record.get('color_max').split(',');
                metaData.style = 'background-color:#' + IMPACT.Utilities.Common.rgbToHex(tmp[0],tmp[1],tmp[2])+';';
                return value;
            }
        }
    ],

    constructor: function(cfg){
        var me = this;
        me.superclass.constructor.call(me, cfg);

        me.reconfigure(Ext.create('IMPACT.store.ColorPalette.Grid.Raster', { legendType: me.legendType }));
    }

});