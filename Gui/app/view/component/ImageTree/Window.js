/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.component.ImageTree.Window', {
    extend: 'IMPACT.view.component.Window',

    requires: [
                'IMPACT.view.component.ImageTree.Tree',
                'IMPACT.view.component.ImageTree.TreeBrowser'
              ],

    itemId: 'ImageTreeWindow',

    width: 550,
    height: 350, /* Do not use minHeight & maxHeight: cause scroll-up on item check !!! */
    resizable: true,
    bodyStyle: 'padding: 0; background-color: white;',
    closeAction: 'destroy',
    closable: false,
   // overflowY: 'scroll',
   // overflowX: 'scroll',
    layout: 'fit',
    modal: true,
    title: 'Data folder',
    tree: null,
    alwaysOnTop: true,

    buttons: [
//         {
//             text: 'Clear',
//             handler: function(){
//                var window = this.up('#ImageTreeWindow');
//                window.destroy();
//             }
//         },
         {
             text: 'Apply',
             handler: function(){
                var window = this.up('#ImageTreeWindow');
                window.getSelected();
                window.destroy();
             }
         }
    ],

    initComponent: function(){
        var me = this;
        me.callParent();
        me.add(me.tree);
    },

    /**
     * Retrieve info of the selected files and raise 'file_selected' event
     */
    getSelected: function(){

        // template can be created using default ImageTree or ImageTreeBrowser
        var tree = this.down('#ImageTree') ? this.down('#ImageTree') : this.down('#ImageTreeBrowser');
        var selection = tree.getSelected();

        this.fireEvent('file_selected', selection);
    }

});