/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.ImageTree.TreeBrowser', {
    extend: 'Ext.tree.Panel',

    alias: 'widget.ImageTreeBrowser',

    itemId: 'ImageTreeBrowser',

    header: false,
    rootVisible: false,
    border: false,
    collapsible: true,
    //disableSelection: true,   // prevent dragDrop as well
    allowDrag : false,
    returnDir: false,

    fileStore: null,
    store: null,
    selectionStore: null,
    selectionMode: 'multiple',

    viewConfig: {
        listeners: {
            checkchange: function(node, checked){
                var me = this;
                if(node.hasChildNodes()){
                    node.cascadeBy(function(childNode){
                        childNode.set('checked', checked);
                    });
                }
            }
        }
    },

    initComponent: function(config){
        var me = this;
        me.disableSelection = me.allowDrag ? false : true;
        me.callParent(arguments);

    },

    /**
     * Return selected files
     * @returns {Array}
     */
    getSelected: function() {
        var me = this;
        var selection = [];
        var rootNode = this.getRootNode();
        //  return the directory for e.g. PhotosToKML tools
        if(me.returnDir){
              rootNode.cascadeBy(function (childNode) {
                if(childNode.data.data_from_DB && childNode.data.checked===true){
                     selection.push(childNode.data.data_from_DB.full_path);
                }
            });

        } else  {
            rootNode.cascadeBy(function (childNode) {
                if(childNode.isLeaf() && childNode.data.checked===true){
                    selection.push(childNode.data.data_from_DB);
                }
            });
        }
        return selection;
    },


    __getBreadcrumbs: function(file_path){
        return file_path.replace("\\", '/').replace(/\/$/, '').split('/');
    },

    /**
     * Build the tree
     * @private
     */
    __buildTree: function() {
        var me = this;

        var rootNode = me.getRootNode();

        var parentNode = me.getRootNode(); //me.__createFolder(rootNode, me.rootLabel);

        me.fileStore.data.each(function(item) {
                var breadcrumbs = me.__getBreadcrumbs(item.data['full_path']);
                me.__createNode(parentNode, breadcrumbs, item);
        });

    },

    /**
     * Create a tree node (folder/leaf recursively)
     * @private
     */
    __createNode: function(parentNode, breadcrumbs, item){
        var me = this;

        if(breadcrumbs.length>0){

                var folder_label = breadcrumbs.shift();

                // build a directory node if not last path or if labeled as folder even if last path
                if(item.data['directory'] === 'true' || breadcrumbs.length > 0){
                    var folderNode = me.__createFolder(parentNode, folder_label, item);
                    me.__createNode(folderNode, breadcrumbs, item);
                } else {
                    me.__createLeaf(parentNode, item)
            }
        }

    },

    /**
     * Create a tree folder node
     * @param parentNode
     * @param label
     * @returns {Node}
     * @private
     */
    __createFolder: function(parentNode, label, item){
        var me = this;
        var breadcrumbs = (parentNode.data.hasOwnProperty('breadcrumbs') ? parentNode.data['breadcrumbs'].replace(/\/$/,'')+'/'+label : label);
        var folderNode = me.__getNodeBy(parentNode, 'breadcrumbs', breadcrumbs);
        // Create folder if not added yet
        if (!folderNode){

            folderNode = parentNode.appendChild({
                expanded: false,
                leaf: false,
                text: label,
                allowChildren: true,
                expandable: true,
                allowDrag: false,
                qtip: label,
                children:[{
                           text: '',
                           leaf: true,
                           cls: 'x-tree-hidden'
                          }
                         ]
            });

            if(me.selectionMode==='multiple' && parentNode!==me.getRootNode()){
                folderNode.set('checked', false);
            }
            folderNode.data['breadcrumbs'] = breadcrumbs;
            folderNode.set('data_from_DB', item.data);
            folderNode.opened=false;
        }
        return folderNode
    },



    __createLeaf: function(parentNode, item){
        var me = this;
        var leafNode = me.__getNodeBy_data_from_DB(parentNode, 'full_path', item.data['full_path']);

        // Create leaf if not added yet
        if (!leafNode){

            var text = item.data['basename'];
            var myIconClass = 'x-tree-icon-leaf_zip';
            if (item.data['basename'].endsWith(".tif")){
                myIconClass = 'x-tree-icon-leaf_raster';
            };
            if (item.data['basename'].endsWith(".vrt")){
                myIconClass = 'x-tree-icon-leaf_virtual';
            };


            leafNode = parentNode.appendChild({
                //leaf: item.data['directory'] === 'true' ? false : true,
                allowChildren: false,
                leaf: true,
                qtip: text,
                text: text,
                checked: me.__setLayerChecked(item),
                //iconCls: item.data['basename'].endsWith(".tif") ? 'x-tree-icon-leaf_raster' : 'x-tree-icon-leaf_zip',
                iconCls: myIconClass,
                cls: '',
                allowDrag: false
            });

            leafNode.opened = false;
            leafNode.set('data_from_DB', item.data);
            return leafNode
        }
    },

    __getNodeBy: function(node, attribute, value){
        if(node && node.childNodes.length>0){
            for(var i=0; i<node.childNodes.length; i++){
                if( node.childNodes[i].data && node.childNodes[i].data[attribute] === value){
                    return node.childNodes[i];
                }
            }
        }
        return false;
    },

    __getNodeBy_data_from_DB: function(node, attribute, value){
        if(node && node.childNodes.length>0){
            for(var i=0; i<node.childNodes.length; i++){
                if(node.childNodes[i].data && node.childNodes[i].data.data_from_DB && node.childNodes[i].data.data_from_DB[attribute] === value){
                    return node.childNodes[i];
                }
            }
        }
        return false;
    },

    __setLayerChecked: function(item) {
         var checked = false;
         if(this.selectionStore !== null){
             checked = this.selectionStore.query('full_path', item.data['full_path']).getCount()>0;
         }
         return checked;
    },

    /**
     * Select children on folder selection
     * @param node
     * @param checked
     * @private
     */
  
});