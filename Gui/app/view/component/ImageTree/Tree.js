/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.ImageTree.Tree', {
    extend: 'Ext.tree.Panel',

    alias: 'widget.ImageTree',

    itemId: 'ImageTree',

    header: false,
    rootVisible: false,
    border: false,
    collapsible: true,

    disableSelection: true,   // prevent dragDrop as well
    allowDrag : false,

    fileStore: null,
    store: null,
    selectionStore: null,
    selectionMode: 'single',
    filters: ['vector', 'raster', 'class', 'virtual','kml', 'geojson'],

    viewConfig: {
        plugins: [{
           ptype: 'treeviewdragdrop', //disableSelection: true,   // prevent dragDrop as well
        }],
        listeners: {
            checkchange: function(node, checked){
                var me = this.up('#ImageTree');
                if(me.selectionMode==='multiple'){
                    me.__selectAllChildren(node, checked);
                } else if(me.selectionMode==='single'){
                    me.__deSelectAllBeforeSelect(node, checked);
                }
            }
        }
    },

    initComponent: function(config){
        var me = this;

        me.disableSelection = me.allowDrag ? false : true;

//      this.fileStore.on('updateStore_finished', function(){
//            //alert('update');
//            //Ext.ComponentQuery.query('#LayerTreePanel')[0].__buildTree();
//            me.__buildTree(IMPACT.GLOBALS['data_paths']['data']);
//
//        });
//      this.fileStore.on('updateCSStore_finished', function(){
//            //alert('Update cs');
//            //Ext.ComponentQuery.query('#CSLayerTreePanel')[0].__buildTree(IMPACT.GLOBALS['data_paths']['unix_raw_data']);
//           me.__buildTree(IMPACT.GLOBALS['data_paths']['unix_raw_data']);
//        });

        me.callParent(arguments);
    },




    /**
     * Return selected files
     * @returns {Array}
     */
    getSelected: function() {
        var selection = [];
        var rootNode = this.getRootNode();
        rootNode.cascadeBy(function (childNode) {
            if(childNode.isLeaf() && childNode.data.checked===true){
                selection.push(childNode.data.data_from_DB);
            }
        });
        return selection;
    },

    /**
     * Build the tree
     * @private
     */
    __buildTree: function(rootNodeValue) {
        var me = this;
        if(rootNodeValue == null) {
            rootNodeValue = '';
        }


        var rootNode = me.getRootNode();
        var parentNode = me.__createFolder(rootNode, rootNodeValue );
        me.fileStore.data.each(function(item) {
            if(me.filterItem(item)){
                var breadcrumbs = IMPACT.Utilities.Impact.getBreadcrumbs(item.data['full_path'], rootNodeValue);
                me.__createNode(parentNode, breadcrumbs, item);
            }
        });

        // remove orphans
        // after building to avoid removing node on fast file re-creation
        me.__removeOrphans();
    },
    /**
     *
     * @param item
     * @returns {boolean}
     */
    filterItem: function(item) {
        return this.filters.includes(IMPACT.Utilities.Impact.getLayerType(item.data));
    },

    /**
     * Create a tree node (folder/leaf recursively)
     * @private
     */
    __createNode: function(parentNode, breadcrumbs, item){
        var me = this;

        if(breadcrumbs.length>0){
            var folder_label = breadcrumbs.shift();
            var folderNode = me.__createFolder(parentNode, folder_label);
            me.__createNode(folderNode, breadcrumbs, item);
        }
        // Leaf
        else {
            me.__createLeaf(parentNode, item);
        }
    },

    /**
     * Create a tree folder node
     * @param parentNode
     * @param label
     * @returns {Node}
     * @private
     */
    __createFolder: function(parentNode, label){
        var me = this;
        var breadcrumbs = (parentNode.data.hasOwnProperty('breadcrumbs') ? parentNode.data['breadcrumbs'].replace(/\/$/,'')+'/'+label : label);
        var folderNode = me.__getNodeBy(parentNode, 'breadcrumbs', breadcrumbs);
        // Create folder if not added yet
        if (!folderNode){
            var isFirstLevel = parentNode.isRoot();
            folderNode = parentNode.appendChild({
                expanded: isFirstLevel,
                leaf: false,
                text: label,
                allowChildren: true,
                allowDrag: !isFirstLevel && me.allowDrag,
                qtip: label
            });
            if(me.selectionMode==='multiple' && parentNode!==me.getRootNode()){
                folderNode.set('checked', false);
            }
            folderNode.data['breadcrumbs'] = breadcrumbs;
        }
        return folderNode;
    },

    /**
     * Create a tree leaf node
     * @param parentNode
     * @param item
     * @returns {Node}
     * @private
     */
    __createLeaf: function(parentNode, item){
        var me = this;
        var leafNode = me.__getNodeBy_data_from_DB(parentNode, 'full_path', item.data['full_path']);
        // Create leaf if not added yet
        if (!leafNode){
            var text = item.data['basename'];
            if (item.data.hasOwnProperty('ready') && item.data['ready'] !== 'true'){
                text = '<span class="fa fa-lg fa-black fa-spinner fa-pulse"></span> '+text;
            }
            leafNode = parentNode.appendChild({
                leaf: true,
                qtip: item.data['basename'],
                text: text,
                checked: me.__setLayerChecked(item),
                iconCls: me.__setLayerIcon(item),
                cls: me.__setLayerStyle(item),
                allowDrag: me.allowDrag
            });

            leafNode.set('data_from_DB', item.data);
            leafNode.dirty = false;
            return leafNode

        } else {
                me.__updateLeaf(leafNode, item);
               }
        return null
    },


    /**
     * Update node with new data fron DB
     */
    __updateLeaf: function(leafNode, item){
        var me = this;
        // update attributes if there (vector editing)
        leafNode.dirty = false;
        if( leafNode.data.data_from_DB['modification_date'] != item.data['modification_date'] ||
                                         leafNode.data.data_from_DB['ready'] != item.data['ready']){
                leafNode.set('data_from_DB', item.data);
                // keep red style while in editing mode
                if(leafNode.active_editing !== true){
                    leafNode.set('cls', me.__setLayerStyle(item));
                }
                if (item.data['ready'] !== 'true'){
                    leafNode.set('text', '<span class="fa fa-lg fa-black fa-spinner fa-pulse"></span> '+ item.data['basename']);
                } else {
                    leafNode.set('text', item.data['basename']);
                    // set true to redraw layer
                    leafNode.dirty = true;
                }
        }
    },

    /**
     * Return "checked" attribute (if file had been already selected)
     * @param item
     * @returns {boolean}
     * @private
     */
    __setLayerChecked: function(item) {
         var checked = false;
         if(this.selectionStore !== null){
             checked = this.selectionStore.query('full_path', item.data['full_path']).getCount()>0;
         }
         return checked;
    },

    /**
     * Return CSS classes according to file
     * @param item
     * @returns {string}
     * @private
     */
    __setLayerStyle: function(item){
        var cls = '';
        if (item.data.hasOwnProperty('ready') && item.data['ready'] !== 'true'){
            cls = 'x-tree-disabled';
            if (item.data['PROJ4'] === 'Unknown'){
                cls = 'x-tree-error';
            }

        }
        return cls;
    },

    /**
     * Return icon CSS class according to file type
     * @param item
     * @returns {string}
     * @private
     */
    __setLayerIcon: function(item){
        var iconCls = '';
        var file_type = IMPACT.Utilities.Impact.getLayerType(item.data);
        if(file_type==='class'){
            iconCls = 'x-tree-icon-leaf_class';
        } else if(file_type==='raster'){
            iconCls = 'x-tree-icon-leaf_raster';
        }else if(file_type==='virtual'){
            iconCls = 'x-tree-icon-leaf_virtual';
        }else if(file_type==='vector'){
            iconCls = 'x-tree-icon-leaf_vector';
        }else if(file_type==='kml'){
            iconCls = 'x-tree-icon-leaf_kml';
        }else if(file_type==='geojson'){
            iconCls = 'x-tree-icon-leaf_geojson';
        }
        return iconCls;
    },

    /**
     * Search for a node by the given attribute in childNodes
     * @param node
     * @param attribute
     * @param value
     * @returns {Array|boolean}
     * @private
     */
    __getNodeBy_data_from_DB: function(node, attribute, value){
        if(node && node.childNodes.length>0){
            for(var i=0; i<node.childNodes.length; i++){
                if(node.childNodes[i].data && node.childNodes[i].data.data_from_DB && node.childNodes[i].data.data_from_DB[attribute] === value){
                    return node.childNodes[i];
                }
            }
        }
        return false;
    },

     /**
     * Search for a node by the given attribute in childNodes
     * @param node
     * @param attribute
     * @param value
     * @returns {Array|boolean}
     * @private
     */
    __getNodeBy: function(node, attribute, value){
        if(node && node.childNodes.length>0){
            for(var i=0; i<node.childNodes.length; i++){
                if( node.childNodes[i].data && node.childNodes[i].data[attribute] === value){
                    return node.childNodes[i];
                }
            }
        }
        return false;
    },

    /**
     * Select children on folder selection
     * @param node
     * @param checked
     * @private
     */
    __selectAllChildren: function(node, checked){
        if(node.hasChildNodes()){
            node.cascadeBy(function(childNode){
                childNode.set('checked', checked);
            });
        }
    },

    /**
     * Deselect ALL nodes before selection
     * @param node
     * @param checked
     * @private
     */
    __deSelectAllBeforeSelect: function(node, checked){
        this.__deSelectAll();
        if(node.isLeaf()){
            node.set('checked', checked);
        }
    },

    /**
     * Deselect ALL tree nodes
     * @private
     */
    __deSelectAll: function(){
        var rootNode = this.getRootNode();
        rootNode.cascadeBy(function (childNode) {
            //console.log(childNode);
            //if(childNode.raw.hasOwnProperty('checked')){
                childNode.set('checked', false);
           // }
        });
    },

    __removeOrphans: function(){
        var me = this;
        var nodeToRemove = [];
        var rootNode = me.getRootNode();
        rootNode.cascadeBy(function (childNode) {
            if( childNode && childNode.data.data_from_DB && me.fileStore.findRecord('full_path',childNode.data.data_from_DB['full_path']) === null) {
                nodeToRemove.push(childNode);
            }
        });

        for (var x in nodeToRemove){

            if (nodeToRemove[x].data.layer){
                me.map.removeLayer(nodeToRemove[x].data.layer);
            }
            nodeToRemove[x].remove();
        }

    },

});