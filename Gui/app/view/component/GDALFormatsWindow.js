/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.GDALFormatsWindow', {
    extend: 'IMPACT.view.component.Window',

    itemId: 'GDALFormatsWindow',

    title: 'Supported GDAL formats',

    closeAction: 'destroy',
    width: 450,
    height: 500,
    autoDestroy: true,
    overflowY: 'scroll',
    autoShow: true,
    bodyStyle: 'background-color: #ddd; padding-top: 10px;',

    html: '',

    initComponent: function(){
       this.html = this.get_formats();
       this.callParent();
    },

    get_formats: function(){
        var html = '';
        var formats = IMPACT.Utilities.GDAL.input_formats.sort();
        for(var i in formats){
            var format = formats[i];
            var regexp = /^([\w\d]*)\s+(\-.+\-)\s+(\(.+\))\:\s(.*)$/;
            var match = regexp.exec(format);
            html += '<li><b>'+match[1]+'</b>: '+match[4]+' <i>'+match[2]+' '+match[3]+' </i></li>';
        }
        return '<ul style="padding-left: 30px;">'+html+'</ul>';
    }

});