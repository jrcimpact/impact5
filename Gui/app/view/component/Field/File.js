/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.Field.File', {
    extend: 'Ext.container.Container',

    alias: 'widget.ProcessingWindowFileInput',

    width: 425,

    labelWidth: 90,
    fieldLabel: null,
    //style: 'margin-bottom: 5px;',

    selectionMode: 'single',
    filters: ['vector', 'raster', 'class'],
    returnDir: false,

    allowBlank: false,
    validator: null,
    infoEnabled: true,

    store: null,

    initComponent: function(config){
        var me = this;
        me.callParent(arguments);

        // ####  Add custom event  ####
        //this.addEvents('FileInput_change');

        // ####  Label (only for "multiple") ####
        if(me.selectionMode==='multiple' && me.fieldLabel!==null){
             me.add({
                 xtype: 'component',
                 html: me.fieldLabel+': ',
                 style: "font-weight: bold;"
             });
        }

        // ####  Input list container ####
        me.add({
            xtype: 'container',
            itemId: 'FileInput_container',
            items: []
        });

        // ####  Add button ####
        if(me.selectionMode==='multiple'){
            me.add({
                xtype: 'button',
                text: 'Select inputs',
                iconCls: 'fas fa-lg fa-green fa-plus-circle',
                //cls: 'impact-icon-button',
                handler: function() {
                    me.__openSelectionWindow();
                }
            });
        }

        // Initialize the data store
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                'label',
                'full_path',
                'data_from_DB'
            ]
        });
        me.__refreshUI();
    },

    /**
     * Refresh Store and UI
     * @param data
     */
    refresh: function(data){
        var me = this;
        me.__refreshStore(data);
        me.__refreshUI();
        me.fireEvent('FileInput_change', me);
    },

    /**
     * Reset Store and UI
     */
    reset: function() {
        this.refresh([]);
    },

    /**
     * Refresh Store
     * @private
     */
    __refreshStore: function(data) {
        var me = this;
        //console.log(me.store);
        me.store.removeAll();
        if(data.length>0){
            // Populate store
            data.forEach(function(item){
                //  return the directory for e.g. PhotosToKML tools
                // prepare teh format for the function getValues
                if(me.returnDir && item){
                    me.store.add({
                        'label': item,
                        'full_path': item,
                        'data_from_DB': {'full_path':item}
                    });
                }
                else if (item && item.full_path){
                    var path = item.full_path.replace(IMPACT.GLOBALS['data_paths']['data'],'');
                    me.store.add({
                        'label': path,
                        'full_path': item.full_path,
                        'data_from_DB': item
                    });
                };
            });
        }
    },

    /**
     * Refresh UI
     * @private
     */
    __refreshUI: function() {
        var me = this;
        var container = me.down('#FileInput_container');

        container.removeAll();
        //console.log(me.store.count());
        if(me.store.count()>0) {
            me.store.each(function (item, index) {
                container.add(me.__inputRecord(index));
            });
        } else {
            container.add(me.__inputRecord());
        }
    },

    __removeRecord: function(index) {
        if(index!==null) {
            this.store.removeAt(index);
            this.__refreshUI();
        }
    },

    /**
     * Return record container
     * @private
     */
    __inputRecord: function(index) {
        var me = this;
        index = typeof index !== "undefined" ? index : null;

        var items = [];

        // ### text field ###
        items.push(me.__inputField(index));

        // ### info button ###
        if(me.infoEnabled){
            items.push({
                xtype: 'button',
                itemId: 'FileInput_info',
                iconCls: 'fa fa-lg fa-blue fa-info-circle',
                cls: 'impact-icon-button',//'margin-left: 2px;',
                hidden: index===null,
                handler: function() {
                    me.__showInfo(index);
                }
            });
        }
        // ### remove button ###
        if(me.selectionMode==='multiple') {
            items.push({
                xtype: 'button',
                //style: 'margin-left: 2px;',
                iconCls: 'fa fa-lg fa-red far fa-times-circle',
                cls: 'impact-icon-button',
                hidden: index === null,
                handler: function () {
                    me.__removeRecord(index);
                }
            });
        }

        return {
            xtype: 'container',
            itemId: index!==null
                ? 'FileInput_record_'+index
                : 'FileInput_record_0',
            //style: 'margin-bottom: 5px;',
            layout: 'hbox',
            items: items
        }
    },


    /**
     * Return input file text field
     * @private
     */
    __inputField: function(index) {
        var me = this;

        var inputField = {
            xtype: 'textfield',
            itemId: 'FileInput_field',
            hideLabel: false,
            width: me.selectionMode==='multiple' ? me.width - 70 : me.width - 30,
            readOnly: true,
            value: index!==null ? me.store.getAt(index).data['label'] : '',
            listeners: {
                focus: function(){
                    me.__openSelectionWindow();
                    me.blur();
                }
            },
            allowBlank: me.allowBlank,
            inputAttrTpl: index!==null ? " data-qtip='"+ me.store.getAt(index).data['data_from_DB']['full_path']+"' " : ''
        };

        // ### Label (if given) ###
        if(me.fieldLabel!==null){
            inputField.fieldLabel = me.fieldLabel;
            inputField.labelWidth = me.labelWidth;
            inputField.labelStyle = 'font-weight: bold;';
            if(me.selectionMode==='multiple'){
                inputField.hideLabel = true;
            }
        }

        // Field custom validator (if given)
        if(me.validator!==null){
            inputField.validator = me.validator;
        }

        return inputField;
    },

    /**
     * Show file info
     * @param index
     * @private
     */
    __showInfo: function(index){
        var me = this;
        if(index!==null){
            Ext.create('IMPACT.view.Workspace.Main.component.LayerInfo', {
                data_from_DB: me.store.getAt(index).data['data_from_DB']
            }).show();
        }
    },

    /**
     *  Open the selection window (with file tree-panel)
     */
    __openSelectionWindow: function(){
        var me = this;

        me.treeWin = Ext.create('IMPACT.view.component.ImageTree.Window', {
                    tree: {
                        xtype: 'ImageTree',
                        selectionMode: me.selectionMode,
                        filters: me.filters,
                        fileStore: Ext.data.StoreManager.lookup('ImageStore'),
                        selectionStore: me.store
                    },
                    listeners: {
                        'file_selected': function(selected_files){
                            me.refresh(selected_files);
                        }
                    }
                });

        //if (me.treeWin){
        me.treeWin.down('#ImageTree').__buildTree(IMPACT.GLOBALS['data_paths']['data']);  // 'updateStore_finished' is fired only on layertree creation
        me.treeWin.show();
        me.treeWin.toFront();
        //}

    },

    /**
     * Retrieve the selected files as array of full_path
     * @returns {Array}
     */
    getValues: function(){
        var me =this;
        var values = [];
        me.store.each(function (item) {
                 values.push(item.data['data_from_DB']['full_path']);
        });
        return values;
    },

    /**
     * Retrieve selected file info (actually the first)
     * @returns {null|mixed}
     */
    getFileData: function(){
        if(this.store.count()>0) {
            return this.store.first().data['data_from_DB'];
        }
        return null;
    }

});