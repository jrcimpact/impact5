/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.Field.Slider', {
    extend: 'Ext.container.Container',

    requires: ['IMPACT.view.component.Field.Float'],

    alias: 'widget.impactFieldSlider',

    width: 200,
    layout: 'hbox',
    style: 'padding: 1px 1px 1px 1px;',

    value: 50,
    minValue: 0,
    maxValue: 100,
    increment: 0,
    decimalPrecision: 0,

    fieldLabel: null,
    labelWidth: 0,
    labelStyle: '',

    items: [],
    propagateToTextField: true,

    respect_boundaries: true,
    disable_textField: false,

    initComponent: function(){

        var me = this;
        me.items = [];

        var text_field_width = 50;
        var slider_width = me.width - text_field_width - 5;

        // ####  Slider  ####
        var slider = {
            xtype: 'slider',
            itemId: 'slider',
            width: slider_width,
            value: me.value,
            minValue: me.minValue,
            maxValue: me.maxValue,
            increment: me.increment,
            decimalPrecision: me.decimalPrecision
        };
        if(me.fieldLabel!==null){
            slider.fieldLabel = me.fieldLabel;
            slider.labelWidth = me.labelWidth;
            slider.labelStyle = me.labelStyle;
        }
        slider.listeners = {
            change: function(slider, newValue, thumb) {
                if(me.propagateToTextField===true){
                     me.__setTextField(slider, newValue, thumb);
                }
                me.fireEvent('change');
            },
            changecomplete: function(){
                me.fireEvent('changecomplete', this.getValue());
            }
        };
        me.items.push(slider);

        // ####  Text fields  ####
        me.items.push({
            xtype: 'textfield',
            itemId: 'slider_text_field',
            width: text_field_width,
            style: 'margin-left: 5px;',
            fieldStyle: 'text-align: center;',
            value: me.value,
            disabled: me.disable_textField,
            enableKeyEvents: true,
            validator: function(value){
                        if(value === '' ){  //|| value === '1' && value != me.maxValue
                            return 'Accepted range: ['+me.minValue+'-'+me.maxValue+']';
                        }
                        return true
            },
            listeners: {
                keyup: function(field) {
                    //console.log(me.down('#slider_text_field').getValue());
                    value = field.getValue().replace(/[^0-9]+/g, '');
                    field.setValue(value);
                    me.__ensure_within_boundaries(field);
                    me.__setSlider(field);
                    me.fireEvent('change');
                    me.fireEvent('changecomplete');
                }
            }
        });
        me.callParent();

        //this.addEvents('change');
        //this.addEvents('changecomplete');
    },

    getValue: function() {
        return this.down('#slider').getValue();
    },

     /**
     *  Update textfield according to slider selection
     */
    __setTextField: function(slider, value){
        var me = this;
        me.down('#slider_text_field').setValue(value);
        me.value = value;
    },

    /**
     *  Update slider according to textfield input
     */
    __setSlider: function(text_field){
        var me = this;
        me.propagateToTextField = false;
        me.down('#slider').setValue(text_field.value);
        me.value = text_field.value;
        //console.log(text_field.value);
        me.propagateToTextField = true;
    },

    /**
     *  Ensure type value does not exceed boundaries (minValue & maxValue) or switch thumb order
     */
    __ensure_within_boundaries: function(text_field){
        var me = this;
        if(me.respect_boundaries){
            if(text_field.value == ''){
                return
            }

            if(text_field.value < me.minValue){
                text_field.setValue(me.minValue);
            }
            if(text_field.value > me.maxValue){
                text_field.setValue(me.maxValue);
            }
        }
    }


});