/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.Field.MultiSlider', {
    extend: 'Ext.container.Container',

    requires: ['IMPACT.view.component.Field.Float'],

    alias: 'widget.impactFieldMultiSlider',
    itemId: 'impactFieldMultiSlider',

    width: 200,
    layout: 'vbox',
    style: 'padding: 1px 1px 1px 1px;',

    items: [],

    fieldLabel: null,
    labelWidth: 0,
    values: [0, 10],
    minValue: 0,
    maxValue: 10,
    increment: 1,
    decimalPrecision: 3,

    propagateToTextField: true,

    respect_boundaries: true,

    initComponent: function(){
        var me = this;
        me.items = [];

        var text_field_width = 80;
        var apply_field_width = 25;
        var spacer_width = (me.width - (text_field_width*2) - apply_field_width) / 2 ;

        // ####  Slider  ####
        var slider = {
            xtype: 'multislider',
            itemId: 'slider',
            width: me.width,
            values: me.values,
            minValue: me.minValue,
            maxValue: me.maxValue,
            increment: me.increment,
            decimalPrecision: me.decimalPrecision,
        };
        if(me.fieldLabel!=null){
            slider.fieldLabel = me.fieldLabel;
            slider.labelWidth = me.labelWidth;
        }
        slider.listeners = {
            change: function(slider, newValue, thumb) {
                if(me.propagateToTextField==true){
                     me.__setTextField(slider, newValue, thumb);
                 }
                me.fireEvent('change');
            },
            changecomplete: function(){
                me.fireEvent('changecomplete');
            }
        }

        // ####  Boundaries' text fields  ####
        var text_fields = {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'impactFieldFloat',
                    itemId: 'min_field',
                    width: text_field_width,
                    fieldStyle: 'text-align: center;',
                    value: me.values[0],
                    allowBlank: false,
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function(text_field) {
                            //me.__ensure_within_boundaries(text_field);
                            //me.__setSlider(text_field);
                            //me.fireEvent('change');
                            //me.fireEvent('changecomplete');
                            me.down("#applyChanges").enable();
                            me.down("#applyChanges").setIconCls('fa fa-lg fa-red fas fa-sync');
                        }
                    }
                },
                {
                    text: '',
                    border: false,
                    width: spacer_width
                },
                {
                    xtype: 'impactFieldFloat',
                    itemId: 'max_field',
                    width: text_field_width,
                    fieldStyle: 'text-align: center;',
                    allowBlank: false,
                    value: me.values[1],
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function(text_field) {
                            //me.__ensure_within_boundaries(text_field);
                            //me.__setSlider(text_field);
                           // me.fireEvent('change');
                            //me.fireEvent('changecomplete');
                            me.down("#applyChanges").enable();
                            me.down("#applyChanges").setIconCls('fa fa-red fas fa-sync');
                        }
                    }
                },
                {
                    text: '',
                    border: false,
                    width: spacer_width
                },
                {
                    xtype: 'button',
                    itemId: 'applyChanges',
                    tooltip: 'Apply changes',
                    fieldLabel:'Apply',
                    width: apply_field_width,
                    iconCls: 'fa-lg fa-green fas fa-sync',
                    cls: 'impact-icon-button',
                    handler: function () {

                        var min = me.down("#min_field").getValue();
                        var max = me.down("#max_field").getValue();

                        if(min === null || max === null){
                            return
                        }
                        if (max < min){
                            me.down("#max_field").markInvalid('Max < Min');
                            return
                        }

                        me.down("#applyChanges").setIconCls('fa-lg fa-green fas fa-sync');
                        me.__ensure_within_boundaries(min,max);
                        me.__setSlider(min,max);
                        me.fireEvent('changecomplete');
                    }
                }
            ]
        };

        me.items.push(slider);
        me.items.push(text_fields);
        me.callParent();

        //this.addEvents('change');
        //this.addEvents('chagecomplete');
    },

    /**
     *  Update minValue & maxValue
     */
    updateMinMax: function(MinMaxValues){
        var me = this;
        me.minValue = MinMaxValues[0];
        me.maxValue = MinMaxValues[1];
        me.down('#slider').minValue = MinMaxValues[0];
        me.down('#slider').maxValue = MinMaxValues[1];
    },


    /**
     *  Update textfield according to slider selection
     */
    __setTextField: function(slider, value, thumb){
        var me = this;
        if(thumb.index==0){
            me.down('#min_field').setValue(value);
            me.values[0] = value;
        } else {
            me.down('#max_field').setValue(value);
            me.values[1] = value;
        }
    },

    /**
     *  Update slider according to textfield input
     */
    __setSlider: function(min, max){
        var me = this;
        me.propagateToTextField = false;
        var slider = me.down('#slider');
        //var slider_values = slider.values;

        slider.setValue(0, min);
        me.values[0] = min;

        slider.setValue(1, max);
        me.values[1] = max;

        me.propagateToTextField = true;
    },
    /**
     *  Ensure type value does not exceed boundaries (minValue & maxValue) or switch thumb order
     */
    __ensure_within_boundaries: function(min, max){
        var me = this;
        if(me.respect_boundaries) {

                ('respect b');
                // min_field < max_field
                var max_field = me.down('#max_field');
                if (max > max_field.value) {
                    max = max_field.value;
                };

                // max_field > min_field
                var min_field = me.down('#min_field');
                if (min < min_field.value) {
                    min = min_field.value;
                };

        }
    }

});