/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.Field.FileByType', {
    extend: 'IMPACT.view.component.Field.File',

    alias: 'widget.ProcessingWindowFileByTypeInput',

    fileType: null,
    returnDir: false,
    data_path: null,
    infoEnabled: false,
    width: 455,
    fileStore: null,
    treeWin: null,
    selectionMode: null,

    initComponent: function(config){
        var me = this;
        me.callParent(arguments);

        me.fileStore = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'full_path'},
                {name: 'basename'},
                {name: 'directory'},
            ],
            proxy: {
                type: 'ajax',
                url: 'get_files.py',
                extraParams: {
                    fileType: me.fileType,
                    data_path: me.data_path
                }
            },
            //autoLoad: me.data_path === '' ? false : true
            autoLoad: false
        });



    },

    __openSelectionWindow: function(){
        var me = this;


        // use standard ImageTree for data selection within DATA folder
        // use ImageTreeBrowser for file system browsing and directory expand listener

        var ImageTreeType = me.data_path === 'IMPACT_DATA_FOLDER' ? 'ImageTree' : 'ImageTreeBrowser';

        me.treeWin = Ext.create('IMPACT.view.component.ImageTree.Window', {
                        tree: {
                            xtype: ImageTreeType,
                            selectionMode: me.selectionMode,
                            fileStore: me.fileStore,
                            selectionStore: me.store,
                            returnDir: me.returnDir,
                            // overwrite superclass
                            filterItem: function(item) {
                                return true
                            },
                            listeners: {
                                'beforeitemexpand': function(node){
                                     if (me.data_path != 'IMPACT_DATA_FOLDER'){
                                        me.__load_node_content(node);
                                     }
                                }
                            }
                        },
                        listeners: {
                            'file_selected': function(selected_files){
                                me.refresh(selected_files);
                            }
                        }
                    });

        me.fileStore.on('load', function(){
            if (me.treeWin){
                me.treeWin.down('#'+ImageTreeType).__buildTree(IMPACT.GLOBALS['data_paths']['data']);  //itemId is the same in ImageTree and ImageTreeBrowser
                me.treeWin.show();
                me.treeWin.toFront();
            }
        });


        me.fileStore.load({
                    params: {fileType: me.fileType,
                        data_path: me.data_path}
                    }
        );

    },



    __load_node_content: function(node){
        var me = this;
        if (me.treeWin == null){
            return
        }

        // expand if already loaded
        if (node.opened == true){
            return
        }

        node.opened = true;
        me.fileStore.load({
            params: {fileType: me.fileType,
                    data_path: node.data && node.data.data_from_DB ? node.data.data_from_DB['full_path'] : ''},
            addRecords: true
        }, true)

    }

});