/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.component.Field.RadioYesNo', {
    extend: 'Ext.form.RadioGroup',

    alias: 'widget.impactFieldRadioYesNo',

    labelWidth: 120,
    width: 250,

    radioName : '',
    defaultValue: 'No',

    initComponent: function() {
        var me = this;

        var yesChecked = me.defaultValue==="Yes";
        var noChecked = me.defaultValue==="No";

        Ext.apply(me, {
            itemId: me.radioName,
            items: [
                { boxLabel: 'Yes', name: me.radioName+'_val', inputValue: 'Yes', checked: yesChecked },
                { boxLabel: 'No',  name: me.radioName+'_val', inputValue: 'No', checked: noChecked }
            ]
        });
        me.callParent();
    }

});