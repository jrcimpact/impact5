/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.UserSettings.component.EndMembers',{
    extend: 'Ext.panel.Panel',

    alias: 'widget.UserSettingsEndMembers',

    requires: [
        'IMPACT.view.UserSettings.component.EndMemberField'
    ],

    layout: 'column',
    margin: '0 0 5px 0',
    bodyStyle: 'padding: 5px;',
    border: false,

    items: [],

    initComponent: function(){
        var me = this;
        var items = [];
        Ext.each( me.itemData, function(){
            items.push(
                {
                    xtype: 'UserSettingsEndMemberField',
                    name: this[0],
                    fieldLabel: this[1],
                    value: this[2]
                }
            );
        });
        me.items = items;
        me.callParent();
    }

});