/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.component.Legend', {
    extend: 'Ext.panel.Panel',

    requires: [
        'IMPACT.view.component.ColorPalette.Vector.EditableGrid',
        'IMPACT.view.component.ColorPalette.Raster.EditableGrid'
    ],

    store: null,
    gridType: null,
    //layout: 'fit',
    items: [
        {
            xtype: 'panel',
            itemId: 'accordion',
            header: false,
            border: false,
            layout: {
                type: 'accordion',
                multi: true,
                collapseFirst: true,
                fill: false
            },
            items: []
        },
        {
            xtype: 'button',
            text: 'Add new legend',
            itemId: 'addNew',
            style: {
                marginTop: '5px'
            },
            handler: function(){
                var me = this.up('panel');
                Ext.Msg.prompt('New legend', 'Please enter the new legend name:', function(btn, text){
                    if (btn === 'ok'){
                        var type = text.toLowerCase().replace(/ /g,'').replace(/[^\w-]+/g,'');
                        me.__addItem(type, text);
                    }
                });
            }
        }
    ],
//
    initComponent: function(){
        var me = this;
        var accordion = [{hidden: true}];
        var legends = me.store.getAll();

        for(var i=0; i<legends.length; i++){
             accordion.push(
                me.__createItem(legends[i].type, legends[i].name)
            );
        }
        me.items[0].items = accordion;

        this.callParent();
    },

    __createItem: function(type, name){
        var me = this;
        return {
            xtype: me.gridType,
            itemId: me.gridType+'_'+type,
            title: '<b>'+name+'</b>',
            legendName: name,
            legendType: type,
            collapsed: true,
            tools:[
                {
                    type: 'close',
                    tooltip: 'Remove',
                    handler: function(event, toolEl, panelHeader) {
                        Ext.Msg.show({
                            title: 'Delete the legend?',
                            msg: 'Delete the legend?',
                            buttonText: {
                                ok: 'Delete',
                                cancel: 'Cancel'
                            },
                            fn: function(btn){
                                if (btn === 'ok'){
                                    me.down('#'+me.gridType+'_'+type).destroy();
                                }
                            }
                        });
                    }
                }
            ]
        }
    },

    __addItem: function(type, name){
        var me = this;
        me.down('#accordion').add(me.__createItem(type, name));
    }

});