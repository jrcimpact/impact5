/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.component.CommentsGrid',{
    extend: 'Ext.grid.Panel',

    alias: 'widget.UserSettingsCommentsGrid',

    title: '',
    hideHeaders: true,
    margin: '5px 0 5px 0',
    store: null,

    columns: [
        {
            dataIndex: 'label',
            flex: 1,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            xtype: 'templatecolumn',
            width: 25,
            align: 'center',
            text: '',
            tpl: '<span class="fa fa-lg fa-red fa-times-circle pointer"></span>',
            listeners: {
                click: function(gridView, gridCell, rowIndex){
                    gridView.getStore().removeAt(rowIndex);
                }
            }
        }
    ],

    bbar: [{
        xtype: 'button',
        text: 'Add new item',
        handler:function(){
            var newRow = {
                label: ''
            };
            Ext.data.StoreManager.lookup( this.up('grid').storeID ).insert(0, newRow);
        }
    }],

    plugins:[
        {
            ptype: 'cellediting',
            clicksToEdit: 1
        }
    ],

    initComponent: function(config) {
        this.callParent(arguments);
        this.reconfigure (
            Ext.create('Ext.data.ArrayStore', {
                storeId: this.storeID,
                fields: ['label'],
                data : this.storeData
            })
        );
    }

});



