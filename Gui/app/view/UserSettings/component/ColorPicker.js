/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.component.ColorPicker',{
    extend: 'Ext.window.Window',

    layout: 'fit',
    border: false,
    draggable: false,
    closable: false,
    closeAction: 'destroy',

    itemId: 'UserSettingsColorPicker',

    initColor: '000000',
    gridStore: null,
    rowIndex: null,
    dataIndex: null,

    items: [],

    initComponent: function(){
        var me = this;
        me.callParent();
        me.add({
            xtype: 'colorpicker',
            allowReselect: true,
            value: me.initColor,
            listeners: {
                select: function(item, color){
                    me.setColor(color);
                }
            }
        });
    },

    constructor: function(gridStore, rowIndex, dataIndex, mouseXY) {
        var me = this;
        me.gridStore = gridStore;
        me.rowIndex = rowIndex;
        me.dataIndex = dataIndex;
        var rgbColor = me.gridStore.getAt(me.rowIndex).get(me.dataIndex).split(',');
        me.initColor = IMPACT.Utilities.Common.rgbToHex(rgbColor[0], rgbColor[1], rgbColor[2]);
        me.callParent();
        me.showAt(mouseXY);
    },

    setColor: function(color){
        var me = this;
        var rgbColor = IMPACT.Utilities.Common.hexToRgb(color).join(',');
        me.gridStore.getAt(me.rowIndex).set(me.dataIndex, rgbColor);
        me.destroy();
        me.rowIndex = null;
    }

});