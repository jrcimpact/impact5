/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.UserSettings', {
    extend: 'Ext.Panel',

    itemId: 'UserSettings',
    //title: 'IMPACT Tool Settings',

   // y: 180,
   // width: 650,
    layout: 'fit',

    //scrollY: true,
    //autoShow: true,
    //closeAction: 'destroy',
   // renderTo: Ext.ComponentQuery.query("UserSettingsTab")[0],

    requires: [
        'IMPACT.view.UserSettings.tab.General',
        'IMPACT.view.UserSettings.tab.LegendRaster',
        'IMPACT.view.UserSettings.tab.LegendVector',
        'IMPACT.view.UserSettings.tab.Validation',
        'IMPACT.view.UserSettings.tab.Processing',
        'IMPACT.view.UserSettings.tab.Update'
    ],


    resizable: false,
    buttons: [
        {
            text: 'Factory Reset',
            itemId: 'UserSettings_reset_button',
            handler: function() {
                var settingsGui = this.up('#UserSettings');
                Ext.Msg.show({
                    title:'Factory Reset',
                    msg: 'Would you like to reset factory values and refresh the page?',
                    buttons: Ext.Msg.YESNO,
                    fn: function(e){
                        if(e === 'yes'){
                            settingsGui.resetFactorySettings();
                        }
                    }
                });
            }
        },
//        {
//            text: 'Cancel',
//            itemId: 'UserSettings_cancel_button',
//            handler: function() {
//                this.up('#UserSettings').close();
//            }
//        },
        {
            text: 'Save',
            itemId: 'UserSettings_save_button',
            handler: function() {
                this.up('#UserSettings').saveSettings();
                Ext.Msg.show({
                    title:'Apply your settings',
                    msg: 'Would you like to refresh the page now?',
                    buttons: Ext.Msg.YESNO,
                    fn: function(e){
                        if(e === 'yes'){
                            location.reload(true);
                        }
                    }
                });
            }
        }
    ],
    items: [
        {
            xtype: 'tabpanel',
            activeTab: 0,
            itemId: 'UserSettingsTabpanelId',
            defaults:{
                bodyPadding: 25,
//                //height: 550,
//                //maxHeight: 400,
                overflowY: 'auto',
//                border: false
            },
            items: [
                // ##########  General Options  ##########
                {
                    xtype: 'UserSettingsTabGeneral',
                    title: '<b style="color: Blue;">General</b>'
                },

//                 ############  Vector Legends  ############
                {
                    xtype: 'UserSettingsTabLegendVector',
                    title: '<b style="color: Blue;">Vector Legends</b>'
                },
//                // ############  Raster Styles  ############
                {
                    xtype: 'UserSettingsTabLegendRaster',
                    title: '<b style="color: Blue;">Raster Styles</b>'
                },
//                // ##########  Vector Editing  ##########
                {
                    xtype: 'UserSettingsTabValidation',
                    title: '<b style="color: Blue;">Vector Editing</b>'
                },
//                // ###########  Processing  ###########
                {
                    xtype: 'UserSettingsTabProcessing',
                    title: '<b style="color: Blue;">Processing</b>'
                },
//                // ###########  Update  ###########
                {
                    xtype: 'UserSettingsTabUpdate',
                    title: '<b style="color: Blue;">Update</b>'
                }
            ],
            listeners: {
                tabchange: function(tabPanel, newCard, oldCard, eOpts){
                    if(newCard.xtype === "UserSettingsTabUpdate"){
                        tabPanel.up("#UserSettings").disableButtons(true);
                    } else {
                        tabPanel.up("#UserSettings").disableButtons(false);
                    }
                }
            }
        }
    ],
    listeners: {
        drag: function(){
            Ext.each( Ext.ComponentQuery.query('#UserSettingsColorPicker'), function(){
                this.close();
            });
        },
        close: function(){
            Ext.each( Ext.ComponentQuery.query('#UserSettingsColorPicker'), function(){
                this.close();
            });
        }
    },

    disableButtons: function(disable){
        var me = this;
        if(disable){
            me.down('#UserSettings_reset_button').disable();
            me.down('#UserSettings_save_button').disable();
        } else {
            me.down('#UserSettings_reset_button').enable();
            me.down('#UserSettings_save_button').enable();
        }
    },

    /**
     * Save Settings to JSON
     */
    saveSettings: function(){

        var me = this;

        // Parse fields from UserSettings' window
        var fields = me.query('textfield');
        var fieldsValues = {};
        for(var i=0; i<fields.length; i++){
            //console.log(fields[i].name);
            //console.log(fields[i].value);
            fieldsValues[fields[i].name] = fields[i].value;
        }

        // Parse ColorPalettes

        fieldsValues['map_legends_vector'] = me.__parseColorPalette(me.down('UserSettingsTabLegendVector'), 'VectorEditableGrid', ['class','label','color','opacity']);
        fieldsValues['map_legends_raster'] = me.__parseColorPalette(me.down('UserSettingsTabLegendRaster'), 'RasterEditableGrid', ['range_min','range_max','label','color_min','color_max']);

        // Re-organize some fields
        fieldsValues["vector_editing_predefined_comments"] = me.__parseGrid('VectorEditingPredefinedComments', ['label']);
        fieldsValues['map_options_max_extent'] = [
            fieldsValues['map_options_max_extent_w'],
            fieldsValues['map_options_max_extent_s'],
            fieldsValues['map_options_max_extent_e'],
            fieldsValues['map_options_max_extent_n']
        ];

        // send to server and save to JSON
        Ext.Ajax.request({
            url: 'save_user_settings.py',
            method: "POST",
            params: {
                toDo: 'save',
                settings : Ext.JSON.encode(fieldsValues)
            },
            scope: this,
            success: function(response){

                //  todo : save_user_settings.py does not return the settings, why this part ?  fix the .py code accordingly
                if(response.responseText != ''){
                    IMPACT.SETTINGS = Ext.decode(response.responseText);
                    Ext.data.StoreManager.lookup('RasterStyles').loadData(IMPACT.SETTINGS['map_legends']['raster']);
                    Ext.data.StoreManager.lookup('VectorLegends').loadData(IMPACT.SETTINGS['map_legends']['vector']);
                }


                this.close();
            }
        });
    },

    __parseColorPalette: function(tab, gridType, grid_values){
        var me = this;
        var palettes = tab.query(gridType);
        var paletteValues = [];
        for(var i=0; i<palettes.length; i++){
            var palette = {
                "type": palettes[i].legendType,
                "name": palettes[i].legendName,
                "classes": me.__parseGrid(gridType+'_'+palettes[i].legendType, grid_values)
            };
            paletteValues.push(palette);
        }
        return paletteValues;
    },

    __parseGrid: function(storeId, fields){

        var grid = Ext.data.StoreManager.lookup(storeId).data.items;
        var records = [];
        for(var i=0; i<grid.length; i++){
            var record = [];
            for(var j=0; j<fields.length; j++){
                record.push(grid[i].data[fields[j]].toString())
            }
            records.push(record);
        }
        return records;
    },

    /**
     * Delete user's settings
     */
    resetFactorySettings: function(){
        Ext.Ajax.request({
            url: 'save_user_settings.py',
            method: "POST",
            params: {
                toDo: 'delete'
            },
            success: function(){
                //this.close();
                location.reload(true);
            },
            failure: function(){
                console.log("Failure while loading default parameters");
            }
        });
    }

});
