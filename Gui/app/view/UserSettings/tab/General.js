/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.tab.General', {
    extend: 'Ext.panel.Panel',

    requires: ['IMPACT.view.component.Field.Combo'],

    alias: 'widget.UserSettingsTabGeneral',

    border: false,

    initComponent: function () {
        var me = this;

        me.items = [
           {
                xtype: 'panel',
                title: '<b>IMPACT Tool Settings</b>',
                margin: '0 0 10px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                    // ### TMF Quality Tool ###
                    {
                        xtype: 'impactFieldCombo',
                        name: 'tool_options_validationGUI',
                        fieldLabel: 'Validation Tools',
                        value: IMPACT.SETTINGS['tool_options']['validationGUI'],
                        store: ['default','TMF', 'GSW'],
                        labelWidth: 200
                    },
                    {
                        xtype: 'impactFieldCombo',
                        name: 'tool_options_scanRemoteData',
                        fieldLabel: 'Scan and load Remote Data content (e.g. mapped network drive of JRC eStation/cStation)',
                        value: IMPACT.SETTINGS['tool_options']['scanRemoteData'],
                        store: ['enabled', 'disabled'],
                        labelWidth: 200
                    }
                ]
            },
            {
                xtype: 'panel',
                title: '<b>Internet Settings</b>',
                margin: '0 0 10px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                    // ###  ###
                    {
                        xtype: 'textfield',
                        name: 'internet_options_proxy',
                        fieldLabel: 'Proxy',
                        value: IMPACT.SETTINGS['internet_options']['proxy']
                    },
                    {
                        xtype: 'textfield',
                        name: 'internet_options_username',
                        fieldLabel: 'Username',
                        value: IMPACT.SETTINGS['internet_options']['username']
                    },
                    {
                        xtype: 'textfield',
                        name: 'internet_options_password',
                        inputType: 'password',
                        fieldLabel: 'Password  Note:[@`"] not supported',
                        value: IMPACT.SETTINGS['internet_options']['password']
                    },
                ]
            },
            {
                xtype: 'panel',
                title: '<b>Google Earth Engine Authentication </b>',
                margin: '0 0 10px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                    {
                            xtype: 'button',
                            itemId: 'GEE_Authenticate',
                            tooltip: 'Authenticate',
                            text:'Authenticate',
                            labelWidth: 80,
                            width: 120,
                            iconCls: 'fa-lg fa-green fas fa-sync',
                            //cls: 'impact-icon-button',
                            handler: function () {
                                var params = {
                                    toolID:  "eeAuthenticate",
                                };
                                alert('Paste the authentication code into the IMPACT DOS Console');
                                IMPACT.Utilities.Requests.launchProcessing(params)
                            }
                        }
                ]
            },
            {
                xtype: 'panel',
                title: '<b>API Key for WMS / Tile Services</b>',
                margin: '0 0 10px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                    {
                        xtype: 'textfield',
                        name: 'apiKey_options_googleKey',
                        fieldLabel: 'Google API KEY',
                        width: 350,
                        value: IMPACT.SETTINGS['apiKey_options']['googleKey']
                    },
                    {
                        xtype: 'textfield',
                        name: 'apiKey_options_planetKey',
                        fieldLabel: 'Planet API KEY',
                        width: 350,
                        value: IMPACT.SETTINGS['apiKey_options']['planetKey']
                    },{
                        html:['How to get <a href="https://developers.google.com/maps/documentation/embed/get-api-key">Google</a> or ',
                              '<a href="https://developers.planet.com/quickstart/apis/"> Planet</a> API KEYs to access data programmatically'
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                title: '<b>Map Settings</b>',
                bodyStyle: 'padding: 5px;',
                items: [
                    // ### Single Tile ###
                    {
                        xtype: 'impactFieldCombo',
                        name: 'map_options_singleTile',
                        fieldLabel: 'Single tile',
                        value: IMPACT.SETTINGS['map_options']['singleTile'],
                        store: [
                            ['true', 'yes'],
                            ['false', 'no']
                        ],
                        labelWidth: 75,
                        width: 140
                    },
                    // ### Max Extent ###
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: '<b>Max Extent</b>',
                        labelWidth: 80,
                        layout: 'form',
                        width: '200px',
                        defaults: {
                            labelWidth: 40,
                            width: 60,
                            labelStyle: 'text-align: right; font-style: italic;',
                            labelSeparator: ''
                        },
                        items: [
                            {
                                xtype: 'numberfield',
                                name: 'map_options_max_extent_w',
                                fieldLabel: 'West',
                                value: IMPACT.SETTINGS['map_options']['max_extent'][0],
                                minValue: -180,
                                maxValue: 0
                            },
                            {
                                xtype: 'numberfield',
                                name: 'map_options_max_extent_s',
                                fieldLabel: 'South',
                                value: IMPACT.SETTINGS['map_options']['max_extent'][1],
                                minValue: -90,
                                maxValue: 0
                            },
                            {
                                xtype: 'numberfield',
                                name: 'map_options_max_extent_e',
                                fieldLabel: 'East',
                                value: IMPACT.SETTINGS['map_options']['max_extent'][2],
                                minValue: 0,
                                maxValue: 180
                            },
                            {
                                xtype: 'numberfield',
                                name: 'map_options_max_extent_n',
                                fieldLabel: 'North',
                                value: IMPACT.SETTINGS['map_options']['max_extent'][3],
                                minValue: 0,
                                maxValue: 90
                            }
                        ]
                    },

                    // ### Initial position ###
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: '<b>Initial position</b>',
                        labelWidth: 95,
                        layout: 'form',
                        width: '200px',
                        defaults: {
                            labelWidth: 40,
                            width: 80,
                            labelStyle: 'text-align: right; font-style: italic;',
                            labelSeparator: ''
                        },
                        items: [
                            {
                                xtype: 'numberfield',
                                name: 'map_options_init_lat',
                                fieldLabel: 'Lat',
                                value: IMPACT.SETTINGS['map_options']['init_lat'],
                                minValue: -90,
                                maxValue: 90
                            },
                            {
                                xtype: 'numberfield',
                                name: 'map_options_init_lon',
                                fieldLabel: 'Lon',
                                value: IMPACT.SETTINGS['map_options']['init_lon'],
                                minValue: -180,
                                maxValue: 180
                            },
                            {
                                xtype: 'impactFieldCombo',
                                name: 'map_options_init_zoom',
                                fieldLabel: 'Zoom',
                                value: IMPACT.SETTINGS['map_options']['init_zoom'],
                                labelWidth: 40,
                                width: 120,
                                store: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14']
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                title: '<b>WMS Settings</b>',
                margin: '0 0 10px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                        {
                            xtype: 'impactFieldCombo',
                            name: 'wms_options_s2',
                            fieldLabel: 'Sentinel2 annual composite',
                            value: IMPACT.SETTINGS['wms_options']['s2'],
                            store: [
                                ['true', 'yes'],
                                ['false', 'no']
                            ],
                            labelWidth: 230,
                            width: 290
                        },
                        {
                            xtype: 'impactFieldCombo',
                            name: 'wms_options_ortofoto',
                            fieldLabel: 'Ortofoto 2016 - 2019',
                            value: IMPACT.SETTINGS['wms_options']['ortofoto'],
                            store: [
                                ['true', 'yes'],
                                ['false', 'no']
                            ],
                            labelWidth: 230,
                            width: 290
                        },
                        {
                            xtype: 'impactFieldCombo',
                            name: 'wms_options_tmf',
                            fieldLabel: 'Tropical Moist Forest (TMF)',
                            value: IMPACT.SETTINGS['wms_options']['tmf'],
                            store: [
                                ['true', 'yes'],
                                ['false', 'no']
                            ],
                            labelWidth: 230,
                            width: 290
                        }

                ]
                }
        ];

        me.callParent();
    }

});