/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.tab.Processing', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.UserSettingsTabProcessing',

    requires: [
        'IMPACT.view.UserSettings.component.EndMembers'
    ],

    initComponent: function () {
        var me = this;

        me.items = [
            {
                xtype: 'panel',
                title: '<b>Classification Settings</b>',
                collapsible: true,
                margin: '0 0 5px 0',
                bodyStyle: 'padding: 5px;',
                items: [
                    {
                        html: '<i>Values are expressed in TOA [0-1]</i>',
                        border: false,
                        margin: '0 0 5px 0'
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'Landsat soil endmembers',
                        itemData: [
                            ['processing_classification_Landsat_soil_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['b']],
                            ['processing_classification_Landsat_soil_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['g']],
                            ['processing_classification_Landsat_soil_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['r']],
                            ['processing_classification_Landsat_soil_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['near']],
                            ['processing_classification_Landsat_soil_endmember_swir1', 'SWIR 1', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['swir1']],
                            ['processing_classification_Landsat_soil_endmember_swir2', 'SWIR 2', IMPACT.SETTINGS['processing_classification']['Landsat_soil_endmember']['swir2']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'Landsat vegetation endmembers',
                        itemData: [
                            ['processing_classification_Landsat_vegetation_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['b']],
                            ['processing_classification_Landsat_vegetation_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['g']],
                            ['processing_classification_Landsat_vegetation_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['r']],
                            ['processing_classification_Landsat_vegetation_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['near']],
                            ['processing_classification_Landsat_vegetation_endmember_swir1', 'SWIR 1', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['swir1']],
                            ['processing_classification_Landsat_vegetation_endmember_swir2', 'SWIR 2', IMPACT.SETTINGS['processing_classification']['Landsat_vegetation_endmember']['swir2']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'Landsat water endmembers',
                        itemData: [
                            ['processing_classification_Landsat_water_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['b']],
                            ['processing_classification_Landsat_water_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['g']],
                            ['processing_classification_Landsat_water_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['r']],
                            ['processing_classification_Landsat_water_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['near']],
                            ['processing_classification_Landsat_water_endmember_swir1', 'SWIR 1', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['swir1']],
                            ['processing_classification_Landsat_water_endmember_swir2', 'SWIR 2', IMPACT.SETTINGS['processing_classification']['Landsat_water_endmember']['swir2']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'Landsat forest normalization',
                        itemData: [
                            ['processing_classification_Landsat_forest_normalization_b', 'B', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['b']],
                            ['processing_classification_Landsat_forest_normalization_g', 'G', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['g']],
                            ['processing_classification_Landsat_forest_normalization_r', 'R', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['r']],
                            ['processing_classification_Landsat_forest_normalization_near', 'NIR', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['near']],
                            ['processing_classification_Landsat_forest_normalization_swir1', 'SWIR 1', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['swir1']],
                            ['processing_classification_Landsat_forest_normalization_swir2', 'SWIR 2', IMPACT.SETTINGS['processing_classification']['Landsat_forest_normalization']['swir2']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'RapidEye soil endmembers',
                        itemData: [
                            ['processing_classification_RapidEye_soil_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['RapidEye_soil_endmember']['b']],
                            ['processing_classification_RapidEye_soil_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['RapidEye_soil_endmember']['g']],
                            ['processing_classification_RapidEye_soil_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['RapidEye_soil_endmember']['r']],
                            ['processing_classification_RapidEye_soil_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['RapidEye_soil_endmember']['near']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'RapidEye vegetation endmembers',
                        itemData: [
                            ['processing_classification_RapidEye_vegetation_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['RapidEye_vegetation_endmember']['b']],
                            ['processing_classification_RapidEye_vegetation_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['RapidEye_vegetation_endmember']['g']],
                            ['processing_classification_RapidEye_vegetation_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['RapidEye_vegetation_endmember']['r']],
                            ['processing_classification_RapidEye_vegetation_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['RapidEye_vegetation_endmember']['near']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'RapidEye water endmembers',
                        itemData: [
                            ['processing_classification_RapidEye_water_endmember_b', 'B', IMPACT.SETTINGS['processing_classification']['RapidEye_water_endmember']['b']],
                            ['processing_classification_RapidEye_water_endmember_g', 'G', IMPACT.SETTINGS['processing_classification']['RapidEye_water_endmember']['g']],
                            ['processing_classification_RapidEye_water_endmember_r', 'R', IMPACT.SETTINGS['processing_classification']['RapidEye_water_endmember']['r']],
                            ['processing_classification_RapidEye_water_endmember_near', 'NIR', IMPACT.SETTINGS['processing_classification']['RapidEye_water_endmember']['near']]
                        ]
                    },
                    {
                        xtype: 'UserSettingsEndMembers',
                        title: 'RapidEye forest normalization',
                        itemData: [
                            ['processing_classification_RapidEye_forest_normalization_b', 'B', IMPACT.SETTINGS['processing_classification']['RapidEye_forest_normalization']['b']],
                            ['processing_classification_RapidEye_forest_normalization_g', 'G', IMPACT.SETTINGS['processing_classification']['RapidEye_forest_normalization']['g']],
                            ['processing_classification_RapidEye_forest_normalization_r', 'R', IMPACT.SETTINGS['processing_classification']['RapidEye_forest_normalization']['r']],
                            ['processing_classification_RapidEye_forest_normalization_near', 'NIR', IMPACT.SETTINGS['processing_classification']['RapidEye_forest_normalization']['near']]
                        ]
                    }
                ]
            }
        ];

        me.callParent();
    }

});