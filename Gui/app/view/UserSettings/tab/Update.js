/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.tab.Update', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.UserSettingsTabUpdate',

    border: false,

    items: [],

    initComponent: function(){
        var me = this;
        me.items = [];

        if(IMPACT.GLOBALS['repository']['branch']!='master'){
            me.items.push({
                xtype: 'panel',
                html: 'Current branch: <b style="color: Red;">'+IMPACT.GLOBALS['repository']['branch']+'</b><br/>',
                //style: 'margin-bottom: 5px;',
                border: false
            });
        }

        me.items.push({
            xtype: 'panel',
            html: 'Current version: <b>'+IMPACT.Utilities.Impact.version_readable()+'</b><br/>',
            style: 'margin-bottom: 10px;',
            border: false
        });

        if(IMPACT.GLOBALS['repository']['up-to-date']==false){
            me.items.push({
                xtype: 'panel',
                title: '<b>Version '+IMPACT.Utilities.Impact.version_readable(IMPACT.GLOBALS['repository']['remote']['version'])+' available</b>',
                margin: '0 0 5px 0',
                bodyStyle: 'padding: 5px;',
                items:[{
                    xtype: 'panel',
                    border: false,
                    bodyStyle: 'color: Green; font-weight: bold;',
                    items:[
                    {
                        xtype: 'panel',
                        html: IMPACT.GLOBALS['repository']['remote']['changelog'].replace('\n', '<br />'),
                        style: 'margin-bottom: 10px;',
                        border: false
                    },
                    {
                        xtype: 'button',
                        text: 'Update',
                        itemId: 'UpdateButtonId',
                        handler: function() {
                            me.confirm_update();
                        }
                    }
                    ]
                }]
            });
        } else {
            me.items.push({
                xtype: 'panel',
                html:  'IMPACT Tool is up to date.',
                border: false,
                bodyStyle: 'color: Green; font-weight: bold;',
            });
        }


        me.callParent();
    },

    confirm_update: function(){
        var me = this;
        Ext.Msg.show({
            title: 'IMPACT update',
            msg: 'IMPACT toolbox and all running processes will be stopped.\nAre you sure you want to update now?',
            buttons: Ext.Msg.YESNO,
            fn: function(e){
                if(e == 'yes'){
                    me.query('#UpdateButtonId')[0].disable();
                    IMPACT.Utilities.Requests.launchProcessing({toolID: 'update'});
                }
            }
        });
    }

});