/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.UserSettings.tab.Validation', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.UserSettingsTabValidation',

    requires: [
        'IMPACT.view.UserSettings.component.CommentsGrid'
    ],

    border: false,

    initComponent: function () {
        var me = this;

        me.items = [
        {
            xtype: 'panel',
            title: '<b>Shapefile DBF attributes</b>',
            margin: '0 0 10px 0',
            bodyStyle: 'padding: 5px;',
            defaults: {
                labelStyle: 'font-weight: bold;',
                labelSeparator: '',
                width: 300
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_ID',
                    fieldLabel: 'Feature ID',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['ID']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_cluster_t1',
                    fieldLabel: 'Cluster (T1)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t1']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_cluster_t2',
                    fieldLabel: 'Cluster (T2)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t2']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_cluster_t3',
                    fieldLabel: 'Cluster (T3)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['cluster_t3']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_class_t1',
                    fieldLabel: 'Class (T1)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t1']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_class_t2',
                    fieldLabel: 'Class (T2)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t2']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_class_t3',
                    fieldLabel: 'Class (T3)',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['class_t3']
                },
                {
                    xtype: 'textfield',
                    name: 'vector_editing_attributes_comments',
                    fieldLabel: 'Comments',
                    value: IMPACT.SETTINGS['vector_editing']['attributes']['comments']
                }
            ]
        },
        {
            xtype: 'UserSettingsCommentsGrid',
            title: 'Predefined Comments',
            storeID: 'VectorEditingPredefinedComments',
            storeData: IMPACT.SETTINGS['vector_editing']['predefined_comments']
        }
        ];

        me.callParent();
    }

});