/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Viewport',{
    requires:[
        'IMPACT.view.Processing.Panel',
        'IMPACT.view.Workspace.Main.Workspace',
        'IMPACT.view.UserSettings.UserSettings'
    ],
    extend: 'Ext.Viewport',

    itemId: 'IMPACTViewport',

    layout : "border",
    border : false,

    initComponent: function(config){
        var me = this;

        // ####### EC-JRC header (north) #######
        var header = {
            region : "north",
            xtype : "container",
            loader: {
                url: 'app/headerJRC.htm',
                autoLoad: true,
                listeners: {
                    load: function(){
                        // ## Display version label ##
                        Ext.get("versionLabel").dom.innerHTML = IMPACT.Utilities.Impact.version_readable();
                        if(IMPACT.GLOBALS.repository && IMPACT.GLOBALS['repository']['branch'] && IMPACT.GLOBALS['repository']['branch']!=='master'){
                            Ext.get("branchLabel").dom.innerHTML = 'Branch: '+IMPACT.GLOBALS['repository']['branch'];
                        }
                    }
                }
            }

        };


        // ####### Processing Panel (east) #######
        var ProcessingPanel = Ext.create('IMPACT.view.Processing.Panel');
        var SettingPanel = Ext.create('IMPACT.view.UserSettings.UserSettings');

        // ####### Main Panel (center) #######
        var mainTabPanel = {
            region: "center",
            xtype: 'tabpanel',
            itemId: 'viewportTab',

//            style: 'margin-left: 5px; margin-top: 25px;',
            defaults: {
                border: false,
                layout: "border",
                deferredRender: false,
                collapsible: false
            },
            items: [
                Ext.create('IMPACT.view.Workspace.Main.Workspace'),
                {
                    xtype: "panel",
                    title : 'Logs Monitor',
                    id : 'logTab',
                    iconCls: 'x-fa fa-lg fa-black fa-tasks',
                    layout: 'fit',
                    items: [
                        {
                            xtype: "panel",
                            id: 'logPanel',
                            style: { color: "red" },
                            bodyStyle: "backgroundColor: White;",
                            html: '',
                            autoScroll: true
                        }
                    ]
                },{
                    xtype: 'panel',
                    title: 'Settings',
                    itemId: 'UserSettingsTab',
                    id : 'settingsTab',
                    layout: 'fit',
                    //closable: false,
                    autoScroll: true,
                    iconCls: 'x-fa fa-lg fa-black fa-wrench',
                    //renderTo: Ext.getBody(),
                    items:[SettingPanel],

                }
            ],

            activeTab : 0,

            listeners : {
                'tabchange' : function(tabPanel, tab) {
                    this.up('#IMPACTViewport').switchTab(tab);
                }
            }

        };

        me.items =[
            header,
            ProcessingPanel,
            mainTabPanel
        ];

        me.callParent();
    },

    switchTab: function(tab){
        var me = this;

        if(Ext.ComponentQuery.query('#StatsWindow').length>0){
            Ext.ComponentQuery.query('#StatsWindow')[0].destroy();
        }

        if(tab.id=="MainWorkspace" || tab.id=="logTab"){
            Ext.ComponentQuery.query('#ProcessingPanel')[0].setVisible(true);
        } else {
            Ext.ComponentQuery.query('#ProcessingPanel')[0].setVisible(false);
        }

        if(Ext.ComponentQuery.query('#ColorPaletteVectorWindow')[0]){
            Ext.ComponentQuery.query('#ColorPaletteVectorWindow')[0].close;
        }


        me.down('#MainWorkspace').down('#MainToolbar').setPanMode();
        //OpenLayers.Control.CacheWrite.clearCache();

    },

    resetActiveTab: function(){
        this.down('#MainWorkspace').down('#MainToolbar').setPanMode();
        this.down('#viewportTab').setActiveTab(0);
    }


});