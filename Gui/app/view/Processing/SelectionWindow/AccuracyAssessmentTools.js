/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.AccuracyAssessmentTools', {
    extend: 'Ext.window.Window',

    itemId: 'AccuracyTools',
    title: 'Accuracy Assessment Tools',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width:455,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            html: '<b>Photos to MAP</b>',
            xtype: 'button',
            tooltip: 'Visualize your geo-referenced photos on the map',
            cls : 'ToolWindow_Icon ProcessingPanel_Icon_PhotosToKML',
            handler: function () {
                this.up('#AccuracyTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowPhotosToKML').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.PhotosToKML');
                }
                Ext.ComponentQuery.query('#ProcessingWindowPhotosToKML')[0].show();
            }
        },
        {
            html: '<b>Random Sampling</b>',
            xtype: 'button',
            tooltip: 'Generate (stratified) Random Sampling',
            cls: 'ToolWindow_Icon ProcessingPanel_Icon_Sampling',
            handler: function () {
                alert('Tool under development, use at your own risk!');
                this.up('#AccuracyTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowRandomSampling').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.RandomSampling');
                }
                Ext.ComponentQuery.query('#ProcessingWindowRandomSampling')[0].show();
            }
        },
        {
            html: '<b>Confusion Matrix</b>',
            xtype: 'button',
            tooltip: 'Calculate the Confusion Matrix and Accuracy report',
            cls: 'ToolWindow_Icon ProcessingPanel_Icon_ConfusionM',
            handler: function () {
                this.up('#AccuracyTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowConfusionM').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.ConfusionM');
                }
                Ext.ComponentQuery.query('#ProcessingWindowConfusionM')[0].show();
            }
        }


    ]


});
