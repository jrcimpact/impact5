/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.ClassificationTools', {
    extend: 'Ext.window.Window',

    itemId: 'ClassificationTools',
    title: 'Classification Tools',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width:455,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            html: '<b>Automatic Classification</b>',
            xtype: 'button',
            tooltip: 'Fully Automatic Classification for Landsat, S2, RE',
            cls : 'ToolWindow_Icon ToolWindow_Icon_Classification',
            handler: function () {
                this.up('#ClassificationTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowClassification').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Classification');
                }
                Ext.ComponentQuery.query('#ProcessingWindowClassification')[0].show();
            }
        },
        {
            html: '<b>K-Means</b>',
            xtype: 'button',
            tooltip: 'K-means Clustering',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Kmeans',
            handler: function () {
                this.up('#ClassificationTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowKmeans').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Kmeans');
                }
                Ext.ComponentQuery.query('#ProcessingWindowKmeans')[0].show();
            }
        },
        {
            html: '<b>Water Dynamics</b>',
            xtype: 'button',
            tooltip: 'Water Dynamics',
            cls: 'ToolWindow_Icon ToolWindow_Icon_WaterBalance',
            handler: function () {
                this.up('#ClassificationTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowWaterBalance').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.WaterBalance');
                }
                Ext.ComponentQuery.query('#ProcessingWindowWaterBalance')[0].show();
            }
        }


    ]


});
