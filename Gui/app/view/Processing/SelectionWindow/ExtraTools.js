/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.ExtraTools', {
    extend: 'Ext.window.Window',

    itemId: 'ProcessingWindowExtraTools',
    title: 'Tools',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width: 455,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            html: '<b>Raster Conversion </br> Import</b>',
            xtype: 'button',
            tooltip: 'Raster Conversion / Import / Raster to Vector ',
            cls: 'ToolWindow_Icon ToolWindow_Icon_RasterConversion',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowRasterConversion').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.RasterConversion');
                }
                Ext.ComponentQuery.query('#ProcessingWindowRasterConversion')[0].show();
            }
        },
        {
            html: '<b>Vector Conversion </br> </b>',
            xtype: 'button',
            tooltip: 'Vector to Raster Conversion ',
            cls: 'ToolWindow_Icon ToolWindow_Icon_VectorConversion',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowVectorConversion').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.VectorConversion');
                }
                Ext.ComponentQuery.query('#ProcessingWindowVectorConversion')[0].show();
            }
        },
        {
            html: '<b>Mosaic</b>',
            xtype: 'button',
            tooltip: 'Mosaic & Time Series',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Mosaic',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowMosaic').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Mosaic');
                }
                Ext.ComponentQuery.query('#ProcessingWindowMosaic')[0].show();
            }
        },
        {
            html: '<b>Raster Calculator</b>',
            xtype: 'button',
            tooltip: 'Raster Calculator',
            cls: 'ToolWindow_Icon ToolWindow_Icon_RasterCalculator',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowRasterCalculator').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.RasterCalculator');
                }
                Ext.ComponentQuery.query('#ProcessingWindowRasterCalculator')[0].show();
            }
        },
        {
            html: '<b>Fishnet / Sampling</b>',
            xtype: 'button',
            tooltip: 'Create Fishnet / Sampling',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Fishnet',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowFishnet').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Fishnet');
                }
                Ext.ComponentQuery.query('#ProcessingWindowFishnet')[0].show();
            }
        },
        {
            html: '<b>Zonal Statistics</b>',
            xtype: 'button',
            tooltip: 'Zonal Statistics: fill vector polygons with raster statistics',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Statistics',
            handler: function () {
                this.up('#ProcessingWindowExtraTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowStatistics').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Statistics');
                }
                Ext.ComponentQuery.query('#ProcessingWindowStatistics')[0].show();
            }
        }

    ]


});
