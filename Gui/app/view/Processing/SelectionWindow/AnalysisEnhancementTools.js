/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.AnalysisEnhancementTools', {
    extend: 'Ext.window.Window',

    itemId: 'AnalysisEnhancementTools',
    title: 'Analysis & Enhancement Tools',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width:465,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            html: '<b>Linear Spectral Unmixing</b>',
            xtype: 'button',
            tooltip: 'Linear Spectral Unmixing',
            cls : 'ToolWindow_Icon ToolWindow_Icon_Unmixing',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowUnmixing').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Unmixing');
                }
                Ext.ComponentQuery.query('#ProcessingWindowUnmixing')[0].show();
            }
        },
        {
            html: '<b>Evg Forest Normalization</b>',
            xtype: 'button',
            tooltip: 'Evergreen Forest Normalization',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Fnormalization',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowFnormalization').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Fnormalization');
                }
                Ext.ComponentQuery.query('#ProcessingWindowFnormalization')[0].show();
            }
        },
        {
            html: '<b> - ND(V,W,S)I Threshold - </b>',
            xtype: 'button',
            tooltip : 'ND(V,W,S)I Threshold Classification',
            cls : 'ToolWindow_Icon ToolWindow_Icon_NDVI',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowNDVIthreshold').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.NDVIthreshold');
                }
                Ext.ComponentQuery.query('#ProcessingWindowNDVIthreshold')[0].show();
            }
        },
        {
            html: '<b>Disturbance detection </br>using &Delta;NBR</b>',
            xtype: 'button',
            tooltip: 'NBR workflow',
            cls: 'ToolWindow_Icon ToolWindow_Icon_NBR',
            //style: 'margin-left: 25px; margin-top: 20px;',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowDegradationNBR').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.DegradationNBR');
                }
                Ext.ComponentQuery.query('#ProcessingWindowDegradationNBR')[0].show();
            }
        },
        {
            html: '<b>Pansharpen</b>',
            xtype: 'button',
            tooltip: 'Pansharpen',
            //style: "margin-left: 80px;",
            cls: 'ToolWindow_Icon ToolWindow_Icon_Pansharpen',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowPansharpen').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Pansharpen');
                }
                Ext.ComponentQuery.query('#ProcessingWindowPansharpen')[0].show();
            }
        },

        {
            html: '<b>Principal Component </b>',
            xtype: 'button',
            tooltip: 'Principal Component Analysis',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Pca',
            handler: function () {
                this.up('#AnalysisEnhancementTools').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowPca').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.Pca');
                }
                Ext.ComponentQuery.query('#ProcessingWindowPca')[0].show();
            }
        },



    ]


});
