/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.Degradation', {
    extend: 'Ext.window.Window',

    itemId: 'ProcessingWindowDegradation',
    title: 'Degradation and deforestation detection and reporting',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width:400,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            xtype: 'ProcessingInfo',
            html: '<div> PIXEL &emsp;&emsp;&emsp; vs    &emsp;&emsp;&emsp; GRID (MMU) </div></br></br>ForestER &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; CarbEF',
            width: 350,
            style:'margin-left:90px; margin-top:5px;font-weight: bold;',

        },
        {
            html: '</br><b> ForestER </br>Pixel-Based Forest </br>Emission Reporting</b>',
            xtype: 'button',
            tooltip: 'Reporting on forest degradation and deforestation and related emissions',
            cls: 'ToolWindow_Icon ToolWindow_Icon_ForestER',
            style: 'margin-left: 25px; margin-top: 10px;',
            handler: function () {
                this.up('#ProcessingWindowDegradation').close();
                if( Ext.ComponentQuery.query('#ForestER').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.ForestER');
                }
                Ext.ComponentQuery.query('#ProcessingWindowForestER')[0].show();
            }
        },
        {
            html: '<div style="background-color:#f5f5f5; width=100%; opacity:0.75"><b>CarbEF<br/>Reporting on<br/>Forest Carbon<br/>Emissions</b></div>',
            xtype: 'button',
            tooltip: 'Reporting on forest degradation and deforestation and related emissions',
            cls: 'ToolWindow_Icon ToolWindow_Icon_CARBEF',
            style: 'margin-left: 30px; margin-top: 10px;',
            handler: function () {
                this.up('#ProcessingWindowDegradation').close();
                if( Ext.ComponentQuery.query('#ProcessingWindowDegradationCARBEF').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.DegradationCARBEF');
                }
                Ext.ComponentQuery.query('#ProcessingWindowDegradationCARBEF')[0].show();
            }
        },
        {
            xtype: 'ProcessingInfo',
            html: 'Asses per pixel deforestation \nand forest degradation and\nrelated emissions from two\nforest maps.',
            width: 180,
            style: 'margin-left: 15px; margin-top: 5px; ',
         },
         {
            xtype: 'ProcessingInfo',
            html: 'Assess forest units degradation,\ndeforestation and their related\nemissions, from pixel level\nanalysis, for two periods of time.',
            width: 180
         },
         {
            width: 180,
            xtype: 'ProcessingInfo',
            html: '<a target="_blank" href="https://drive.google.com/drive/folders/1dGneHYbKEzTFP1HzMr5YE32pPkLoOmW_?usp=drive_link">INFO</a>',
            style: 'margin-left: 160; margin-top: 5px; font-weight: bold;',
         }

    ]


});
