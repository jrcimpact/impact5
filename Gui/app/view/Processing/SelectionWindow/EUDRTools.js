/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.SelectionWindow.EUDRTools', {
    extend: 'Ext.window.Window',

    itemId: 'EUDRTools',
    title: 'EUDR Tools',

    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'column',
    width:304,
    closeAction: 'destroy',
    autoScroll: false,
    defaultType: 'container',
    buttons: null,
    cls:  'ProcessingWindowExtraTools',

    items: [
        {
            html: '<b>Intersection</b>',
            xtype: 'button',
            tooltip: 'Intersects user geometry with GFC Map 2020',
            cls : 'ToolWindow_Icon ToolWindow_Icon_Eudr_Intersection',
            handler: function () {
                this.up('#EUDRTools').close();
                if( Ext.ComponentQuery.query('#IntersectionWindowEUDR').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.EUDR_intersect');
                }
                Ext.ComponentQuery.query('#IntersectionWindowEUDR')[0].show();
            }
        },
        {
            html: '<b>Info & Manuals</b>',
            xtype: 'button',
            tooltip: 'Info & Manuals',
            cls: 'ToolWindow_Icon ToolWindow_Icon_Eudr_Info',
            handler: function () {
                this.up('#EUDRTools').close();
                if( Ext.ComponentQuery.query('#InfoWindowEUDR').length == 0 ){
                    Ext.create('IMPACT.view.Processing.Window.EUDR_info');
                }
                Ext.ComponentQuery.query('#InfoWindowEUDR')[0].show();
            }
        }


    ]


});
