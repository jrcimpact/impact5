/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.DegradationNBR', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowDegradationNBR',
    title: "Degradation NBR",
    backgroundColor: "#e6e6fa",

    toolID: 'degradationNBR',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            title: 'Inputs',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'master_img',
                    filters: ['class', 'raster'],
                    fieldLabel: 'Input Image (Time 1)',
                    labelWidth: 140
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'master_mask',
                    filters: ['class', 'raster'],
                    fieldLabel: 'Input Mask (Time 1)',
                    allowBlank: true,
                    labelWidth: 140
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'slave_img',
                    filters: ['class', 'raster'],
                    fieldLabel: 'Input Image (Time 2)',
                    labelWidth: 140
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'slave_mask',
                    filters: ['class', 'raster'],
                    fieldLabel: 'Input Mask (Time 2)',
                    allowBlank: true,
                    labelWidth: 140
                })
            ]
        });
        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            title: 'Processing options',
            items: [
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_name',
                    fieldLabel: 'NBR Output Name',
                    allowBlank: false,
                    labelWidth: 120,
                    width: 350,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                Ext.create('IMPACT.view.component.Field.Combo', {
                    fieldLabel: 'NIR band',
                    itemId: 'nir',
                    store: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                    value: 4,
                    labelWidth: 80,
                    width: 140
                }),
                Ext.create('IMPACT.view.component.Field.Combo', {
                    fieldLabel: 'SWIR band',
                    itemId: 'swir',
                    store: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                    value: 6,
                    labelWidth: 80,
                    width: 140
                }),
                Ext.create('IMPACT.view.component.Field.Integer', {
                    itemId: 'kernel_size',
                    fieldLabel: 'Self-referencing Kernel Radius in pixel',
                    labelWidth: 210,
                    width: 260,
                    value: 7,
                    allowBlank: false,
                    validator: function(value){
                        if(value==="0"){
                            me.down('#kernel_size_warning').show();
                        } else {
                            me.down('#kernel_size_warning').hide();
                        }
                        return true;
                    }
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: ' e.g. Landsat: 210m ->7px, S2A: 210m->21px; if set to 0 filter is not applied.'
                },
                {
                    xtype: 'ProcessingInfo',
                    type: 'warning',
                    itemId: 'kernel_size_warning',
                    hidden: true,
                    html: '<b>Warning</b>: please be aware that when using a kernel size of 0 pixels no self-referencing is undertaken. As a consequence it has to be noted that when using the statistics tool on such result, the applied threshold values might lead to incorrect degradation levels.'
                },

                // -----------------------  Reprojection on the fly ------------------------------
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Force reprojection </b>',
                    radioName: 'reproject',
                    defaultValue: 'No',
                    listeners: {
                        change: function(radio, value) {
                            if(value.reproject_val === "Yes"){
                                me.down('#reproject_warning').show();
                            } else {
                                me.down('#reproject_warning').hide();
                            }
                        }
                    }
                }),
                {
                    xtype: 'ProcessingInfo',
                    type: 'warning',
                    itemId: 'reproject_warning',
                    hidden: true,
                    html: '<b>Warning</b>: please be aware that using on the fly reprojection and warping might lead to incorrect degradation detection.\n Time1 image projection, size and extent is used for computation.\n Forest masks are merged (or)'
                },


                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'dnbr_overwrite',
                    defaultValue: 'No'
                })
            ]
        });

        return items;
    },

    launchProcessing: function(){
        var me = this;
        return {
            toolID:         me.toolID,
            master_img:     me.parseInputValues('master_img'),
            master_mask:    me.parseInputValues('master_mask'),
            slave_img:      me.parseInputValues('slave_img'),
            slave_mask:     me.parseInputValues('slave_mask'),
            outname:        me.parseInputValues('output_name'),
            nir:            me.parseInputValues('nir'),
            swir:           me.parseInputValues('swir'),
            kernel_size:    me.parseInputValues('kernel_size'),
            reproject:      me.parseInputValues('reproject'),
            overwrite:      me.parseInputValues('dnbr_overwrite'),

        };
    }

});