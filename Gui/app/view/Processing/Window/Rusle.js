/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Rusle', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowRusle',
    title: "Soil erosion using RUSLE model",
    backgroundColor: "#ffffe5",
    width: 900,
    //height: 800,
    toolID: 'Rusle',
    scroll: true,

    rainStartDate:'202001',
    rainEndDate:'202012',
    processingResolution:'max',


    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            //scroll: true,
            height: 380,
            title: 'Inputs',
            items: [

                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleAOI',
                    filters: ['vector'],
                    selectionMode: 'single',
                    fieldLabel: 'AOI',
                }),

                {
                    xtype: 'panel',
                    border: false,
                    backgroundColor: me.backgroundColor,
                    layout: {
                        type: 'hbox',
                    },
                    items: [
                        Ext.create('IMPACT.view.component.Field.File', {
                            itemId: 'rusleDEM',
                            filters: ['raster'],
                            selectionMode: 'single',
                            fieldLabel: 'DEM',
                        }),
                        {
                            xtype: 'checkboxfield',
                            itemId: 'onlineDEM',
                            width: 500,
                            labelWidth: 450,
                            boxLabel: 'Download from JRC - SRTM 30m',
                            name: 'JRC',
                            inputValue: 'JRC',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===false){
                                        me.down('#rusleDEM').enable();
                                    } else {
                                        me.down('#rusleDEM').disable();
                                    }
                                }
                            }
                        }
                    ]
                },
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Median filter (3x3)',
                    radioName: 'rusleMedian',
                    margin: '4 0 0 100',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Dem filling',
                    radioName: 'rusleFill',
                    margin: '4 0 0 100',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280
                }),


                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleSand',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Soil Sand %',
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleSilt',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Soil Silt %',
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleClay',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Soil Clay %',
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleCarbon',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Soil Organic Carbon %',
                }),
                {
                    xtype: 'checkboxfield',
                    itemId: 'onlineSoil',
                    width: 500,
                    labelWidth: 450,
                    boxLabel: 'Download all from JRC - Source: https://files.isric.org/public/afsis250m/',
                    name: 'JRC',
                    inputValue: 'JRC',
                    margin: '-80 0 0 430',
                    listeners: {
                        buffer: 50,
                        change: function(checkbox, newValue) {
                            if(newValue===false){
                                me.down('#rusleSand').enable();
                                me.down('#rusleSilt').enable();
                                me.down('#rusleClay').enable();
                                me.down('#rusleCarbon').enable();
                            } else {
                                me.down('#rusleSand').disable();
                                me.down('#rusleSilt').disable();
                                me.down('#rusleSilt').disable();
                                me.down('#rusleClay').disable();
                                me.down('#rusleCarbon').disable();

                            }
                        }
                    }
                },

                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleRain',
                    filters: ['raster'],
                    selectionMode: 'single',
                    fieldLabel: 'R Factor layer',
                    margin: '60 0 0 0',
                }),
                {
                    xtype: 'panel',
                    border: false,
                    backgroundColor: me.backgroundColor,
                    margin: '5 0 0 100',
                    layout: {
                        type: 'hbox',
                    },
                    items: [
                            {
                                xtype: 'datefield',
                                //label: 'Start date',
                                labelAlign:'left',
                                itemId: 'rusleRain_startDate',
                                name: 'rusleRain_startDate',
                                format: 'm/Y',
                                fieldLabel: 'Start',
                                labelWidth : 35,
                                width: 140,
                                disabled: true,
                                minValue: new Date('1981-01'),
                                //height: 20,
                                //border: false,
                                value: new Date('2020-01'),
                                listeners : {
                                            'change' : function(field, newValue, oldValue) {
                                                 this.up('#ProcessingWindowRusle').rainStartDate = Ext.Date.format(newValue, "Ym")
                                            }
                                        }
                            },
                            {
                                xtype: 'datefield',
                                //label: 'End',
                                labelAlign:'left',
                                itemId: 'rusleRain_endDate',
                                name: 'rusleRain_endDate',
                                format: 'm/Y',
                                fieldLabel: 'End',
                                labelWidth : 35,
                                width: 140,
                                margin: '0 0 0 10',
                                disabled: true,
                                //height: 20,
                                //border: false,
                                value: new Date('2020-12'),
                                maxValue: new Date('2024-01'),
                                listeners : {
                                            'change' : function(field, newValue, oldValue) {
                                                 this.up('#ProcessingWindowRusle').rainEndDate = Ext.Date.format(newValue, "Ym");
                                            }
                                        }
                            },
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                fieldLabel: 'R Formula',
                                labelAlign: 'left',
                                labelWidth: 65,
                                margin: '0 0 0 60',
                                width: 280,
                                store: ['Arnoldus (1977)','Renard and Freimund (1994)','Wischmeier and Smith (1978)','Ferro et al. (1999)'],
                                itemId: 'RfactStrategy',
                                value: 'Arnoldus (1977)',
                                disabled: true

                            }),
                             {
                                    xtype: 'button',
                                    text: 'R Factor info',
                                    iconCls: 'fa fa-lg fa-blue fa-info-circle',
                                    iconAlign: 'left',
                                    style: 'color: red;',
                                    cls: 'impact-icon-button',
                                    disabled: false,

                                    listeners: {
                                        click: function () {
                                            Ext.Msg.show({
                                                title: 'R Factor info ',
                                                msg: '<b>Arnoldus (1977)</b>:<br/>'+
                                                      'R = 17.02 * (10 ** (1.5 * numpy.log10(MFI) - 0.8188))<br/>'+
                                                      ' with MFI = ∑𝑖 𝑃𝑖^2/𝑃 <br/>'+
                                                      ' with P𝑖 = rain of month i (1-12)<br/>'+
                                                      ' with P = annual cumulative rainfall<br/>' +
                                                      '<br/>'+
                                                       'Reference: Arnoldus, H.M.J. (1977) Methodology Used to Determine the Maximum Potential Range Average Annual Soil Loss to Sheet and Rill Erosion in Morocco. Assessing Soil Degradation, FAO Soils Bulletin (FAO), 34, 39-48. <br/>'+
                                                      'Usage : <br/>'+

                                                      '<br/>'+

                                                      '<b>Renard and Freimund (1994)</b>:<br/>'+
                                                      'R = (0.0483 * P^1.61) where P<=850 <br/>'+
                                                      'R = (587.8 - 1.219*P^2) where P >850 <br/>'+
                                                      ' with P = annual cumulative rainfall <br/>'+
                                                      '<br/>'+
                                                      'Reference: Renard, K.G., Freimund, J.R., 1994. Using monthly precipitation data to estimate the R-factor in the revised USLE. J. Hydrol. (Amst) 157, 287–306.<br/>'+
                                                      'Usage : <br/>'+
                                                      'Watene, G., Yu, L., Nie, Y., Zhang, Z., Hategekimana, Y., Mutua, F., ... & Ayugi, B. (2021). Spatial-temporal variability of future rainfall erosivity and its impact on soil loss risk in Kenya. Applied Sciences, 11(21), 9903.<br/>'+
                                                      '<br/>'+

                                                      '<b>Wischmeier and Smith (1978)</b>:<br/>'+
                                                      'R = ∑𝑖 1.735*10(1.5*log10(𝑃𝑖^2/𝑃)−0.08188) <br/>'+
                                                      ' with P𝑖 = rain of month i (1-12)<br/>'+
                                                      ' with P = annual cumulative rainfall <br/>'+
                                                      '<br/>'+
                                                      'Wischmeier, W.H.; Smith, D.D. Predicting Rainfall Erosion Losses—A Guide to Conservation Planning; US Department of Agriculture Science and Education Administration: Washington, DC, USA, 1978.<br/>'+
                                                      'Usage : <br/>'+
                                                      '- Hategekimana, Y.; Allam, M.; Meng, Q.; Nie, Y.; Mohamed, E. Quantification of soil losses along the coastal protected areas in Kenya. Land 2020, 9, 137<br/>'+
                                                      '- Watene, G., Yu, L., Nie, Y., Zhang, Z., Hategekimana, Y., Mutua, F., ... & Ayugi, B. (2021). Spatial-temporal variability of future rainfall erosivity and its impact on soil loss risk in Kenya. Applied Sciences, 11(21), 9903.<br/>'+

                                                      '<br/>'+
                                                      '<b>Ferro et al. (1999)</b>:<br/>'+
                                                      'R =  0.6120 * MFI^1.56 <br/>'+
                                                      ' with MFI = ∑𝑖 𝑃𝑖^2/𝑃 <br/>'+
                                                      ' with P𝑖 = rain of month i (1-12)<br/>'+
                                                      ' with P = annual cumulative rainfall<br/>' +
                                                      '<br/>'+
                                                      'Mediterranean regions <br/>'+
                                                      'Reference: Ferro, V., Porto, P., & Yu, B. (1999). A comparative study of rainfall erosivity estimation for southern Italy and southeastern Australia. <br/>'+
                                                      'Hydrological sciences journal, 44(1), 3-24.<br/>'+
                                                      'Usage: <br/>'+
                                                      ' - Aiello, A., Adamo, M., & Canora, F. (2015). Remote sensing and GIS to assess soil erosion with RUSLE3D and USPED at river basin scale in southern Italy. Catena, 131, 174-185.<br/>'+
                                                      ' - Samela, C., Imbrenda, V., Coluzzi, R., Pace, L., Simoniello, T., & Lanfredi, M. (2022). Multi-decadal assessment of soil loss in a Mediterranean region characterized by contrasting local climates. Land, 11(7), 1010.<br/>'+
                                                      '<br/>',

                                                buttons: Ext.Msg.OK
                                            });
                                            }
                                    }
                             }

                    ]
                },
                {
                    xtype: 'checkboxfield',
                    itemId: 'onlineRain',
                    width: 500,
                    labelWidth: 450,
                    boxLabel: 'Download from JRC - from monthly CHIRPS v2.0 data',
                    name: 'JRC',
                    inputValue: 'JRC',
                    margin: '-70 0 0 430',
                    listeners: {
                        buffer: 50,
                        change: function(checkbox, newValue) {
                            if(newValue===false){
                                me.down('#rusleRain').enable();
                                me.down('#rusleRain_startDate').disable();
                                me.down('#rusleRain_endDate').disable();
                                me.down('#RfactStrategy').disable();
                            } else {
                                me.down('#rusleRain').disable();
                                me.down('#rusleRain_startDate').enable();
                                me.down('#rusleRain_endDate').enable();
                                me.down('#RfactStrategy').enable();
                            }
                        }
                    }
                },


                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'rusleLC',
                    filters: ['raster', 'class', 'vector'],
                    selectionMode: 'single',
                    fieldLabel: 'Land cover map',
                    margin: '50 0 0 0',
                }),
                {
                    xtype: 'panel',
                    border: false,
                    backgroundColor: me.backgroundColor,
                    margin: '5 0 0 100',
                    layout: {
                        type: 'hbox',
                    },
                    items: [
                                     // ##### Output type #####
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                itemId: 'onlineLCType',
                                fieldLabel: 'Map',
                                //labelStyle: 'font-weight: bold;',
                                labelWidth: 40,
                                width: 140,
                                disabled: true,
                                store: {
                                    xtype: 'store',
                                    fields: ['value', 'name'],
                                    data : [
                                        {"name": "ESA CCI", "value": "esa-cci"},
                                        //{"value": "Copernicus", "name": "copernicus"}
                                    ]
                                },
                                displayField: 'name',
                                valueField: 'value',
                                value: "esa-cci"
                            }),
                                             // ##### Output type #####
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                itemId: 'onlineLCYY',
                                fieldLabel: 'Year',
                                margin: '0 0 0 10',
                                //labelStyle: 'font-weight: bold;',
                                labelWidth: 40,
                                width: 140,
                                disabled: true,
                                //queryMode: 'local',
                                //mode: 'local',
                                store: ['1992','1993','1994','1995','1996','1997','1998','1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020'],
                                displayField: 'value',
                                value: "2020"
                            })
                    ]
                },
                {
                    xtype: 'checkboxfield',
                    itemId: 'onlineLC',
                    width: 500,
                    labelWidth: 450,
                    boxLabel: 'Download from JRC - Source: ESA CCI ',
                    name: 'JRC',
                    inputValue: 'JRC',
                    margin: '-60 0 0 430',
                    listeners: {
                        buffer: 50,
                        change: function(checkbox, newValue) {
                            if(newValue===false){
                                me.down('#rusleLC').enable();
                                me.down('#onlineLCType').disable();
                                me.down('#onlineLCYY').disable();

                                me.down('#rusleForestID').enable();
                                me.down('#rusleShrubID').enable();
                                me.down('#rusleGrassID').enable();
                                me.down('#rusleAgriID').enable();
                                me.down('#rusleAquaticID').enable();
                                me.down('#rusleBareID').enable();
                                me.down('#rusleSparseID').enable();
                                me.down('#rusleUrbanID').enable();

                            } else {
                                me.down('#rusleLC').disable();
                                me.down('#onlineLCType').enable();
                                me.down('#onlineLCYY').enable();

                                me.down('#rusleForestID').disable();
                                me.down('#rusleShrubID').disable();
                                me.down('#rusleGrassID').disable();
                                me.down('#rusleAgriID').disable();
                                me.down('#rusleAquaticID').disable();
                                me.down('#rusleBareID').disable();
                                me.down('#rusleSparseID').disable();
                                me.down('#rusleUrbanID').disable();

                            }
                        }
                    }
                },


            ] // end of processing panel
        });

       items.push(
                // MAIN FRAME for  Class and C M  Factor table   +   Notes
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    margin: '1 0 0 0',
                    title:'Parameters ',
                    width:900,
                    collapsible: true,
                    items: [
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                //margin: '30 0 0 0',
                                title:' Land Cover  - Slope - M C P factors',
                                width:500,
                                items: [
                                       //     Land COVER ID
                                       {
                                        xtype: 'container',
                                        layout: 'vbox',
                                        //margin: '20 0 0 0',

                                        items: [
                                               {
                                                    html:" <b>Class ID</b>",
                                                    margin: '2 0 0 120',
                                               },
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleForestID',
                                                    fieldLabel: 'Forest',
                                                    //hideLabel: true,
                                                    width: 165,
                                                    value: '10,95',
                                                    margin: '1 -5 0 0',
                                                    inputAttrTpl: " data-qtip=' List class ID of each Forest class (e.g. 10,11,13)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleShrubID',
                                                    fieldLabel: 'Shrub',
                                                    //hideLabel: true,
                                                    width: 165,
                                                    value: '20',
                                                    margin: '-5 0 0 0',
                                                    //disabled: true,
                                                    inputAttrTpl: " data-qtip=' List class ID of each Shrub class (e.g. 20,22,23)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleGrassID',
                                                    fieldLabel: 'Grass',
                                                    width: 165,
                                                    value: '30',
                                                    margin: '-5 0 0 0',
                                                    inputAttrTpl: " data-qtip=' List class ID of each Grass class (e.g. 30,32,33)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleSparseID',
                                                    fieldLabel: 'Sparse Veg.',
                                                    //hideLabel: true,
                                                    width: 165,
                                                    value: '',
//                                                    allowBlank: true,
                                                    margin: '-5 0 0 0',
                                                    //disabled: true,
                                                    inputAttrTpl: " data-qtip=' List class ID of each Sparse Vegetation class (e.g. 70)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleBareID',
                                                    fieldLabel: 'Bare',
                                                    //hideLabel: true,
                                                    width: 165,
                                                    value: '60',
                                                    margin: '-5 0 0 0',
                                                    //disabled: true,
                                                    inputAttrTpl: " data-qtip=' List class ID of each Bare class (e.g. 60)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleAgriID',
                                                    fieldLabel: 'Agriculture',
                                                    width: 165,
                                                    value: '40',
                                                    margin: '-5 0 0 0',
                                                    inputAttrTpl: " data-qtip=' List class ID of each Agriculture class (e.g. 40)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),

                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleUrbanID',
                                                    fieldLabel: 'Urban',
                                                    width: 165,
                                                    value: '50',
                                                    margin: '-5 0 0 0',
                                                    inputAttrTpl: " data-qtip=' List class ID of each Urban class (e.g. 60)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                Ext.create('IMPACT.view.component.Field.Text',{
                                                    itemId: 'rusleAquaticID',
                                                    fieldLabel: 'Aquatic Veg.',
                                                    width: 165,
                                                    value: '90',
                                                    margin: '-5 0 0 0',
                                                    inputAttrTpl: " data-qtip=' List class ID of each Aquatic Vegetation class (e.g. 90)' ",
                                                    validator: function(value){
                                                        if (value == ''){ return true};
                                                        if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                          return true;
                                                        }
                                                        return 'This field format is not valid';
                                                    }
                                                }),
                                                {
                                                    xtype: 'button',
                                                    text: 'Defautl Worldcover classes',
                                                    iconCls: 'fa fa-lg fa-blue fa-info-circle',
                                                    iconAlign: 'left',
                                                    //style: 'color: red;',
                                                    cls: 'impact-icon-button_yellow',
                                                    disabled: false,
                                                    listeners: {
                                                        click: function () {
                                                            Ext.Msg.show({
                                                                title: '"WorldCover',
                                                                msg:    'Forest_class = [10, 95]<br/>'+
                                                                        'Shrub_class = [20]<br/>'+
                                                                        'Grass_class = [30]<br/>'+
                                                                        'Agri_class = [40]<br/>'+
                                                                        'Bare_class = [60]<br/>'+
                                                                        'Sparse_class = []<br/>'+
                                                                        'Urban_class = [50]<br/>'+
                                                                        'Aquatic_class = [90]<br/>',
                                                                buttons: Ext.Msg.OK
                                                            });
                                                            }
                                                        }
                                               },
                                               {
                                                    xtype: 'button',
                                                    text: 'ESA CCI classes',
                                                    iconCls: 'fa fa-lg fa-blue fa-info-circle',
                                                    iconAlign: 'left',
                                                    //style: 'color: red;',
                                                    cls: 'impact-icon-button_yellow',
                                                    disabled: false,

                                                    listeners: {
                                                        click: function () {
                                                            Ext.Msg.show({
                                                                title: 'ESA CCI',
                                                                msg: 'Forest_class = [50,60,61,62,70,71,72,80,81,82,90]<br/>'+
                                                                      'Shrub_class = [40,100,110,120,121,122]<br/>'+
                                                                      'Grass_class = [130]<br/>'+
                                                                      'Sparse_class = [140,150,151,152,153]<br/>'+
                                                                      'Bare_class = [200,201,202]<br/>'+
                                                                      'Agri_class = [10,11,12,20,30]<br/>'+
                                                                      'Urban_class = [190]<br/>'+
                                                                      'Aquatic_class = [160,170,180]<br/>'+

                                                                      ' ---------- <br/> '+

                                                                      '[10] = "Cropland, rainfed"<br/>'+
                                                                      '[11] = "Cropland, rainfed, herbaceous cover"<br/>'+
                                                                      '[12] = "Cropland, rainfed, tree or shrub cover"<br/>'+
                                                                      '[20] = "Cropland, irrigated or postflooding"<br/>'+
                                                                      '[30] = "Mosaic cropland (>50%) / natural vegetation (tree, shrub,herbaceous cover) (<50%)"<br/>'+
                                                                      '[40] = "Mosaic natural vegetation (tree, shrub, herbaceous cover) (>50%) / cropland (<50%)"<br/>'+
                                                                      '[50] = "Tree cover, broadleaved, evergreen, closed to open (>15%)"<br/>'+
                                                                      '[60] = "Tree cover, broadleaved, deciduous, closed to open (>15%)"<br/>'+
                                                                      '[61] = "Tree cover, broadleaved, deciduous, closed (>40%)"<br/>'+
                                                                      '[62] = "Tree cover, broadleaved, deciduous, open (15-40%)"<br/>'+
                                                                      '[70] = "Tree cover, needleleaved, evergreen, closed to open (>15%)"<br/>'+
                                                                      '[71] = "Tree cover, needleleaved, evergreen, closed (>40%)"<br/>'+
                                                                      '[72] = "Tree cover, needleleaved, evergreen, open (15-40%)"<br/>'+
                                                                      '[80] = "Tree cover, needleleaved, deciduous, closed to open (>15%)"<br/>'+
                                                                      '[81] = "Tree cover, needleleaved, deciduous, closed (>40%)"<br/>'+
                                                                      '[82] = "Tree cover, needleleaved, deciduous, open (15-40%)"<br/>'+
                                                                      '[90] = "Tree cover, mixed leaf type (broadleaved and needleleaved)"<br/>'+
                                                                      '[100] = "Mosaic tree and shrub (>50%) / herbaceous cover (<50%)"<br/>'+
                                                                      '[110] = "Mosaic herbaceous cover (>50%) / tree and shrub (<50%)"<br/>'+
                                                                      '[120] = "Shrubland"<br/>'+
                                                                      '[121] = "Evergreen shrubland"<br/>'+
                                                                      '[122] = "Deciduous shrubland"<br/>'+
                                                                      '[130] = "Grassland"<br/>'+
                                                                      '[140] = "Lichens and mosses"<br/>'+
                                                                      '[150] = "Sparse vegetation (tree, shrub, herbaceous cover) (<15%)"<br/>'+
                                                                      '[151] = "Sparse tree (<15%)"<br/>'+
                                                                      '[152] = "Sparse shrub (<15%)"<br/>'+
                                                                      '[153] = "Sparse herbaceous cover (<15%)"<br/>'+
                                                                      '[160] = "Tree cover, flooded, fresh or brakish water"<br/>'+
                                                                      '[170] = "Tree cover, flooded, saline water"<br/>'+
                                                                      '[180] = "Shrub or herbaceous cover, flooded, fresh/saline/brakish water"<br/>'+
                                                                      '[190] = "Urban areas"<br/>'+
                                                                      '[200] = "Bare areas"<br/>'+
                                                                      '[201] = "Consolidated bare areas"<br/>'+
                                                                      '[202] = "Unconsolidated bare areas"<br/>'+
                                                                      '[210] = "Water bodies"<br/>'+
                                                                      '[220] = "Permanent snow and ice"<br/>',
                                                                buttons: Ext.Msg.OK
                                                            });
                                                            }
                                                        }
                                               }
                                            ]
                                       },
                                       //      C Factor
                                       {
                                            xtype: 'container',
                                            layout: 'vbox',
                                            margin: '0 0 0 10',
                                            items: [
                                                   {
                                                        html:" <b>C Factor </b>",
                                                        margin: '2 0 0 10',
                                                   },
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleForestCfact',
                                                        width: 60,
                                                        value: 0.01,
                                                        margin: '1 -5 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Forest' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<=1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleShrubCfact',
                                                        width: 60,
                                                        value: 0.08,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Shrub' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleGrassCfact',
                                                        width: 60,
                                                        value: 0.05,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Grass' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleSparseCfact',
                                                        width: 60,
                                                        value: 0.03,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Sparse Vegetation' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }) ,
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleBareCfact',
                                                        width: 60,
                                                        value: 0.5,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Bareland' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleAgriCfact',
                                                        width: 60,
                                                        value: 0.15,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Agriculture' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }) ,


                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleUrbanCfact',
                                                        width: 60,
                                                        value: 0.1,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Urban' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleAquaticCfact',
                                                        width: 60,
                                                        value: 0.03,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set C Factor for Aquatic Vegetation' ",
                                                        hideLabel: true,
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    })
                                            ]
                                       },
                                       //      P Factor
                                       {
                                            xtype: 'container',
                                            layout: 'vbox',
                                            margin: '0 0 0 30',
                                            width: 100,
                                            items: [
                                                   {
                                                        html:" <b>Slope % - P </br>for Agriculture</b>",
                                                        margin: '2 0 0 5',
                                                   },
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact1',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.6,
                                                        margin: '1 -5 0 0',
                                                        fieldLabel: '[0-2]',
                                                        inputAttrTpl: " data-qtip=' Set P Factor' ",
                                                        validator: function(value){
                                                            if(value>=0 && value<=1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact2',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.5,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor' ",
                                                        fieldLabel:  ']2-8]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact3',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.6,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor' ",
                                                        fieldLabel:  ']8-12]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact4',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.7,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor ' ",
                                                        fieldLabel: ']12,16]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact5',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.8,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor ' ",
                                                        fieldLabel: ']16,20]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'ruslePfact6',
                                                        disabled: true,
                                                        width: 100,
                                                        labelWidth: 50,
                                                        value: 0.9,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl:" data-qtip=' Set M Factor ' ",
                                                        fieldLabel: ']20,25]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
//                                                   {
//                                                        html:'Others P = 1',
//                                                        itemId: 'ruslePfact7',
//                                                        disabled: true,
//                                                   },
                                                                           {
                                                    xtype: 'checkboxfield',
                                                    itemId: 'dafaultPval',
                                                    width: 500,
                                                    labelWidth: 450,
                                                    boxLabel: 'Default P = 1',
                                                    name: 'defP',
                                                    inputValue: '1',
                                                    checked:true,
                                                    listeners: {
                                                        buffer: 50,
                                                        change: function(checkbox, newValue) {
                                                            if(newValue===false){
                                                                me.down('#ruslePfact1').enable();
                                                                me.down('#ruslePfact2').enable();
                                                                me.down('#ruslePfact3').enable();
                                                                me.down('#ruslePfact4').enable();
                                                                me.down('#ruslePfact5').enable();
                                                                me.down('#ruslePfact6').enable();
//                                                                me.down('#ruslePfact7').enable();
                                                            } else {
                                                                me.down('#ruslePfact1').disable();
                                                                me.down('#ruslePfact2').disable();
                                                                me.down('#ruslePfact3').disable();
                                                                me.down('#ruslePfact4').disable();
                                                                me.down('#ruslePfact5').disable();
                                                                me.down('#ruslePfact6').disable();
//                                                                me.down('#ruslePfact7').disable();
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                       },
                                       //      M Factor
                                       {
                                            xtype: 'container',
                                            layout: 'vbox',
                                            margin: '35 0 0 20',
                                            width: 230,
                                            items: [
                                                   {
                                                        html:" <b>Slope% -  M Factor</b>",
                                                   },
                                                   Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleMfact1',
                                                        width: 80,
                                                        labelWidth: 50,
                                                        value: 0.2,
                                                        margin: '1 -5 0 0',
                                                        fieldLabel: '[0-1]',
                                                        inputAttrTpl: " data-qtip=' Set M Factor' ",
                                                        validator: function(value){
                                                            if(value>=0 && value<=1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleMfact2',
                                                        width: 80,
                                                        labelWidth: 50,
                                                        value: 0.3,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor' ",
                                                        fieldLabel:  ']1-3]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleMfact3',
                                                        width: 80,
                                                        labelWidth: 50,
                                                        value: 0.4,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor' ",
                                                        fieldLabel:  ']3-5]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    }),
                                                    Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                                        itemId: 'rusleMfact4',
                                                        width: 80,
                                                        labelWidth: 50,
                                                        value: 0.5,
                                                        margin: '-5 0 0 0',
                                                        inputAttrTpl: " data-qtip=' Set M Factor ' ",
                                                        fieldLabel: ']5,90]',
                                                        validator: function(value){
                                                            if(value>=0 && value<= 1){
                                                                return true;
                                                            }
                                                            return 'Must be greater the 0';
                                                        }
                                                    })
                                            ]
                                       },

                                    ]
                            },//    Notes on 2nd col on right
                            {
                            xtype: 'ProcessingInfo',
                            width:400,
                            margin:'13 0 0 10',
                            padding: 21,
                            html: '<b>NOTES</b>:<br />- <b>Beta Version: </b> under consolidation, use for demo only</br>'+
                                   '- <b>Output</b> directory is same as the input AOI</br>'+
                                   '- <b>Prefix</b> is use to construct the name of output files </br>'+
                                   '- Output <b>resolution</b> is the highest among selected files </br>'+
                                   '- Yearly <b>precipitation</b>: if more than 12 months and Renard and Freimund option, annual cumulative rain is rescaled to 1 year -> cum_rain / months * 12 </br> '+
                                   '- <b>Land cover classes</b>: internally adjusted when ESA-CCI is downloaded from JRC </br> ' +
                                   '- <a target="_blank" href="https://publications.jrc.ec.europa.eu/repository/handle/JRC139752">JRC Technical Report</a> '
                            }
                        ]
                    } //  end of 2 col block with params and notes

       );


        items.push(
                   //   processing options
                  {
                    xtype: 'panel',
                    layout: 'hbox',
                    border: false,
                    collapsible: true,
                    bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
                    title: 'Processing options',
                    items: [

                        Ext.create('IMPACT.view.component.EPSGCodesCombo', {
                                    itemId: 'rusle_reproject',
                                    fieldLabel: 'Output Projection',
                                    margin: '3 0 0 0',
                                    width: 380,
                                    value: 'ESRI:102022' ,
                                    labelWidth: 100,
                                    initFilter : "Equal",
                                    validator: function(value){
                                        if(value!==''){
                                            return true;
                                        }
                                        return 'Only Albers Equal Area projections allowed.';
                                    }
                        }),
                        Ext.create('IMPACT.view.component.Field.Combo',{
                                itemId: 'rusle_resolution',
                                fieldLabel: 'Processing Resolution',
                                //labelStyle: 'font-weight: bold;',
                                labelWidth: 130,
                                width: 240,
                                margin: '10 0 0 25',
                                disabled: false,
                                store: {
                                    xtype: 'store',
                                    fields: ['value', 'name'],
                                    data : [
                                        {"name": "Highest", "value": "max"},
                                        {"name": "100m (1ha)", "value": "1ha"},
                                        {"name": "user", "value": "user"},
                                        //{"value": "Copernicus", "name": "copernicus"}
                                    ]
                                },
                                displayField: 'name',
                                valueField: 'value',
                                value: "highest ",
                                listeners: {
                                    select: function (item) {
                                        console.log(item.value);
                                       if (item.value == 'user'){
                                            me.down('#rusle_user_resolution').enable();
                                       } else {
                                            me.processingResolution=item.value;
                                            me.down('#rusle_user_resolution').disable();
                                       }


                                    }
                                },
                            }),
                            Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                itemId: 'rusle_user_resolution',
                                fieldLabel: '<b>Meters</b>',
                                labelWidth: 50,
                                width: 130,
                                margin: '10 0 0 25',
                                allowBlank:false,
                                disabled: true,
                                listeners: {
                                    change: function (item) {
                                        me.processingResolution=item.value;
                                    }
                                },
                            }),

                        ]
                    }


        );
        items.push(
                   //   processing options
                  {
                    xtype: 'panel',
                    layout: 'hbox',
                    border: false,
                    collapsible: false,
                    bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
                    //title: 'Processing options',
                    items: [

                        Ext.create('IMPACT.view.component.Field.Text',{
                            itemId: 'rusle_output_name',
                            fieldLabel: '<b>Output prefix</b>',
                            labelWidth: 95,
                            width: 300,
                            margin: '5 0 0 0',
                            allowBlank:false,
                            regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                        }),
                        Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                            fieldLabel: '<b>Save all tmp files</b>',
                            radioName: 'rusle_tmp',
                            defaultValue: 'No',
                            labelWidth: 120,
                            width: 250,
                            margin: '8 0 0 15'
                        }),
                        Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                            fieldLabel: '<b>Overwrite</b> Output',
                            radioName: 'rusle_overwrite',
                            defaultValue: 'No',
                            labelWidth: 100,
                            width: 250,
                            margin: '8 0 0 15'
                        }),
                    ]
                },

        );

        return items;
    },


    __parseLC_Cfact: function(){
        var me = this;
        var LC_Ids_Cfact = [];
        LC_Ids_Cfact.push([me.parseInputValues('rusleForestID').split(','),me.parseInputValues('rusleForestCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleShrubID').split(','),me.parseInputValues('rusleShrubCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleGrassID').split(','),me.parseInputValues('rusleGrassCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleSparseID').split(','),me.parseInputValues('rusleSparseCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleBareID').split(','),me.parseInputValues('rusleBareCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleAgriID').split(','),me.parseInputValues('rusleAgriCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleUrbanID').split(','),me.parseInputValues('rusleUrbanCfact')]);
        LC_Ids_Cfact.push([me.parseInputValues('rusleAquaticID').split(','),me.parseInputValues('rusleAquaticCfact')]);

        return LC_Ids_Cfact;
    },
    __parseAgri_Pfact: function(){
        var me = this;
        var P_fact = [];

        if(me.down('#dafaultPval').getValue() == false){
            P_fact.push(me.parseInputValues('ruslePfact1'));
            P_fact.push(me.parseInputValues('ruslePfact2'));
            P_fact.push(me.parseInputValues('ruslePfact3'));
            P_fact.push(me.parseInputValues('ruslePfact4'));
            P_fact.push(me.parseInputValues('ruslePfact5'));
            P_fact.push(me.parseInputValues('ruslePfact6'));
        } else {
            P_fact = [1,1,1,1,1,1]
        }
        return P_fact;
    },
    __parse_Mfact: function(){
        var me = this;
        var M_fact = [];
        M_fact.push(me.parseInputValues('rusleMfact1'));
        M_fact.push(me.parseInputValues('rusleMfact2'));
        M_fact.push(me.parseInputValues('rusleMfact3'));
        M_fact.push(me.parseInputValues('rusleMfact4'));
        return M_fact;
    },
    launchProcessing: function(){

        var me = this;

        return {
            toolID:                 me.toolID,
            aoi:                    me.parseInputValues('rusleAOI'),
            dem:                    me.down('#onlineDEM').getValue() ? '' : me.parseInputValues('rusleDEM'),
            demFill:                me.parseInputValues('rusleFill'),
            demMedian:              me.parseInputValues('rusleMedian'),

            rusleSand:              me.down('#onlineSoil').getValue() ? '' : me.parseInputValues('rusleSand'),
            rusleSilt:              me.down('#onlineSoil').getValue() ? '' : me.parseInputValues('rusleSilt'),
            rusleClay:              me.down('#onlineSoil').getValue() ? '' : me.parseInputValues('rusleClay'),
            rusleCarbon:            me.down('#onlineSoil').getValue() ? '' : me.parseInputValues('rusleCarbon'),

            R_file:                 me.down('#onlineRain').getValue() ? '' : me.parseInputValues('rusleRain'),
            rainStart:              me.rainStartDate,
            rainEnd:                me.rainEndDate,
            Rstrategy:              me.parseInputValues('RfactStrategy'),

            LC :                    me.down('#onlineLC').getValue() ? '' : me.parseInputValues('rusleLC'),
            LC_YY:                  me.parseInputValues('onlineLCYY'),
            LC_type:                me.parseInputValues('onlineLCType'),

            LC_Ids_Cfact:           JSON.stringify(me.__parseLC_Cfact()),
            Mfact:                  JSON.stringify(me.__parse_Mfact()),
            Agri_Pfact:             JSON.stringify(me.__parseAgri_Pfact()),

            out_name:               me.parseInputValues('rusle_output_name'),
            overwrite:              me.parseInputValues('rusle_overwrite'),
            out_epsg:               me.parseInputValues('rusle_reproject'),
            save_tmp:               me.parseInputValues('rusle_tmp'),
            resolution:             me.processingResolution
        };
    }

});