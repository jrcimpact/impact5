/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Pansharpen', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowPansharpen',
    title: "Pansharpen",

    toolID: 'pansharpen',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
            title: 'Inputs',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'pan_names',
                    filters: ['raster', 'class'],
                    fieldLabel: 'Pancromatic'
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'pan_img_names',
                    filters: ['raster', 'class'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Multispectral Rasters'
                })
            ]
        });

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
            title: 'Processing options',
            items: [
                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_band',
                    emptyText : 'default = Use all bands',
                    fieldLabel: 'Output Bands',
                    inputAttrTpl: " data-qtip=' e.g. 3,2,1' ",
                    regex: new RegExp(/^(\d{1,2},)*\d{1,2}$/),
                    labelWidth: 150
                }),
                // ######  Resampling method  ######
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Resampling Method',
                    itemId: 'resampling_method',
                    store: ['nearest', 'bilinear', 'cubic','average'],
                    value: 'cubic'
                }),

                // ######  Output data type  ######
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Output DataType',
                    itemId: 'output_type',
                    store: ['Byte', 'UInt16', 'Float32'],
                    value: 'Byte'
                }),
                // ######  Exponential stretch  ######
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'exponent',
                    fieldLabel: 'Exponential stretch used while reducing bit-depth',
                    labelWidth: 280,
                    width: 340,
                    value: 1.5,
                    decimalPrecision: 2,
                    allowBlank: false,
                    validator: function(value){
                        if(value>0) return true;
                        return 'Must be greater the 0'
                    }
                }),
                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_name',
                    emptyText : 'without .tif',
                    fieldLabel: 'Output Name Suffix',
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/),
                    labelWidth: 150,
                    width: 400
                }),
                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'sharp_overwrite',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 150,
                    width: 270
                })
            ]
        });

        return items;
    },



    launchProcessing: function(){

        var me = this;

        return {
            toolID: me.toolID,
            pan_names:     me.parseInputValues('pan_names'),
            img_names:     me.parseInputValues('pan_img_names'),
            bands:         me.parseInputValues('output_band'),
            resampling:    me.parseInputValues('resampling_method'),
            outDataType:   me.parseInputValues('output_type'),
            suffix:        me.parseInputValues('output_name'),
            exponent:      me.parseInputValues('exponent'),
            overwrite_pan: me.parseInputValues('sharp_overwrite')
        };
    }

});