/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.NDVIthreshold', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowNDVIthreshold',
    title: "Index Threshold Options",
    backgroundColor: "#e5ffdd",

    toolID: 'ndvi_threshold',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input images',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'input_images',
                    filters: ['raster', 'class'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Input images'
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Band 1',
                    itemId: 'red',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 60,
                    width: 140,
                    store: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                    value: 1
                }),
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Band 2',
                    itemId: 'nir',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 60,
                    width: 140,
                    store: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                    value: 1
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: '- INDEX: (B1-B2)/(B1+B2);<br />' +
                          '- e.g. NDVI: b1=NIR b2=RED (Landsat TM b1=4 b2=3)<br />' +
                          '- Use the Raster Calculator tool for custom operation <br />' +
                          '- Pixel == 0 in either NIR or RED are set as No Data (255)'
                },
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Classify </b> image with predefined number of clusters?',
                    defaultValue: 'Yes',
                    radioName: 'saveindex',
                    labelWidth: 300,
                    width: 420,
                    listeners: {
                        change: function(radio, value) {
                            if(value.saveindex_val === "Yes"){
                                me.down('#num_class').setDisabled(false);
                            } else {
                                me.down('#num_class').setDisabled(true);
                            }
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Slider', {
                    fieldLabel: 'Num. of clusters',
                    itemId: 'num_class',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 120,
                    width: 420,
                    value: 10,
                    maxValue: 250,
                    minValue: 1
                }),
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_suffix',
                    fieldLabel: 'Output Suffix',
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/),
                    labelWidth: 120,
                    width: 330
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;',
                    radioName: 'ndvi_overwrite'
                })
            ]
        });

        return items;
    },


    launchProcessing: function(){

        var me = this;

        return {
            toolID:         me.toolID,
            red:            me.parseInputValues('red'),
            nir:            me.parseInputValues('nir'),
            num_class:      me.parseInputValues('num_class'),
            img_names:      me.parseInputValues('input_images'),
            overwrite:      me.parseInputValues('ndvi_overwrite'),
            save_index:     me.parseInputValues('saveindex'),
            name_index:     me.parseInputValues('out_suffix')
        };
    }

});