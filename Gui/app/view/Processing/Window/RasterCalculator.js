/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.RasterCalculator', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    requires: [
        'IMPACT.view.Processing.Window.component.RasterCalculatorInput'
    ],

    itemId: 'ProcessingWindowRasterCalculator',
    title: "Raster calculator",

    toolID: 'RasterCalculator',
    backgroundColor: "#e5fff4",

    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        // #####  Inputs rasters  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                title: 'Input rasters',
                scroll: true,
                bodyStyle: {
                    padding: '5px',
                    backgroundColor: '#ffffff'
                },
                items: [
                    {
                        xtype: 'panel',
                        itemId: 'inputContainer',
                        border: false,
                        items: [
                            {
                                xtype: 'RasterCalculatorInput',
                                itemId: 'RasterCalculatorInput_0'
                            },
                            {
                                xtype: 'RasterCalculatorInput',
                                itemId: 'RasterCalculatorInput_1'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Add new input',
                        iconCls: 'fas fa-lg fa-green fa-plus-circle',
                        cls: 'impact-icon-button',
                        handler: function() {
                            me.__addNewInputRecord();
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Clear inputs',
                        //style: 'margin-left: 5px;',
                        cls: 'impact-icon-button',
                        iconCls: 'fa fa-lg fa-red fa-times-circle',
                        handler: function() {
                           me.__clearInputRecords();
                        }
                    }
                ]
            }
        );

        // #####  Calc Expression  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                backgroundColor: me.backgroundColor,
                title: 'Expression',
                items: [
                    Ext.create('IMPACT.view.component.Field.TextArea',{
                        itemId: 'calc_expression',
                        fieldLabel: 'Raster calculation expression',
                        allowOnlyWhitespace: false,
                        width: 420,
                        regex: new RegExp(/^[^:\\%^&@#$!]+$/)
                    }),
                    {
                        xtype: 'ProcessingInfo',
                        html: '<b>NOTES</b>:<br />' +
                              '- select "All" band in case you are masking multiple bands <br />'+
                              '- internal computation is done converting input bands into Float32 data<br />'+
                              '- no need to type A.astype(float) <br /> - letter identifying raster must be uppercase <br />'+
                              '- forbidden characters: :\\%^&@#$!'
                    }
                ]
            }

        );
        items.push(
            {
                xtype: 'ProcessingPanel',
                backgroundColor: me.backgroundColor,
                title: 'Examples',
                collapsed: true,
                html: '- average of 2 layers:    (A+B)/2<br />'+
                      '- set values < 0.5 to 0 : A*(A>0.5)<br />'+
                      '- min value of 2 bands:   minimum(A,B)<br />'+
                      '- set range to 0:         A*logical_or(A<=100,A>=200)'
            }
        );
        // #####  Processing options  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                backgroundColor: me.backgroundColor,
                title: 'Processing Options',
                items: [
                                // -----------------------  Reprojection on the fly ------------------------------
                    Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                        fieldLabel: 'Force reprojection and resampling',
                        radioName: 'reproject',
                        defaultValue: 'No',
                        listeners: {
                            change: function(radio, value) {
                                if(value.reproject_val === "Yes"){
                                    me.down('#rc_reproject_warning').show();
                                } else {
                                    me.down('#rc_reproject_warning').hide();
                                }
                            }
                        }
                    }),
                    {
                        xtype: 'ProcessingInfo',
                        type: 'warning',
                        itemId: 'rc_reproject_warning',
                        hidden: true,
                        html: '<b>Warning</b>: please be aware that using on the fly reprojection and warping might lead to incorrect image alignment.\n Projection, size and extent used for computation are taken from the first image'
                    },
                    // ##  No data value  ##
                    Ext.create('IMPACT.view.component.Field.Float',{
                        itemId: 'no_data',
                        fieldLabel: 'Output "No data" value',
                        labelWidth: 138,
                        width: 200
                    }),
                    // ##  Output type  ##
                    Ext.create('IMPACT.view.component.Field.Combo',{
                        fieldLabel: 'Output data type',
                        itemId: 'output_type',
                        labelAlign: 'Left',
                        labelWidth: 138,
                        width: 250,
                        store: ['Byte', 'UInt16', 'Float32'],
                        value: '',
                        allowBlank: false
                    }),
                    // ##  Output File Name  ##
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        style: 'margin-bottom: 3px;',
                        items: [
                            Ext.create('IMPACT.view.component.Field.Text',{
                                itemId: 'output_name',
                                fieldLabel: 'Output File Name',
                                labelWidth: 138,
                                width: 290,
                                allowBlank: false,
                                regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                            }),
                            {
                                xtype: 'ProcessingInfo',
                                html: ' .tif',
                                style: 'margin: 5px 0 0 5px;'
                            }
                        ]
                    },
                    // ##  Overwrite  ##
                    Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                        fieldLabel: '<b>Overwrite</b> Output',
                        defaultValue: 'No',
                        radioName: 'calc_overwrite'
                    })
                ]
            }
        );

        return items;
    },

    /**
     *  Add new input file record (RasterCalculatorInput - raster+band)
     */
    __addNewInputRecord: function(){
        var container = this.down('#inputContainer');
        var index = 0;
        // Retrieve record index from itemId
        var last_item = container.items.items[container.items.length-1];
        if(typeof last_item !== "undefined"){
            index = last_item.itemId.substring(last_item.itemId.lastIndexOf("_")+1);
            index = parseInt(index)+1;
        }
        // Show/hide 'add' button
        if(index===10){
            container.next('.button').hide()
        } else {
            container.next('.button').show()
        }
        // Add new record
        container.add({
            xtype: 'RasterCalculatorInput',
            itemId: 'RasterCalculatorInput_'+index
        })
    },

    /**
     *  Clear raster calculator inputs
     */
    __clearInputRecords: function(){
        var me = this;
        var container = me.down('#inputContainer');
        Ext.Msg.show({
            title: 'Clear all inputs',
            msg: 'Are you sure you want to clear all the inputs?',
            buttons: Ext.Msg.YESNO,
            fn: function(e){
                if(e==='yes'){
                    // destroy all existing record
                    container.items.each(function(item){
                        item.destroy();
                    });
                    // Add 2 new empty records
                    me.__addNewInputRecord();
                    me.__addNewInputRecord();
                }
            }
        });
    },

    /**
     *  Retrieve path/band pairs from user selection
     */
    __parseInputFiles: function(){
        var paths = [];
        var inputFiles = this.query('#raster_calculator_input');
        inputFiles.forEach(function(item){
            var data_from_DB = item.getFileData();
            if(data_from_DB !== null){
                paths.push({
                    label: item.fieldLabel.replace(':', ''),
                    path: data_from_DB['full_path'],
                    band: item.next('#raster_calculator_band').getValue(),
                    sizeX: data_from_DB['num_columns'],
                    sizeY: data_from_DB['num_rows']
                });
            }
        });
        return paths;
    },

    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        var inputs = me.__parseInputFiles();
        var expression = encodeURIComponent(me.parseInputValues('calc_expression')).replace(/%0A/gi, '');

        return {
            toolID:         me.toolID,
            inputs:         JSON.stringify(inputs),
            output_name:    me.parseInputValues('output_name'),
            no_data:        me.parseInputValues('no_data'),
            expression:     expression,
            type:           me.parseInputValues('output_type'),
            overwrite:      me.parseInputValues('calc_overwrite'),
            reproject:      me.parseInputValues('reproject')
        };
    }


});