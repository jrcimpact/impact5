/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.EUDR_intersect', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'IntersectionWindowEUDR',
    title: "Intersect vector data with GFC 2020 map",
    backgroundColor: "#ffffe5",
    width: 460,
    //height: 800,
    toolID: 'EUFOintersect',
    scroll: true,


    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            //scroll: true,
            //height: 800,
            title: 'Inputs',
            items: [

                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'eudrAOI',
                    filters: ['vector', 'geojson'],
                    selectionMode: 'single',
                    fieldLabel: 'AOI',
//                    listeners: {
//                        'FileInput_change': function(){
//                            me.__setAttributes(this.getFileData());
//                        }
//                    }
                }),
//                Ext.create('IMPACT.view.component.Field.Combo',{
//                    fieldLabel: 'Keep unique ID from field',
//                    itemId: 'eudrAttribute',
//                    labelWidth: 140,
//                    width: 250,
//                    padding: '5 5 10 100',
//                    store: [],
//                    allowBlank: true,
//
//
//                }),

                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'eudrGFCM',
                    filters: ['raster'],
                    selectionMode: 'single',
                    fieldLabel: 'Forest Map',
                    padding: '5 0 10 0',
                }),
                {
                    xtype: 'checkboxfield',
                    itemId: 'onlineGFCM',
                    padding: '5 5 10 100',
                    //width: 500,
                    //labelWidth: 450,
                    boxLabel: 'Download from JRC - GFC 2020',
                    name: 'JRC',
                    inputValue: 'JRC',
                    listeners: {
                        buffer: 50,
                        change: function(checkbox, newValue) {
                            if(newValue===false){
                                me.down('#eudrGFCM').enable();
                                me.down('#eudrForestID').enable();
                                me.down('#eudr_tmp').disable();
                            } else {
                                me.down('#eudrGFCM').disable();
                                me.down('#eudrForestID').disable();
                                me.down('#eudr_tmp').enable();
                            }
                        }
                    }
                }



            ] // end of processing panel
        });

       items.push(
                // MAIN FRAME for  Class and C M  Factor table   +   Notes
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    //margin: '10 10 0 0',
                    title:'Parameters ',
                    //width:600,
                    collapsible: true,
                    items: [

                               //     Land COVER ID
                               Ext.create('IMPACT.view.component.Field.Text',{
                                            itemId: 'eudrForestID',
                                            fieldLabel: 'Forest class values',
                                            //hideLabel: true,
                                            labelAlign: 'top',
                                            padding: '5 5 10 10',
                                            labelWidth: 80,
                                            width: 100,
                                            value: '1',
                                            labelStyle: 'font-weight: bold;',
                                            inputAttrTpl: " data-qtip=' List class ID of each Forest class (e.g. 10,11,13)' ",
                                            validator: function(value){
                                                if (value == ''){ return false};
                                                if(value.match(/^(\d{1,5},)*\d{1,5}$/g)){
                                                  return true;
                                                }
                                                return 'This field format is not valid';
                                            }
                               }),

                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'Mode',
                                    labelWidth: 50,
                                    labelAlign: 'top',
                                    padding: '5 5 10 10',
                                    labelStyle: 'font-weight: bold; margin-top:20px;margin-left:20px;',
                                    columns: 1,
                                    itemId: 'eudr_proc_mode',
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            boxLabel: 'Intersect Vertices',
                                            name: 'eudr_mode',
                                            inputValue: 'vertex',

                                        },
                                        {
                                            xtype: 'radiofield',
                                            boxLabel: 'Intersect geometry',
                                            name: 'eudr_mode',
                                            inputValue: 'geometry',
                                            checked: true
                                        }
                                    ],
                                    listeners: {
                                        change: function(obj, newValue){
                                                    if(newValue.eudr_mode == 'vertex'){
                                                        this.up('#IntersectionWindowEUDR').down('#eudr_proc_type').disable();
                                                    } else{
                                                        this.up('#IntersectionWindowEUDR').down('#eudr_proc_type').enable();
                                                    }
                                                }
                                    }
                                },

                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'Type',
                                    labelWidth: 50,
                                    labelAlign: 'top',
                                    padding: '5 5 10 10',
                                    labelStyle: 'font-weight: bold; margin-top:20px;margin-left:20px;',
                                    columns: 1,
                                    itemId: 'eudr_proc_type',
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            boxLabel: 'Count',
                                            name: 'eudr_type',
                                            inputValue: 'count',
                                            checked: false,
                                        },
                                        {
                                            xtype: 'radiofield',
                                            boxLabel: 'Percentage',
                                            name: 'eudr_type',
                                            inputValue: 'percentage',
                                            checked: true,
                                        }
                                    ]
                                },


                               //    Notes on 2nd col on right


                    ]
                } //  end of 2 col block with params and notes
       );

       items.push(
                    {
                        xtype: 'ProcessingInfo',
                        width:630,
                        //margin:'5 0 0 10',
                        padding: 5,
                        html: '<b>NOTES</b>:<br />- <b>Beta Version: </b> under consolidation</br>'+
                               '- <b>Output</b> directory is same as the input AOI</br>'+
                               '- Processing by <b>Vertices </b> returns a new vector file with "Point" geometries </br>'+
                               '- Documentation and data access see “Info and Manuals” under "EUFO Tools"'

                    }
       );


        items.push(
                   //   processing options
                  {
                    xtype: 'panel',
                    layout: 'hbox',
                    border: false,
                    collapsible: false,
                    bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
                    //title: 'Processing options',
                    items: [

                        Ext.create('IMPACT.view.component.Field.Text',{
                            itemId: 'eudr_output_name',
                            fieldLabel: '<b>Output suffix</b>',
                            labelWidth: 95,
                            labelAlign: 'top',
                            width: 150,
                            margin: '5 0 0 0',
                            allowBlank:false,
                            regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                        }),
                        Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                            fieldLabel: '<b>Save JRC map</b>',
                            radioName: 'eudr_tmp',
                            defaultValue: 'No',
                            labelAlign: 'top',
                            labelWidth: 100,
                            width: 100,
                            margin: '8 0 0 15'
                        }),
                        Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                            fieldLabel: '<b>Overwrite</b>',
                            radioName: 'eudr_overwrite',
                            defaultValue: 'No',
                            labelAlign: 'top',
                            labelWidth: 100,
                            width: 100,
                            margin: '8 0 0 15'
                        }),
                    ]
                },

        );

        return items;
    },


//    __setAttributes: function(data_from_DB){
//        if(!data_from_DB){return}
//        var attributes = data_from_DB['attributes'].split(",");
//
//        var attributeField = this.query('#eudrAttribute')[0];
//        attributeField.setValue('');
//        attributeField.bindStore(attributes);
//        attributeField.setDisabled(false);
//
//    },


    launchProcessing: function(){

        var me = this;

        return {
            toolID:                 me.toolID,
            aoi:                    me.parseInputValues('eudrAOI'),
            attribute:              '',                               //me.parseInputValues('eudrAttribute'),
            forestMap:              me.down('#onlineGFCM').getValue() ? '' : me.parseInputValues('eudrGFCM'),
            LC_Ids:                 JSON.stringify(me.parseInputValues('eudrForestID').split(',')),
            proc_mode:              me.parseInputValues('eudr_proc_mode'),
            proc_type:              me.parseInputValues('eudr_proc_type'),
            out_name:               me.parseInputValues('eudr_output_name'),
            overwrite:              me.parseInputValues('eudr_overwrite'),

            save_tmp:               me.parseInputValues('eudr_tmp')
        };
    }

});