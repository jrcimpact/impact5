/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.WaterBalance', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowWaterBalance',
    title: "Water Dynamics",
    backgroundColor: "#e6e6fa",

    toolID: 'waterBalance',

    selectionMode: 'multiple',
    filters: ['raster', 'class'],

    initComponent: function(){
        var me = this;
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                'label',
                'full_path',
                'data_from_DB'
            ]
        });

        me.callParent();
       },


    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];
        var tree = Ext.create('IMPACT.view.component.ImageTree.Tree', {
                        height : 300,
                        selectionMode: me.selectionMode,
                        filters: me.filters,
                        fileStore: Ext.data.StoreManager.lookup('ImageStore'),
                        selectionStore: me.store,
                        allowDrag : true,
                        listeners: {
                            checkchange: function(  ) {
                                    me.__appendProjectionPixelSize();
                            }
                        }

                });
         //tree.__buildTree(IMPACT.GLOBALS['data_paths']['data']);

         items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            title: 'Input images',
            items: [
                {
                    xtype: 'ProcessingInfo',
                    margin: '5px',
                    html: 'Reorder using drag & drop. TOP = OLDER'
                },

                tree
            ]
        });
        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            title: 'Processing options',
            items: [

                Ext.create('IMPACT.view.component.Field.Combo', {
                    fieldLabel: 'Band 1',
                    itemId: 'nir',
                    store: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                    value: 2,
                    labelWidth: 80,
                    width: 140
                }),
                Ext.create('IMPACT.view.component.Field.Combo', {
                    fieldLabel: 'Band 2',
                    itemId: 'swir',
                    store: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                    value: 5,
                    labelWidth: 80,
                    width: 140
                }),

                {
                    xtype: 'ProcessingInfo',
                    html: '<div> e.g. NDWI Landsat/Sentinel: B1 = Green B2 = SWIR1 <br/> More info on <a target="_blank" href="https://drive.google.com/drive/folders/1dGneHYbKEzTFP1HzMr5YE32pPkLoOmW_?usp=drive_link">Manual</a></div>'
                },

                {
                    itemId: 'RecodeValuesContainer',
                    title: 'Water ranges',
                    bodyStyle: 'padding: 3px 5px;',
                    hidden: false,
                    maxHeight: 300,
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                    items: [{

                            xtype: 'panel',
                            collapsible: false,
                            border: false,
                            layout:'column',
                            style: 'margin-left:120px;',
                            items: [
                                {
                                    html: 'from [>=]',
                                    border: false,
                                    width: 75,
                                    bodyStyle: 'text-align: center; font-weight: bold;'
                                },
                                {
                                    html: 'to [<=]',
                                    border: false,
                                    width: 75,
                                    bodyStyle: 'text-align: center; font-weight: bold;'
                                }

                            ]
                            },
                             me.__insert_data_range()

                    ]},
                // -----------------------  Reprojection on the fly ------------------------------
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Force reprojection </b>',
                    radioName: 'reproject',
                    defaultValue: 'No',
                    listeners: {
                        change: function(radio, value) {
                            if(value.reproject_val === "Yes"){
                                me.down('#reproject_warning').show();
                                me.down('#psizeX').show();
                                me.down('#psizeY').show();
                                me.down('#outEPSG').show();
                            } else {
                                me.down('#reproject_warning').hide();
                                me.down('#psizeX').hide();
                                me.down('#psizeY').hide();
                                me.down('#outEPSG').hide();
                            }
                        }
                    }
                }),
                {
                    xtype: 'ProcessingInfo',
                    type: 'warning',
                    itemId: 'reproject_warning',
                    hidden: true,
                    html: '<b>Warning</b>: please be aware that using on the fly reprojection and warping might lead to incorrect detection',
                },

                // ##### Output type #####
                Ext.create('IMPACT.view.component.Field.Combo',{
                    itemId: 'outEPSG',
                    fieldLabel: 'Output projection',
                    //labelStyle: 'font-weight: bold;',
                    hidden: true,
                    labelWidth: 130,
                    width: 450,
                    store: {
                        xtype: 'store',
                        fields: ['value', 'name', 'psize'],
                        data : []
                    },
                    displayField: 'name',
                    valueField: 'value',
                    value: "Select",
                    listeners: {
                        'select': function(combo, record, index) {
                            me.down('#psizeX').setValue(record[0].data.psize);
                            me.down('#psizeY').setValue(record[0].data.psize);
                        }
                    },
                    validator: function (value) {
                        if(me.parseInputValues('reproject')==='Yes' && value==='Select'){
                            return 'This field is required'
                        }
                        return true;
                    }
                }),

                                // ######  Output pixel Size  ######
                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'psizeX',
                    labelAlign: 'Left',
                    labelWidth : 130,
                    width: 270,
                    hidden: true,
                    fieldLabel: 'Output Pixel Size X',
                    inputAttrTpl: " data-qtip=' Force output Pixel Size in X' "
                }),
                // ######  Output pixel Size  ######
                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'psizeY',
                    labelAlign: 'Left',
                    labelWidth : 130,
                    width: 270,
                    hidden: true,
                    fieldLabel: 'Output Pixel Size Y',
                    inputAttrTpl: " data-qtip=' Force output Pixel Size in Y' "
                }),



                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    defaultValue: 'No',
                    radioName: 'water_overwrite'
                }),
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_name',
                    fieldLabel: 'Output prefix ',
                    allowBlank: false,
                    labelWidth: 120,
                    width: 350,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                })
            ]
        });

        return items;
    },

    __insert_data_range: function(){
        var me = this;
        return {
            xtype: 'panel',
            //itemId: 'biomRangeId',
            collapsible: false,
            border: false,
            layout:'column',
            style: 'margin-left:120px;',
            items: [
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'start_value',
                    width: 75,
                    value: 0,
                    validator: function(value){

                            if(value===''){
                                return 'This field is required';
                            }
                        return true;
                    },
                    listeners: {
                        change: function (item,newValue,oldValue) {
                            this.up('panel').down('#end_value').isValid();
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'end_value',
                    width: 75,
                    value: 1,
                    validator: function(value){
                                if(value===''){
                                    return 'This field is required';

                                }
                                return true;
                    },
                    listeners: {
                        change: function (item,newValue,oldValue) {
                             this.up('panel').down('#start_value').isValid();
                        }
                    }
                }),


            ]
        }
    },

    __appendProjectionPixelSize: function(){
        var me = this;
        var projList = [];
        Ext.Array.each(me.down('#ImageTree').getSelected(), function(node){        // undefined if never filtered - need to be init

                if(projList.length == 0 || projList.map(projList => projList.value).indexOf(node['EPSG']) == -1 ){
                    projList.push({"value": node['EPSG'], "name": node['PROJ4'],"psize":node['pixel_size']});
                }


        });

        if(projList.length > 1){
            // enable reprojection tool
            me.down('#reproject').setValue({reproject_val:"Yes"});
        }

        me.down('#outEPSG').store.loadData(projList);
        me.down('#outEPSG').setValue(projList[0].value);

        me.down('#psizeX').setValue(projList[0].psize);
        me.down('#psizeY').setValue(projList[0].psize);

    },




    launchProcessing: function(){
        var me = this;

        var names = [];
        Ext.Array.each(me.down('#ImageTree').getSelected(), function(node){        // undefined if never filtered - need to be init
                names.push(node['full_path']);
        });

        if (names.length < 2){
            alert("At least 2 images are required")
            return

        }


        return {
            toolID:         me.toolID,
            img_names:      JSON.stringify(names),
            start_value:    me.parseInputValues('start_value'),
            end_value:      me.parseInputValues('end_value'),
            outname:        me.parseInputValues('output_name'),
            nir:            me.parseInputValues('nir'),
            swir:           me.parseInputValues('swir'),

            reproject:      me.parseInputValues('reproject'),
            outEPSG:        me.parseInputValues('outEPSG'),
            psizeX:         me.parseInputValues('psizeX'),
            psizeY:         me.parseInputValues('psizeY'),
            overwrite:      me.parseInputValues('water_overwrite')

        };
    },

    afterShow: function() {
                // refresh store to sync data change
                 this.down('#ImageTree').__buildTree(IMPACT.GLOBALS['data_paths']['data']);
    }

});