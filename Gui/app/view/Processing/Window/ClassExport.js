/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.ClassExport', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowClassExport',
    title: 'Convert to Binary Mask',

    toolID:'vector2raster',

    width: 450,

    legendType: null,
    full_path: null,
    layername: null,
    units: null,

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            title: 'Select classes to convert',
            maxHeight: 300,
            overflowY: 'scroll',

            items: [
                Ext.create('IMPACT.view.component.ColorPalette.Vector.Grid', {
                    legendType: me.legendType,
                    showCheckbox: true

                })
            ]
        });

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
            title: 'Conversion options',
            items: [
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'raster2vector_output_name',
                    fieldLabel: 'Output Name',
                    labelWidth: 120,
                    width: 320,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'raster2vector_resolution',
                    fieldLabel: 'Raster resolution',
                    labelWidth: 120,
                    width: 200,
                    value: 30,
                    allowBlank: false
                }),
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'raster2vector_units',
                    fieldLabel: 'Units',
                    labelWidth: 120,
                    width: 200,
                    value: me.units.toString(),
                    disabled: true
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    itemId:'raster2vector_overwrite',
                    defaultValue: 'No',
                    radioName: 'raster2vector_overwrite_name',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 120,
                    width: 260
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: '<b>Note</b>: <br />' +
                          '- selected Classes -> 1, other -> 0 <br />'

                }
            ]
        });

        return items;
    },

    /**
     * Extend __validate(): allow to validate class selection
     * @returns {boolean}
     * @private
     */
    __validate: function(){
        var me = this;
        var classes = me.down('#ColorPaletteVectorGrid').getChecked();
        if(classes.length===0){
            IMPACT.view.Processing.Window.component.ValidationMsg.show(['Select at least one class.']);
            return false;
        }
        return me.callParent();
    },

    /**
     *  Launch processing
     */
    launchProcessing: function(){
        var me = this;

        return {
            toolID:         me.toolID,
            ids:            JSON.stringify(me.down('#ColorPaletteVectorGrid').getChecked()),
            full_path:      me.full_path,
            layername:      me.layername,
            outname:        me.parseInputValues('raster2vector_output_name'),
            resolution:     me.parseInputValues('raster2vector_resolution'),
            overwrite:      me.parseInputValues('raster2vector_overwrite_name')
        };
    }


});