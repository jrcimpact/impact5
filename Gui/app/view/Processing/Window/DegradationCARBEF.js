/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 * Modifications: Bruno Combal
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.DegradationCARBEF', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowDegradationCARBEF',
    title: "CarbEF - Reporting on Forest Carbon Emission (degradation and deforestation)",
    backgroundColor: "#e6e6fa",

    toolID: 'CarbEF',

    width: 700,

     /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'activity_map',
                    filters: ['raster', 'class'],
                    fieldLabel: 'Tree cover loss map',
                    labelWidth: 100,
                    width: 710
                }),
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Outputs: MFU classification image, general HTML report and detailed Excel report'
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Text',{
                            itemId: 'output_name',
                            fieldLabel: 'Output Name',
                            labelWidth: 80,
                            width: 240,
                            allowBlank: false,
                            regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                        }),
                        /*
                        Ext.create('IMPACT.view.component.Field.Combo',{
                            itemId: 'repo_language',
                            store: ["EN","FR"],
                            value: "EN",
                            fieldLabel: 'Report Language',
                            labelWidth: 100,
                            width: 160,
                            style: 'margin-left: 15px;'
                            }),
                            */
                        /* Now, overwrite is forced
                        Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                            radioName: 'degradation_overwrite',
                            fieldLabel: 'Overwrite output',
                            labelWidth: 100,
                            width: 220,
                            style: 'margin-left: 15px;'
                        })
                        */
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Minimum Forest Units are square-shaped, their size (width) is in tree cover loss map pixel.<br />Typical size is 9px for Landsat, equivalent to 0.81ha.'
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Integer',{
                            itemId: 'kernel_size',
                            fieldLabel: 'MFU width (px)',
                            labelWidth: 100,
                            width: 150,
                            value: 3,
                            validator: function(value){
                                if(value>=3) return true;
                                return 'Must be greater or equal to 3'
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'forest_mmu_fraction',
                            fieldLabel: 'Minimum tree cover per Forest Unit (%)',
                            labelWidth: 230,
                            width: 270,
                            value: 30,
                            style: 'margin-left: 15px;'
                        })
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Definition (starting and ending years) of periods 1 and 2'
                },
                {
                    xtype:'radiogroup',
                    itemId:'use1or2Periods',
                    fieldLabel:'',
                    labelWidth:170,
                    width:320,
                    items:[{boxLabel:'One period', name:'useTwoPeriods', inputValue:'False', checked:true},
                            {boxLabel:'Two periods', name:'useTwoPeriods', inputValue:'True', checked:false}],
                    listeners:{
                        change: function (field, value) {
                            if (value['useTwoPeriods'] == 'False'){
                                thisPeriod = me.down('#startYY2');
                                thisPeriod.disable().reset();
                                thisPeriod = me.down('#endYY2');
                                thisPeriod.disable().reset();
                            } else {
                                thisPeriod = me.down('#startYY2');
                                thisPeriod.enable().show();
                                thisPeriod.value='';
                                thisPeriod = me.down('#endYY2');
                                thisPeriod.enable().show();
                                thisPeriod.value='';
                            }
                        }
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Integer',{
                            itemId: 'startYY1',
                            fieldLabel: 'Period 1, start year',
                            labelStyle:'font-size:11px',
                            labelWidth: 115,
                            width: 160,
                            validator: function(value){
                                if(value < 1970) return 'Must be greater the 1970';
                                return true;
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Integer',{
                            itemId: 'endYY1',
                            fieldLabel: 'Period 1, end year',
                            labelWidth: 115,
                            width: 160,
                            style: 'margin-left: 10px;',
                            labelStyle:'font-size:11px',
                            validator: function(value){
                                if(value < 1970) return 'Must be greater than 1970';
                                if(value < me.down('#startYY1').getValue()) return 'Must be greater than start year';
                                return true;
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Integer',{
                            itemId: 'startYY2',
                            fieldLabel: 'Period 2, start year',
                            labelWidth: 105,
                            width: 150,
                            style: 'margin-left: 10px',
                            disabled:true,
                            labelStyle:'font-size:11px',
                            validator: function(value){
                                if(value < 1970 && this.isDisabled()==false) return 'Must be greater than 1970';
                                return true;
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Integer',{
                            itemId: 'endYY2',
                            fieldLabel: 'Period 2, end year',
                            labelWidth: 105,
                            width: 150,
                            style: 'margin-left: 10px;',
                            disabled:true,
                            labelStyle:'font-size:11px',
                            validator: function(value){
                                if(this.isDisabled()==false){
                                    if(value < 1970) return 'Must be greater than 1970';
                                    if(value < me.down('#startYY1').getValue()) return 'Must be greater than start year';
                                }
                                return true;
                            }
                        })
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Use forest carbon stock map or a constant'
                },
                {
                    xtype: 'radiogroup',
                    itemId: 'useConversionMap',
                    fieldLabel: 'Use forest carbon stock map or a constant?',
                    labelWidth: 300,
                    width: 450,
                    style: 'margin-bottom: 10px;',
                    items: [
                        {boxLabel: 'map', name: 'useImageGrp', inputValue: 'True', checked: false},
                        {boxLabel: 'constant', name: 'useImageGrp', inputValue: 'False', checked: true}
                    ],
                    listeners: {
                        change: function (field, newValue) {
                            var emission_factor_map = me.down('#emission_factor_map');
                            if (newValue['useImageGrp'] === 'True') {
                                emission_factor_map.enable().show();
                                me.down('#biomass_value').disable().setValue('');
                            }
                            else {
                                emission_factor_map.disable().reset();
                                emission_factor_map.hide();
                                me.down('#biomass_value').enable();
                            }
                        }
                    }
                },
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'emission_factor_map',
                    filters: ['raster', 'class'],
                    fieldLabel: 'Forest carbon stock map',
                    disabled: true,
                    hidden: true,
                    labelWidth: 150,
                    width: 650,
                    allowBlank: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('useConversionMap')==='True'){
                            return 'This field is required';
                        }
                        return true;
                    }
                }),
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'biomass_value',
                            fieldLabel: 'Forest carbon stock (t/ha)',
                            labelWidth: 170,
                            width: 220,
                            validator: function(value){
                                if(me.parseInputValues('useConversionMap')==='False'){
                                    if(value===''){
                                        return 'This field is required';
                                    } else if(value<0){
                                        return 'Must be >= 0'
                                    }
                                }
                                return true;
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'biomassDegradPercent',
                            fieldLabel: 'Option A - Proportion (%) of carbon stock emitted by degradation',
                            labelWidth: 240,
                            width: 290,
                            value: 30.0,
                            style: 'margin-left: 15px',
                            validator: function(value){
                                if(value>=0 && value<=100) return true;
                                return 'Must be between 0 and 100'
                            }
                        })
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Landuse layer'
                },
                {
                    xtype: 'radiogroup',
                    itemId: 'useDisagShp',
                    fieldLabel: 'Use landuse layer?',
                    labelWidth: 170,
                    width: 320,
                    items: [
                        {boxLabel: 'Yes', name: 'useDisagGrp', inputValue: 'True', checked: false},
                        {boxLabel: 'No', name: 'useDisagGrp', inputValue: 'False', checked: true}
                    ],
                    listeners: {
                        change: function (field, newValue) {
                            var disaggregation_shapefile = me.down('#disaggregation_shapefile');
                            var fieldDisagDD = me.down('#fieldDisagDD');
                            if (newValue['useDisagGrp'] === 'True') {
                                disaggregation_shapefile.enable().show();
                                fieldDisagDD.show();
                            } else {
                                disaggregation_shapefile.disable().reset();
                                disaggregation_shapefile.hide();
                                fieldDisagDD.setAttributes(null);
                                fieldDisagDD.disable();
                                fieldDisagDD.hide();
                            }
                        }
                    }
                },
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'disaggregation_shapefile',
                    filters: ['vector'],
                    fieldLabel: 'Landuse shapefile',
                    labelWidth: 170,
                    width: 650,
                    disabled: true,
                    hidden: true,
                    allowBlank: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('useDisagShp')==='True'){
                            return 'This field is required';
                        }
                        return true;
                    },
                    listeners: {
                        'FileInput_change': function(){
                            var data = this.getFileData();
                            var fieldDisagDD = me.down('#fieldDisagDD');
                            fieldDisagDD.enable();
                            fieldDisagDD.setAttributes(data);
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Select a field',
                    labelWidth: 90,
                    width: 240,
                    itemId: 'fieldDisagDD',
                    store: [],
                    disabled: true,
                    hidden: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('useDisagShp')==='True'){
                            return 'This field is required';
                        }
                        return true;
                    },
                    setAttributes: function(data){
                        this.setValue('');
                        if(data!==null && data.hasOwnProperty('attributes')){
                            var attributes = data['attributes'].split(",");
                            this.bindStore(attributes);
                            this.enable();
                        } else{
                            this.disable();
                        }
                    }
                }),
                {
                    xtype: 'ProcessingInfo',
                    type: 'title',
                    html: 'Exception map'
                },
                {
                    xtype:'radiogroup',
                    itemId:'useExceptMap',
                    fieldLabel:'Use Exception map?',
                    labelWidth: 170,
                    width: 320,
                    items:[
                        {boxLabel: 'Yes', name: 'useExceptGrp', inputValue: 'True', checked: false},
                        {boxLabel: 'No', name: 'useExceptGrp', inputValue: 'False', checked: true}
                    ],
                    listeners:{
                        change:function(field, newValue){
                            var exception_map = me.down('#exception_map');
                            if (newValue['useExceptGrp'] === 'True'){
                                exception_map.enable().show();
                            } else {
                                exception_map.disable().reset();
                                exception_map.hide();
                            }
                        }
                    }
                },
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'exception_map',
                    filters: ['raster', 'class'],
                    fieldLabel: 'Exception map',
                    labelWidth: 170,
                    width: 650,
                    disabled: true,
                    hidden: true,
                    allowBlank: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('useExceptMap')==='True'){
                            return 'This field is required';
                        }
                        return true;
                    }
                })

            ]
        });

        return items;
    },

    launchProcessing: function(){

        var me = this;

        me.debug_params = false;

        return {
            toolID:                 me.toolID,
            master_img:             me.parseInputValues('activity_map'),
            outname:                me.parseInputValues('output_name'),
            kernel_size:            me.parseInputValues('kernel_size'),
            biomass_value:          me.parseInputValues('biomass_value'),
            //overwrite:              me.parseInputValues('degradation_overwrite'),
            overwrite:  'Yes',
            //repo_language:          me.parseInputValues('repo_language'),
            startYY1:               me.parseInputValues('startYY1'),
            endYY1:                 me.parseInputValues('endYY1'),
            startYY2:               me.parseInputValues('startYY2'),
            endYY2:                 me.parseInputValues('endYY2'),
            useTwoPeriods:          me.parseInputValues('use1or2Periods'),
            forest_mmu_fraction:    me.parseInputValues('forest_mmu_fraction'),
            useConversionMap:       me.parseInputValues('useConversionMap'),
            biomassDegradPercent:   me.parseInputValues('biomassDegradPercent'),
            conversionMap:          me.parseInputValues('emission_factor_map'),
            useDisagShp:            me.parseInputValues('useDisagShp'),
            disagShp:               me.parseInputValues('disaggregation_shapefile'),
            disagField:             me.parseInputValues('fieldDisagDD'),
            exceptMap:              me.parseInputValues('exception_map'),
            useExceptMap:           me.parseInputValues('useExceptMap')
        };

    }

});