/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.RoadMapper', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowRoadMapper',
    title: "Road Mapper",
    backgroundColor: "#e5ffdd",

    toolID: 'roadMapper',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input images',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'input_images',
                    filters: ['raster', 'class'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Input images',
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                 {
                    xtype: 'container',
                    layout: 'vbox',
                    style: 'margin-bottom: 6px;',
                    items: [

                            Ext.create('IMPACT.view.component.Field.Combo',{
                                itemId: 'roadBand',
                                fieldLabel: 'Band',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'left',
                                labelWidth: 150,
                                width: 280,
                                store: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                                displayField: 'value',
                                valueField: 'value',
                                value: 1,
                            }),

                             {
                                xtype: 'numberfield',
                                itemId: 'roadKernel',
                                fieldLabel: 'Kernel',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'left',
                                labelWidth: 150,
                                width: 280,
                                value: 7,
                                minValue: 1,
                                step: 2,
                                maxValue: 35
                            },
                            {
                                xtype: 'numberfield',
                                itemId: 'roadThreshold',
                                fieldLabel: 'Kernel sum threshold',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'left',
                                labelWidth: 150,
                                width: 280,
                                value: 2.5,
                                minValue: 0,
                                maxValue: 255,
                                step: 0.1,
                            },
                            {
                                xtype: 'numberfield',
                                itemId: 'roadMMU',
                                fieldLabel: 'Sieve size (MMU)',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'left',
                                labelWidth: 150,
                                width: 280,
                                value: 300,
                                minValue: 1,
                                maxValue: 5000
                            }

                            ]
                 },

                                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_suffix',
                    fieldLabel: 'Output Suffix',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 110,
                    width: 350,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),

                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'road_overwrite',
                    defaultValue: 'No',
                    labelWidth: 170,
                    width: 300,
                    labelStyle: 'font-weight: bold;'
                }),

                {
                    xtype: 'ProcessingInfo',
                    style:'margin-left:90px; margin-top:20px;backgroundColor:'+ me.backgroundColor+';',
                    html: 'More info about Road Mapper are available <a target="_blank" href="https://drive.google.com/drive/folders/1dGneHYbKEzTFP1HzMr5YE32pPkLoOmW_?usp=drive_link">here</a> '
                }

            ]
        });

        return items;
    },

    launchProcessing: function(){

        var me = this;

        return {
            toolID:        me.toolID,
            roadBand:      me.parseInputValues('roadBand'),
            roadKernel:    me.parseInputValues('roadKernel'),
            roadMMU:       me.parseInputValues('roadMMU'),
            roadThreshold: me.parseInputValues('roadThreshold'),

            out_suffix:     me.parseInputValues('out_suffix'),
            img_names:      me.parseInputValues('input_images'),
            overwrite:      me.parseInputValues('road_overwrite'),

        };
    }

});
