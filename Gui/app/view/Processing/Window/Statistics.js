/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Statistics', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowStatistics',
    title: "Statistics",
    backgroundColor: "#e5fff4",

    toolID: 'statistics',

    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: false,
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'input_shapefile',
                    filters: ['vector'],
                    fieldLabel: 'Input Shapefile'
                }),
                {
                xtype: 'ProcessingPanel',
                backgroundColor: me.backgroundColor,
                scroll: true,
                items: [
                    Ext.create('IMPACT.view.component.Field.File', {
                        itemId: 'input_raster',
                        selectionMode: 'multiple',
                        filters: ['raster', 'class'],
                        fieldLabel: 'Input Rasters'
                    })
                ]},
                Ext.create('IMPACT.view.component.Field.Combo',{
                    itemId: 'statsBands',
                    fieldLabel: 'Stats from band(s)',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 130,
                    width: 185,
                    store: {
                        xtype: 'store',
                        fields: ['value', 'name'],
                        data : [
                            {"value":"All", "name":"All"},
                            {"value":"1", "name":"1"},
                            {"value":"2", "name":"2"},
                            {"value":"3", "name":"3"},
                            {"value":"4", "name":"4"},
                            {"value":"5", "name":"5"},
                            {"value":"6", "name":"6"},
                            {"value":"7", "name":"7"},
                            {"value":"8", "name":"8"},
                            {"value":"9", "name":"9"},
                            {"value":"10", "name":"10"},
                            {"value":"11", "name":"11"},
                            {"value":"12", "name":"12"},
                            {"value":"13", "name":"13"}

                        ]
                    },
                    displayField: 'name',
                    valueField: 'value',
                    value: "1",
                }),
                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'nodata_list',
                    style: 'margin: 3px;',
                    emptyText : 'use Metadata',
                    fieldLabel: 'Extra Nodata value (in addition to metadata one)',
                    labelWidth : 300,
                    width: 420,
                    inputAttrTpl: " data-qtip=' e.g. -2' "
                }),
                {
                    xtype: 'checkboxgroup',
                    itemId: 'statsList',
                    fieldLabel: 'Statistics',
                    labelWidth: 54,
                    labelStyle: 'font-weight: bold;',
                    width: 430,
                    columns: 5,
                    defaults: {
                        boxLabelAlign: 'before'
                    },
                    items: [
                        { boxLabel: 'Area',  name: 'Area', inputValue: 'i_area',checked: true},
                        { boxLabel: 'Para*',  name: 'Para', inputValue: 'i_para'},
                        { boxLabel: 'Count',  name: 'Count', inputValue: 'ct', checked: true},
                        { boxLabel: 'Percent',  name: 'Percent', inputValue: 'pct', checked: true},
                        { boxLabel: 'Mean',  name: 'Mean', inputValue: 'mean'},
                        { boxLabel: 'Median',  name: 'Median', inputValue: 'med', checked: true},
                        { boxLabel: 'Mode',  name: 'Mode', inputValue: 'mode'},
                        { boxLabel: 'SD',  name: 'SD', inputValue: 'sd'},
                        { boxLabel: 'Min',  name: 'Min', inputValue: 'min'},
                        { boxLabel: 'Max',  name: 'Max', inputValue: 'max'}
                    ]

                },
                {
                    xtype: 'ProcessingInfo',
                    html: '* Para = Perimeter / Area (indicator of shape compactness)<br />'

                }
            ]
        });

        return items;
    },

    launchProcessing: function(){

        var me = this;

       return {
            toolID:         me.toolID,
            shp_names:      me.parseInputValues('input_shapefile'),
            img_names:      me.parseInputValues('input_raster'),
            nodata_list:    me.parseInputValues('nodata_list'),
            statsList:      me.parseInputValues('statsList'),
            statsBands:     me.parseInputValues('statsBands')
        };
    }


});