/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Segmentation', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    requires: [
        'IMPACT.view.Processing.Window.component.SegmentationInput',
        'IMPACT.view.component.Field.Float',
        'IMPACT.view.component.Field.Text'
    ],

    itemId: 'ProcessingWindowSegmentation',
    title: "Segmentation Options",
    width: 555,

    toolID: 'segmentation',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        // #####  Inputs rasters  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                title: 'Import existing segmentation / vector map',
                //scroll: true,
                bodyStyle: {
                    padding: '15px',
                    backgroundColor: '#ffffff'
                },
                items: [
                            {   xtype: 'impactFieldRadioYesNo',
                                fieldLabel: '<b>Import and update existing segmentation / vector map? </b>',
                                radioName: 'segmentation_useTemplate',
                                defaultValue: 'No',
                                labelWidth: 400,
                                listeners : {
                                    change : function(radio, newValue) {
                                        var useTemplate = newValue['segmentation_useTemplate_val']==="Yes";

                                        if (useTemplate) {
                                            me.down('#old_segmentation_input').show();
                                            me.down('#old_segmentation_attribute').show();
                                            me.down('#multidate').setDisabled(true);


                                        } else {
                                              me.down('#old_segmentation_input').hide();
                                              me.down('#old_segmentation_attribute').hide();
                                              me.down('#multidate').setDisabled(false);
                                        }
                                    }
                                }
                          },
                           {
                                xtype: 'ProcessingInfo',
                                itemId: 'oldSegmInfo',
                                hidden: false,
                                html: '- <b>No</b>&nbsp;: segmentation is based only on selected input images(s)<br/> ' +
                                      '- <b>Yes</b>: vector map is dissolved by selected attributes and the new image(s) is sub-segmented <br/>' +
                                      ' &nbsp;&nbsp;&nbsp;&nbsp; within each class using the parameters defined hereafter.<br/>'+
                                      ' &nbsp;&nbsp;&nbsp;&nbsp; The old class will be saved into the DBF as T1_class while T2_class will contain <br/>'+
                                      ' &nbsp;&nbsp;&nbsp;&nbsp; a copy of the old attribute/class if no other classifications (optional) are provided '+
                                      ' &nbsp;&nbsp;&nbsp;&nbsp; Triggers multi-dates segmentation mode'

                           },

                           {
                                xtype: 'panel',
                                itemId: 'oldSegmentationOptions',
                                border: false,
                                layout: 'vbox',
                                defaults: {
                                    border: false,
                                    //width: 220,
                                    bodyStyle: 'font-weight: bold; padding-bottom: 5px;'
                                },
                                items: [

                                    Ext.create('IMPACT.view.component.Field.File', {
                                        itemId: 'old_segmentation_input',
                                        filters: ['vector'],
                                        fieldLabel: 'Input vector',
                                        width: 390,
                                        //disabled: true,
                                        hidden: true,
                                        allowBlank: true,
                                        listeners: {
                                            'FileInput_change': function(){
                                                me.__setAttributes(this.getFileData());
                                            }
                                        },
                                        validator: function(value){
                                            if(value==='' && me.parseInputValues('segmentation_useTemplate')==='Yes'){
                                                return 'This field is required';
                                            }
                                            return true;
                                        }
                                    }),
                                    {
                                        html: '<br/>'
                                    },
                                    Ext.create('IMPACT.view.component.Field.Combo',{
                                        fieldLabel: 'Select attribute field containing class code (numeric)',
                                        itemId: 'old_segmentation_attribute',
                                        labelWidth: 300,
                                        width: 470,
                                        //disabled: true,
                                        hidden: true,
                                        store: [],
                                        allowBlank: true,
                                        validator: function(value){
                                            if(value==='' && me.parseInputValues('segmentation_useTemplate')==='Yes'){
                                                return 'This field is required';
                                            }
                                            return true;
                                        }
                                    }),
                                ]
                           },
                        ]
               },
               {
                xtype: 'ProcessingPanel',
                title: 'Inputs',
                scroll: true,
                bodyStyle: {
                    padding: '5px',
                    backgroundColor: '#ffffff'
                },
                items: [
                    {
                        xtype: 'panel',
                        itemId: 'baseSegmentationOptions',
                        border: false,
                        layout: 'hbox',

                        defaults: {
                            border: false,
                            width: 220,
                            bodyStyle: 'font-weight: bold; padding-bottom: 5px;'
                        },
                        items: [
                            {
                                html: 'Inputs'
                            },
                            {
                                html: 'Classification (optional)'
                            },
                            {
                                html: 'Band',
                                width: 50
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        itemId: 'seg_inputContainer',
                        border: false,
                        items: [
                            {
                                xtype: 'SegmentationInput',
                                itemId: 'SegmentationInput_0'
                            },
                            {
                                xtype: 'SegmentationInput',
                                itemId: 'SegmentationInput_1'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Add new input',
                        iconCls: 'fas fa-lg fa-green fa-plus-circle',
                        cls: 'impact-icon-button',
                        handler: function () {
                            me.__addNewInputRecord();
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Clear inputs',
                        //style: 'margin-left: 5px;',
                        iconCls: 'fa fa-lg fa-red fa-times-circle',
                        cls: 'impact-icon-button',
                        handler: function () {
                            me.__clearInputRecords();
                        }
                    }
                ]
            }
        );

        // #####  Processing options  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                backgroundColor: me.backgroundColor,
                title: 'Processing Options',
                items: [
                    {
                        xtype: 'ProcessingInfo',
                        itemId: 'generalInfo',
                        hidden: false,
                        html: '<b>Info:</b><br />' +
                                '- <b>Classification</b>: if provided, each polygon is filled with the most frequent value <br /> ' +
                                '- <b>Single-date</b>: each image is segmented separately in batch mode <br />' +
                                '- <b>Multi-date</b>: input images are segmented all together (e.g. change detection);<br />'+
                                ' &nbsp;&nbsp;&nbsp;&nbsp; - output projection and pixel size are taken from the first image; <br />' +
                                ' &nbsp;&nbsp;&nbsp;&nbsp; - number of classification files determines the number of attribute columns in the DBF.'
                                //'- <b>Legend</b>: used to assign majority values from Classification to Legend Class ID  <br />'

                    },
                    Ext.create('IMPACT.view.component.Field.Combo', {
                        itemId: 'AggregationRules',
                        store: ['Majority','Min','Max'],
                        fieldLabel: '<b>Aggregation rule </b><br />only used in pre-classification',
                        value: 'Majority',
                        labelWidth : 320,
                        width: 440,
                        disabled: false,
                        allowBlank: false
                    }),
                    {   xtype: 'impactFieldRadioYesNo',
                        fieldLabel: '<b>Multi-date segmentation?</b></br>Process selected and overlapping images as time-series',
                        radioName: 'multidate',
                        defaultValue: 'No',
                        labelWidth : 320,
                        width: 440,
                        listeners: {
                            change: function (item, newValue) {
                                if(newValue.multidate_val==='Yes'){
                                    me.down('#multidate_info').show();
                                } else {
                                    me.down('#multidate_info').hide();
                                }
                            }
                        }
                    },
                    {
                        xtype: 'ProcessingInfo',
                        itemId: 'multidate_info',
                        hidden: true,
                        html: '<b>Notes:</b><br />' +
                                '- Same bands and weights will be used for all inputs<br />' +
                                '- Output directory and target projection will be taken from the first input provided<br />' +
                                '- The order of the provided inputs will reflect the order of the attributes in the DBF file'
                    },
                    Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                        fieldLabel: '<b>Overwrite</b> existing shapefile?',
                        radioName: 'baatz_overwrite',
                        defaultValue: 'No',
                        labelWidth : 320,
                        width: 440
                    }),
                    Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                        fieldLabel: '<b>Optimization</b>: use tiling to reduce memory?',
                        radioName: 'tiling',
                        labelWidth : 320,
                        width: 440,
                        defaultValue: 'Yes'
                    })
                ]
            }
        );

        // #####  Segmentation Parameters  ######
        items.push(
            {
                xtype: 'ProcessingPanel',
                title: 'Segmentation Parameters',
                layout:'column',
                height: 165,
                items: [
                    // ######  RGB Bands  ######
                    {
                        xtype: 'panel',
                        bodyStyle: 'padding: 5px;',
                        border: false,
                        width: 90,
                        items:[
                            {
                                html: 'Bands',
                                border: false,
                                bodyStyle: 'font-weight: bold; padding-bottom: 3px;'
                            },
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                fieldLabel: 'R',
                                labelWidth: 10,
                                width: 70,
                                itemId: 'R_img_bands',
                                store: [1,2,3,4,5,6,7,8,9,10,11,12],
                                value: 3
                            }),
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                fieldLabel: 'G',
                                labelWidth: 10,
                                width: 70,
                                itemId: 'G_img_bands',
                                store: ['-',1,2,3,4,5,6,7,8,9,10,11,12],
                                value: 4
                            }),
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                fieldLabel: 'B',
                                labelWidth: 10,
                                width: 70,
                                itemId: 'B_img_bands',
                                store: ['-',1,2,3,4,5,6,7,8,9,10,11,12],
                                value: 5
                            })
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'padding: 5px;',
                        border: false,
                        width: 120,
                        items: [
                            // ##### Strategy #####
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                fieldLabel: 'Strategy',
                                labelAlign: 'top',
                                labelWidth: 40,
                                width: 110,
                                store: ['Baatz','Region Growing'],
                                itemId: 'strategy',
                                value: 'Baatz',
                                listeners: {
                                    select: function(item){
                                        if(item.value === 'Baatz'){
                                            me.down('#compactness').enable();
                                            me.down('#color').enable();
                                        } else {
                                            me.down('#compactness').disable();
                                            me.down('#color').disable();
                                        }
                                    }
                                }
                            }),
                            // ##### MMU #####
                            {
                                xtype: 'impactFieldFloat',
                                itemId: 'mmu',
                                fieldLabel: 'MMU',
                                labelAlign: 'top',
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                value: 100,
                                width: 80,
                                maxValue: 10000,
                                minValue: 1
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'padding: 5px; text-align: center;',
                        border: false,
                        width: 95,
                        items: [
                            // ##### Compactness #####
                            {
                                xtype: 'impactFieldFloat',
                                itemId: 'compactness',
                                fieldLabel: 'Compactness',
                                labelAlign: 'top',
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                value: 0.8,
                                width: 50,
                                maxValue: 1,
                                minValue: 0,
                                allowBlank: false
                            },
                            // ##### Color #####
                            {
                                xtype: 'impactFieldFloat',
                                itemId: 'color',
                                fieldLabel: 'Color',
                                labelAlign: 'top',
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                value: 0.9,
                                width: 50,
                                maxValue: 1,
                                minValue: 0,
                                allowBlank: false
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'padding: 5px;',
                        border: false,
                        width: 125,
                        items: [
                            // ##### Similarity #####
                            {
                                xtype: 'impactFieldFloat',
                                itemId: 'similarity',
                                fieldLabel: 'Similarity [0-1]',
                                labelAlign: 'top',
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                width: 120,
                                value: 0.9,
                                maxValue: 1,
                                minValue: 0
                            },
                            // ##### Shapefile Suffix #####
                            {
                                xtype: 'impactFieldText',
                                itemId: 'suffix_name',
                                fieldLabel: 'Shapefile Suffix',
                                labelAlign: 'top',
                                labelStyle: 'font-weight: bold; padding-bottom: 3px;',
                                value: '',
                                width: 120,
                                regex: new RegExp(/^[0-9a-zA-Z_-]+$/),
                                validator: function(value){
                                    if(value==='' && me.parseInputValues('multidate')==='Yes'){
                                        return 'This field is required';
                                    }
                                    return true;
                                }
                            }
                        ]
                    }
                ]
            }
        );

        return items;
    },

    /**
     *  Add new input file record (RasterCalculatorInput - raster+band)
     */
    __addNewInputRecord: function(){
        var container = this.down('#seg_inputContainer');
        var index = 0;
        // Retrieve record index from itemId
        var last_item = container.items.items[container.items.length-1];
        if(typeof last_item !== "undefined"){
            index = last_item.itemId.substring(last_item.itemId.lastIndexOf("_")+1);
            index = parseInt(index)+1;
        }
        // Show/hide 'add' button
        if(index===10){
            container.next('.button').hide()
        } else {
            container.next('.button').show()
        }
        // Add new record
        container.add({
            xtype: 'SegmentationInput',
            itemId: 'SegmentationInput_'+index
        })
    },

    /**
     *  Clear raster calculator inputs
     */
    __clearInputRecords: function(){
        var me = this;
        var container = me.down('#seg_inputContainer');
        Ext.Msg.show({
            title: 'Clear all inputs',
            msg: 'Are you sure you want to clear all the inputs?',
            buttons: Ext.Msg.YESNO,
            fn: function(e){
                if(e==='yes'){
                    // destroy all existing record
                    container.items.each(function(item){
                        item.destroy();
                    });
                    // Add 2 new empty records
                    me.__addNewInputRecord();
                    me.__addNewInputRecord();
                }
            }
        });
    },

    /**
     *  Retrieve path/band pairs from user selection
     */
    __parseInputFiles: function(){
        var paths = [];
        var inputFiles = this.query('#segmentation_input');
        inputFiles.forEach(function(item){
            var input = item.getFileData();
            var classification = item.next('#classification_input').getFileData();
            var classification_band = item.next('#classification_band').getValue();
            if(input !== null){
                paths.push({
                    input:  input['full_path'],
                    classification: classification!==null ? classification['full_path'] : '',
                    classification_band: classification_band
                });
            }
        });
        return paths;
    },

     /**
     *  Retrieve attributes from old segmentation shapefile
     */
    __setAttributes: function(data_from_DB){
        if(!data_from_DB){return}
        var attributes = data_from_DB['attributes'].split(",");
        var attributeField = this.query('#old_segmentation_attribute')[0];
        attributeField.setValue('');
        attributeField.bindStore(attributes);
        attributeField.setDisabled(false);
    },




    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        var inputs = me.__parseInputFiles();
        var import_old = me.parseInputValues('segmentation_useTemplate')==="Yes" ? 'Yes' : 'No';
        var old_shp = me.parseInputValues('old_segmentation_input');
        var old_attrib = me.parseInputValues('old_segmentation_attribute');
        if (import_old === 'No'){
            old_shp = '';
            old_attrib = '';
        }
        console.log(old_shp)
        
        return {
            toolID:             me.toolID,
            inputs:             JSON.stringify(inputs),
            singledate:         me.parseInputValues('multidate')==="Yes" ? 'No' : 'Yes',
            AggregationRules:   me.parseInputValues('AggregationRules'),
            update_shp:         me.parseInputValues('baatz_overwrite'),
            tiling:             me.parseInputValues('tiling'),
            R_img_bands:        me.parseInputValues('R_img_bands'),
            G_img_bands:        me.parseInputValues('G_img_bands'),
            B_img_bands:        me.parseInputValues('B_img_bands'),
            strategy:           me.parseInputValues('strategy'),
            compactness:        me.parseInputValues('compactness'),
            similarity:         me.parseInputValues('similarity'),
            mmu:                me.parseInputValues('mmu'),
            color:              me.parseInputValues('color'),
            suffix_name:        me.parseInputValues('suffix_name'),
            old_shp:            old_shp,
            old_attrib:         old_attrib
        };
    }

});