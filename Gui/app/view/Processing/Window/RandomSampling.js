/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.RandomSampling', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowRandomSampling',
    title: "RandomSampling",
    backgroundColor: "#e5fff4",

    toolID: 'RandomSampling',

    width: 600,

    inEPSG:'',
    maxFeatures:0,
    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,

            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'sampling_input',
                    style: 'margin-top: 10px;',
                    filters: ['vector'],
                    labelWidth: 245,
                    width: 600,
                    fieldLabel: 'Input vector containing AOI or classified objects (strata)',
                    listeners: {
                        'FileInput_change': function(){
                            me.__setAttributes(this.getFileData());
                        }
                    }
                }),
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-top: 20px;',
                    //bodyStyle: "backgroundColor:  '#fffff4';",
                    items: [

                            // ######  Overwrite  ######
                            Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                                fieldLabel: 'Use <b>strata </b> ',
                                radioName: 'sampling_strata',
                                style: 'margin-left: 5px;margin-top: 25px;',
                                labelWidth: 80,
                                width: 200,
                                defaultValue: 'No',
                                listeners: {
                                            change: function(obj, newValue){
                                                if(Object.values(newValue)[0] == 'Yes'){
                                                    this.up('#ProcessingWindowRandomSampling').down('#sampling_minPoint').enable();
                                                } else{
                                                    this.up('#ProcessingWindowRandomSampling').down('#sampling_minPoint').disable();
                                                }
                                            }
                                }

                            }),


                            Ext.create('IMPACT.view.component.Field.Combo',{
                                fieldLabel: 'Attribute field for strata / exclusion list',
                                labelAlign: 'top',
                                itemId: 'sampling_attribute',
                                labelWidth: 225,
                                width: 210,
                                store: [],
                                allowBlank: true,
                                //disabled: true
                                validator: function(value){
                                    if( value == '-' && !this.up('#ProcessingWindowRandomSampling').down('#sampling_exclusion').getValue() == ''){
                                        return 'This field is required';
                                    }
                                    var radioVal = this.up('#ProcessingWindowRandomSampling').down('#sampling_strata').getValue();
                                    if( value == '-' && Object.values(radioVal)[0] == 'Yes'){
                                        return 'This field is required for stratification';
                                    }

                                    return true
                                }
                            }),
                            Ext.create('IMPACT.view.component.Field.Text',{
                                itemId: 'sampling_exclusion',
                                fieldLabel: 'Exclusion list (e.g. 0,99)',
                                labelAlign: 'top',
                                hideLabel: false,
                                labelWidth: 150,
                                width: 140,
                                style: 'margin-left: 10px;',
                                value: '',
                                //disabled: true,
                                inputAttrTpl: " data-qtip='Strata to be excluded (e.g. 0,99)' ",
                            }),
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-left: 35px;margin-top: 35px;',
                   // height: 80,
                    items: [
                            // ######  Output name  ######
                            // ######  Overwrite  ######
                            Ext.create('IMPACT.view.component.Field.Text',{
                                    itemId: 'sampling_TotPoint',
                                    fieldLabel: 'Desired <b>total</b> number of point/objects',
                                    //style: 'margin-top:5px;',
                                    //labelAlign: 'bottom',
                                    labelWidth: 170,
                                    width: 230,
                                    value: 1000,
                                    allowBlank: false,
                                    regex: new RegExp(/^[0-9]+$/),
                                    validator: function(value){

                                        var maxFeatures = this.up('#ProcessingWindowRandomSampling').maxFeatures;
                                        var radioVal = this.up('#ProcessingWindowRandomSampling').down('#sampling_object_aoi').getValue();
                                        if( value > maxFeatures && Object.values(radioVal)[0] == 'Yes'){
                                            return 'Number should be equal/smaller than the number of total objects in input file';
                                        }
                                        return true

                                    }
                            }),
                            Ext.create('IMPACT.view.component.Field.Text',{
                                    itemId: 'sampling_minPoint',
                                    fieldLabel: 'Minimum number of point/object per strata',
                                    style: 'margin-left: 35px;',
                                    //labelAlign: 'bottom',
                                    disabled: true,
                                    labelWidth: 150,
                                    width: 200,
                                    value: 10,
                                    allowBlank: false,
                                    regex: new RegExp(/^[0-9]+$/)
                            }),
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-left: 5px;margin-top: 20px;',
                    //height: 90,
                    items: [
                            // ######  Output name  ######
                            // ######  Overwrite  ######
                            Ext.create('IMPACT.view.component.Field.RadioYesNo', {

                                fieldLabel: 'If the input vector contains objects generated from image segmentation (wall-to-wall), return selected objects instead of points?',
                                //labelAlign: 'top',
                                radioName: 'sampling_object_aoi',
                                //style: 'margin-top: 15px;',
                                labelWidth: 200,
                                height: 100,
                                width: 350,
                                defaultValue: 'No',
                                listeners: {
                                            change: function(obj, newValue){
                                                if(Object.values(newValue)[0] == 'No'){
                                                    this.up('#ProcessingWindowRandomSampling').down('#sampling_buffer').enable();
                                                } else{
                                                    this.up('#ProcessingWindowRandomSampling').down('#sampling_buffer').disable();
                                                    var maxFeatures = this.up('#ProcessingWindowRandomSampling').maxFeatures;
                                                    var totObj = this.up('#ProcessingWindowRandomSampling').down('#sampling_TotPoint').getValue();
                                                    if (totObj > maxFeatures){
                                                        Ext.Msg.show({
                                                            title: 'Invalid number of objects',
                                                            msg:'Number of objects should be equal/smaller than the total objects in input file ('+maxFeatures.toString()+')',
                                                            buttonText: {
                                                                ok: 'Ok',
                                                            }
                                                        });

                                                        this.up('#ProcessingWindowRandomSampling').down('#sampling_TotPoint').setValue(maxFeatures);
                                                    }


                                                }
                                            }
                                }
                            }),


                            Ext.create('IMPACT.view.component.Field.FloatAsText',{
                                    itemId: 'sampling_buffer',
                                    fieldLabel: 'Add square buffer (m)',
                                    labelAlign: 'bottom',
                                    style: 'margin-top: 5px;',
                                    width: 200,
                                    labelWidth: 150,
                                    value: 0,
                                    //disabled: true,
                                    allowBlank: false,
                                    regex: new RegExp(/^[0-9.]+$/)
                                    //inputAttrTpl: " data-qtip=' Set pixel size X value, default = As input ' ",


                            })
                    ]
                },

                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-left: 5px;margin-top: 5px;',
                    height:50,
                    items: [
                            // ######  Output name  ######
                            Ext.create('IMPACT.view.component.Field.Text',{
                                itemId: 'sampling_out_name',
                                //style: 'margin-left: 5px;',
                                fieldLabel: '<b>Output suffix </b>',
                                labelWidth: 120,
                                width: 300,
                                allowBlank: false,
                                regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                            }),
                            // ######  Overwrite  ######
                            Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                                style: 'margin-left: 30px;',
                                labelWidth: 100,
                                width: 220,
                                fieldLabel: '<b>Overwrite</b> Output',
                                //labelAlign: 'top',
                                radioName: 'sampling_overwrite',
                                defaultValue: 'No'
                            })
                    ]
                },


            ]
        });
        return items;
    },

    __setAttributes: function(data_from_DB){
        if(!data_from_DB){return}

        this.inEPSG=data_from_DB['EPSG'];
        this.maxFeatures=data_from_DB['numFeatures'];
        var attributes = data_from_DB['attributes'].split(",");

        attributes.unshift('-');

        var attributeField = this.query('#sampling_attribute')[0];
        attributeField.setValue('-');
        attributeField.bindStore(attributes);
        attributeField.setDisabled(false);

    },




    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        return {
            toolID:           me.toolID,

            inShp:            me.parseInputValues('sampling_input'),
            inEPSG:           me.inEPSG,

            strata:           me.parseInputValues('sampling_strata'),
            classField:       me.parseInputValues('sampling_attribute'),
            exclusion:        me.parseInputValues('sampling_exclusion'),

            totalPoint:       me.parseInputValues('sampling_TotPoint'),
            minPoint:         me.parseInputValues('sampling_minPoint'),

            outObject:        me.parseInputValues('sampling_object_aoi'),
            buffer:           me.parseInputValues('sampling_buffer'),

            overwrite:        me.parseInputValues('sampling_overwrite'),
            out_name:         me.parseInputValues('sampling_out_name')
        };
    }

});
