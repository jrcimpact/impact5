/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.RasterConversion', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowRasterConversion',
    title: "Raster Conversion",

    toolID: 'RasterConversion',

     /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            title: 'Input rasters',
            scroll: true,
            items: [
                // #### Input Rasters ####
                Ext.create('IMPACT.view.component.Field.FileByType', {
                    itemId: 'raster_inputs',
                    selectionMode: 'multiple',
                    fieldLabel: 'Input images',
                    fileType: 'gdal_rasters',
                    data_path: 'IMPACT_DATA_FOLDER' //IMPACT.GLOBALS['data_paths']['data']
                }),
                // #### Format info ####
                {
                    xtype: 'button',
                    text: 'Supported Input Formats',
                    iconCls: 'fa fa-lg fa-blue fa-info-circle',
                    iconAlign: 'left',
                    style: 'margin-left: 270px;',
                    cls: 'impact-icon-button',
                    listeners: {
                        click: function () {
                            if (Ext.ComponentQuery.query('#GDALFormatsWindow').length === 0) {
                                Ext.create('IMPACT.view.component.GDALFormatsWindow');
                            }
                        }
                    }
                }
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            title: 'Conversion options',
            items: [
                // #### OUTPUT FORMAT ####
                {
                    xtype: 'radiogroup',
                    fieldLabel: '<b>Output Format</b>',
                    itemId: 'outFormat',
                    labelWidth: 110,
                    columns: 2,
                    width: 395,
                    style: 'margin-left: 3px; ',
                    vertical: false,
                    disabled: false,
                    items: [
                         {boxLabel: "GTiff (.tiff)",         name: "format",    inputValue: 'GTiff', checked: true },
                         {boxLabel: "ESRI Shapefile (.shp)", name: "format",    inputValue: 'ESRI Shapefile'},
                         {boxLabel: "JP2OpenJPEG (.jp2) ",   name: "format",    inputValue: 'JP2OpenJPEG'},
                         {boxLabel: "KML",                   name: "format",    inputValue: 'KML'},
                         {boxLabel: "JPEG (.jpg)",           name: "format",    inputValue: 'JPEG'},
                         {boxLabel: "GML",                   name: "format",    inputValue: 'GML'},
                         {boxLabel: "PNG (.png)",            name: "format",    inputValue: 'PNG'}
                    ],
                    listeners: {
                        change: function(item, newValue) {
                            // Enable band selection
                            if(newValue.format === 'ESRI Shapefile'
                                    || newValue.format === 'KML'
                                    || newValue.format === 'GML' ){
                                me.down('#bandsCheck').setValue(true);
                            } else {
                                me.down('#bands').setValue('');
                                me.down('#bandsCheck').setValue(false);
                            }
                            // Show messages
                            me.down('#JPEG_info').hide();
                            me.down('#JP2OpenJPEG_info').hide();
                            if(newValue.format === 'JP2OpenJPEG'){
                                me.down('#JP2OpenJPEG_info').show();
                            }
                            if(newValue.format === 'JPEG' || newValue.format === 'GIF' || newValue.format === 'PNG'){
                                me.down('#JPEG_info').show();
                            }
                        }
                    }
                },
                {
                    xtype: 'ProcessingInfo',
                    itemId: 'JP2OpenJPEG_info',
                    hidden: true,
                    html: '<b>JP2OpenJPEG</b> formats supports <i>Byte</i>, <i>Int16</i>, <i>Uint16</i>, <i>Int32</i> or <i>Uint32</i> data type. Please check the log.'
                },
                {
                    xtype: 'ProcessingInfo',
                    itemId: 'JPEG_info',
                    hidden: true,
                    html: 'With <b>PNG</b>, <b>GIF</b> and <b>JPEG</b> formats, <i>Byte</i> data type and <i>Auto</i> rescale to [0 255] will be used.'
                },

                // #### Projection block ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'reprojectCheck',
                            width: 110,
                            boxLabel: 'Reproject',
                            name: 'reproj',
                            inputValue: 'reproj',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#reproject').enable();
                                    } else {
                                        me.down('#reproject').disable().setValue('');
                                    }
                                }
                            }
                        },
                        Ext.create('IMPACT.view.component.EPSGCodesCombo', {
                            itemId: 'reproject',
                            disabled: true,
                            validator: function(value){
                                if(!me.down('#reprojectCheck').getValue() || value!==''){
                                    return true;
                                }
                                return 'This field is required';
                            }
                        })
                    ]
                },

                // #### NO data ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'nodataCheck',
                            width: 110,
                            boxLabel: 'Set Nodata',
                            name: 'nodata',
                            inputValue: 'nodata',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#rconv_nodata').enable();
                                    } else {
                                        me.down('#rconv_nodata').disable().setValue(0);
                                    }
                                }
                            }
                        },
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'rconv_nodata',
                            fieldLabel: 'No data value',
                            hideLabel: true,
                            width: 90,
                            value: 0,
                            disabled: true,
                            inputAttrTpl: " data-qtip=' Set nodata value, default = 0 ' ",
                            validator: function(value){
                                if(!me.down('#nodataCheck').getValue() || value!==''){
                                    return true;
                                }
                                return 'This field is required';
                            }
                        })
                    ]
                },
                // #### Pixel Size ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'pixelSizeCheck',
                            width: 110,
                            boxLabel: 'Set Pixel Size',
                            name: 'psize',
                            inputValue: 'psize',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#pixelSizeX').enable();
                                        me.down('#pixelSizeY').enable();
                                        me.down('#pixelSizeInfo').show();
                                    } else {
                                        me.down('#pixelSizeX').disable().setValue(0);
                                        me.down('#pixelSizeY').disable().setValue(0);
                                        me.down('#pixelSizeInfo').hide();
                                    }
                                }
                            }
                        },
                        Ext.create('IMPACT.view.component.Field.FloatAsText',{
                            itemId: 'pixelSizeX',
                            width: 90,
                            value: 0,
                            disabled: true,
                            inputAttrTpl: " data-qtip=' Set pixel size X value, default = As input ' ",
                            fieldLabel: 'Pixel size X',
                            hideLabel: true,
                            validator: function(value){
                                if(!me.down('#pixelSizeCheck').getValue() || value>0){
                                    return true;
                                }
                                return 'Must be greater the 0';
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.FloatAsText',{
                            itemId: 'pixelSizeY',
                            width: 90,
                            value: 0,
                            disabled: true,
                            style: 'margin-left: 2px;',
                            inputAttrTpl: " data-qtip=' Set pixel size Y value, default = As input ' ",
                            fieldLabel: 'Pixel size Y',
                            hideLabel: true,
                            validator: function(value){
                                if(!me.down('#pixelSizeCheck').getValue() || value>0){
                                    return true;
                                }
                                return 'Must be greater the 0';
                            }
                        }),
                        {
                            itemId: 'pixelSizeInfo',
                            html: '<i>in target PROJ unit</i>',
                            bodyStyle: 'padding-top: 3px; padding-left: 2px;',
                            border: false,
                            hidden: true
                        }
                    ]
                },
                // ####  Set Output data type  ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'outputTypeCheck',
                            width: 110,
                            boxLabel: 'Set Data Type',
                            name: 'outputType',
                            inputValue: 'outputType',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#outputType').enable();
                                    } else {
                                        me.down('#outputType').disable().setValue('Byte');
                                    }
                                }
                            }
                        },
                        Ext.create('IMPACT.view.component.Field.Combo',{
                            itemId: 'outputType',
                            width: 70,
                            store: ['Byte', 'UInt16', 'Float32'],
                            value: 'Byte',
                            disabled: true
                        })
                    ]
                },
                // #### Band Min Max scaling ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'rescaleCheckbox',
                            labelWidth: 110,
                            boxLabel: ' Rescale values',
                            name: 'rescale',
                            inputValue: 'rescale',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#rescaleRadio').enable();
                                    }
                                    else {
                                        me.down('#rescaleRadio').disable().setValue('auto');
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'radiogroup',
                            itemId: 'rescaleRadio',
                            labelWidth: 100,
                            columns: 1,
                            width: 250,
                            vertical: false,
                            disabled: true,
                            items: [
                                { boxLabel: 'Auto scale Min - Max', name: 'click', inputValue: 'auto', checked: true },
                                { boxLabel: 'Use ranges',           name: 'click', inputValue: 'range'}
                            ],
                            listeners: {
                                change: function(item, newValue) {
                                    if(newValue.click==='range'){
                                        me.down('#rescaleRangesContainer').setVisible(true);
                                    } else {
                                        me.down('#rescaleRangesContainer').resetRanges();
                                        me.down('#rescaleRangesContainer').setVisible(false);
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    itemId: 'rescaleRangesContainer',
                    disabled: false,
                    hidden: true,
                    items: [
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'inMin',
                            labelAlign: 'top',
                            fieldLabel: 'IN min',
                            width: 100,
                            value: 0,
                            style: 'margin-left: 5px;'
                        }),
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'inMax',
                            labelAlign: 'top',
                            fieldLabel: 'IN max',
                            width: 100,
                            value: 0,
                            style: 'margin-left: 5px;',
                            validator: function(value){
                                if(me.down('#rescaleCheckbox').getValue()
                                        && me.down('#rescaleRadio').getChecked()[0].inputValue==='range'){
                                    if(value > me.down('#inMin').getValue()){
                                        return true;
                                    }
                                    return 'Must be smaller than "IN min"';
                                }
                                return true;
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'outMin',
                            labelAlign: 'top',
                            fieldLabel: 'OUT min',
                            width: 100,
                            value: 0,
                            style: 'margin-left: 5px;'
                        }),
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'outMax',
                            labelAlign: 'top',
                            fieldLabel: 'OUT max',
                            width: 100,
                            value: 0,
                            style: 'margin-left: 5px;',
                            validator: function(value){
                                if(me.down('#rescaleCheckbox').getValue()
                                        && me.down('#rescaleRadio').getChecked()[0].inputValue==='range'){
                                    if(value > me.down('#outMin').getValue()){
                                        return true;
                                    }
                                    return 'Must be smaller than "OUT min"';
                                }
                                return true;
                            }
                        })
                    ],
                    resetRanges: function(){
                        var ranges = this.query('impactFieldFloat');
                        for(var i in ranges){
                            ranges[i].setValue(0);
                        }
                    }
                },

                // #### Band Selection  ####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            itemId: 'bandsCheck',
                            width: 110,
                            boxLabel: 'Band selection',
                            name: 'bands',
                            inputValue: 'bands',
                            listeners: {
                                buffer: 50,
                                change: function(checkbox, newValue) {
                                    if(newValue===true){
                                        me.down('#bands').enable();
                                        me.down('#bandsInfo').show();
                                    } else {
                                        me.down('#bands').disable();
                                        me.down('#bandsInfo').hide();
                                    }
                                }
                            }
                        },
                        Ext.create('IMPACT.view.component.Field.Text',{
                            itemId: 'bands',
                            fieldLabel: 'Bands',
                            hideLabel: true,
                            width: 170,
                            value: '',
                            disabled: true,
                            inputAttrTpl: " data-qtip=' Select bands, default = As input ' ",
                            validator: function(value){
                                if(!me.down('#bandsCheck').getValue() || value.match(/^(\d{1,2},)*\d{1,2}$/g)){
                                    var format = me.down('#outFormat').getValue()['format'];
                                    if(!value.match(/^[0-9]{1,2}$/) && (format === 'KML' || format === 'GML' || format === 'ESRI Shapefile')){
                                        return 'Invalid selection for '+format+' output format: only 1 band allowed.'
                                    }
                                    if(value.split(',').length>3 && (format === 'JPEG' || format === 'PNG' || format === 'GIF')){
                                        return 'Invalid selection for '+format+' output format: only 3 band allowed.'
                                    }
                                    return true;
                                }
                                return 'This field format is not valid';
                            }
                        }),
                        {
                            itemId: 'bandsInfo',
                            html: '<i>(e.g. 1,3,5)</i>',
                            bodyStyle: 'padding-top: 3px; padding-left: 3px;',
                            border: false,
                            hidden: true
                        }
                    ]
                },

                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_name',
                    fieldLabel: 'Output Suffix',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 110,
                    width: 350,
                    allowBlank: false
                }),

                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    labelStyle: 'font-weight: bold;',
                    defaultValue: 'No',
                    radioName: 'rconv_overwrite'
                })
            ]
        });

        return items;
    },

    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        var rescale = '';
        if (me.down('#rescaleCheckbox').getValue()){
            if (me.down('#rescaleRadio').getChecked()[0].inputValue === 'auto'){
                rescale = 'auto';
            }
            if (me.down('#rescaleRadio').getChecked()[0].inputValue === 'range'){
                rescale = String(me.parseInputValues('inMin')) + ','
                            + String(me.parseInputValues('inMax')) + ','
                            + String(me.parseInputValues('outMin')) + ','
                            + String(me.parseInputValues('outMax'))
            }
        }
        var psizeX = me.parseInputValues('pixelSizeX');
        var psizeY = me.parseInputValues('pixelSizeY');
        var psize = psizeX+','+psizeY;
        if( psize == ","){ psize = ''; }

        return {
            toolID:         me.toolID,
            img_names:      me.parseInputValues('raster_inputs'),
            format:         me.parseInputValues('outFormat'),
            projection:     me.down('#reprojectCheck').getValue()   ? me.parseInputValues('reproject')      : '',
            nodata:         me.down('#nodataCheck').getValue()      ? me.parseInputValues('rconv_nodata')         : '',
            psize:          me.down('#pixelSizeCheck').getValue()   ? psize                                 : '',
            type:           me.down('#outputTypeCheck').getValue()  ? me.parseInputValues('outputType')     : '',
            rescale:        rescale,
            bands:          me.down('#bandsCheck').getValue()       ? me.parseInputValues('bands')          : '',
            out_name:          me.parseInputValues('out_name'),
            overwrite:         me.parseInputValues('rconv_overwrite')
        };
    }

});