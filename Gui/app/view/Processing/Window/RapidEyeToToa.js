/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.RapidEyeToToa', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowRapidEyeToToa',
    title: "RapidEye Dn to TOA Reflectance GeoTiff",
    backgroundColor: "#ffffcc",

    toolID: 'rapideye_to_toa',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input RAW images',
            items: [
                Ext.create('IMPACT.view.component.Field.FileByType', {
                    itemId: 're_img_names',
                    selectionMode: 'multiple',
                    fieldLabel: 'Input RAW images',
                    fileType: 'rapideye',
                    data_path: ((IMPACT.GLOBALS.OS == "unix") ? IMPACT.GLOBALS.data_paths.data : '') // browse from disk

                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    defaultValue: 'No',
                    radioName: 're_overwrite',
                    labelStyle: 'font-weight: bold;'
                })
            ]
        });

        return items;
    },

    launchProcessing: function(){
        var me = this;

        return {
            toolID:     me.toolID,
            overwrite:  me.parseInputValues('re_overwrite'),
            img_names:  me.parseInputValues('re_img_names'),
            outdir:     IMPACT.GLOBALS['data_paths']['data']
        };
    }

});