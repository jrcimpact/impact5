/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Sentinel2ToToa', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowSentinel2ToToa',
    title: "Sentinel2 (Zip) to Reflectance GeoTiff",
    backgroundColor: "#ffffcc",

    toolID: 'sentinel2_to_toa',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input RAW images',
            items: [
                Ext.create('IMPACT.view.component.Field.FileByType', {
                    itemId: 's2_img_names',
                    selectionMode: 'multiple',
                    fieldLabel: 'Input RAW images',
                    fileType: 'sentinel2',
                    data_path: ((IMPACT.GLOBALS.OS == "unix") ? IMPACT.GLOBALS.data_paths.data : '') // browse from disk
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                // ### Bands ###
                {
                    xtype: 'ProcessingInfo',
                    html: ' B01 B02 B03 B04 B05 B06 B07 B08 B8A B09 B10 B11 B12'.replace(/\s/g, '&ensp;.'),
                    style: 'margin-left:42px; margin-top:1px; margin-bottom:-1px;'
                },
                {
                    xtype: 'checkboxgroup',
                    itemId: 'bands',
                    fieldLabel: 'Bands',
                    labelWidth: 45,
                    labelStyle: 'font-weight: bold;',
                    width: 450,
                   // labelStyle: 'margin: 20 20 20 20',
                    //boxLabelCls : 'boxLabelCls',
//                    defaults: {
//                        boxLabelAlign: 'none',
//
//                    },
                    items: [
                        { boxLabel: '',  name: 'B01', inputValue: 'B01'},
                        { boxLabel: '',  name: 'B02', inputValue: 'B02', checked: true},
                        { boxLabel: '',  name: 'B03', inputValue: 'B03', checked: true},
                        { boxLabel: '',  name: 'B04', inputValue: 'B04', checked: true},
                        { boxLabel: '',  name: 'B05', inputValue: 'B05' },
                        { boxLabel: '',  name: 'B06', inputValue: 'B06' },
                        { boxLabel: '',  name: 'B07', inputValue: 'B07' },
                        { boxLabel: '',  name: 'B08', inputValue: 'B08', checked: true},
                        { boxLabel: '',  name: 'B8A', inputValue: 'B8A', },
                        { boxLabel: '',  name: 'B09', inputValue: 'B09', },
                        { boxLabel: '',  name: 'B10', inputValue: 'B10', },
                        { boxLabel: '',  name: 'B11', inputValue: 'B11', checked: true},
                        { boxLabel: '',  name: 'B12', inputValue: 'B12', checked: true}
                    ]
                },



                // ### Projections ###
                Ext.create('IMPACT.view.component.Field.Combo',{
                    itemId: 'projection',
                    fieldLabel: 'Projection',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 90,
                    width: 240,
                    store: {
                        xtype: 'store',
                        fields: ['value', 'name'],
                        data : [
                            //{"value":"EPSG:4326", "name":"EPSG:4326"},    available in the list of user defined proj
                            {"value":"UTM", "name":"UTM (most frequent)"},
                            {"value":"user", "name":"user EPSG / ERSI"}
                        ]
                    },
                    displayField: 'name',
                    valueField: 'value',
                    value: "UTM",
                    listeners: {
                        'select': function(combo){
                            me.down('#userProjection').initializeField(combo.getValue());
                            if(combo.getValue() !== 'UTM'){

                                me.down('#resolution').setValue('user');
                                me.down('#userResolution').initializeField('user');
                            }
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.EPSGCodesCombo', {
                    itemId: 'userProjection',
                    labelWidth: 90,
                    width: 350,
                    disabled: true,
                    hidden: true,
                    initializeField: function(projection){
                        if(projection==='user'){
                            this.enable().show();
                        } else {
                            this.setValue('');
                            this.disable().hide();
                        }
                    },
                    validator: function (value) {
                        if(me.parseInputValues('projection')==='user' && value===''){
                            return 'This field is required'
                        }
                        return true;
                    }
                }),
                // ##### Resolution #####
                {
                    xtype: 'container',
                    layout: 'hbox',
                    //style: 'margin-bottom: 5px;',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Combo',{
                            itemId: 'resolution',
                            fieldLabel: 'Resolution',
                            labelStyle: 'font-weight: bold;',
                            labelWidth: 90,
                            width: 210,
                            store: {
                                xtype: 'store',
                                fields: ['value', 'name'],
                                data : [
                                    {"value": "highest", "name": "highest"},
                                    {"value": "lowest", "name": "lowest"},
                                    {"value": "user", "name": "user"}
                                ]
                            },
                            displayField: 'name',
                            valueField: 'value',
                            value: "highest",
                            listeners: {
                                'select': function(combo){
                                    var userResolution = me.down('#userResolution');
                                    var projection = me.down('#projection');
                                    userResolution.initializeField(combo.getValue());
                                }
                            }
                        }),
                        Ext.create('IMPACT.view.component.Field.Float', {
                            itemId: 'userResolution',
                            style: 'margin-left: 15px;',
                            fieldLabel: 'Resolution',
                            hideLabel: true,
                            width: 190,
                            disabled: true,
                            hidden: true,
                            initializeField: function(resolution){
                                var me = this;
                                if(resolution === 'user'){
                                    me.setValue('').enable().show();
                                    //me.emptyText = 'degrees/pixel or meters/pixel';
                                    //me.setValue('degrees/pixel or meters/pixel');
                                } else {
                                    me.setValue('').disable().hide();
                                    me.emptyText = '';
                                }
                                //me.applyEmptyText();
                                //me.reset();
                            },
                            validator: function (value) {
                                if(me.parseInputValues('resolution')==='user' && value===''){
                                    return 'This field is required'
                                }
                                return true;
                            }
                        })
                    ]
                },
                 // ##### Output type #####
                Ext.create('IMPACT.view.component.Field.Combo',{
                    itemId: 'outtype',
                    fieldLabel: 'Output type',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 90,
                    width: 210,
                    store: {
                        xtype: 'store',
                        fields: ['value', 'name'],
                        data : [
                            {"value": "Float32", "name": "Float32 (0-1)"},
                            {"value": "UInt16", "name": "UInt16 (0-10000)"},
                            {"value": "Byte", "name": "Byte (0-255)"}
                        ]
                    },
                    displayField: 'name',
                    valueField: 'value',
                    value: "Byte"
                }),
                // ##### AOI #####
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'aoi',
                    selectionMode: 'single',
                    filters: ['vector'],
                    fieldLabel: 'AOI',
                    allowBlank: true
                }),
                // ##### Output Name #####
                Ext.create('IMPACT.view.component.Field.Combo',{
                    itemId: 'outname',
                    fieldLabel: 'Output name',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 90,
                    width: 200,
                    store: {
                        xtype: 'store',
                        fields: ['value', 'name'],
                        data : [
                            {"value": "Original", "name": "Original"},
                            {"value": "Short", "name": "Short*"}
                        ]
                    },
                    displayField: 'name',
                    valueField: 'value',
                    value: "Original"
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: '* Short = [ID]_[ProcTime]_[Row]_[AcqTimeStart].tif<br />' +
                          'e.g. S2A_MSIL1C_20160222T155750_R018_V20160219T034145.tif'
                },
                // ### Overwrite ###
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'sentinel2_overwrite',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;'
                })
            ]
        });

        return items;
    },

    launchProcessing: function(){
        var me = this;

        var resolution = me.parseInputValues('resolution');
        resolution = (resolution!=='user') ? resolution : me.parseInputValues('userResolution');
        var projection = me.parseInputValues('projection');
        projection = (projection!=='user') ? projection : me.parseInputValues('userProjection');

        return {
            toolID:         me.toolID,
            overwrite:      me.parseInputValues('sentinel2_overwrite'),
            outdir:         IMPACT.GLOBALS['data_paths']['data'],
            bands:          me.parseInputValues('bands'),
            projection:     projection,
            resolution:     resolution,
            outtype:        me.parseInputValues('outtype'),
            aoi:            me.parseInputValues('aoi'),
            outname:        me.parseInputValues('outname'),
            img_names:      me.parseInputValues('s2_img_names')
        };
    }

});


