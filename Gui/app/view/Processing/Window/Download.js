/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Download', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowDownload',
    title: "Copernicus CDSE EO downloader",
    backgroundColor: "#ffffe5",

    toolID: 'sentinelDownload',
    layout: {
        type: 'hbox',
    },
    width: 960,
    height: 640,
    resizable: true,
    maxHeight: 640,
    minHeight: 640,
    maxWidth: 1000,

    map: null,

    logged: false,

    selectedIDS:[],

    dateStart: Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), "Ymd"),
    dateEnd: Ext.Date.format(new Date(), "Ymd"),

    buttons: [
        {
            text: 'Download Selected',
            itemId: 'ProcessingWindowRunId',
            disabled: true,
            handler: function(){
                this.up('.window')._RUN();
            }
        },
        {
            text: 'Close',
            handler: function(){
                this.up('.window').hide();
            }
        }
    ],

    items:[

        {
            xtype: 'panel',
            border: false,
            collapsible: false,
            width: 400,
            height: 640,


            bodyStyle: "background-color:#ffffe5",
            items: [
              {
                    xtype:'ProcessingWindowFileInput',
                    itemId: 'download_shp_names',
                    filters: ['vector'],
                    selectionMode: 'single',
                    labelWidth: 100,
                    width: 350,
                    fieldLabel: 'Area of interest',
                    style: 'margin-top: 10px;'
              },
              {
                    xtype:'impactFieldCombo',
                    itemId: 'downloadSAT',
                    store: ['SENTINEL-2','LANDSAT-9','LANDSAT-8','LANDSAT-7','LANDSAT-5','LANDSAT-4'],  //,'Sentinel-1'
                    fieldLabel: 'Satellite',
                    value: 'SENTINEL-2',
                    labelWidth : 70,
                    width: 250,
                    disabled: false,
                    allowBlank: false,
                    style: 'margin-top: 10px;',
                    listeners : {
                                'change' : function(field, newValue, oldValue) {
                                     if (newValue != 'SENTINEL-2'){
                                        alert('Available on limited areas only');
                                     }
                                }
                            }
                },

                {
                    xtype: 'datefield',
                    //label: 'Start date',
                    labelAlign:'left',
                    itemId: 'download_startDate',
                    name: 'download_startDate',
                    format: 'd/m/Y',
                    fieldLabel: 'Start',
                    labelWidth : 70,
                    width: 200,
                    //height: 20,
                    //border: false,
                    value: Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
                    listeners : {
                                'change' : function(field, newValue, oldValue) {
                                     this.up('#ProcessingWindowDownload').dateStart = Ext.Date.format(newValue, "Ymd")
                                }
                            }
                },
                {
                    xtype: 'datefield',
                    //label: 'End',
                    labelAlign:'left',
                    itemId: 'download_endDate',
                    name: 'download_endDate',
                    format: 'd/m/Y',
                    fieldLabel: 'End',
                    labelWidth : 70,
                    width: 200,
                    //height: 20,
                    //border: false,
                    value: new Date(),
                    listeners : {
                                'change' : function(field, newValue, oldValue) {
                                     this.up('#ProcessingWindowDownload').dateEnd = Ext.Date.format(newValue, "Ymd")
                                }
                            }
                },
                {   xtype: 'impactFieldSlider',
                        fieldLabel: 'Max cloud %',
                        itemId: 'download_Cloud',
                        labelWidth: 100,
                        width: 350,
                        value: 10,
                        maxValue: 100,
                        minValue: 0,
                        increment: 5,
                        decimalPrecision: 0,
                        disable_textField: true,

                },

               {
                    xtype:'impactFieldRadioYesNo',
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'download_overwrite',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280,
                    style: 'margin-top: 10px;'
                },

                {
                 style: 'margin-top: 20px;',
                 html:['</br>Note:</br><b>Out will be saved next to the selected shapefile (AOI)</br></br>']
                },

                {
                            xtype: 'button',
                            itemId: 'download_applyChanges',
                            tooltip: 'Query catalogue and select what to download from the right panel',
                            fieldLabel:'Query catalogue',
                            text: 'Query catalogue',
                            width: 150,
                            labelWidth: 200,
                            style: 'margin-top: 10px; margin-left: 130px;',
                            iconCls: 'fa-lg fa-green fas fa-sync',
                            //cls: 'impact-icon-button',
                            handler: function () {
                                var DownloadWindow = this.up('#ProcessingWindowDownload');
                                DownloadWindow.clearPanel();
                                DownloadWindow.retrieveData();
                            }
                },

                {
                        style: 'margin-top: 20px;',
                        html:['</br><b>Copernicus Explorer credencials</b> </br> If you are not yet registerd please visit the ',
                              '<a href="https://dataspace.copernicus.eu/browser/"> Copernicus (CDSE)</a></br></br>'
                        ]
                },
                {
                        xtype: 'textfield',
                        itemId: 'download_username',
                        fieldLabel: 'Copernicus Username',
                        width: 350,
                        value: '',
                        allowBlank: false
                },
                {
                        xtype: 'textfield',
                        inputType: 'password',
                        itemId: 'download_password',
                        fieldLabel: 'Copernicus Password',
                        width: 350,
                        value: '',
                        allowBlank: false
                },

            ]
        },
        {
            xtype: 'gridpanel',
            title: 'Quicklooks',
            itemId: 'downloadQKLpanel',
            border: false,
            autoScroll: true,
            overflowY: 'scroll',
            collapsible: false,
            width: 560,
            height: 600,
            maxWidth: 600,
            store : Ext.create('Ext.data.Store', {fields: ['Image'],   data: []}),
            columns: [
                        {
                            text: 'Image',
                            //width : 500,
                            flex:1,
                            dataIndex: 'html',
                            html:''
                        },{
                            text: 'Size',
                            width : 80,
                            //flex:1,
                            dataIndex: 'prodSize',
                        }

            ],
            selModel: {
                        checkOnly: false,
                        injectCheckbox: 'last',
                        mode: 'SIMPLE'
            },
            selType: 'checkboxmodel',

           listeners:{
               cellclick: function(checkcolumn){

                       var IDS= Ext.ComponentQuery.query('#downloadQKLpanel')[0].getSelectionModel().getSelection();
                       if(IDS.length > 0){
                            Ext.ComponentQuery.query('#ProcessingWindowRunId')[0].setDisabled(false);
                       } else {
                            Ext.ComponentQuery.query('#ProcessingWindowRunId')[0].setDisabled(true);
                       }
                    }

           }
        }

     ],


    clearPanel: function(){
        var me = this;
        me.down('#downloadQKLpanel').store.removeAll();

    },


   retrieveData: function(){
     var me = this;
     me.populate_quicklooks();

    },


    populate_quicklooks: function(){
        var me = this;
        var url = IMPACT.GLOBALS['url']+"/getESAquicklooks.py?";
            url += "&shp_names=" +me.parseInputValues('download_shp_names');
            url += "&dateStart=" + me.dateStart;
            url += "&dateEnd=" + me.dateEnd;
            url += "&clouds=" + me.parseInputValues('download_Cloud');
            url += "&sat=" + me.parseInputValues('downloadSAT');
//            url += "&usr=" + me.parseInputValues('download_username');
//            url += "&pwd=" + me.parseInputValues('download_password');



        Ext.Ajax.request({
                timeout: 300*1000,
                cors: true,
                useDefaultXhrHeader: false,
                withCredentials: true,
                url : url,
                success : function(response) {
                    responseTxt = JSON.parse(response.responseText);
                    var success = responseTxt.success;
                    var mex = responseTxt.mex;
                    if (success == 'False'){
                        alert(mex);
                        return false;

                    }


                    var jpegs = responseTxt.jpegs;
                    var titles = responseTxt.titles;
                    var uuids = responseTxt.uuids;
                    var prodSize = responseTxt.prodSize;
//                    if (me.logged == false){
//                                       Ext.Ajax.request({
//                                            method: "GET",
//                                            //async: false,
//                                            cors: true,
//                                            useDefaultXhrHeader: false,
//                                            withCredentials: true,
//                                            url : jpegs[0],
//                                            success : function(response) {
//                                                me.logged = true;
//                                                me.down('#downloadQKLpanel').getView().refresh();
//                                            },
//                                            failure: function(response) {
//                                                me.logged = true;
//                                                me.down('#downloadQKLpanel').getView().refresh();
//                                            }
//                                        })
//                                        me.logged = true;
//
//                    }

                    //var newurl=responseTxt.jpegs[i].replace('https://apihub','https://'+me.parseInputValues('download_username')+':'+me.parseInputValues('download_password')+'@apihub');
                    // passing user+pwd does not work !
                    for (i in jpegs){
                                      me.down('#downloadQKLpanel').store.loadData([{html:'<img style="border:4px solid light-blue; margin-left:5px" src="'+responseTxt.jpegs[i]+'" width=400  title="'+titles[i]+'"/>',
                                                                          prodSize:prodSize[i],
                                                                          uuid:uuids[i],
                                                                          name:titles[i]}]
                                                                          ,true);
                    }


                },
                failure: function(response) {
                    console.log('Error while downloading info from CDSE: ', response.responseText)
                    alert('Error retrieving data: ' + response.responseText);
                }
            })
    },





    launchProcessing: function(){

       var me = this;
       if(me.parseInputValues('download_shp_names') == '[]' ){
            alert('Please select the AOI');
            return
        };
        if(me.parseInputValues('download_username') == '' || me.parseInputValues('download_password')== ''){
            alert('Please insert your Copernicus Explorer credentials in the interface');
            return
        };


        var records= me.down('#downloadQKLpanel').getSelectionModel().getSelection();
        var UUIDs=[];
        var names=[];
        for (i in records){
            UUIDs.push(records[i].data.uuid);
            names.push(records[i].data.name);
        }

       var outpath=me.down('#download_shp_names').getValues()[0];

       return {
            toolID:       me.toolID,
            uuids:        JSON.stringify(UUIDs),
            names:        JSON.stringify(names),
            outpath:      outpath,
            usr:          encodeURIComponent(me.parseInputValues('download_username')),
            pwd:          encodeURIComponent(me.parseInputValues('download_password')),
            overwrite: me.parseInputValues('download_overwrite')

       };

    }

});