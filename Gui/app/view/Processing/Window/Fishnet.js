/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Fishnet', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowFishnet',
    title: "Create Fishnet",

    toolID: 'fishnet',

    itemsUI: function(){
        var me = this;
        var items = [];

         items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            items: [
                // ## Use Template ? ##
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Use template for extent</b>',
                    radioName: 'fishnet_useTemplate',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280,
                    listeners : {
                        change : function(radio, newValue) {
                            var useTemplate = newValue['fishnet_useTemplate_val']==="Yes";
                            me.setExtent(useTemplate);
                        }
                    }
                }),
                // ## Template input file ##
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'template',
                    style: 'margin-bottom: 3px;',
                    filters: ['vector', 'raster', 'class'],
                    fieldLabel: 'Template image',
                    labelWidth: 110,
                    listeners: {
                        'FileInput_change': function(){
                            me.getLayerBBOX(this.getFileData());
                        }
                    },
                    disabled: true,
                    allowBlank: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('fishnet_useTemplate')==='Yes'){
                            return 'This field is required';
                        }
                        return true;
                    }
                }),
                // ## Extent  ##
                { xtype: 'panel',
                  border: false,
                  collapsible : true,
                  title: 'Extent',
                  items:[
//                        {
//                            xtype: 'ProcessingInfo',
//                            html: 'Extent: ',
//                            itemId: 'extent_label',
//                            border: false,
//                            style: 'font-weight: bold;'
//                        },
                        // ## Extent ##
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'extent_top',
                            style: 'margin-left: 130px;',
                            labelAlign: 'top',
                            labelWidth: 50,
                            width: 150,
                            fieldLabel: 'Upper latitude coordinate',
                            validator: function(value){
                                if(value==='' && me.parseInputValues('fishnet_useTemplate')==='No'){
                                    return 'This field is required';
                                }
                                return true;
                            }
                        }),
                        {
                            xtype: 'container',
                            border: false,
                            layout: 'hbox',
                            items: [
                                Ext.create('IMPACT.view.component.Field.Float',{
                                    itemId: 'extent_left',
                                    labelAlign: 'top',
                                    labelWidth: 50,
                                    width: 150,
                                    fieldLabel: 'Left longitude coordinate',
                                    validator: function(value){
                                        if(value==='' && me.parseInputValues('fishnet_useTemplate')==='No'){
                                            return 'This field is required';
                                        }
                                        return true;
                                    }
                                }),
                                Ext.create('IMPACT.view.component.Field.Float',{
                                    itemId: 'extent_right',
                                    labelAlign: 'top',
                                    labelWidth: 50,
                                    width: 150,
                                    fieldLabel: 'Right longitude coordinate',
                                    style: 'margin-left: 110px;'
                                })
                            ]
                        },
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'extent_bottom',
                            style: 'margin-left: 130px;',
                            labelAlign: 'top',
                            labelWidth: 50,
                            width: 150,
                            fieldLabel: 'Lower latitude coordinate'
                        })
                  ]
                },

                // ## EPSG ##
                Ext.create('IMPACT.view.component.EPSGCodesCombo', {
                    itemId: 'epsg',
                    inputAttrTpl: " data-qtip=' Projection of output file and input coordinates' ",
                    allowBlank: false,
                    style: 'margin-top: 5px;',

                }),
                // ## Projection ##
                {
                    xtype: 'textarea',
                    itemId: 'proj4',
                    fieldLabel: 'PROJ4',
                    labelAlign: 'left',
                    labelWidth: 50,
                    value: '',
                    width: 410,
                    height: 30,
                    disabled: true,
                    rows: 2
                },
                // ## Units ##
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'units',
                    fieldLabel: 'Units',
                    disabled: true
                }),
                // ######  Cell Size  ######
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'cell_size_width',
                    fieldLabel: 'Cell size Width',
                    labelWidth: 120,
                    validator: function(value){
                        if(value==='' && me.down('#cell_size_height').getValue()!==null){
                            return 'The field is required.'
                        }
                        if(value==='' && me.down('#num_columns').getValue()===null){
                            return 'The field or "Number of Columns" is required.'
                        }
                        return true;
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'cell_size_height',
                    fieldLabel: 'Cell size Height',
                    labelWidth: 120,
                    validator: function(value){
                        if(value==='' && me.down('#cell_size_width').getValue()!==null){
                            return 'The field is required.'
                        }
                        return true;
                    }
                }),
                // ######  Number of cells  ######
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'num_rows',
                    fieldLabel: 'Number of Rows',
                    labelWidth: 120,
                    validator: function(value){
                        if(value==='' && me.down('#num_columns').getValue()!==null){
                            return 'The field is required.'
                        }
                        if(value==='' && me.down('#cell_size_width').getValue()===null){
                            return 'The field or "Cell size Width" is required.'
                        }
                        return true;
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'num_columns',
                    fieldLabel: 'Number of Columns',
                    labelWidth: 120,
                    validator: function(value){
                        if(value==='' && me.down('#num_rows').getValue()!==null){
                            return 'The field is required.'
                        }
                        return true;
                    }
                }),
                // ######  Step  ######
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'step_x',
                    fieldLabel: 'Step X',
                    labelWidth: 50,
                    width: 200,
                    inputAttrTpl: " data-qtip=' Longitude gap between 2 cells' "
                }),
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'step_y',
                    fieldLabel: 'Step Y',
                    labelWidth: 50,
                    width: 200,
                    inputAttrTpl: " data-qtip=' Latitude gap between 2 cells' "
                }),
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'randomVal',
                    fieldLabel: 'Select random percentage (<b>%</b>) of total samples',
                    labelWidth: 270,
                    value: 100,
                    width: 320,
                    validator: function(value){
                        if(value>=0 && value<=100){
                            return true;
                        }
                        return 'The field is invalid: must be between 0 and 100.';
                    }
                }),
                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_name',
                    fieldLabel: 'Output Name',
                    labelStyle: 'font-weight: bold;',
                    width: 350,
                    allowBlank: false
                }),
                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;',
                    radioName: 'fishnet_overwrite'
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: '- Right and Bottom can be omitted if Rows and Columns are defined<br />' +
                          '- Width and Height can be omitted if Right and Bottom are defined'
                }
            ]
        });

        return items;
    },

    setExtent: function(useTemplate){
        var me = this;

        // Set input selector visible
        var input = me.down('#template');
        input.reset();
        input.setDisabled(!useTemplate);

        // Set extent's fields editability
        //me.down('#extent_label').setDisabled(useTemplate);
        me.down('#extent_left').setDisabled(useTemplate);
        me.down('#extent_right').setDisabled(useTemplate);
        me.down('#extent_bottom').setDisabled(useTemplate);
        me.down('#extent_top').setDisabled(useTemplate);
        me.down('#epsg').setDisabled(useTemplate);
        // Empty extent's fields
        me.down('#extent_left').setValue('');
        me.down('#extent_right').setValue('');
        me.down('#extent_bottom').setValue('');
        me.down('#extent_top').setValue('');
        me.down('#epsg').setValue('');
        me.down('#proj4').setValue('');
        me.down('#units').setValue('');
    },

    getLayerBBOX: function(data){
        if(data!==null){
            console.log(data);
            var me = this;
            var extent = JSON.parse(data['extent']);
            me.down('#extent_left').setValue(extent.W);
            me.down('#extent_right').setValue(extent.E);
            me.down('#extent_bottom').setValue(extent.S);
            me.down('#extent_top').setValue(extent.N);
            me.down('#epsg').setValue(data['EPSG'].toString());
            me.down('#units').setValue( IMPACT.Utilities.Impact.getReadableUnits(data) );
            me.down('#proj4').setValue(data['PROJ4']);
        }
    },

    launchProcessing: function(){
        var me = this;
        return {
            toolID:             me.toolID,
            template:           me.parseInputValues('template'),
            extent_top:         me.parseInputValues('extent_top'),
            extent_left:        me.parseInputValues('extent_left'),
            extent_right:       me.parseInputValues('extent_right'),
            extent_bottom:      me.parseInputValues('extent_bottom'),
            epsg:               me.parseInputValues('epsg'),
            cell_size_width:    me.parseInputValues('cell_size_width'),
            cell_size_height:   me.parseInputValues('cell_size_height'),
            num_columns:        me.parseInputValues('num_columns'),
            num_rows:           me.parseInputValues('num_rows'),
            step_x:             me.parseInputValues('step_x'),
            step_y:             me.parseInputValues('step_y'),
            randomVal:          me.parseInputValues('randomVal'),
            output_name:        me.parseInputValues('output_name'),
            overwrite:          me.parseInputValues('fishnet_overwrite')
        };
    }

});