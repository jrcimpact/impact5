/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.LandsatToToa', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowLandsatToToa',
    title: "Landsat (Zip) to Reflectance GeoTiff",
    backgroundColor: "#ffffcc",

    toolID: 'landsat_to_toa',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input RAW images',
            items: [
                Ext.create('IMPACT.view.component.Field.FileByType', {
                    itemId: 'landsat_img_names',
                    selectionMode: 'multiple',
                    fieldLabel: 'Input RAW images',
                    fileType: 'landsat',
                    data_path: ((IMPACT.GLOBALS.OS == "unix") ? IMPACT.GLOBALS.data_paths.data : '') // browse from disk
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                {
                    xtype: 'ProcessingInfo',
                    html: ' Only B,G,R,NIR,SWIR1,SWIR2 bads are processed',
                    style: 'font-weight: bold; margin-left:45px; margin-bottom:10px;'
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Processing Type',
                    labelWidth: 140,
                    labelStyle: 'font-weight: bold;',
                    columns: 1,
                    itemId: 'proc_mode',
                    items: [

                        {
                            xtype: 'radiofield',
                            boxLabel: 'Keep values (already SR or TOA Reflectance)',
                            name: 'DN2TOA',
                            inputValue: 'DN',
                            checked: true,
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'DN to TOA Reflectance',
                            name: 'DN2TOA',
                            inputValue: 'DN2TOA'
                        }
                    ],
                    listeners : {
                                change : function(radio, newValue) {
                                    if(newValue.DN2TOA == 'DN'){
                                        this.up('#ProcessingWindowLandsatToToa').down('#date_mode').items.items[0].setValue(true);
                                        this.up('#ProcessingWindowLandsatToToa').down('#date_mode').items.items[1].setDisabled(true);
                                        this.up('#ProcessingWindowLandsatToToa').down('#date_mode').items.items[2].setDisabled(true);
                                    }else {
                                        this.up('#ProcessingWindowLandsatToToa').down('#date_mode').items.items[1].setDisabled(false);
                                        this.up('#ProcessingWindowLandsatToToa').down('#date_mode').items.items[2].setDisabled(false);
                                    }

                                    }
                                }
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Options',
                    labelWidth: 140,
                    labelStyle: 'font-weight: bold;',
                    columns: 1,
                    itemId: 'proc_opt',
                    items: [
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Keep original Uint16 [0-65000]',
                            name: 'TOA',
                            checked: true,
                            inputValue: 'TOAi'
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Rescale to Byte [0-255]',
                            name: 'TOA',
                            inputValue: 'TOAb'
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Rescale to Float [0-1]',
                            name: 'TOA',
                            inputValue: 'TOAf'
                        }
                    ]

                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Output name format',
                    labelWidth: 140,
                    labelStyle: 'font-weight: bold;',
                    columns: 1,
                    itemId: 'date_mode',
                    items: [
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Input Format',
                            name: 'date',
                            inputValue: 'ORIGINAL',
                            checked: true,
                            disabled: false
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Sensor Path Row YYYYMMDD',
                            name: 'date',
                            inputValue: 'YYYYMMDD',
                            disabled: true
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'Sensor Path Row DDMMYYYY',
                            name: 'date',
                            inputValue: 'DDMMYYYY',
                            disabled: true
                        }
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    html: 'Note: <br />'+
                          '<b>Surface Reflectance Collection 1 and 2 </b> are automatically rescaled during the layerstak and forced to Float32. <br />'+
                          'C1 reflectance rescaling factor = 0.0001<br />'+
                          'C2 reflectance rescaling factor = 0.0000275 + -0.2<br />'+
                          'Please visit the USGS <a target="_blank" href="https://www.usgs.gov/faqs/how-do-i-use-scale-factor-landsat-level-2-science-products">page.</a><br />'+
                          ' Use the Raster Calculator tool to converto to byte or other data format.'
                },


                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'landsat_overwrite',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;'
                })
            ]
        });

        return items;
    },



    launchProcessing: function(){
        var me = this;

        return {
            toolID:     me.toolID,
            outdir:     IMPACT.GLOBALS['data_paths']['data'],
            proc_mode:  me.parseInputValues('proc_mode'),
            proc_opt:  me.parseInputValues('proc_opt'),
            date_mode:  me.parseInputValues('date_mode'),
            overwrite:  me.parseInputValues('landsat_overwrite'),
            img_names:  me.parseInputValues('landsat_img_names')
        };
    }

});


