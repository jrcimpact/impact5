/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Unmixing', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowUnmixing',
    title: "Unmixing Options",
    backgroundColor: "#e5ffdd",

    toolID: 'unmix',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input images',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'input_images',
                    filters: ['raster', 'class'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Input images'
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            title: 'Processing options',
            collapsible: false,
            items: [
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'EVG Forest normalization',
                    radioName: 'unmix_fnorm',
                    defaultValue: 'No',
                    labelWidth: 150,
                    width: 280
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'unmix_overwrite',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 150,
                    width: 280
                })
            ]
        });

        return items;
    },


    launchProcessing: function(){

        var me = this;

        return {
            toolID:     me.toolID,
            overwrite:  me.parseInputValues('unmix_overwrite'),
            fnorm:      me.parseInputValues('unmix_fnorm'),
            img_names:  me.parseInputValues('input_images')
        };
    }

});