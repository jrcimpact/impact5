/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.ConfusionM', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowConfusionM',
    title: "Confusion Matrix",
    backgroundColor: "#e5fff4",

    toolID: 'ConfusionMatrix',

    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'ConfusionM_input',
                    filters: ['vector'],
                    fieldLabel: 'Input vector containing ground truth information',
                    listeners: {
                        'FileInput_change': function(){
                            me.__setAttributes(this.getFileData());
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Select attribute field',
                    itemId: 'ConfusionM_attribute',
                    labelWidth: 140,
                    width: 250,
                    store: [],
                    allowBlank: false,

                }),
                 Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Select weight field (optional)',
                    itemId: 'ConfusionM_weight',
                    labelWidth: 140,
                    width: 250,
                    store: [],
                    allowBlank: true
                }),
                 Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'ConfusionM_rasterVector_map',
                    filters: ['vector','raster','class','virtual'],
                    fieldLabel: 'Vector or raster map to assess',
                    listeners: {
                        'FileInput_change': function(){
                            me.__setRasterVector(this.getFileData());
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Select attribute field',
                    itemId: 'ConfusionM_attribute_shp',
                    labelWidth: 140,
                    width: 250,
                    store: [],
                    allowBlank: false,
                    disabled: true,
                }),
//                Ext.create('IMPACT.view.component.Field.Float',{
//                            itemId: 'ConfusionM_pixelSize',
//                            labelWidth: 250,
//                            fieldLabel: 'Computation at Spatial Resolution of (same Unit dd/m as input map)',
//                            //labelAlign: 'right',
//                            width: 350,
//                            disabled: true,
//                            allowBlank: true,
//                            validator: function(value){
//                                if(value>0) return true;
//                                return 'Must be greater the 0'
//                            }
//                }),

                Ext.create('IMPACT.view.component.Field.Combo', {
                    itemId: 'ConfusionM_attribute_tif',
                    fieldLabel: 'Select Map Band',
                    labelWidth: 70,
                    width: 150,
                    store: [''],
                    disabled: true,
                    labelStyle: 'padding-left: 10px;',
                    allowBlank: true

                }),

                              // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'ConfusionM_NoClass',
                    fieldLabel: 'Exclude classes (list: 0,1,99)',
                    labelWidth: 200,
                    width: 400,
                    allowBlank: true,
                    regex: new RegExp(/^[0-9,]+$/)
                }),


                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'ConfusionM_out_name',
                    fieldLabel: '<b>Output filename</b>',
                    labelWidth: 90,
                    width: 400,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'ConfusionM_overwrite',
                    defaultValue: 'No'
                })
            ]
        });
        return items;
    },

    __setAttributes: function(data_from_DB){
        if(!data_from_DB){return}
        var attributes = data_from_DB['attributes'].split(",");

        var attributeField = this.query('#ConfusionM_attribute')[0];
        attributeField.setValue('');
        attributeField.bindStore(attributes);
        attributeField.setDisabled(false);

        var weightField = this.query('#ConfusionM_weight')[0];
        weightField.setValue('');
        var tmp = attributes.splice(0,0,"-");
        weightField.bindStore(attributes);
        weightField.setDisabled(false);
    },


    __setRasterVector:function(data_from_DB){
         //console.log(data_from_DB);
         if(data_from_DB['extension'] == '.shp'){
            this.query('#ConfusionM_attribute_tif')[0].setDisabled(true);
            this.query('#ConfusionM_attribute_shp')[0].setDisabled(false);
            //this.query('#ConfusionM_pixelSize')[0].setDisabled(false);
            var attributes = data_from_DB['attributes'].split(",");
            var attributeField = this.query('#ConfusionM_attribute_shp')[0];
            attributeField.setValue('');
            attributeField.bindStore(attributes);

         } else {
            this.query('#ConfusionM_attribute_tif')[0].setDisabled(false);
            this.query('#ConfusionM_attribute_shp')[0].setDisabled(true);
            //this.query('#ConfusionM_pixelSize')[0].setDisabled(true);
            var bands = [];
            for(var i=1; i<=data_from_DB['num_bands']; i++){
                bands.push(i);
            }
            var attributeField = this.query('#ConfusionM_attribute_tif')[0];
            attributeField.setValue('1');
            attributeField.bindStore(bands);
         }
    },

    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        return {
            toolID:           me.toolID,

            inShp:            me.parseInputValues('ConfusionM_input'),
            pct:              null,
            attribute:        me.parseInputValues('ConfusionM_attribute'),
            weight:           me.parseInputValues('ConfusionM_weight'),
            map:              me.parseInputValues('ConfusionM_rasterVector_map'),
            attribTif:        me.parseInputValues('ConfusionM_attribute_tif'),
            attribShp:        me.parseInputValues('ConfusionM_attribute_shp'),
            noClass:          me.parseInputValues('ConfusionM_NoClass'),

            overwrite:        me.parseInputValues('ConfusionM_overwrite'),
            out_name:         me.parseInputValues('ConfusionM_out_name')
        };
    }

});
