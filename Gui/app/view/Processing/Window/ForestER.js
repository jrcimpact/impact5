/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.ForestER', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowForestER',
    title: "Pixel based deforestation & disturbance reporting",
    backgroundColor: "#ffffe5",

    toolID: 'ForestER',


    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            minHeight: 200,
            maxHeight: 600,
            title: 'Inputs',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'mapT1',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Forest Map at Time 1'
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'mapT2',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Forest Map at Time 2'
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'disturbMap',
                    filters: ['raster', 'class','cluster'],
                    selectionMode: 'single',
                    fieldLabel: 'Disturbance Map [0-1]',
                    allowBlank: true
                }),
                 {
                    xtype: 'radiogroup',
                    itemId: 'useConversionMap',
                    fieldLabel: 'Biomass value per forest type ',
                    margin: '8 0 0 0',
                    labelWidth: 180,
                    width: 400,
                    //style: 'margin-bottom: 5px;',
                    items: [
                        {boxLabel: 'map', name: 'useImageGrp', inputValue: 'True', checked: false},
                        {boxLabel: 'constant', name: 'useImageGrp', inputValue: 'False', checked: true}
                    ],
                    listeners: {
                        change: function (field, newValue) {
                            var emission_factor_map = me.down('#emission_factor_map');
                            var emission_factor_map_warning = me.down('#emission_factor_map_warning');
                            if (newValue['useImageGrp'] === 'True') {
                                emission_factor_map.enable().show();
                                emission_factor_map_warning.show();
                                //me.down('#biomass_value').disable().setValue('');
                                var biomasItems = me.query('#recode_value');
                                biomasItems.forEach(function(item){
                                    item.disable();
                                });

                            }
                            else {
                                emission_factor_map.disable().reset();
                                emission_factor_map.hide();
                                emission_factor_map_warning.hide();
                                var biomasItems = me.query('#recode_value');
                                biomasItems.forEach(function(item){
                                    item.enable();
                                });
                            }
                        }
                    }
                },

                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'emission_factor_map',
                    filters: ['raster', 'class'],
                    fieldLabel: 'Biomass map (t/ha)',
                    margin: '8 0 0 0',
                    disabled: true,
                    hidden: true,
                    labelWidth: 140,
                    width: 425,
                    allowBlank: true,
                    validator: function(value){
                        if(value==='' && me.parseInputValues('useConversionMap')==='True'){
                            return 'This field is required';
                        }
                        return true;
                    }
                }),
                {
                    xtype: 'ProcessingInfo',
                    itemId: 'emission_factor_map_warning',
                    type:'warning',
                    hidden: true,
                    height: 80,
                    html:'<b>WARNING</b>:<br /> Emissions are not computed<br />'+
                          'Only average Biomass values per class will be reported<br />'+
                          'Please verify them and re-run ForestER using constant values instead<br />'
//                    html: '<b>WARNING</b>:<br /> - result might not be accurate in presence of more forest types<br />'+
//                                                '- time of the map should be in between the two forest maps<br />'+
//                                                '- the average biomass values are computed for each forest types at Time 1 & 2 separately <br />'+
//                                                '- for the same forest type, derived biomass values could be different in Time 1 & 2 <br />'+
//                                                '- different forest types might have the same biomass value depending on the accuracy/resolution of the map <br />'+
//                                                '- some forest types might not be present in the map (e.g aforestation occurring after the date the biomass map is referring to) '
                },

            ]
        });

        items.push({
            itemId: 'RecodeValuesContainer',
            title: 'Tree cover maps values per forest type and related biomass values',
            bodyStyle: 'padding: 3px 5px;',
            hidden: false,
            maxHeight: 300,
            overflowY: 'scroll',
            overflowX: 'hidden',
            items: [{

                    xtype: 'panel',
                    collapsible: false,
                    border: false,
                    layout:'column',
                    style: 'margin: 5px 0;',
                    items: [
                        {
                            html: 'from [>=]',
                            border: false,
                            width: 75,
                            bodyStyle: 'text-align: center; font-weight: bold;'
                        },
                        {
                            html: 'to [<=]',
                            border: false,
                            width: 75,
                            bodyStyle: 'text-align: center; font-weight: bold;'
                        },
                        {
                            html: 'Estimated biomass (t/ha)',
                            border: false,
                            width: 200,
                            bodyStyle: 'text-align: center; font-weight: bold;'
                        }
                    ]
                    },
                     me.__insert_data_range()

            ]


        });

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
            title: 'Processing options',
            items: [

                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'nodata_value',
                    disabled: false,
                    emptyText : '',
                    fieldLabel: 'No data value (eg. clouds or no observation)',
                    labelWidth : 260,
                    width: 300
                }),

                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'biomassDegradPercent',
                    fieldLabel: 'Weight of disturbance map <small>(% of biomass loss)</small>',
                    labelWidth: 260,
                    width: 300,
                    value: 50.0,
                    disabled: false,
                    validator: function(value){
                        if(value>=0 && value<=100) return true;
                        return 'Must be between 0 and 100'
                    }
                }),
                Ext.create('IMPACT.view.component.EPSGCodesCombo', {
                            itemId: 'reproject',
                            fieldLabel: 'Output Projection',
                            width: 350,
                            labelWidth: 120,
                            initFilter : "Equal",
                            validator: function(value){
                                if(value!==''){
                                    return true;
                                }
                                return 'Only Albers Equal Area projections allowed.';
                            }
                }),
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'output_name',
                    fieldLabel: 'Output report/products prefix',
                    allowBlank: false,
                    labelWidth: 120,
                    width: 350,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'forester_overwrite',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: '<b>NOTES</b>:<br /> - Forest map values: selected values are considered as forest, not mentioned as non forest (except No Data) </br>- only intersection between forest maps is processed </br>- disturbance map (binary 0-1) can be a subset of other maps </br> - all maps are warped to the selected projection using highest pixel size; if input files have different projections result might show artifacts/errors'
                },
            ]
        });

        return items;
    },


     __insert_data_range: function(){
        var me = this;


        if (me.down('#useConversionMap') === null){
            var disabled = false;
        } else {
            var disabled = me.parseInputValues('useConversionMap') === 'True';
        }

        return {
            xtype: 'panel',
            //itemId: 'biomRangeId',
            collapsible: false,
            border: false,
            layout:'column',
            style: 'margin: 5px 0;',
            items: [
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'start_value',
                    width: 75,
                    validator: function(value){
                        if(disabled === false){
                            if(value===''){
                                return 'This field is required';
                            } else if(value > this.up('panel').down('#end_value').value ){
                                //this.up('panel').down('#end_value').validator();
                                return 'Must be <= ' + this.up('panel').down('#end_value').value
                            }
                        }
                        return true;
                    },
                    listeners: {
                        change: function (item,newValue,oldValue) {
                            this.up('panel').down('#end_value').isValid();
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Integer',{
                    itemId: 'end_value',
                    width: 75,
                    validator: function(value){
                        if(disabled === false){



                            if(value===''){
                                return 'This field is required';
                            } else if(value < this.up('panel').down('#start_value').value ){
                                //this.up('panel').down('#start_value').validator();
                                return 'Must be >= ' + this.up('panel').down('#start_value').value
                            }
                        }
                        return true;
                    },
                    listeners: {
                        change: function (item,newValue,oldValue) {
                             this.up('panel').down('#start_value').isValid();
                        }
                    }
                }),
                {
                    html: '&nbsp;-->&nbsp;',
                    border: false,
                    width: 60,
                    bodyStyle: 'text-align: center;'
                },
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'recode_value',
                    width: 75,
                    disabled: disabled,
                    validator: function(value){
                        if(disabled === false){
                            if(value===''){
                                return 'This field is required';
                            } else if(value<0){
                                return 'Must be >= 0'
                            }
                        }
                        return true;
                    }
                }),
                Ext.create('Ext.Button', {
                    itemId: 'delete_button',
                    iconCls: 'fa fa-lg fa-red fa-times-circle',
                    cls: 'impact-icon-button',
                    //style: 'margin-left: 10px;',
                    hidden: true,
                    handler: function() {
                        var itemId = this.up('panel', 1).getId();
                        me.down('#RecodeValuesContainer').remove(itemId);
                    }
                }),
                Ext.create('Ext.Button', {
                    itemId: 'add_button',
                    iconCls: 'fas fa-lg fa-green fa-plus-circle',
                    //style: 'margin-left: 10px;',
                    cls: 'impact-icon-button',
                    handler: function() {
                        this.hide();
                        this.previousSibling('#delete_button').show();
                        me.down('#RecodeValuesContainer').add(
                            me.__insert_data_range()
                        );
                        me.doLayout();
                    }
                }),
            ]
        }
    },
    __parseRecodeValues: function(){
        var me = this;
        var recode_values = []
        var recodeValuesContainer = me.down('#RecodeValuesContainer');

        // Manual recoding

        var index = 0;
        recodeValuesContainer.items.each(function(item){
//          skip pos = 0
            if(index>0){
                var start_value = IMPACT.Utilities.Input.parseField(item.down('#start_value'));
                var end_value = IMPACT.Utilities.Input.parseField(item.down('#end_value'));
                var recode_value = IMPACT.Utilities.Input.parseField(item.down('#recode_value'));
//                if(me.errorMsg.length==0){
//                    if(start_value=='') { me.errorMsg.push('Start value is required') }
//                    if(end_value=='') { me.errorMsg.push('End value is required') }
//                    if(recode_value=='') { me.errorMsg.push('Recode value is required') }
//                }
                recode_values.push([start_value, end_value, recode_value]);
            }
            index++;
        });

        return recode_values;
    },

    launchProcessing: function(){

        var me = this;
        return {
            toolID:                 me.toolID,
            mapT1:                  me.parseInputValues('mapT1'),
            mapT2:                  me.parseInputValues('mapT2'),
            disturbMap:             me.parseInputValues('disturbMap'),
            em_factor_map:          me.parseInputValues('emission_factor_map'),
            biomass_value:          me.__parseRecodeValues(),
            biomassDegradPercent:   me.parseInputValues('biomassDegradPercent'),
            useConversionMap:       me.parseInputValues('useConversionMap'),
            outEPSG:                me.parseInputValues('reproject'),
            out_name:               me.parseInputValues('output_name'),
            overwrite:              me.parseInputValues('forester_overwrite'),
            nodata_value:           me.parseInputValues('nodata_value')
        };
    }

});