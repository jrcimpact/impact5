/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.PhotosToKML', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowPhotosToKML',
    title: "Photos to KML",
    backgroundColor: "#e5fff4",

    toolID: 'PhotosToKML',

    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            items: [

                Ext.create('IMPACT.view.component.Field.FileByType', {
                    itemId: 'PhotosToKML_input',
                    selectionMode: 'multiple',
                    fieldLabel: 'Input directory containing geolocated JPG photos',
                    fileType: '',
                    returnDir: true,
                    allowBlank: false,
                    //data_path: ((IMPACT.GLOBALS.OS == "unix") ? IMPACT.GLOBALS.data_paths.unix_raw_data : '') // browse from disk
                    data_path: IMPACT.GLOBALS.data_paths.data  // browse from disk
                }),

                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'PhotosToKML_out_name',
                    fieldLabel: '<b>Output filename</b>',
                    labelWidth: 120,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'PhotosToKML_overwrite',
                    defaultValue: 'No'
                })
            ]
        });
        return items;
    },


    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;

        return {
            toolID:           me.toolID,

            inDir:            me.parseInputValues('PhotosToKML_input'),
            overwrite:        me.parseInputValues('PhotosToKML_overwrite'),
            out_name:         me.parseInputValues('PhotosToKML_out_name')
        };
    }

});
