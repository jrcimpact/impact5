/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.component.TreePanel', {
    extend: 'Ext.tree.Panel',


    rootVisible: false,
    border: false,
    collapsible: true,
    width: '100%',

    store: null,
    selectionMode: 'multiple',

    listeners: {
        checkchange: function( node, checked ){
            var me = this;
            if(me.selectionMode=='multiple'){
                me.selectAllChildren(node, checked);
            } else if(me.selectionMode=='single'){
                me.deSelectAllBeforeSelect(node, checked);
            }
        }
    },

    /**
     *  Select children on folder selection
     */
    selectAllChildren: function(node, checked){
        if(node.hasChildNodes()){
            node.eachChild(function(childNode){
                childNode.set('checked', checked);
            });
        }
    },

    /**
     *  Deselect ALL before selection
     */
    deSelectAllBeforeSelect: function(node, checked){
        this.deSelectAll();
        if(node.isLeaf()){
            node.set('checked', checked);
        }
    },

    deSelectAll: function(){
        var rootNode = this.getRootNode();
        rootNode.cascadeBy(function (childNode) {
           childNode.set('checked', false);
        });
    }


});