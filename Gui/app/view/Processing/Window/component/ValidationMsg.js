/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.Window.component.ValidationMsg', {

    singleton: true,

    show: function(errorMsg){

        var msg = '';
        for (var i in errorMsg){
            if(i!=0){
                msg += "<br />";
            }
            msg += '- '+errorMsg[i];
        }

        Ext.Msg.show({
             title: "Missing parameters",
             msg: msg,
             buttons: Ext.Msg.OK,
        });
        return;
    }

});
