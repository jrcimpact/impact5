/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.Info', {
    extend: 'Ext.Component',

    alias: 'widget.ProcessingInfo',

    type: 'info', // Available types: info, warning, title
    html: '',

    border: false,
    style: {
        padding: '5px',
        margin: '5px 0'
    },


    constructor: function(cfg){
        var me = this;

        me.type = cfg.type || me.type;

        if(me.type === 'info'){
            me.style.backgroundColor = "#d4f0f7";
            me.style.fontWeight = 'normal';
        }
        else if(me.type === 'warning'){
            me.style.backgroundColor = '#f7cbc3;';
            me.style.fontWeight = 'normal';
        }
        else if(me.type === 'title'){
            me.style.backgroundColor = '#dddddd;';
            me.style.fontWeight = 'bold';
        }

        me.superclass.constructor.call(me, cfg);
    }


});