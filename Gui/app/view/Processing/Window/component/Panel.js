/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.Panel', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.ProcessingPanel',

    border: false,
    collapsible: true,

    bodyStyle: {
        padding: '5px',
        backgroundColor: '#ffffff'
    },

    constructor: function(cfg){
        var me = this;

        if(cfg.hasOwnProperty('backgroundColor')){
            me.bodyStyle.backgroundColor =  cfg.backgroundColor;
        }
        if(!cfg.hasOwnProperty('title')){
            me.collapsible = false;
        }
        if(cfg.hasOwnProperty('scroll') && cfg.scroll===true){
            me.overflowY = 'scroll';
            me.maxHeight = 250;
        }

        me.superclass.constructor.call(me, cfg);
    }

});



