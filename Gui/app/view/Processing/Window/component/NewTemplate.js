/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.NewTemplate', {
    extend: 'Ext.window.Window',

    requires: [
        'IMPACT.view.Processing.Window.component.Panel',
        'IMPACT.view.Processing.Window.component.Info',
        'IMPACT.view.Processing.Window.component.ValidationMsg',
        'IMPACT.Utilities.Input'
    ],
    cls: 'processing-win-style',
    title: '',
    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'auto',
    width: 465,
    y: 100,
    closeAction: 'hide',
    constrain: true,
    border: false,

    toolID: null,

    backgroundColor: "#ffffff",

    debug_params: false,

    buttons: [
        {
            text: 'Run',
            itemId: 'ProcessingWindowRunId',
            handler: function(){
                this.up('.window')._RUN();
            }
        },
        {
            text: 'Close',
            handler: function(){
                this.up('.window').hide();
            }
        }
    ],

    items: [],

    /**
     * initComponent
     */
    initComponent: function(){
        var me = this;
        // check if toolID has been defined in the instantiating class
        if(me.toolID===null){
            alert("ERROR!! toolID is not defined in "+this.self.getName()+"!!");
            return;
        }
        me.callParent();
        me.bodyStyle = {"background-color": me.backgroundColor};
        me.add({
            xtype: 'form',
            items: me.itemsUI()
        });
    },

    /**
     * Create the window's UI
     */
    itemsUI: function(){
        return [];
    },

    /**
     *  Validate input parameters before launching
     */
    __validate: function(){
        var form = this.down('form').getForm();
        if(form.isValid()){
            return true;
        } else {
            this.__showValidationErrors(form.getFields());
            return false;
        }
    },

    __showValidationErrors: function(fields){
        var errorMsg = [];
        fields.each(function (field) {
            var fieldLabel = '';
            if(typeof field.fieldLabel !== "undefined"){
                fieldLabel = field.fieldLabel
                .replace("<b>", "")
                .replace("</b>", "");
                fieldLabel = '"'+fieldLabel+'": ';
            }
            var errors = field.getErrors();
            if(errors.length > 0 && !field.disabled){
                Ext.each(errors, function(error){
                    errorMsg.push(fieldLabel+error);
                });
            }
        });
        IMPACT.view.Processing.Window.component.ValidationMsg.show(errorMsg);
    },


    /**
     *  Function to be executed on "Run" button press
     */
    _RUN: function(){
        var me = this;

        // Validate input parameters
        if(!me.__validate()){
            return;
        }

        // Retrieve parameters
        var params = me.launchProcessing();

        // Debug params (do not run processing)
        if(me.debug_params){
            console.log(params);
            return;
        }

        // Disable RUN button for 2 seconds
        me.query('#ProcessingWindowRunId')[0].disable();
        setTimeout(function(){
            me.query('#ProcessingWindowRunId')[0].enable();
        }, 5000);

        // Check if another  instance of the tool is already running
        if(IMPACT.LogMonitor.toolIsAlreadyRunning(me.toolID)===true){
            alert("Processing not started. An instance is already running.");
            return;
        }

        // Run the tool
        IMPACT.Utilities.Requests.launchProcessing(params);

    },

    /**
     *  This is an abstract method: it needs to be override in order to instruct "Run" button
     */
    launchProcessing: function(){
        alert(this.self.getName()+': function not implemented');
        return 'done';
    },

    /**
     *  Get and parse values from form's inputs (ensure to output only STRING)
     */
    parseInputValues: function(inputFieldId){
        return IMPACT.Utilities.Input.parseField(this.query('#'+inputFieldId)[0]);
    }

});