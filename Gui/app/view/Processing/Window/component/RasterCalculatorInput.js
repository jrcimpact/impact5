/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.RasterCalculatorInput', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.RasterCalculatorInput',

    border: false,
    layout: 'hbox',
    style: 'margin-bottom: 3px;',
    height: 25,

    index: null,

    items: [],

    initComponent: function(){
        var me = this;
        me.callParent();

        me.index = parseInt(me.itemId.substring(me.itemId.lastIndexOf("_")+1));

        // #### File input selector ###
        me.add(
            Ext.create('IMPACT.view.component.Field.File', {
                itemId: 'raster_calculator_input',
                fieldLabel: me.__getLabel(),
                labelWidth: 15,
                width: 300,
                filters: ['raster', 'class'],
                style: 'margin-bottom: 0;',
                listeners: {
                    'FileInput_change': function(){
                        me.__setBands(this.getFileData());
                    }
                },
                validator: function(){
                    return me.__checkSize();
                }
            })
        );

        // ### Band selector ####
        me.add(
            Ext.create('IMPACT.view.component.Field.Combo', {
                itemId: 'raster_calculator_band',
                fieldLabel: 'Band',
                labelWidth: 40,
                width: 90,
                store: [''],
                disabled: true,
                labelStyle: 'padding-left: 10px;',
                validator: function(value){
                    var input = this.prev('#raster_calculator_input').getFileData();
                    if(input === null || (input !== null && value!=='')){
                        return true
                    }
                    return 'This field is required';
                }
            })
        );

        // ### Destroy button ###
        me.add(
            {
                xtype: 'button',
                iconCls: 'fa fa-lg fa-red fa-times-circle',
                cls: 'impact-icon-button',
                style: 'margin-left: 5px;',
                handler: function() {
                    me.destroy();
                }
            }
        );

    },

    /**
     *  Return the letter to use as parameter (according to record index)
     */
    __getLabel: function(){
        var letter = String.fromCharCode(97 + this.index);
        letter = letter.toUpperCase();
        return letter;
    },

    /**
     *  Populate band combo according to the selected raster
     */
    __setBands: function(data_from_DB){
        var num_bands = data_from_DB['num_bands'];
        var bandInput = this.query('#raster_calculator_band')[0];
        var bands = [];
        for(var i=1; i<=num_bands; i++){
            bands.push(i);
        }
        if(bands.length>1){
            bands.push("All");  // for Masking all Bands
        }

        bandInput.setValue(null);
        bandInput.bindStore(bands);
        if(bands.length===1){
            bandInput.setValue(1);
        }
        bandInput.setDisabled(false);
    },

    /**
     *  Test if image has same size
     */
    __checkSize: function(data_from_DB){
        var files = this.up("#ProcessingWindowRasterCalculator").__parseInputFiles();
        var sizeX=[];
        var sizeY=[];
        if (files.length == 0){return true}
        for(var i=0; i<files.length; i++){
            sizeX.push(files[i].sizeX);
            sizeY.push(files[i].sizeY);
        }
        if ((sizeX.every( (val, i, arr) => val == arr[0] ))  &&
            (sizeY.every( (val, i, arr) => val == arr[0] ))){
            return true
        }
        var reproj = this.up("#ProcessingWindowRasterCalculator").parseInputValues('reproject');
        console.log(reproj);
        if(reproj == 'No'){
            return 'Different number of Columns / Rows. Enable "Force reprojection/resampling"';

        }else{
            return true
        }

    }

});