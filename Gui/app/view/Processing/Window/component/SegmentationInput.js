/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.SegmentationInput', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.SegmentationInput',

    border: false,
    layout: 'hbox',
    style: 'margin-bottom: 3px;',
    height: 25,

    index: null,

    items: [],

    initComponent: function(){
        var me = this;
        me.callParent();

        me.index = parseInt(me.itemId.substring(me.itemId.lastIndexOf("_")+1));

        // #### File input selector ###
        me.add(
            Ext.create('IMPACT.view.component.Field.File', {
                itemId: 'segmentation_input',
                filters: ['raster', 'class'],
                width: 220,
                allowBlank: true,
                validator: function(value){
                    var all_inputs = me.up('#ProcessingWindowSegmentation').query('#segmentation_input');
                    var all_empty = true;
                    all_inputs.forEach(function(input){
                        if(input.getValues().length>0){
                            all_empty = false;
                        }
                    });
                    if(all_empty){
                        return '"Inputs": at least one input is required';
                    }
                    return true;
                }
            })
        );

        // ### Classification selector ####
        me.add(
            Ext.create('IMPACT.view.component.Field.File', {
                itemId: 'classification_input',
                filters: ['raster', 'class'],
                width: 220,
                allowBlank: true
            })
        );

        // ### Band selector ####
        me.add(
            Ext.create('IMPACT.view.component.Field.Combo', {
                itemId: 'classification_band',
                store: [1],
                value: 1,
                width: 40,
                disabled: true
            })
        );

        // ### Destroy button ###
        me.add(
            {
                xtype: 'button',
                iconCls: 'fa fa-lg fa-red fa-times-circle',
                cls: 'impact-icon-button',
                // style: 'margin-left: 5px;',
                style: 'margin-left: 10px;',
                handler: function() {
                    me.destroy();
                }
            }
        );

    }

});