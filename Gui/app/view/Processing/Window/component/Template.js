/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.view.Processing.Window.component.Template', {
    extend: 'Ext.window.Window',

    requires: [
        'IMPACT.view.Processing.Window.component.ValidationMsg',
        'IMPACT.Utilities.Input',
    ],

    title: '',
    closable: true,
    collapsible: false,
    maximizable: false,
    resizable: false,
    layout: 'fit',
    width: 450,
    y: 100,
    closeAction: 'hide',
    constrain: true,

    toolID: null,

    buttons: [
        {
            text: 'Run',
            itemId: 'ProcessingWindowRunId',
            handler: function(){
                this.up('.window').launchProcessing();
            }
        },
        {
            text: 'Close',
            handler: function(){
                this.up('.window').hide();
            }
        }
    ],
    items: [
        {
            xtype: 'panel',
            itemId: 'ProcessingWindowPanel',
            border: false,
            items: []
        }
    ],

    /**
     * Define the Image Store & the filters
     */
    storeParams: {
        dataFolders: null,
        dataTypes: null,
        showFolders: null,
        regex: null
    },
    imageFilters: [
        { boxLabel: 'Oli',      name: 'imageFilters',  inputValue: "oli_",           checked: false },
        { boxLabel: 'Etm',      name: 'imageFilters',  inputValue: "etm_",           checked: false },
        { boxLabel: 'Tm',       name: 'imageFilters',  inputValue: "tm_",            checked: false },
        { boxLabel: 'RapidEye', name: 'imageFilters',  inputValue: "_RE[1-9]_",      checked: false }
    ],
    imageStore: null,

    processingOptionsPanel: null,
    processingOptionsPanelColor: "#ffffff",
    processingOptionsPanelLoaded: false,

    /**
     * initComponent
     */
    initComponent: function(){
        var me = this;
        // check if toolID has been defined in the instantiating class
        if(me.toolID==null){
            alert("ERROR!! toolID is not defined in "+this.self.getName()+"!!")
            return;
        }
        me.callParent();
        // ### initialize STORE ###
        me.imageStore = new Ext.create('IMPACT.store.ProcessingWindow', {
            storeId: me.id+'_imageStore',
            dataFolders: me.storeParams.dataFolders,
            dataTypes: me.storeParams.dataTypes,
            showFolders: me.storeParams.showFolders,
            regex: me.storeParams.regex
        });
        // ### Render window UI and filter image list after STORE is loaded ###
        me.imageStore.on('load', function(){
            me.afterStoreLoad();
        });
    },

    /**
     * Load (and refresh) image store when window is shown
     */
    beforeShow: function(){
        var me = this;
        me.imageStore.load();
    },

    /**
     * add Items only after image store finishes to load
     */
    afterStoreLoad: function(){
        var me = this;
        if(me.processingOptionsPanelLoaded==false){
            me.down('#ProcessingWindowPanel').add(me.itemsUI());
            me.processingOptionsPanelLoaded = true;
        }
    },

    /**
     * Create the window's UI
     */
    itemsUI: function(){
        var me = this;

        var items = [];

        // #### Image List ####
        items.push(
            Ext.create('IMPACT.view.Processing.Window.component.ImageListPanel', {
                store: me.imageStore,
            })
        );
        // #### Image filters ####
        if(me.imageFilters!=null) {
            items.push(
                {
                    xtype: 'panel',
                    title: 'Image Filters',
                    itemId: 'imageFilterPanel',
                    collapsible: true,
                    border: false,
                    items: [
                        {
                            xtype: 'checkboxgroup',
                            itemId: 'imagefiltersCheckbox',
                            width: '100%',
                            style: 'margin: 3px;',
                            columns: 3,
                            items: me.imageFilters,
                            listeners: {
                                change: function () {
                                    me.imageStore.filterBy(this.getValue().imageFilters);
                                }
                            }
                        }
                    ]
                }
            );
        }
        // #### Processing Options ####
        if(me.processingOptionsPanel!=null){
            items.push(
                {
                    xtype: 'panel',
                    title: 'Processing Options',
                    itemId: 'processingOptionsPanel',
                    collapsible: true,
                    border: false,
                    items: me.processingOptionsPanel,
                    bodyStyle:{"background-color": me.processingOptionsPanelColor}
                }
            );
        }

        return items;
    },

    /**
     *  Function to be executed on "Run" button press
     */
    RUN: function(params){
        var me = this;

        // Disable RUN button for 2 seconds
        me.query('#ProcessingWindowRunId')[0].disable();
        setTimeout(function(){
            me.query('#ProcessingWindowRunId')[0].enable();
        }, 2000);

        // Check if another  instance of the tool is already running
        if(IMPACT.LogMonitor.toolIsAlreadyRunning(me.toolID)==true){
            alert("Processing not started. An instance is already running.");
            return;
        }

        // Run the tool
        IMPACT.Utilities.Requests.launchProcessing(params);
    },

    /**
     *  This is an abstract method: it needs to be override in order to instruct "Run" button
     */
    launchProcessing: function(){
        alert(this.self.getName()+': function not implemented');
        return 'done';
    },

    /**
     *  Get and parse values from form's inputs (ensure to output only STRING)
     */
    parseInputValues: function(inputFieldId){
        return IMPACT.Utilities.Input.parseField(this.query('#'+inputFieldId)[0]);
    },

    getSelectedImages: function(imageTreePanelID){

        imageTreePanelID = typeof(imageTreePanelID)==="undefined" ? "imageListPanel" : imageTreePanelID;

        var me = this;
        var checkedImages = me.query('#'+imageTreePanelID)[0].getView().getChecked();

        // ### Retrieve selected images ###
        var names = [];
        Ext.Array.each(checkedImages, function(image){        // undefined if never filtered - need to be init
            if(image.get('leaf') && ((image.get('visible') === undefined) || (image.get('visible') == true))){
                names.push(image.raw.fullPath);
            }
        });

        return JSON.stringify(names);
    }


});
