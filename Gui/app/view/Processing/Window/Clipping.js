/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Clipping', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowClipping',
    title: "Image Clip Options",
    backgroundColor: "#ffffe5",

    toolID: 'clip',


    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Inputs',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'clip_shp_names',
                    filters: ['vector'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Shapefiles'
                }),
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'clip_img_names',
                    filters: ['raster', 'class'],
                    selectionMode: 'multiple',
                    fieldLabel: 'Rasters'
                })
            ]
        });

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: {"padding: 5px; background-color": me.backgroundColor},
            title: 'Processing options',
            items: [
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Keep all touching pixels? ',
                    radioName: 'touching',
                    defaultValue: 'Yes',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 160,
                    width: 280
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Use individual features?',
                    radioName: 'use_features',
                    defaultValue: 'No',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 160,
                    width: 280
                }),
                {
                    xtype: 'ProcessingInfo',
                    html: 'If No, uses the bounding-box'
                },
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'clip_overwrite',
                    defaultValue: 'No',
                    labelWidth: 160,
                    width: 280
                })
            ]
        });

        return items;
    },

    launchProcessing: function(){

        var me = this;

        return {
            toolID:             me.toolID,
            shp_names:          me.parseInputValues('clip_shp_names'),
            img_names:          me.parseInputValues('clip_img_names'),
            use_features:       me.parseInputValues('use_features'),
            overwrite_clip:     me.parseInputValues('clip_overwrite'),
            touching:           me.parseInputValues('touching')
        };
    }

});