/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Mspa', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowMspa',
    title: "Pattern Analysis",
    backgroundColor: "#e5ffdd",

    toolID: 'mspa',

    /**
     *  Build the component UI
     */
    itemsUI: function() {
        var me = this;
        var items = [];
        var spacer_width = 25;

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            scroll: true,
            title: 'Input images',
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'input_images',
                    filters: ['raster', 'class'],
                    selectionMode: 'single',
                    fieldLabel: 'Input images',
                    listeners: {
                            'FileInput_change': function(){
                                me.__setBands(this.getFileData());
                            }
                    },
                    validator: function(){
                            return me.__checkSize();
                    }
                })
            ]
        });

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            collapsible: false,
            title: 'Processing options',
            items: [
                 {
                    xtype: 'container',
                    layout: 'hbox',
                    style: 'margin-bottom: 6px;',
                    items: [
                            {
                                text: '',
                                border: false,
                                width: spacer_width
                            },
                            Ext.create('IMPACT.view.component.Field.Combo',{
                                itemId: 'mspaBand',
                                fieldLabel: 'Band',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'top',
                                labelWidth: 50,
                                width: 80,
                                store: [''],
                                displayField: 'value',
                                valueField: 'value',
                                value: 1,
                            }),

                            {
                                text: '',
                                border: false,
                                width: spacer_width
                            },
                             {
                                xtype: 'numberfield',
                                itemId: 'mspa_foregroud',
                                fieldLabel: 'Foreground',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'top',
                                labelWidth: 70,
                                width: 80,
                                value: 2,
                                minValue: 1,
                                maxValue: 255
                            },
                            {
                                text: '',
                                border: false,
                                width: spacer_width
                            },
                            {
                                xtype: 'numberfield',
                                itemId: 'mspa_background',
                                fieldLabel: 'Background',
                                labelStyle: 'font-weight: bold;',
                                labelAlign: 'top',
                                labelWidth: 70,
                                width: 80,
                                value: 1,
                                minValue: 1,
                                maxValue: 255
                            },
                            {
                                text: '',
                                border: false,
                                width: spacer_width
                            },
                            {
                                xtype: 'numberfield',
                                itemId: 'mspa_nodata',
                                fieldLabel: 'No data',
                                labelStyle: 'font-weight: bold; ',
                                labelAlign: 'top',
                                labelWidth: 50,
                                width: 80,
                                value: 0,
                                minValue: 0,
                                maxValue: 255,
                            },
                            ]
                 },
                 {
                    xtype: 'ProcessingInfo',
                    html: 'Note:\n- Foreground corresponds to the target of interest e.g. Forest\n- Background is the complement e.g. Non Forest\n- No data e.g. water, clouds\n ',
                    labelStyle: 'margin-top: 10px;',
                    style:'margin-left:10px; margin-bottom:10px;',
                    //html: '<div>More info about GuidosToolbox and MSAP can be found <a target="_blank" href="http://forest.jrc.ec.europa.eu/download/software/guidos/">here </a>'
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Mode',
                    labelWidth: 50,
                    labelStyle: 'font-weight: bold; margin-top:10px;',
                    columns: 1,
                    itemId: 'proc_mode',
                    items: [
                        {
                            xtype: 'radiofield',
                            boxLabel: '3 class (Core, Core-Opening, Margin)',
                            name: 'mspa',
                            inputValue: '3',
                            checked: true,
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: '5 class (Core, Core-Opening, Edge, Perforation, Margin)',
                            name: 'mspa',
                            inputValue: '5'
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: '6 class (Core, Core-Opening, Edge, Perforation, Islet, Margin)',
                            name: 'mspa',
                            inputValue: '6'
                        },
                        {
                            xtype: 'radiofield',
                            boxLabel: 'MSPA <a target="_blank" href=/doc/MSPA_Guide.pdf>(Pdf Guide)</a>',
                            name: 'mspa',
                            inputValue: 'MSPA'
                        }
                    ],
                    listeners: {
                        change: function(obj, newValue){
                                    if(newValue.mspa == 'MSPA'){
                                        this.up('#ProcessingWindowMspa').down('#mspa_options').enable();
                                    } else{
                                        this.up('#ProcessingWindowMspa').down('#mspa_options').disable();
                                    }
                                }
                    }
                },
               {
                    xtype: 'container',
                    //layout: 'hbox',
                    style: 'margin-left: 30px;margin-bottom : 20px;',
                    itemId: 'mspa_options',
                    //hidden: true,
                    disabled: true,
                    items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                items:[
                                    Ext.create('IMPACT.view.component.Field.Combo',{
                                        itemId: 'mspa_connectivity',
                                        fieldLabel: 'Foreground connectivity',
                                        //labelStyle: 'font-weight: bold; margin-bottom: 5px;',
                                        labelAlign: 'left',
                                        labelWidth: 100,
                                        width: 170,
                                        store: {
                                            xtype: 'store',
                                            fields: ['value', 'name'],
                                            data : [
                                                {"value":"4", "name":"4"},
                                                {"value":"8", "name":"8"},
                                            ],
                                        },
                                        displayField: 'name',
                                        valueField: 'value',
                                        value: "8",
                                    }),
                                    {
                                        text: '',
                                        border: false,
                                        width: spacer_width
                                    },
                                    {
                                        xtype: 'numberfield',
                                        name: 'mspa_edge',
                                        itemId: 'mspa_edge',
                                        fieldLabel: 'Effective Edge Width',

                                        //labelStyle: 'font-weight: bold; margin-bottom: 5px;',
                                        labelAlign: 'left',
                                        labelWidth: 100,
                                        width: 170,
                                        value: 1,
                                        minValue: 1,
                                        maxValue: 255,
                                    }
                                ]
                            },
                          {
                            xtype: 'container',
                            layout: 'hbox',
                            items:[
                                 Ext.create('IMPACT.view.component.Field.Combo',{
                                        itemId: 'mspa_transition',
                                        fieldLabel: 'Transition',
                                        //labelStyle: 'font-weight: bold; margin-bottom: 5px;',
                                        labelAlign: 'left',
                                        labelWidth: 100,
                                        width: 170,
                                        store: {
                                            xtype: 'store',
                                            fields: ['value', 'name'],
                                            data : [
                                                {"value":"0", "name":"Off"},
                                                {"value":"1", "name":"On"},
                                            ],
                                        },
                                        displayField: 'name',
                                        valueField: 'value',
                                        value: "1",
                                }),
                                {
                                    text: '',
                                    border: false,
                                    width: spacer_width
                                },
                                Ext.create('IMPACT.view.component.Field.Combo',{
                                        itemId: 'mspa_intFlag',
                                        fieldLabel: 'Internal flag',
                                        //labelStyle: 'font-weight: bold; margin-bottom: 5px;',
                                        labelAlign: 'left',
                                        labelWidth: 100,
                                        width: 170,
                                        store: {
                                            xtype: 'store',
                                            fields: ['value', 'name'],
                                            data : [
                                                {"value":"0", "name":"Off"},
                                                {"value":"1", "name":"On"},
                                            ],
                                        },
                                        displayField: 'name',
                                        valueField: 'value',
                                        value: "1",
                                })
                            ]
                          }
                    ]

               },

                                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_suffix',
                    fieldLabel: 'Output Suffix',
                    labelStyle: 'font-weight: bold;',
                    labelWidth: 110,
                    width: 350,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),

                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'mspa_overwrite',
                    defaultValue: 'No',
                    labelWidth: 170,
                    width: 300,
                    labelStyle: 'font-weight: bold;'
                }),

                 {
                    xtype: 'ProcessingInfo',
                    width: 350,
                    style:'margin-left:40px; margin-top:20px;',
                    html: '<div> More info about GuidosToolbox and MSPA are available <a target="_blank" href="http://forest.jrc.ec.europa.eu/download/software/guidos/">here</a> </div>'
                }

            ]
        });

        return items;
    },


     __setBands: function(data_from_DB){
        var num_bands = data_from_DB['num_bands'];
        var bandInput = this.query('#mspaBand')[0];
        var bands = [];
        for(var i=1; i<=num_bands; i++){
            bands.push(i);
        }
        bandInput.setValue(null);
        bandInput.bindStore(bands);
        bandInput.setValue(1);
    },

    /**
     *  Test if image has same size
     */
    __checkSize: function(){
        var files = this.__parseInputFiles();
        if (files.length == 0){return true}
        for(var i=0; i<files.length; i++){
            if ( (files[i].sizeX * files[i].sizeY) > (10000*10000)){
                alert('Image is too big, MSPA might fail.');
                return true;
            }
        }
        return true
    },
    __parseInputFiles: function(){
        var paths = [];
        var inputFiles = this.query('#input_images');
        inputFiles.forEach(function(item){
            var data_from_DB = item.getFileData();
            if(data_from_DB !== null){
                paths.push({
                    sizeX: data_from_DB['num_columns'],
                    sizeY: data_from_DB['num_rows']
                });
            }
        });
        return paths;
    },

    launchProcessing: function(){

        var me = this;

        return {
            toolID:         me.toolID,
            mspa_foregroud: me.parseInputValues('mspa_foregroud'),
            mspa_background:me.parseInputValues('mspa_background'),
            mspa_nodata:    me.parseInputValues('mspa_nodata'),
            proc_mode:      me.parseInputValues('proc_mode'),
            mspaBand:       me.parseInputValues('mspaBand'),
            out_suffix:     me.parseInputValues('out_suffix'),
            img_names:      me.parseInputValues('input_images'),
            overwrite:      me.parseInputValues('mspa_overwrite'),
            // for mspa
            mspa_edge :         me.parseInputValues('mspa_edge'),
            mspa_connectivity : me.parseInputValues('mspa_connectivity'),
            mspa_intFlag :      me.parseInputValues('mspa_intFlag'),
            mspa_transition :   me.parseInputValues('mspa_transition')
        };
    }

});
