/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.Mosaic', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowMosaic',
    title: "Mosaic & Time Series",

    toolID: 'mosaic',
    
    width: 555,

    selectionMode: 'multiple',
    filters: ['raster', 'class'],
    allowBlank: false,
    validator: null,
    store: null,
    treePanel: null,

    initComponent: function(){
        var me = this;
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                'label',
                'full_path',
                'data_from_DB'
            ]
        });

        me.callParent();
       },


    /**
     *  Build the component UI
     */
    itemsUI: function () {
        var me = this;
        var items = [];


        var tree = Ext.create('IMPACT.view.component.ImageTree.Tree', {
                        height : 300,
                        selectionMode: me.selectionMode,
                        filters: me.filters,
                        fileStore: Ext.data.StoreManager.lookup('ImageStore'),
                        selectionStore: me.store,
                        allowDrag : true
                });
        //tree.__buildTree(IMPACT.GLOBALS['data_paths']['data']);  // 'updateStore_finished' is fired only on layertree creation

        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            title: 'Input images',
            items: [
                {
                    xtype: 'ProcessingInfo',
                    margin: '5px',
                    html: 'In areas of overlap, the first image will be on top. Reorder using drag & drop.'
                },

                tree
            ]
        });



        items.push({
            xtype: 'panel',
            border: false,
            collapsible: true,
            bodyStyle: "padding: 5px;",
            title: 'Processing options',
            items: [
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Place each file into separate bands (<b>Time-Stack</b>)',
                    radioName: 'separate',
                    defaultValue: 'No',
                    labelWidth : 350,
                    width: 500
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Force image reprojection; output is the most frequent',
                    radioName: 'force_reprojection',
                    defaultValue: 'No',
                    labelWidth : 350,
                    width: 500
                }),
                // ######  Output nodadata  ######
                Ext.create('IMPACT.view.component.Field.Float',{
                    itemId: 'mosa_nodata',
                    labelAlign: 'Left',
                    labelWidth : 130,
                    width: 270,
                    fieldLabel: 'Nodata value',
                    value: 0,
                    inputAttrTpl: " data-qtip=' Input nodata value, default = 0 ' "
                }),
                // ######  Output pixel Size  ######
                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'psizeX',
                    labelAlign: 'Left',
                    labelWidth : 130,
                    width: 270,
                    fieldLabel: 'Output Pixel Size X',
                    inputAttrTpl: " data-qtip=' Force output Pixel Size in X' "
                }),
                // ######  Output pixel Size  ######
                Ext.create('IMPACT.view.component.Field.FloatAsText',{
                    itemId: 'psizeY',
                    labelAlign: 'Left',
                    labelWidth : 130,
                    width: 270,
                    fieldLabel: 'Output Pixel Size Y',
                    inputAttrTpl: " data-qtip=' Force output Pixel Size in Y' "
                }),
                // ######  Resampling method  ######
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Resampling Method',
                    itemId: 'resampling_method',
                    store: ['near', 'bilinear', 'cubic','average'],
                    value: 'near',
                    labelWidth : 130,
                    width: 270
                }),

                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Align the coordinates of the extent of the output file to the values of the pixel resolution, such that the aligned extent includes the minimum extent (<b>-tap</b>)',
                    //style: 'margin-left: 50px;',
                    radioName: 'tap',
                    defaultValue: 'No',
                    labelAlign: 'left',
                    labelWidth : 350,
                    width: 500
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Build Internal Overviews',
                    radioName: 'mos_overviews',
                    defaultValue: 'Yes',
                    labelWidth : 200,
                    width: 350
                }),
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: 'Overwrite Output',
                    radioName: 'mos_overwrite',
                    defaultValue: 'No',
                    labelWidth : 200,
                    width: 350
                }),
                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'out_name',
                    fieldLabel: '<b>Output Name</b>',
                    labelWidth : 130,
                    width: 370,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
            ]
        });

        return items;
    },

    __validate: function(){
        var me = this;
        var img_names =  me.down('#ImageTree').getSelected();
        if(img_names==='[]'){
            IMPACT.view.Processing.Window.component.ValidationMsg.show(['"Input images": selected at least one image.']);
            return;
        }
        return me.callParent();
    },

    launchProcessing: function () {

        var me = this;
        var names = [];
        Ext.Array.each(me.down('#ImageTree').getSelected(), function(node){        // undefined if never filtered - need to be init
                names.push(node['full_path']);
        });

        var out_name =  IMPACT.GLOBALS['data_paths']['data'] + me.parseInputValues('out_name');

        var psize = me.parseInputValues('psizeX') + ',' + me.parseInputValues('psizeY');
        if(psize === ","){
            psize = '';
        }

        return {
            toolID:             me.toolID,
            img_names:          JSON.stringify(names),
            separate:           me.parseInputValues('separate'),
            force_reprojection: me.parseInputValues('force_reprojection'),
            nodata:             me.parseInputValues('mosa_nodata'),
            psize:              psize,
            resampling:         me.parseInputValues('resampling_method'),
            out_name:           out_name,
            overwrite_mosa:     me.parseInputValues('mos_overwrite'),
            overviews:          me.parseInputValues('mos_overviews'),
            tap:                me.parseInputValues('tap'),
        };
    },

    afterShow: function() {
                // refresh store to sync data change
                 this.down('#ImageTree').__buildTree(IMPACT.GLOBALS['data_paths']['data']);
    }

});


