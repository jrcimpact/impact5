/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.VectorConversion', {
    extend: 'IMPACT.view.Processing.Window.component.NewTemplate',

    itemId: 'ProcessingWindowVectorConversion',
    title: "Vector to Raster Conversion",
    backgroundColor: "#e5fff4",

    toolID: 'VectorConversion',

    /**
     *  Build the component UI
     */
    itemsUI: function(){
        var me = this;
        var items = [];

        items.push({
            xtype: 'ProcessingPanel',
            backgroundColor: me.backgroundColor,
            items: [
                Ext.create('IMPACT.view.component.Field.File', {
                    itemId: 'vector_conversion_input',
                    filters: ['vector'],
                    fieldLabel: 'Input vector',
                    listeners: {
                        'FileInput_change': function(){
                            me.__setAttributes(this.getFileData());
                        }
                    }
                }),
                Ext.create('IMPACT.view.component.Field.Combo',{
                    fieldLabel: 'Select attribute field',
                    itemId: 'vconv_attribute',
                    labelWidth: 140,
                    width: 250,
                    store: [],
                    allowBlank: false
                }),
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Combo',{
                            fieldLabel: 'Attribute Type (cast)',
                            itemId: 'attributeType',
                            labelWidth: 140,
                            width: 250,
                            store: ['Auto', 'GDT_Byte', 'GDT_UInt16', 'GDT_Int32', 'GDT_Float32', 'GDT_Float64'],
                            value: 'Auto',
                            listeners: {
                                change: function() {
                                    var v = this.getValue();
                                    if (v === 'Auto' || v === 'GDT_Byte' || v === 'GDT_UInt16' ){
                                        me.down('#output_pct').enable();
                                    } else {
                                        me.down('#output_pct').disable();
                                    }
                                }
                            }
                        })
                    ]
                },
                // ######  PCT  ######
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Combo',{
                            fieldLabel: 'Apply Palette Color',
                            itemId: 'output_pct',
                            labelWidth: 140,
                            width: 250,
                            store: me.__getLegendRasterStyle(),
                            value: "None"
                        }),
                        {
                            xtype: 'ProcessingInfo',
                            html: ' only Byte or Uint16',
                            style: 'margin: 5px 0 0 5px;'
                        }
                    ]
                },
                {
                    xtype: 'ProcessingInfo',
                    html: ' Output raster options:',
                    style: 'font-weight: bold;margin: 20px 0 0px 0px;'
                },
                {
                    xtype: 'radiogroup',
                    itemId: 'vconv_raster_out_psize',
                    //fieldLabel: 'Output raster options',
                    //labelStyle: 'font-weight: bold;',
                    //labelWidth: 140,
                    columns:1,
                    //width: 300,
                    style: 'margin: 5px 0px 0px 5px;',
                    labelAlign: 'top',
                    vertical: false,
                    disabled: false,
                    items: [
                        { boxLabel: 'Set pixel size in input Unit (auto Extent)', name: 'click', inputValue: 'psize', checked: true },
                        { boxLabel: 'Select raster as template', name: 'click', inputValue: 'template'}
                    ],
                    listeners: {
                        change: function(item, newValue) {
                            if(newValue.click==='psize'){
                                me.down('#vconv_pixelSize').enable();
                                me.down('#raster_template').disable();
                            } else {
                                me.down('#vconv_pixelSize').disable();
                                me.down('#raster_template').enable();
                            }
                        }
                    }
                },
                // ######  Pixel Size  ######
                {
                    xtype: 'container',
                    layout: 'vbox',
                    style: 'margin: -45px 0 10px 255px;',
                    items: [
                        Ext.create('IMPACT.view.component.Field.Float',{
                            itemId: 'vconv_pixelSize',
                            labelWidth: 120,
                            //fieldLabel: 'Pixel Size',
                            //labelAlign: 'right',
                            width: 100,
                            allowBlank: false,
                            validator: function(value){
                                if(value>0) return true;
                                return 'Must be greater the 0'
                            }
                        }),

                        Ext.create('IMPACT.view.component.Field.File', {
                            itemId: 'raster_template',
                            filters: ['raster', 'class'],
                            selectionMode: 'single',
                            width: 180,
                            //style: 'margin-left: -50px;',
                            //fieldLabel: 'Template',
                            //hidden: false
                            disabled: true
                        }),
                    ]
                },


                // ######  Output name  ######
                Ext.create('IMPACT.view.component.Field.Text',{
                    itemId: 'vconv_out_name',
                    fieldLabel: '<b>Output Suffix</b>',
                    labelWidth: 120,
                    width: 280,
                    allowBlank: false,
                    regex: new RegExp(/^[0-9a-zA-Z_-]+$/)
                }),
                // ######  Overwrite  ######
                Ext.create('IMPACT.view.component.Field.RadioYesNo', {
                    fieldLabel: '<b>Overwrite</b> Output',
                    radioName: 'vconv_overwrite',
                    defaultValue: 'No'
                })
            ]
        });
        return items;
    },

    __setAttributes: function(data_from_DB){
        if(!data_from_DB){return}
        var attributes = data_from_DB['attributes'].split(",");
        var attributeField = this.query('#vconv_attribute')[0];
        attributeField.setValue('');
        attributeField.bindStore(attributes);
        attributeField.setDisabled(false);
    },

    __getLegendRasterStyle: function(){
        var legends = ['None'];
        legends = legends.concat(IMPACT.SETTINGS['map_legends']['vector'].map( function(item) { return item.name;}));
        legends = legends.concat(IMPACT.SETTINGS['map_legends']['raster'].map( function(item) { return item.name;}));
        return legends
    },

    /**
     *  Launch processing
     */
    launchProcessing: function(){

        var me = this;
        var val = me.parseInputValues('vconv_raster_out_psize');
        if(val == 'psize'){
            var psize = me.parseInputValues('vconv_pixelSize');
        }else{
            var psize = me.parseInputValues('raster_template');
        }

        return {
            toolID:            me.toolID,
            out_name:          me.parseInputValues('vconv_out_name'),
            img_names:         me.parseInputValues('vector_conversion_input'),
            psize:             psize,
            pct:               me.parseInputValues('output_pct'),
            attribute:         me.parseInputValues('vconv_attribute'),
            attributeType:     me.parseInputValues('attributeType'),
            overwrite:         me.parseInputValues('vconv_overwrite')
        };
    }

});
