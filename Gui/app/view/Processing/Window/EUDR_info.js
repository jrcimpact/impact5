/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.view.Processing.Window.EUDR_info', {
    extend: 'Ext.window.Window',

    itemId: 'InfoWindowEUDR',
    title: "Info",
    backgroundColor: "#ffffe5",
    width: 450,
    height: 450,
    scroll: true,
    resizable:false,


    /**
     *  Build the component UI
     */
    items: [
                        {
                            xtype: 'container',
                            width:630,
                            margin:'5 0 0 10',
                            padding: 10,
                            html: '<b>NOTES</b>:<br />- <b>Beta Version: </b> under consolidation</br>'+
                                   '- <a target="_blank" href="https://forest-observatory.ec.europa.eu/">EU observatory on deforestation and forest degradation</a> '
                        }
    ],
    buttons: [
        {
            text     : 'Close',
            handler  : function(){ this.up('#InfoWindowEUDR').destroy();}
        }
    ],

});