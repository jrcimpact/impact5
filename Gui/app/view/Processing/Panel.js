/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.view.Processing.Panel',{
    extend: 'Ext.panel.Panel',

    id: 'ProcessingPanel',
    region : "east",
    width : 205,

    autoScroll: true,
    collapsible : true,
    border : false,

    items:[
        // ######################################
        // ##########  Image Download  ##########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>CDSE Data Download</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #ffffe5",
            collapsible : true,
            //collapsed : true,
            //hidden: true,

            items:[
                {   xtype: 'button',
                    tooltip : 'CDSE Data Downloader',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_SentinelDownload',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowDownload').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.Download');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowDownload')[0].show();
                    }
                },
                {
                    html: ' Copernicus Explorer',
                    style: "margin-left: 10px;",
                    border: false,
                    bodyCls: 'ProcessingPanel_Description',
                    bodyStyle: "background-color: #ffffe5"
                }
            ]
        },
        // ################################
        // #######  Zip to GeoTiff  #######
        // ################################
        {
            xtype: 'panel',
            title: '<b>Zip to Reflectance</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #ffddf9",
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'Process raw zipped Landsat data',
                    cls : 'ProcessingPanel_Icon_landsat',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowLandsatToToa').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.LandsatToToa');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowLandsatToToa')[0].show();
                    }
                },

                {   xtype: 'button',
                    tooltip : 'Process raw L1C/L2A zipped Sentinel2 data',
                    cls : 'ProcessingPanel_Icon_Sentinel2',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowSentinel2ToToa').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.Sentinel2ToToa');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowSentinel2ToToa')[0].show();
                    }
                },

                {   xtype: 'button',
                    tooltip : 'Process raw RapidEye data',
                    cls : 'ProcessingPanel_Icon_RapidEye',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowRapidEyeToToa').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.RapidEyeToToa');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowRapidEyeToToa')[0].show();
                    }
                },
                {
                    html: 'Unzip and Layerstack to single GeoTiff TOA-Reflectance image',
                    border: false,
                    bodyCls: 'ProcessingPanel_Description',
                    bodyStyle: "background-color: #ffddf9"
                }
            ]
        },


        // ######################################
        // ##########  Image Clipping  ##########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Image Clip</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #ffffe5",
            collapsible : true,
            collapsed : false,
            items:[
                {   xtype: 'button',
                    tooltip : 'Clip raster from Vector',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Clipping',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowClipping').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.Clipping');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowClipping')[0].show();
                    }
                },
                {
                    html: ' Clip Raster from Vector',
                    style: "margin-left: 10px;",
                    border: false,
                    bodyCls: 'ProcessingPanel_Description',
                    bodyStyle: "background-color: #ffffe5"
                }
            ]
        },

        // ######################################
        // #######  Image Classification  #######
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Image Classification</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #e5ffdd;",
            collapsible : true,
            items:[

                {   xtype: 'button',
                    //style: "margin-bottom: 7px;",
                    tooltip : 'Classification Tool',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Classification',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ClassificationTools').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.ClassificationTools');
                        }
                        Ext.ComponentQuery.query('#ClassificationTools')[0].show();
                    }

                },
            ]
        },

        // ######################################
        // ########  Index Builder       ########
        // ######################################
           {
            xtype: 'panel',
            title: '<b>Analysis & Enhancement</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #e5ffdd;",
            collapsible : true,
            items:[
                {   xtype: 'button',
                    style: "margin-bottom: 4px;",
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_AnalysisEnhancement',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#AnalysisEnhancementTools').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.AnalysisEnhancementTools');
                        }
                        Ext.ComponentQuery.query('#AnalysisEnhancementTools')[0].show();
                    }
                },
                {
                    html: 'F-Norm, Unmix, PCA, NDVI, NBR',
                    style: "margin-left: 2px;",
                    border: false,
                    bodyCls: 'ProcessingPanel_Description',
                    bodyStyle: "background-color: #e5ffdd"
                }
             ]
             },

        // ######################################
        // ########  Image Segmentation  ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Image Segmentation</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #e5fff4",
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'Select segmentation parameters ',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Segmentation',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowSegmentation').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.Segmentation');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowSegmentation')[0].show();
                    }
                }
            ]
        },
        // ######################################
        // ########  Degradation         ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Forest Emission Reporting</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #e6e6fa",
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'Forest degradation and deforestation assessment',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Degradation',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowDegradation').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.Degradation');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowDegradation')[0].show();

//                        if( Ext.ComponentQuery.query('#ProcessingWindowDegradationNBR').length == 0 ){
//                            Ext.create('IMPACT.view.Processing.Window.DegradationNBR');
//                        }
//                        Ext.ComponentQuery.query('#ProcessingWindowDegradationNBR')[0].show();
                    }
                }
            ]
        },

        // ######################################
        // ##### Extra Processing Tools  ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>General Tools</b>',
            bodyCls: 'ProcessingPanel_Container',
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'Image Processing Tools',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Generic',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowExtraTools').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.ExtraTools');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowExtraTools')[0].show();
                    }
                }
            ]
        },

         // ######################################
        // ##### Extra Processing Tools  ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Pattern Analysis</b>',
            bodyCls: 'ProcessingPanel_Container',
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'MSPA from JRC Guidos Toolbox',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Mspa',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowMspa').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.Mspa');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowMspa')[0].show();
                    }
                },
                {   xtype: 'button',
                    tooltip : 'Road Mapper',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Roads',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowRoadMapper').length == 0 ){
                            Ext.create('IMPACT.view.Processing.Window.RoadMapper');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowRoadMapper')[0].show();
                    }
                },
                {
                    html: 'MSPA &emsp; &emsp;Road Mapper ',
                    style: "margin-left: 20px;",
                    border: false,
                    bodyCls: 'ProcessingPanel_Description',
                    bodyStyle: "background-color: #ffffe5"
                }
            ]
        },

         // ######################################
        // ##### Extra Processing Tools  ########
        // ######################################
        {
            xtype: 'panel',
            title: '<b>Sampling & Assessment</b>',
            bodyCls: 'ProcessingPanel_Container',
            bodyStyle: "background-color: #e5ffdd;",
            collapsible : true,
            items:[

                {   xtype: 'button',
                    //style: "margin-bottom: 7px;",
                    tooltip : 'Design Samplig, Import Photos, Confusion Matrix',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_SamplingAndAssessment',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#AccuracyTools').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.AccuracyAssessmentTools');
                        }
                        Ext.ComponentQuery.query('#AccuracyTools')[0].show();
                    }

                },
            ]
        },


        // ######################################
        //         ##### Model  ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>Modeling tools</b>',
            bodyCls: 'ProcessingPanel_Container',
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'Soil erosion: RUSLE model ',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_Rusle',
                    handler: function () {
                        if( Ext.ComponentQuery.query('#ProcessingWindowRusle').length == 0 ){
//                            alert('Soon available ...')
                            Ext.create('IMPACT.view.Processing.Window.Rusle');
                        }
                        Ext.ComponentQuery.query('#ProcessingWindowRusle')[0].show();
                    }
                }
            ]
        },
        // ######################################
        //         ##### EUDR  ########
        // ######################################

        {
            xtype: 'panel',
            title: '<b>EUDR tools</b>',
            bodyCls: 'ProcessingPanel_Container',
            collapsible : true,
            items:[
                {   xtype: 'button',
                    tooltip : 'EUDR related tools',
                    cls : 'ProcessingPanel_Icon ProcessingPanel_Icon_EUDR',
                    handler: function () {


                        if( Ext.ComponentQuery.query('#EUDRTools').length == 0 ){
                            Ext.create('IMPACT.view.Processing.SelectionWindow.EUDRTools');

                        }
                        Ext.ComponentQuery.query('#EUDRTools')[0].show();
                    }
                }
            ]
        }

    ]

});