/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.store.ColorPalette.Grid.Vector', {
    extend: 'Ext.data.ArrayStore',

    model: 'IMPACT.model.ColorPalette.Vector',
    autoDestroy: true,
    config: {
        legendType: null
    },

    constructor: function(cfg){
        var me = this;
        me.superclass.constructor.call(me, cfg);
        var item = Ext.data.StoreManager.lookup('VectorLegends').__filterByType(me.legendType);
        if(item!==null){
            var res = [];
            Ext.Array.forEach(item.classes, function(id) {

                res.push({
                    "class":  id[0],
                    "label":  id[1],
                    "color": id[2],
                    "opacity":id[3]

                });
            });
            me.loadData(res);
        }
    }

});