/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.store.ColorPalette.Grid.Raster', {
    extend: 'Ext.data.ArrayStore',

    model: 'IMPACT.model.ColorPalette.Raster',
    autoDestroy: true,
    config: {
        legendType: null
    },

    constructor: function(cfg){
        var me = this;
        me.superclass.constructor.call(me, cfg);
        var item = Ext.data.StoreManager.lookup('RasterStyles').getByType(me.legendType);
        if(item!==null){

            var res = [];
            Ext.Array.forEach(item.classes, function(id) {
                res.push({
                    "range_min": id[0],
                    "range_max": id[1],
                    "label":    id[2],
                    "color_min": id[3],
                    "color_max": id[4]
                });
            });
            me.loadData(res);
        }
    }

});

