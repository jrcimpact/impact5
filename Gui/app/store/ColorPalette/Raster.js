/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.store.ColorPalette.Raster', {
    extend: 'Ext.data.Store',

    storeId: 'RasterStyles',

    fields: [
        //{name: 'id'},  // fake val introduced by EXTJS7
        {name: 'type'},
        {name: 'name'},
        {name: 'classes'}
    ],

    // data: IMPACT.SETTINGS['map_legends']['raster'],

    getAll: function(){
        var me = this;
        var items = me.data.items;
        var all = [];
        for(var i=0; i<items.length; i++){
            all.push(items[i].data);
        }
        return all;
    },

    getByType: function(type){
        var me = this;
        if(typeof type !== "undefined" && type!==null){
            var result = me.query('type', type);
            if(result.length>0){
                return result.items[0].data;
            }
        }
        return null;
    }

});

// Quickfix but should get the data through proxy
// IMPACT.store.ColorPalette.Raster.loadData(IMPACT.SETTINGS['map_legends']['raster']);