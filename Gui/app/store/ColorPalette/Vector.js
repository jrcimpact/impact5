/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */


Ext.define('IMPACT.store.ColorPalette.Vector', {
    extend: 'Ext.data.Store',
    //model: 'IMPACT.model.ColorPalette.Vector',
    storeId: 'VectorLegends',


    fields: [
        //{name: 'id'},   // fake val introduced by EXTJS7
        {name: 'type'},
        {name: 'name'},
        {name: 'classes'}
    ],

    //data: null,

    getAll: function(){
        var me = this;
        var items = me.data.items;
        var legends = [];
        for(var i=0; i<items.length; i++){
            legends.push(items[i].data);
        }
        return legends;
    },

    getLegendList: function(){
        var me = this;
        var items = me.data.items;
        var legends = [];
        for(var i=0; i<items.length; i++){
            legends.push([items[i].data.type, items[i].data.name]);
        }
        return legends;
    },

    getClassColors: function(legendType){
        var me = this;
        var classColors = "";
        var legend = me.__filterByType(legendType);
        if(legend!==null){
            var classes = legend.classes;
            for(var i=0; i<classes.length; i++){
                classColors += classes[i][2]+";";
            }
            classColors = classColors.substring(0, classColors.length - 1);
        }
        return classColors;
    },

    getClassIDs: function(legendType){
        var me = this;
        var classIDs = "";
        var legend = me.__filterByType(legendType);
        if(legend!==null){
            var classes = legend.classes;
            for(var i=0; i<classes.length; i++){
                classIDs += classes[i][0]+";";
            }
            classIDs = classIDs.substring(0, classIDs.length - 1);
        }
        return classIDs;
    },

    getFeatureClassStyles: function(legendType){
        var me = this;
        var styles = {}
        var legend = me.__filterByType(legendType);
        if(legend!==null){
            var classes = legend.classes;
            for(var i=0; i<classes.length; i++){
                var r = classes[i][2].split(",")[0];
                var g = classes[i][2].split(",")[1];
                var b = classes[i][2].split(",")[2];
                var htmlColor = "#"+IMPACT.Utilities.Common.rgbToHex(r, g, b);
                var alpha = (typeof classes[i][3] !== "undefined") ? classes[i][3] : "0";
                var classId =  classes[i][0];
                styles[classId] = {
                    strokeColor: htmlColor,
                    fillColor: htmlColor,
                    fillOpacity: alpha
                }
            }
        }
        return styles;
    },

    __filterByType: function(legendType){
        var me = this;
        if(typeof legendType!=="undefined" && legendType!==null){
            var legend = me.query('type', legendType);
            if(legend.length>0){
                return legend.items[0].data;
            }
        }
        return null;
    }

});

// Quickfix but should get the data through proxy
// IMPACT.store.ColorPalette.Vector.loadData(IMPACT.SETTINGS['map_legends']['vector']);