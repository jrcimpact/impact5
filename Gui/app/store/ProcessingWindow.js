/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.store.ProcessingWindow', {
    extend: 'Ext.data.TreeStore',

    url: 'get_user_data_list.py',

    sorters: [
        { property: 'leaf', direction: 'ASC' },
        { property: 'text', direction: 'ASC' }
    ],

    /**
     * Initialize component
     */
    constructor: function(args) {
        var me = this;
        // check if request extraParams has been defined in the instantiating class
        if(typeof(args.dataFolders)=="undefined" || args.dataFolders==null ||
            typeof(args.dataTypes)=="undefined" || args.dataTypes==null ||
            typeof(args.showFolders)=="undefined" || args.showFolders==null){
            alert("ERROR!!\ndataFolders, dataTypes and showFolders are not defined in "+this.self.getName()+"!!")
        }
        // set the proxy
        me.proxy = new Ext.data.proxy.Ajax({
            url: me.url,
            extraParams: {
                dataFolders: args.dataFolders,
                dataTypes: args.dataTypes,
                showFolders: args.showFolders,
                regex: args.regex
            }
        });

        me.storeId = args.storeId;
        me.callParent();
    },

    /**
     * Filter image list by checkbox selection
     * @param filters
     */
    filterBy: function(filters){

        filters = typeof(filters)==="undefined" ? [] : filters;

        var me = this;
        var tree = me.ownerTree;
        var root = me.getRootNode();

        root.cascadeBy(function (node) {
            if (node.data.leaf == true) {
                // ####  define if the node should be visible or not  ####
                var visible = false;
                if(Ext.isEmpty(filters)){   // if no filters are selected show ALL images
                    visible = true;
                } else {
                    Ext.each(filters, function (filter) {
                        if (node.raw.text.match(filter)) {
                            visible = true;
                        }
                    });
                }
                // ####  set the node visibility ####
                var viewNode = Ext.fly(tree.getView().getNode(node));
                viewNode.setVisibilityMode(Ext.Element.DISPLAY);
                viewNode.setVisible(visible);
                node.set('visible', visible);
                if(visible==false){
                    node.set('checked', false);
                }
            }
        });
    },

    clearFilter: function(){
        this.filterBy();
    }



});