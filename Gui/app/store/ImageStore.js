/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.store.ImageStore', {
    extend: 'Ext.data.Store',

    model: 'IMPACT.model.ImageStore',
    storeId: 'ImageStore',

    autoLoad: false,
    autoSync: true,

    pathList: [],
    is_dirty: false,

    /**
      *  Initialize component
      */
    initComponent:function() {

        // call parent init component
        this.superclass.initComponent.apply(this, arguments);

        // ####  Add custom event  ####
        //this.addEvents('updateStore_finished');
        //this.addEvents('raster_editing_layer_changed');
    },

    /**
      *  Synchronize database with Store
      */
    updateStore: function(database_list){
        var me = this;

        me.pathList = [];
        me.is_dirty = false;

        // #### Synchronize database with Store ####
        for(var i=0; i < database_list.length; i++) {

            var database_item = database_list[i];

            // Force as JSON strings objects
            database_item['extent'] = JSON.stringify(database_item['extent']);
            database_item['metadata'] = JSON.stringify(database_item['metadata']);
            if(typeof database_item['statistics'] === 'object'){
                database_item['statistics'] = JSON.stringify(database_item['statistics']);
            } else {
                 var stats = database_item['statistics'];
                 stats = stats.replace(/\[\[/g, '[["');
                 stats = stats.replace(/\]\]/g, '"]]');
                 stats = stats.replace(/\]\,\s\[/g, '"], ["');
                 stats = stats.replace(/\, /g, '","');
                 stats = stats.replace(/\]\"\,\"\[/g, '],[');
                 database_item['statistics'] = stats;
            }
            database_item['unique_values'] = JSON.stringify(database_item['unique_values']);
            database_item['num_unique_values'] = JSON.stringify(database_item['num_unique_values']);
            database_item['lookup_table'] = JSON.stringify(database_item['lookup_table']);
            database_item['ready'] = JSON.stringify(database_item['ready']);

            var store_item = me.findRecord('full_path', database_item['full_path']);
            me.pathList.push(database_item['full_path']);

            if(store_item !== null) {

                // "modification_date" and "ready" are UNCHANGED: do nothing
                if (store_item.get('modification_date') === database_item['modification_date']
                        && (store_item.get('ready') === database_item['ready'])  ){
                    continue
                }
                // "modification_date" or "ready" are CHANGED: remove item from store
                else {
                    //console.log(database_item['basename']+' is changed (new modification time: ' + database_item['modification_date']  + ') ');
                    me.remove(store_item);
                }
            }

            // add item to store
            me.is_dirty = true;
            me.loadData([database_item],true);
            if(database_item['ready'] === 'true'){
                //console.log('firing');
                this.fireEvent('raster_editing_layer_changed',database_item);
            }
        }

        // #### Remove orphans items from store ###
        if (me.count() !== database_list.length
                || me.is_dirty){
            me.each(function(store_item){
                if (typeof store_item !== 'undefined'
                        && me.pathList.indexOf(store_item.get('full_path')) === -1){
                    me.remove(store_item);
                    me.is_dirty = true;
                }
            })
        }

        // ####  Fire "updateStore_finished" event  ###
        this.fireEvent('updateStore_finished', me);

    },

    /**
      *  Get all records data
      */
    getStoreData: function(){
        var me = this;
        var records=[];
        me.each(function(record){
            records.push(record.data);
        });
        return records
    },

    /**
      *  Get all records data filtered by field/value pair
      */
    getRecordBy: function(field, value){
        var me = this;
        var records=[];
        me.each(function(record){
            if (record.get(field) === value) {records.push(record.data);}
        });
        return records
    }

});