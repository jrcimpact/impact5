/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.store.ImageSettingsStore', {
    extend: 'Ext.data.Store',

    model: 'IMPACT.model.ImageSettingsStore',
    storeId: 'ImageSettingsStore',

    autoLoad: true,
    autoSync: false,  // set to true later on if works

    pathList: [],
    is_dirty: false,

    proxy: {
        type: 'ajax',
        extraParams: {
                toDo: 'load'
        },
        url : 'layer_settings_manager.py'

    },

    /**
      *  Initialize component
      */
    initComponent:function() {

        // call parent init component
        this.superclass.initComponent.apply(this, arguments);

        // ####  Add custom event  ####
        //this.addEvents('updateStore_finished');
    },

    /**
      *  Synchronize database with Store
      */
    saveRecord: function(full_path, value){

        var me = this;
        var database_item = {};
        //
        var store_item = me.findRecord('full_path', full_path);
        if(store_item !== null) {
            me.remove(store_item);
        }
        database_item['full_path'] = full_path;
        database_item['settings'] = value;
        me.loadData([database_item],true);
        //me.is_dirty = true;

    },
     deleteRecord: function(full_path){
        var me = this;
        var store_item = me.findRecord('full_path', full_path);
        if(store_item !== null) {
            me.remove(store_item);
        }
    },
    /**
      *  Get all records data
      */
    getStoreData: function(){
        var me = this;
        var records=[];
        me.each(function(record){
            records.push(record.data);
        });
        return records
    },

    /**
      *  Get all records data filtered by field/value pair
      */
    getRecordBy: function(field, value){
        var me = this;
        var records=[];
        me.each(function(record){
            if (record.get(field) === value) {records.push(record.data);}
        });
        return records
    }

});