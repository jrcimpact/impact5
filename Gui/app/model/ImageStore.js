/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.model.ImageStore', {
    extend: 'IMPACT.model.Base',
    fields: [
        // File related information
        {name: 'full_path', type: 'string'},
        {name: 'path', type: 'string'},
        {name: 'basename', type: 'string'},
        {name: 'extension', type: 'string'},
        {name: 'modification_date', type: 'string'},
        {name: 'fileSize', type: 'string'},
        // Basic geo information
        {name: 'extent', type: 'string'},
        {name: 'EPSG', type: 'string'},
        {name: 'PROJ4', type: 'string'},
        // Flags
        {name: 'ready', type: 'string'},
        // Vector related information
        {name: 'attributes', type: 'string'},
        {name: 'numFeatures', type: 'int'},
        {name: 'simplificationRequired', type: 'string'},
        {name: 'simplificationMethod', type: 'string'},
        // Raster related information
        {name: 'pixel_size', type: 'string'},
        {name: 'metadata', type: 'string'},
        {name: 'statistics', type: 'string'},
        {name: 'num_bands', type: 'int'},
        {name: 'data_type', type: 'string'},
        {name: 'has_colorTable', type: 'int'},
        {name: 'no_data', type: 'string'},
        {name: 'num_columns', type: 'int'},
        {name: 'num_rows', type: 'int'},
        {name: 'num_unique_values', type: 'string'},
        {name: 'unique_values', type: 'string'},
        {name: 'lookup_table', type: 'string'}
    ]
});