Ext.define('IMPACT.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'IMPACT.model'
    }
});
