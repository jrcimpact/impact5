/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('IMPACT.Application', {
    extend: 'Ext.app.Application',

    name: 'IMPACT',

    quickTips: true,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    requires: [
        'IMPACT.*',
        'Ext.tip.QuickTipManager'
    ],

    stores: [
        'ColorPalette.Vector'
        , 'ColorPalette.Raster'
        , 'ImageStore'
        , 'CSImageStore'
        , 'ImageSettingsStore'
    ],

    onBeforeLaunch: function () {
        var me = this;

        Ext.ariaWarn = Ext.emptyFn;

        // #####  Initialize Ext QuickTips  #####
        Ext.tip.QuickTipManager.init();
        // Apply a set of config properties to the singleton
        Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
            shadow: false,
            frame: true,
            trackMouse: true,
            showDelay: 50      // Show 50ms after entering target
            // dismissDelay: 20000
        });

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));


        Ext.Loader.loadScript({url: 'app/OpenLayers/Draw.js'});
        Ext.Loader.loadScript({url: 'app/OpenLayers/EditingRasterPreview.js'});
        Ext.Loader.loadScript({url: 'app/OpenLayers/Raster.js'});
        Ext.Loader.loadScript({url: 'app/OpenLayers/Vector.js'});

        // #####  Disable browser context menu  #####
        window.oncontextmenu = function (e) {
            e = e ? e : window.event;
            if (e.preventDefault) e.preventDefault();   // For non-IE browsers.
            else return false;                          // For IE browsers.
        };


        IMPACT.GLOBALS = [];
        Ext.Ajax.request({
            method: 'GET',
            async: false,
            url: 'getGlobals',
            success: function(response, opts){
                IMPACT.GLOBALS = Ext.JSON.decode(response.responseText);
                //console.info(IMPACT.GLOBALS);

            },
            failure: function(response, opts) {
                console.info(response.status);
                alert('Error in loading application');
                alert(response.status);
            }
        });

        if(IMPACT.GLOBALS['url']==''){
           IMPACT.GLOBALS['url']= window.location.href.replace('/IMPACT','');
        }


        IMPACT.SETTINGS = [];
        Ext.Ajax.request({
            method: 'GET',
            async: false,
            url: 'getSettings',
            success: function(response, opts){
                IMPACT.SETTINGS = Ext.JSON.decode(response.responseText);

                IMPACT.SETTINGS['WGS84'] = "EPSG:4326";
                IMPACT.SETTINGS['WGS84_mercator'] = "EPSG:3857";
                IMPACT.SETTINGS['UseSingleTile'] = IMPACT.SETTINGS['map_options']['singleTile'] === "true";

                IMPACT.SETTINGS['maxExtent'] = ol.proj.transformExtent(IMPACT.SETTINGS['map_options']['max_extent'], IMPACT.SETTINGS['WGS84'], IMPACT.SETTINGS['WGS84_mercator']);
                IMPACT.SETTINGS['initialCenter'] = ol.proj.transform([IMPACT.SETTINGS['map_options']['init_lon'], IMPACT.SETTINGS['map_options']['init_lat']], IMPACT.SETTINGS['WGS84'], IMPACT.SETTINGS['WGS84_mercator']);
                IMPACT.SETTINGS['initialZoom'] = IMPACT.SETTINGS['map_options']['init_zoom'];

                Ext.data.StoreManager.lookup('RasterStyles').loadData(IMPACT.SETTINGS['map_legends']['raster']);
                Ext.data.StoreManager.lookup('VectorLegends').loadData(IMPACT.SETTINGS['map_legends']['vector']);

                // launch after SETINGS are loaded
                var taskLaunch = new Ext.util.DelayedTask(function() {
                    me.launch();
                });
                taskLaunch.delay(2000);


            },
            failure: function(response, opts) {
                console.info(response.status);
                alert('Error in loading application');
                alert(response.status);
            }
        });


    },

    launch: function () {

        // #####  Initialize LogMonitor & sseStreamer  #####
        IMPACT.LogMonitor = Ext.create('IMPACT.Utilities.LogMonitor');
        IMPACT.ImageStore = Ext.data.StoreManager.lookup('ImageStore');
        IMPACT.CSImageStore = Ext.data.StoreManager.lookup('CSImageStore');

        // #####  Build Viewport  #####
        var IMPACTViewport = Ext.create('IMPACT.view.Viewport');
        IMPACTViewport.resetActiveTab();


        // #####  DEBUGGING  #####
        IMPACT.main_mapPanel = IMPACTViewport.down('#MapPanel');
        IMPACT.main_map = IMPACT.main_mapPanel.map;

        // --------------------------------------------------------
        //Textfield inside menu loses focus on mouse-out
        // --------------------------------------------------------
        Ext.menu.Menu.override({
            canActivateItem: function (item) {
                return item && !item.isDisabled() && item.isVisible() && item.canActivate;
            }
        });


        // -------------------  Image Store and socket for standard DATA folder ------------------------------
        var WS = "ws:";
        if (IMPACT.GLOBALS['url'].includes("https://")){
            var WS = "wss:";
        }

        var ImageStoreSocket = new WebSocket( WS + IMPACT.GLOBALS['url'].replace('http://', '').replace('https://', '') + "/ImageStore");
        ImageStoreSocket.onopen = function () {
            ImageStoreSocket.send("Ready");
        };

        ImageStoreSocket.onmessage = function (event) {
            var data = JSON.parse(event.data);
            if (data['status'] === 'ok') {
                // ### update log monitor ###
                if (IMPACT.ImageStore !== null) {
                    IMPACT.ImageStore.updateStore(data['image_db']);
                }
            } else {
                console.log('Error loading logs');
                IMPACT.LogMonitor.SendWarning('Error loading ImageStore');
            }

        }

        ImageStoreSocket.onerror = function (error) {
            console.log('WebSocket Error ' + error);
        };


        // -----------------------   Log store and socket ------------------------------------------------
        var LogStoreSocket = new WebSocket(WS + IMPACT.GLOBALS['url'].replace('http://', '').replace('https://', '') + "/LogStore");
        LogStoreSocket.onopen = function () {
            LogStoreSocket.send("Ready");
        };

        LogStoreSocket.onmessage = function (event) {
            var data = JSON.parse(event.data);
            if (data['status'] == 'ok') {
                // ### update log monitor ###
                if (IMPACT.LogMonitor !== null) {
                    IMPACT.LogMonitor.updateStore(data['logs']);
                    IMPACT.LogMonitor.printLogs();
                }
            } else {
                console.log('Error loading logs');
                IMPACT.LogMonitor.SendWarning('Error loading logs');
            }
        }

        LogStoreSocket.onerror = function (error) {
            console.log('WebSocket Error ' + error);
        };


        this.callParent();
        IMPACT.Utilities.Impact.alertNewVersion();


        // -------------------  Image Store and socket for Clima Station CS DATA folder ------------------------------
        var CSImageStoreSocket = new WebSocket(WS + IMPACT.GLOBALS['url'].replace('http://', '').replace('https://', '') + "/CSImageStore");
        CSImageStoreSocket.onopen = function () {
            if (IMPACT.SETTINGS['tool_options']['scanRemoteData'] == 'enabled'){
                // send the ready mex to the server to scan the remote folder
                CSImageStoreSocket.send("Ready");
            }

        };

        CSImageStoreSocket.onmessage = function (event) {
            var CSdata = JSON.parse(event.data);

            if (CSdata['status'] === 'ok') {
                // ### update log monitor ###
                if (IMPACT.CSImageStore !== null) {
                    IMPACT.CSImageStore.updateStore(CSdata['image_db']);
                }
            } else {
                console.log('Error loading CS logs');
                IMPACT.LogMonitor.SendWarning('Error loading CS store');
            }

        }

        CSImageStoreSocket.onerror = function (error) {
            console.log('WebSocket CS Error ' + error);
        };



    },


});
