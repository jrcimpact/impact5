/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.OpenLayers.Raster', {

    singleton: true,

    createLayer: function(store_record, settings, tileSize){
        //var rasterLayer = new ol.layer.Image({
        var rasterLayer = new ol.layer.Tile({


              title: store_record['basename'],
              name: store_record['basename'],
              //source: new ol.source.ImageWMS({
              source: new  ol.source.TileWMS({
                      url: IMPACT.GLOBALS['url']+'/mapscript_img_fn_tornado.py?',
                      serverType: 'mapserver',
                      ratio: 1,
                      cacheSize: 0,
                      params: {
                        transparent : 'true',
                        layers : ['IMG'],
                        TILED: 'true',
                        full_path: store_record['full_path'],
                        psize: store_record['pixel_size'],
                        bbox: IMPACT.Utilities.Impact.parse_bbox(store_record),

                        apply_data_range: settings['apply_data_range'],
                        legendType: settings['legendType'],                                  // use only if user raster style is provided
                        //editing: settings['editing'],
                        bands: settings['bands'],
                        stretch : settings['stretch'],
                        scale1 : settings['scale1'],
                        scale2 : settings['scale2'],
                        scale3 : settings['scale3'],

                        has_colorTable: store_record['has_colorTable'],
                        metadata: Ext.decode(store_record['metadata']),

                        token: IMPACT.Utilities.Impact.token(store_record['modification_date']),
                        modification_date: store_record['modification_date']


                      }
            }),

            singleTile : IMPACT.SETTINGS['UseSingleTile'],
            map_size: tileSize,
            PROJ4: store_record['PROJ4'],
            transitionEffect: "resize",
            visible: true,
            isBaseLayer: false,
            opacity: settings['opacity'],
            type: (store_record['has_colorTable'] ? 'class' : 'raster'),
            group: IMPACT.Utilities.Common.dirname(store_record['full_path']).replace(IMPACT.GLOBALS['root_path'],'').replace("\\",'/').replace(/\/$/,''),
            modification_date: store_record['modification_date']

          });

        return rasterLayer;
    }
});