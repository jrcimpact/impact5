/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.OpenLayers.Draw', {

    singleton: true,

    createLayer: function() {

        source = new ol.source.Vector({
                wrapX: false,
                features: new ol.Collection(),
                params : {
                    buffer: 0,
                }
        });
        vector = new ol.layer.Vector({

          title: "Temporary Features Layer",
          name: "Temporary Features Layer",

          source: source,
          visible: true,
          isBaseLayer: false,
          buffer: 0,
          type: 'hidden',

          style: {
            'fill-color': 'rgba(255, 255, 255, 0.2)',
            'stroke-color': '#ffcc33',
            'stroke-width': 2,
            'circle-radius': 7,
            'circle-fill-color': '#ffcc33',
          }
        });





        source.on( 'addfeature', function (ft) {

            // WHILE IN EDITING MODE ANY POLY IS VALID FOR SELECTION
            if(IMPACT.main_mapPanel.mapClickMode != 'editing'){

                    var feature = ft.feature;
                    if (feature.getGeometry().getType() === "Polygon") {

                        var poly = feature.getGeometry().getCoordinates();
                        var kinkedPoly = turf.polygon(poly);
                        var kinks = turf.kinks(kinkedPoly);

                       // if there are self-intersections, buffer by 0 to get rid of them
                        if(kinks.features.length > 0) {
                            alert("Self-Intersecting features are not allowed");
                            source.removeFeature(feature);
                        }
                    }
                    // toggle handles the number of features
                    var featTypes=[];
                    source.getFeatures().forEach(feat => { featTypes.push(feat.getGeometry().getType()) })
                    // get unique feature type - do not mix line and poly
                    featTypes = Array.from(new Set(featTypes));
                    if( featTypes.length == 1 ){
                        Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(true);
                    } else {
                        Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(false)
                    }
            }
        });

        source.on( 'removefeature', function (ft) {

            if(IMPACT.main_mapPanel.mapClickMode != 'editing'){

                var featTypes=[];
                source.getFeatures().forEach(feat => { featTypes.push(feat.getGeometry().getType()) })
                // get unique
                featTypes = Array.from(new Set(featTypes));
                if( featTypes.length == 1 ){
                    Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(true);
                } else {
                    Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(false)
                }
            }
        });


        return vector;
    }

// reactivate
//    eventListeners: {
//        'beforefeatureadded': function(evt){
//            // if Line components == undef
//            if (typeof(evt.feature.geometry.components[0].components) === 'undefined') { return true}
//
//            // poly
//            var coordinates = [];
//            for(var i=0; i<evt.feature.geometry.components[0].components.length; i++) {
//                coordinates.push(
//                    [evt.feature.geometry.components[0].components[i].x,
//                     evt.feature.geometry.components[0].components[i].y]
//                 );
//            }
//
//            // #### Check for invalid features (with turf.js) ####
//
//            var polyFeature = {
//                "type": "Feature",
//                "properties": {},
//                "geometry": {
//                    "type": "Polygon",
//                    "coordinates": [coordinates]
//                }
//            };
//            var kinks = turf.kinks(polyFeature);
//
//            // if there are self-intersections, buffer by 0 to get rid of them
//            if(kinks.features.length > 0) {
//                alert("Self-Intersecting features are not allowed");
//                return false;
//            }
//
//            // 1 click creates a small squared box of few pixel at any resolution = if x-y is ~ 0 then delete
//            if(turf.area(polyFeature) < 1){
//                alert("Point or line are not allowed; keep mouse pressed and draw your AOI");
//                return false;
//            }
//
//
//        },
//        'featureadded': function(evt){
//
//            var me = this;
//            // ####  Get feature coordinates  ####
//
//            // Feature id valid
//            Ext.ComponentQuery.query('#MainToolbar2')[0].toggleSaveFeatures(true);
//            me.setZIndex(1100);  // zindex is lost after editing
//        },
//
//        'vertexmodified': function(evt){
//
//
//            var me = this;
//            if (typeof(evt.feature.geometry.components[0].components) === 'undefined') { return true}
//
//            // poly
//            var coordinates = [];
//            for(var i=0; i<evt.feature.geometry.components[0].components.length; i++) {
//                coordinates.push(
//                    [evt.feature.geometry.components[0].components[i].x,
//                     evt.feature.geometry.components[0].components[i].y]
//                 );
//            }
//
//            // #### Check for invalid features (with turf.js) ####
//
//            var polyFeature = {
//                "type": "Feature",
//                "properties": {},
//                "geometry": {
//                    "type": "Polygon",
//                    "coordinates": [coordinates]
//                }
//            };
//            var kinks = turf.kinks(polyFeature);
//
//            // if there are self-intersections, buffer by 0 to get rid of them
//            if(kinks.features.length > 0) {
//                alert("Self-Intersecting features are not allowed");
//                return false;
//            }
//
//            // 1 click creates a small squared box of few pixel at any resolution = if x-y is ~ 0 then delete
//            if(turf.area(polyFeature) < 1){
//                alert("Point or line are not allowed; keep mouse pressed and draw your AOI");
//                return false;
//            }
//        },
//
//
//
//    },



});