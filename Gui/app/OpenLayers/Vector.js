/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('IMPACT.OpenLayers.Vector', {

    singleton: true,

    createLayer: function(store_record, settings, tileSize){

        vectorLayer = new ol.layer.Tile({

                  title: store_record['basename'],
                  name: store_record['basename'],
                  source: new ol.source.TileWMS({
                      url: IMPACT.GLOBALS['url']+'/mapscript_shp_fn_tornado.py?',
                      serverType: 'mapserver',
                      ratio: 1,
                      params: {
                            transparent : 'true',
                            layers : ['Skeleton'],
                            full_path: store_record['full_path'],
                            units: (store_record['PROJ4'].search("longlat")>0 ? 'dd' : 'meters' ),
                            bbox: IMPACT.Utilities.Impact.parse_bbox(store_record),

                            legendType: '',  // reload each time
                            filters: '',
                            ids: '',
                            outline_size: settings['outline_size'],
                            color : settings['color'],
                            //editing: settings['editing'],
                            layer_type: store_record['data_type'],

                            classColors: '',
                            classIDs: '',
                            classVisible: '',   // used in editing mode to show hide class on attribute layers only
                            simplificationMethod: store_record['simplificationMethod'],
                            simplificationRequired: settings['simplificationRequired'],
                            show_outline_on_selection: '---',  // if != ''  it will indicate the class/cluster ID to select/change outline as red,
                            show_outline_on_NotActiveClass: '',      // used in conjuction with show_outline_on_selection if selection is based on deactivated class // force outline
                            selectOnlyActiveClass: 'false',
                            token: IMPACT.Utilities.Impact.token(store_record['modification_date'])
                      }
                }),
                singleTile : IMPACT.SETTINGS['UseSingleTile'],
                transitionEffect : "resize",
                map_size: tileSize,
                PROJ4: store_record['PROJ4'],
                isSelected: false,
                visible: true,
                opacity: settings['opacity'],
                displayInLayerSwitcher: true,
                isBaseLayer: false,
                removeBackBufferDelay: 0,
                type: 'vector',
                attributes: store_record['attributes'],
                group: IMPACT.Utilities.Common.dirname(store_record['full_path']).replace(IMPACT.GLOBALS['root_path'],'').replace("\\",'/').replace(/\/$/,''),
                modification_date: store_record['modification_date']

          });

        return vectorLayer;
    }
});