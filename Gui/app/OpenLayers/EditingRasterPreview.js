/*
 * Copyright (c) 2015, European Union
 * All rights reserved.
 * Authors: Simonetti Dario, Marelli Andrea
 *
 * This file is part of IMPACT toolbox.
 *
 * IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with IMPACT toolbox.
 * If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('IMPACT.OpenLayers.EditingRasterPreview', {

    singleton: true,

    createLayer: function(store_record, tileSize){
        //var rasterLayer = new ol.layer.Image({
        var rasterLayer = new ol.layer.Tile({


              title: 'EditingRasterPreview',
              name: 'EditingRasterPreview',
              //source: new ol.source.ImageWMS({
              source: new  ol.source.TileWMS({
                      url: IMPACT.GLOBALS['url']+'/mapscript_img_fn_tornado.py?',
                      serverType: 'mapserver',
                      ratio: 1,
                      cacheSize: 0,
                      params: {
                        transparent : 'true',
                        layers : ['EditingRasterPreview'],
                        TILED: 'true',
                        full_path: store_record['full_path'],
                        token: IMPACT.Utilities.Impact.token(store_record['modification_date']),
                        modification_date: store_record['modification_date']


                      }
            }),

            singleTile : IMPACT.SETTINGS['UseSingleTile'],
            map_size: tileSize,
            PROJ4: store_record['PROJ4'],
            transitionEffect: "resize",
            visible: false,
            isBaseLayer: false,
            type: 'hidden',
            data_from_DB:store_record,

            //token: IMPACT.Utilities.Impact.token(store_record['modification_date']),

          });

        return rasterLayer;
    }
//    updateRange: function(band, min, max){
//        var me = this;
//        me.getSource().updateParams({'bands':band});
//        me.getSource().updateParams({'apply_data_range' : [false, min, max]});
//        me.getSource().refresh();
//        if(me.getVisible()==false){
//            me.setVisible(true);
//        }
//    },

//    hide: function(){
//        var me = this;
//        me.setVisible(false);
//        me.getSource().refresh();
//    },





});

//
//
//OpenLayers.IMPACT = OpenLayers.IMPACT || {};
//
//OpenLayers.IMPACT.EditingRasterPreview = OpenLayers.Class(OpenLayers.Layer.MapServer , {
//
//    url: IMPACT.GLOBALS['url']+'/mapscript_img_fn_tornado.py',
//
//    DEFAULT_PARAMS: {
//         transparent : 'true',
//         layers : ['EditingRasterPreview'],
//    },
//
//    data_from_DB: null,
//
//    /**
//     * Constructor: OpenLayers.IMPACT.Raster
//     */
//    initialize: function(store_record, tileSize) {
//
//        var me = this;
//        me.data_from_DB = store_record;
//
//        var params = {
//            full_path: store_record['full_path'],
//            token: IMPACT.Utilities.Impact.token(store_record['modification_date'])
//        };
//
//        var options = {
//            singleTile : IMPACT.SETTINGS['UseSingleTile'],
//            map_size: tileSize,
//            transitionEffect: "resize",
//            visible: false,
//            isBaseLayer: false,
//            type: 'hidden'
//        };
//
//        OpenLayers.Layer.MapServer.prototype.initialize.apply(me, [
//            "EditingRasterPreview",
//            me.url,
//            params,
//            options
//        ]);
//        me.params = OpenLayers.Util.applyDefaults(
//            me.params, me.DEFAULT_PARAMS
//        );
//
//    },
//
//    updateRange: function(band, min, max){
//        var me = this;
//        me.getSource().updateParams({bands:band});
//        me.getSource().updateParams({apply_data_range : [false, min, max]});
//        me.getSource().refresh();
//        if(me.getVisible()==false){
//            me.setVisible(true);
//        }
//    },
//
//    hide: function(){
//        var me = this;
//        me.setVisible(false);
//        me.getSource().refresh();
//    },
//
//
//
//    CLASS_NAME: "OpenLayers.IMPACT.EditingRasterPreview"
//});