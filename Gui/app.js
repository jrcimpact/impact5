/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'IMPACT.Application',

    name: 'IMPACT',

    requires: [
        // This will automatically load all classes in the IMPACT namespace
        // so that application classes do not need to require each other.
        'IMPACT.*'
    ]

    // The name of the initial view to create.
    // mainView: 'IMPACT.view.Viewport'
});
