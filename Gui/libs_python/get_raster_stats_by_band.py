#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os, sys

from __config__ import *
import Settings
import json
import numpy
from random import randint

# import GDAL/OGR modules
try:
    from osgeo import gdal
except ImportError:
    import gdal



def run(data_path, band):
    try:
        ## retrieve parameters from request ##
        #params = cgi.FieldStorage()
        #data_path = params.getvalue("data_path", None)
        #band = int(params.getvalue("band", 1))
        SETTINGS = Settings.load()

        def rgb2hex(r, g, b):
            return '#{:02x}{:02x}{:02x}'.format(r, g, b)

        chart = {"title": {}, 'seriesData':[], 'seriesColor':[], 'legendData':[], 'min_max': {}, 'percentile':[]}


        #print "Content-Type: text/html\n"
        file = str(os.path.basename(data_path))
        text = "File: "+file+' Band: '+str(band)
        fontSize = 14
        if len(file) > 25:
            fontSize = 12

        textStyle = {'fontSize': fontSize, 'fontWeight': 'bold'}


        # open
        img_ds = gdal.Open(data_path, gdal.GA_ReadOnly)
        MAP = img_ds.GetRasterBand(band).ReadAsArray()  # .astype(numpy.float32)
        lookup_table = img_ds.GetRasterBand(band).GetColorTable()

        unique, counts = numpy.unique(MAP, return_counts=True)

        # ----- COLORS  from PCT ------
        if lookup_table != None:
            subtext = "Colors derived from palette color table"
        # ----- Random COLORS ------
        else:
            subtext = "Random Colors"

        chart['title'] = \
            {
                'text': text,
                'subtext': subtext,
                'x': 'left',
                'textStyle': textStyle
                #subtextStyle: {fontWeight: 'bold'}
            }


        # ------ BUILDING data array
        mydata = []
        for ct, id in enumerate(unique):
            if lookup_table != None:
                color = lookup_table.GetColorEntry(int(id))
                hexColor = (rgb2hex(color[0], color[1], color[2]))
            else:
                hexColor = rgb2hex(randint(1, 200), randint(1, 200), randint(1, 200))

            mydata.append({'name': float(id), 'value':[float(id),float(counts[ct])], 'count': float(counts[ct]), 'id': float(id), 'itemStyle': {'color': hexColor}})



        chart['seriesData'] = mydata
        chart['min_max'] = {'min:': float(min(unique)), 'max': float(max(unique))}
        chart['percentile'] = {'min:': float(numpy.percentile(unique, 10)), 'max': float(numpy.percentile(unique, 70))}


        return json.dumps(chart)

    except Exception as e:

        chart['title'] = \
            {
                'text': "ERROR in computing stats "+str(e)
            }

        return json.dumps(chart)

def runHichart(data_path, band):
    try:
        ## retrieve parameters from request ##
        #params = cgi.FieldStorage()
        #data_path = params.getvalue("data_path", None)
        #band = int(params.getvalue("band", 1))
        SETTINGS = Settings.load()

        def rgb2hex(r, g, b):
            return '#{:02x}{:02x}{:02x}'.format(r, g, b)


        #print "Content-Type: text/html\n"
        file = str(os.path.basename(data_path))
        text = "<b>File:</b> "+file+'</br> <b>Band:</b> '+str(band)
        if len(file) > 25:
            text = '<font size="-5">'+text+'</font>'

        chart = {"title": {'align':'left','useHTML':'True','text': text}, "series": [], 'colors': []}

        # open
        img_ds = gdal.Open(data_path, gdal.GA_ReadOnly)
        MAP = img_ds.GetRasterBand(band).ReadAsArray()  # .astype(numpy.float32)
        lookup_table = img_ds.GetRasterBand(band).GetColorTable()

        unique, counts = numpy.unique(MAP, return_counts=True)

        # ----- COLORS  from PCT ------
        if lookup_table != None:
            chart["subtitle"] = {'text': "Colors derived from palette color table"}
            mydata = []
            for ct, id in enumerate(unique):
                color = lookup_table.GetColorEntry(int(id))
                chart['colors'].append(rgb2hex(color[0], color[1], color[2]))

        # ----- Random COLORS ------
        else:
            chart["subtitle"] = {'text': "File does not have palette color, using random color palette"}
            for ct, id in enumerate(unique):
                chart['colors'].append(rgb2hex( randint(1,200), randint(1,200), randint(1,200)))

        # ------ BUILDING data array
        mydata = []
        for ct, id in enumerate(unique):
            mydata.append({'id': str(id), 'name': str(id), 'y': float(counts[ct])})

        chart['series'].append({'name':'Value', 'colorByPoint':'true', 'data':mydata})
        return json.dumps(chart)

    except Exception as e:
        #print str(e)
        chart["subtitle"] = {'text': "ERROR in computing stats "+str(e)}
        return json.dumps(chart)
