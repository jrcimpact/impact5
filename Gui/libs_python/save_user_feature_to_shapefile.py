#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
#import cgi
import subprocess

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

# Import local libraries
from __config__ import *


#if __name__ == '__main__':
def run(params):

#    print "Content-type: text/plain;\n"


    shp_dir = GLOBALS['data_paths']['data']
    # retrieve parameters from request ##
    #params = cgi.FieldStorage()
    shp_filename = (params.get("shp_filename")).decode('utf-8')
    geoJSON = (params.get("geoJSON")).decode('utf-8')


    try:
        if not os.path.exists(shp_dir):
            os.mkdir(shp_dir)

        driver = ogr.GetDriverByName("ESRI Shapefile")
        if os.path.exists(shp_dir + shp_filename + '.shp'):
            driver.DeleteDataSource(shp_dir + shp_filename + '.shp')

        file_handler = open(os.path.join(shp_dir, shp_filename+'.json'), 'w')
        file_handler.write(geoJSON)
        file_handler.close()
        subprocess.call(['ogr2ogr', "-f", "ESRI Shapefile", "-s_srs", "EPSG:4326", "-t_srs", "EPSG:4326", os.path.join(shp_dir, shp_filename+'.shp'), os.path.join(shp_dir, shp_filename+'.json')])
        os.remove(shp_dir + shp_filename + '.json')

        return 'OK'

    except Exception as e:
        print('ERROR: ' + str(e))
        return '-1'
