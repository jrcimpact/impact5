#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import shutil

import json
# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import LogStore
from __config__ import *

def run(params):

    shp_name = params.get("shp_name").decode()
    fid = params.get("fid").decode()  # name of the ID column
    list_ids = params.get("list_ids", b'[]').decode()  # user manual poly selection IDs
    bkupID = params.get("backupID", b'').decode()
    bkupIsActive = params.get("bkupIsActive", b'False').decode()
    if bkupIsActive in ['True','true',True,1]:
        bkupIsActive = True
    else:
        bkupIsActive = False


    try:
        # ----------------------------------------------------
        # ------ DEL out file if necessary  ------------------
        # ----------------------------------------------------
        driver = ogr.GetDriverByName('ESRI Shapefile')
        vector_ds = driver.Open(shp_name, 1)
        layer = vector_ds.GetLayer(0)
        layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom
        layerName = str(layer.GetName())

        list_ids = json.loads(list_ids)
        masterID = list_ids[0]

        where_ids = ','.join(map(lambda x: "'" + str(x) + "'", list_ids))

        # preparing BKUP
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_validation")
        if bkupID != '':
            if not os.path.exists(tmp_indir):
                os.makedirs(tmp_indir)

            shp = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp', '_' + bkupID + '.shp'))
            shutil.copy(shp_name, shp)
            shx = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp', '_' + bkupID + '.shx'))
            shutil.copy(shp_name.replace('.shp', '.shx'), shx)
            dbf = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp','_'+bkupID+'.dbf'))
            shutil.copy(shp_name.replace('.shp','.dbf'), dbf)



        # ----- return the dissolved geometry of selected items
        sql = 'SELECT ST_Union(geometry) FROM "%s" WHERE "%s" IN (%s);' % (layerName, fid, where_ids)
        # print(sql)
        dissolved_layer = vector_ds.ExecuteSQL(sql, dialect='SQLITE')
        DissGeom = dissolved_layer.GetNextFeature()

        layer.ResetReading()
        layer.SetAttributeFilter("{} IN ({})".format(fid, where_ids))
        # print('Count = ', layer.GetFeatureCount())
        for i in range(0, layer.GetFeatureCount()):
            # layer.ResetReading()
            feature = layer.GetNextFeature()
            if feature.GetFieldAsString(fid) == masterID:
                # print("Updating GEOM")
                # print("FID ", feature.GetFID())
                feature.SetGeometry(DissGeom.geometry())
                layer.SetFeature(feature)
            else:
                # print('DEL')
                # print('ID', feature.GetFieldAsString(fid))
                # print("FID ",feature.GetFID())
                layer.DeleteFeature(feature.GetFID())
                # vector_ds.ExecuteSQL('REPACK ' + layerName)
                # vector_ds.ExecuteSQL('RECOMPUTE EXTENT ON ' + layerName)

        vector_ds.ExecuteSQL('REPACK ' + layerName)
        vector_ds.ExecuteSQL('RECOMPUTE EXTENT ON ' + layerName)
        vector_ds.ExecuteSQL('CREATE SPATIAL INDEX ON ' + layerName)
        layer = None
        features = None
        feature = None
        vector_ds.ReleaseResultSet(dissolved_layer)
        vector_ds.Destroy()
        vector_ds = None
        DissGeom = None
        print('Done')
        return '1'



    except Exception as e:
        print('Error')
        print(str(e))
        if dissolved_layer is not None:
            vector_ds.ReleaseResultSet(dissolved_layer)
        vector_ds.Destroy()
        DissGeom = None
        vector_ds = None
        return '-1'



if __name__ == "__main__":
    shp = 'E:\dev\IMPACT5\DATA\SHP_test.shp'
    field = 'ID'
    idlist = ['Box2','Box3','Box4']
    RunDissolve(shp, field, idlist)
