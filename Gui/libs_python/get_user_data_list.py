#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import re

# Import local libraries
from __config__ import *
import FileSystem
import DataTypes


def conditions_on_file(filePath, dataTypes, regex):
    basename, file_extension = os.path.splitext(filePath)
    for dataType in dataTypes:
        # Tif #
        if dataType == 'tif' and \
                file_extension == '.tif' and \
                not basename.endswith('_browse') and \
                not basename.endswith('_udm'):

            return bool(re.search(regex, basename))
        # ShapeFile #
        elif dataType == 'shp' and \
                file_extension == '.shp':
            return True
        # Landsat #
        elif dataType == 'landsat' and \
                (file_extension == '.bz' or file_extension == '.gz') and \
                not (basename.endswith('_browse.tif') or basename.endswith('_udm.tif')):
            return True
        # Sentinel2 #
        elif dataType == 'sentinel2' \
                and file_extension == '.zip':
            return bool(re.search(regex, basename))

        elif dataType == 'pancromatic':
            if file_extension == '.gz':
                return bool(re.search(regex, basename))
            elif file_extension == '.tif' \
                    and '_calrefbyt' not in basename \
                    and '_calrefflt' not in basename \
                    and '_calrefx10k' not in basename \
                    and '_cluster' not in basename \
                    and '_class' not in basename \
                    and '_unmix' not in basename:
                return True
        if dataType == 'any_raster' and file_extension not in ['.txt','.shp','.dbf','.shx','.prj','.xml','.avi','.gitkeep','.gz','.bz','.zip','.tfw','.jpw','.gfw','.IMD'] :
            return bool(re.search(regex, basename))
    return


def print_data_tree(path, showFolders, dataTypes, regex, recursive):
    """
    Generate layer tree structure from filesystem
    :param path:
    :param showFolders:
    :param dataTypes:
    :param regex:
    :param recursive:
    :return:
    """
    output = ''
    for filePath in FileSystem.list_files(path):
        fullPath = path.rstrip('/')+'/'+filePath
        if fullPath in GLOBALS['data_paths']['tmp_data']:
            continue
        if FileSystem.path_length_is_valid(fullPath):
            if os.path.isfile(fullPath):
                try:
                    if conditions_on_file(fullPath, dataTypes, regex):
                        output += '''{
                                        "text": "'''+os.path.basename(fullPath)+'''",
                                        "qtip": "'''+os.path.basename(fullPath)+'''",
                                        "fullPath": "'''+fullPath+'''",
                                        "leaf": true,
                                        "checked": false,
                                        "qtip":"'''+os.path.basename(fullPath)+'''"
                                    },'''
                except:
                    pass
            elif os.path.isdir(fullPath) and recursive:
                if showFolders:
                    output += '''{
                                    "text": "'''+fullPath.rstrip('/').rpartition('/')[2]+'''",
                                    "qtip": "'''+fullPath.rstrip('/').rpartition('/')[2]+'''",
                                    "cls": "folder",
                                    "expanded": false,
                                    "checked": false,
                                    "children": ['''+print_data_tree(fullPath, showFolders, dataTypes, regex, recursive).rstrip(',')+''']
                                  },'''
                else:
                    output += print_data_tree(fullPath, showFolders, dataTypes, regex, recursive)
        else:
            output += '''{
                            "iconCls": "fa fa-lg fa-red fas fa-exclamation-triangle",
                            "text": "ERROR: path too long found!!",
                            "leaf": true
                        },'''
    return output


def run(dataFolder, dataTypes, showFolders, regex):
    #print "Content-Type: text/html\n"

    # retrieve parameters from GET #
    #params = cgi.FieldStorage()
    # dataFolder = params.getvalue('dataFolders').split(',')[0]
    # dataTypes = params.getvalue('dataTypes').lower().split(',')
    # showFolders = DataTypes.str2bool(params.getvalue('showFolders'))
    # regex = params.getvalue("regex", '')
    try:
        dataFolder = dataFolder.split(',')[0]
        dataTypes = dataTypes.lower().split(',')
        showFolders = DataTypes.str2bool(showFolders)
        #regex = params.getvalue("regex", '')

        # retrieve data tree structure #
        fullPath = GLOBALS['data_paths'][dataFolder.lower()]

        recursive = True
        dataTree = '''{
                        "text": "''' + fullPath.rstrip('/').rpartition('/')[2] + '''",
                        "qtip": "''' + fullPath.rstrip('/').rpartition('/')[2] + '''",
                        "cls": "folder",
                        "expanded": true,
                        "checked": false,
                        "children": [''' + print_data_tree(fullPath, showFolders, dataTypes, regex, recursive).rstrip(',') + ''']
                        },
                    '''

        return '[' + dataTree.rstrip(',') + ']'

    except Exception as e:
        return str(e)
