# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys
import mapscript

# Import local libraries
from __config__ import *


def init_map(debug=False, debug_level=0):
    """ Initialize a generic mapObj item """
    from datetime import datetime

    map_obj = mapscript.mapObj()

    if debug:
        map_obj.debug = debug_level
        map_obj.setConfigOption("MS_ERRORFILE", os.path.join(GLOBALS['temp_path'], 'mapserver.log'))
    map_obj.name = 'map_'+datetime.now().strftime('%Y%m%d%H%M%S%f')
    map_obj.transparent = mapscript.MS_TRUE
    map_obj.imagecolor = mapscript.colorObj(255, 0, 0)
    map_obj.maxsize = 2500
    # map_obj.setConfigOption("PROJ_LIB", "/usr/local/share/proj")
    # map_obj.applyConfigOptions()

    return map_obj


def png_format(map_obj):
    output_format = mapscript.outputFormatObj('AGG/PNG')
    output_format.name = 'png'
    output_format.imagemode = mapscript.MS_IMAGEMODE_RGBA
    output_format.extension = 'png'
    output_format.mimetype = 'image/png'  # ;mode=24bit'
    output_format.transparent = mapscript.MS_ON
    map_obj.appendOutputFormat(output_format)
    map_obj.setImageType('png')


def ows_request(map_obj):
    req = mapscript.OWSRequest()
    req.loadParams()
    map_obj.OWSDispatch(req)


def map_to_http(map_obj, also_mapfile=False):
    """ Generate map and sent as HTTP response """
    try:
        # Draw map
        # print("Content-type: image/png;")
        # print()
        #save_mapfile(map_obj)
        output = map_obj.draw().getBytes() # write to stdaout
        if sys.platform != "linux":
            mapscript.msCleanup()
        # Save mapfile
        if also_mapfile:
            save_mapfile(map_obj)
        # Print output image
        return output

    except Exception as err:
        pass
        # print("Content-type: text/plain;")
        # print('Error on map_to_http')
        # print(err)


def map_to_file(map_obj, also_mapfile=False):
    """ Generate map and save file """
    # Generate filename
    from datetime import datetime
    img_filename = 'map_'+datetime.now().strftime('%Y%m%d%H%M%S%f')
    img_filename = os.path.join(GLOBALS['temp_path'], img_filename+'.png')
    # Draw map and save file
    image = map_obj.draw()
    image.save(img_filename)
    # Save mapfile
    if also_mapfile:
        save_mapfile(map_obj, img_filename)
    return img_filename


def save_mapfile(map_obj, mapfile_filename=None):
    """ Generate mapfile and save """
    if mapfile_filename is None:
        from datetime import datetime
        mapfile_filename = 'map_' + datetime.now().strftime('%Y%m%d%H%M%S%f')
    mapfile_filename = os.path.join(GLOBALS['temp_path'], mapfile_filename + '.map')
    map_obj.save(mapfile_filename)


def generate_worldfile(map_obj, img_filename):
    """ Generate world file """
    map_extent = map_obj.extent
    pixel_size = calculate_pixel_size(map_extent, [map_obj.width, map_obj.height])
    world_file_content = str(pixel_size[0])+"\n"           # pixel size in the x-direction in map units/pixel
    world_file_content += "0.0\n"                         # rotation about y-axis
    world_file_content += "0.0\n"                         # rotation about x-axis
    world_file_content += "-"+str(pixel_size[1])+"\n"      # pixel size in the y-direction
    world_file_content += str(map_extent.minx)+"\n"        # x-coordinate of the center of the upper left pixel
    world_file_content += str(map_extent.maxy)+"\n"        # y-coordinate of the center of the upper left pixel
    world_filename = img_filename.replace('.png', '.pgw')
    file_obj = open(world_filename, 'w')
    file_obj.write(world_file_content)
    file_obj.close()
    return world_file_content


def calculate_image_size(image_extent, pixel_size):
    """ calculate image size """
    map_with = abs(float(image_extent[2]) - float(image_extent[0]))
    map_height = abs(float(image_extent[3]) - float(image_extent[1]))
    map_size_x = map_with/float(pixel_size)
    map_size_y = map_height/float(pixel_size)
    return [map_size_x, map_size_y]


def calculate_pixel_size(map_extent, map_size):
    """ calculate map resolution """
    map_with = abs((float(map_extent.maxx)) - float(map_extent.minx))
    map_height = abs((float(map_extent.maxy)) - float(map_extent.miny))
    pixel_size_x = map_with/int(map_size[0])
    pixel_size_y = map_height/int(map_size[1])
    return [pixel_size_x, pixel_size_y]


def calculate_scale(map_extent, map_size):
    """ calculate map scale """
    map_with = abs((float(map_extent[2])) - float(map_extent[0]))
    pixel_size = map_with/int(map_size[0])
    scale = pixel_size * 2 * 1000
    return scale


def max_extent_xy(extent):
    """ get the max between width and height """
    return max([abs(float(extent[0]) - float(extent[2])), abs(float(extent[1]) - float(extent[3]))])


def shapefile_simplification_index(units, extent):
    """ calculate simplification index for shapefile """
    index = None
    if extent is None:
        max_extent_size = 1
    else:
        max_extent_size = max_extent_xy(extent)
    if units != 'meters':
        if max_extent_size >= 200000:
            index = '0.01'
        elif max_extent_size >= 100000:
            index = '0.005'
        elif max_extent_size >= 50000:
            index = '0.001'
        elif max_extent_size >= 20000:
            index = '0.0001'
        elif max_extent_size >= 10000:
            index = '0.00005'
    else:
        if max_extent_size >= 500000:
            index = '300'
        elif max_extent_size >= 300000:
            index = '200'
        elif max_extent_size >= 100000:
            index = '120'
        elif max_extent_size >= 50000:
            index = '60'
        elif max_extent_size >= 40000:
            index = '30'
        elif max_extent_size >= 30000:
            index = '15'
        elif max_extent_size >= 20000:
            index = '10'
        elif max_extent_size >= 10000:
            index = '5'
    return index
