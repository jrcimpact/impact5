# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sqlite3
import sys
import os.path
import json
import random
import subprocess
import numpy

# import local libraries
from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr


UNIQUE_VALUES_THRESHOLD = 50


def open_shapefile(shapefile):
    """ Open the given shapefile """
    if shapefile.endswith('.shp'):
        driver = ogr.GetDriverByName('ESRI Shapefile')
    if shapefile.endswith('.kml'):
        driver = ogr.GetDriverByName('LIBKML')
    if shapefile.endswith('.vrt'):
        driver = ogr.GetDriverByName('VRT')
    if shapefile.endswith('json'):
        driver = ogr.GetDriverByName('GeoJSON')

    data_source = driver.Open(shapefile, 0)
    return data_source


def get_feature_attributes(layer, feature=None, kml=False):
    """ Retrieve all attributes (dbf) from the given layer (feature) """
    # ---------------------------------------------
    # IMPORTANT : by setting utf-8 is not necessary to use decode()
    #             layer_definition.GetFieldDefn(i).GetName().decode('utf-8', 'ignore')
    # ---------------------------------------------
    # reload(sys)
    # sys.setdefaultencoding("utf-8")

    if feature is None:
        feature = layer[0]
    layer_definition = layer.GetLayerDefn()
    attributes = {}
    for i in range(layer_definition.GetFieldCount()):
        # -- encoding on query is ok in utf-8 --------
        column_name = layer_definition.GetFieldDefn(i).GetName()
        # attributes[column_name.decode('utf-8', 'replace')] = \
        #     str(feature.GetField(column_name)).decode('utf-8', 'replace')
        tmp = str(feature.GetField(column_name))
        if kml:
            tmp = tmp.replace("src='file:///","src=getImageFile.py?format=png&filename=")
            tmp = tmp.replace(".jpg'",".jpg").replace(".JPG'",".JPG")


        attributes[column_name] = tmp
    return attributes


def query_shapefile_by_point(filename, lon, lat, lat_lon_epsg=3857, mapWidth=0):
    """ Query shapefile by point and retrieve features (with all attributes) """
    data_source = open_shapefile(filename)
    layer = data_source.GetLayer()

    feature = layer.GetNextFeature()
    geometry = feature.GetGeometryRef()
    vtype = (geometry.GetGeometryName()).upper()
    kml = filename.endswith('.kml')

    # buffer in meters as Lat Lon are in mercator
    Buffer = 0.0
    if 'POINT' in vtype:
        if mapWidth < 2000:
            Buffer = 10
        if mapWidth > 2000 and mapWidth < 5000:
            Buffer = 20
        if mapWidth > 5000 and mapWidth < 10000:
            Buffer = 50
        if mapWidth > 10000 and mapWidth < 50000:
            Buffer = 70
        if mapWidth > 50000 and mapWidth < 100000:
            Buffer = 100
        if mapWidth > 100000:
            Buffer = 1000


    geom = __get_point_geometry(lon, lat, layer.GetSpatialRef(), lat_lon_epsg, Buffer)
    # Execute spatial query
    layer.SetSpatialFilter(geom)
    records = []
    for feature in layer:
        attributes = get_feature_attributes(layer, feature, kml)
        records.append(attributes)
    #del data_source
    data_source = None
    layer = None
    feature = None
    return records


def query_tiff_by_point(filename, lon, lat, band=0, lat_lon_epsg=3857):
    """ Query Tiff by point and retrieve pixel value """
    gdal.AllRegister()
    data_source = gdal.Open(filename)

    num_bands = data_source.RasterCount
    num_col = data_source.RasterXSize
    num_line = data_source.RasterYSize
    records = []

    try:
        import numpy
        import struct
        # retrieve x,y coordinates from given lat,lon
        target_ref = osr.SpatialReference()
        target_ref.ImportFromWkt(data_source.GetProjectionRef())
        point = __get_point_geometry(lon, lat, target_ref, lat_lon_epsg)
        geo_transform = data_source.GetGeoTransform()

        # # ------  GDAL 3 changes X Y default return val --------
        # #https://github.com/OSGeo/gdal/issues/1546
        # if gdal.VersionInfo()[0] == '3':
        #     if '4326' == target_ref.GetAttrValue('AUTHORITY',1):
        #         x, y = point.GetY(), point.GetX()
        #     else:
        #         x, y = point.GetX(), point.GetY()
        x, y = point.GetX(), point.GetY()
        px = (x - geo_transform[0]) / geo_transform[1]  # x pixel
        py = (y - geo_transform[3]) / geo_transform[5]  # y pixel

        if px < 0 or py < 0:
            data_source = None
            return []
        else:
            px = int(px)
            py = int(py)

        # for each band

        if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
            data_source = None
            return []

        for band_index in range(1, num_bands + 1):
            if band != 0 and band != band_index:
                continue
            # retrieve pixel value from raster band
            band_obj = data_source.GetRasterBand(band_index)
            pixel_value = band_obj.ReadAsArray(px, py, 1, 1)
            # print pixel_value[0][0]
            if pixel_value is None:
                # out of image
                return []
            band_info = {
                'band': band_index,
                'value': str(pixel_value[0][0]),
                'x': px,
                'y': py,
                'Geox': x,
                'Geoy': y,
            }
            records.append(band_info)
    except Exception as e:
        pass

    data_source = None
    return records

def __get_point_geometry(lon, lat, target_ref=None, lat_lon_epsg=4326, myBuffer=0):
    """ Get point geometry from coordinates (4326) transformed to file projection """
    point = ogr.CreateGeometryFromWkt("POINT (" + str(float(lon)) + " " + str(float(lat)) + ")")
    if myBuffer:
        point = point.Buffer(myBuffer)
    source_ref = osr.SpatialReference()
    source_ref.ImportFromEPSG(int(lat_lon_epsg))
    source_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    if target_ref is not None:
        target_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        transform = osr.CoordinateTransformation(source_ref, target_ref)
        point.Transform(transform)
    return point


def get_info_from_shapefile(filename, with_simplification_method=False):
    """ Extract info from a Shapefile """

    extent = [0,0,0,0,0]
    info = {
        'extent': json.dumps({"E": extent[1], "S": extent[2], "W": extent[0], "N": extent[3]}),
        'PROJ4': 'Unknown',
        'EPSG': 'Unknown'
    }
    info['attributes'] = ''
    info['data_type'] = 'Unknown'


    try:
        if filename.endswith(".shp"):
            data_source = ogr.GetDriverByName('ESRI Shapefile').Open(filename, 0)
        if filename.endswith(".kml"):
            data_source = ogr.GetDriverByName('LIBKML').Open(filename, 0)
        if filename.endswith(".geojson") or filename.endswith(".json") :
            data_source = ogr.GetDriverByName('GeoJSON').Open(filename, 0)

    except:
        if filename.endswith(".shp"):
            print("Missing .dbf or .shx file on " + filename)
        else:
            print("Reading file " + filename)
        return info

    try:
        layer = data_source.GetLayer(0)
        # basic information
        extent = layer.GetExtent()
        srs = layer.GetSpatialRef()
        srs_proj4 = srs.ExportToProj4()
        # EPSG = srs.GetAuthorityCode(None)
        # print('from file', EPSG)
        # print(srs.GetAuthorityCode('PROJCS'))
        # print(srs.GetAuthorityCode('GEOGCS'))
        # print(srs.GetAuthorityCode('EOGCS|UNIT'))
        EPSG = None
        try:
            EPSG = get_code_from_projname(srs.GetName())
        except Exception as e:
            pass
        if EPSG in ["", None]:
            tmp = srs.GetAuthorityCode(None)
            if tmp not in ["", None]:
                EPSG = "EPSG:" + str(tmp)
            else:
                print("File :",filename, " has no projection CODE")
                print("Name :",srs.GetName(), " Proj4 :",srs_proj4)

                # scan projDB and search for Proj4 definition to get EPSG/ESRI code
                # 2b improved
                # print('Searching')
                tmp_srs = osr.SpatialReference()
                gdal.UseExceptions()
                # gdal.PushErrorHandler('CPLQuietErrorHandler')
                for authority in ['EPSG','ESRI']:
                    if EPSG is None:
                        # print(authority)
                        for crs_info in osr.GetCRSInfoListFromDatabase(authority):
                            try:
                                tmp_srs.SetFromUserInput(authority + ':' + str(crs_info.code))
                                if tmp_srs.ExportToProj4() == srs_proj4:
                                    EPSG = authority + ':' + str(crs_info.code)
                                    break
                            except Exception as e:
                                pass
        #print(EPSG)

        info = {
            'extent': json.dumps({"E": extent[1], "S": extent[2], "W": extent[0], "N": extent[3]}),
            'PROJ4': srs_proj4,
            'EPSG': EPSG
        }
        # SHP specific attributes
        layer_definition = layer.GetLayerDefn()
        attributes = []
        info['attributes'] = ''

        # ---------------------------------------------
        # IMPORTANT : by setting utf-8 is not necessary to use decode()
        #             attribute_name = str(layer_definition.GetFieldDefn(i).GetName()).decode('utf-8', 'ignore')
        # ---------------------------------------------
        # reload(sys)
        # sys.setdefaultencoding("utf-8")

        for i in range(layer_definition.GetFieldCount()):
            column_name = layer_definition.GetFieldDefn(i).GetName()
            attributes.append(column_name)

        #print attributes
        if len(attributes) > 0:
            info['attributes'] = ",".join(attributes)
        info['numFeatures'] = layer.GetFeatureCount()
        if with_simplification_method:
            info['numFeatures'], info['simplificationRequired'], info['simplificationMethod'] \
                = set_simplification_method(filename)

        feature = layer.GetNextFeature()

        try:
            geometry = feature.GetGeometryRef()
            info['data_type'] = (geometry.GetGeometryName()).upper()
        except:
            #print (layer.features[0].geometry) #.GetGeometryType()).upper()
            info['data_type'] = 'Unknown'
    except Exception as e:
        print(e)
        print("Missing .prj file on " + filename)

    feature = None

    data_source = None
    layer = None
    #del data_source
    return info


def set_simplification_method(filename):
    """ Extract statistics from a Shapefile """
    if filename.endswith('.shp'):
        data_source = ogr.GetDriverByName('ESRI Shapefile').Open(filename, 0)
        layer = data_source.GetLayer(0)
        num_features = layer.GetFeatureCount()
        simplification_required = 'false'
        simplification_method = 'generalize'

        file_size = os.path.getsize(filename)/(953700)
        if num_features > 10000 and file_size > 10000000:  # 10Mb
            simplification_required = 'true'
            box_occurrence = 0
            check_num = min(int(num_features * 0.1), 500)
            for featId in random.sample(range(num_features), check_num):
                feat = layer[featId]
                if feat.GetGeometryRef().GetGeometryRef(0).GetPointCount() < 6:
                    box_occurrence += 1
                    if box_occurrence > check_num * 0.2:
                        simplification_method = 'smoothsia'
                        break
                del feat
        #del data_source
        data_source = None
        layer = None
        return num_features, simplification_required, simplification_method
    else:
        sizeMb = os.path.getsize(filename)/(953700)
        if sizeMb > 200 or filename.endswith('.vrt'):
            return 'approximate'
        else:
            return ''


def get_info_from_tiff(filename):
    """ Extract info from a raster file """
    #print " Extract info from a raster file " + filename
    gdal.AllRegister()
    data_source = gdal.Open(filename,0)
    geo_transform = data_source.GetGeoTransform()
    metadata = data_source.GetMetadata()

    # remove default metadata field : not necessary
    if 'AREA_OR_POINT' in metadata:
        del metadata['AREA_OR_POINT']
    if geo_transform[1] == 1 and geo_transform[5] == 1:
        # NO PROJECTION set a default one centered to 0N 0E
        info = {
            'extent': json.dumps({"E": data_source.RasterXSize, "S": 0, "W": 0, "N": data_source.RasterYSize}),
            'PROJ4': '',
            'EPSG': '',
            'pixel_size': geo_transform[1],
            'metadata': json.dumps(metadata),
            'num_columns': data_source.RasterXSize,
            'num_rows': data_source.RasterYSize,
            'no_data': data_source.GetRasterBand(1).GetNoDataValue(),
            'simplificationMethod': set_simplification_method(filename)
        }
    else:
        min_x = geo_transform[0]
        max_y = geo_transform[3]
        max_x = min_x + geo_transform[1] * data_source.RasterXSize
        min_y = max_y + geo_transform[5] * data_source.RasterYSize
        srs = osr.SpatialReference(wkt=data_source.GetProjection())
        # print(srs.GetName())
        # print(srs.IsGeographic())
        # print(srs.IsProjected())
        # print(srs.AutoIdentifyEPSG())
        # print(srs.GetAuthorityCode(None))
        # print(srs.GetAuthorityCode('PROJCS'))
        # print(srs.GetAuthorityCode('GEOGCS'))
        # print(srs.GetAuthorityCode('EOGCS|UNIT'))
        # print(proj.GetAttrValue('AUTHORITY', 1))

        srs_proj4 = srs.ExportToProj4()
        EPSG = srs.GetAuthorityCode(None)

        if EPSG in ["", None]:
            EPSG = srs.GetAttrValue('AUTHORITY', 1)

        gdal.UseExceptions()
        try:
            if EPSG in ["", None]:
                # scan projDB and search for Proj4 definition to get EPSG/ESRI code
                # 2b improved
                # print('Searching DB')
                tmp_srs = osr.SpatialReference()

                for authority in ['EPSG','ESRI']:
                    if EPSG is None:
                        for crs_info in osr.GetCRSInfoListFromDatabase(authority):
                            tmp_srs.SetFromUserInput(authority + ':' + str(crs_info.code))
                            if tmp_srs.ExportToProj4() == srs_proj4:
                                EPSG = authority + ':' + str(crs_info.code)
                                break
        except Exception as e:
            # print(e)
            # no error raised with UseExceptions()
            pass
        
        print(EPSG)
        info = {
            'extent': json.dumps({"E": max_x, "S": min_y, "W": min_x, "N": max_y}),
            'PROJ4': srs.ExportToProj4(),
            'EPSG': EPSG,
            'pixel_size': geo_transform[1],
            'metadata': json.dumps(metadata),
            'num_columns': data_source.RasterXSize,
            'num_rows': data_source.RasterYSize,
            'no_data': data_source.GetRasterBand(1).GetNoDataValue(),
            'simplificationMethod': set_simplification_method(filename)
        }
        if ("+units=m" in info['PROJ4'] or "+units=Met" in info['PROJ4']) and str(info['EPSG']) == '4326':
            info['EPSG'] = None

        info['num_bands'] = data_source.RasterCount
        info['data_type'] = gdal.GetDataTypeName(data_source.GetRasterBand(1).DataType)
        info['has_colorTable'] = 0
        lookup_table = get_lookup_table(data_source)
        if lookup_table is not None:
            # return  if contains palette, mapscript does not apply stretch
            info['has_colorTable'] = 1
            info['lookup_table'] = lookup_table
    data_source = None
    #del data_source

    return info


def get_num_bands(filename):
    """ Get number of bands """
    gdal.AllRegister()
    data_source = gdal.Open(filename, 0)
    bands = data_source.RasterCount
    data_source = None
    return range(bands)


def get_lookup_table(data_source):
    """ Get lookup table """
    lookup_table = data_source.GetRasterBand(1).GetColorTable()
    if lookup_table is not None:
        colors = dict()
        num_colors = lookup_table.GetCount()
        for i in range(num_colors):
            color_entry = lookup_table.GetColorEntry(i)
            # if color_entry[0] == 0 and color_entry[1] == 0 and color_entry[2] == 0:
            #     pass
            # else:
            colors[i] = color_entry
        return colors
    return None


def calculate_raster_statistics(filename, force_no_stats=False):
    """ Calculate statistics of a raster file (MULTIPROCESSING) """
    import time
    import psutil
    import multiprocessing
    now_time = time.time()

    bands = get_num_bands(filename)
    num_unique_values = []
    unique_values = []
    statistics = []
    if force_no_stats:
        statsMethod = 'None'
    else:
        statsMethod = set_simplification_method(filename)
    num_cpu = psutil.cpu_count()
    num_cpu = int(num_cpu * 0.6) if num_cpu > 2 else 1
    num_cpu = min(num_cpu,len(bands))
    #
    # add test on memory size
    #
    pool = multiprocessing.Pool(num_cpu)
    result = [pool.apply_async(func=calculate_band_statistics, args=(filename, band_index, statsMethod)) for band_index in bands]
    pool.close()
    pool.join()
    for res in result:
        band_stats = res.get()
        statistics.append(band_stats['statistics'])
        num_unique_values.append(band_stats['num_unique_values'])
        unique_values.append(band_stats['unique_values'])
   # print time.time() - now_time
    return str(statistics), str(num_unique_values), str(unique_values)


def calculate_band_statistics(filename, band, statsMethod):
    """ Calculate statistics for a single band """
    gdal.AllRegister()
    stats = [0,0,0,0]
    raster_info = dict()
    gdal.UseExceptions()

    try:
        data_source = gdal.Open(filename, 0)
        #data_source.GetRasterBand(band + 1).ComputeStatistics(0)  # 0 = approximate = false
        # raster_info['statistics'] = data_source.GetRasterBand(band + 1).GetStatistics(1, 0)
        if statsMethod == '':
            try:
                stats = data_source.GetRasterBand(band + 1).ComputeStatistics(0)  # 0 = approximate = false
                raster_info['statistics'] = stats
            except Exception as e:
                print(e)
                raster_info['statistics'] = [0,0,0,0]
            raster_info['unique_values'], raster_info['num_unique_values'] = calculate_unique_values(data_source, band)

        if statsMethod == 'approximate':
            #print('Calculating Approximate Stats')
            try:
                stats = data_source.GetRasterBand(band + 1).ComputeStatistics(1)  # 0 = approximate = false
                raster_info['statistics'] = stats
            except Exception as e:
                print(e)
                raster_info['statistics'] = [0, 0, 0, 0]
            raster_info['unique_values'], raster_info['num_unique_values'] = [],[]

        if statsMethod == 'None':
            raster_info['statistics'] = [0, 0, 0, 0]
            raster_info['unique_values'], raster_info['num_unique_values'] = [], []

    except RuntimeError as e:  # <- Check first what exception is being thrown
        print('Error in calculate_band_statistics')
        print(e)
        pass
    #del data_source
    data_source = None
    return raster_info


def calculate_unique_values(data_source, band):
    """ Calculate unique_values for a single band """
    unique_values = []

    if gdal.GetDataTypeName(data_source.GetRasterBand(1).DataType) in ['Byte', 'UInt16', 'Int16']:
        raster_band = data_source.GetRasterBand(band + 1)
        num_columns = data_source.RasterXSize
        num_rows = data_source.RasterYSize
        # loop entire ime (recalc also 1st line )
        step = 500
        for row_index in range(0,num_rows,step):
            if row_index+step > num_rows:
                step = num_rows - row_index
            row_values = raster_band.ReadAsArray(0, row_index, num_columns, step)
            tmp_unique_values = numpy.unique(row_values)
            # test on length here since the bottleneck is numpy.unique(numpy.append())  - slower the io for each line
            if tmp_unique_values.size > UNIQUE_VALUES_THRESHOLD:
                return [], 0
            else:
                unique_values = numpy.unique(numpy.append(unique_values, tmp_unique_values))
            # stop as soon as exceed the threshold
            if unique_values.size > UNIQUE_VALUES_THRESHOLD:
                return [], 0

        #unique_values = map(int, unique_values.tolist()) #removed since DB field is in byte already
        unique_values = unique_values.tolist()
    return unique_values, len(unique_values)


def get_code_from_projname(name):
    db_conn = sqlite3.connect(GLOBALS['projdb'])
    cursor = db_conn.cursor()
    sql = "select auth_name,CODE from crs_view where name = '"+name+"' limit 1"
    cursor.execute(sql)
    db_conn.commit()
    records = cursor.fetchall()
    cursor.close()
    db_conn.close()
    if len(records) > 0:
        return str(records[0][0] + ":" + records[0][1])
    else:
        return None
