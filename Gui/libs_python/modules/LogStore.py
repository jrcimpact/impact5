# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import core libraries
from datetime import datetime
from time import time

# Import local libraries
from __config__ import *
import Database


class LogProcessing:

    db_file = GLOBALS['logs_db']
    db_table = "logs"
    printToShell = True

    log_id = ''
    log_structure = {
        # metadata
        'id': '',
        'PID': '',
        'toolID': '',
        'startTime': '',
        'status': '',
        'numErrors': 0,
        'numWarnings': 0,
        # content
        'title': '',
        'date': '',
        'inputs': [],
        'params': [],
        # messages
        'messages': [],
        'errors': []
    }

    def __init__(self, title=None, tool_id=None):
        """ Initialize DB object and connection """
        self.db = Database.Database(self.db_file)
        self.db.create_table(self.db_table, self.log_structure.keys())
        if title is not None and tool_id is not None:
            self.create(title, tool_id)

    def create(self, title, tool_id):
        """ Initialize a new log """
        now = datetime.now().strftime("%Y%m%d%H%M%S%f")[:-5]
        self.log_id = now + '_' + tool_id
        # metadata
        self.log_structure['id'] = self.log_id
        self.log_structure['PID'] = str(os.getpid())
        self.log_structure['toolID'] = tool_id
        self.log_structure['startTime'] = now
        # content
        self.log_structure['title'] = title
        self.log_structure['date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.log_structure['status'] = 'running'
        # insert into DB
        self.db.insert(self.db_table, self.log_structure)

    def get(self, log_id):
        """ Retrieve existing log from DB """
        records = self.db.select(self.db_table, '*', [['id', '=', log_id]])
        if records:
            self.log_id = records[0]['id']
            self.log_structure = records[0]

    def get_all(self):
        """ Retrieve all logs from DB """
        return self.db.select(self.db_table, fields='*', order_by='startTime')

    def remove(self, log_id=None):
        """ Remove a log file """
        log_id = log_id if log_id else self.log_id
        self.db.delete(self.db_table, [['id', '=', log_id]])

    def __update(self):
        """ Write updated to DB """
        self.db.update(self.db_table, self.log_structure, [['id', '=', self.log_id]])

    def set_input_file(self, input_files):
        """ Set processing input files """
        if isinstance(input_files, list):
            for input_file in input_files:
                self.log_structure['inputs'].append(str(input_file))
        else:
            self.log_structure['inputs'].append(str(input_files))
        self.__update()

    def set_parameter(self, param_label, param_value):
        """ Set processing parameters """
        self.log_structure['params'].append({'label': str(param_label), 'value': str(param_value)})
        self.__update()

    def send_message(self, message):
        """ Send message to log """
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        message = '['+str(now)+'] '+str(message)
        if self.printToShell:
            print(message)
        self.log_structure['messages'].append(message)
        self.__update()

    def send_warning(self, warning_msg):
        """ Send warning to log """
        self.log_structure['numWarnings'] = int(self.log_structure['numWarnings']) + 1
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.send_message('__WARNING__' + str(warning_msg))
        self.log_structure['errors'].append('[' + str(now) + ']__WARNING__' + str(warning_msg))
        self.__update()

    def send_error(self, error_msg):
        """ Send error to log """
        self.log_structure['numErrors'] = int(self.log_structure['numErrors']) + 1
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.send_message('__ERROR__' + str(error_msg))
        self.log_structure['errors'].append('[' + str(now) + ']__ERROR__' + str(error_msg))
        self.__update()

    def close(self):
        """ Close log """
        # Failed
        if int(self.log_structure['numErrors']) > 0:
            self.log_structure['status'] = 'error'
        # Completed with warnings
        elif int(self.log_structure['numWarnings']) > 0:
            self.log_structure['status'] = 'warning'
        # Completed
        else:
            self.log_structure['status'] = 'success'
        self.__update()

    def removeAll(self):
        self.db.deleteAll(self.db_table)

def remove(log_id):
    """ Remove a log file """
    logs = LogProcessing()
    logs.remove(log_id)


def remove_all():
    """ Remove a log file """
    logs = LogProcessing()
    logs.removeAll()


def get_all():
    """ Retrieve all log files """
    logs = LogProcessing()
    return logs.get_all()

def updated_recently(wait=10):
    """ Check if the database had been updated recently """
    store = LogProcessing()
    db_time = int(os.path.getmtime(store.db_file))
    now_time = int(time())
    return True if now_time-db_time <= wait else False

