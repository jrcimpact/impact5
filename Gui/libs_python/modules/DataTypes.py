# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


# ##########################################
# ############    dictionary    ############
# ##########################################
import functools

def get_from_dict(data, map_list):
    """ Get a given data from a dictionary with position provided as a list """
    return functools.reduce(lambda d, k: d[k], map_list, data)


def set_in_dict(data, map_list, value):
    """ Set a given data in a dictionary with position provided as a list """
    get_from_dict(data, map_list[:-1])[map_list[-1]] = value


def traverse_dict(dic, path=None):
    """ Traverse a multilevel dictionary
        Output for each item the position (as list) and the value  """
    if not path:
        path = []
    if isinstance(dic, dict):
        for x in dic.keys():
            local_path = path[:]
            local_path.append(x)
            for b in traverse_dict(dic[x], local_path):
                yield b
    else:
        yield path, dic


# #######################################
# ############    boolean    ############
# #######################################

def str2bool(input_string):
    return input_string.lower() in ("yes", "true", "t", "1")
