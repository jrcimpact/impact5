# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import git
import time
import sys

# Import local libraries
from __config__ import *
import LogStore


def current_version():
    # if sys.platform != "linux":
    try:
        repo = GitRepository()
        repo.read()
        return repo.current_version()
    except Exception as e:
        print(e)
        return 'NOT AVAILABLE'
    # else:
    #     return 'CLimateStation'

def get_status():
    log = LogStore.LogProcessing('Check for IMPACT updates', 'update')
    log.printToShell = False
    log.send_message("Checking for updates ...")
    log.send_message("Online:"+GLOBALS['online'])


    # if sys.platform != "linux":
    try:
        repo = GitRepository()
        # -- get local info
        repo.read()
        repo.local_info()
        # -- get remote info
        repo.info['remote'] = repo.info['current']
        if 'online' in GLOBALS and GLOBALS['online'] == "True":
            repo.remote_info()
        # -- check if is up-to-date
        repo.compare_version()
    except Exception as exception:
        log.send_error("GIT: " + str(exception))
        # log.close()
    else:
        if not repo.info['up-to-date']:
            log.send_message("<b>New version available !!</b>")
            # log.close()
        else:
            log.remove()
    log.close()
    return repo.info
    # else:
    #     log.close()
    #     info = dict()
    #     info['up-to-date'] = True
    #     info['synced'] = True
    #     info['commits'] = []
    #     info['current'] = dict()
    #     info['remote'] = dict()
    #     info['branch'] = ''
    #     return info




class GitRepository:

    __git_repository_path = GLOBALS['root_path']
    __stable_branch_name = 'master'                 # stable releases branch
    __development_branch_name = 'development'       # unstable release branch
    __remote_repository_name = 'origin'
    __max_commit_history = 1

    current_branch = ''
    local_repository = None
    remote_repository = None

    def __init__(self):
        git.Git.USE_SHELL = False  # in docker True returns 'git usage'
        self.info = self.info_structure()

    def read(self):
        try:
            self.local_repository = git.repo.base.Repo(self.__git_repository_path)
            self.current_branch = self.local_repository.active_branch
            self.remote_repository = self.local_repository.remotes[self.__remote_repository_name]
        except Exception as exception:
            raise Exception('Unable to access repository ('+str(exception)+')')

    def on_master_branch(self):
        """ Check if the current HEAD is on master or not """
        if self.current_branch.name == self.__stable_branch_name:
            return True
        return False

    def development_branch_exist(self):
        """ Check if "development" branch exists """
        for branch in self.local_repository.heads:
            if branch.name == self.__development_branch_name:
                return True
        return False

    def switch_branch(self):
        if self.on_master_branch():
            self.move_to_development()
        else:
            self.move_to_stable()

    def move_to_stable(self):
        """  Checkout to stable (master) branch """
        if not self.on_master_branch():
            self.local_repository.heads[self.__stable_branch_name].checkout()
            self.current_branch = self.local_repository.active_branch
            print('Moved to stable branch.')

    def move_to_development(self):
        """  Checkout to development branch """
        if self.on_master_branch():
            if not self.development_branch_exist():
                self.local_repository.create_head(self.__development_branch_name,
                                                  self.remote_repository.refs[self.__development_branch_name])
            self.local_repository.heads[self.__development_branch_name].checkout(force=True)
            self.current_branch = self.local_repository.active_branch
            print('Moved to development branch.')

    def current_version(self):
        """ Get current local version """
        last_commit = self.current_branch.commit
        info = self.__parse_commit(last_commit)
        return info['version']

    def local_info(self):
        """ Get local repository info """
        try:
            self.info['branch'] = self.current_branch.name
            # Local repository info
            last_commits = list(self.local_repository.iter_commits(self.current_branch.name,
                                                                   max_count=self.__max_commit_history))
            for commit in last_commits:
                self.info['commits'].append(self.__parse_commit(commit))
            self.info['current'] = self.__parse_commit(self.current_branch.commit)
        except Exception as exception:
            raise Exception('Unable to read local repository (' + str(exception) + ')')

    def remote_info(self):
        try:
            """ Retrieve remote repository info """
            self.remote_repository.update()
            last_remote_commit = self.remote_repository.refs[self.current_branch.name].commit
            self.info['remote'] = self.__parse_commit(last_remote_commit)
        except Exception as exception:
            raise Exception('Unable to read remote repository (' + str(exception) + ')')

    def compare_version(self):
        """ Compare local/remote versions """
        self.info['up-to-date'] = True
        self.info['synced'] = False
        if self.info['remote'] in self.info['commits']:
            self.info['synced'] = True
        if 'version' in self.info['current'] and (
                    not self.info['synced'] or
                    self.info['current'] != self.info['commits'][0]):
            self.info['up-to-date'] = False

    @staticmethod
    def info_structure():
        info = dict()
        info['up-to-date'] = True
        info['synced'] = True
        info['commits'] = []
        info['current'] = dict()
        info['remote'] = dict()
        info['branch'] = ''
        return info

    @staticmethod
    def __parse_commit(commit):
        msg = commit.message.split('\n', 1)
        commit_info = {
            'hash': str(commit.hexsha),
            'date': time.strftime('%Y-%m-%d', time.gmtime(commit.authored_date)),
            'version': str(msg[0]),
            'changelog': str(msg[1])
        }
        return commit_info
