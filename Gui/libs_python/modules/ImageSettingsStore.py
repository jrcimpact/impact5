# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import local libraries
from __config__ import *
import Database
import json

#import sys
# reload(sys)
# sys.setdefaultencoding("utf-8")

def delete_file(full_path):
    """ Delete the given image from the database """
    store = ImageSettingsStore()
    return store.delete(full_path)

def save_file(full_path, settings):
    """ Delete the given image from the database """
    store = ImageSettingsStore()
    return store.insert(full_path, settings)

def create():
    """ Analyze the filesystem and populate the database """
    store = ImageSettingsStore()

def get_all():
    """ Retrieve the full image list (all DB) """
    store = ImageSettingsStore()
    return store.get_all()


def get_by_path(full_path):
    """ Retrieve image info """
    store = ImageSettingsStore()
    return store.get_by_path(full_path)



class ImageSettingsStore:

    __database_dump = []

    def __init__(self):
        """ Initialize DB controller """
        self.root_path = GLOBALS['root_path']
        self.db_file = GLOBALS['image_settings_store']
        self.db_table = "image_settings_store"
        self.file_info_structure = {
            # File related information
            'full_path': '',
            'settings': ''
        }

        # initialize DB object and connection
        self.db = Database.Database(self.db_file)
        self.db.create_table(self.db_table, self.file_info_structure.keys())

    def insert(self, full_path, file_info, in_transaction=False):
        try:
            self.delete(full_path)
            self.db.insert(self.db_table, file_info, in_transaction=in_transaction)
            return 'true'
        except Exception as e:
            return 'false :'+str(e)

    def delete(self, full_path, in_transaction=False):
        """ Delete a record from the DB """
        try:
            self.db.delete(self.db_table, [['full_path', '=', full_path]], in_transaction=in_transaction)
            return 'true'
        except Exception as e:
            return 'false: '+str(e)

    def get_all(self):
        """ Get all the records from the DB """
        records = self.db.select(self.db_table, '*', None, 'full_path ASC')
        for item in records:
            item['settings'] = json.dumps(item['settings'])
        return records

    def get_by_path(self, full_path):
        """ Query DB by full_path (only 1 record expected) """
        records = self.db.select(self.db_table, '*', [['full_path', '=', full_path]])
        if records:
            return records[0]
        return None
