# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import urllib3
import urllib

# Import local libraries
from __config__ import *

import GitRepository
import Settings
import os
import sys

def test_proxy(pip):
    import urllib.request
    import socket
    import urllib.error
    try:
        proxy_handler = urllib.request.ProxyHandler({'https': pip})
        opener = urllib.request.build_opener(proxy_handler)
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        urllib.request.install_opener(opener)
        req = urllib.request.Request('https://bitbucket.org/jrcimpact/impact5.git')  # change the URL to test here
        sock = urllib.request.urlopen(req)
    except urllib.error.HTTPError as e:
        print('Error code: ', e.code)
        return False
    except Exception as detail:
        print("ERROR:", detail)
        return False
    return True

def is_online():
    SETTINGS = Settings.load()

    # proxy.pid is used as global war in Win to set proxy on python and windows script
    # since proxy is not set in ENV or START.bat file
    # if present GEE lib is setting the proxy before authentication and ee.Initialize()
    # 2do: set global proxy
    if os.path.exists(GLOBALS['proxy']):
        try:
            os.remove(GLOBALS['proxy'])
        except Exception as e:
            print(e)
            pass

    try:
        print("Testing internet connection")
        http = urllib3.PoolManager()
        response = http.request('GET', 'https://bitbucket.org/jrcimpact/impact5.git', timeout=5, retries=False)
        print('Response:', response.status)
        if response.status == 200:
            print('Online')
            print(sys.platform)
            if sys.platform != "linux":
                os.system("git config --local --unset http.proxy")
                os.system("git config --local --unset https.proxy")

            return True
        else:
            return False
    except Exception as e:
        print(e)
        try:
            # internal proxy
            print("Internet not accessible, try using proxy")

            if SETTINGS['internet_options']['username'] != "":
                proxySTR = SETTINGS['internet_options']['username']+':'+SETTINGS['internet_options']['password']+'@'+SETTINGS['internet_options']['proxy']
            else:
                proxySTR = SETTINGS['internet_options']['proxy']


            response = test_proxy(proxySTR)
            if response:
                print("Set proxy")
                if not os.path.exists(GLOBALS['proxy']):
                    open(GLOBALS['proxy'], 'w').close()

                # unix container
                # proxy on git set on dockerfile
                # windows
                if sys.platform != "linux":
                    print('Setting git')
                    os.system("git config --local http.proxy http://" + proxySTR)
                    os.system("git config --local https.proxy http://" + proxySTR)
                    os.system("set HTTP_PROXY=http://" + proxySTR)
                    os.system("set HTTPS_PROXY=http://" + proxySTR)
                    os.environ["HTTPS_PROXY"] = "http://" + proxySTR
                    os.environ["HTTP_PROXY"] = "http://" + proxySTR
                return True
            else:
                print('Internet not accessible by using proxy')
        except Exception as e:
            print('Error Internet not accessible by using proxy')
            print(str(e))
            return False

        return False
    return False


def get_version():
    """ Alias to GitRepository.current_version() """
    return GitRepository.current_version()
