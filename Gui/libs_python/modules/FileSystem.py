# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


# Import global libraries
import os
import sys
import ctypes
import itertools
import string
from __config__ import *

def get_root_folders():
    dirs = []
    if sys.platform == 'win32':
        drive_bitmask = ctypes.cdll.kernel32.GetLogicalDrives()
        dirs = list(
            itertools.compress(string.ascii_uppercase, map(lambda x: ord(x) - ord('0'), bin(drive_bitmask)[:1:-1])))
        dirs = [i+':' for i in dirs]
    else:
        dirs = next(os.walk(GLOBALS['data_paths']['unix_raw_data']))[1]
        dirs = [GLOBALS['data_paths']['unix_raw_data'] + i for i in dirs]

    return dirs


def traverse_recursively(path):
    """ List all the files (full path) in the given directory (including all sub-directories) """
    file_list = []
    for root, dirs, files in os.walk(path):
        for filename in files:
            if not root.endswith('/'):
                full_path = root + '/' + filename
            else:
                full_path = root + filename
            file_list.append(full_path.replace('\\', '/'))
    return file_list


def list_files(path, end_with=''):
    """ List files in the given directory """
    files = []
    if not path.endswith('/'):
        path += '/'
    if os.path.isdir(path):
        for f in os.listdir(path):
            if end_with == '' or f.lower().endswith(end_with.lower()):
                files.append(f)
    return files


def list_sub_folders(path):
    """ List subdir in the given directory """
    sub_folders = []
    if not path.endswith('/'):
        path += '/'

    if os.path.isdir(path):
        for item in os.listdir(path):
            if os.path.isdir(path+item):
                sub_folders.append(item)
    return sub_folders


def split_filename(fullname):
    """ List subdir in the given directory """
    (path, filename) = os.path.split(fullname)
    (basename, extension) = os.path.splitext(filename)
    return path, basename, extension, filename


def path_length_is_valid(path, buffer_length=5):
    """ Verify if the file path is not too long """
    max_length_allowed = 250
    count = len(path) + buffer_length
    if count > max_length_allowed:
        return False
    return True
