# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import json

# Import local libraries
from __config__ import *
import DataTypes


class Settings:

    def __init__(self):
        self.defaultFileName = GLOBALS['default_settings']
        self.FileName = GLOBALS['settings']
        # create user's file (if not exist)
        if not os.path.isfile(self.FileName):
            self.reset()

    def reset(self):
        """ Reset to default """
        import shutil
        if os.path.isfile(self.FileName):
            shutil.move(self.FileName, self.FileName+'_bkup')
        shutil.copy(self.defaultFileName, self.FileName)

    @staticmethod
    def __read_file(file_name):
        """ Read file """
        file_handler = open(file_name, 'r')
        return file_handler.read()

    @staticmethod
    def __write_file(file_name, file_content):
        """ Write file """
        file_handler = open(file_name, 'w')
        file_handler.write(file_content)

    def read(self, as_json_string=False):
        """ Get the settings parameters """
        file_content = self.__read_file(self.FileName)
        return json.loads(file_content) if not as_json_string else file_content

    def save(self, new_settings):
        """ Save/Overwrite settings file """
        new_settings_obj = json.loads(new_settings)
        current_settings_obj = self.read()
        # replace changes parameters
        changed = False
        for item in DataTypes.traverse_dict(current_settings_obj):
            item_value = item[1]
            item_path = item[0]
            item_concatenate_key = ''
            for i in item_path:
                item_concatenate_key += i+'_'
            item_concatenate_key = item_concatenate_key[:-1]
            if item_concatenate_key in new_settings_obj.keys():
                if new_settings_obj[item_concatenate_key] != item_value:
                    DataTypes.set_in_dict(current_settings_obj, item_path, new_settings_obj[item_concatenate_key])
                    changed = True
        # write changes to file
        if changed:
            self.__write_file(self.FileName, json.dumps(current_settings_obj))
        return True

    def upgrade(self):
        if os.path.isfile(self.FileName):
            user_settings_obj = json.loads(self.__read_file(self.FileName))
            # tmp_user_settings_obj = json.loads(self.__read_file(self.FileName))

            new_default_settings_obj = json.loads(self.__read_file(self.defaultFileName))
            if user_settings_obj != new_default_settings_obj:
                # update settings file version
                version_changed = False
                if user_settings_obj['settings_file_version'] != new_default_settings_obj['settings_file_version']:
                    tmp_user_settings_obj = json.loads(self.__read_file(self.defaultFileName))
                    version_changed = True
                    print('User settings with different version')
                    #
                    # ADD HERE CODE TO FILL THE NEW SETTINGS WITH USER'S
                    #

                # push new legends
                    user_legends = []
                    for leg in user_settings_obj['map_legends']['vector']:
                        user_legends.append(leg['type'])
                    new_legends = []
                    for leg in new_default_settings_obj['map_legends']['vector']:
                        new_legends.append(leg['type'])

                    for leg in new_default_settings_obj['map_legends']['vector']:
                        if leg['type'] not in user_legends :
                            tmp_user_settings_obj['map_legends']['vector'].append(leg)
                    for leg in user_settings_obj['map_legends']['vector']:
                        if leg['type'] not in new_legends:
                            tmp_user_settings_obj['map_legends']['vector'].append(leg)

                # push new legends
                    user_legends = []
                    for leg in user_settings_obj['map_legends']['raster']:
                        user_legends.append(leg['type'])
                    new_legends = []
                    for leg in new_default_settings_obj['map_legends']['raster']:
                        new_legends.append(leg['type'])

                    for leg in new_default_settings_obj['map_legends']['raster']:
                        if leg['type'] not in user_legends:
                            tmp_user_settings_obj['map_legends']['raster'].append(leg)
                    for leg in user_settings_obj['map_legends']['raster']:
                        if leg['type'] not in new_legends:
                            tmp_user_settings_obj['map_legends']['raster'].append(leg)




                    tmp_user_settings_obj['internet_options'] = user_settings_obj['internet_options']
                    tmp_user_settings_obj['apiKey_options'] = user_settings_obj['apiKey_options']

                    tmp_user_settings_obj['map_options'] = user_settings_obj['map_options']
                    tmp_user_settings_obj['processing_classification'] = user_settings_obj['processing_classification']
                    tmp_user_settings_obj['vector_editing'] = user_settings_obj['vector_editing']
                    tmp_user_settings_obj['wms_options'] = user_settings_obj['wms_options']

                    self.__write_file(self.FileName, json.dumps(tmp_user_settings_obj))



# #############################
# ########   Aliases  #########
# #############################

def load():
    """ Get the settings parameters """
    settings_obj = Settings()
    return settings_obj.read(False)


def load_as_json():
    """ Get the settings parameters as JSON string """
    settings_obj = Settings()
    return settings_obj.read(True)


def save(new_settings):
    """ Save new settings """
    settings_obj = Settings()
    return settings_obj.save(new_settings)


def delete():
    """ Get the settings parameters """
    settings_obj = Settings()
    return settings_obj.reset()


def upgrade():
    """ Upgrade user's file to latest version """
    settings_obj = Settings()
    return settings_obj.upgrade()
