#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import numpy
import sys
import glob
import os
import time
import math
import random

# Import local libraries
from __config__ import *
import Settings

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr, gdalconst
except ImportError:
    import ogr
    import gdal
    import osr
    import gdalconst

SETTINGS = Settings.load()

gdal.TermProgress = gdal.TermProgress_nocb


def MYmode2D_modified(a):
    # print "All"
    # print a.tolist()

    for i in range(len(a) - 1):
        for z in range(len(a[i]) - 1):
            if a[i][z] >= 0:
                # out.append(PBS2PBSsimple(a[i][z]))
                a[i][z] = (PBS2PBSsimple(a[i][z]))
    # a=out
    # print a.tolist()
    uniqueValues = numpy.unique(a).tolist()
    # print "Uniq Values"
    # print uniqueValues
    # print "######"
    uniqueCounts = [len(numpy.nonzero(a == uv)[0])
                    for uv in uniqueValues]
    # print uniqueCounts
    modeIdx = uniqueCounts.index(max(uniqueCounts))
    # mymode = a[modeIdx]
    # print "-------------"
    # print uniqueValues
    # print uniqueCounts
    # print modeIdx
    # print uniqueValues[modeIdx]
    # print len(uniqueValues)
    # print "------X------"
    # All counts as a map
    valueToCountMap = dict(zip(uniqueValues, uniqueCounts))
    # print valueToCountMap
    if len(uniqueValues) == 2:
        return uniqueValues[modeIdx]
    else:
        # print uniqueValues
        # print uniqueCounts
        # print sum(uniqueCounts)
        # print uniqueCounts[modeIdx]
        PROP = (uniqueCounts[modeIdx]) / float(sum(uniqueCounts)) * 100
        if PROP > 60:
            return uniqueValues[modeIdx]  # have to be better defined , test for absolute majority or other combinatons

        if PROP > 50:
            if uniqueValues[modeIdx] == 10: return 11

            if uniqueValues[modeIdx] == 23: return 24
            if uniqueValues[modeIdx] == 20: return 26
            if uniqueValues[modeIdx] == 14: return 26
            return uniqueValues[modeIdx]
        if PROP > 30:
            if uniqueValues[modeIdx] == 10: return 23
            if uniqueValues[modeIdx] == 23: return 24
            if uniqueValues[modeIdx] == 20: return 26
            if uniqueValues[modeIdx] == 14: return 26
            return uniqueValues[modeIdx]
        # else :
        #	#if uniqueValues[modeIdx] == 25 : return 25
        return uniqueValues[modeIdx]


def MYmode2D(a):
    uniqueValues = numpy.unique(a).tolist()

    uniqueCounts = [len(numpy.nonzero(a == uv)[0])
                    for uv in uniqueValues]

    modeIdx = uniqueCounts.index(max(uniqueCounts))
    # mymode = a[modeIdx]
    # print "-------------"
    # print uniqueValues
    # print uniqueCounts
    # print modeIdx
    # print uniqueValues[modeIdx]
    # print "-------------"
    # All counts as a map
    # valueToCountMap = dict(zip(uniqueValues, uniqueCounts))
    # print valueToCountMap
    return uniqueValues[modeIdx]


def bbox_to_pixel_offsets(gt, bbox):
    originX = gt[0]
    originY = gt[3]
    pixel_width = gt[1]
    pixel_height = gt[5]
    x1 = ((bbox[0] - originX) / pixel_width)
    x2 = ((bbox[1] - originX) / pixel_width)  # + 1

    y1 = ((bbox[3] - originY) / pixel_height)
    y2 = ((bbox[2] - originY) / pixel_height)  # + 1

    xsize = x2 - x1
    ysize = y2 - y1
    return (int(numpy.round(x1, 0)), int(numpy.round(y1, 0)), int(xsize), int(ysize))


def SD2Class(cla):
    try:
        cl = int(cla)
        res = cl
        if cl == 1: res = 15
        if cl == 2: res = 15
        if cl == 3: res = 15

        if cl == 5: res = 13
        if cl == 6: res = 13
        if cl == 7: res = 13
        if cl == 8: res = 13

        if cl == 9: res = 9
        if cl == 10: res = 1
        if cl == 11: res = 1
        if cl == 12: res = 1
        if cl == 13: res = 7
        if cl == 14: res = 9
        if cl == 16: res = 9
        if cl == 21: res = 12
        if cl == 22: res = 12
        if cl == 30: res = 14
        if cl == 31: res = 14
        if cl == 32: res = 14
        if cl == 34: res = 11
        if cl == 35: res = 11
        if cl == 40: res = 1
        if cl == 41: res = 11
        if cl == 42: res = 11
        if cl == 99: res = 0

        return res
    except:
        return 0


def SD2Class_modified(cla):
    try:
        cl = int(cla)
        res = cl
        if cl == 1: res = 21
        if cl == 2: res = 21
        if cl == 3: res = 21

        if cl == 5: res = 18
        if cl == 6: res = 17
        if cl == 7: res = 18
        if cl == 8: res = 18

        if cl == 9: res = 17
        if cl == 10: res = 10
        if cl == 11: res = 10
        if cl == 12: res = 10
        if cl == 13: res = 10
        if cl == 14: res = 14
        if cl == 15: res = 11
        if cl == 16: res = 13
        if cl == 20: res = 23
        if cl == 21: res = 24
        if cl == 25: res = 23
        if cl == 26: res = 24

        if cl == 28: res = 25
        if cl == 29: res = 25
        if cl == 30: res = 25
        if cl == 31: res = 25

        if cl == 32: res = 26
        if cl == 34: res = 26
        if cl == 35: res = 20
        if cl == 36: res = 20

        if cl == 40: res = 10
        if cl == 41: res = 20
        if cl == 42: res = 10
        if cl == 99: res = 0

        return res
    except:
        return 0


def PBS2Class(cla):
    try:
        cl = int(cla)
        res = cl
        if cl == 0: res = 0
        if cl == 1: res = 15
        if cl == 2: res = 15
        if cl == 3: res = 17

        if cl == 6: res = 13
        if cl == 7: res = 13
        if cl == 8: res = 13
        if cl == 9: res = 13

        if cl == 10: res = 1
        if cl == 11: res = 1
        if cl == 12: res = 2
        if cl == 13: res = 3
        if cl == 14: res = 7
        if cl == 15: res = 7
        if cl == 16: res = 7

        if cl == 20: res = 3
        if cl == 21: res = 3
        if cl == 25: res = 9
        if cl == 26: res = 9

        if cl == 27: res = 12

        if cl == 28: res = 9
        if cl == 29: res = 9
        if cl == 30: res = 9

        if cl == 31: res = 14
        if cl == 32: res = 14
        if cl == 34: res = 14
        if cl == 35: res = 11
        if cl == 36: res = 11

        if cl == 41: res = 11
        if cl == 42: res = 11

        if cl == 99: res = 0

        return res

    except:
        return 0


def PBS2PBSsimple(cla):
    try:
        cl = int(cla)
        res = cl
        if cl == 0: res = 0
        if cl == 1: res = 15
        if cl == 2: res = 2
        if cl == 3: res = 2

        if cl == 6: res = 8
        if cl == 7: res = 8
        if cl == 8: res = 8
        if cl == 9: res = 8

        if cl == 10: res = 10
        if cl == 11: res = 10
        if cl == 12: res = 10
        if cl == 13: res = 11
        if cl == 14: res = 14
        if cl == 15: res = 15
        if cl == 16: res = 16

        if cl == 20: res = 20
        if cl == 21: res = 20
        if cl == 25: res = 20
        if cl == 26: res = 20

        if cl == 27: res = 8

        if cl == 28: res = 28
        if cl == 29: res = 29
        if cl == 30: res = 29

        if cl == 31: res = 31
        if cl == 32: res = 31
        if cl == 34: res = 34
        if cl == 35: res = 34
        if cl == 36: res = 34

        if cl == 41: res = 34
        if cl == 42: res = 10

        if cl == 99: res = 0

        return res

    except:
        return 0


def RUN_populate_shapefile_with_class(CLASS, SHP, AggregationRules):
    tstart = time.time()
    print("Adding fields...")

    shp = ogr.Open(SHP, update=1)
    shplayer = shp.GetLayer(0)
    layerDefinition = shplayer.GetLayerDefn()

    ShpFieldList = []
    for i in range(layerDefinition.GetFieldCount()):
        ShpFieldList.append(str(layerDefinition.GetFieldDefn(i).GetName()))

    if not os.path.exists(CLASS):
        # ------------   ADD BASIC LAYER ID;cluster;class ---------------
        MyFieldList = [str(SETTINGS['vector_editing']['attributes']['ID']),
                       str(SETTINGS['vector_editing']['attributes']['class_t1']),
                       str(SETTINGS['vector_editing']['attributes']['cluster_t1']),
                       str(SETTINGS['vector_editing']['attributes']['comments'])
                       ]
    else:
        class_ds = gdal.Open(CLASS, gdal.GA_ReadOnly)
        num_bands = class_ds.RasterCount

        MyFieldList = [str(SETTINGS['vector_editing']['attributes']['ID']),
                       str(SETTINGS['vector_editing']['attributes']['class_t1']),
                       str(SETTINGS['vector_editing']['attributes']['cluster_t1'])]
        if num_bands == 1:
            print('1 band')
        if num_bands == 2:
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['class_t2']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['cluster_t2']))
        if num_bands == 3:
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['class_t2']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['cluster_t2']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['class_t3']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['cluster_t3']))
        if num_bands > 3:
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['class_t2']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['cluster_t2']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['class_t3']))
            MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['cluster_t3']))
            for b in range(3, num_bands):
                print("Extra band " + str(b))
                MyFieldList.append('T' + str(b + 1) + '_class')
                MyFieldList.append('T' + str(b + 1) + '_cluster')
        MyFieldList.append(str(SETTINGS['vector_editing']['attributes']['comments']))

    # - --------------- ADD FIELDS ---------------------
    for index, field in enumerate(MyFieldList):
        # if any(i in s for s in ShpFieldList):
        if field in ShpFieldList:
            print(field + " found ")
        else:
            if index == 0:
                f1 = ogr.FieldDefn(field, ogr.OFTInteger)  # SET ID
            else:
                f1 = ogr.FieldDefn(field, ogr.OFTString)
                f1.SetWidth(10)
            shplayer.CreateField(f1)
            f1 = None

    # -------------------------------------------------   EMPTY SHP with NO CLASSIF  ---------------------------------------------------
    if not os.path.exists(CLASS):
        feat = shplayer.GetNextFeature()
        while feat is not None:
            feat.SetField(str(SETTINGS['vector_editing']['attributes']['ID']), feat.GetFID())
            feat.SetField(str(SETTINGS['vector_editing']['attributes']['class_t1']), 0)
            feat.SetField(str(SETTINGS['vector_editing']['attributes']['cluster_t1']), 0)
            # comments is empty
            shplayer.SetFeature(feat)
            feat.Destroy()
            feat = shplayer.GetNextFeature()
        shplayer = None
        return 1

    # -------------------------------------------------   POPULATE SHP with VALID CLASSIFICATION   -------------------------------------

    transform = class_ds.GetGeoTransform()
    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]
    # shplayer = shp.GetLayer(0)

    # -----------------------------------------------------------------
    # if cluster mode is selected : return a random land cover class
    # legend should be read from the settings ..... 2bdone
    # -----------------------------------------------------------------
    # LC_class = [1,7,9,14,12,11,13,15,16,17]
    # new code 
    # LC_class = []
    # for item in SETTINGS['map_legends']['vector']:
    #     if item['type'] == legend:
    #         for cl in item['classes']:
    #             LC_class.append(int(cl[0]))
    # print LC_class
    # print "----------------"
    # #time.sleep(10)
    #
    # MAX_bands=[]
    # for i in range(num_bands):
    #     MAX_bands.append(class_ds.GetRasterBand(i+1).GetStatistics(0,1)[1])
    #
    # print "[ MAX ] = ",str(int(max(MAX_bands)))
    # Cluster_Lookup=[]
    #
    # for v in range(int(max(MAX_bands))+1):
    #         if v in LC_class:
    #             Cluster_Lookup.append(v)
    #         else:
    #             Cluster_Lookup.append(LC_class[int(v % len(LC_class))])

    print('filling ....')
    # -------------------------------------------------------------------
    # -------------------------------------------------------------------

    mem_drv = ogr.GetDriverByName('Memory')
    driver = gdal.GetDriverByName('MEM')

    # Loop through vectors

    featureCount = shplayer.GetFeatureCount()

    feat = shplayer.GetNextFeature()

    sourceSR = shplayer.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(class_ds.GetProjectionRef())

    sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    print(sourceSR.ExportToProj4())
    print(targetSR.ExportToProj4())
    coordTrans = osr.CoordinateTransformation(sourceSR, targetSR)

    index = 0.0
    while feat is not None:
        fct = feat.GetFID()
        gdal.TermProgress(index / featureCount)
        index += 1

        feat.SetField(MyFieldList[0], fct)

        feat_geom = feat.GetGeometryRef().Clone()    # clone is necessary to not everwrite the proj


        if sourceSR.ExportToProj4() != targetSR.ExportToProj4():
            feat_geom.Transform(coordTrans)


        x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()
        # featenvelope=feat.geometry().GetEnvelope()
        src_offset = bbox_to_pixel_offsets(transform, [x_min, x_max, y_min, y_max])
        src_offset_list = list(src_offset)
        if src_offset_list[2] == 0:
            src_offset_list[2] = 1
        if src_offset_list[3] == 0:
            src_offset_list[3] = 1
        src_offset = tuple(src_offset_list)
        try:
            # calculate new geotransform of the feature subset
            new_gt = (
                (transform[0] + (src_offset[0] * transform[1])),
                transform[1],
                0.0,
                (transform[3] + (src_offset[1] * transform[5])),
                0.0,
                transform[5]
            )
            mem_ds = mem_drv.CreateDataSource('out')
            mem_layer = mem_ds.CreateLayer('poly', None, ogr.wkbPolygon)
            mem_layer.CreateFeature(feat.Clone())

            # Rasterize it
            rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
            rvds.SetGeoTransform(new_gt)

            gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])

            rv_array = rvds.ReadAsArray()
            #  ------  if polygon covers a tiny portion of a pixel, keep the pixel
            if rv_array.max() == 0:
                rv_array.fill(1)
            rvds = None
            # loop each classification band and set fiels with mode

            ct = 1  # 0 is the ID
            for i in range(num_bands):
                class_band = class_ds.GetRasterBand(i + 1)
                src_array = class_band.ReadAsArray(src_offset[0], src_offset[1], src_offset[2], src_offset[3])
                masked = numpy.ma.MaskedArray(
                    src_array,
                    mask=numpy.logical_not(rv_array)
                )

                # print "Mode Set"
                if AggregationRules == 'PINO':
                    val = SD2Class(MYmode2D(masked))
                elif AggregationRules == 'PBS':
                    val = PBS2Class(MYmode2D(masked))
                elif AggregationRules == 'Majority':
                    # AggregationRules[i] == 'CLUSTER':
                    # feat.SetField(MyFieldList[ct],Cluster_Lookup[modeval])
                    val = MYmode2D(masked)
                elif AggregationRules == 'Min':
                    val = numpy.min(masked)
                elif AggregationRules == 'Max':
                    val = numpy.max(masked)

                feat.SetField(MyFieldList[ct], str(val))
                feat.SetField(MyFieldList[ct + 1], str(val))

                ct = ct + 2

            shplayer.SetFeature(feat)
            feat = shplayer.GetNextFeature()

        except Exception as e:

            feat.SetField(MyFieldList[0], fct)
            ct = 1
            for i in range(num_bands):
                feat.SetField(MyFieldList[ct], 0)
                feat.SetField(MyFieldList[ct + 1], 0)
                ct = ct + 2

            shplayer.SetFeature(feat)
            feat.Destroy()

            rvds = None
            mem_ds = None
            feat = shplayer.GetNextFeature()

    shplayer = None
    class_ds = None
    tend = time.time()
    duration = tend - tstart
    hours, remainder = divmod(duration, 3600)
    minutes, seconds = divmod(remainder, 60)
    print("Completed in " + str(hours) + "hrs " + str(minutes) + "min " + str(seconds) + "sec")


# ----------------------  not used 	 ??? ----------------------------------
def GETmodeFrom_CL(IN):
    CLASS = IN[0]
    SHPIN = IN[1]

    class_ds = gdal.Open(CLASS, gdal.GA_ReadOnly)
    shp = ogr.Open(SHPIN, update=1)

    class_band = class_ds.GetRasterBand(1)
    transform = class_ds.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]

    shplayer = shp.GetLayer(0)

    mem_drv = ogr.GetDriverByName('Memory')
    driver = gdal.GetDriverByName('MEM')

    # Loop through vectors
    feat = shplayer.GetNextFeature()

    sourceSR = shplayer.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(class_ds.GetProjectionRef())
    sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    coordTrans = osr.CoordinateTransformation(sourceSR, targetSR)

    out = []
    for x in range(IN[3] - IN[2]):
        feat = shplayer.GetFeature(IN[2] + x)
        # print IN[2]+x
        feat_geom = feat.GetGeometryRef()  # .Clone()

        x_min, x_max, y_min, y_max = feat_geom.GetEnvelope()
        # featenvelope=feat.geometry().GetEnvelope()
        # featgeom = feat.GetGeometryRef()
        # print featgeom
        # sys.exit()
        # featgeom.Transform(coordTrans)
        # featgeom = feat.GetGeometryRef()
        # print featgeom

        # src_offset = bbox_to_pixel_offsets(transform,featgeom )
        src_offset = bbox_to_pixel_offsets(transform, [x_min, x_max, y_min, y_max])
        src_array = class_band.ReadAsArray(src_offset[0], src_offset[1], src_offset[2], src_offset[3])

        # calculate new geotransform of the feature subset
        new_gt = (
            (transform[0] + (src_offset[0] * transform[1])),
            transform[1],
            0.0,
            (transform[3] + (src_offset[1] * transform[5])),
            0.0,
            transform[5]
        )
        mem_ds = mem_drv.CreateDataSource('out')
        mem_layer = mem_ds.CreateLayer('poly', None, ogr.wkbPolygon)
        mem_layer.CreateFeature(feat.Clone())

        # Rasterize it
        rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
        rvds.SetGeoTransform(new_gt)

        gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])

        rv_array = rvds.ReadAsArray()

        masked = numpy.ma.MaskedArray(
            src_array,
            mask=numpy.logical_not(rv_array)
        )

        # print masked
        try:
            modeval = MYmode2D(masked)
        except:
            modeval = 99
        feat.SetField(0, IN[2] + x)
        feat.SetField(1, modeval)
        feat.SetField(2, modeval)
        feat.SetField(3, modeval)
        feat.SetField(4, SD2Class(modeval))
        feat.SetField(5, SD2Class(modeval))
        feat.SetField(6, SD2Class(modeval))

        shplayer.SetFeature(feat)
        feat.Destroy()
    # out.append([IN[2]+x,modeval])
    shplayer = None
    # print "MAPP "+str(IN[2]+x)+"  DONE"
    return (1)


def RasterizeSHP(IN, OUT, FieldID, pixel_size):
    NoData_value = 0
    source_ds = ogr.Open(IN)
    source_layer = source_ds.GetLayer()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()
    print(x_min, x_max, y_min, y_max)

    # Create the destination data source
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)
    target_ds = gdal.GetDriverByName('GTiff').Create(OUT, x_res, y_res, 1, gdal.GDT_UInt16)
    prj = source_layer.GetSpatialRef()
    print("EPSG:" + prj + "<br>")

    outprj = osr.SpatialReference()
    outprj.ImportFromWkt(prj.ExportToWkt())
    target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
    target_ds.SetProjection(prj.ExportToWkt())

    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(NoData_value)

    # Rasterize
    #
    featList = range(source_layer.GetFeatureCount())
    index = source_layer.GetFeature(0).GetFieldIndex(FieldID)

    for FID in featList:
        feat = source_layer.GetFeature(FID)
        ID = float(feat.GetField(index))
        print("Features: " + str(ID) + "<br>")
        source_layer.SetAttributeFilter(FieldID + " = " + str(ID))
        gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[ID])

    shplayer = None

    pixel_size = 1


def get_image_medians_from_mask(image_file, sat, mask_file):
    try:

        evg_ds = gdal.Open(mask_file)
        evg_geot = evg_ds.GetGeoTransform()
        evgSR = osr.SpatialReference()
        evgSR.ImportFromWkt(evg_ds.GetProjectionRef())

        img_ds = gdal.Open(image_file)
        DataType = img_ds.GetRasterBand(1).DataType
        DataType = gdal.GetDataTypeName(DataType)
        print("Type: " + DataType)

        img_geot = img_ds.GetGeoTransform()
        imgSR = osr.SpatialReference()
        imgSR.ImportFromWkt(img_ds.GetProjectionRef())
        cols = img_ds.RasterXSize
        rows = img_ds.RasterYSize

        mem_drv = gdal.GetDriverByName('MEM')
        dest = mem_drv.Create('', cols, rows, 1, gdal.GDT_Byte)

        dest.SetGeoTransform(img_geot)
        dest.SetProjection(img_ds.GetProjectionRef())
        res = gdal.ReprojectImage(evg_ds, dest, evgSR.ExportToWkt(), imgSR.ExportToWkt(), gdal.GRA_NearestNeighbour)
        FMASK_data = dest.ReadAsArray().astype(numpy.bool)

        modeval = []
        for band in range(img_ds.RasterCount):
            band += 1
            # print "GETTING BAND: ", band
            CURRBAND = img_ds.GetRasterBand(band).ReadAsArray()
            CURRBAND *= FMASK_data

            try:
                if len(CURRBAND[numpy.nonzero(CURRBAND)]):
                    medianval = numpy.nanmedian(CURRBAND[numpy.nonzero(CURRBAND)])
                    print(medianval)
                else:
                    medianval = 0
            except:
                medianval = 0

            CURRBAND = None

            if ((medianval == 0) or (math.isnan(medianval))):
                evg_ds = None
                img_ds = None
                dest = None

                modeval = []
                return modeval

            modeval.append(medianval)
        evg_ds = None
        img_ds = None
        dest = None
        CURRBAND = None

        return modeval

    except Exception as e:
        print(sys.exc_info())
        print(e)
        evg_ds = None
        img_ds = None
        dest = None
        CURRBAND = None

        return []


def test_overlapping(shp_img, img):

    if shp_img.endswith('.shp') or shp_img.endswith('.json') or shp_img.endswith('.geojson'):
        # --------------  IN SHP  -------------------
        #print("Shapefile vs raster")
        shp_ds = ogr.Open(shp_img)
        layer = shp_ds.GetLayer(0)
        INSR = layer.GetSpatialRef()
        INextent = layer.GetExtent()

        #print('INextent')
        #print(INextent)
        wkt1 = "POLYGON ((" + str(INextent[0]) + " " + str(INextent[2]) + "," + str(INextent[1]) + " " + str(
            INextent[2]) + "," + str(INextent[1]) + " " + str(INextent[3]) + "," + str(INextent[0]) + " " + str(
            INextent[3]) + "," + str(INextent[0]) + " " + str(INextent[2]) + "))"

        shp_ds = None

    if shp_img.endswith('.tif') or shp_img.endswith('.vrt'):
        # --------------  IN IMG  -------------------
        #print("Raster vs raster")
        in_ds = gdal.Open(shp_img)
        INSR = osr.SpatialReference()
        INSR.ImportFromWkt(in_ds.GetProjectionRef())
        geoTransform = in_ds.GetGeoTransform()
        minx = geoTransform[0]
        maxy = geoTransform[3]
        maxx = minx + geoTransform[1] * (in_ds.RasterXSize-1)
        miny = maxy + geoTransform[5] * (in_ds.RasterYSize-1)

        if '+proj=longlat +datum=WGS84 +no_defs' == INSR.ExportToProj4():
            if minx <= -180:
                minx = -179.99999999
            if miny <= -90:
                miny = -89.99999999
            if maxx >= 180:
                maxx = 179.9999999
            if maxy >= 90:
                maxy = 89.9999999

        INextent = (minx, maxx, miny, maxy)
        #print('INextent')
        #print(INextent)


        in_ds = None

        wkt1 = "POLYGON ((" + str(INextent[0]) + " " + str(INextent[2]) + "," + str(INextent[1]) + " " + str(
            INextent[2]) + "," + str(INextent[1]) + " " + str(INextent[3]) + "," + str(INextent[0]) + " " + str(
            INextent[3]) + "," + str(INextent[0]) + " " + str(INextent[2]) + "))"


    LL_SRS = osr.SpatialReference()
    #   4326 has some issue when testing SHP in AEA vs raster in AEA
    # --------------  IN IMG  -------------------
    LL_SRS.ImportFromEPSG(3857)

    img_ds = gdal.Open(img)
    imgGeo = img_ds.GetGeoTransform()


    cols = img_ds.RasterXSize
    rows = img_ds.RasterYSize

    target_ref = osr.SpatialReference()
    target_ref.ImportFromWkt(img_ds.GetProjectionRef())
    # print('-----------------------------------------')
    # print(target_ref)
    # print('-----------------------------------------')
    # print(img_ds.GetProjectionRef())
    # print('-----------------------------------------')
    # print(target_ref.ExportToProj4())
    minx2 = imgGeo[0]
    maxy2 = imgGeo[3]
    maxx2 = minx2 + imgGeo[1] * (img_ds.RasterXSize -1)
    miny2 = maxy2 + imgGeo[5] * (img_ds.RasterYSize -1)
    if '+proj=longlat +datum=WGS84 +no_defs' == target_ref.ExportToProj4():
        if minx2 <= -180:
            minx2 = -179.99999999
        if miny2 <= -90:
            miny2 = -89.99999999
        if maxx2 >= 180:
            maxx2 = 179.9999999
        if maxy2 >= 90:
            maxy2 = 89.9999999
    OUTextent = (minx2, maxx2, miny2, maxy2)
    # print('OUTextent')
    # print(OUTextent)

    img_ds = None

    if gdal.VersionInfo()[0] == '3':
        LL_SRS.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        INSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        target_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    transform1 = osr.CoordinateTransformation(INSR, LL_SRS)
    transform2 = osr.CoordinateTransformation(target_ref, LL_SRS)
    #LLtransformLL = osr.CoordinateTransformation(, target_ref)
    # # ------  GDAL 3 changes X Y default return val --------
    # # https://github.com/OSGeo/gdal/issues/1546
    ## strange if LL is coming from SHP has inverese behaviour compare to raster

    wkt2 = "POLYGON ((" + str(OUTextent[0]) + " " + str(OUTextent[2]) + "," + str(OUTextent[1]) + " " + str(
        OUTextent[2]) + "," + str(OUTextent[1]) + " " + str(OUTextent[3]) + "," + str(OUTextent[0]) + " " + str(
        OUTextent[3]) + "," + str(OUTextent[0]) + " " + str(OUTextent[2]) + "))"

    poly1 = ogr.CreateGeometryFromWkt(wkt1)
    poly2 = ogr.CreateGeometryFromWkt(wkt2)
    poly1.Transform(transform1)
    poly2.Transform(transform2)

    print('Intersection:', poly1.Intersects(poly2))
    return poly1.Intersects(poly2)
    # intersection = poly1.Intersection(poly2)
    # print(intersection.ExportToWkt())
    # contains = poly1.Overlaps(poly2)
    # print(contains)
    # contains = poly2.Overlaps(poly1)
    # print(contains)
    # print('#################')
    # return intersection.ExportToWkt() != 'GEOMETRYCOLLECTION EMPTY'

# NOT USED
def stats_tabulate_area_per_polygon(SHP, CLASS, outfile):
    if not os.path.exists(SHP):
        print("No Shp")
        return -1
    if not os.path.exists(CLASS):
        print("No Image")
        return -1
    if test_overlapping(SHP, CLASS):
        if outfile == '':
            outfile = SHP.replace(".shp", "_stats.csv")

        tstart = time.time()
        text_file = open(outfile, "w")
        text_file.write("FID;Band;Mode;Mode_mod" + '\n')

        shp = ogr.Open(SHP, update=1)
        shplayer = shp.GetLayer(0)
        layerDefinition = shplayer.GetLayerDefn()

        class_ds = gdal.Open(CLASS, gdal.GA_ReadOnly)
        num_bands = class_ds.RasterCount

        if (layerDefinition.GetFieldCount() == 2):
            for i in range(num_bands):
                f1 = ogr.FieldDefn('T' + str(i + 1) + '_mode', ogr.OFTString)
                f1.SetWidth(4)
                shplayer.CreateField(f1)
                f1 = None

        transform = class_ds.GetGeoTransform()

        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = transform[5]
        mem_drv = ogr.GetDriverByName('Memory')
        driver = gdal.GetDriverByName('MEM')

        # Loop through vectors

        featureCount = shplayer.GetFeatureCount()

        feat = shplayer.GetNextFeature()

        # print "------NEW-------"
        sourceSR = shplayer.GetSpatialRef()
        targetSR = osr.SpatialReference()
        targetSR.ImportFromWkt(class_ds.GetProjectionRef())
        sourceSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        targetSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        coordTrans = osr.CoordinateTransformation(sourceSR, targetSR)

        while feat is not None:
            fct = feat.GetFID()
            feat.SetField(0, fct)

            if ((fct % 1000) == 0):
                print("Features %d of %d" % (fct, featureCount))

            featenvelope = feat.geometry().GetEnvelope()
            src_offset = bbox_to_pixel_offsets(transform, featenvelope)
            try:
                # if 1:
                # calculate new geotransform of the feature subset
                new_gt = (
                    (transform[0] + (src_offset[0] * transform[1])),
                    transform[1],
                    0.0,
                    (transform[3] + (src_offset[1] * transform[5])),
                    0.0,
                    transform[5]
                )
                mem_ds = mem_drv.CreateDataSource('out')
                mem_layer = mem_ds.CreateLayer('poly', None, ogr.wkbPolygon)
                mem_layer.CreateFeature(feat.Clone())

                # Rasterize it
                rvds = driver.Create('', src_offset[2], src_offset[3], 1, gdal.GDT_Byte)
                rvds.SetGeoTransform(new_gt)

                gdal.RasterizeLayer(rvds, [1], mem_layer, burn_values=[1])

                rv_array = rvds.ReadAsArray()
                # loop each classification band and set fiels with mode
                ct = 2
                for i in range(num_bands):
                    class_band = class_ds.GetRasterBand(i + 1)
                    src_array = class_band.ReadAsArray(src_offset[0], src_offset[1], src_offset[2], src_offset[3])
                    try:
                        masked = numpy.ma.MaskedArray(
                            src_array,
                            mask=numpy.logical_not(rv_array)
                        )
                        modeval = MYmode2D(masked)

                        modeval_mod = MYmode2D_modified(masked)

                    except:
                        modeval = 0
                        modeval_mod = 0
                        print("-")

                    # stats % of each class

                    string = str(fct) + ';' + str(ct) + ';' + str(i) + ';' + str(modeval_mod) + '\n';
                    # print string
                    text_file.write(string)

                    feat.SetField(ct, modeval)
                    # feat.SetField(ct+1,SD2Class(modeval))
                    ct = ct + 1
            except:

                ct = 2
                for i in range(num_bands):
                    feat.SetField(ct, 0)
                    # feat.SetField(ct+1,0)
                    print("Failure on id= " + str(fct))
                    ct = ct + 1

            shplayer.SetFeature(feat)
            feat.Destroy()

            rvds = None
            mem_ds = None
            feat = shplayer.GetNextFeature()

        shplayer = None
        class_ds = None
        tend = time.time()
        duration = tend - tstart
        hours, remainder = divmod(duration, 3600)
        minutes, seconds = divmod(remainder, 60)
        print("Completed in " + str(hours) + "hrs " + str(minutes) + "min " + str(seconds) + "sec")
        text_file.close()


    else:
        print("No Overlap")
        return -1
