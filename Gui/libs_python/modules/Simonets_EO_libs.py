#!python
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
#
#            MAIN CODE : GET METADATA INFO 
#
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------	



class Metadata:
    def __init__(self):
        self.sensor = ""
        self.gain = []
        self.bias = []
        self.sunel = 0.
        self.sunazi = 0.
        self.ESdistance = 0.
        self.clouds = -1
        self.yyyy = ''
        self.mm = ''
        self.dd = ''
        self.path = ''
        self.row = ''

    def __str__(self):
        return self
   
    #def getSensor(self):
    #	print self.sensor
    #def getGain(self):
    #  print self.gain
    #def getBias(self):
    #  print self.bias
    #def getSunel(self):
    #  print self.sunel
    #def getSunazi(self):
    #  print self.sunazi


def get_Landsat_metadata_info(MYMET):
    from datetime import date
    import math
    import time


    try:
        METOBJ=Metadata()
        newline="\n"
        f=open(MYMET, 'r')
        content = f.read()
        f.close()
        if MYMET.endswith('.xml'):
            print('XML')
            # --- New > 2020 +- Landsat Surface Refl or TOA  ----
            try:
                st = content.index('<instrument>')
                end = content.index('</instrument>')  # use 10 ch after " to include OLI-TIRS since the lenght is not define a priori
                SENSOR = content[st+12:end].lower()
                SENSOR = SENSOR.replace('"', '')
            except:
                st = content.index('<SENSOR_ID>')
                end = content.index('</SENSOR_ID>')  # use 10 ch after " to include OLI-TIRS since the lenght is not define a priori
                SENSOR = content[st + 11:end].lower()
                SENSOR = SENSOR.replace('"', '')

            if (SENSOR=='oli/tirs_combined'):
                SENSOR='oli'
            if (SENSOR=="oli/tirs"):
                SENSOR='oli'
            if (SENSOR == "oli_tirs"):
                SENSOR = 'oli'
            if (SENSOR=="etm+"):
                SENSOR='etm'
            METOBJ.sensor = SENSOR

            try:
                ID='<SUN_AZIMUTH>'
                st=content.index(ID)
                end=content.index('</SUN_AZIMUTH>')
                AZI=float(content[st+len(ID):end])
                METOBJ.sunazi=AZI

                ID='<SUN_ELEVATION>'
                st=content.index(ID)
                end=content.index('</SUN_ELEVATION>')
                ELEV=float(content[st+len(ID):end])
                METOBJ.sunel=ELEV
            except:
                print('No sun elevation/azimuth')

            try:
                st=content.index('<acquisition_date>')
                DATE=content[st+18:st+18+10]
                YEAR = DATE.split("-")[0]
                MONTH = DATE.split("-")[1]
                DAY = DATE.split("-")[2]
                METOBJ.yyyy = YEAR
                METOBJ.mm = MONTH
                METOBJ.dd = DAY
            except:

                st=content.index('<DATE_ACQUIRED>')
                DATE=content[st+15:st+15+10]
                YEAR = DATE.split("-")[0]
                MONTH = DATE.split("-")[1]
                DAY = DATE.split("-")[2]
                METOBJ.yyyy = YEAR
                METOBJ.mm = MONTH
                METOBJ.dd = DAY
            try:
                st = content.index('row="')
                end = content[st+5:st+5+4].index('"')
                ROW = content[st+5:st+5+end]
                st = content.index('path="')
                end = content[st+6:st+6+4].index('"')
                PATH = content[st+6:st+6+end]
            except:

                st = content.index('<WRS_PATH>')
                PATH = content[st+10:st+10+3]
                PATH = PATH.replace('<','')  # not always 3 digits
                PATH = PATH.replace('/','')  # not always 2 digits
                st = content.index('<WRS_ROW>')
                ROW = content[st+9:st+9+3]
                ROW = ROW.replace('<','')  # not always 3 digits
                ROW = ROW.replace('/','')  # not always 2 digits

            if len(PATH) == 2:
                PATH = '0' + PATH
            if len(PATH) == 1:
                PATH = '00' + PATH
            if len(ROW) == 2:
                ROW = '0' + ROW
            if len(ROW) == 1:
                ROW = '00' + ROW

            METOBJ.path = PATH
            METOBJ.row = ROW


        # // end of new SR or TA
        # // load from old metadata
        else:
            #  --- standard Landsat DN metadata files ----
            st=content.index('SENSOR_ID = ')
            end=content[st+13:st+13+10].index(newline)-1    # use 10 ch after " to include OLI-TIRS since the lenght is not define a priori
            SENSOR=content[st+13:st+13+end].lower()
            SENSOR=SENSOR.replace('"','')

            if (SENSOR=='oli_tirs'):
                SENSOR='oli'
            if (SENSOR=="oli"):
                SENSOR='oli'
            if (SENSOR=="etm+"):
                SENSOR='etm'
            METOBJ.sensor=SENSOR

            try:
                st=content.index('ACQUISITION_DATE = ')
                DATE=content[st+19:st+19+10]

            except:
                st=content.index('DATE_ACQUIRED = ')
                DATE=content[st+16:st+16+10]

            YEAR=DATE.split("-")[0]
            MONTH=DATE.split("-")[1]
            DAY=DATE.split("-")[2]
            METOBJ.yyyy = YEAR
            METOBJ.mm = MONTH
            METOBJ.dd = DAY

            st = content.index('WRS_PATH = ')
            end = content[st + 11:st + 11 + 5].index(newline)
            PATH = content[st + 11:st + 11 + end]
            try:
                st = content.index('WRS_ROW = ')
                end = content[st + 10:st + 10 + 5].index(newline)
                ROW = content[st + 10:st + 10 + end]
            except:
                print("WRS_ROW not present trying  with STARTING_ROW <br>")
                st = content.index('STARTING_ROW = ')
                end = content[st + 15:st + 15 + 5].index(newline)
                ROW = content[st + 15:st + 15 + end]


            if len(PATH) == 2:
                PATH = '0' + PATH
            if len(PATH) == 1:
                PATH = '00' + PATH
            if len(ROW) == 2:
                ROW = '0' + ROW
            if len(ROW) == 1:
                ROW = '00' + ROW

            METOBJ.path = PATH
            METOBJ.row = ROW


            if (SENSOR == 'etm' or SENSOR == 'tm'):
                ID='LMAX_BAND'
                ID1='GAIN_BAND'
                ID2='RADIANCE_MULT_BAND_'
                try:
                    res=content.index(ID+'1')
                except:
                    res=0
                try:
                    res1=content.index(ID1+'1')
                except:
                    res1=0
                try:
                    res2=content.index(ID2+'1')
                except:
                    res2=0

                for b in ['1','2','3','4','5','7']:
                    # ----------------------------------------------------------------------------------------------------
                    # -----------------------  OLD MET format ------------------------------------------------------------
                    # ----------------------------------------------------------------------------------------------------
                    if res > 0:
                        ID='LMAX_BAND'+b+' = '
                        print(ID)
                        st=content.index(ID)
                        try:
                            end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                        except:
                            end=10

                        LMAX=float(content[st+len(ID):st+len(ID)+end])
                        ID='LMIN_BAND'+b+' = '
                        st=content.index(ID)
                        try:
                            end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                        except:
                            end=10

                        LMIN=float(content[st+len(ID):st+len(ID)+end])

                        ID='QCALMAX_BAND'+b+' = '
                        st=content.index(ID)
                        try:
                            end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                        except:
                            end=10

                        QMAX=float(content[st+len(ID):st+len(ID)+end])

                        ID='QCALMIN_BAND'+b+' = '
                        st=content.index(ID)
                        try:
                            end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                        except:
                            end=10

                        QMIN=float(content[st+len(ID):st+len(ID)+end])

                        METOBJ.gain.append((LMAX - LMIN) / (QMAX - QMIN))
                        METOBJ.bias.append(LMIN)
                        continue

                    #else:
                    # ----------------------------------------------------------------------------------------------------
                    # -----------------------  TRANSITIONAL MET format ---------------------------------------------------
                    # ----------------------------------------------------------------------------------------------------
                    if ( res1 > 0 ):

                            ID='GAIN_BAND'+b+' = '
                            st=content.index(ID)
                            try:
                                end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                            except:
                                end=10
                            G=float(content[st+len(ID):st+len(ID)+end])
                            METOBJ.gain.append(G)

                            ID='BIAS_BAND'+b+' = '
                            st=content.index(ID)
                            try:
                                end=content[st+len(ID):st+len(ID)+10].index(newline)    # use 10 ch after " to include more
                            except:
                                end=10
                            B=float(content[st+len(ID):st+len(ID)+end])
                            METOBJ.bias.append(B)
                            continue

                    # ----------------------------------------------------------------------------------------------------
                    # -----------------------  L8 standard MET format ---------------------------------------------------
                    # ----------------------------------------------------------------------------------------------------
                    #else:
                    if ( res2 > 0 ):
                        #print "NEW"
                        ID='RADIANCE_MULT_BAND_'+b+' = '
                        st=content.index(ID)
                        end=content[st+len(ID):st+len(ID)+20].index(newline)    # use 10 ch after " to include more
                        G=float(content[st+len(ID):st+len(ID)+end])
                        METOBJ.gain.append(G)

                        ID='RADIANCE_ADD_BAND_'+b+' = '
                        st=content.index(ID)
                        end=content[st+len(ID):st+len(ID)+20].index(newline)    # use 10 ch after " to include more
                        B=float(content[st+len(ID):st+len(ID)+end])
                        METOBJ.bias.append(B)
                        continue


                ID='SUN_AZIMUTH = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                AZI=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.sunazi=AZI

                ID='SUN_ELEVATION = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                ELEV=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.sunel=ELEV
                #print datetime.datetime(YEAR, MONTH, DAY, 0, 0).timetuple().tm_yday

            if (SENSOR == 'oli'):
                for b in ['2','3','4','5','6','7']:
                    ID='RADIANCE_MULT_BAND_'+b+' = '
                    st=content.index(ID)
                    end=content[st+len(ID):st+len(ID)+20].index(newline)    # use 10 ch after " to include more
                    G=float(content[st+len(ID):st+len(ID)+end])
                    METOBJ.gain.append(G)

                    ID='RADIANCE_ADD_BAND_'+b+' = '
                    st=content.index(ID)
                    end=content[st+len(ID):st+len(ID)+20].index(newline)    # use 10 ch after " to include more
                    B=float(content[st+len(ID):st+len(ID)+end])
                    METOBJ.bias.append(B)

                ID='SUN_AZIMUTH = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                AZI=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.sunazi=AZI

                ID='SUN_ELEVATION = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                ELEV=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.sunel=ELEV

                ID='EARTH_SUN_DISTANCE = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                ES=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.ESdistance=ES

                ID='CLOUD_COVER = '
                st=content.index(ID)
                end=content[st+len(ID):st+len(ID)+15].index(newline)    # use 10 ch after " to include more
                CL=float(content[st+len(ID):st+len(ID)+end])
                METOBJ.clouds=CL


            if (METOBJ.ESdistance == 0):

                Jday=date(int(YEAR), int(MONTH), int(DAY)).toordinal()  - date(int(YEAR), 1, 1).toordinal()+1
                #E-S-distance (Achard and D Souza 1994; Eva and Lambin, 1998))
                d = (1-0.01672*math.cos(0.01745*(0.9856*(Jday-4))))

                METOBJ.ESdistance=float(d*d)


        return METOBJ
    except:
        #print "Invalid Metadata"
        return METOBJ



def get_Rapideye_metadata_info(MYMET):
    from datetime import date
    import math
    import time


    try:
        METOBJ=Metadata()
        newline="\n"
        f=open(MYMET, 'r')
        content = f.read()
        f.close()

        ID='serialIdentifier>'
        st=content.index(ID)
        SENSOR=content[st+len(ID):st+len(ID)+4]    # use 10 ch after " to include OLI-TIRS since the lenght is not define a priori



        METOBJ.sensor=SENSOR
        try:
            ID='acquisitionDateTime>'
            st=content.index(ID)
            DATE=content[st+len(ID):st+len(ID)+10]
            YEAR=DATE.split("-")[0]
            MONTH=DATE.split("-")[1]
            DAY=DATE.split("-")[2]
            print(int(YEAR))
            print(int(MONTH))
            print(int(DAY))
            print(date(int(YEAR), int(MONTH), int(DAY)))
            Jday=date(int(YEAR), int(MONTH), int(DAY)).toordinal()  - date(int(YEAR), 1, 1).toordinal()+1

            #E-S-distance (Achard and D Souza 1994; Eva and Lambin, 1998))
            d = (1-0.01672*math.cos(0.01745*(0.9856*(Jday-4))))
            METOBJ.ESdistance=float(d*d)
        except:
            print("Invalid ACQ date")
            return METOBJ


        if (SENSOR == 'RE-1' or SENSOR == 'RE-2'or SENSOR == 'RE-3'or SENSOR == 'RE-4'or SENSOR == 'RE-5'):

            ID='illuminationAzimuthAngle uom="deg">'
            st=content.index(ID)
            end=content[st+len(ID):st+len(ID)+15].index('<')    # use 10 ch after " to include more
            AZI=float(content[st+len(ID):st+len(ID)+end])
            METOBJ.sunazi=AZI

            ID='illuminationElevationAngle uom="deg">'
            st=content.index(ID)
            end=content[st+len(ID):st+len(ID)+15].index('<')    # use 10 ch after " to include more
            ELEV=float(content[st+len(ID):st+len(ID)+end])
            METOBJ.sunel=ELEV


            #print datetime.datetime(YEAR, MONTH, DAY, 0, 0).timetuple().tm_yday



        if (METOBJ.ESdistance == 0.):

            #Jday=date(int(YEAR), int(MONTH), int(DAY)).toordinal()  - date(int(YEAR), 1, 1).toordinal()+1
            #E-S-distance (Achard and D Souza 1994; Eva and Lambin, 1998))
            #d = (1-0.01672*math.cos(0.01745*(0.9856*(Jday-4))))
            # set to 1 if not possible to calc
            METOBJ.ESdistance=1.

        return METOBJ
    except:
        print("Invalid Metadata")
        return METOBJ
