#!python
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


import os
import sys

GLOBALS = dict()

# -- IMPACT base package path -- #
GLOBALS['root_path'] = os.path.dirname(os.path.realpath(__file__)).replace("\\", "/").replace('Gui/libs_python/modules','')

# -- base URL (Gui/) -- ##
#GLOBALS['url'] = "http://127.0.0.1:8899" if 'IMPACT_HTTP_HOST' not in os.environ.keys() else "http://"+os.environ['IMPACT_HTTP_HOST']
#GLOBALS['url'] = "" if 'IMPACT_HTTP_HOST' not in os.environ.keys() else os.environ['IMPACT_HTTP_HOST']

#GLOBALS['url'] = "" if 'IMPACT_HTTP_HOST' not in os.environ.keys() else os.environ['IMPACT_HTTP_HOST']

# if port is not 9999 change into NGINx conf file



# -- data PATHs -- ##
GLOBALS['data_paths'] = {}

if sys.platform == "win32":

    GLOBALS['url'] = "http://127.0.0.1:"+os.environ['IMPACT_NGINX_PORT'] if 'IMPACT_HTTP_HOST' not in os.environ.keys() else "http://" + os.environ['IMPACT_HTTP_HOST']
    GLOBALS['data_paths']['data'] = os.path.join(GLOBALS['root_path'], "DATA/")
    GLOBALS['data_paths']['unix_raw_data'] = "" if 'REMOTE_DATA_VOLUME' not in os.environ.keys() else os.environ['REMOTE_DATA_VOLUME']
    GLOBALS['data_paths']['tmp_data'] = os.path.join(GLOBALS['data_paths']['data'], 'processing_tmp/')
    GLOBALS['OS'] = 'win32'
else:
    # server URL is taken from the Aplication.js
    GLOBALS['url'] = "" if 'IMPACT_HTTP_HOST' not in os.environ.keys() else os.environ['IMPACT_HTTP_HOST']
    # remote_data is defined in the compose.yml file and pointing to user directory
    GLOBALS['data_paths']['data'] = "/data/"
    GLOBALS['data_paths']['unix_raw_data'] = "/remote_data/" #if 'REMOTE_DATA_VOLUME' not in os.environ.keys() else os.environ['REMOTE_DATA_VOLUME']
    GLOBALS['data_paths']['tmp_data'] = '/data/processing_tmp/'
    GLOBALS['OS'] = 'unix'



# -- temp PATH -- ##
# used my mapserver script in case of debug mode
GLOBALS['temp_path'] = GLOBALS['data_paths']['data']




# -- settings db name -- #
GLOBALS['default_settings'] = os.path.join(GLOBALS['root_path'], "Gui/settings/settings.default.json")
if sys.platform == "win32":
    GLOBALS['settings'] = os.path.join(GLOBALS['root_path'], "Gui/settings/settings.json")
else:
    GLOBALS['settings'] = "/data/db/settings.json"
    if not os.path.exists(GLOBALS['settings']):
        try:
            import shutil
            if not os.path.exists("/data/db"):
                os.mkdir("/data/db")
            shutil.copy(GLOBALS['default_settings'], GLOBALS['settings'])
        except Exception as e:
            print(e)
            print('Error while creating settings file to host machine')
            print('From ', GLOBALS['default_settings'])
            print('To ', GLOBALS['settings'])
            print('Keeping settings within the container, please fix it!')


#-- DB -- #
if sys.platform == "win32":
    GLOBALS['logs_db'] = os.path.join(GLOBALS['root_path'], "Gui/db/logs.db")
    GLOBALS['image_store'] = os.path.join(GLOBALS['root_path'], "Gui/db/image_store.db")
    GLOBALS['cs_image_store'] = os.path.join(GLOBALS['root_path'], "Gui/db/cs_image_store.db")
    GLOBALS['image_settings_store'] = os.path.join(GLOBALS['root_path'], "Gui/db/image_settings_store.db")
else:
    GLOBALS['logs_db'] = "/data/db/logs.db"
    GLOBALS['image_store'] = "/data/db/image_store.db"
    GLOBALS['cs_image_store'] = "/data/db/cs_image_store.db"
    GLOBALS['image_settings_store'] = "/data/db/image_settings_store.db"
# -- Tools ----
GLOBALS['Tools'] = os.path.join(GLOBALS['root_path'], "Tools")

# -- Win Libs path ----
GLOBALS['WinLibs'] = os.path.join(GLOBALS['root_path'], "Libs", "win64")

# -- proj.db in Proj6 ----
GLOBALS['projdb'] = os.path.join(os.environ['PROJ_LIB'], "proj.db")

# -- proxy.pid is used as global war in Win to set proxy on python and windows script
#    since proxy is not set in ENV or START.bat file
#    if present GEE lib is setting the proxy before authentication and ee.Initialize()
#     2do: set global proxy
GLOBALS['proxy'] = os.path.join(GLOBALS['root_path'], "Gui/tmp/proxy.pid")

