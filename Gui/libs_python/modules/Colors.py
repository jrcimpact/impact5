# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


def hex2rgb(hex_color):
    """ Translate HEX color to RGB """
    hex_color = hex_color.lstrip('#')
    if len(hex_color) != 6:
        raise False
    r, g, b = hex_color[:2], hex_color[2:4], hex_color[4:]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return r, g, b
