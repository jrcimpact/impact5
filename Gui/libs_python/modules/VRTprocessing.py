# Licence: MIT
# Copyright 2016, Even Rouault


def percentage_int(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, radius, gt, z, scale):

    out_ar[:] = (in_ar[0] / in_ar[1])*100

def percentage(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize,
            raster_ysize, radius, gt, **kwargs):
    z = float(kwargs['z_factor']) ## not used but here as example
    scale= float(kwargs['scale']) ## not used but here as example
    percentage_int(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize,
                raster_ysize, radius, gt, z, scale)
                
                
def sum_int(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, radius, gt, z, scale):
    import numpy as np
    out_ar[:] = np.sum(in_ar, axis=0)

    
def sum(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize,
            raster_ysize, radius, gt, **kwargs):
    z = float(kwargs['z_factor']) ## not used but here as example
    scale= float(kwargs['scale']) # not used but here as example
    sum_int(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize,
                raster_ysize, radius, gt, z, scale)