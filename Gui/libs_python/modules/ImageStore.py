# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
from time import time, strftime, gmtime, strptime
from calendar import timegm
import re
import subprocess
import copy

# Import local libraries
from __config__ import *
import FileSystem
import GdalOgr
import ImageProcessing
import Database
import os

#import sys
# reload(sys)
# sys.setdefaultencoding("utf-8")


def copy_file_info(full_path, new_full_path=None):
    """ Copy file: duplicate record in DB updating general file info  """
    store = ImageStore()
    file_info = store.get_by_path(full_path)
    file_info = store.get_file_info(file_info, new_full_path)
    store.update(new_full_path, file_info)

def update_file_info_and_geo(full_path, new_full_path=None):
    """ Update general file info about the given image in the database """
    store = ImageStore()
    file_info = dict()
    if new_full_path is None:
        new_full_path = full_path
    file_info = store.get_file_info(file_info, new_full_path)
    file_info = store.get_geo_info(file_info, new_full_path)
    store.update(full_path, file_info)
    return file_info


def update_file_info(full_path, new_full_path=None):
    """ Update general file info about the given image in the database """
    store = ImageStore()
    file_info = dict()
    if new_full_path is None:
        new_full_path = full_path
    file_info = store.get_file_info(file_info, new_full_path)
    store.update(full_path, file_info)


def update_file_statistics(full_path):
    """ Update all file info (including statistics) of the given file and update the database record """
    full_path = full_path.replace("\\", '/')  # replace path if called from Processing Python Tools
    store = ImageStore()
    store.insert(full_path, with_statistics=True)


def set_status(full_path, status='false'):
    store = ImageStore()
    file_info = {
        'ready': status
    }
    store.update(full_path, file_info)


def delete_file(full_path):
    """ Delete the given image from the database """
    store = ImageStore()
    store.delete(full_path)


def populate():
    """ Analyze the filesystem and populate the database """
    store = ImageStore()
    store.analyze_filesystem()


def calculate_statistics():
    """ Calculate statistics in multiprocessing """
    script_path = os.path.join(GLOBALS['root_path'], 'Gui', 'libs_python', 'calculate_statistics.py')
    args = ["python", script_path, 'ImageStore']
    subprocess.Popen(args, shell=False, env=dict(os.environ))

def get_all():
    """ Retrieve the full image list (all DB) """
    store = ImageStore()
    return store.get_all()


def get_by_path(full_path):
    """ Retrieve image info """
    store = ImageStore()
    return store.get_by_path(full_path)


def get_without_statistics():
    """ Retrieve the list of images without statistics """
    store = ImageStore()
    file_list = []
    for record in store.get_not_ready():
        file_list.append(record['full_path'])
    store = None
    del store
    return file_list


def updated_recently(wait=20):
    """ Check if the database had been updated recently """
    store = ImageStore()
    db_time = int(os.path.getmtime(store.db_file))
    now_time = int(time())
    return True if now_time-db_time <= wait else False

def reset_pending_files():

    store = ImageStore()
    for record in store.get_pending():
        print("Reset Pending status IN: " + record['full_path'])
        file_info = {
            'ready': 'false'
        }
        store.update(record['full_path'], file_info)


class ImageStore:

    def __init__(self):

        self.__database_dump = []

        """ Initialize DB controller """
        self.root_path = GLOBALS['root_path']
        self.data_paths = [
            GLOBALS['data_paths']['data']
        ]
        self.db_file = GLOBALS['image_store']
        self.db_table = "image_store"
        self.file_info_structure = {
            # File related information
            'full_path': '',
            'path': '',
            'basename': '',
            'extension': '',
            'modification_date': '',
            'fileSize': '0',
            # Basic geo information
            'extent': '{}',
            'PROJ4': '',
            'EPSG': '',
            # Flags
            'ready': 'false',
            # Vector related information
            'attributes': '',
            'numFeatures': '',
            'simplificationRequired': '',
            'simplificationMethod': '',
            # Raster related information
            'pixel_size': '',
            'metadata': '{}',
            'statistics': '{}',
            'num_bands': '',
            'data_type': '',
            'has_colorTable': '',
            'no_data': '',
            'num_columns': '',
            'num_rows': '',
            'num_unique_values': '{}',
            'unique_values': '{}',
            'lookup_table': '{}'
        }

        # initialize DB object and connection
        self.db = Database.Database(self.db_file)
        if self.conditions_to_remove_db():
            self.db.remove_db()
        self.db.create_table(self.db_table, self.file_info_structure.keys())
        #print "INIT"
        # item = {}
        # item['ready'] = 'false'
        # self.db.update(self.db_table, item, [['ready', '=', 'pending']])

    def conditions_to_remove_db(self):
        if not self.db.column_exist(self.db_table, 'no_data'):
            return True
        if not self.db.column_exist(self.db_table, 'num_unique_values'):
            return True
        return False

    def insert(self, full_path, with_statistics=False, in_transaction=False):
        """ Retrieve all the info about the given image (to be saved into DB) """
        file_info = copy.deepcopy(self.file_info_structure)
        try:
            file_info = self.get_file_info(file_info, full_path)  # reload modification date
            file_info = self.get_geo_info(file_info, full_path)
            # statistics
            if with_statistics:
                file_info['ready'] = 'pending'
                file_info = self.get_statistics(file_info, full_path)
                file_info = self.get_file_info(file_info, full_path)  # reload modification date
                self.update(full_path, file_info)
            else:
                if file_info['PROJ4'] == 'Unknown':
                    file_info['ready'] = 'pending'
                self.db.insert(self.db_table, file_info, in_transaction=in_transaction)
        except Exception as e:
            print("Error reading file "+ full_path + ' ' + str(e))
            return None


    def update(self, full_path, file_info):
        """ Update a record in the DB """
        self.db.update(self.db_table, file_info, [['full_path', '=', full_path]])

    def delete(self, full_path, in_transaction=False):
        """ Delete a record from the DB """
        self.db.delete(self.db_table, [['full_path', '=', full_path]], in_transaction=in_transaction)

    def get_all(self):
        """ Get all the records from the DB """
        return self.db.select(self.db_table, '*', None, 'full_path ASC')

    def get_by_path(self, full_path):
        """ Query DB by full_path (only 1 record expected) """
        records = self.db.select(self.db_table, '*', [['full_path', '=', full_path]])
        if records:
            return records[0]
        return None

    @staticmethod
    def __get_from_dump(database_dump, file_path):
        """ Query DB dump by full_path (used for higher performances) """
        for item in database_dump:
            if item['full_path'] == file_path:
                return item
        return None

    def get_not_ready(self):
        """ Get all the records from the DB """
        return self.db.select(self.db_table, '*', [['ready', '=', 'false']], 'CAST (fileSize as INTEGER)')

    def get_pending(self):
        """ Get all the records from the DB """
        return self.db.select(self.db_table, '*', [['ready', '=', 'pending']], 'CAST (fileSize as INTEGER)')

    def analyze_filesystem(self):
        try:
            """ Analyze the filesystem and populate the database """
            # Clean the DB: remove orphans records
            for record in self.get_all():
                if not os.path.exists(record['full_path']):
                    #print "DEL " + record['full_path']
                    self.delete(record['full_path'], in_transaction=True)
            self.db.execute_transaction()
            # Analyze all images in DATA/  #
            self.__database_dump = []
            self.__database_dump = self.get_all()
            for data_path in self.data_paths:
                file_list = FileSystem.traverse_recursively(data_path)
                for file_path in file_list:
                    if GLOBALS['data_paths']['tmp_data'] in file_path:
                        continue
                    file_path = ImageProcessing.tif_extension_to_lower(file_path)
                    if file_path.endswith('.tif') or file_path.endswith('.shp') or file_path.endswith('.vrt') or file_path.endswith('.kml') or file_path.endswith('.geojson') or file_path.endswith('.json'):
                        self.__analyze_image(file_path)
            self.db.execute_transaction()
            return None
        except Exception as e:
            print("Error in analyze_filesystem " + str(e))

    def __analyze_image(self, full_path):
        """ Insert/Update an image to the DB (or do nothing if not necessary) """
        item_from_db = self.__get_from_dump(self.__database_dump, full_path)
        # --- File not found in db: ADD ---
        if item_from_db is None:
            #print "ADD " + full_path
            self.insert(full_path, with_statistics=False, in_transaction=True)
        else:
            modification_date = int(os.path.getmtime(full_path))
            item_modification_date = int(timegm(strptime(item_from_db['modification_date'], '%Y-%m-%d %H:%M:%S')))
            # --- File found but modified: REPLACE --
            if item_from_db['ready'] != 'pending' and modification_date > item_modification_date:
                #print "RE - ADD " + full_path
                self.delete(full_path, in_transaction=False)
                self.insert(full_path, with_statistics=False, in_transaction=True)
            # else:
            #     #print item_from_db['statistics'] == {}
            #     if item_from_db['ready'] == 'pending' and modification_date != item_modification_date and item_from_db['statistics'] == {}:
            #         print full_path
            #         print "Pending"
            #         self.insert(full_path, with_statistics=False, in_transaction=True)



    def get_file_info(self, file_info, full_path):
        """ get file related information (path, size, last modification date, ...) """
        file_info['full_path'] = full_path
        (file_info['path'], file_info['basename'], file_info['extension'], _) = \
            FileSystem.split_filename(full_path.replace(self.root_path, ''))
        file_info['fileSize'] = os.path.getsize(full_path)/(953700)
        file_info['modification_date'] = strftime('%Y-%m-%d %H:%M:%S', gmtime(os.path.getmtime(full_path)))
        return file_info

    @staticmethod
    def get_geo_info(file_info, full_path):
        """ get gdal/org basic info """
        if file_info['extension'] == '.shp' or file_info['extension'] == '.kml' or file_info['extension'] == '.geojson' or file_info['extension'] == '.json':
            for key, value in GdalOgr.get_info_from_shapefile(full_path).items():
                file_info[key] = value

        # gdal/org info: TIF
        elif file_info['extension'] == '.tif' or file_info['extension'] == '.vrt':
            for key, value in GdalOgr.get_info_from_tiff(full_path).items():
                file_info[key] = value
        return file_info

    @staticmethod
    def get_statistics(file_info, full_path):
        """ calculate gdal/org statistics """
        try:
            if file_info['extension'] == '.shp':
                file_info['numFeatures'], file_info['simplificationRequired'], file_info['simplificationMethod'] \
                    = GdalOgr.set_simplification_method(full_path)
                file_info['ready'] = 'true'
            # gdal/org info: TIF
            elif file_info['extension'] == '.tif':
                file_info['statistics'], file_info['num_unique_values'], file_info['unique_values'] \
                    = GdalOgr.calculate_raster_statistics(full_path)
                file_info['ready'] = 'true'
            elif file_info['extension'] == '.vrt':
                file_info['statistics'], file_info['num_unique_values'], file_info['unique_values'] \
                    = GdalOgr.calculate_raster_statistics(full_path, force_no_stats=True)
                file_info['ready'] = 'true'
            elif file_info['extension'] == '.kml':
                file_info['ready'] = 'true'

            elif file_info['extension'] == '.geojson' or file_info['extension'] == '.json':
                file_info['ready'] = 'true'

        except Exception as e:
            # reset "pending to false"
            print("ERROR IN STATS " + str(e))
            file_info['ready'] = 'pending'
        return file_info
