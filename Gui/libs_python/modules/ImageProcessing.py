# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import sys

import FileSystem


def validPathLengths(files_prefix, output_path, buffer_length, log_obj):
    """
    Verify if the expected output files paths are not too long
    :param files_prefix:
    :param output_path:
    :param buffer_length:
    :param log_obj:
    :return:
    """
    if isinstance(files_prefix, list):
        # list of files
        for file_prefix in files_prefix:
            validPathLengths(file_prefix, output_path, buffer_length, log_obj)
    else:
        # single file
        (_, file_name) = os.path.split(os.path.abspath(files_prefix))
        if output_path is not None:
            full_path = os.path.join(os.path.abspath(output_path), file_name)
        else:
            full_path = files_prefix
        if not FileSystem.path_length_is_valid(full_path, buffer_length):
            log_obj.send_error('Cannot process file' + files_prefix + '.<br />' + 'Expected output file path too long!')
            log_obj.close()
            sys.exit(0)


def tif_extension_to_lower(full_path):
    if full_path.endswith('.TIF'):
        new_filename = full_path.replace('.TIF', '.tif')
        try:
            os.rename(full_path, new_filename)
        except OSError:
            pass
        return new_filename
    return full_path
