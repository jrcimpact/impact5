#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
#import sys
#import cgi


def run(file):
    # retrieve parameters from request #
    #params = cgi.FieldStorage()
    #file = params.getvalue("file", '')
    if file == '' or not os.path.exists(file):
        print('Input file not valid')
        return ''
    f = open(file, "r")
    content = f.read()
    f.close()
    return content




# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     main()
