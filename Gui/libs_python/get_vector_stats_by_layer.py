#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import cgi
from __config__ import *
import Settings
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr


#if __name__ == '__main__':
def run(shapefileName, legend_type, layerName):
    
    ## retrieve parameters from request ##
    # params = cgi.FieldStorage()
    # shapefileName = params.getvalue("data_path", None)
    # layerName = params.getvalue("layer", None)
    # legend_type = params.getvalue("legend_type", None)

    SETTINGS = Settings.load()

    def rgb2hex(r, g, b):
        return '#{:02x}{:02x}{:02x}'.format(r, g, b)


    #print "Content-Type: text/html\n"

    # open SHP file
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataSource = driver.Open(shapefileName, 0)

    chart = {"title": {}, "seriesData": [], 'colors':[]}
    fontSize = 14
    if len(shapefileName) > 25:
        fontSize = 12

    textStyle = {'fontSize': fontSize, 'fontWeight': 'bold'}
    chart['title'] = \
        {
            'text': "Dataset: "+str(os.path.basename(shapefileName).replace('.shp','')),
            'subtext': '',
            'x': 'left',
            'textStyle':textStyle
            # subtextStyle: {fontWeight: 'bold'}
        }

    try:

        #chart = {"title": {'text': "Dataset: <b>"+str(os.path.basename(shapefileName).replace('.shp',''))+"</b>"}, "series": [], 'colors':[]}

        classes = {'name':[],'id':[],'area':[]}

        for legend in SETTINGS['map_legends']['vector']:
            if legend['type'] == str(legend_type):

                chart['title']["subtext"] = "Statistics from layer: "+str(layerName)+" Legend:  "+legend['name']
                for i in legend['classes']:

                    classes['name'].append(i[1])
                    classes['id'].append(str(i[0]))
                    classes['area'].append(0)
                    col = str(i[2]).split(',')
                    chart['colors'].append(rgb2hex(int(col[0]),int(col[1]),int(col[2])))

                classes['name'].append("Unmatched classes")
                classes['area'].append(0)
                chart['colors'].append(rgb2hex(255,10,10))



        # return area by class ID
        sql = "select "+str(layerName)+", sum(area(geometry)) from '"+str(os.path.basename(shapefileName)).replace('.shp','')+"' group by "+str(layerName)
        layer = dataSource.ExecuteSQL(sql, dialect = 'SQLITE')

        areaTot = 0.0
        if layer != None:
            for i, feature in enumerate(layer):
                areaTot += float(feature.GetField(1))

                if str(feature.GetField(0)) in classes['id']:
                    pos = classes['id'].index(str(feature.GetField(0)))
                    classes['area'][pos] = feature.GetField(1)
                else:
                    pos = classes['name'].index("Unmatched classes")
                    classes['area'][pos] += feature.GetField(1)

        else:
            pos = classes['name'].index("Unmatched classes")
            classes['area'][pos] = 100

        # calculate area in % and save to dictionary
        mydata = []
        for i, item in enumerate(classes['area']):
            if areaTot == 0.0:
                areaTot = 1

            mydata.append({'name' : classes['name'][i], 'value': [float(i), float(classes['area'][i]/areaTot*100)], 'count': float(float(classes['area'][i]/areaTot*100)), 'id': float(i),'itemStyle': {'color': chart['colors'][i]}})

        # rebuild HigCharts data structure
        chart['seriesData'] = mydata
        # chart['series'].append(               {
        #             'name': 'Value',
        #             'colorByPoint': 'true',
        #             'data': mydata
        #         }
        # )
        dataSource.ReleaseResultSet(layer)
        dataSource.Destroy()
        dataSource = None
        layer = None
        return json.dumps(chart)


    except Exception as e:
        print(-1)
        print(str(e))
        dataSource = None
        layer = None

