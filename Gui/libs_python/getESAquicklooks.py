#!python

# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import time
import json
import shutil
import requests
import csv

# from sentinelsat import read_geojson, geojson_to_wkt
from datetime import date

# Import local libraries
from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

def usage():
    print()
    print('Author: Simonetti Dario for European Commission 2015')
    print()
    print('Download Sentinel data from ESA HUB using sentinelsat libs')
    print()
    sys.exit(1)

def run(shp_names, sat, startDate, endDate, clouds, usr, pwd):
    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "download")
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)
    for f in os.listdir(TMPDIR):
        if os.stat(os.path.join(TMPDIR, f)).st_mtime < time.time() - 1 * 86400:
            try:
                if os.path.isfile(os.path.join(TMPDIR, f)):
                    os.remove(os.path.join(TMPDIR, f))
                else:
                    shutil.rmtree(os.path.join(TMPDIR, f), ignore_errors=True)
            except Exception as e:
                pass

    timestr = time.strftime("%Y%m%d-%H%M%S")
    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "download", timestr)
    if not os.path.exists(TMPDIR):
        os.makedirs(TMPDIR)

    try:
        jpegs = []
        titles = []
        uuids = []
        prodSize = []

        shp_names = json.loads(shp_names)
        for shp in shp_names:

            shpname = os.path.basename(shp)
            shpbasename = shpname.replace(".shp", "")

            tmpshp = os.path.join(TMPDIR, shpbasename + '.csv')
            if os.path.exists(tmpshp):
                os.remove(tmpshp)
            # ---------- OPEN SHP -----------
            # os.system('ogr2ogr -f GeoJSON -t_srs EPSG:4326 -dialect sqlite -sql "SELECT ST_Union(geometry) from '+ shpbasename +'" ' + tmpshp + ' ' + shp)
            os.system('ogr2ogr -f CSV -lco GEOMETRY=AS_WKT -t_srs EPSG:4326 -dialect sqlite -sql "SELECT Extent(geometry) from '+ shpbasename +'" ' + tmpshp + ' ' + shp)
            # search by polygon, time, and SciHub query keywords
            data = []
            with open(tmpshp, "r") as f:
                reader = csv.reader(f)  #, delimiter="\t"
                for i, line in enumerate(reader):
                    if line[0] != 'WKT':
                        data.append(line[0])
            aoi = data[0]

        data_collection = sat
        # data_collection = "LANDSAT-8"
        startDate = startDate[0:4]+'-'+startDate[4:6]+'-'+startDate[6:8]
        endDate = endDate[0:4]+'-'+endDate[4:6]+'-'+endDate[6:8]

        products = requests.get(
                    f"https://catalogue.dataspace.copernicus.eu/odata/v1/Products?$filter=Collection/Name eq '{data_collection}' and OData.CSC.Intersects(area=geography'SRID=4326;{aoi}') and ContentDate/Start gt {startDate}T00:00:00.000Z and ContentDate/Start lt {endDate}T00:00:00.000Z and Attributes/OData.CSC.DoubleAttribute/any(att:att/Name eq 'cloudCover' and att/OData.CSC.DoubleAttribute/Value le {clouds})&$expand=Assets&$top=50&$orderby=ContentDate/Start desc").json()
        # print(products)

        found = False
        try:
            for prod in products['value']:
                found = True
                uuids.append(prod['Id'])
                titles.append(prod['Name'])
                prodSize.append(int(int(prod['ContentLength']) / 1000000))
                try:
                    jpegs.append(prod['Assets'][0]['DownloadLink'])
                except Exception as e:
                    jpegs.append('')
        except Exception as e:
            pass

        if found:
            return json.dumps(
                {'success': 'True', 'mex': '', 'jpegs': jpegs, 'titles': titles, 'uuids': uuids, 'prodSize': prodSize})
        else:
            return json.dumps(
                {'success': 'False', 'mex': 'No images', 'jpegs': jpegs, 'titles': titles, 'uuids': uuids, 'prodSize': prodSize})


    except Exception as e:
        return json.dumps({'success': 'False', 'mex': str(e), 'jpegs': jpegs, 'titles': titles, 'uuids': uuids,
                           'prodSize': prodSize})






# def run_old(shp_names, sat, startDate, endDate, clouds, usr, pwd):
#     # clean old files in tmp folder
#     TMPDIR=os.path.join(GLOBALS['data_paths']['tmp_data'], "download")
#     if not os.path.exists(TMPDIR):
#         os.makedirs(TMPDIR)
#     for f in os.listdir(TMPDIR):
#         if os.stat(os.path.join(TMPDIR, f)).st_mtime < time.time() - 1 * 86400:
#             try:
#                 if os.path.isfile(os.path.join(TMPDIR, f)):
#                     os.remove(os.path.join(TMPDIR, f))
#                 else:
#                     shutil.rmtree(os.path.join(TMPDIR, f), ignore_errors=True)
#             except Exception as e:
#                 pass
#
#
#     timestr = time.strftime("%Y%m%d-%H%M%S")
#     TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "download",timestr)
#     if not os.path.exists(TMPDIR):
#         os.makedirs(TMPDIR)
#
#     try:
#         jpegs = []
#         titles = []
#         uuids = []
#         prodSize = []
#
#
#
#         shp_names = json.loads(shp_names)
#         for shp in shp_names:
#
#
#             shpname = os.path.basename(shp)
#             shpbasename = shpname.replace(".shp", "")
#
#             tmpshp = os.path.join(TMPDIR,shpbasename+'.json')
#             if os.path.exists(tmpshp):
#                 os.remove(tmpshp)
#             # ---------- OPEN SHP -----------
#             os.system('ogr2ogr -f GeoJSON -t_srs EPSG:4326 ' + tmpshp + ' '+ shp)
#
#
#             api = SentinelAPI(usr, pwd, 'https://apihub.copernicus.eu/apihub/')
#             # api = SentinelAPI(usr, pwd, 'https://scihub.copernicus.eu/dhus/')
#
#             # search by polygon, time, and SciHub query keywords
#             footprint = geojson_to_wkt(read_geojson(tmpshp))
#             products = api.query(footprint,
#                                  date=(startDate, endDate),
#                                  platformname=sat,
#                                  cloudcoverpercentage=(0, int(clouds)),
#                                  # show_progressbars=True,
#                                  order_by='cloudcoverpercentage',
#                                  # area_relation({'Intersects', 'Contains', 'IsWithin'}
#                                  )
#
#             for prod in products:
#                 jpegs.append(products[prod]['link_icon'])
#                 titles.append(products[prod]['title'])
#                 uuids.append(products[prod]['uuid'])
#                 prodSize.append(products[prod]['size'])
#
#
#             # return json.dumps({'jpegs':jpegs,'uuids':uuids})
#             return json.dumps({'success':'True', 'mex':'', 'jpegs':jpegs, 'titles':titles,'uuids':uuids, 'prodSize':prodSize})
#
#
#     except Exception as e:
#
#         if ('password' in str(e)):
#             print('HERE')
#             e="Wrong username or password."
#         return json.dumps({'success': 'False', 'mex':str(e) , 'jpegs':jpegs, 'titles':titles,'uuids':uuids, 'prodSize':prodSize})
#
