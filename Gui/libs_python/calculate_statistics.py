#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# statistics are calculated in multiprocessing , needs libs in path
import sys
import os
# libsPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'modules')
# if libsPath not in sys.path:
#     #sys.path.insert(0, libsPath)
#     sys.path.append(libsPath)

# Import local libraries
import sys
import ImageStore
import CSImageStore


if __name__ == '__main__':

    currentImageStore = ImageStore
    if sys.argv[1] == 'CSImageStore':
        currentImageStore = CSImageStore

    try:
        imageList = None
        imageList = currentImageStore.get_without_statistics()
        if len(imageList) > 0:
            for image in imageList:
                currentImageStore.set_status(image, 'pending')

            for image in imageList:
                currentImageStore.update_file_statistics(image)

        imageList = None
        currentImageStore = None
        ImageStore = None
        CSImageStore = None
        exit(0)

    except Exception as e:
        print("Error in Calculate_stats.py")
        print(str(e))
