#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import cgi
import subprocess
import psutil
import json

# Import local libraries
from __config__ import *
import LogStore


def execute_script(script_path, args):
    hide_shell = False
    script_path = os.path.join(GLOBALS['Tools'], script_path)
    args = ["python", script_path] + args
    # print(args)
    # multiprocessing_cpu_count() does not work if called via CGI
    # scipy.cluster raises error
    # reset os.environ['NUMBER_OF_PROCESSORS']  variable in case is needed in sub-modules
    os.environ['NUMBER_OF_PROCESSORS'] = str(psutil.cpu_count())
    #os.environ["PYTHON_MODULES"] = os.path.join(GLOBALS['root_path'], 'Gui','libs_python', 'modules')

    try:
        subprocess.Popen(args, shell=hide_shell,env=dict(os.environ))
        return True
    except Exception as e:
        print("Error subprocess.Popen "+str(e))
        return False


def run(params):

    # retrieve parameters from request #
    #params = cgi.FieldStorage()
    toolID = params.get("toolID").decode('utf-8')
    print(toolID)

    if not os.path.exists(GLOBALS['data_paths']['tmp_data']):
        os.makedirs(GLOBALS['data_paths']['tmp_data'])


    # -------------  LOG MONITOR  -------------
    if toolID == 'processLogRemove':
        log_id = params.get('log_id').decode('utf-8')
        if log_id == 'all':
            LogStore.remove_all()
        else:
            LogStore.remove(log_id)
        return 'Log removed'

    elif toolID == 'processKill':
        import signal

        log_id = params.get('log_id').decode('utf-8')
        log_obj = LogStore.LogProcessing()
        log_obj.get(log_id)
        try:
            print(log_obj.log_structure['PID'])
            os.kill(int(log_obj.log_structure['PID']), signal.SIGKILL)
            result = 'Process killed'
        except:
            result = 'Process does not exists'
        log_obj.send_error("Processing STOPPED")
        log_obj.close()
        return result

    # -------------  IMPACT tool UPDATE  -------------
    elif toolID == 'update':
        import sys
        if sys.platform != "linux":
            result = subprocess.Popen(os.path.join(GLOBALS['WinLibs'], 'UPDATE.bat'))
            return "Update OK" if result else "Update ERROR"
        else:
            return "Automatic update under development, pleas use git pull from commandline"

    # -------------  LANDSAT GZ to GeoTiff  -------------
    elif toolID == 'landsat_to_toa':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        outdir = params.get('outdir', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        proc_mode = params.get('proc_mode', b'DN').decode('utf-8')
        proc_opt = params.get('proc_opt', b'TOAb').decode('utf-8')
        date_mode = params.get('date_mode', b'ORIGINAL').decode('utf-8')
        result = execute_script("TOA_Reflectance/RUN_Landsat_ZIP_DN_TOA.py", [overwrite, outdir, img_names, proc_mode, date_mode, proc_opt])
        return "Preprocessing OK" if result else "Preprocessing ERROR"

    # -------------  Sentinel2 zip to GeoTiff  -------------
    elif toolID == 'sentinel2_to_toa':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        outdir = params.get('outdir', b'').decode('utf-8')
        bands = params.get('bands', b'').decode('utf-8')
        projection = params.get('projection', b'').decode('utf-8')
        resolution = params.get('resolution', b'').decode('utf-8')
        if resolution != 'highest' and resolution != 'lowest':
            resolution = 'user -tr '+resolution+' '+resolution
        outtype = params.get('outtype', b'').decode('utf-8')
        aoi = params.get('aoi', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        outname = params.get('outname', b'Original').decode('utf-8')

        result = execute_script("Pre-Processing/RUN_Process_Sentinel2.py", [overwrite, outdir, bands, projection, resolution, outtype, aoi, outname, img_names])
        return "Preprocessing OK" if result else "Preprocessing ERROR"

    # -------------  DN to TOA reflectance  -------------
    elif toolID == 'rapideye_to_toa':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        outdir = params.get('outdir', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        result = execute_script("TOA_Reflectance/RUN_Rapideye_DN_to_TOA_Reflectance.py", [overwrite, outdir, img_names])
        return "DN to TOA OK" if result else "DN to TOA ERROR"

    # -------------  CLIP  -------------
    elif toolID == 'clip':

        shp_names = params.get('shp_names', b'[]').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        overwrite_clip = params.get('overwrite_clip', b'No').decode('utf-8')
        use_features = params.get('use_features', b'').decode('utf-8')
        touching = params.get('touching', b'Yes').decode('utf-8')

        result = execute_script("Clip/Clip_data_from_shp.py", [shp_names, img_names, overwrite_clip, use_features, touching])
        return "Clip OK" if result else "Clip ERROR"

    # -------------  CLASSIFICATION  -------------
    elif toolID == 'class':
        overwrite = params.get('overwrite', 'No').decode('utf-8')
        fnorm = params.get('fnorm', '').decode('utf-8')
        palette = params.get('palette', 'Yes').decode('utf-8')
        kernel_size = params.get('kernel_size', '0').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        result = execute_script("Classification/RUN_PiNo_classifier.py", [overwrite, fnorm, palette, kernel_size, img_names])
        return "Classification OK" if result else "Classification ERROR"

    # -------------  K-Means Clustering  -------------
    elif toolID == 'kmeans':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        out_suffix = params.get('out_suffix', b'').decode('utf-8')
        num_cluster = params.get('num_cluster', b'').decode('utf-8')
        num_iteration = params.get('num_iteration', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')

        #script_path = os.path.join(GLOBALS['root_path'], 'Tools', 'Classification','RUN_image_kmeans_va.bat')
        #os.system('E:\IMPACT\IMPACT2\IMPACT\Tools\Classification\RUN_image_Kmeans_va.bat')
        result = execute_script("Classification/RUN_image_Kmeans_class.py", [img_names, out_suffix, overwrite, num_cluster, num_iteration])
        #args = "python E:/IMPACT/IMPACT2/IMPACT/Tools/Classification/RUN_image_Kmeans_va.py"#+img_names+" "+out_suffix+" "+ overwrite+" "+ num_cluster+" "+num_iteration
        #os.system(args)
        return "Classification OK" if result else "Classification ERROR"

    # -------------  Water Balance  -------------
    elif toolID == 'waterBalance':

        img_names = params.get('img_names', []).decode('utf-8')
        # max length of PARAMS
        if len(img_names) > 20000:
            out_file = os.path.join(GLOBALS['data_paths']['tmp_data'], "water_mosaic.json")
            if os.path.exists(out_file):
                os.remove(out_file)
            file_handler = open(out_file, 'w')
            file_handler.write(img_names)
            file_handler.close()
            img_names = out_file

        start_value = params.get('start_value', b'').decode('utf-8')
        end_value = params.get('end_value', b'').decode('utf-8')
        outname = params.get('outname', b'').decode('utf-8')
        nir = params.get('nir', b'').decode('utf-8')
        swir = params.get('swir', b'').decode('utf-8')

        outEPSG = params.get('outEPSG', b'').decode('utf-8')
        psizeX = params.get('psizeX', b'30').decode('utf-8')
        psizeY = params.get('psizeY', b'30').decode('utf-8')


        #out_pixel_size = params.get('out_pixel_size', '')
        reproject = params.get('reproject', b'No').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("Classification/WaterBalance.py", [overwrite, img_names, start_value, end_value, outname, nir, swir, reproject, outEPSG, psizeX, psizeY])
        return "Water Balance OK" if result else "Water Balance ERROR"



    # -------------------  PCA   --------------------
    elif toolID == 'pca':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        out_suffix = params.get('out_suffix', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        result = execute_script("Classification/RUN_image_pca.py", [img_names, out_suffix, overwrite])

        return "Classification OK" if result else "Classification ERROR"
    # -------------  UNMIXING  -------------
    elif toolID == 'unmix':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        fnorm = params.get('fnorm', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        result = execute_script("Classification/RUN_image_unmixing.py", [overwrite, fnorm, img_names])
        return "Unmix OK" if result else "Unmix ERROR"

    # -------------  NDVI  -------------
    elif toolID == 'ndvi_threshold':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        num_class = params.get('num_class', b'').decode('utf-8')
        red = params.get('red', b'').decode('utf-8')
        nir = params.get('nir', b'').decode('utf-8')
        save_index = params.get('save_index', b'').decode('utf-8')
        name_index = params.get('name_index', b'').decode('utf-8')
        img_names = params.get('img_names', b'[]').decode('utf-8')
        result = execute_script("Classification/RUN_image_ndvi_threshold.py", [overwrite, num_class, red, nir, save_index, name_index, img_names])
        return "Index Threshold OK" if result else "Index Threshold ERROR"

    # -------------  SEGMENTATION  -------------
    elif toolID == 'segmentation':
        print(params)
        update_shp = params.get('update_shp', b'').decode('utf-8')
        img_names = params.get('inputs', []).decode('utf-8')
        multiprocessing = params.get("multiprocessing", b'No').decode('utf-8')
        singledate = params.get('singledate', b'').decode('utf-8')
        isodata = params.get('isodata', b'').decode('utf-8')

        # BANDS and WEIGTH
        bands = str(int(params.get('R_img_bands', b'').decode('utf-8'))-1)
        #weight = str(params.getvalue('R_weights', ''))
        if params.get('G_img_bands', b'-').decode('utf-8') != '-':
            bands = bands+','+str(int(params.get('G_img_bands', b'').decode('utf-8'))-1)
            #weight = weight+','+str(params.getvalue('G_weights', ''))
        if params.get('B_img_bands', b'-').decode('utf-8') != '-':
            bands = bands+','+str(int(params.get('B_img_bands', b'').decode('utf-8'))-1)
            #weight = weight+','+str(params.getvalue('B_weights', ''))
        mmu = params.get('mmu', b'').decode('utf-8')
        compactness = params.get('compactness', b'').decode('utf-8')
        color = params.get('color', b'').decode('utf-8')
        similarity = params.get('similarity', b'0.1').decode('utf-8')
        suffix_name = str(params.get("suffix_name", b'').decode('utf-8'))
        tiling = params.get('tiling', b'').decode('utf-8')
        strategy = params.get('strategy', b'baatz').decode('utf-8').lower()
        AggregationRules = params.get('AggregationRules', b'').decode('utf-8')
        preclassification = 'Not used, 2 be deleted'
        old_shp = params.get('old_shp', b'').decode('utf-8')
        old_attrib = params.get('old_attrib', b'').decode('utf-8')
        result = execute_script("Segmentation/Segmentation.py", [update_shp, multiprocessing, img_names, preclassification, singledate, isodata, bands, mmu, compactness, color, similarity, suffix_name, strategy, tiling, AggregationRules, old_shp, old_attrib])
        return "Segmantation OK" if result else "Segmantation ERROR"

    # -------------  Statistics  -------------
    elif toolID == 'statistics':
        shp_names = params.get('shp_names', b'').decode('utf-8')
        img_names = params.get('img_names', b'').decode('utf-8')
        nodata_list = params.get('nodata_list', b'').decode('utf-8')
        statsList = params.get('statsList', b'').decode('utf-8')
        statsBands = params.get('statsBands', b'0').decode('utf-8')
        result = execute_script("Vector/Compute_stats_fill_shapefile.py", [shp_names, img_names, nodata_list, statsList, statsBands])
        return "Statistics OK" if result else "Statistics ERROR"

    # -------------  FISHNET  -------------
    elif toolID == 'fishnet':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        template = params.get('template', b'').decode('utf-8')
        extent_top = params.get('extent_top', b'').decode('utf-8')
        extent_left = params.get('extent_left', b'').decode('utf-8')
        extent_right = params.get('extent_right', b'').decode('utf-8')
        extent_bottom = params.get('extent_bottom', b'').decode('utf-8')
        epsg = params.get('epsg', b'').decode('utf-8')
        cell_size_width = params.get('cell_size_width', b'').decode('utf-8')
        cell_size_height = params.get('cell_size_height', b'').decode('utf-8')
        num_columns = params.get('num_columns', b'').decode('utf-8')
        num_rows = params.get('num_rows', b'').decode('utf-8')
        step_x = params.get('step_x', b'0').decode('utf-8')
        step_y = params.get('step_y', b'0').decode('utf-8')
        randomVal = params.get('randomVal', b'100').decode('utf-8')
        output_name = params.get('output_name', b'').decode('utf-8')
        # output_folder = params.get('output_folder', '').decode('utf-8')
        output_fullPath = os.path.join(GLOBALS['data_paths']['data'], output_name)
        result = execute_script("Vector/Create_fishnet.py", [overwrite, template, extent_top, extent_left, extent_right, extent_bottom, epsg, cell_size_width, cell_size_height, num_columns, num_rows, step_x, step_y, randomVal, output_fullPath])
        return "Fishnet OK" if result else "Fishnet ERROR"

    # -------------  Forest Normalization  -------------
    elif toolID == 'fnorm':
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        img_names = params.get('img_names', b'').decode('utf-8')
        result = execute_script("Evergreen_normalization/EVG_forest_normalization.py", [overwrite,img_names])
        return "F Norm OK" if result else "F Norm ERROR"

    # -------------  DegradationNBR -------------
    elif toolID == 'degradationNBR':
        master_img = params.get('master_img', b'[]').decode('utf-8')
        slave_img = params.get('slave_img', b'[]').decode('utf-8')
        master_mask = params.get('master_mask', b'[]').decode('utf-8')
        slave_mask = params.get('slave_mask', b'[]').decode('utf-8')
        outname = params.get('outname', b'').decode('utf-8')
        nir = params.get('nir', b'').decode('utf-8')
        swir = params.get('swir', b'').decode('utf-8')
        kernel_size = params.get('kernel_size', b'').decode('utf-8')
        #out_pixel_size = params.get('out_pixel_size', '')
        reproject = params.get('reproject', b'No').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("Degradation/DegradationNBR.py", [overwrite, master_img, slave_img, master_mask, slave_mask, outname, nir, swir, kernel_size, reproject])
        return "Degradation OK" if result else "Degradation ERROR"

    # -------------  CarbEF  -------------
    elif toolID == 'CarbEF':
        master_img = params.get('master_img', b'[]').decode('utf-8')
        outname = params.get('outname', b'').decode('utf-8')
        kernel_size = params.get('kernel_size', b'').decode('utf-8')
        biomass_value = params.get('biomass_value', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        #language = params.getvalue('repo_language', 'EN')
        startYY1 = params.get('startYY1', b'0').decode('utf-8')
        endYY1 = params.get('endYY1', b'0').decode('utf-8')
        startYY2 = params.get('startYY2', b'0').decode('utf-8')
        endYY2 = params.get('endYY2', b'0').decode('utf-8')
        useTwoPeriods = params.get('useTwoPeriods', b'Yes').decode('utf-8')
        forest_mmu_fraction = params.get('forest_mmu_fraction', b'30').decode('utf-8')
        useConversionMap = params.get('useConversionMap', b'No').decode('utf-8')
        conversionMap= params.get('conversionMap', b'[]').decode('utf-8')
        biomassDegradPercent = params.get('biomassDegradPercent',b'').decode('utf-8')
        useDisagShp = params.get('useDisagShp', b'No').decode('utf-8')
        disagShp = params.get('disagShp', b'[]').decode('utf-8')
        disagField = params.get('disagField', b'').decode('utf-8')
        useExceptMap = params.get('useExceptMap', b'No').decode('utf-8')
        exceptMap = params.get('exceptMap', b'[]').decode('utf-8')


        if len(str(startYY2)) == 0:
            startYY2 = '0'
        if len(str(endYY2)) == 0:
            endYY2 = '0'

        result = execute_script("Degradation/CarbEF.py", [overwrite, master_img, outname, kernel_size,
                                                          biomass_value, biomassDegradPercent,
                                                          startYY1, endYY1, startYY2, endYY2, useTwoPeriods,
                                                          forest_mmu_fraction,useConversionMap, conversionMap,
                                                          useDisagShp,disagShp, disagField, useExceptMap, exceptMap])
        return "CarbEF OK" if result else "CarbEF ERROR"

    # ------        vector2raster    ------------
    elif toolID == 'vector2raster':
        ids = params.get('ids', b'[]').decode('utf-8')
        full_path = params.get('full_path', b'[]').decode('utf-8')
        layername = params.get('layername', b'[]').decode('utf-8')
        outname = params.get('outname', b'[]').decode('utf-8')
        resolution = params.get('resolution', b'[]').decode('utf-8')
        overwrite = params.get('overwrite', b'[]').decode('utf-8')
        result = execute_script("Vector/Vector2raster.py", [overwrite, full_path, layername, outname, resolution, ids])
        return "vector2raster OK" if result else "vector2raster ERROR"

    # -------------  Mask creation from classification  -------------
    elif toolID == 'rasterMask':
        infile = params.get('infile', b'').decode('utf-8')
        minrange = params.get('minrange', b'').decode('utf-8')
        maxrange = params.get('maxrange', b'').decode('utf-8')

        result = execute_script("Raster/create_mask_from_classification.py", [infile,minrange,maxrange])
        return "Raster Mask OK " if result else "Raster Mask ERROR"

    # -------------  Rebuild Stats and Image pyramids  -------------
    elif toolID == 'buildPyramids':
        infile = params.get('infile', b'').decode('utf-8')
        result = execute_script("Raster/build_pyramids.py", [infile])
        return "Rebuild Stats  OK" if result else "Rebuild Stats  ERROR"

    # ----------------  pansharpen      -------------
    elif toolID == 'pansharpen':
        pan_names = params.get('pan_names', b'').decode('utf-8')
        img_names = params.get('img_names', []).decode('utf-8')
        bands = params.get('bands', b'').decode('utf-8')
        resampling = params.get('resampling', b'cubic').decode('utf-8')
        outDataType = params.get('outDataType', b'Byte').decode('utf-8')
        suffix = params.get('suffix', b'').decode('utf-8')
        exponent = params.get('exponent', b'').decode('utf-8')
        overwrite_pan = params.get('overwrite_pan', b'No').decode('utf-8')
        result = execute_script("Pansharpen/pansharpen.py", [pan_names,img_names,suffix,bands,resampling,outDataType,exponent,overwrite_pan])
        return "Pansharpen  OK" if result else "Pansharpen  ERROR"

    # -------------  mosaic  -------------
    elif toolID == 'mosaic':
        out_name = params.get('out_name', b'').decode('utf-8')
        img_names = params.get('img_names', []).decode('utf-8')
        # max length of PARAMS
        if len(img_names) > 20000:
            out_file = os.path.join(GLOBALS['data_paths']['tmp_data'], "mosaic.json")
            if os.path.exists(out_file):
                os.remove(out_file)
            file_handler = open(out_file, 'w')
            file_handler.write(img_names)
            file_handler.close()
            img_names = out_file



        separate = params.get('separate', b'No').decode('utf-8')
        force_reprojection = params.get('force_reprojection', b'Yes').decode('utf-8')
        nodata = params.get('nodata', b'0').decode('utf-8')
        psize = params.get('psize', b'').decode('utf-8')
        resampling = params.get('resampling', b'near').decode('utf-8')
        overwrite_mosa = params.get('overwrite_mosa', b'No').decode('utf-8')
        overviews = params.get('overviews', b'Yes').decode('utf-8')
        tap = params.get('tap', 'bNo').decode('utf-8')
        result = execute_script("Raster/Mosaic.py", [out_name,img_names,separate,force_reprojection,nodata,psize,resampling,overwrite_mosa, overviews, tap])
        return "Mosaic  OK" if result else "Mosaic  ERROR: "+str(result)

    # ------------- raster conversion ---------------------------
    elif toolID == 'RasterConversion':
        out_name = params.get('out_name', b'').decode('utf-8')
        img_names = params.get('img_names', []).decode('utf-8')
        projection = params.get('projection', b'').decode('utf-8')
        nodata = params.get('nodata', b'').decode('utf-8')
        psize = params.get('psize', b'').decode('utf-8')
        mytype = params.get('type', b'').decode('utf-8')
        rescale = params.get('rescale', b'').decode('utf-8')
        bands = params.get('bands', b'').decode('utf-8')
        format = params.get('format', b'GTiff').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("Raster/RasterConversion.py", [out_name,img_names,projection,nodata,psize,mytype,rescale,bands,format,overwrite])
        return "RasterConversion  OK" if result else "RasterConversion  ERROR"


    # ------------- vector conversion ---------------------------
    elif toolID == 'VectorConversion':
        out_name = params.get('out_name', b'').decode('utf-8')
        img_names = params.get('img_names', []).decode('utf-8')
        psize = params.get('psize', b'').decode('utf-8')
        pct = params.get('pct', b'None').decode('utf-8')
        attribute = params.get('attribute', b'').decode('utf-8')
        attributeType = params.get('attributeType', b'Auto').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("Vector/VectorConversion.py", [out_name, img_names, psize, attribute, attributeType, pct, overwrite])
        return "Vector Conversion  OK" if result else "vector Conversion  ERROR"

    elif toolID == 'raster_recode':
        input_file = params.get('input_file', b'').decode('utf-8')
        band = params.get('band', b'').decode('utf-8')
        recode_values = params.get('recode_values', b'[]').decode('utf-8')
        save_to_new = params.get('save_to_new', b'No').decode('utf-8')
        out_suffix = params.get('out_suffix', b'').decode('utf-8')
        recode_extent = params.get('recode_extent', b'').decode('utf-8')
        recode_AOI = params.get('recode_AOI', b'').decode('utf-8')
        result = execute_script("Raster/RasterRecoder.py", [input_file, band, recode_values, save_to_new, out_suffix, recode_extent, recode_AOI])
        return "Raster recode OK" if result else "Raster recode  ERROR"

    elif toolID == 'rasterPCTmanager':
        input_file = params.get('infile', b'').decode('utf-8')
        pct = params.get('pct', b'').decode('utf-8')
        result = execute_script("Raster/RasterPCTmanager.py", [input_file, pct])
        return "Raster PCT OK" if result else "Raster PCT  ERROR"

    # ------------- Raster calculator ---------------------------
    elif toolID == 'RasterCalculator':
        inputs = params.get('inputs', b'[]').decode('utf-8')
        output_name = params.get('output_name', b'').decode('utf-8')
        no_data = params.get('no_data', b'').decode('utf-8')
        expression = params.get('expression', b'').decode('utf-8')
        mytype = params.get('type', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        reproject = params.get('reproject', b'No').decode('utf-8')
        result = execute_script("Raster/RasterCalculator.py", [inputs, output_name, no_data, expression, mytype, overwrite, reproject])
        return "Raster Calculator OK" if result else "Raster PCT  ERROR"

    # ------------- ForestER ---------------------------
    elif toolID == 'ForestER':
        mapT1 = params.get('mapT1', b'').decode('utf-8')
        mapT2 = params.get('mapT2', b'').decode('utf-8')
        disturbMap = params.get('disturbMap', b'').decode('utf-8')
        em_factor_map = params.get('em_factor_map', b'').decode('utf-8')
        biomass_value = params.get('biomass_value', b'').decode('utf-8')
        biomassDegradPercent = params.get('biomassDegradPercent', b'0').decode('utf-8')
        useConversionMap = params.get('useConversionMap', b'False').decode('utf-8')
        outEPSG = params.get('outEPSG', b'').decode('utf-8')
        nodata = params.get('nodata_value', b'').decode('utf-8')
        out_name = os.path.join(GLOBALS['data_paths']['data'], params.get('out_name', b'ForestER_output').decode('utf-8'))+'.tif'
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("Degradation/ForestER.py", [mapT1, mapT2, disturbMap, em_factor_map, biomass_value, biomassDegradPercent, useConversionMap, out_name, overwrite, outEPSG, nodata])

        return "ForestER OK" if result else "ForestER ERROR"
    # ------------- Raster calculator ---------------------------
    elif toolID == 'mspa':


        mspa_nodata = params.get('mspa_nodata', b'0').decode('utf-8')
        mspa_background = params.get('mspa_background', b'1').decode('utf-8')
        mspa_foregroud = params.get('mspa_foregroud', b'2').decode('utf-8')
        inputs = params.get('img_names', b'[]').decode('utf-8')
        out_suffix = params.get('out_suffix', b'').decode('utf-8')
        proc_mode = params.get('proc_mode', b'mspa').decode('utf-8')
        band = params.get('mspaBand', b'1').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')

        mspa_edge = params.get('mspa_edge', b'1').decode('utf-8')
        mspa_connectivity = params.get('mspa_connectivity', b'8').decode('utf-8')
        mspa_intFlag = params.get('mspa_intFlag', b'1').decode('utf-8')
        mspa_transition = params.get('mspa_transition', b'1').decode('utf-8')

        result = execute_script("Mspa/Mspa.py", [inputs, band, proc_mode, out_suffix, mspa_nodata, mspa_background, mspa_foregroud, overwrite, mspa_edge, mspa_connectivity, mspa_intFlag, mspa_transition])
        return "Mspa OK" if result else "MSPA  ERROR"

    # ------------- Raster calculator ---------------------------
    elif toolID == 'roadMapper':

        roadBand = params.get('roadBand', '0').decode('utf-8')
        roadKernel = params.get('roadKernel', '7').decode('utf-8')
        roadMMU = params.get('roadMMU', '300').decode('utf-8')
        roadThreshold = params.get('roadThreshold', '2.5').decode('utf-8')
        inputs = params.get('img_names', b'[]').decode('utf-8')
        out_suffix = params.get('out_suffix', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')

        result = execute_script("Raster/Road_Mapper.py", [inputs, out_suffix, overwrite, roadBand, roadKernel, roadMMU, roadThreshold ])
        return "RoadMapper OK" if result else "RoadMapper  ERROR"


    # ------------- Raster calculator ---------------------------
    elif toolID == 'ConfusionMatrix':
        attribute = params.get('attribute', '-').decode('utf-8')
        weight = params.get('weight', '-').decode('utf-8')
        map = params.get('map', '').decode('utf-8')
        attribTif = params.get('attribTif', '1').decode('utf-8')
        attribShp = params.get('attribShp', '-').decode('utf-8')
        noClass = params.get('noClass', '').decode('utf-8')

        inShp = params.get('inShp', b'[]').decode('utf-8')
        out_name = params.get('out_name', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')

        result = execute_script("AccuracyA/ConfusionMatrix.py", [inShp, attribute, weight, map, attribTif, attribShp, noClass, out_name, overwrite])
        return "Confusion Matrix OK" if result else "Confusion Matrix  ERROR"

    elif toolID == 'PhotosToKML':
        inDir = params.get('inDir', b'[]').decode('utf-8')
        out_name = params.get('out_name', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        result = execute_script("AccuracyA/PhotosToKML.py", [inDir, out_name, overwrite])
        return "PhotosToKML OK" if result else "PhotosToKML  ERROR"

    elif toolID == 'buildSpatialIndex':
        infile = params.get('infile', b'').decode('utf-8')
        result = execute_script("Vector/build_spatial_index.py", [infile])
        return "Rebuild Stats  OK" if result else "Rebuild Stats  ERROR"

    elif toolID == 'eeAuthenticate':
        result = execute_script("Gee/eeAuthenticate.py",[])
        return "ee OK"

    elif toolID == 'sentinelDownload':
        uuids = params.get('uuids', b'[]').decode('utf-8')
        names = params.get('names', b'[]').decode('utf-8')
        outpath = params.get('outpath', b'').decode('utf-8')

        usr = params.get('usr', b'').decode('utf-8')
        pwd = params.get('pwd', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')

        result = execute_script("Download/SentinelDownload.py", [uuids, names, outpath, usr, pwd, overwrite])

        return "Download  OK"

    elif toolID == 'Rusle':
        aoi = params.get('aoi', b'[]').decode('utf-8')
        dem = params.get('dem', b'[]').decode('utf-8')
        demFill = params.get('demFill', b'').decode('utf-8')
        demMedian = params.get('demMedian', b'').decode('utf-8')

        rusleSand = params.get('rusleSand', b'[]').decode('utf-8')
        rusleSilt = params.get('rusleSilt', b'[]').decode('utf-8')
        rusleClay = params.get('rusleClay', b'[]').decode('utf-8')
        rusleCarbon = params.get('rusleCarbon', b'[]').decode('utf-8')

        R_file_raw = params.get('R_file', b'[]').decode('utf-8')
        rainStart = params.get('rainStart', b'').decode('utf-8')
        rainEnd = params.get('rainEnd', b'').decode('utf-8')
        Rstrategy = params.get('Rstrategy', b'').decode('utf-8')

        LC = params.get('LC', b'[]').decode('utf-8')
        LC_YY = params.get('LC_YY', b'').decode('utf-8')
        LC_type = params.get('LC_type', b'').decode('utf-8')

        LC_Ids_Cfact = params.get('LC_Ids_Cfact', b'[]').decode('utf-8')
        Mfact = params.get('Mfact', b'[]').decode('utf-8')
        Agri_Pfact = params.get('Agri_Pfact', b'[]').decode('utf-8')

        out_name = params.get('out_name', b'').decode('utf-8')
        out_epsg = params.get('out_epsg', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        save_tmp = params.get('save_tmp', b'Yes').decode('utf-8')
        resolution = params.get('resolution', b'').decode('utf-8')
        print('RUSEL RES')
        print(resolution)

        result = execute_script("Rusle/RUSLE_model.py", [aoi, dem, demFill, demMedian, rusleSand, rusleSilt, rusleClay, rusleCarbon, R_file_raw, rainStart, rainEnd, Rstrategy, LC, LC_YY, LC_type, LC_Ids_Cfact, Mfact, Agri_Pfact, out_name, out_epsg, overwrite, save_tmp, resolution])

        return "Rusle OK"

    elif toolID == 'RandomSampling':
        totalPoint = params.get('totalPoint', '0').decode('utf-8')
        minPoint = params.get('minPoint', '0').decode('utf-8')
        outObject = params.get('outObject', 'False').decode('utf-8')
        buffer = params.get('buffer', '0').decode('utf-8')

        strata = params.get('strata', 'False').decode('utf-8')
        exclusion = params.get('exclusion', b'[]').decode('utf-8')
        classField = params.get('classField', '').decode('utf-8')

        inShp = params.get('inShp', b'[]').decode('utf-8')
        inEPSG = params.get('inEPSG', b'').decode('utf-8')
        out_name = params.get('out_name', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')

        result = execute_script("AccuracyA/Stratified_random_sampling.py", [inShp, out_name, overwrite, inEPSG, strata, outObject, totalPoint, classField, exclusion, minPoint, buffer ])
        return "RandomSampling OK" if result else "RandomSampling  ERROR"

    elif toolID == 'EUFOintersect':

        aoi = params.get('aoi', b'[]').decode('utf-8')
        attribute = params.get('attribute', b'[]').decode('utf-8')
        forestMap = params.get('forestMap', b'[]').decode('utf-8')
        LC_Ids = params.get('LC_Ids', b'[]').decode('utf-8')

        proc_mode = params.get('proc_mode', b'[]').decode('utf-8')
        proc_type = params.get('proc_type', b'[]').decode('utf-8')


        out_name = params.get('out_name', b'').decode('utf-8')
        overwrite = params.get('overwrite', b'No').decode('utf-8')
        save_tmp = params.get('save_tmp', b'Yes').decode('utf-8')
        result = execute_script("Eudr/vector_intersection_GFCM2020.py", [aoi, attribute, forestMap, LC_Ids, proc_mode, proc_type, save_tmp, out_name, overwrite])
        return "EUFO_intersect OK" if result else "EUFO_intersect  ERROR"



    elif toolID is None:
        return "Done!"

# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     print main()
