#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
import ImageStore
import Settings


#if __name__ == '__main__':
def run(shp_name, missing_fields):
    #print "Content-type: text/html;\n"

    SETTINGS = Settings.load()

    # params = cgi.FieldStorage()
    # shp_name = params.getvalue("shp_name")
    # missing_fields = json.loads(params.getvalue("fields"))

    ID = str(SETTINGS['vector_editing']['attributes']['ID'])
    #CLASS = str(SETTINGS['vector_editing']['attributes']['class_t1'])
    #CLUSTER = str(SETTINGS['vector_editing']['attributes']['cluster_t1'])
    #COMMENTS = str(SETTINGS['vector_editing']['attributes']['comments'])

    try:

        shp_ds = ogr.Open(shp_name, 1)
        layer = shp_ds.GetLayer(0)
        layerName = layer.GetName()
        layerDefinition = layer.GetLayerDefn()

        existing_fields = []
        for i in range(layerDefinition.GetFieldCount()):
            existing_fields.append(layerDefinition.GetFieldDefn(i).GetName())

        newFields=[]
        mex=''
        for field in missing_fields:
            if field not in existing_fields:
                if field in [x for x in existing_fields]:
                    mex+= "Field '"+ str(field) + "' already exists in DBF; skip \n"
                    continue
                    #import sys
                    #sys.exit()

                if field == ID:
                    f1 = ogr.FieldDefn(str(field), ogr.OFTReal)
                else:
                    f1 = ogr.FieldDefn(str(field), ogr.OFTString)

                layer.CreateField(f1)
                f1 = None
                newFields.append('\"'+str(field)+'\"')

        if len(newFields) > 0 :
            newFields = ' = 0,'.join(newFields)+' = 0'
            sql = ' UPDATE \"' + layerName + '\" SET ' + newFields
            layer = shp_ds.ExecuteSQL(sql, dialect='SQLITE')
            shp_ds.ReleaseResultSet(layer)
            shp_ds.Destroy()
            shp_ds = None

            # re-open to fill ID val
            shp_ds = ogr.Open(shp_name, 1)
            layer = shp_ds.GetLayer(0)
            # Populate ID
            feat = layer.GetNextFeature()
            if ID not in existing_fields:
                while feat is not None:
                    feat.SetField(ID, feat.GetFID())
                    layer.SetFeature(feat)
                    feat = layer.GetNextFeature()
            shp_ds.Destroy()
            shp_ds = None
            layer = None

        ImageStore.update_file_statistics(shp_name)

        if mex == "":
            return 'OK'
        else:
            return mex

    except Exception as e:
        print('Error: '+str(e))
        return str(e)
