import sys
import os.path
import json
import random
import subprocess
import numpy

# import local libraries
from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

import sys
import os.path
import json
import random
import subprocess
import numpy

# import local libraries
from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

def query_tiff_by_point(filename, lon, lat, band=0, lat_lon_epsg=3857):
    """ Query Tiff by point and retrieve pixel value """
    gdal.AllRegister()
    data_source = gdal.Open(filename)

    num_bands = data_source.RasterCount
    num_col = data_source.RasterXSize
    num_line = data_source.RasterYSize
    records = []

    try:
        import numpy
        import struct
        # retrieve x,y coordinates from given lat,lon
        target_ref = osr.SpatialReference()
        target_ref.ImportFromWkt(data_source.GetProjectionRef())
        point = __get_point_geometry(lon, lat, target_ref, lat_lon_epsg)
        geo_transform = data_source.GetGeoTransform()
        x, y = point.GetY(), point.GetX()
        print(x)
        print(y)
        px = (x - geo_transform[0]) / geo_transform[1]  # x pixel
        py = (y - geo_transform[3]) / geo_transform[5]  # y pixel

        if px < 0 or py < 0 :
            data_source = None
            return []
        else:
            px = int(px)
            py = int(py)

        # for each band

        if (px < 0 or px >= num_col) or (py < 0 or py >= num_line):
            data_source = None
            return []

        for band_index in range(1, num_bands+1):
            if band != 0 and band != band_index:
                continue
            # retrieve pixel value from raster band
            band_obj = data_source.GetRasterBand(band_index)
            pixel_value = band_obj.ReadAsArray(px, py, 1, 1)
            #print pixel_value[0][0]
            if pixel_value is None:
                # out of image
                return []
            band_info = {
                'band': band_index,
                'value': str(pixel_value[0][0]),
                'x': px,
                'y': py,
                'Geox': x,
                'Geoy': y,
            }
            records.append(band_info)
    except Exception as e:
        pass

    data_source = None
    return records


def __get_point_geometry(lon, lat, target_ref=None, lat_lon_epsg=4326, myBuffer=0):
    """ Get point geometry from coordinates (4326) transformed to file projection """
    point = ogr.CreateGeometryFromWkt("POINT (" + str(float(lon)) + " " + str(float(lat)) + ")")
    if myBuffer:
        point = point.Buffer(myBuffer)
    source_ref = osr.SpatialReference()
    source_ref.ImportFromEPSG(int(lat_lon_epsg))
    if target_ref is not None and target_ref != source_ref:
        transform = osr.CoordinateTransformation(source_ref, target_ref)
        point.Transform(transform)
    return point

if __name__ == '__main__':
  print('Running')
  res=query_tiff_by_point('D:\IMPACT4_Apache\DATA\Class.tif',911009.15328316,4874828.4582455)
  print(res)



if __name__ == '__main__':
  print('Running')
  res=query_tiff_by_point('D:\IMPACT4_Apache\DATA\Class.tif',911009.15328316,4874828.4582455)
  print(res)
