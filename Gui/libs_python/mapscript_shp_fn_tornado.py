#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys 
import mapscript

# Import local libraries
import Mapserver
import Colors


def init_layer(map_obj, name, type, data, units):
    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = name
    layer_obj.status = mapscript.MS_ON
    layer_obj.type = type
    layer_obj.setConnectionType(mapscript.MS_OGR, None)
    layer_obj.connection = data
    if units != 'meters':
        layer_obj.units = mapscript.MS_DD
    return layer_obj


def skeleton_layer(map_obj, data, units, max_extent_xy, rgb, outline_size, simplificationRequired, simplificationMethod, layer_type):

    if 'POINT' not in layer_type :
        layer_obj = init_layer(map_obj, "Skeleton", mapscript.MS_LAYER_LINE, data, units)
        if simplificationRequired == 'true':
            if simplificationMethod == 'generalize':
                if max_extent_xy >= 100000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*3 ))")				# 70s as with none
                elif max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*2 ))")				# 70s as with none
                elif max_extent_xy >= 25000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*1.5 ))")				# 70s as with none
            else:
                if max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( smoothsia([shape],3,1))")				# 70s as with none

    else:
        layer_obj = init_layer(map_obj, "Skeleton", mapscript.MS_LAYER_POINT, data, units)
        layer_obj.setGeomTransform("( buffer([shape],[map_cellsize]*5 ))")

        symbolObj = mapscript.symbolObj('circle')
        symbolObj.type = mapscript.MS_SYMBOL_ELLIPSE
        index = map_obj.symbolset.appendSymbol(symbolObj)

    layer_obj.setProjection('AUTO')

    class_obj = mapscript.classObj(layer_obj)
    style_obj = mapscript.styleObj(class_obj)
    style_obj.outlinecolor.setRGB(rgb[0], rgb[1], rgb[2])

    style_obj.width = outline_size
    #style_obj.size = outline_size

    if 'POINT' in layer_type :
        style_obj.symbol = index

    return layer_obj


def shapefile_layer(map_obj, data, units, max_extent_xy, classIDs, classItem, classColors, simplificationRequired, simplificationMethod, classVisible, layer_type):

    if 'POINT' not in layer_type :
        layer_obj = init_layer(map_obj, "Shapefile", mapscript.MS_LAYER_POLYGON, data, units)

        if simplificationRequired == 'true':
            if simplificationMethod == 'generalize':
                if max_extent_xy >= 100000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*3 ))")				# 70s as with none
                elif max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*2 ))")				# 70s as with none
                elif max_extent_xy >= 25000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*1.5 ))")				# 70s as with none
            else:
                if max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( smoothsia([shape],3,1))")				# 70s as with none
    else:
        layer_obj = init_layer(map_obj, "Shapefile", mapscript.MS_LAYER_POINT, data, units)
        layer_obj.setGeomTransform("( buffer([shape],[map_cellsize]*5 ))")

        symbolObj = mapscript.symbolObj('circle')
        symbolObj.type = mapscript.MS_SYMBOL_ELLIPSE
        index = map_obj.symbolset.appendSymbol(symbolObj)

    layer_obj.setProjection("AUTO")

    if classIDs != '' and classItem != '':
        # ------  DRAW CLASS MODE  -------
            layer_obj.classitem = classItem
            for i in range(len(classIDs)):
                class_i_obj = mapscript.classObj(layer_obj)
                class_i_obj.setExpression(classIDs[i])
                #class_i_obj.setExpression('([' + classItem + '] == ' + classIDs[i] + ')')
                style_i_obj = mapscript.styleObj(class_i_obj)
                tmp_color = classColors[i].split(",")
                style_i_obj.outlinecolor.setRGB(0, 0, 0)
                style_i_obj.width = 0.2
                if 'POINT' in layer_type :
                    style_i_obj.symbol = index

                # show only class selected by the legend while in editing mode
                if classIDs[i] in classVisible:
                    style_i_obj.color.setRGB(int(tmp_color[0]), int(tmp_color[1]), int(tmp_color[2]))

            class_i_obj = mapscript.classObj(layer_obj)
            style_i_obj = mapscript.styleObj(class_i_obj)
            style_i_obj.color = mapscript.colorObj(200, 200, 200, 50)
            style_i_obj.outlinecolor.setRGB(0, 0, 0)
            style_i_obj.width = 0.5


    return None


def shapefile_IDS_selection_layer(map_obj, data, units, max_extent_xy, ids, simplificationRequired, simplificationMethod, layer_type):

    if 'POINT' not in layer_type :
        layer_obj = init_layer(map_obj, "Selection", mapscript.MS_LAYER_POLYGON, data, units)
        if simplificationRequired == 'true':
            if simplificationMethod == 'generalize':
                if max_extent_xy >= 100000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*3 ))")				# 70s as with none
                elif max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*2 ))")				# 70s as with none
                elif max_extent_xy >= 25000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*1.5 ))")				# 70s as with none
            else:
                if max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( smoothsia([shape],3,1))")				# 70s as with none
    else:
        layer_obj = init_layer(map_obj, "Selection", mapscript.MS_LAYER_POINT, data, units)
        symbolObj = mapscript.symbolObj('circle')
        symbolObj.type = mapscript.MS_SYMBOL_ELLIPSE
        index = map_obj.symbolset.appendSymbol(symbolObj)


    layer_obj.setProjection("AUTO")

    class_obj = mapscript.classObj(layer_obj)
    class_obj.setExpression("([ID] in ('" + ids.replace('"', "") + "'))")
    style_obj = mapscript.styleObj(class_obj)
    style_obj.outlinecolor.setRGB(255, 0, 128)  # ff0080 magenta
    style_obj.width = 2
    if 'POINT' in layer_type :
        style_obj.symbol = index

    return None

def shapefile_Cluster_selection_layer(map_obj, data, units, max_extent_xy, clusterItem, show_outline_on_selection, simplificationRequired, simplificationMethod, layer_type, selectOnlyActiveClass, classItem, classVisible, show_outline_on_NotActiveClass ):

    if 'POINT' not in layer_type :
        layer_obj = init_layer(map_obj, "Selection2", mapscript.MS_LAYER_POLYGON, data, units)
        if simplificationRequired == 'true':
            if simplificationMethod == 'generalize':
                if max_extent_xy >= 100000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*3 ))")				# 70s as with none
                elif max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*2 ))")				# 70s as with none
                elif max_extent_xy >= 25000:
                    layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*1.5 ))")				# 70s as with none
            else:
                if max_extent_xy >= 50000:
                    layer_obj.setGeomTransform("( smoothsia([shape],3,1))")				# 70s as with none
    else:
        layer_obj = init_layer(map_obj, "Selection2", mapscript.MS_LAYER_POINT, data, units)
        symbolObj = mapscript.symbolObj('circle')
        symbolObj.type = mapscript.MS_SYMBOL_ELLIPSE
        index = map_obj.symbolset.appendSymbol(symbolObj)



    layer_obj.setProjection("AUTO")


    #selectOnlyActiveClass = 'true'
    # select only poly with selected attribute and with active class
    if selectOnlyActiveClass == 'true':
        #  in case of selection on deactivated classes replace classVisible with Non active class list
        if show_outline_on_NotActiveClass[0] != '':
            classVisible = show_outline_on_NotActiveClass
        class_obj = mapscript.classObj(layer_obj)
        #class_i_obj.setExpression("([T1_class] = 1 and [T1_cluster] = 9)") # VA
        class_obj.setExpression("([{0}] in ('{1}') and [{2}] = {3})".format(classItem, ','.join([str(elem) for elem in classVisible]),clusterItem,show_outline_on_selection))
        style_obj = mapscript.styleObj(class_obj)

    else:
        layer_obj.classitem = clusterItem
        class_obj = mapscript.classObj(layer_obj)
        class_obj.setExpression(show_outline_on_selection)
        style_obj = mapscript.styleObj(class_obj)

    style_obj.outlinecolor.setRGB(255, 0, 128)
    style_obj.width = 2


    if 'POINT' in layer_type :
        style_obj.symbol = index

    return None



def run(data_path, extent, map_size, map_size_x, map_size_y, color, outline_size,LAYER, ids, classItem, units, classIDs, classColors, classVisible, clusterItem, show_outline_on_selection, selectOnlyActiveClass, simplificationRequired, simplificationMethod, layer_type, show_outline_on_NotActiveClass):

    rgb = Colors.hex2rgb(color)

    # Create map object
    mapObj = Mapserver.init_map(debug=False, debug_level=5)
    mapObj.unit = mapscript.MS_METERS
    # mapObj.setProjection("init=epsg:3857")
    mapObj.setProjection("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs")

    mapObj.setExtent(float(extent[0]), float(extent[1]), float(extent[2]), float(extent[3]))
    mapObj.setSize(int(map_size[0]), int(map_size[1]))
    Mapserver.png_format(mapObj)

    max_extent_xy = Mapserver.max_extent_xy(extent)

    # Skeleton Layer
    if LAYER == 'Skeleton':
        skeleton_layer(mapObj, data_path, units, max_extent_xy, rgb, outline_size, simplificationRequired,
                       simplificationMethod, layer_type)

    # Shapefile Layer
    if LAYER == 'MySHP':

        shapefile_layer(mapObj, data_path, units, max_extent_xy, classIDs, classItem, classColors,
                        simplificationRequired, simplificationMethod, classVisible, layer_type)
        if ids != '':
            shapefile_IDS_selection_layer(mapObj, data_path, units, max_extent_xy, ids, simplificationRequired,
                                          simplificationMethod, layer_type)

        if show_outline_on_selection != '---':
            shapefile_Cluster_selection_layer(mapObj, data_path, units, max_extent_xy, clusterItem,
                                              show_outline_on_selection, simplificationRequired, simplificationMethod,
                                              layer_type, selectOnlyActiveClass, classItem, classVisible, show_outline_on_NotActiveClass)
    #Mapserver.save_mapfile(mapObj)
    res = Mapserver.map_to_http(mapObj)

    if sys.platform != "linux":
        mapscript.msCleanup()

    return res

# if __name__ == '__main__':
#
#     try:
#
#         # retrieve parameters from request
#         params = cgi.FieldStorage()
#         data_path = params.getvalue("full_path")
#         extent = params.getvalue("mapext", "0 0 0 0").split()
#         map_size = params.getvalue("map_size", "512 512").split()
#         map_size_x = map_size[0]
#         map_size_y = map_size[1]
#         rgb = Colors.hex2rgb(params.getvalue("color", "#FF0000"))
#         outline_size = float(params.getvalue("outline_size", '0.2'))
#         LAYER = params.getvalue("layers", '')
#
#         ids = params.getvalue("ids", '')
#         classItem = params.getvalue("classItem", '')
#
#         units = params.getvalue("units", 'meters')
#         classIDs = params.getvalue("classIDs", '').split(";")
#         classColors = params.getvalue("classColors", '').split(";")
#         classVisible = params.getvalue("classVisible", '').split(";")
#
#         clusterItem = params.getvalue("clusterItem", '')
#         show_outline_on_selection = params.getvalue("show_outline_on_selection", '---')
#         selectOnlyActiveClass = params.getvalue("selectOnlyActiveClass", 'false')
#
#         unset_projection = params.getvalue("unset_projection", 'false')
#         simplificationRequired = params.getvalue("simplificationRequired", 'true')
#         simplificationMethod = params.getvalue("simplificationMethod", 'generalize')
#
#         # IF POINT then draw using Symbols
#         layer_type = params.getvalue("layer_type", '')
#
#         # Create map object
#         mapObj = Mapserver.init_map()
#         mapObj.unit = mapscript.MS_METERS
#         mapObj.setProjection("init=epsg:3857")
#         mapObj.setExtent(float(extent[0]), float(extent[1]), float(extent[2]), float(extent[3]))
#         mapObj.setSize(int(map_size[0]), int(map_size[1]))
#         Mapserver.png_format(mapObj)
#
#         max_extent_xy = Mapserver.max_extent_xy(extent)
#
#         # Skeleton Layer
#         if LAYER == 'Skeleton':
#             skeleton_layer(mapObj, data_path, units, max_extent_xy, rgb, outline_size, simplificationRequired, simplificationMethod, layer_type)
#
#         # Shapefile Layer
#         if LAYER == 'MySHP':
#
#             shapefile_layer(mapObj, data_path, units, max_extent_xy, unset_projection, classIDs, classItem, classColors, simplificationRequired, simplificationMethod, classVisible, layer_type)
#             if ids != '':
#                 shapefile_IDS_selection_layer(mapObj, data_path, units, max_extent_xy, ids, simplificationRequired, simplificationMethod, layer_type)
#
#             if show_outline_on_selection != '---':
#                 shapefile_Cluster_selection_layer(mapObj, data_path, units, max_extent_xy, clusterItem, show_outline_on_selection, simplificationRequired, simplificationMethod, layer_type, selectOnlyActiveClass, classItem, classVisible)
#
#
#         #Mapserver.save_mapfile(mapObj,'last.map')
#         Mapserver.map_to_http(mapObj)
#
#     except Exception as e:
#         print("Content-type: text/plain;")
#         print()
#         print(e)
#         sys.exit()
