#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import mapscript
# import cgitb
# cgitb.enable()
#
# import cgi

# Import local libraries
import Mapserver

def raster_layer(map_obj, data_path, extent, apply_data_range, bands, scale1, scale2, scale3, has_colorTable, legendType, shp):


    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = "IMG"
    layer_obj.data = data_path
    layer_obj.type = mapscript.MS_LAYER_RASTER
    layer_obj.status = mapscript.MS_ON
    layer_obj.classitem = "[pixel]"

    if shp != "":
        max_extent_xy = Mapserver.max_extent_xy(extent)
        if max_extent_xy >= 50000:
            layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*1.5 ))")
    else:
        layer_obj.setProjection("AUTO")

    if apply_data_range[0] == 'true':
        class_obj = mapscript.classObj(layer_obj)
        new_style = mapscript.styleObj()
        class_obj.insertStyle(new_style)
        class_obj.setExpression("([pixel] >= " + str(apply_data_range[1]) + " and [pixel] <= " + str(apply_data_range[2]) + ")")

    elif legendType not in ['', 'none']:
        import Settings
        SETTINGS = Settings.load()

        for item in SETTINGS['map_legends']['raster']:
            if item['type'] == legendType:
                for cl in item['classes']:
                    class_obj = mapscript.classObj(layer_obj)
                    new_style = mapscript.styleObj()
                    if float(cl[0]) == float(cl[1]):
                        mincol = cl[3].split(',')
                        class_obj.setExpression(cl[0])
                        new_style.color = mapscript.colorObj(int(mincol[0]), int(mincol[1]), int(mincol[2]))
                    else:
                        class_obj.setExpression("([pixel] >= " + str(cl[0]) + " and [pixel] < " + str(cl[1]) + ")")
                        new_style.rangeitem = "[pixel]"
                        mincol = cl[3].split(',')
                        maxcol = cl[4].split(',')
                        new_style.mincolor = mapscript.colorObj(int(mincol[0]), int(mincol[1]), int(mincol[2]))
                        new_style.maxcolor = mapscript.colorObj(int(maxcol[0]), int(maxcol[1]), int(maxcol[2]))
                        new_style.minvalue = float(cl[0])
                        new_style.maxvalue = float(cl[1])

                    class_obj.insertStyle(new_style)
                    new_style = None

                    #layer_obj.setProcessing("NODATA=ON")
                    #layer_obj.setProcessing("COLOR_MATCH_THRESHOLD=3")
                    #layer_obj.setProcessing("DITHER=YES")
                break




    elif bands != "" and (has_colorTable == 0 or (has_colorTable == 1 and legendType == 'none')):

        layer_obj.offsite = mapscript.colorObj(0, 0, 0)
        layer_obj.clearProcessing()
        layer_obj.setProcessing("BANDS=" + bands)

        scale1_1 = float(scale1.split(",")[0])
        scale1_1 -= 1/50. #scale1_1 / 125.
        scale1_2 = float(scale1.split(",")[1])
        scale1_2 += 1/50. #scale1_2 / 125.
        scale2_1 = float(scale2.split(",")[0])
        scale2_1 -= 1/50. #scale2_1 / 125.
        scale2_2 = float(scale2.split(",")[1])
        scale2_2 += 1/50. #scale2_2 / 125.
        scale3_1 = float(scale3.split(",")[0])
        scale3_1 -= 1/50. #scale3_1 / 125.
        scale3_2 = float(scale3.split(",")[1])
        scale3_2 += 1/50. #scale3_2 / 125.
        layer_obj.setProcessing("SCALE_1="+str(scale1_1)+","+str(scale1_2))
        layer_obj.setProcessing("SCALE_2="+str(scale2_1)+","+str(scale2_2))
        layer_obj.setProcessing("SCALE_3="+str(scale3_1)+","+str(scale3_2))

        #layer_obj.setProcessing("NODATA=OFF")

    #layer_obj.setProcessing("COLOR_MATCH_THRESHOLD=3")
    #layer_obj.setProcessing("DITHER=YES")
    #layer_obj.setProcessing("NODATA={}"format(float(-1.6999999999999999e+308)))
    return layer_obj


def raster_preview_layer(map_obj, data_path, extent, band, apply_data_range):
    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = "preview"
    layer_obj.data = data_path
    layer_obj.type = mapscript.MS_LAYER_RASTER
    layer_obj.status = mapscript.MS_ON

    layer_obj.setProjection("AUTO")
    layer_obj.classitem = "[pixel]"
    layer_obj.clearProcessing()
    layer_obj.setProcessing("BANDS="+str(band))
    layer_obj.offsite = mapscript.colorObj(0, 0, 0)
    class_obj = mapscript.classObj(layer_obj)
    class_obj.setExpression(apply_data_range[1])

    new_style = mapscript.styleObj()
    # new_style.color = mapscript.colorObj(179, 198, 255)
    # new_style.color = mapscript.colorObj(0, 0, 0)
    if str(apply_data_range[1]) == str(apply_data_range[2]):
        class_obj.setExpression(apply_data_range[1])
        new_style.color = mapscript.colorObj(230, 138, 1)
    else:
        class_obj.setExpression(
            "([pixel] >= " + str(apply_data_range[1]) + " and [pixel] <= " + str(apply_data_range[2]) + ")")
        new_style.rangeitem = "[pixel]"
        new_style.maxcolor = mapscript.colorObj(230, 138, 1)
        new_style.mincolor = mapscript.colorObj(255, 214, 153)
        new_style.minvalue = float(apply_data_range[1])
        new_style.maxvalue = float(apply_data_range[2])
    class_obj.insertStyle(new_style)
    return layer_obj


def skeleton_layer(map_obj):
    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = "Skeleton"
    layer_obj.type = mapscript.MS_LAYER_POLYGON
    layer_obj.status = mapscript.MS_ON
    # set class + style
    class_obj = mapscript.classObj(layer_obj)
    class_obj.name = 'ON'
    style_obj = mapscript.styleObj(class_obj)
    style_obj.outlinecolor = mapscript.colorObj(40, 40, 100)
    style_obj.width = 0.2
    return layer_obj

def run(layers, data_path, shp, extent, mytype, map_size, bands, scale1, scale2, scale3, has_colorTable, apply_data_range, legendType):
    # Create map object
    mapObj = Mapserver.init_map(debug=False, debug_level=5)
    mapObj.unit = mapscript.MS_METERS
    # mapObj.setProjection("init=epsg:3857")
    mapObj.setProjection("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs")
    mapObj.setExtent(float(extent[0]), float(extent[1]), float(extent[2]), float(extent[3]))
    mapObj.setSize(int(map_size[0]), int(map_size[1]))
    Mapserver.png_format(mapObj)

    # Raster image layer
    if "IMG" in layers:
        raster_layer(mapObj, data_path, extent, apply_data_range, bands, scale1, scale2, scale3, has_colorTable,
                     legendType, shp)

    if "EditingRasterPreview" in layers:
        raster_preview_layer(mapObj, data_path, extent, bands, apply_data_range)

    # Skeleton layer
    if shp != "":
        skeleton_layer(mapObj)

    res = Mapserver.map_to_http(mapObj)


    if sys.platform != "linux":
        mapscript.msCleanup()

    return res
#
# if __name__ == '__main__':
#     try:
#         # retrieve parameters from request
#         params = cgi.FieldStorage()
#         layers = params.getvalue("layers", '').split()
#         data_path = params.getvalue("full_path")
#         shp = params.getvalue("shp", "")
#         extent = params.getvalue("mapext", "0 0 0 0").split()
#         type = params.getvalue("type", "")
#         map_size = params.getvalue("map_size", "512 512").split()
#         bands = params.getvalue("bands", '')
#         scale1 = params.getvalue("scale1", '0,255')
#         scale2 = params.getvalue("scale2", '0,255')
#         scale3 = params.getvalue("scale3", '0,255')
#         has_colorTable = int(params.getvalue("has_colorTable", '0'))
#         apply_data_range = params.getvalue("apply_data_range", "false").split()
#         legendType = params.getvalue("legendType", "")
#
#         # Create map object
#         mapObj = Mapserver.init_map()
#         mapObj.unit = mapscript.MS_METERS
#         mapObj.setProjection("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs")
#         mapObj.setExtent(float(extent[0]), float(extent[1]), float(extent[2]), float(extent[3]))
#         mapObj.setSize(int(map_size[0]), int(map_size[1]))
#         Mapserver.png_format(mapObj)
#         # Raster image layer
#         if "IMG" in layers:
#             raster_layer(mapObj, data_path, extent, apply_data_range, bands, scale1, scale2, scale3, has_colorTable, legendType, shp)
#
#         if "EditingRasterPreview" in layers:
#             raster_preview_layer(mapObj, data_path, extent, bands, apply_data_range)
#
#         # Skeleton layer
#         if shp != "":
#             skeleton_layer(mapObj)
#
#         Mapserver.map_to_http(mapObj)
#
#     except Exception as e:
#         print("Content-type: text/plain;")
#         print()
#         print('----- main------')
#         print(e)
#         print
#         sys.exit()
