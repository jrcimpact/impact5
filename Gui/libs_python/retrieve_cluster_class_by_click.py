#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json

try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr


#if __name__ == '__main__':
def run(params):

    #print "Content-Type: text/html\n"

    # retrieve parameters from request #
    #params = cgi.FieldStorage()

    shapefileName = params.get("shp", b'').decode()

    wkt = params.get("wkt", b'').decode()
    items = params.get("items", None).decode()
    geom = params.get("geom", b'False').decode()
    EPSG = params.get("EPSG", None)

    if EPSG is not None:
        EPSG = EPSG.decode()

    selectOnlyActiveClass = params.get("selectOnlyActiveClass", b'False').decode()

    selectOnlyActiveClass_Layer = params.get("selectOnlyActiveClass_Layer", b'').decode()
    activeClasses = params.get("activeClasses", b'').decode()

    if activeClasses != '':
        activeClasses = activeClasses.split(';')

    if selectOnlyActiveClass in ['true', True, ]:
        selectOnlyActiveClass = True
    else:
        selectOnlyActiveClass = False


    if items is not None:
        items = json.loads(items)
    # Open SHP file
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataSource = driver.Open(shapefileName, 0)
    layer = dataSource.GetLayer()

    # Get type of vector: if point add buffer
    feature = layer.GetNextFeature()
    geometry = feature.GetGeometryRef()
    vtype = (geometry.GetGeometryName()).upper()


    # Read projection and set coordinates transformations
    outSpatialRef = layer.GetSpatialRef()
    if EPSG is not None:
        inSpatialRef = osr.SpatialReference()
        inSpatialRef.ImportFromEPSG(int(EPSG))
    else:
        inSpatialRef = outSpatialRef

    inSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    outSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
    inverseCoordTransform = osr.CoordinateTransformation(outSpatialRef, inSpatialRef)


    poly = ogr.CreateGeometryFromWkt(wkt)
    # buffer in meters as Lat Lon are in mercator
    if vtype == 'POINT' and "POINT" in wkt:
        poly = poly.Buffer(100)

    # return geom in file proj
    poly.Transform(coordTransform)
    # Apply spatial filter
    layer.SetSpatialFilter(poly)

    result = list()
    # if no active classes (positive or negative selection )
    if selectOnlyActiveClass and activeClasses == '':
        return json.dumps(result)

    if selectOnlyActiveClass:

        # apply filter on attribute X having class in list of active ID
        filter = selectOnlyActiveClass_Layer + " in ("
        for id in activeClasses:
            filter += "'"+str(id)+"',"
        filter = filter[:-1]+")"   

        layer.SetAttributeFilter(filter)




    try:
        # Iterate on features
        feature = layer.GetNextFeature()
        i = 0
        while feature:
            result.append(dict())
            for item in items:
                field_value = feature.GetFieldAsString(str(item)) 
                if field_value is not None:
                    result[i][item] = field_value
                else:
                    result[i][item] = ''
            if geom:
                feature_geometry = feature.GetGeometryRef()
                feature_geometry.Transform(inverseCoordTransform)
                result[i]['geom'] = str(feature_geometry.ExportToWkt())
            feature = layer.GetNextFeature()
            i += 1

        return json.dumps(result)

    except Exception as e:
        print(str(e))

