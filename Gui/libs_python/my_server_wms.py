# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.
import os
import sys
import json
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules'))

os.environ["PYTHONPATH"] += os.pathsep + os.path.dirname(os.path.realpath(__file__))
os.environ["PYTHONPATH"] += os.pathsep + os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules')

os.environ["PATH"] += os.pathsep + os.path.dirname(os.path.realpath(__file__))
os.environ["PATH"] += os.pathsep + os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules')



from __config__ import *
#import Settings
#import IMPACT
import mapscript_shp_fn_tornado
import mapscript_img_fn_tornado

# Import global libraries
from tornado import ioloop, web, websocket, httpserver
#from tornado.httpclient import AsyncHTTPClient, HTTPResponse, HTTPRequest


async def resolve_img(layers, data_path, shp, extent, mytype, map_size, bands, scale1, scale2,
                                              scale3, has_colorTable, apply_data_range, legendType):
    #import mapscript_img_fn_tornado
    val = mapscript_img_fn_tornado.run(layers, data_path, shp, extent, mytype, map_size, bands, scale1, scale2,
                                              scale3, has_colorTable, apply_data_range, legendType)
    #del mapscript_img_fn_tornado

    return val

class load_mapserver_img_response(web.RequestHandler):
    def check_origin(self, origin):
        return True

    async def get(self):
        layers = self.get_argument("layers", '')
        data_path = self.get_argument("full_path", 'empty path')
        shp = self.get_argument("shp", "")
        extent = self.get_argument("BBOX", "0,0,0,0").split(',')
        mytype = self.get_argument("type", "")
        map_size = [self.get_argument("WIDTH", "512"),self.get_argument("HEIGHT", "512")]
        bands = str(self.get_argument("bands", '', True))
        scale1 = str(self.get_argument("scale1", '0,255'))
        scale2 = str(self.get_argument("scale2", '0,255'))
        scale3 = str(self.get_argument("scale3", '0,255'))
        has_colorTable = int(self.get_argument("has_colorTable", '0'))
        apply_data_range = self.get_argument("apply_data_range", "false").split(',')
        legendType = self.get_argument("legendType", "")

        response = await resolve_img(layers, data_path, shp, extent, mytype, map_size, bands, scale1, scale2,
                                              scale3, has_colorTable, apply_data_range, legendType)

        if isinstance(response, bytes):
            self.set_header("Content-type", "image/png")
            self.write(response)

async def resolve_shp(data_path, extent, map_size, map_size_x, map_size_y, color, outline_size,
                                 LAYER, ids, classItem, units,
                                 classIDs, classColors, classVisible, clusterItem,
                                 show_outline_on_selection, selectOnlyActiveClass,
                                 simplificationRequired, simplificationMethod, layer_type, show_outline_on_NotActiveClass):
    #import mapscript_shp_fn_tornado
    val = mapscript_shp_fn_tornado.run(data_path, extent, map_size, map_size_x, map_size_y, color, outline_size,
                                 LAYER, ids, classItem, units,
                                 classIDs, classColors, classVisible, clusterItem,
                                 show_outline_on_selection, selectOnlyActiveClass,
                                 simplificationRequired, simplificationMethod, layer_type, show_outline_on_NotActiveClass)
    #del mapscript_shp_fn_tornado
    return val

class load_mapserver_shp_response(web.RequestHandler):
    def check_origin(self, origin):
        return True

    async def get(self):

        legendType = self.get_argument("legendType", "")
        data_path = self.get_argument("full_path")
        extent = self.get_argument("BBOX", "0,0,0,0").split(',')

        map_size = [self.get_argument("WIDTH", "512"),self.get_argument("HEIGHT", "512")]
        map_size_x = map_size[0]
        map_size_y = map_size[1]
        color = self.get_argument("color", "#FF0000")
        outline_size = float(self.get_argument("outline_size", '0.2'))
        LAYER = self.get_argument("layers", '')

        ids = self.get_argument("ids", '')
        classItem = self.get_argument("classItem", '')

        units = self.get_argument("units", 'meters')
        classIDs = self.get_argument("classIDs", '').split(";")
        classColors = self.get_argument("classColors", '').split(";")
        classVisible = self.get_argument("classVisible", '').split(";")

        clusterItem = self.get_argument("clusterItem", '')
        show_outline_on_selection = self.get_argument("show_outline_on_selection", '---')
        selectOnlyActiveClass = self.get_argument("selectOnlyActiveClass", 'false')
        show_outline_on_NotActiveClass = self.get_argument("show_outline_on_NotActiveClass", '').split(";")

        # unset_projection = self.get_argument("unset_projection", 'false')
        simplificationRequired = self.get_argument("simplificationRequired", 'true')
        simplificationMethod = self.get_argument("simplificationMethod", 'generalize')

        # IF POINT then draw using Symbols
        layer_type = self.get_argument("layer_type", '')
        response = await resolve_shp(data_path, extent, map_size, map_size_x, map_size_y, color, outline_size,
                                              LAYER, ids, classItem, units,
                                              classIDs, classColors, classVisible, clusterItem,
                                              show_outline_on_selection, selectOnlyActiveClass,
                                              simplificationRequired, simplificationMethod, layer_type, show_outline_on_NotActiveClass)

        if isinstance(response, bytes):
            self.set_header("Content-type", "image/png")
            self.write(response)

        self.finish()
        

class mapserverMainHandler(web.RequestHandler):
    def get(self):

        try:
            self.set_header("Content-type", "text/html")
            self.write("NGINx redirecting to mapscript ")

        except Exception as e:
            print("ERROR MAPSERVER " + str(e))
        self.finish()


async def resolve_getGeeThumbUrl(params):
    import getGeeThumbUrl
    return getGeeThumbUrl.run(params)

class getGeeThumbUrl_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    async def get(self):
        try:

            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = await resolve_getGeeThumbUrl(params)
            # print result
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Content-type", "text/html")
            #self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR on GetGeeThumb")
            print(str(e))


class getGeeDateList_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            import getGeeDateList
            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = json.dumps(getGeeDateList.run(params))
            # print result
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Content-type", "text/html")
            #self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Content-type", "text/html")
            self.write(json.dumps('Authentication_expired'))
            self.finish()


if __name__ == "__main__":

    # async def asynchronous_fetch(url):
    #     http_client = AsyncHTTPClient()
    #     url = "http://127.0.0.1:9999/mapscript_img_fn_tornado.py"
    #     response = await http_client.fetch(url)
    #     return response.body
    me, URL, PORT = sys.argv

    application_mapserver = web.Application([
        (r"/", mapserverMainHandler),

        (r'/mapscript_shp_fn_tornado.py', load_mapserver_shp_response),
        (r'/mapscript_img_fn_tornado.py', load_mapserver_img_response),

        (r'/getGeeThumbUrl.py', getGeeThumbUrl_Handler),  # fixed
        (r'/getGeeDateList.py', getGeeDateList_Handler),  # fixed

    ], debug=True, autoreload=True
    )

    application_mapserver.listen(PORT)



    try:
        import tornado
        #import tornado.autoreload
        #print('..... loading tornado .... ')
        #tornado.autoreload.start()
        #tornado.autoreload.watch(os.path.realpath(__file__))
        tornado.ioloop.IOLoop.instance().start()
        # https: // towardsdatascience.com / stop - waiting - start - using - async - and -await - 18
        # fcd1c28fd0
    except Exception as e:
        print('iloop error')
        print(e)
        # print('\rClosing... (Interrupted by keyboard)')
