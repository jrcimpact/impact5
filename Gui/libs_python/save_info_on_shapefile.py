#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json
import time
import shutil
# Import local libraries
import ImageStore
import os

from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr


def run(params):
    startTime = time.time()

    # retrieve parameters from request #
    #params = cgi.FieldStorage()
    shp_name = params.get("shp_name").decode()
    extent = params.get("extent").decode()

    saveValue = params.get("saveValue").decode()
    saveTo = params.get("saveTo", b'')  .decode()                # recode_to_item is used when selection and filter is on cluster --PA

    whereItem = params.get("whereItem").decode()                         # column in DBF
    whereValue = params.get("whereValue").decode()

    fid = params.get("fid").decode()                            # name of the ID column
    list_ids = params.get("list_ids", b'[]').decode()           # user manual poly selection IDs

    # print("loading time: " + str(time.time() - startTime))

    selectOnlyActiveClass = params.get("selectOnlyActiveClass", b'False').decode()
    selectOnlyActiveClass_Layer = params.get("selectOnlyActiveClass_Layer", b'').decode()
    activeClasses = params.get("activeClasses", b'').decode()
    #activeClasses = activeClasses.split(";")
    activeClassesArray=[]
    if activeClasses != '':
        activeClasses = activeClasses.split(';')
        # for ac in activeClasses:
        #     activeClassesArray.append("'{}'" % (ac))

    if selectOnlyActiveClass in ['true',True,1]:
        selectOnlyActiveClass = True
    else:
        selectOnlyActiveClass = False


    extent = json.loads(extent)

    list_ids = json.loads(list_ids)

    bkupID = params.get("backupID", b'').decode()
    bkupIsActive = params.get("bkupIsActive", b'False').decode()
    if bkupIsActive in ['True','true',True,1]:
        bkupIsActive = True
    else:
        bkupIsActive = False
    # -----------------------------------------------------
    # ---------------  INIT SHAPEFILE ---------------------
    # -----------------------------------------------------

    try:
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_validation")
        if not os.path.exists(tmp_indir):
            os.makedirs(tmp_indir)
        if bkupID != '':

            dbf = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp','_'+bkupID+'.dbf'))
            shutil.copy(shp_name.replace('.shp','.dbf'), dbf)



        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(shp_name, 1)
        layer = dataSource.GetLayer(0)
        layerName = str(layer.GetName())

        layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom

        if len(extent) > 1:
            layer.SetSpatialFilterRect(extent[0], extent[1], extent[2], extent[3])  # new in OL10  [minX, minY, maxX, maxY]

        # ------------------------------------------
        # ----------  RECODE LIST of IDs  ----------
        # ------------------------------------------
        ids_len = len(list_ids)
        if ids_len > 0:
            id_time = time.time()

            where_ids = ','.join(map(lambda x: "'"+str(x)+"'", list_ids))
            sql = 'UPDATE "%s" SET "%s" = \'%s\' WHERE "%s" IN (%s);' % (layerName, saveTo, saveValue, fid, where_ids)
            #print(sql)
            dataSource.ExecuteSQL(sql, dialect='SQLITE')
            #print("SQL time ID: " + str(time.time() - id_time))

        # -------------------------------------------
        # ----------  RECODE ENTIRE CLASS  ----------
        # -------------------------------------------
        elif whereItem != '' and whereValue != None:

            sql = ' UPDATE \"' + layerName + '\" SET \"' + saveTo + '\" = \"' + saveValue + '\" '
            sql += ' WHERE \"' + whereItem + '\" = \"' + whereValue + '\"'

            if selectOnlyActiveClass:
                # apply filter on attribute X having class in list of active ID
                where_classes = ','.join(map(lambda x: "'" + str(x) + "'", activeClasses))
                sql += ' AND "%s" IN (%s)' % (selectOnlyActiveClass_Layer, where_classes)

            id_time = time.time()
            dataSource.ExecuteSQL(sql, dialect='SQLITE')
           # print("SQL time Item: " + str(time.time() - id_time))

        if(os.path.getsize(shp_name)/1000000) > 10:
            print('Rebuilding spatial index')
            dataSource.ExecuteSQL('CREATE SPATIAL INDEX ON ' + layerName)


        dataSource.Destroy()
        dataSource = None
        layer = None


        #print('Updating store')
        ImageStore.update_file_info(shp_name)
        #print("SQL time tot: " + str(time.time() - startTime))

        # # Delete temporary files
        # if not bkupIsActive:
        #     print("DEL BKUP")
        #     for root, dirs, files in os.walk(tmp_indir, topdown=False):
        #         for name in files:
        #             try:
        #                 os.remove(os.path.join(root, name))
        #             except Exception as e:
        #                 pass

        for f in os.listdir(tmp_indir):
            if os.stat(os.path.join(tmp_indir, f)).st_mtime < time.time() - 1 * 86400:
                try:
                        os.remove(os.path.join(tmp_indir, f))
                except Exception as e:
                    pass

        return '1'

    except Exception as e:
        print(str(e))
        return '-1'

# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     print main()
