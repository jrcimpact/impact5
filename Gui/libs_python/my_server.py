# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules'))

os.environ["PYTHONPATH"] += os.pathsep + os.path.dirname(os.path.realpath(__file__))
os.environ["PYTHONPATH"] += os.pathsep + os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules')

os.environ["PATH"] += os.pathsep + os.path.dirname(os.path.realpath(__file__))
os.environ["PATH"] += os.pathsep + os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules')



from __config__ import *
import Settings
import IMPACT
import GitRepository
import ImageStore
import CSImageStore
import ImageSettingsStore
import LogStore
import json
# import mapscript_img_fn_tornado
# import mapscript_shp_fn_tornado
import save_user_settings
import layer_settings_manager
import add_missing_fields_to_shp
import copyDBFfields
import fileHandler
import get_files
import updateDBFrecord
import save_user_feature_to_shapefile
import save_info_on_shapefile
import getHtmlFile
import get_raster_stats_by_band
import get_user_data_list
import get_vector_DBF_in_Json
import get_vector_stats_by_layer
import getImageFile
import goToPrevNextFeature
import geotiff_to_KML_wms
import python_tool_launcher
import query_file
import retrieve_cluster_class_by_click
import save_info_on_shapefile_QAssessment
import dissolve_features
import getESAquicklooks
import get_attribute_class

# Import global libraries
from tornado import ioloop, web, websocket, httpserver


class RootHandler(web.RequestHandler):
    def get(self):
        try:
            self.write("Entry test page - ")
            self.write("Launch <a target=\"_blank\" href=\""+GLOBALS['url']+"/IMPACT\">IMPACT</a></p>")
        except Exception as e:
            print("ERROR RootHandler " + str(e))
        self.finish()


class MainHandler(web.RequestHandler):
    def get(self):
        try:
            # sys.path.insert(0, os.path.join(os.path.dirname(__file__),'Gui','IMPACT'))
            # import index
            # self.write(index.get_impact_indexPage())

            # Initialize database - if lots of new data on load it takes too much
            # ImageStore and CSImageStore will be created once client sends ready via socket
            #ImageStore.populate()
            #CSImageStore.populate()

            # ImageSettingsStore takes no time, can be created on init
            ImageSettingsStore.create()
            tmpSettings = Settings.load()
            content = '''
            <!DOCTYPE HTML>
                <html manifest="">
                <head>
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes">

                    <title>IMPACT Toolbox</title>

                    <!-- The line below must be kept intact for Sencha Cmd to build your application -->
                    <script id="microloader" data-app="e06f4518-c52e-48c7-8d10-d8aff09d2eac" type="text/javascript" src="bootstrap.js"></script>

                </head>
                <body>Press F5 if page is not successfully loaded </body>
                
            </html>
            '''

            self.write(content)
        except Exception as e:
            print("ERROR 1 MainHandler " + str(e))
            self.write(str(e))
        self.finish()


class getGlobalsHandler(web.RequestHandler):
    def get(self):
        try:
            GLOBALS['online'] = str(IMPACT.is_online())
            self.write(json.dumps(GLOBALS))
        except Exception as e:
            print("ERROR getGlobalsHandler " + str(e))
            self.write(json.dumps(GLOBALS))
        self.finish()


class getSettingsHandler(web.RequestHandler):
    def get(self):
        try:
            Settings.upgrade()
            self.write(Settings.load_as_json())
        except Exception as e:
            print("ERROR getSettingsHandler " + str(e))
            self.write(Settings.load_as_json())
        self.finish()

class LogStore_SocketHandler(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def return_LogStore(self):
        data = dict()
        try:
            if LogStore.updated_recently():
                data['status'] = 'ok'
                data['logs'] = LogStore.get_all()
                self.write_message(json.dumps(data))
        except:
            pass

    def open(self):
        self.pCallback = ioloop.PeriodicCallback(self.return_LogStore, callback_time=2000)
        self.pCallback.start()

    def on_message(self, message):
        pass
        #
        # data = dict()
        # data['status'] = 'ok'
        # data['logs'] = LogStore.get_all()
        # self.write_message(json.dumps(data))


class ImageStore_SocketHandler(websocket.WebSocketHandler):
    loop_counter = 0
    first_run = True

    def check_origin(self, origin):
        return True

    def return_ImageStore(self):
        data = dict()

        try:
            if ImageStore_SocketHandler.first_run:
                ImageStore.reset_pending_files()

            if ImageStore.updated_recently() and not ImageStore_SocketHandler.first_run:
                data['status'] = 'ok'
                data['image_db'] = ImageStore.get_all()
                self.write_message(json.dumps(data))

            ImageStore_SocketHandler.first_run = False

            ImageStore_SocketHandler.loop_counter += 1
            if ImageStore_SocketHandler.loop_counter == 2:
                ImageStore.reset_pending_files()
            if ImageStore_SocketHandler.loop_counter == 4:
                ImageStore_SocketHandler.loop_counter = 0
                try:
                    # Populate database with general information

                    ImageStore.populate()
                    # Calculate statistics in multiprocessing
                    ImageStore.calculate_statistics()  # process is async
                    # all done

                except Exception as e:
                    print("ERROR return_ImageStore " + str(e))
                    data['status'] = 'error'
                    data['message'] = 'Error while scanning file system'
                    self.write_message(json.dumps(data))
        except Exception as e:
            # print "ERROR 0 " + str(e)
            pass

    def open(self):
        self.pCallback = ioloop.PeriodicCallback(self.return_ImageStore, callback_time=5000)
        self.pCallback.start()

    def on_message(self, message):
        data = dict()
        data['status'] = 'ok'
        data['image_db'] = ImageStore.get_all()
        self.write_message(json.dumps(data))

        # ImageStore.populate()
        # Calculate statistics in multiprocessing
        # ImageStore.calculate_statistics()


class CSImageStore_SocketHandler(websocket.WebSocketHandler):
    loop_counter = 0
    first_run = True

    def check_origin(self, origin):
        return True

    def return_ImageStore(self):
        data = dict()

        try:
            if CSImageStore_SocketHandler.first_run:
                CSImageStore.reset_pending_files()

            if CSImageStore.updated_recently(wait=1200) and not CSImageStore_SocketHandler.first_run:
                data['status'] = 'ok'
                data['image_db'] = CSImageStore.get_all()
                self.write_message(json.dumps(data))

            CSImageStore_SocketHandler.first_run = False

            CSImageStore_SocketHandler.loop_counter += 1
            if CSImageStore_SocketHandler.loop_counter == 2:
                CSImageStore.reset_pending_files()
            if CSImageStore_SocketHandler.loop_counter == 4:
                CSImageStore_SocketHandler.loop_counter = 0
                try:
                    # Populate database with general information

                    CSImageStore.populate()
                    # Calculate statistics in multiprocessing
                    CSImageStore.calculate_statistics()  # process is async
                    # all done

                except Exception as e:
                    print("ERROR return_CSImageStore " + str(e))
                    data['status'] = 'error'
                    data['message'] = 'Error while scanning CS file system'
                    self.write_message(json.dumps(data))
        except Exception as e:
            # print "ERROR 0 " + str(e)
            pass

    def open(self):
        self.pCallback = ioloop.PeriodicCallback(self.return_ImageStore, callback_time=300000)
        self.pCallback.start()

    def on_message(self, message):
        data = dict()
        data['status'] = 'ok'
        data['image_db'] = CSImageStore.get_all()
        self.write_message(json.dumps(data))

        # CSImageStore.populate()
        # Calculate statistics in multiprocessing
        #CSImageStore.calculate_statistics()



class UpdateHandler(web.RequestHandler):
    def get(self):

        try:
            self.write(GitRepository.get_status())
        except Exception as e:
            print("ERROR on Update " + str(e))
        self.finish()


class layer_settings_manager_Handler(web.RequestHandler):

    def check_origin(self, origin):
        return True

    # Handler for the GET requests
    def get(self):
        toDo = str(self.get_argument('toDo'))
        source_path = str(self.get_argument('source_path', ''))
        destination_path = str(self.get_argument('destination_path', ''))
        settings = str(self.get_argument('settings', ''))
        result = layer_settings_manager.run(toDo, source_path, destination_path, settings)
        self.set_header("Content-type", "text/html")
        self.write(result)
        self.finish()

    # Handler for the GET requests
    def post(self):
        toDo = str(self.get_argument('toDo'))
        source_path = str(self.get_argument('source_path', ''))
        destination_path = str(self.get_argument('destination_path', ''))
        settings = str(self.get_argument('settings', ''))
        result = layer_settings_manager.run(toDo, source_path, destination_path, settings)
        self.set_header("Content-type", "text/html")
        self.write(result)
        self.finish()


class add_missing_fields_to_shp_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:
            shp_name = self.get_argument("shp_name")
            missing_fields = json.loads(self.get_argument("fields", ''))
            result = add_missing_fields_to_shp.run(shp_name, missing_fields)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print(str(e))


class copyDBFfields_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = copyDBFfields.run(params)

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 16c")
            print(str(e))


class fileHandler_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def post(self):
        try:
            input = self.get_argument("input", '')
            output = self.get_argument("output", '')
            operation = self.get_argument("operation", '')
            content = self.get_argument("content", '')
            self.set_header("Content-type", "text/html")
            result = fileHandler.run(input, output, operation, content)
            if result:
                self.write(result)

        except Exception as e:
            print("File Handler error")
            print(str(e))

        self.finish()


class get_files_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:

            fileType = self.get_argument("fileType", '')
            regex = self.get_argument("regex", '')
            data_path = self.get_argument("data_path", '')

            result = get_files.run(fileType, regex, data_path)
            self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            # pass
            print("ERROR 5" + str(e))


class get_raster_stats_by_band_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:

            data_path = self.get_argument("data_path", '')
            band = int(self.get_argument("band", '1'))

            result = get_raster_stats_by_band.run(data_path, band)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print(str(e))


class get_user_data_list_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True
        # Handler for the GET requests

    def get(self):
        try:

            dataFolder = str(self.get_argument("dataFolders", ''))
            dataTypes = str(self.get_argument("dataTypes", ''))
            showFolders = str(self.get_argument("showFolders", ''))
            regex = str(self.get_argument("regex", ''))

            result = get_user_data_list.run(dataFolder, dataTypes, showFolders, regex)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 6")
            print(str(e))


class get_vector_DBF_in_Json_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:

            filepath = str(self.get_argument("filepath", ''))
            sort = str(self.get_argument("sort", ''))
            limit = str(self.get_argument("limit", ''))
            start = str(self.get_argument("start", ''))
            result = get_vector_DBF_in_Json.run(filepath, sort, limit, start)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 7")
            print(str(e))


class get_vector_stats_by_layer_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:
            data_path = self.get_argument("data_path", None)
            legend_type = self.get_argument("legend_type", None)
            layer = self.get_argument("layer", None)

            result = get_vector_stats_by_layer.run(data_path, legend_type, layer)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 8")
            print(str(e))


class getHtmlFile_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:
            file = self.get_argument("file", '')
            result = getHtmlFile.run(file)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR9")
            print(str(e))


class getImageFile_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:
            file = self.get_argument("filename", '')
            img_format = self.get_argument("format", '')
            result = getImageFile.run(file, img_format)
            self.set_header("Content-type", "image/" + img_format)
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR9a")
            print(str(e))


class goToPrevNextFeature_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def post(self):
        try:
            shp = self.get_argument("shp", None)
            items = self.get_argument("items", 'fid')
            currFeatureID = self.get_argument("currFeatureID", 0)
            direction = self.get_argument("direction", 'next')
            geom = self.get_argument("geom", False)
            filter = self.get_argument("filter", '')
            result = goToPrevNextFeature.run(shp, items, currFeatureID, direction, geom, filter)
            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 10")
            print(str(e))


class geotiff_to_KML_wms_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        import mapscript
        req = mapscript.OWSRequest()
        # req.loadParams()
        req.loadParamsFromURL(self.request.uri)
        req.setParameter('SERVICE', 'WMS')
        req.setParameter('VERSION', '1.1.0')
        req.setParameter('FORMAT', 'image/png')
        try:
            result = geotiff_to_KML_wms.run(req)

            # if params['REQUEST'] == 'GetMap':
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Content-type", "image/png")
            # else:
            #     self.set_header("Content-type", "text/html")
            self.write(result)

        except Exception as e:
            print("ERROR 11")
            print(str(e))

        # self.finish()


class python_tool_launcher_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = python_tool_launcher.run(params)

            self.set_header("Content-type", "text/html")
            # self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 12")
            print(str(e))

    def post(self):
        try:
            params = dict()
            filters = self.request.arguments
            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = python_tool_launcher.run(params)
            self.set_header("Content-type", "text/html")
            # self.write(result)
            self.finish()

        except Exception as e:
            print("ERROR 13")
            print(str(e))
            self.finish()


class get_attribute_class_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            params = dict()
            filters = self.request.arguments
            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = get_attribute_class.run(params)

            self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 14")
            print(str(e))


class query_file_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            params = dict()
            filters = self.request.arguments
            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = query_file.run(params)

            self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 14")
            print(str(e))


class retrieve_cluster_class_by_click_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()

            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = retrieve_cluster_class_by_click.run(params)

            self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 15")
            print(str(e))







class save_info_on_shapefile_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = save_info_on_shapefile.run(params)

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 16")
            print(str(e))

class save_info_on_shapefile_QAssessment_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()

            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = save_info_on_shapefile_QAssessment.run(params)

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 16a")
            print(str(e))

class dissolve_features_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()
            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = dissolve_features.run(params)

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 16b")
            print(str(e))

class save_user_feature_to_shapefile_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):
        try:
            params = dict()

            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = save_user_feature_to_shapefile.run(params)

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 17")
            print(str(e))


class save_user_settings_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def post(self):

        try:
            params = dict()

            filters = self.request.arguments

            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = save_user_settings.run(params)
            # print result

            # self.set_header("Content-type", "text/html")
            # self.write(result)
            # self.finish()
        except Exception as e:
            print("ERROR 18")
            print(str(e))


class updateDBFrecord_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            params = dict()
            filters = self.request.arguments
            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            result = updateDBFrecord.run(params)
            # print result

            self.set_header("Content-type", "text/html")
            self.write(result)
            self.finish()
        except Exception as e:
            print("ERROR 19")
            print(str(e))


class download_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

    def get(self):
        try:
            params = dict()
            filters = self.request.arguments
            for k, v in filters.items():
                # Do filtering etc...
                params[k] = v[0]
            # download.run(params)
            file_name = params.get('file_path').decode('utf-8')
            buf_size = 4096
            self.set_header('Content-Type', 'application/octet-stream')
            self.set_header('Content-Disposition', 'attachment; filename=' + os.path.basename(file_name))
            with open(file_name, 'rb') as f:
                while True:
                    data = f.read(buf_size)
                    if not data:
                        break
                    self.write(data)
            self.finish()
        except Exception as e:
            print("ERROR 20")
            print(str(e))



class getESAquicklooks_Handler(web.RequestHandler):
    def check_origin(self, origin):
        return True

        # Handler for the GET requests

    def get(self):
        try:

            shp_names = self.get_argument("shp_names", '')
            sat = self.get_argument("sat", '')
            startDate = self.get_argument("dateStart", '')
            endDate = self.get_argument("dateEnd", '')
            clouds = self.get_argument("clouds", '')
            usr = self.get_argument("usr", '')
            pwd = self.get_argument("pwd", '')

            result = getESAquicklooks.run(shp_names, sat, startDate, endDate, clouds, usr, pwd)
            self.set_header("Content-type", "application/json")
            self.write(result)
            self.finish()
        except Exception as e:
            # pass
            print("ERROR 21:  " + str(e))





if __name__ == "__main__":


    me, IMPACT_HTTP_INTERNAL_PORT = sys.argv
    impactRoot = os.path.dirname(os.path.realpath(__file__)).replace('libs_python','')
    print()
    print()
    print(impactRoot)
    os.chdir(impactRoot)

    # test connection & verify version
    GLOBALS['online'] = str(False)
    GLOBALS['repository'] = GitRepository.get_status()
    GLOBALS['version'] = GitRepository.current_version()

    application = web.Application([
        # (r"/APP2/*/*.*", testHandler),
        (r"/", RootHandler),
        (r"/IMPACT", MainHandler),
        (r"/getGlobals", getGlobalsHandler),
        (r"/getSettings", getSettingsHandler),

        (r"/check4update.py", UpdateHandler),

        (r'/add_missing_fields_to_shp.py', add_missing_fields_to_shp_Handler),  # fixed
        (r'/copyDBFfields.py', copyDBFfields_Handler),  # fixed
        (r'/fileHandler.py', fileHandler_Handler),  # fixed
        (r'/get_files.py', get_files_Handler),  # fixed
        (r'/get_raster_stats_by_band.py', get_raster_stats_by_band_Handler),  # fixed
        (r'/get_user_data_list.py', get_user_data_list_Handler),  # fixed
        (r'/get_vector_DBF_in_Json.py', get_vector_DBF_in_Json_Handler),  # fixed
        (r'/get_vector_stats_by_layer.py', get_vector_stats_by_layer_Handler),  # fixed
        (r'/get_attribute_class.py', get_attribute_class_Handler),  # fixed


        (r'/getHtmlFile.py', getHtmlFile_Handler),
        (r'/getImageFile.py', getImageFile_Handler),
        (r'/goToPrevNextFeature.py', goToPrevNextFeature_Handler),  # fixed


        (r'/retrieve_cluster_class_by_click.py', retrieve_cluster_class_by_click_Handler),  # fixed
        (r'/save_info_on_shapefile.py', save_info_on_shapefile_Handler),  # fixed
        (r'/dissolve_features.py', dissolve_features_Handler),  # fixed
        (r'/save_info_on_shapefile_QAssessment.py', save_info_on_shapefile_QAssessment_Handler),  # fixed
        (r'/save_user_feature_to_shapefile.py', save_user_feature_to_shapefile_Handler),  # fixed

        (r'/query_file.py', query_file_Handler),  # fixed
        (r'/updateDBFrecord.py', updateDBFrecord_Handler),  # fixed
        (r'/download_file', download_Handler),  # fixed

        (r'/getESAquicklooks.py', getESAquicklooks_Handler),  # fixed

        (r'/save_user_settings.py', save_user_settings_Handler),

        (r'/python_tool_launcher.py', python_tool_launcher_Handler),

        (r'/layer_settings_manager.py', layer_settings_manager_Handler),
        (r'/ImageStore', ImageStore_SocketHandler),
        (r'/CSImageStore', CSImageStore_SocketHandler),
        (r'/LogStore', LogStore_SocketHandler),

        (r'/geotiff_to_KML_wms.py', geotiff_to_KML_wms_Handler),

        (r'/(.py)', web.StaticFileHandler, {'path': "libs_python"}),
        (r'/(.*)', web.StaticFileHandler, {'path': "./"})

    ],debug=False, autoreload=True
    )
    application.listen(IMPACT_HTTP_INTERNAL_PORT)


    try:
        import tornado
        print('..... loading tornado .... ')
        tornado.ioloop.IOLoop.instance().start()

    except Exception as e:
        print('iloop error')
        print(e)
