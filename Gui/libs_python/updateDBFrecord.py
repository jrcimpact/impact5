#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import os


# Import local libraries
import ImageStore

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

import time

def run(params):

    # retrieve parameters from request #
    #params = cgi.FieldStorage()
    data_path = params.get("data_path",b'').decode()
    sql = params.get("sql", b'').decode()

    # -----------------------------------------------------
    # ---------------  INIT SHAPEFILE ---------------------
    # -----------------------------------------------------

    try:
        dataSource = ogr.Open(data_path, 1)
        layer = dataSource.GetLayer(0)
        layerName = str(layer.GetName())
        dataSource.ExecuteSQL(sql, dialect='SQLITE')
        dataSource.ExecuteSQL('CREATE SPATIAL INDEX ON ' + layerName)
        dataSource.Destroy()
        dataSource = None
        layer = None
        ImageStore.update_file_info(data_path)
        return '1'


    except Exception as e:
        print(str(e))
        return '-1'

# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     print main()
