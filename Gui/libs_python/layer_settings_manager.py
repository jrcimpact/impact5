#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.


# Import local libraries
#import cgi
import json
import ImageSettingsStore


# def main():
#     records = ImageSettingsStore.get_all()
#     print '%s' % json.dumps(records)


def run(to_do, source_path, destination_path, settings):
    if to_do == 'load':
        records = ImageSettingsStore.get_all()
        return json.dumps(records)

    if to_do == 'save':
        #source_path = params['source_path'].value
        #settings = params['settings'].value #json.load(params['settings'].value)
        record = dict()
        record['full_path'] = source_path
        record['settings'] = settings
        return ImageSettingsStore.save_file(source_path, record)

    if to_do == 'delete':
        #full_path = params['source_path'].value
        return ImageSettingsStore.delete_file(source_path)

    if to_do == 'move':
        try:
            #source_path = params['source_path'].value
            #destination_path = params['destination_path'].value
            #settings = params['settings'].value
            print('del'+source_path)
            return ImageSettingsStore.delete_file(source_path)


            record = dict()
            record['full_path'] = destination_path
            record['settings'] = settings
            print('saving '+destination_path)
            return ImageSettingsStore.save_file(destination_path, record)
        except Exception as e:
            print(str(e))

# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     params = cgi.FieldStorage()
#     to_do = params['toDo'].value
#
#     if to_do == 'load':
#         records = ImageSettingsStore.get_all()
#         print json.dumps(records)
#
#     if to_do == 'save':
#         source_path = params['source_path'].value
#         settings = params['settings'].value #json.load(params['settings'].value)
#         record = dict()
#         record['full_path'] = source_path
#         record['settings'] = settings
#         print ImageSettingsStore.save_file(source_path, record)
#
#     if to_do == 'delete':
#         full_path = params['source_path'].value
#         print ImageSettingsStore.delete_file(full_path)
#
#     if to_do == 'move':
#         try:
#             source_path = params['source_path'].value
#             destination_path = params['destination_path'].value
#             settings = params['settings'].value
#             print 'del'+source_path
#             print ImageSettingsStore.delete_file(source_path)
#             print 'ok'
#
#             record = dict()
#             record['full_path'] = destination_path
#             record['settings'] = settings
#             print 'saving '+destination_path
#             print ImageSettingsStore.save_file(destination_path, record)
#         except Exception as e:
#             print str(e)

