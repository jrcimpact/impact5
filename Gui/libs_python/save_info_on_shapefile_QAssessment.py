#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json

# Import local libraries
import ImageStore

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr


def run(params):

    # retrieve parameters from request #
    #params = cgi.FieldStorage()
    shp_name = params.get("shp_name").decode()


    saveFieldValue = params.get("saveFieldValue").decode()

    fid = params.get("fid").decode()                            # name of the ID column
    list_ids = params.get("list_ids", b'[]').decode()           # user manual poly selection IDs


    # -----------------------------------------------------
    # ---------------  INIT SHAPEFILE ---------------------
    # -----------------------------------------------------

    try:

        list_ids = json.loads(list_ids)
        saveFieldValue = json.loads(saveFieldValue)


        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(shp_name, 1)
        layer = dataSource.GetLayer(0)
        layerName = str(layer.GetName())

        layer.SetIgnoredFields(["OGR_STYLE"])  # little bit faster do not add "OGR_GEOMETRY" will not save back the geom

        # ------------------------------------------
        # ----------  RECODE LIST of IDs  ----------
        # ------------------------------------------
        #print(saveFieldValue)
        #print('--------')
        ids_len = len(list_ids)
        if ids_len > 0:
            sql = 'UPDATE "%s" SET ' % (layerName)
            # where_ids = ','.join(map(lambda x: "'"+str(x)+"'", list_ids))
            where_ids = ','.join(map(lambda x: str(x), list_ids))

            what = ''
            for item in saveFieldValue:
                # key = [*item][0]
                # print(key,item[key])
                what += ' "%s" = \'%s\',' % (item['key'], item['value'])

            # where =' WHERE "%s" IN (%s);' % (fid, where_ids)
            where =' WHERE %s IN (%s);' % (fid, where_ids)

            sql += what[:-1]
            sql += where
            #print('sql')
            #print(sql)

            #''"%s" = "%s" ,"%s" = \'%s\' WHERE "%s" IN (%s);' % (layerName, saveToClass, saveToClassValue, saveTo, saveValue, fid, where_ids)
            #sql = 'UPDATE "%s" SET "%s" = \'%s\' WHERE "%s" IN (%s);' % (layerName, saveTo, saveValue, fid, where_ids)
            dataSource.ExecuteSQL(sql, dialect='SQLITE')




        dataSource.Destroy()
        dataSource = None
        layer = None

        # Update record in the Database
        ImageStore.update_file_info(shp_name)
        return '1'

    except Exception as e:
        print('ERROR .-......')
        print(str(e))
        return '-1'

# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     print main()
