#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries

import json
import re
import os
import sys
import ctypes
import itertools
import string

# Import local libraries
from __config__ import *
import FileSystem


def conditions_on_file(file_name, file_type):
    basename, file_extension = os.path.splitext(file_name)
    file_extension = file_extension.lower()

    # Sentinel2 #
    if file_type == 'sentinel2':
        if file_extension == '.zip':
            return bool(re.search('S2[AB]_.*MSI.*L[12][AC]_', basename)) or (bool(re.search('L[12][AC]_T', basename)))
                                                                            # USGS name format

    # RapidEye #
    elif file_type == 'rapideye':
        if file_extension == '.tif' and \
                not basename.endswith('_browse') and \
                not basename.endswith('_calrefx10k') and \
                not basename.endswith('_udm'):
            return bool(re.search("_RE[1-9]_", basename))

    # Landsat #
    elif file_type == 'landsat':
        if file_extension == '.bz' or file_extension == '.gz' or file_extension == '.tar':
            return True

    # All GDAL supported file types #
    elif file_type == 'gdal_rasters':
        if file_extension not in \
                ['', '.txt', '.shp', '.dbf', '.shx', '.prj', '.xml', '.sbn', '.sbx', '.cpg', '.qpj', '.avi', '.gz',
                 '.bz', '.zip', '.tfw', '.jpw', '.gfw', '.IMD', '.kml', '.kmz','.rar', '.bat', '.doc', '.docx'] and \
                not basename.endswith('.gitkeep') and \
                not basename.endswith('.gitignore'):
            return True
    # Landsat #
    elif file_type == 'jpg':
        if file_extension == '.jpg' or file_extension == '.JPG':
            return True
    return False


#if __name__ == '__main__':
def run(fileType, regex, data_path):
    try:
        import os

        fileType = fileType.lower()
        data = []
        # use default DATA path
        if data_path == 'IMPACT_DATA_FOLDER':
            data_path = GLOBALS['data_paths']['data']
            for fileName in FileSystem.traverse_recursively(data_path):
                if conditions_on_file(fileName, fileType):
                    # data.append(filename)
                    data.append({
                        'full_path': fileName,
                        'basename': os.path.basename(fileName),
                        'directory': 'false'
                    })

        else:
            #
            # fileType == 'sentinel2' or fileType == 'rapideye' or fileType == 'landsat':
            # used in file browser to scan dir by dir
            if data_path == '':

                dirs = FileSystem.get_root_folders()

                for dir in dirs:
                    data.append({
                        'full_path': dir,
                        'basename': dir,
                        'directory': 'true'
                    })

            else:
                if not data_path.endswith('/'):
                    data_path += '/'
                dirs = FileSystem.list_sub_folders(data_path)
                # dirs = [os.path.join(data_path, x) for x in dirs]

                for dir in dirs:
                    data.append({
                        'full_path': os.path.join(data_path, dir+'/'),
                        'basename': dir,
                        'directory': 'true'
                    })

                zipFile = FileSystem.list_files(data_path, regex)
                for fileName in zipFile:
                    if conditions_on_file(fileName, fileType):
                        data.append({
                            'full_path': os.path.join(data_path, fileName),
                            'basename': os.path.basename(fileName),
                            'directory': 'false'
                        })
    except Exception as e:
        print("ERR" + str(e))
        pass

    return json.dumps(data)
