#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json
# from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    print("ERROR on GDAL import")
    import ogr
    import osr

# Import local libraries
import GdalOgr
import Settings

#if __name__ == '__main__':
def run(params):
    try:
        SETTINGS = Settings.load()

        result = []
        layer_files = json.loads(params.get("files", '[]'))
        lat = float(params.get("lat", None))
        lon = float(params.get("lon", None))
        legend = params.get("legend", b'').decode()
        classItem = params.get("classItem", str(SETTINGS['vector_editing']['attributes']['class_t1']))
        EPSG = params.get("EPSG", 3857)



        # retrieve info for each file
        for full_path in layer_files:
            file_info = dict()

            if full_path.endswith(".shp") or full_path.endswith(".kml") or full_path.endswith(".vrt"):
                feature = GdalOgr.query_shapefile_by_point(full_path, lon, lat, lat_lon_epsg=EPSG)
                if len(feature) == 0:
                    continue
                else:
                    # return only field
                    featId = feature[0][classItem]
                    file_info['value'] = featId
                    if legend != '':
                        # search PCT in VECTOR legend
                        for item in SETTINGS['map_legends']['vector']:
                            if item['name'] == legend:
                                for cid in item['classes']:
                                    id = int(cid[0])
                                    if id == int(featId):
                                        file_info['name'] = cid[1].replace('\u00a0','').replace('\u250a','').replace('\u2502','').replace(featId,'')

            result.append(file_info)


            #print(file_info)
    except Exception as e:
        print(e)
        pass
    return json.dumps(result)
