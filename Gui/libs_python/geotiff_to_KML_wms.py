#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import sys
import os
import mapscript

# Import local libraries
import Mapserver
import Colors
from __config__ import *
import Settings

SETTINGS = Settings.load()

def raster_layer(map_obj, LAYER, bands, scale1, scale2, scale3, has_colorTable, legendType):

    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = LAYER
    # layer_obj.setMetaData('wms_title', 'Geotiff 2 WMS')
    # layer_obj.setMetaData('wms_enable_request', '*')
    # layer_obj.setMetaData('wms_srs', "EPSG:3857 EPSG:4326")
    # layer_obj.setMetaData('wms_format', 'image/png')
    # layer_obj.setMetaData('wms_abstract', ".....")
    # layer_obj.setMetaData('wms_attribution_title', "JRC")
    # layer_obj.setMetaData('ows_enable_request', "*")


    layer_obj.status = mapscript.MS_ON
    layer_obj.setProjection("AUTO")
    layer_obj.offsite = mapscript.colorObj(0, 0, 0)


    layer_obj.type = mapscript.MS_LAYER_RASTER
    layer_obj.data = os.path.join(GLOBALS['data_paths']['data'], LAYER)
    layer_obj.clearProcessing()

    if legendType not in ['', 'none']:
        import Settings
        SETTINGS = Settings.load()

        for item in SETTINGS['map_legends']['raster']:
            if item['type'] == legendType:

                for cl in item['classes']:
                    class_obj = mapscript.classObj(layer_obj)
                    new_style = mapscript.styleObj()
                    if float(cl[0]) == float(cl[1]):
                        mincol = cl[3].split(',')
                        class_obj.setExpression(cl[0])
                        new_style.color = mapscript.colorObj(int(mincol[0]), int(mincol[1]), int(mincol[2]))
                    else:
                        class_obj.setExpression("([pixel] >= " + str(cl[0]) + " and [pixel] < " + str(cl[1]) + ")")
                        new_style.rangeitem = "[pixel]"
                        mincol = cl[3].split(',')
                        maxcol = cl[4].split(',')
                        new_style.mincolor = mapscript.colorObj(int(mincol[0]), int(mincol[1]), int(mincol[2]))
                        new_style.maxcolor = mapscript.colorObj(int(maxcol[0]), int(maxcol[1]), int(maxcol[2]))
                        new_style.minvalue = float(cl[0])
                        new_style.maxvalue = float(cl[1])

                    class_obj.insertStyle(new_style)
                    new_style = None

                break

    elif (has_colorTable == 0 or (has_colorTable == 1 and legendType == 'none')):

        print('QUI')
        layer_obj.offsite = mapscript.colorObj(0, 0, 0)
        layer_obj.clearProcessing()
        layer_obj.setProcessing("BANDS=" + bands)
        print('Bands')
        print(bands)
        scale1_1 = float(scale1.split(",")[0])
        scale1_1 -= 1 / 50.  # scale1_1 / 125.
        scale1_2 = float(scale1.split(",")[1])
        scale1_2 += 1 / 50.  # scale1_2 / 125.
        scale2_1 = float(scale2.split(",")[0])
        scale2_1 -= 1 / 50.  # scale2_1 / 125.
        scale2_2 = float(scale2.split(",")[1])
        scale2_2 += 1 / 50.  # scale2_2 / 125.
        scale3_1 = float(scale3.split(",")[0])
        scale3_1 -= 1 / 50.  # scale3_1 / 125.
        scale3_2 = float(scale3.split(",")[1])
        scale3_2 += 1 / 50.  # scale3_2 / 125.

        layer_obj.setProcessing("SCALE_1=" + str(scale1_1) + "," + str(scale1_2))
        layer_obj.setProcessing("SCALE_2=" + str(scale2_1) + "," + str(scale2_2))
        layer_obj.setProcessing("SCALE_3=" + str(scale3_1) + "," + str(scale3_2))



    return layer_obj


def vector_layer(map_obj, LAYER, classItem, legendType):

    layer_obj = mapscript.layerObj(map_obj)
    layer_obj.name = LAYER

    layer_obj.status = mapscript.MS_ON
    layer_obj.setProjection("AUTO")
    layer_obj.offsite = mapscript.colorObj(0, 0, 0)


    layer_obj.setConnectionType(mapscript.MS_OGR, None)
    layer_obj.connection = os.path.join(GLOBALS['data_paths']['data'],LAYER)
    if not classItem:
        layer_obj.type = mapscript.MS_LAYER_LINE
        class_obj = mapscript.classObj(layer_obj)
        style_obj = mapscript.styleObj(class_obj)
        rgb = Colors.hex2rgb(legendType)
        style_obj.outlinecolor.setRGB(rgb[0], rgb[1], rgb[2])
        style_obj.width = 2

    else:
        layer_obj.type = mapscript.MS_LAYER_POLYGON
        #layer_obj.setGeomTransform("( generalize([shape],[map_cellsize]*2 ))")
        layer_obj.classitem = classItem

        if legendType != '':
            # search PCT in VECTOR legend
            for item in SETTINGS['map_legends']['vector']:
                if item['type'] == legendType:
                    for cid in item['classes']:
                        class_i_obj = mapscript.classObj(layer_obj)
                        class_i_obj.setExpression(cid[0])
                        # class_i_obj.setExpression('([' + classItem + '] == ' + classIDs[i] + ')')
                        style_i_obj = mapscript.styleObj(class_i_obj)
                        tmp_color = cid[2].split(",")
                        style_i_obj.outlinecolor.setRGB(0, 0, 0)
                        style_i_obj.width = 0.2
                        style_i_obj.color.setRGB(int(tmp_color[0]), int(tmp_color[1]), int(tmp_color[2]))

                    class_i_obj = mapscript.classObj(layer_obj)
                    style_i_obj = mapscript.styleObj(class_i_obj)
                    style_i_obj.color = mapscript.colorObj(200, 200, 200, 50)
                    style_i_obj.outlinecolor.setRGB(0, 0, 0)
                    style_i_obj.width = 0.5




    return layer_obj


def GetMap(req):
    try:
        # Create map object #
        map_obj = Mapserver.init_map()
        # WMS
        map_obj.setMetaData('wms_title', 'GeoTiff to WMS')
        map_obj.setMetaData('ows_enable_request', "*")
        map_obj.setMetaData('wms_enable_request', "*")
        map_obj.setMetaData('wms_abstract', "WMS abstract")
        map_obj.setMetaData('wms_srs', "EPSG:3857 EPSG:4326")

        map_obj.unit = mapscript.MS_DD
        LAYER = req.getValueByName('LAYERS')

        WIDTH = req.getValueByName('WIDTH')
        HEIGHT = req.getValueByName('HEIGHT')
        if WIDTH is None:
            WIDTH = 1000
            req.setParameter('WIDTH', '1000')
        if HEIGHT is None:
            HEIGHT = 1000
            req.setParameter('HEIGHT', '1000')

        map_size = [WIDTH, HEIGHT]

        BBOX = req.getValueByName('BBOX').split(',')
        SRS = req.getValueByName('SRS').lower()

        map_obj.setProjection("init="+SRS)


        # map_scale = Mapserver.calculate_scale(BBOX, map_size)
        map_obj.setExtent(float(BBOX[0]), float(BBOX[1]), float(BBOX[2]), float(BBOX[3]))
        map_obj.setSize(int(float(map_size[0])), int(float(map_size[1])))

        legendType = req.getValueByName('legendType')

        if LAYER.endswith('.tif'):
            has_colorTable = int(req.getValueByName('has_colorTable'))

            raster_layer(map_obj, LAYER, req.getValueByName('BANDS'), req.getValueByName('SCALE1'),
                     req.getValueByName('SCALE2'), req.getValueByName('SCALE3'), has_colorTable, legendType)
        else:
            classItem = req.getValueByName('classItem')
            # if not classItem:
            #     classItem = str(SETTINGS['vector_editing']['attributes']['class_t1'])
            vector_layer(map_obj, LAYER, classItem, legendType)

        return map_obj

    except Exception as e:
        print("Content-type: text/plain;\n")
        print("ERROR:")
        print(str(e))
        # sys.exit()

def GetCapabilities(params):
    # Create map object #
    map_obj = Mapserver.init_map()
    # map_obj.unit = mapscript.MS_DD
    #
    # map_size = [params['WIDTH'], params['HEIGHT']]
    # BBOX = params['BBOX'].split(',')
    # SRS = str(params['SRS'])
    # LAYER = params['LAYERS']
    # # map_obj.setProjection("init=" + SRS)
    # map_obj.setProjection("+proj=longlat +datum=WGS84 +no_defs")
    #
    # #map_scale = Mapserver.calculate_scale(BBOX, map_size)
    # map_obj.setExtent(float(BBOX[0]), float(BBOX[1]), float(BBOX[2]), float(BBOX[3]))
    # map_obj.setSize(int(float(map_size[0])), int(float(map_size[1])))
    # # -----------------   Sentinel2 layers -------------------------------------
    # raster_layer(map_obj, LAYER, params['BANDS'], params['SCALE1'], params['SCALE2'], params['SCALE3'], 0, '')
    #
    return map_obj




def run(req):
    try:



        # Map loads parameters from OWSRequest, adjusting its SRS, extents,
        # active layers accordingly
        # wms_map.loadWMSRequest('1.1.0', wms_request)
        # if params['REQUEST'] == 'GetMap':
            #mapscript.msIO_installStdoutToBuffer()
        mapscript.msIO_installStdoutToBuffer()
        map = GetMap(req)
        map.OWSDispatch(req)
        content_type = mapscript.msIO_stripStdoutBufferContentType()
        content = mapscript.msIO_getStdoutBufferBytes()

            #map.OWSDispatch(req)
            # win OWSDispatch does not print the correct header
            #content_type = mapscript.msIO_stripStdoutBufferContentType()
            #content = mapscript.msIO_getStdoutBufferBytes()
            # print("Content-type: image/png;")
            # print()
            # print(content)
        #content = Mapserver.map_to_http(map)
            # # # Do special processing for GetCapabilities

        # elif params['REQUEST'] == 'GetCapabilities':
        #
        #     mapscript.msIO_installStdoutToBuffer()
        #     map = GetCapabilities(params)
        #     #map.OWSDispatch(req)
        #     #content_type = mapscript.msIO_stripStdoutBufferContentType()
        #     #content = mapscript.msIO_getStdoutBufferBytes()
        #     # content_type = 'text/xml;'
        #     # print('Content-type: ' + content_type)
        #     # print()
        #     # print(content)
        #     content = Mapserver.map_to_http(map)


        # content = Mapserver.map_to_http(map)

        return content



    except Exception as e:
        print("Content-type: text/plain;")
        print()
        print(e)
        # sys.exit()
# if __name__ == '__main__':
#     try:
#         req = mapscript.OWSRequest()
#         req.loadParams()
#         req.setParameter('SERVICE', 'WMS')
#         req.setParameter('VERSION', '1.1.0')
#         req.setParameter('FORMAT', 'image/png')
#         print()
#         if not req.getValueByName('BBOX'):
#             req.setParameter('BBOX', '-78, -30, 155, 30')
#
#         if not req.getValueByName('SRS'):
#             req.setParameter('SRS', 'epsg:4326')
#         if not req.getValueByName('WIDTH'):
#             req.setParameter('WIDTH', '512')
#         if not req.getValueByName('HEIGHT'):
#             req.setParameter('HEIGHT', '512')
#         if not req.getValueByName('BANDS'):
#             req.setParameter('BANDS', '1,1,1')
#         if not req.getValueByName('SCALE1'):
#             req.setParameter('SCALE1', '1,255')
#             req.setParameter('SCALE2', '1,255')
#             req.setParameter('SCALE3', '1,255')
#
#         if not req.getValueByName('has_colorTable'):
#             req.setParameter('has_colorTable', 0)
#         if not req.getValueByName('legendType'):
#             req.setParameter('legendType', '')
#
#         if req.getValueByName('REQUEST') == 'GetMap':
#             mapscript.msIO_installStdoutToBuffer()
#             map = GetMap(req)
#             map.OWSDispatch(req)
#             # win OWSDispatch does not print the correct header
#             content_type = mapscript.msIO_stripStdoutBufferContentType()
#             content = mapscript.msIO_getStdoutBufferBytes()
#             print("Content-type: image/png;")
#             print()
#             print(content)
#
#             # # # Do special processing for GetCapabilities
#
#         elif req.getValueByName('REQUEST') == 'GetCapabilities':
#
#             mapscript.msIO_installStdoutToBuffer()
#             map = GetCapabilities(req)
#             map.OWSDispatch(req)
#             content_type = mapscript.msIO_stripStdoutBufferContentType()
#             content = mapscript.msIO_getStdoutBufferBytes()
#             content_type = 'text/xml;'
#             print('Content-type: ' + content_type)
#             print()
#             print(content)
#
#
#     except Exception as e:
#         print("Content-type: text/plain;")
#         print()
#         print(e)
#         sys.exit()
