#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi

# Import local libraries
import Settings


def run(params):

    # retrieve parameters from POST
    #params = cgi.FieldStorage()
    to_do = params.get('toDo')

    # Save user's settings
    if to_do == b'save':

        new_settings = params.get('settings')
        Settings.save(new_settings)
        Settings.load_as_json()

    # Delete user's settings
    elif to_do == b'delete':
        Settings.delete()

    return None


# if __name__ == '__main__':
#     print "Content-Type: text/html\n"
#     print main()
