#!/bin python
# Copyright (c) 2021, European Union
# All rights reserved.
# Authors: Simonetti Dario
import sys
import ee
import os

from __config__ import *
import Settings

SETTINGS = Settings.load()
if SETTINGS['internet_options']['username'] != "":
    proxySTR = "http://" + SETTINGS['internet_options']['username'] + ':' + SETTINGS['internet_options']['password'] + '@' + \
               SETTINGS['internet_options']['proxy']
else:
    proxySTR = "http://" + SETTINGS['internet_options']['proxy']

if sys.platform != "linux" and os.path.exists(GLOBALS['proxy']):
    os.environ["HTTPS_PROXY"] = proxySTR
    os.environ["HTTP_PROXY"] = proxySTR

#ee.Autenticate()
ee.Initialize()

def run(params):

    #  this function can be replaced for YYYY and MM by a python func generating the intervals
    #  need to return the start date even in case of Year (only year will compute the annual that is not the same of user selection)
    # for the daily option the collection query is necessary if out is a single image and not a FILM

    lat = params.get("lat", b'0').decode()
    lon = params.get("lon", b'22').decode()
    dateStart = params.get("dateStart", b'2021-01-01').decode()
    dateEnd = params.get("dateEnd", b'2022-01-01').decode()
    timeFrame = (params.get("timeFrame", b'year').decode()).lower()
    sensor = params.get("sensor", b'Landsat (all)').decode()

    imgList = ee.List([])

    def addYYYMMDD(img):
        yyyy = ee.Image(img).date().format('YYYY')
        mm = ee.Image(img).date().format('YYYY-MM')
        dd = ee.Image(img).date().format('YYYY-MM-dd')
        return img.set('year', yyyy).set('month', mm).set('day', dd)

    try:
        aoi1 = ee.Feature(ee.Geometry.Point(float(lon), float(lat)))
        geom = aoi1.geometry()#.buffer(60)

        if sensor == 'Landsat (all)':
            L9 = ee.ImageCollection('LANDSAT/LC09/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day'))
            L8 = ee.ImageCollection('LANDSAT/LC08/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day'))
            L7 = ee.ImageCollection('LANDSAT/LE07/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day'))
            L5 = ee.ImageCollection('LANDSAT/LT05/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day'))
            L4 = ee.ImageCollection('LANDSAT/LT04/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day'))
            imageCollraw = ee.ImageCollection(L9.merge(L8).merge(L7).merge(L5).merge(L4)).map(addYYYMMDD)
        else:
            imageCollraw = ee.ImageCollection("COPERNICUS/S2_SR").filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateEnd).advance(1, 'day')).map(addYYYMMDD)



        if timeFrame == "year":
            # return the max between yyyy-01-01 and user date
            # return the min between all year and user date
            imgList = ee.List(imageCollraw.aggregate_array('year')).distinct().sort().getInfo()
            imgList = list(map(lambda item: (item + '-01-01'), imgList))
        if timeFrame == "month":
            imgList = ee.List(imageCollraw.aggregate_array('month')).distinct().sort().getInfo()
            imgList = list(map(lambda item: (item + '-01'), imgList))

        if timeFrame == "day":
            imgList = ee.List(imageCollraw.aggregate_array('day')).distinct().sort().getInfo()

        return imgList
    except Exception as e:
        print('Error: ' + str(e))
        return '[]'


if __name__ == "__main__":
    print(run({}))
