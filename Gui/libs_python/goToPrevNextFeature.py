#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json

try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr


#if __name__ == '__main__':
def run(shapefileName, items, currFeatureID, direction, geom, filter):

    items = json.loads(items)
    # Open SHP file
    dataSource = ogr.Open(shapefileName, update=0)
    layer = dataSource.GetLayer()
    layerName = layer.GetName()
    inSpatialRef = layer.GetSpatialRef()

    EPSG = "3857"
    if EPSG is not None:
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(int(EPSG))

    inSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    outSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    result = list()

    try:
        if direction == 'next':
            OPER= " > "
            ORDER = " ORDER BY cast(\"" + str(items[0]) + "\" as integer) ASC LIMIT 1"
        else:
            OPER = " < "
            ORDER = " ORDER BY cast(\"" + str(items[0]) + "\" as integer) DESC LIMIT 1"

        WHERE = "WHERE cast(\"" + str(items[0]) + "\" as integer) " + OPER + str(currFeatureID)


        #filter = " and cast(\"T1_class\" as integer(100)) > 0 "
        SQL = "SELECT * FROM \"" + layerName + "\" " + WHERE + " " +filter + ORDER

        # print(SQL)
        page = dataSource.ExecuteSQL(SQL, dialect="SQLITE")

        # Iterate on features
        feature = page.GetNextFeature()

        result.append(dict())
        if feature != None:
            for item in items:
                field_value = feature.GetField(str(item))
                result[0][item] = str(field_value)
            if geom:
                feature_geometry = feature.GetGeometryRef()
                feature_geometry.Transform(coordTransform)
                result[0]['geom'] = str(feature_geometry.ExportToWkt())

        dataSource.ReleaseResultSet(page)
        dataSource.Destroy()
        feature = None
        dataSource = None
        layer = None
        inSpatialRef = None
        outSpatialRef = None
        feature_geometry = None
        page = None
        return json.dumps(result)


    except Exception as e:
        print('ERROR:')
        print(str(e))
        if page is not None :
            dataSource.ReleaseResultSet(page)
        dataSource.Destroy()
        feature = None
        dataSource = None
        layer = None
        inSpatialRef = None
        outSpatialRef = None
        feature_geometry = None

