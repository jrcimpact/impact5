#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
#import cgi
import json
import os

from __config__ import *

# # import GDAL/OGR modules
# try:
#     from osgeo import ogr, osr
# except ImportError:
#     print("ERROR on GDAL import")
#     import ogr
#     import osr

# Import local libraries
import GdalOgr
import ImageStore
import CSImageStore


#if __name__ == '__main__':
def run(params):
    try:
        result = []
        layer_files = json.loads(params.get("layer_files", '[]'))
        lat = float(params.get("lat", None))
        lon = float(params.get("lon", None))
        band = int(params.get("band", 0))
        mapWidth = int(float(params.get("mapWidth", 0)))
        EPSG = int(params.get("EPSG",3857))
        legend = params.get("legend",False)

        #print(EPSG)



        if len(str(band)) == 0:
            band = 0

        # retrieve info for each file
        for full_path in layer_files:
            file_info = dict()


            file_info['data_from_DB'] = ImageStore.get_by_path(full_path)
            if file_info['data_from_DB'] == None:
                file_info['data_from_DB'] = CSImageStore.get_by_path(full_path)

            # no result , try with full path from settings (case of thest LCLU static map query )
            if file_info['data_from_DB'] == None:
                full_path = os.path.join(GLOBALS['data_paths']['data'], full_path)



            if full_path.endswith(".shp") or full_path.endswith(".kml") or full_path.endswith("json"):
                file_info['features'] = GdalOgr.query_shapefile_by_point(full_path, lon, lat, lat_lon_epsg=EPSG, mapWidth=mapWidth)
                if len(file_info['features']) == 0:
                    file_info = dict()
                # else:
                #     if legend:
                #         import Settings
                #         SETTINGS = Settings.load()
                #         CLASS = str(SETTINGS['vector_editing']['attributes']['class_t1'])
                #         val = file_info['features'][0][CLASS]
                #         for item in SETTINGS['map_legends']['vector']:
                #             if item['type'] == 'corinelegend':
                #
                #                 for cl in item['classes']:
                #                     if str(val) == str(cl[0]):
                #                         file_info['description']=cl[1].split('\xa0')[-1]
                #                         file_info['code']=''.join(f'{v}.' for v in val)[:-1]


            if full_path.endswith(".tif") or full_path.endswith(".vrt"):
                file_info['bands'] = GdalOgr.query_tiff_by_point(full_path, lon, lat, band)
                if len(file_info['bands']) == 0:
                    file_info = dict()
                else:
                    file_info['lon'] = file_info['bands'][0]['Geox']
                    file_info['lat'] = file_info['bands'][0]['Geoy']
                    file_info['col'] = file_info['bands'][0]['x']
                    file_info['row'] = file_info['bands'][0]['y']

            if 'data_from_DB' in file_info:
                result.append(file_info)

            #print(file_info)
    except Exception as e:
        print(e)
        pass
    return json.dumps(result)
