#!/bin python
# Copyright (c) 2021, European Union
# All rights reserved.
# Authors: Simonetti Dario
import sys
import ee
import os

from __config__ import *
import Settings


SETTINGS = Settings.load()
if SETTINGS['internet_options']['username'] != "":
    proxySTR = "http://" + SETTINGS['internet_options']['username'] + ':' + SETTINGS['internet_options'][
        'password'] + '@' + \
               SETTINGS['internet_options']['proxy']
else:
    proxySTR = "http://" + SETTINGS['internet_options']['proxy']

if sys.platform != "linux" and os.path.exists(GLOBALS['proxy']):
    os.environ["HTTPS_PROXY"] = proxySTR
    os.environ["HTTP_PROXY"] = proxySTR

#ee.Autenticate()
ee.Initialize()

def run(params):
    lat = params.get("lat", b'').decode()
    lon = params.get("lon", b'').decode()
    date = params.get("date", b'').decode()

    timeFrame = params.get("timeFrame", b'day').decode().lower()
    sensor = params.get("sensor", b'Landsat (all)').decode()
    execMode = str(params.get("execMode", b'').decode())
    #planetUrl = params.get("planetUrl", b'').decode()

    R = params.get("R", b'SWIR1').decode()
    G = params.get("G", b'NIR').decode()
    B = params.get("B", b'RED').decode()
    minVal = float(params.get("minVal", b'0.1').decode())
    maxVal = float(params.get("maxVal", b'0.4').decode())
    inBuff = int(params.get("inBuff", b'30').decode())
    outBuff = int(params.get("outBuff", b'150').decode())

    res = outBuff / 100.
    if res <= 0:
        myScale = 1
    else:
        myScale = int(res)

    url = ''
    try:

        aoi1 = ee.Feature(ee.Geometry.Point(float(lon), float(lat)))
        geom = aoi1.geometry()
        aoiSmallCircle = geom.buffer(inBuff).bounds()
        aoiLargeCircle = geom.buffer(outBuff).bounds()
        aoiImg = ee.Image().byte().paint(ee.FeatureCollection(ee.Feature(aoiLargeCircle)), 1, 2).visualize(
            **{'palette': 'FF0000'})
        aoiImg2 = ee.Image().byte().paint(ee.FeatureCollection(ee.Feature(aoiSmallCircle)), 1, 2).visualize(
            **{'palette': 'FFFF00'})

        vizParamsRaw = {"bands": [R, G, B], 'min': [minVal, minVal, minVal],
                        'max': [maxVal, maxVal, maxVal],
                        'gamma': [1.1, 1.1, 1.1]}


        if execMode != '':

            dateStart = date


            def selectL89bands(image):
                return image.select(["SR_B2", "SR_B3", "SR_B4", "SR_B5", "SR_B6","SR_B7"],
                                    ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2"])

            def selectL457bands(image):
                return image.select(["SR_B1", "SR_B2", "SR_B3", "SR_B4", "SR_B5", "SR_B7"],
                                    ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2"])

            L9 = ee.ImageCollection('LANDSAT/LC09/C02/T1_L2').filterBounds(geom).filterDate(ee.Date(dateStart),
                                                                                            ee.Date(
                                                                                                dateStart).advance(
                                                                                                int(1), 'day')).map(
                selectL89bands).select([R, G, B])

            L8 = ee.ImageCollection('LANDSAT/LC08/C02/T1_L2').filterBounds(geom).filterDate(ee.Date(dateStart),
                                                                                             ee.Date(dateStart).advance(
                                                                                                 int(1), 'day')).map(
                selectL89bands).select([R, G, B])
            L7 = ee.ImageCollection('LANDSAT/LE07/C02/T1_L2').filterBounds(geom).filterDate(ee.Date(dateStart),
                                                                                             ee.Date(dateStart).advance(
                                                                                                 int(1), 'day')).map(
                selectL457bands).select([R, G, B])
            L5 = ee.ImageCollection('LANDSAT/LT05/C02/T1_L2').filterBounds(geom).filterDate(ee.Date(dateStart),
                                                                                             ee.Date(dateStart).advance(
                                                                                                 int(1), 'day')).map(
                selectL457bands).select([R, G, B])
            L4 = ee.ImageCollection('LANDSAT/LT04/C02/T1_L2').filterBounds(geom).filterDate(ee.Date(dateStart),
                                                                                             ee.Date(dateStart).advance(
                                                                                                 int(1), 'day')).map(
                selectL457bands).select([R, G, B])

            imageColl = ee.ImageCollection(L9.merge(L8).merge(L7).merge(L5).merge(L4))

            if imageColl.size().getInfo() == 0:
                print('No images')
                image = ee.Image(255)
                vizParamsRaw = {"bands": ['vis-red', 'vis-green', 'vis-blue'], 'min': [0, 0, 0], 'max': [255, 255, 255]}
            else:
                image = imageColl.first().unitScale(0, 45000)
                # vizParams = {"bands": ["B7", "B4", "B2"],'min': [0.01, 0.115 , 0.05], 'max':  [0.4, 0.4, 0.4], 'gamma': [1.1,1.1,1.1]}

                # vizParams = {"bands": [R, G, B], 'min': [minVal*65000, minVal*65000, minVal*65000], 'max': [maxVal*65000, maxVal*65000, maxVal*65000],
                #              'gamma': [1.1, 1.1, 1.1]}

            image = image.blend(aoiImg).blend(aoiImg2)
            url = image.visualize(**vizParamsRaw).getThumbURL(
                {'region': ee.Feature(geom.buffer(int(float(outBuff) * 1.2))).geometry(), 'scale': myScale,
                 'format': 'png'})
            return url




        if execMode == '':
            print('standard mode')
            dateStart = date

            # vizParams = {"bands": [R + '_median', G + '_median', B + '_median'], 'min': [minVal, minVal, minVal],
            #              'max': [maxVal, maxVal, maxVal], 'gamma': [1.1, 1.1, 1.1]}
            # vizParamsRaw = {"bands": [R, G, B], 'min': [minVal , minVal , minVal ],
            #                 'max': [maxVal , maxVal, maxVal ],
            #                 'gamma': [1.1, 1.1, 1.1]}

            #   -------   Landsat section -------------
            if sensor == 'Landsat (all)':
                #print('IN Landsat')
                def selectL89bands(image):
                    # return image.select(["SR_B2", "SR_B3", "SR_B4", "SR_B5", "SR_B6", "SR_B7", "QA_PIXEL"], ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2", "QA_PIXEL"])
                    return image.select(["B2", "B3", "B4", "B5", "B6", "B7", "QA_PIXEL"], ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2", "QA_PIXEL"])

                def selectL457bands(image):
                    # return image.select(["SR_B1", "SR_B2", "SR_B3", "SR_B4", "SR_B5", "SR_B7", "QA_PIXEL"], ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2", "QA_PIXEL"])
                    return image.select(["B1", "B2", "B3", "B4", "B5", "B7", "QA_PIXEL"], ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2", "QA_PIXEL"])

                L9 = ee.ImageCollection('LANDSAT/LC09/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart),ee.Date(dateStart).advance(1,timeFrame)).map(selectL89bands).select([R, G, B, "QA_PIXEL"])
                L8 = ee.ImageCollection('LANDSAT/LC08/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart),ee.Date(dateStart).advance(1,timeFrame)).map(selectL89bands).select([R, G, B, "QA_PIXEL"])
                L7 = ee.ImageCollection('LANDSAT/LE07/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart),ee.Date(dateStart).advance(1,timeFrame)).map(selectL457bands).select([R, G, B, "QA_PIXEL"])
                L5 = ee.ImageCollection('LANDSAT/LT05/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart),ee.Date(dateStart).advance(1,timeFrame)).map(selectL457bands).select([R, G, B, "QA_PIXEL"])
                L4 = ee.ImageCollection('LANDSAT/LT04/C02/T1_TOA').filterBounds(geom).filterDate(ee.Date(dateStart),ee.Date(dateStart).advance(1,timeFrame)).map(selectL457bands).select([R, G, B, "QA_PIXEL"])

                # print(L8.first())
                def cloudMask(img):
                    qa = img.select('QA_PIXEL')
                    cloud = qa.bitwiseAnd(1 << 5).Or(qa.bitwiseAnd(1 << 3))
                    return img.updateMask(cloud.Not())

                if timeFrame != 'day':
                    image = ee.ImageCollection(L9.merge(L8).merge(L7).merge(L5).merge(L4)).map(cloudMask).median().select([R, G, B])
                else:
                    image = ee.ImageCollection(L9.merge(L8).merge(L7).merge(L5).merge(L4)).first().select([R, G, B])

            #   -------   Sentinel 2 section -------------
            else:
                #print('IN Sentinel')
                def selectS2bands(image):
                    return image.select(["B2", "B3", "B4", "B8", "B11", "B12", "SCL"], ["BLUE", "GREEN", "RED", "NIR", "SWIR1", "SWIR2", "SCL"])
                def cloudMask(img):
                    SCL = img.select('SCL')
                    S2A_clouds = SCL.gt(6).And(SCL.lt(12))
                    S2A_shadows = SCL.eq(3)
                    S2A_masked = S2A_clouds.Or(S2A_shadows)

                    return img.updateMask(S2A_masked.Not())

                imageCollraw = ee.ImageCollection("COPERNICUS/S2_SR").filterBounds(geom).filterDate(ee.Date(dateStart), ee.Date(dateStart).advance(1,timeFrame)).map(selectS2bands).select([R, G, B, "SCL"])
                if timeFrame != 'day':
                    image = imageCollraw.map(cloudMask).median().select([R, G, B])
                else:
                    image = imageCollraw.first().select([R, G, B])

                image = image.unitScale(0, 10000)

                # vizParamsRaw = {"bands": [R, G, B], 'min': [minVal * 10000, minVal * 10000, minVal * 10000],
                #             'max': [maxVal * 10000, maxVal * 10000, maxVal * 10000],
                #             'gamma': [1.1, 1.1, 1.1]}

            image = image.blend(aoiImg).blend(aoiImg2)
            url = image.visualize(**vizParamsRaw).getThumbURL(
                {'region': ee.Feature(geom.buffer(int(float(outBuff) * 1.2))).geometry(), 'scale': myScale,
                 'format': 'png'})
            return url
    except Exception as e:
        print('Error: ' + str(e))
        return 'error'


if __name__ == "__main__":
    # lat = sys.argv[1]
    # lon = sys.argv[2]
    # dateStart = sys.argv[3]
    # dateEnd = sys.argv[4]
    # print(lat)
    # print(lon)
    # print(date)
    print('main')
