#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries

import os
import shutil

# Import local libraries
import ImageStore
from __config__ import *

# import GDAL/OGR modules
try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

import time

def run(params):

    # retrieve parameters from request #
    # params = cgi.FieldStorage()
    # data_path = params.getvalue("data_path",'')
    # infield = params.getvalue("infield", '')
    # outfield = params.getvalue("outfield", '')

    # -----------------------------------------------------
    # ---------------  INIT SHAPEFILE ---------------------
    # -----------------------------------------------------

    try:
        shp_name = params.get("data_path").decode()
        infield = params.get("infield").decode()
        outfield = params.get("outfield").decode()
        bkupID = params.get("backupID", b'').decode()
        bkupIsActive = params.get("bkupIsActive", b'False').decode()

        if bkupIsActive in ['True', 'true', True, 1]:
            bkupIsActive = True
        else:
            bkupIsActive = False

            # preparing BKUP
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_validation")
        if bkupID != '':
            if not os.path.exists(tmp_indir):
                os.makedirs(tmp_indir)

            shp = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp', '_' + bkupID + '.shp'))
            shutil.copy(shp_name, shp)
            shx = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp', '_' + bkupID + '.shx'))
            shutil.copy(shp_name.replace('.shp', '.shx'), shx)
            dbf = os.path.join(tmp_indir, os.path.basename(shp_name).replace('.shp', '_' + bkupID + '.dbf'))
            shutil.copy(shp_name.replace('.shp', '.dbf'), dbf)

        dataSource = ogr.Open(shp_name, update=1)
        layername = os.path.basename(shp_name.replace('.shp',''))
        sql = 'UPDATE "%s" SET %s = %s' % (layername, outfield, infield)
        layer = dataSource.ExecuteSQL(sql, dialect = 'SQLITE')
        dataSource.ExecuteSQL('REPACK ' + layername)
        dataSource.ExecuteSQL('RECOMPUTE EXTENT ON ' + layername)
        dataSource.ExecuteSQL('CREATE SPATIAL INDEX ON ' + layername)

        dataSource.ReleaseResultSet(layer)
        dataSource.Destroy()
        dataSource = None
        return b'1'
    except Exception as e:
        print(str(e))
        return b'-1'

if __name__ == '__main__':
    print("Content-Type: text/html\n")
    print(run('E:/JRC_Dario/IMPACT3/IMPACT/DATA/MAP/bo.shp', 'T1_class', 'T2_class'))
