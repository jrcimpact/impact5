#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import cgi
import shutil
import subprocess
import json

# import GDAL/OGR modules
try:
    from osgeo import ogr, gdal, osr
except ImportError:
    import ogr
    import gdal
    import osr

# Import local libraries
from __config__ import *
import ImageStore
import FileSystem

from zipfile import ZipFile


def auxiliary_files_handler(filename, new_filename):
    auxiliaries = []   # array of pairs ["old name", "new name"]
    # ----  ESRI SHP  ----
    if filename.endswith('.shp'):
        basename, extension = os.path.splitext(filename)
        new_basename, _ = os.path.splitext(new_filename)
        auxiliaries.append([basename+'.shx', new_basename+'.shx'])
        auxiliaries.append([basename+'.prj', new_basename+'.prj'])
        auxiliaries.append([basename+'.dbf', new_basename+'.dbf'])
        if os.path.exists(filename.replace('.shp', '.sbn')):
            auxiliaries.append([basename+'.sbn', new_basename+'.sbn'])
        if os.path.exists(filename.replace('.shp', '.sbx')):
            auxiliaries.append([basename+'.sbx', new_basename+'.sbx'])
        if os.path.exists(filename.replace('.shp', '.cpg')):
            auxiliaries.append([basename+'.cpg', new_basename+'.cpg'])
        if os.path.exists(filename.replace('.shp', '.qpj')):
            auxiliaries.append([basename+'.qpj', new_basename+'.qpj'])
    # ----  TIF  ----
    if filename.endswith('.tif'):
        if os.path.exists(filename.replace('.tif', '.tif.aux.xml')):
            auxiliaries.append([filename + '.aux.xml', new_filename + '.aux.xml'])
    return auxiliaries


def run(input, output, operation, content):

    # retrieve parameters from request #
    # params = cgi.FieldStorage()
    # input = params.getvalue("input", '')
    # output = params.getvalue("output", '')
    # operation = params.getvalue("operation", '')
    # content = params.getvalue("content", '')


    # ----------------------------------------------------------
    # ------------------------  DELETE  ------------------------
    # ----------------------------------------------------------
    if operation == 'convert':
        try:
            if os.path.exists(input):

                # ----  Remove SHP  ----
                if output.endswith('.geojson') :
                    format='GeoJSON'

                if output.endswith('.shp') :
                    format = 'ESRI Shapefile'
                srcDS = gdal.OpenEx(input)
                ds = gdal.VectorTranslate(output, srcDS, format=format)


                # print '1'
            return None
        except Exception as e:

            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('convert', 'convert')
            log.set_input_file(input)
            log.send_error("Error: converting file: " + str(e).replace("'", ''))
            log.close()
            return None




    # ----------------------------------------------------------
    # ------------------------  DELETE  ------------------------
    # ----------------------------------------------------------
    if operation == 'delete':
        try:
            if os.path.exists(input):
                # remove, in case of failure wll be re-inserted
                ImageStore.delete_file(input)
                # ----  Remove SHP  ----
                if input.endswith('.shp'):
                    shpdriver = ogr.GetDriverByName("ESRI Shapefile")
                    shpdriver.DeleteDataSource(input)
                # ----  Remove TIF  ----
                if input.endswith('.tif') or input.endswith('.vrt'):
                    os.remove(input)
                    #tifdriver = gdal.GetDriverByName('GTiff')
                    #tifdriver.DeleteDataSource('"'+input+'"')
                    if os.path.exists(input + '.aux.xml'):
                        os.remove(input + '.aux.xml')
                if input.endswith('.kml') or input.endswith('.geojson') or input.endswith('.json'):
                    os.remove(input)
                # ----  Remove record from database  ----

                #print '1'
            return None
        except Exception as e:

            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('Deleting', 'Deleting')
            log.set_input_file(input)
            log.send_error("Error: file might be open elsewhere: " + str(e).replace("'",''))
            log.close()
            return None

            #print '-1'
    # --------------------------------------------------------
    # ------------------------  MOVE  ------------------------
    # --------------------------------------------------------
    if operation == 'move':
        try:
            if input.endswith('.shp'):
                shutil.move(input, output)
                shutil.move(input.replace('.shp', '.shx'), output.replace('.shp', '.shx'))
                shutil.move(input.replace('.shp', '.prj'), output.replace('.shp', '.prj'))
                shutil.move(input.replace('.shp', '.dbf'), output.replace('.shp', '.dbf'))
                try:
                    shutil.move(input.replace('.shp', '.sbn'), output.replace('.shp', '.sbn'))
                    shutil.move(input.replace('.shp', '.sbx'), output.replace('.shp', '.sbx'))
                except:
                    pass
                # ----  Update record from database  ----
                ImageStore.update_file_info(input, output)
            elif input.endswith('.tif'):
                shutil.move(input, output)
                if os.path.exists(input+'.aux.xml'):
                    shutil.move(input+'.aux.xml', output+'.aux.xml')
                # ----  Update record from database  ----
                ImageStore.update_file_info(input, output)

            elif input.endswith('.kml') or input.endswith('.geojson') or input.endswith('.json'):
                shutil.move(input, output)
                 # ----  Update record from database  ----
                ImageStore.update_file_info(input, output)
            else:
                # move entire directory
                file_list = FileSystem.traverse_recursively(input)
                shutil.move(input, output)
                # ----  Update record from database  ----
                for sub_file in file_list:
                    if sub_file.endswith('.shp') or sub_file.endswith('.tif'):
                        ImageStore.update_file_info(sub_file, sub_file.replace(input, output))
                #print '1'
            #print '1'
            return None
        except:
            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('Moving', 'Moving')
            log.set_input_file(input)
            log.set_parameter('Output', output)
            # test if failure is due to file lenght
            ImageProcessing.validPathLengths(output, None, len(output), log)
            log.send_error("Error: file might be open elsewhere or already existing")
            log.close()
            #print '-1'
            return None

    # ----------------------------------------------------------
    # ------------------------  RENAME  ------------------------
    # ----------------------------------------------------------
    if operation == 'rename':
        try:
            # ----  Rename file (and auxiliaries)  ----
            shutil.move(input, output)
            for old_aux, new_aux in auxiliary_files_handler(input, output):
                shutil.move(old_aux, new_aux)
            # ----  Update record from database  ----
            ImageStore.update_file_info(input, output)
            return None
           # print '1'
        except Exception as e:
            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('Renaming', 'Renaming')
            log.set_input_file(input)
            log.set_parameter('Output', output)
            # test if failure is due to file length
            ImageProcessing.validPathLengths(output, None, len(output), log)
            log.send_error("Error: file might be open elsewhere or already existing")
            log.close()
            print(str(e))
            return None
           # print '-1'

    # ----------------------------------------------------------
    # -------------------------  COPY  -------------------------
    # ----------------------------------------------------------
    if operation == 'copy':
        try:
            if input.endswith('.shp'):
                shutil.copy(input, output)
                shutil.copy(input.replace('.shp', '.shx'), output.replace('.shp', '.shx'))
                shutil.copy(input.replace('.shp', '.prj'), output.replace('.shp', '.prj'))
                shutil.copy(input.replace('.shp', '.dbf'), output.replace('.shp', '.dbf'))
                try:
                    shutil.copy(input.replace('.shp', '.sbn'), output.replace('.shp', '.sbn'))
                    shutil.copy(input.replace('.shp', '.sbx'), output.replace('.shp', '.sbx'))
                except:
                    pass
                # ----  Update record from database  ----
                ImageStore.copy_file_info(input, output)

            elif input.endswith('.tif'):
                shutil.copy(input, output)
                if os.path.exists(input + '.aux.xml'):
                    shutil.copy(input + '.aux.xml', output + '.aux.xml')
                # ----  Update record from database  ----
                ImageStore.copy_file_info(input, output)
            else:
                # used in backup / undo call
                shutil.copy(input, output)
            # ----  Update record from database  ----
            # ImageStore.copy_file_info(input, output)
            #print '1'
            return b'1'
        except Exception as e:
            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('Copying', 'Copying')
            log.set_input_file(input)
            log.set_parameter('Output', output)
            # test if failure is due to file length
            ImageProcessing.validPathLengths(output, None, len(output), log)
            log.send_error("Error: file might be open elsewhere or already existing: "+str(e).replace("'",''))
            log.close()
            #print '-1'
            return None

        # ----------------------------------------------------------
        # ------------------------  UNDO Vector editing ------------
        # ----------------------------------------------------------
    if operation == 'undo':

        # ----  Rename file  ----
        tmp_indir = os.path.join(GLOBALS['data_paths']['tmp_data'], "tmp_validation")
        input = os.path.join(tmp_indir, input)
        if os.path.exists(input):
            try:
                # ImageStore.set_status(output.replace('.dbf','.shp'), 'pending')

                shutil.copy(output, output+'_bkup')
                shutil.copy(os.path.join(tmp_indir,input), output)
                shp = input.replace('.dbf','.shp')
                if os.path.exists(shp):
                    shutil.copy(os.path.join(tmp_indir, shp), output.replace('.dbf','.shp'))
                shx = input.replace('.dbf', '.shx')
                if os.path.exists(shx):
                    shutil.copy(os.path.join(tmp_indir, shx), output.replace('.dbf', '.shx'))

                layerName = os.path.basename(output).replace('.dbf','')
                os.system('ogrinfo -sql "CREATE SPATIAL INDEX ON ' + layerName + '" ' + output.replace('.dbf','.shp'))
                return b'1'
            except Exception as e:
                shutil.copy(output + '_bkup', output)
                print('Undo error')

                # ImageStore.set_status(output.replace('.dbf', '.shp'), 'true')
                print(e)
                return b'Error, file is open '
        else:
            return b'No file'


    # ----------------------------------------------------------
    # -------------------------  GoogleEarth  -------------------------
    # ----------------------------------------------------------
    if operation == 'GoogleEarth':
        try:
            # if input.endswith('.shp'):
            #     #shutil.copy(input, output.replace('.shp', '.kml'))
            #     f = open(output.replace('.shp', '.kml'), 'w')
            # elif input.endswith('.tif'):
            #     f = open(output.replace('.tif', '.kml'), 'w')
            # else:
            #     f = open(output+'.kml', 'w')
            f = open(output, 'w')
            f.write(content)
            f.close()

            # change working dir to TEMP; GE is saving temporary files there
            tmp = os.environ.get('TEMP', os.environ.get('TMP'))
            if tmp == 'None' or tmp == None or tmp == '':
                tmp = os.environ.get('HOMEPATH','c:/')
            cmd = 'cd ' + tmp + ' && ' + output

            subprocess.Popen(cmd, shell=True, env=dict(os.environ))
            #print '1'
            return None
        except Exception as e:
            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('GoogleEarth', 'GoogleEarth')
            log.set_input_file(input)
            log.set_parameter('Output', output)
            # test if failure is due to file length
            ImageProcessing.validPathLengths(output, None, len(output), log)
            log.send_error("Error: file might be open elsewhere or already existing")
            log.send_error(str(e))
            log.close()
            return None
           # print '-1'
    # ----------------------------------------------------------
    # -------------------------  Shift image  ------------------
    # ----------------------------------------------------------
    if operation == 'shift':
        try:
            if input.endswith('.tif'):
                ImageStore.set_status(input, 'pending')
                ds = gdal.Open(input, gdal.GA_Update)
                extent = json.loads(content)
                ulx = float(extent['W'])
                uly = float(extent['N'])
                lrx = float(extent['E'])
                lry = float(extent['S'])
                gt = [ulx, (lrx - ulx) / ds.RasterXSize, 0, uly, 0, (lry - uly) / ds.RasterYSize]
                ds.SetGeoTransform(gt)
                #ds.Destroy()
                ds = None
                os.utime(input, None)
                result = ImageStore.update_file_info_and_geo(input)
                ImageStore.set_status(input, 'true')
                return result['extent']
            else:
                raise
            #print '1'

        except Exception as e:
            import LogStore
            import ImageProcessing
            log = LogStore.LogProcessing('Shift Image', 'ImageShift')
            log.set_input_file(input)
            log.set_parameter('Coordinates', content)
            # test if failure is due to file length
            log.send_error("Error: file might be open elsewhere or wrong coordinates")
            log.send_error(str(e).replace("'",""))
            log.close()
            ImageStore.set_status(input, 'true')
            #print '-1'
            print("ERROR")
            return None
    # # ----------------------------------------------------------
    # # -------------------------  Download     ------------------
    # # ----------------------------------------------------------
    if operation == 'download':
        try:
            if os.path.exists(input):
                try:
                    TMPDIR = os.path.join(GLOBALS['data_paths']['tmp_data'], "download")
                    if not os.path.exists(TMPDIR):
                        os.makedirs(TMPDIR)

                    if input.endswith('.shp'):
                        basename1, extension = os.path.splitext(input)

                        zipname = os.path.basename(input).replace('.shp','')
                        outZip = os.path.join(TMPDIR, zipname + '.zip')
                        if os.path.exists(outZip):
                            try:
                                os.remove(outZip)
                            except:
                                pass
                        with ZipFile(outZip, 'w') as zipObj2:
                            # Add multiple files to the zip
                            zipObj2.write(basename1 + '.shp', os.path.basename(input))
                            zipObj2.write(basename1 + '.shx', os.path.basename(input).replace('.shp','.shx'))
                            zipObj2.write(basename1 + '.dbf', os.path.basename(input).replace('.shp','.dbf'))
                            zipObj2.write(basename1 + '.prj', os.path.basename(input).replace('.shp','.prj'))

                        return outZip

                        # ----  TIF  ----
                    else:
                        if os.path.exists(input):
                            return input

                except Exception as e:
                    print(e)
                    return ''
            else:
                return ''

        except Exception as e:
            print("ERROR zipping & downloading file")
            print(e)
            return ''
