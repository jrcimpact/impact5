#!python
#
# Copyright (c) 2015, European Union
# All rights reserved.
# Authors: Simonetti Dario, Marelli Andrea
#
#
# This file is part of IMPACT toolbox.
#
# IMPACT toolbox is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# IMPACT toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with IMPACT toolbox.
# If not, see <http://www.gnu.org/licenses/>.

# Import global libraries
import os
import cgi
import json
#from dbfread import DBF
# Import local libraries

from __config__ import *

try:
    from osgeo import ogr, osr
except ImportError:
    import ogr
    import osr

import datetime


# def Record(obj):
#     vals = []
#     for (name, value) in obj:
#         vals.append(value)
#     return vals
#
#
def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()


#if __name__ == '__main__':
def run(filepath, SORT, limit, start):
    # return '{"total":' + str(100) + ', "items":' + json.dumps([], default=datetime_handler) + '}'
    # Retrieve GET parameters #
    #params = cgi.FieldStorage()
    json.JSONEncoder.default = datetime_handler
    #print "Content-Type: text/html\n"
    #filepath = params['filepath'].value.replace('.shp', '.dbf')
    filepath = filepath.replace('.shp', '.dbf')

    EPSG = "3857"

    try:

        shp = ogr.Open(filepath, update=0)
        shplayer = shp.GetLayer(0)
        featureCount = shplayer.GetFeatureCount()
        layerName = shplayer.GetName()
        fieldCount = shplayer.GetLayerDefn().GetFieldCount()

        inSpatialRef = shplayer.GetSpatialRef()
        if EPSG is not None:
            outSpatialRef = osr.SpatialReference()
            outSpatialRef.ImportFromEPSG(int(EPSG))

        inSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        outSpatialRef.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
        #inverseCoordTransform = osr.CoordinateTransformation(outSpatialRef, inSpatialRef)

        info = []
        #SORT = params.getvalue("sort", '')



        if SORT != '':
            SORT = json.loads(SORT)[0]
            SORT = "ORDER BY "+str(SORT['property'])+" "+str(SORT['direction'])

        #page = shp.ExecuteSQL("SELECT * FROM \"" + layerName +"\" "+SORT+" LIMIT "+params['limit'].value+" OFFSET "+params['start'].value, dialect="SQLITE")
        page = shp.ExecuteSQL("SELECT * FROM \"" + layerName +"\" "+SORT+" LIMIT "+str(limit)+" OFFSET "+str(start), dialect="SQLITE")

        feat = page.GetNextFeature()

        while feat is not None:
            line = []
            for i in range(fieldCount):
                line.append(feat.GetField(i))

            # ---- GEOM -----
            feature_geometry = feat.GetGeometryRef()
            feature_geometry.Transform(coordTransform)

            line.append(str(feature_geometry.ExportToWkt()))
            #line.append(feature_geometry.GetEnvelope())
            info.append(line)
            feat.Destroy()
            feat = None
            feat = page.GetNextFeature()

        shp.ReleaseResultSet(page)
        shp.Destroy()
        shp = None
        feat = None
        page = None
        line = None
        feature_geometry = None
        outSpatialRef = None
        inSpatialRef = None
        coordTransform = None
        shplayer=None

        return '{"total":'+str(featureCount)+', "items":'+json.dumps(info, default=datetime_handler)+'}'

    except Exception as e:
        print(str(e))
        return json.dumps([])